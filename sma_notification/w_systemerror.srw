HA$PBExportHeader$w_systemerror.srw
forward
global type w_systemerror from Window
end type
type cb_copy from commandbutton within w_systemerror
end type
type dw_syserror from datawindow within w_systemerror
end type
type cb_save from commandbutton within w_systemerror
end type
type cb_email from commandbutton within w_systemerror
end type
type cb_print from commandbutton within w_systemerror
end type
type cb_continue from commandbutton within w_systemerror
end type
type cb_close from commandbutton within w_systemerror
end type
end forward

type os_memorystatus from structure
	unsignedlong		ul_length
	unsignedlong		ul_memoryload
	unsignedlong		ul_totalphys
	unsignedlong		ul_availphys
	unsignedlong		ul_totalpagefile
	unsignedlong		ul_availpagefile
	unsignedlong		ul_totalvirtual
	unsignedlong		ul_availvirtual
end type

global type w_systemerror from Window
int X=78
int Y=96
int Width=2757
int Height=1788
boolean TitleBar=true
string Title="System Error"
long BackColor=79741120
WindowType WindowType=response!
string Icon="Error!"
cb_copy cb_copy
dw_syserror dw_syserror
cb_save cb_save
cb_email cb_email
cb_print cb_print
cb_continue cb_continue
cb_close cb_close
end type
global w_systemerror w_systemerror

type prototypes
function ulong GetWindowsDirectoryA(ref string dirtext, ulong textlen) library "kernel32.dll" alias for "GetWindowsDirectoryA;Ansi"
function boolean GetUserNameA(ref string lpBuffer, ref int nSize) library "advapi32.dll" alias for "GetUserNameA;Ansi"
function boolean GetComputerNameA(ref string lpbuffer, ref int nSize) library "kernel32.dll" alias for "GetComputerNameA;Ansi"
subroutine GlobalMemoryStatus(ref os_memorystatus memorystatus) library "kernel32.dll" alias for "GlobalMemoryStatus;Ansi"


end prototypes

type variables
u_AbstractClassFactory	iu_ClassFactory
end variables

forward prototypes
public subroutine wf_geterrorinfo ()
public subroutine wf_getenvironment ()
public subroutine wf_getsysteminfo ()
end prototypes

public subroutine wf_geterrorinfo ();
SetPointer(Hourglass!)

If dw_syserror.RowCount() < 1 Then
	dw_syserror.InsertRow(0)
End If

If Error.Text = "" Then Error.Text = "Description unknown"
dw_syserror.SetItem(1,"problem", Error.Text)
dw_syserror.SetItem(1,"object_event", Error.ObjectEvent)
dw_syserror.SetItem(1,"line_number", Error.Line)
dw_syserror.SetItem(1,"error_number", Error.Number)
dw_syserror.SetItem(1,"object", Error.Object)
dw_syserror.SetItem(1,"window_menu", Error.WindowMenu)

Return
end subroutine

public subroutine wf_getenvironment ();Environment lenv_env
String	ls_temp

If GetEnvironment(lenv_env) = -1 Then Return

If dw_syserror.RowCount() < 1 Then
	dw_syserror.InsertRow(0)
End If

// Set CPU information
Choose Case lenv_env.CPUType
	Case I386!
		ls_temp = "Intel 386"
	Case I486!
		ls_temp = "Intel 486"
	Case Pentium!
		ls_temp = "Intel Pentium"
	Case Else
		ls_temp = "Other CPU"
End Choose

dw_syserror.SetItem(1,"cpu", ls_temp)

// Set the screen information
ls_temp = String(lenv_env.ScreenWidth) + " by " + String(lenv_env.ScreenHeight)
dw_syserror.SetItem(1,"resolution", ls_temp)

// Set Windows version
Choose Case lenv_env.OSType
	Case WindowsNT!
		ls_temp = "Windows NT"
	Case Windows!
		ls_temp = "Windows"
	Case Else
		ls_temp = "Non-Windows"
End Choose
ls_temp += " " + String(lenv_env.OSMajorRevision) + "." + &
	String(lenv_env.OSMinorRevision) + "." + &
	String(lenv_env.OSFixesRevision)
dw_syserror.SetItem(1,"winversion", ls_temp)

// Set PB code, type, release, and version
IF lenv_env.MachineCode Then
	ls_temp = "Machine code"
Else
	ls_temp = "P-code"
End If
dw_syserror.SetItem(1,"executable", ls_temp)
Choose Case lenv_env.PBType
	Case Desktop!
		ls_temp = "Desktop"
	Case Enterprise!
		ls_temp = "Enterprise"
	Case Else
		ls_temp = "Unknown"
End Choose
dw_syserror.SetItem(1,"pbtype", ls_temp)
ls_temp = String(lenv_env.PBMajorRevision) + "." + &
	String(lenv_env.PBMinorRevision) + "." + &
	String(lenv_env.PBFixesRevision)
dw_syserror.SetItem(1,"pbversion", ls_temp)

Return
end subroutine

public subroutine wf_getsysteminfo ();String ls_temp
Int li_size = 256
os_memorystatus lstr_memorystatus

If dw_syserror.RowCount() < 1 Then
	dw_syserror.InsertRow(0)
End If

ls_temp = Space(li_size)
GetUserNameA(ls_temp, li_size)
dw_syserror.SetItem(1,"userid", Trim(ls_temp))

li_size = 256
ls_temp = Space(li_size)
GetComputerNameA(ls_temp, li_size)
dw_syserror.SetItem(1,"computer", Trim(ls_temp))

//ls_temp = Space(li_size)
//GetWindowsDirectoryA(ls_temp, li_size)
//dw_syserror.SetItem(1, "windir", Trim(ls_temp))

GlobalMemoryStatus(lstr_memorystatus)
ls_temp = String(lstr_memorystatus.ul_availphys/1024) + "/" + &
	String(lstr_memorystatus.ul_totalphys/1024)
dw_syserror.SetItem(1,"memphysical", ls_temp)

ls_temp = String(lstr_memorystatus.ul_availvirtual/1024) + "/" + &
	String(lstr_memorystatus.ul_totalvirtual/1024)
dw_syserror.SetItem(1,"memvirtual", ls_temp)

Return
end subroutine

on w_systemerror.create
this.cb_copy=create cb_copy
this.dw_syserror=create dw_syserror
this.cb_save=create cb_save
this.cb_email=create cb_email
this.cb_print=create cb_print
this.cb_continue=create cb_continue
this.cb_close=create cb_close
this.Control[]={this.cb_copy,&
this.dw_syserror,&
this.cb_save,&
this.cb_email,&
this.cb_print,&
this.cb_continue,&
this.cb_close}
end on

on w_systemerror.destroy
destroy(this.cb_copy)
destroy(this.dw_syserror)
destroy(this.cb_save)
destroy(this.cb_email)
destroy(this.cb_print)
destroy(this.cb_continue)
destroy(this.cb_close)
end on

event open;
/* --------------------------------------------------------
w_systemerror

<OBJECT>	This window displays information about
			a system error.</OBJECT>
			
<USAGE>	This window is used by
			u_systemerrornotificationcontroller to
			display information relating to an
			occurrence of a PB system error.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */
u_AbstractParameterStack lu_ParameterStack

lu_ParameterStack = Message.PowerObjectParm
lu_ParameterStack.uf_Pop("u_classfactory", iu_classfactory)

This.wf_geterrorinfo()
This.wf_getenvironment()
This.wf_getsysteminfo()
end event

type cb_copy from commandbutton within w_systemerror
int X=1033
int Y=1572
int Width=247
int Height=108
int TabOrder=50
string Text="Co&py"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;If dw_syserror.AcceptText() = -1 Then Return

dw_syserror.SaveAs("",Clipboard!, True)
end event

type dw_syserror from datawindow within w_systemerror
int X=23
int Y=28
int Width=2670
int Height=1520
int TabOrder=10
string DataObject="d_systemerror"
BorderStyle BorderStyle=StyleLowered!
boolean LiveScroll=true
end type

type cb_save from commandbutton within w_systemerror
int X=1303
int Y=1572
int Width=293
int Height=108
int TabOrder=60
string Text="&Save as..."
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;If dw_syserror.AcceptText() = -1 Then Return
dw_syserror.SaveAs()
end event

type cb_email from commandbutton within w_systemerror
int X=1618
int Y=1572
int Width=247
int Height=108
int TabOrder=40
string Text="&Email"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;u_mail	lu_mail
Boolean  lb_Initialized
mailRecipient	mra_Recipients[]
u_AbstractErrorContext	lu_ErrorContext
String	ls_body

If dw_syserror.AcceptText() = -1 Then Return

iu_ClassFactory.uf_GetObject("u_ErrorContext", lu_ErrorContext)
lu_ErrorContext.uf_Initialize()

lb_Initialized = iu_ClassFactory.uf_GetObject("u_mail",lu_Mail,lu_ErrorContext)
IF Not lu_ErrorContext.uf_IsSuccessful() Then
	// Do some error Handling

	
	Return
End If

IF Not lb_Initialized Then lu_Mail.uf_Initialize(iu_ClassFactory,TRUE,lu_ErrorContext)
// You may want to put more error checking here

mra_Recipients[1].Name = "Conrad Engel"
mra_Recipients[1].RecipientType = mailTo!
lu_mail.uf_Address(mra_Recipients, lu_ErrorContext)

lu_mail.uf_SetSubject("System Error occurred on " + dw_syserror.GetItemString(1,"computer"))

ls_body = "A system error occurred at " + String(Now(), "hh:mm") + " on " + String(Today(), "yyyy-mm-dd")
ls_body += " on computer " + dw_syserror.GetItemString(1,"computer") + "."
ls_body += "~r~n~r~nError Information:~r~n"
ls_body += "Description:  " + dw_syserror.GetItemString(1,"problem") + "~r~n"
ls_body += "Error Number: " + String(dw_syserror.GetItemNumber(1,"error_number")) + "~r~n"
ls_body += "Object: " + dw_syserror.GetItemString(1,"object") + "~r~n"
ls_body += "Window/Menu: " + dw_syserror.GetItemString(1,"window_menu") + "~r~n"
ls_body += "Event: " + dw_syserror.GetItemString(1,"object_event") + "~r~n"
ls_body += "Line: " + String(dw_syserror.GetItemNumber(1,"line_number")) + "~r~n"
ls_body += "~r~nSystem Information:~r~n"
ls_body += "OS: " + dw_syserror.GetItemString(1,"winversion") + "~r~n"
ls_body += "CPU: " + dw_syserror.GetItemString(1,"cpu") + "~r~n"
ls_body += "Virtual Memory (Free/Total): " + dw_syserror.GetItemString(1,"memvirtual") + "~r~n"
ls_body += "Physical Memory (Free/Total): " + dw_syserror.GetItemString(1,"memphysical") + "~r~n"
ls_body += "PB: " + dw_syserror.GetItemString(1,"pbtype") + " " + dw_syserror.GetItemString(1,"pbversion") + "~r~n"
ls_body += "Resolution: " + dw_syserror.GetItemString(1,"resolution") + "~r~n"
ls_body += "User: " + dw_syserror.GetItemString(1,"userid") + "~r~n"
ls_body += "~r~nUser Comments:~r~n"
If Not IsNull(dw_syserror.GetItemString(1,"comments")) Then
	ls_body += dw_syserror.GetItemString(1,"comments")
Else
	ls_body += "~r~n~r~n"
End If

lu_mail.uf_SetBody(ls_body)

lu_Mail.uf_Send(lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then
	// Do some error handling
	MessageBox("Problem","An error occurred attempting to send mail.", Exclamation!)
	Return
End If

end event

type cb_print from commandbutton within w_systemerror
int X=1888
int Y=1572
int Width=247
int Height=108
int TabOrder=30
string Text="&Print"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;If dw_syserror.AcceptText() = -1 Then Return
dw_syserror.Print()
end event

type cb_continue from commandbutton within w_systemerror
int X=2158
int Y=1572
int Width=265
int Height=108
int TabOrder=70
string Text="C&ontinue"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Close(Parent)
end event

type cb_close from commandbutton within w_systemerror
int X=2446
int Y=1572
int Width=247
int Height=108
int TabOrder=20
string Text="&Close"
boolean Cancel=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;HALT CLOSE
end event

