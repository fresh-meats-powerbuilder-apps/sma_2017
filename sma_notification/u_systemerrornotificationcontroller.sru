HA$PBExportHeader$u_systemerrornotificationcontroller.sru
forward
global type u_systemerrornotificationcontroller from u_abstractnotificationcontroller
end type
end forward

global type u_systemerrornotificationcontroller from u_abstractnotificationcontroller
end type
global u_systemerrornotificationcontroller u_systemerrornotificationcontroller

type variables
u_abstractclassfactory	iu_classfactory

end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_abstractclassfactory)
public function boolean uf_display (ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_abstractclassfactory);
/* --------------------------------------------------------

<DESC>	Initialize the notification controller.</DESC>

<ARGS>	au_abstractclassfactory: ClassFactory</ARGS>
			
<USAGE>	This function initializes the notification
			controller.  Call it first.</USAGE>
-------------------------------------------------------- */

iu_classfactory = au_abstractclassfactory

Return True
end function

public function boolean uf_display (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Display a notification message.</DESC>

<ARGS>	au_errorcontext: The error context to display</ARGS>
			
<USAGE>	Call this function to display a message for a given error
			context.  Typically this function is called when
			the error context's uf_IsSuccessful() call returns
			false.</USAGE>
-------------------------------------------------------- */
u_abstractparameterstack lu_parameter

iu_classfactory.uf_GetObject("u_parameterstack", lu_parameter)
lu_parameter.uf_Initialize()

lu_parameter.uf_Push("u_classfactory", iu_classfactory)

iu_classfactory.uf_GetResponseWindow("w_systemerror", lu_parameter)

Return True
end function

on u_systemerrornotificationcontroller.create
TriggerEvent( this, "constructor" )
end on

on u_systemerrornotificationcontroller.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_systemerrornotificationcontroller

<OBJECT>	This object is responsible for notifying the user
			or others about messages generated and stored
			in an error context object.</OBJECT>
			
<USAGE>	Get one of these objects, initialize it, and
			in the systemerror event of the application,
			use its uf_Display() method to display
			information given in an error context
			object.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */

end event

