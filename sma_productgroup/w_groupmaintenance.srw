HA$PBExportHeader$w_groupmaintenance.srw
forward
global type w_groupmaintenance from w_abstractsheetext
end type
type st_scroll from u_st_scroll within w_groupmaintenance
end type
type tv_1 from u_prodgrptree within w_groupmaintenance
end type
type lv_1 from u_prodgrplist within w_groupmaintenance
end type
end forward

global type w_groupmaintenance from w_abstractsheetext
integer x = 5
integer y = 4
integer width = 3867
integer height = 1452
string title = ""
boolean toolbarvisible = false
event ue_copygrp ( )
event ue_changelargeicon ( )
event ue_changesmallicon ( )
event ue_changereport ( )
event ue_changelist ( )
event ue_deletegrp ( )
event ue_modifygrp ( )
event ue_arrangelist ( )
event ue_newgrp ( )
st_scroll st_scroll
tv_1 tv_1
lv_1 lv_1
end type
global w_groupmaintenance w_groupmaintenance

type variables
String					is_Title
u_AbstractClassFactory			iu_ClassFactory
u_AbstractPlantDataAccess			iu_PlantDataAccess
u_AbstractTutlTypeDataAccess		iu_TutlTypeDataAccess

u_AbstractDataAccess			iu_TransferTypeDataAccess
u_ErrorContext				iu_ErrorContext
u_NotificationController			iu_Notification	
w_smaframe				iw_frame	
u_user                                 iu_user
u_grpmaintenance		iu_grpmaintenance
u_ParameterStack	           iu_ParameterStack
u_NotificationController       iu_NotificationController

String		is_userid, is_userpw, is_app_name, is_grouptype
end variables

forward prototypes
public function string wf_get_user_id ()
public function string wf_get_user_pw ()
public function string wf_get_working_dir ()
public subroutine wf_pointerposition (ref long pointerx, ref long pointery)
public function boolean wf_get_u_user ()
public subroutine wf_setmicrohelp (string as_input)
public subroutine wf_dbconnection ()
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack)
end prototypes

event ue_copygrp;GetFocus().TriggerEvent('ue_copygrp')
end event

event ue_changelargeicon;lv_1.View = listviewlargeicon!
end event

event ue_changesmallicon;lv_1.View = listviewsmallicon!
end event

event ue_changereport;lv_1.View = listviewreport!
end event

event ue_changelist;lv_1.View = listviewlist!
end event

event ue_deletegrp;GetFocus().TriggerEvent('ue_deletegrp')
end event

event ue_modifygrp;GetFocus().TriggerEvent('ue_modifygrp')
end event

event ue_arrangelist;lv_1.Arrange()
end event

event ue_newgrp;GetFocus().TriggerEvent('ue_newgrp')
end event

public function string wf_get_user_id ();Return is_userid
end function

public function string wf_get_user_pw ();Return is_userpw

end function

public function string wf_get_working_dir ();String		ls_working_dir
//
//
//ls_working_dir = "C:\pbtest32\"
//
//Return ls_working_dir

//String		ls_userini, ls_workingdir
//u_sdkcalls	lu_sdkcalls
//
//
//// Get Absolute path to ibpuser.ini file
//lu_sdkcalls = Create u_sdkcalls
//lu_sdkcalls.nf_GetUserIniPath(ls_userini, ls_workingdir)
//
//
Return ls_working_dir
end function

public subroutine wf_pointerposition (ref long pointerx, ref long pointery);pointerx = iw_frame.PointerX()
pointery = iw_frame.PointerY()
//pointerx = 900
//pointery = 500
end subroutine

public function boolean wf_get_u_user ();
If Not iu_ClassFactory.uf_GetObject( "u_user", iu_user, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_user.uf_Initialize( iu_ClassFactory, iu_ErrorContext )
	End If
End If

iu_user.uf_Retrieve( iu_ParameterStack, iu_ErrorContext )
If iu_ErrorContext.uf_GetReturnCode( ) = - 1 Then
	iu_NotificationController.uf_Initialize( iu_ClassFactory )
	iu_NotificationController.uf_Display( iu_ErrorContext )
	iu_ErrorContext.uf_Initialize( )
	Return False
End If

iu_ParameterStack.uf_Pop( 'string', is_userpw )
iu_ParameterStack.uf_Pop( 'string', is_userid )

Return True
end function

public subroutine wf_setmicrohelp (string as_input);//wf_setmicrohelp(String)
This.SetMicrohelp(as_input)
end subroutine

public subroutine wf_dbconnection ();string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

//SQLCA = Create transaction // Create a transaction object
SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")

if Upper(SQLCA.dbms) = Upper("SNC SQL Native Client(OLE DB)") Then					 // Connecting to a SQL Server 
	SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
	SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password
	SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
Else
	SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
End If

Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode <> 0 then
	If SQLCA.SQLErrText = "Transaction already connected" Then
		//do nothing - OK
	Else	
		Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
		Disconnect using SQLCA;
		Return
	End If
End if
end subroutine

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
SetPointer(HourGlass!)

iu_ClassFactory = au_ClassFactory
iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then Return False

iu_ClassFactory.uf_GetObject( "u_ParameterStack", iu_ParameterStack, iu_ErrorContext )
iu_ParameterStack.uf_Initialize( )

If Not wf_get_u_user() Then
	MessageBox("Error","Unable to get user id")
	Return False
End If

// initialize sma group maintenance dll
IF NOT iu_grpmaintenance.uf_initializesma( is_grouptype, iu_ClassFactory,iu_ErrorContext) Then
	MessageBox("Error", "Initialize For SMA ugrpmaintenance failed")
End If

is_app_name = GetApplication().AppName

// ** IBDKEEM ** 03/13/2002 ** Make SQL Server Complient. Quick Fix. 
// connect to pblocaldb if not allready
wf_dbConnection()

// inquire on groups
If Not iu_grpmaintenance.uf_inquire() Then
	Return False
End If
//This.Event Post ue_inquire()
Return True

end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack);String 	ls_ClassName, &
			ls_Parameter, & 
			ls_WindowName, &
			ls_Type

If Not IsValid(au_parameterstack) Then 
	MessageBox("Open","This window can not be opened in this manner.")
	Close(This)
	Return FALSE
END IF

au_parameterstack.uf_Pop("string",ls_Type)
au_parameterstack.uf_Pop("string",ls_WindowName)
au_parameterstack.uf_Pop("string",ls_Parameter)
au_parameterstack.uf_Pop("string",ls_ClassName)

this.title = ls_WindowName

if (ls_Parameter = 'P') or (ls_Parameter = 'C') or (ls_Parameter = 'L') Then
	is_grouptype = ls_Parameter
Else
	MessageBox("Open","This window can not be opened in this manner.")
	Close(this)
	return FALSE		
End If

IF NOT wf_initialize(au_ClassFactory,au_ErrorContext) THEN
	Close(this)
	Return FALSE
END IF



end function

on w_groupmaintenance.create
int iCurrent
call super::create
this.st_scroll=create st_scroll
this.tv_1=create tv_1
this.lv_1=create lv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_scroll
this.Control[iCurrent+2]=this.tv_1
this.Control[iCurrent+3]=this.lv_1
end on

on w_groupmaintenance.destroy
call super::destroy
destroy(this.st_scroll)
destroy(this.tv_1)
destroy(this.lv_1)
end on

event ue_inquire;Return iu_grpmaintenance.uf_inquire()

Return False

end event

event closequery;//If dw_detail.ModifiedCount() + dw_detail.DeletedCount() > 0 Then
//	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
//	CASE 1	// Save Changes
//		If Not This.Event ue_save() Then
////			Message.ReturnValue = 1	 // Update failed - do not close window
//			Return 1
//		Else
////			Message.ReturnValue = 0
//		End If
//
//	CASE 2	// Do not save changes
////		Message.ReturnValue = 0
//
//	CASE 3	// Cancel the closing of window
////		Message.ReturnValue = 1
//		Return 1
//	END CHOOSE
//End If
Return 0
end event

event open;is_Title = This.Title

iu_grpmaintenance = Create u_grpmaintenance
IF NOT iu_grpmaintenance.uf_initialize(lv_1, tv_1, This, st_scroll) Then
	MessageBox("Open event", "uf_initialize Failed")
End If
end event

event ue_save;
Return True

end event

event close;//Destroy iu_grpmaintenance
end event

event resize;iu_grpmaintenance.uf_resize(This)
end event

event ue_delete;iu_grpmaintenance.uf_delete()
end event

event ue_new;iu_grpmaintenance.uf_new()
end event

type st_scroll from u_st_scroll within w_groupmaintenance
integer x = 1193
integer height = 1328
end type

type tv_1 from u_prodgrptree within w_groupmaintenance
integer width = 1193
integer height = 1324
boolean bringtotop = true
boolean linesatroot = true
string picturename[] = {"Custom039!","Custom050!","line.ico","Custom051!","Op.ico","Pas.ico","sma.ico","Custom066!"}
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

type lv_1 from u_prodgrplist within w_groupmaintenance
integer x = 1207
integer width = 2597
integer height = 1324
integer taborder = 11
boolean bringtotop = true
string largepicturename[] = {"line.ico","box.ico","Search!","Help!","Op.ico","Pas.ico","sma.ico"}
long largepicturemaskcolor = 553648127
string smallpicturename[] = {"line.ico","box.ico","Search!","Help!","Op.ico","Pas.ico","sma.ico"}
long smallpicturemaskcolor = 553648127
end type

