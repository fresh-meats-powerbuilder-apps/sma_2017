HA$PBExportHeader$u_groupdataaccess.sru
forward
global type u_groupdataaccess from nonvisualobject
end type
end forward

global type u_groupdataaccess from nonvisualobject
end type
global u_groupdataaccess u_groupdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_list_groups (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_list_group_items (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update_group (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update_group_items (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_list_groups (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_list_group_items (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update_group (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update_group_items (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_groupdataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_groupdataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

