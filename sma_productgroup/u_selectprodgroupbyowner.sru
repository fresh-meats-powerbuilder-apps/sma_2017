HA$PBExportHeader$u_selectprodgroupbyowner.sru
forward
global type u_selectprodgroupbyowner from UserObject
end type
type st_2 from statictext within u_selectprodgroupbyowner
end type
type st_1 from statictext within u_selectprodgroupbyowner
end type
type dw_owner from datawindow within u_selectprodgroupbyowner
end type
type dw_prod_groups from datawindow within u_selectprodgroupbyowner
end type
end forward

global type u_selectprodgroupbyowner from UserObject
int Width=1024
int Height=492
boolean Border=true
long BackColor=12632256
long PictureMaskColor=536870912
long TabTextColor=33554432
long TabBackColor=67108864
st_2 st_2
st_1 st_1
dw_owner dw_owner
dw_prod_groups dw_prod_groups
end type
global u_selectprodgroupbyowner u_selectprodgroupbyowner

type variables
Private:
Integer		ii_max_selected, &
		ii_selected
u_olecom		iu_olecom

String		is_owner

u_dwselect	iu_dwselect
end variables

forward prototypes
private subroutine uf_load_groups (string as_owner)
public subroutine uf_enable (boolean ab_on_off)
public function string uf_get_owner ()
public subroutine uf_set_owner (string as_owner)
public function boolean uf_initialize (integer ar_max_selected, string as_user_id, string as_userpw)
public function integer uf_get_sel_desc (ref string as_list)
public function integer uf_get_sel_id (ref string as_list)
public function integer uf_get_sel_iddesc (ref string as_list)
public subroutine uf_set_default_by_id (string as_owner, string as_input)
public subroutine uf_set_default_by_desc (string as_owner, string as_input)
public subroutine uf_set_default_by_iddesc (string as_owner, string as_input)
end prototypes

private subroutine uf_load_groups (string as_owner);//u_pfm001		lu_pfm001
//Integer		li_rtn, li_pagenumber
//s_error		lstr_error
//String		ls_output
Long	ll_count, ll_item, ll_row

SetPointer(HourGlass!)
//iw_frame.SetMicroHelp("Wait.. Inquiring Database")
//
dw_prod_groups.SetRedraw(False)
dw_prod_groups.Reset()
//ii_selected = 0
//
//lstr_error.se_event_name = "uf_load_groups"
//lstr_error.se_procedure_name = "pfmmf03ar_load_prod_groups"
//lstr_error.se_message = Space(71)
//
//lu_pfm001 = Create u_pfm001
//
//li_rtn = lu_pfm001.nf_pfmf03ar(lstr_error, as_owner, ls_output)
//
//Choose Case li_rtn
//	Case 0
//		dw_prod_groups.ImportString(ls_output)
//	Case Else
////		MessageBox("error return code from pfmf03ar",li_rtn)
//End Choose
dw_prod_groups.SetRedraw(True)
SetPointer(Arrow!)
//// //
iu_olecom.filter(as_owner)
ll_count = iu_olecom.count
For ll_item = 1 to ll_count
	ll_row = dw_prod_groups.InsertRow(0)
	dw_prod_groups.Object.prod_group[ll_row] = iu_olecom.item(ll_item).Description
	dw_prod_groups.Object.prod_group_id[ll_row] = iu_olecom.item(ll_item).Groupid
Next

end subroutine

public subroutine uf_enable (boolean ab_on_off);// enable and disable for objects
If ab_on_off Then 
	dw_owner.Enabled = True
	dw_prod_groups.Enabled = True
	dw_owner.Object.owner.Background.Color = 16777215
	dw_prod_groups.Object.prod_group.Background.Color = 16777215
Else
	dw_owner.Enabled = False
	dw_prod_groups.Enabled = False
	dw_owner.Object.owner.Background.Color = 12632256
	dw_prod_groups.Object.prod_group.Background.Color = 12632256
End If
end subroutine

public function string uf_get_owner ();String		ls_return

ls_return = dw_owner.Object.owner[1]

Return ls_return

end function

public subroutine uf_set_owner (string as_owner);If IsNull(as_owner) OR len(trim(as_owner)) <= 0 Then
	as_owner = is_owner
End If

If dw_owner.RowCount() > 0 Then
	dw_owner.Object.owner[1] = as_owner
Else
	dw_owner.InsertRow(0)
	dw_owner.Object.owner[1] = as_owner
End If
uf_load_groups(as_owner)
end subroutine

public function boolean uf_initialize (integer ar_max_selected, string as_user_id, string as_userpw);String		ls_userini, ls_workingdir, ls_server, ls_filename
//u_sdkcalls	lu_sdkcalls


////
//lu_sdkcalls = Create u_sdkcalls
//lu_sdkcalls.nf_GetUserIniPath(ls_userini, ls_workingdir)
//ls_filename = ls_workingdir + "ibp002.ini"
ls_filename = "ibp002.ini"
ls_server = ProfileString (ls_filename, "Netwise Server Info", "ServerPrefix", "dkmvs00.")
ls_server += "pfm001"
ls_server +=ProfileString (ls_filename, "Netwise Server Info", "ServerSuffix", "t")
////

iu_olecom  = Create u_olecom
iu_olecom.ConnectToNewObject("ProductGroupMaintenance.ProductGroupList.1")
iu_olecom.Initialize(as_user_id, as_userpw, ls_server)
iu_olecom.Retrieve()

uf_load_groups(dw_owner.Object.owner[1])

ii_max_selected = ar_max_selected

If ar_max_selected = ii_max_selected Then
	Return True
Else
	Return False
End If
// // //
end function

public function integer uf_get_sel_desc (ref string as_list);Integer		li_count, li_row


as_list = ''
li_count = 0

li_row = dw_prod_groups.GetSelectedRow(0)


Do While li_row > 0
	li_count ++
	as_list += String(dw_prod_groups.Object.prod_group[li_row]) + "~r~n"
	li_row = dw_prod_groups.GetSelectedRow(li_row)
Loop

Return li_count
end function

public function integer uf_get_sel_id (ref string as_list);Integer		li_count, li_row


as_list = ''
li_count = 0

li_row = dw_prod_groups.GetSelectedRow(0)


Do While li_row > 0
	li_count ++
	as_list += String(dw_prod_groups.Object.prod_group_id[li_row]) + "~r~n"
	li_row = dw_prod_groups.GetSelectedRow(li_row)
Loop

Return li_count
end function

public function integer uf_get_sel_iddesc (ref string as_list);Integer		li_count, li_row


as_list = ''
li_count = 0

li_row = dw_prod_groups.GetSelectedRow(0)

Do While li_row > 0
	li_count ++
	as_list += String(dw_prod_groups.Object.prod_group_id[li_row]) + "~t"
	as_list += String(dw_prod_groups.Object.prod_group[li_row]) + "~r~n" 
	li_row = dw_prod_groups.GetSelectedRow(li_row)
Loop

Return li_count
end function

public subroutine uf_set_default_by_id (string as_owner, string as_input);Boolean					lb_first_time
Long						ll_group, ll_row, ll_count
u_string_functions	lu_strfun
String					ls_find

uf_set_owner(as_owner)

dw_prod_groups.SelectRow(0, FALSE)
ll_count = dw_prod_groups.RowCount()

lb_first_time = True
Do
	ll_group = Long(lu_strfun.nf_gettoken(as_input, "~r~n"))
	
		
	If Not IsNull(ll_group) Or ll_group <= 0 Then
		
		ls_find = "prod_group_id = " + String(ll_group)
		ll_row = dw_prod_groups.Find(ls_find, 1, ll_count)
		If ll_row > 0 Then 
			dw_prod_groups.SelectRow(ll_row, True)
			dw_prod_groups.SetRow(ll_row)
		End If
	ElseIf lb_first_time Then
		dw_prod_groups.SelectRow(1, True)
		dw_prod_groups.SetRow(1)
	End If
	lb_first_time = False	
Loop While len(as_input) > 0

end subroutine

public subroutine uf_set_default_by_desc (string as_owner, string as_input);Long						ll_group, ll_row, ll_count
u_string_functions	lu_strfun
String					ls_find, ls_row

uf_set_owner(as_owner)

dw_prod_groups.SelectRow(0, FALSE)
ll_count = dw_prod_groups.RowCount()

Do
	ls_row = lu_strfun.nf_gettoken(as_input, "~r~n")
	
	If lu_strfun.nf_isempty(ls_row) Then
		ls_find = "prod_group = '" + ls_row + "'"
		ll_row = dw_prod_groups.Find(ls_find, 1, ll_count)
		If ll_row > 0 Then dw_prod_groups.SelectRow(ll_row, True)
	End If
	
Loop While len(as_input) > 0
end subroutine

public subroutine uf_set_default_by_iddesc (string as_owner, string as_input);Long						ll_group, ll_row, ll_count
u_string_functions	lu_strfun
String					ls_find, ls_row

uf_set_owner(as_owner)

dw_prod_groups.SelectRow(0, FALSE)
ll_count = dw_prod_groups.RowCount()

Do
	ls_row = lu_strfun.nf_gettoken(as_input, "~r~n")
	ll_group = Long(lu_strfun.nf_gettoken(ls_row, "~t"))
	
	If Not IsNull(ll_group) Or ll_group <= 0 Then
		
		ls_find = "prod_group_id = " + String(ll_group)
		ll_row = dw_prod_groups.Find(ls_find, 1, ll_count)
		If ll_row > 0 Then dw_prod_groups.SelectRow(ll_row, True)
		
	End If
	
Loop While len(as_input) > 0

end subroutine

on u_selectprodgroupbyowner.create
this.st_2=create st_2
this.st_1=create st_1
this.dw_owner=create dw_owner
this.dw_prod_groups=create dw_prod_groups
this.Control[]={this.st_2,&
this.st_1,&
this.dw_owner,&
this.dw_prod_groups}
end on

on u_selectprodgroupbyowner.destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_owner)
destroy(this.dw_prod_groups)
end on

type st_2 from statictext within u_selectprodgroupbyowner
int X=411
int Y=4
int Width=174
int Height=60
boolean Enabled=false
string Text="System:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within u_selectprodgroupbyowner
int Y=44
int Width=379
int Height=56
boolean Enabled=false
string Text="Product Groups"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_owner from datawindow within u_selectprodgroupbyowner
int X=585
int Width=430
int Height=92
int TabOrder=10
string DataObject="d_selectowner"
BorderStyle BorderStyle=StyleLowered!
boolean LiveScroll=true
end type

event constructor;DataWindowChild		ldwc_temp
Integer					li_return
String					ls_app_name

This.InsertRow(0)
This.GetChild ( "owner",  ldwc_temp )

li_return = ldwc_temp.SetTransObject ( SQLCA )

If li_return = 1 Then
	li_return = ldwc_temp.Retrieve()
Else
	If isNull(li_return) Then
		MessageBox("SetTransObject error","Return isNull")
	Else
		MessageBox("SetTransObject error",li_return)
	End If
End If

ls_app_name = GetApplication().AppName
Choose Case ls_app_name
		Case "pas"
			is_owner = "SCHED"
		Case "orp"
			is_owner = "SALES"
		Case "sma"
			is_owner = "SMACC"
		Case Else
			is_owner = "ERROR"
	End Choose
This.Object.owner[1] = is_owner

//uf_load_groups(ls_app_name)
// //
end event

event itemchanged;
uf_load_groups(data)
// //
end event

type dw_prod_groups from datawindow within u_selectprodgroupbyowner
event ue_keydown pbm_dwnkey
int X=5
int Y=100
int Width=1024
int Height=380
int TabOrder=20
string DataObject="d_selectprodgroup"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event ue_keydown;DataWindow				ldw_datawindow
Long						ll_row
Integer					li_selection


ll_row = This.GetRow()

IF ii_max_selected = 1 Then
	li_selection = 1
Else
	li_selection = 3
End If

Choose Case key
	Case keydownarrow!
		ll_row ++
		If ll_row > This.RowCount() Then Return
		ldw_datawindow = This
		iu_DwSelect.uf_initialize(ldw_datawindow, li_selection)
		iu_dwSelect.uf_select(ll_row, ii_max_selected)
	Case keyuparrow!
		ll_row --
		If ll_row <= 0 Then Return
		ldw_datawindow = This
		iu_DwSelect.uf_initialize(ldw_datawindow, li_selection)
		iu_dwSelect.uf_select(ll_row, ii_max_selected)
End Choose

end event

event clicked;DataWindow				ldw_datawindow
Integer					li_selection
long						ll_row


ldw_datawindow = This

IF row = 0 Then Return

IF ii_max_selected = 1 Then
	li_selection = 1
Else
	li_selection = 3
End If

iu_DwSelect.uf_initialize(ldw_datawindow, li_selection)
iu_dwSelect.uf_select(row, ii_max_selected)



//	If ii_max_selected = 1  And ii_selected = 1 Then
//		iu_DwSelect.uf_initialize(ldw_datawindow, 1)
//		iu_dwSelect.uf_select(row)
////		This.SelectRow(0,False)
////		This.SelectRow(row,True)
//	End If
//End If


end event

