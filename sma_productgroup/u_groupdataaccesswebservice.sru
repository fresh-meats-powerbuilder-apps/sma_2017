HA$PBExportHeader$u_groupdataaccesswebservice.sru
forward
global type u_groupdataaccesswebservice from u_groupdataaccess
end type
end forward

global type u_groupdataaccesswebservice from u_groupdataaccess
end type
global u_groupdataaccesswebservice u_groupdataaccesswebservice

type variables
u_ErrorContext				iu_ErrorContext
end variables

forward prototypes
public function boolean uf_list_groups (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_list_group_items (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update_group (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update_group_items (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_list_groups (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

Integer							li_version_number

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

li_version_number = 1

lu_smas01sr.uf_smas20er_list_groups(ls_appname, &
													ls_windowname, &
													"uf_fwh_assign_maint_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext, &
													li_version_number)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_list_group_items (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas22er_list_group_items(ls_appname, &
													ls_windowname, &
													"uf_fwh_assign_maint_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_update_group (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

Integer							li_version_number

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

li_version_number = 1

lu_smas01sr.uf_smas21er_update_group(ls_appname, &
													ls_windowname, &
													"uf_update_group", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext, &
													li_version_number)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_update_group_items (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas23er_update_group_items(ls_appname, &
													ls_windowname, &
													"uf_update_group_items", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

on u_groupdataaccesswebservice.create
call super::create
end on

on u_groupdataaccesswebservice.destroy
call super::destroy
end on

