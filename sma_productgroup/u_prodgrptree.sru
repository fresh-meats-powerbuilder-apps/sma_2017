HA$PBExportHeader$u_prodgrptree.sru
forward
global type u_prodgrptree from treeview
end type
end forward

global type u_prodgrptree from treeview
integer width = 914
integer height = 508
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
grsorttype sorttype = ascending!
string picturename[] = {"Custom039!"}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
event ue_unpopulate ( long handle )
event ue_postconstructor ( )
event ue_copygrp ( )
event ue_deletegrp ( )
event ue_modifygrp ( )
event ue_newgrp ( )
end type
global u_prodgrptree u_prodgrptree

type variables

u_ProdGrpList		ilv_list
u_grpmaintenance		iu_grpmainttv

Long	il_RightClickedHandle

end variables

forward prototypes
public function boolean uf_initialize (u_prodgrplist alv_listview, u_grpmaintenance au_grpmaint)
end prototypes

event ue_unpopulate;Long			ll_ltvi_handle


Do
	ll_ltvi_handle = This.FindItem(ChildTreeItem! , handle)
	If ll_ltvi_handle > 0 Then
		This.DeleteItem (ll_ltvi_handle)
	End If
Loop Until ll_ltvi_handle <= 0
//
end event

event ue_copygrp();treeviewitem	ltvi_root

String			ls_return, &
					ls_ownergrp, &
					ls_description, &
					ls_grouptype, &
					ls_groupcomment

Integer			li_prodsetid, &
					li_index
					
long				ll_handle, &
					ll_parenthandle

TreeViewItem	ltvi_item

u_TreeViewFunctions		lu_TreeFunctions


ll_handle = il_RightClickedHandle
This.GetItem(ll_handle, ltvi_item)

If ll_handle <= 0 Then 
	iu_grpmainttv.uf_SetMicrohelp('No group selected')
	Return
End If

This.GetItem(ll_handle, ltvi_item)
ls_description = ltvi_item.Label

ls_grouptype = iu_grpmainttv.uf_getgrouptype()

ls_groupcomment = iu_grpmainttv.uf_getgroupcomment(long(ltvi_item.Data))

SetPointer(hourglass!)
OpenWithParm(w_groupmaintcpy, ls_description + '~t' + ls_grouptype + '~t')
ls_return = Message.StringParm

ll_parenthandle = iu_grpmainttv.uf_GetOwnerHandle()
If Not lu_TreeFunctions.uf_checkfordupdescriptions(ls_return, ll_parenthandle, This) Then 
	MessageBox("Error","You can not have duplicate Descriptions.")
	Return
End If

If Len(Trim(ls_return)) > 0 Then 
	li_index = long(ltvi_item.data)
	iu_grpmainttv.uf_copygroup(ls_return, li_index, ls_groupcomment)
End If


end event

event ue_deletegrp();Long				ll_rc, &
					ll_handle
TreeViewItem	ltvi_tree
Boolean			lb_rtn


ll_handle = il_RightClickedHandle
If ll_handle <= 0 Then 
	MessageBox('Delete', 'No Group Selected.  Please Select a group to delete.')
	Return
End if

IF MessageBox("Delete Selected Item", &
		"Are you sure you want to delete the selected item?", &
		Question!, YesNo!, 2) = 1 THEN 
	
	SetPointer(hourglass!)
	This.GetItem(ll_handle, ltvi_Tree)
	lb_rtn = iu_grpmainttv.uf_DeleteGroup(Long(ltvi_tree.Data))
END IF

end event

event ue_modifygrp();TreeViewItem	ltvi_item

String			ls_rtn, ls_grouptype

long				ll_index, &
					ll_handle

ll_handle = il_RightClickedHandle
//ll_handle = This.FindItem(CurrentTreeItem!, 0)
This.GetItem(ll_handle, ltvi_item)

ll_index = long(ltvi_item.Data)

iu_grpmainttv.uf_setnewgroupind(False)
iu_grpmainttv.uf_setgroupind(ll_index)

ls_grouptype = iu_grpmainttv.uf_getgrouptype()


If ls_grouptype = 'P' Then
	OpenWithParm(w_groupmaintproduct, iu_grpmainttv)
Else
	If ls_grouptype = 'C' Then
		OpenWithParm(w_groupmaintcustomer, iu_grpmainttv)
	Else
		OpenWithParm(w_groupmaintlocation, iu_grpmainttv)
	End If
End If

ls_rtn = Message.StringParm
If ls_rtn = "OK" Then 
	SetPointer(HourGlass!)
	iu_grpmainttv.uf_inquire()
End If

end event

event ue_newgrp();String			ls_rtn, ls_grouptype

Long				ll_index, &
					ll_handle

TreeViewItem	ltvi_item


ll_handle = This.FindItem(CurrentTreeItem!, 0)

If ll_handle < 1 Then Return

This.GetItem(ll_handle, ltvi_item)
//
IF ltvi_item.level = 1 THEN
	ll_handle = This.FindItem(ChildTreeItem!, ll_handle)
	If ll_handle < 1 Then Return
	This.GetItem(ll_handle, ltvi_item)
END IF
//

ll_index = ltvi_item.Data

iu_grpmainttv.uf_setgroupind(ll_index)
iu_grpmainttv.uf_setnewgroupind(True)

ls_grouptype = iu_grpmainttv.uf_getgrouptype()

If ls_grouptype = 'P' Then
	OpenWithParm(w_groupmaintproduct, iu_grpmainttv)
Else
	If ls_grouptype = 'C' Then
		OpenWithParm(w_groupmaintcustomer, iu_grpmainttv)
	Else
		OpenWithParm(w_groupmaintlocation, iu_grpmainttv)
	End If
End If

ls_rtn = Message.StringParm

If ls_rtn = "OK" Then 
	SetPointer(HourGlass!)
	iu_grpmainttv.uf_inquire()
End If

end event

public function boolean uf_initialize (u_prodgrplist alv_listview, u_grpmaintenance au_grpmaint);ilv_list = alv_listview
iu_grpmainttv = au_grpmaint

Return True

end function

event constructor;PostEvent("ue_postconstructor")
end event

event itempopulate;treeviewitem	ltvi_parent


This.GetItem(handle,ltvi_parent)

CHOOSE CASE ltvi_parent.Level		
	CASE 1			//  Show product groups for an owner
		iu_grpmainttv.uf_tl_populate_grps(String(ltvi_parent.Data),handle)
	CASE 2			//	Show product list of products for a group
		//
END CHOOSE
end event

event selectionchanged;iu_grpmainttv.uf_tl_selectionchange(newhandle)

end event

event selectionchanging;//iu_grpmainttv.uf_tl_selectionchange(newhandle)

end event

event rightclicked;Boolean				lb_disable, &
						lb_owner

m_pfm_TreeView		lm_popmenu
Long					ll_spotx, &
						ll_spoty, &
						ll_index, &
						ll_handle, &
						ll_ownerhandle, &
						ll_ParentHandle
String					ls_parentowner, ls_updateowner						
TreeViewItem		ltvi_item, ltvi_parent

il_RightClickedHandle = handle

ll_parenthandle = This.FindItem(ParentTreeItem!, handle)
This.GetItem(ll_parenthandle,ltvi_parent)
ls_parentowner = righttrim(string(ltvi_parent.Label))
ls_updateowner = iu_grpmainttv.is_updateowner

// If the parent tree handle isn't the application running then, all menu items exept for copy are
// disabled.
lb_disable = Not (ls_parentowner = ls_updateowner)
lb_owner = Not (ls_parentowner = ls_updateowner)

lm_popmenu = CREATE m_pfm_treeView
lm_popmenu.mf_initialize(lb_disable, lb_owner)

iu_grpmainttv.uf_pointerxy(ll_spotx, ll_spoty)
lm_popmenu.m_popup.Popmenu(ll_spotx, ll_spoty)


end event

on u_prodgrptree.create
end on

on u_prodgrptree.destroy
end on

