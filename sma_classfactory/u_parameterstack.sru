HA$PBExportHeader$u_parameterstack.sru
forward
global type u_parameterstack from u_abstractparameterstack
end type
end forward

global type u_parameterstack from u_abstractparameterstack
end type
global u_parameterstack u_parameterstack

type variables
private:
  Any	ia_nodes[]
  String	is_classes[]
  Long	il_depth

end variables

forward prototypes
public function boolean uf_initialize ()
public function boolean uf_pop (string as_theclassname, ref integer ai_theparm)
public function boolean uf_pop (string as_theclassname, ref boolean ab_theparm)
public function boolean uf_push (string as_theclassname, boolean ab_theparm)
public function boolean uf_pop (string as_theclassname, ref date ad_theparm)
public function boolean uf_pop (string as_theclassname, ref datetime adt_theparm)
public function boolean uf_pop (string as_theclassname, ref decimal adc_theparm)
public function boolean uf_pop (string as_theclassname, ref long al_theparm)
public function boolean uf_pop (string as_theclassname, ref string as_theparm)
public function boolean uf_push (string as_theclassname, string as_theparm)
public function boolean uf_push (string as_theclassname, long al_theparm)
public function boolean uf_push (string as_theclassname, integer ai_theparm)
public function boolean uf_push (string as_theclassname, decimal adc_theparm)
public function boolean uf_push (string as_theclassname, datetime adt_theparm)
public function boolean uf_push (string as_theclassname, date ad_theparm)
public function boolean uf_push (string as_theclassname, real ar_theparm)
public function boolean uf_pop (string as_theclassname, ref real ar_theparm)
public function boolean uf_pop (string as_theclassname, ref powerobject ao_theparm)
public function boolean uf_push (string as_theclassname, powerobject ao_theparm)
end prototypes

public function boolean uf_initialize ();il_depth = 0
return true
end function

public function boolean uf_pop (string as_theclassname, ref integer ai_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "integer" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
ai_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_pop (string as_theclassname, ref boolean ab_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "boolean" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
ab_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_push (string as_theclassname, boolean ab_theparm);il_depth++
ia_nodes[il_depth] = ab_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_pop (string as_theclassname, ref date ad_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "date" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
ad_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_pop (string as_theclassname, ref datetime adt_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "datetime" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
adt_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_pop (string as_theclassname, ref decimal adc_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "decimal" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
adc_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_pop (string as_theclassname, ref long al_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "long" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
al_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_pop (string as_theclassname, ref string as_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "string" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
as_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_push (string as_theclassname, string as_theparm);il_depth++
ia_nodes[il_depth] = as_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_push (string as_theclassname, long al_theparm);il_depth++
ia_nodes[il_depth] = al_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_push (string as_theclassname, integer ai_theparm);il_depth++
ia_nodes[il_depth] = ai_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_push (string as_theclassname, decimal adc_theparm);il_depth++
ia_nodes[il_depth] = adc_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_push (string as_theclassname, datetime adt_theparm);il_depth++
ia_nodes[il_depth] = adt_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_push (string as_theclassname, date ad_theparm);il_depth++
ia_nodes[il_depth] = ad_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_push (string as_theclassname, real ar_theparm);il_depth++
ia_nodes[il_depth] = ar_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

public function boolean uf_pop (string as_theclassname, ref real ar_theparm);If il_depth <= 0 Then Return False
If ClassName(ia_nodes[il_depth]) <> "real" Then Return False
If is_classes[il_depth] <> as_theClassName Then Return False
ar_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_pop (string as_theclassname, ref powerobject ao_theparm);If il_depth <= 0 Then Return False
//If Not IsValid(ia_nodes[il_depth]) Then Return False //Causes a Gen Error on compile ** PB 8.0 ** IBDKEEM **
If is_classes[il_depth] <> as_theClassName Then Return False
ao_theParm = ia_nodes[il_depth]
il_depth --
Return True
end function

public function boolean uf_push (string as_theclassname, powerobject ao_theparm);il_depth++
ia_nodes[il_depth] = ao_theParm
is_classes[il_depth] = as_theClassName
Return True
end function

on u_parameterstack.create
call super::create
end on

on u_parameterstack.destroy
call super::destroy
end on

