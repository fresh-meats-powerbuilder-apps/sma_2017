HA$PBExportHeader$u_objectlist.sru
$PBExportComments$Depends on a DataWindow object named 'd_objectlist'.
forward
global type u_objectlist from u_abstractobjectlist
end type
end forward

global type u_objectlist from u_abstractobjectlist
end type
global u_objectlist u_objectlist

type variables
Private:
DataStore	ids_list
end variables

forward prototypes
public function string uf_lookupwindowtype (string as_class)
public function string uf_lookupwhere (string as_class)
public function string uf_lookupduration (string as_class)
public function string uf_lookupclass (string as_class)
public function boolean uf_initialize ()
end prototypes

public function string uf_lookupwindowtype (string as_class);Long		ll_row
String	ls_return = ""

ll_row = ids_list.Find("classname = '" + as_class + "'",1,ids_list.RowCount() + 1)

If ll_row > 0 Then
	ls_return = ids_list.GetItemString(ll_row,"window_type")
End If 

Return ls_return

end function

public function string uf_lookupwhere (string as_class);Long		ll_row
String	ls_return = ""

ll_row = ids_list.Find("classname = '" + as_class + "'",1,ids_list.RowCount() + 1)

If ll_row > 0 Then
	ls_return = ids_list.GetItemString(ll_row,"where")
End If 

Return ls_return

end function

public function string uf_lookupduration (string as_class);Long		ll_row
String	ls_return = ""

ll_row = ids_list.Find("classname = '" + as_class + "'",1,ids_list.RowCount() + 1)

If ll_row > 0 Then
	ls_return = ids_list.GetItemString(ll_row,"duration")
End If 

Return ls_return

end function

public function string uf_lookupclass (string as_class);Long		ll_row
String	ls_return = ""

ll_row = ids_list.Find("classname = '" + as_class + "'",1,ids_list.RowCount() + 1)

If ll_row > 0 Then
	ls_return = ids_list.GetItemString(ll_row,"class")
End If 

Return ls_return

end function

public function boolean uf_initialize ();ids_list = Create DataStore
ids_list.DataObject = 'd_objectlist'
Return True

end function

on u_objectlist.create
call super::create
end on

on u_objectlist.destroy
call super::destroy
end on

