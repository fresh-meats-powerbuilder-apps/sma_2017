HA$PBExportHeader$u_smas01sr.sru
forward
global type u_smas01sr from u_web_service
end type
end forward

global type u_smas01sr from u_web_service
end type
global u_smas01sr u_smas01sr

type variables
// Declare soap connection variable
soapconnection ispcn_conn

// Declare input and output program interface variables
smas01sr_programinterface		ipgif_output
smas01sr_programinterface1	ipgif_input

//Declare container interface variables
smas01sr_programinterfacesmas01ci1	ictif_cics_container_in
smas01sr_programinterfacesmas01in1	ictif_input_container_in
smas01sr_programinterfacesmas01ot	ictif_output_container_out
smas01sr_programinterfacesmas01pg1	ictif_program_container_in
smas01sr_programinterfacesmas01pg	ictif_program_container_out

//Declare container variables
smas01sr_programinterfacesmas01cisma000sr_cics_container1 ict_sma000sr_cics_container
smas01sr_programinterfacesmas01pgsma000sr_program_container1 ict_sma000sr_program_container_in
smas01sr_programinterfacesmas01pgsma000sr_program_container ict_sma000sr_program_container_out

//Web Service path
String		is_web_service_address, is_utlu07er_userid, is_utlu07er_password

u_abstractdefaultmanager	iu_defaultmanager
u_grpmaintenance		iu_grpmaint

end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas02er_get_transfer_customers (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas00er_gettranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas01er_updatetranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_headerstring, string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas10er_fwh_sales_trans_freight_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas16er_fwh_div_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas11er_fwh_sales_trans_feight_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas08er_fwh_sales_trans_divg_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas09er_fwh_sales_trans_divg_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas13er_fwh_sku_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas12er_fwh_sku_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas15er_fwh_div_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas14er_fwh_div_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas18er_fwh_assign_maint_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas19er_fwh_assign_maint_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas03er_updaterebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas05er_getrebatelist (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas07er_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas17er_fwh_location_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas24er_productgroupcheck (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas25er_inq_appliedrebates (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas27er_upd_prod_cat_maint (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas30er_inq_prod_cat_maint (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas28er_inq_cat_by_product (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_assignstring, ref string as_unassignstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas29er_upd_cat_by_product (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas26er_upd_appliedrebates (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas20er_list_groups (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas22er_list_group_items (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas23er_update_group_items (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas21er_update_group (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_inputstring, ref u_abstracterrorcontext au_errorcontext)
public function integer uf_utlu07er_getsecurity (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_procedurename, string as_userid, string as_password, string as_returncode, string as_message, string as_useridpassword, ref string as_actionindicator, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_get_utlu07er_parms ()
public function boolean uf_smas04er_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_headerstring, ref string as_detailstring, ref string as_customergroupstring, ref string as_customerstring, ref string as_divisionstring, ref string as_productstring, ref string as_productgroupstring, ref string as_locationstring, ref string as_rateseqstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas31er_customergroupcheck (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas20er_list_groups (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext, integer ai_version_number)
public function boolean uf_smas21er_update_group (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_inputstring, ref u_abstracterrorcontext au_errorcontext, integer ai_version_number)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext);Boolean		lb_initialized

// Get web service address from IBP002.ini file

lb_initialized = au_classfactory.uf_GetObject("u_inidefaultmanager", iu_defaultmanager, au_errorcontext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	Return False
End If

// We initialize it no matter what
If Not iu_defaultmanager.uf_Initialize(au_classfactory, "IBP002.INI", "Web Service URL", au_errorcontext) Then
	// Error initializing default manager
	// Populate error context and return False
	au_errorcontext.uf_AppendText("Can't initialize default manager for web service info")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If

If Not iu_defaultmanager.uf_GetData("SMAS01SRpath", is_web_service_address, au_errorcontext) Then
	// Error getting server prefix
	// Populate error context and return false
	au_errorcontext.uf_AppendText("Unable to obtain web service path")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If

//Instantiate soapconnection
ispcn_conn = create soapconnection

//Instantiate input and output program interfaces
ipgif_output = create smas01sr_programinterface
ipgif_input = create smas01sr_programinterface1

//Instantiate input and output container interfaces
ictif_cics_container_in = create smas01sr_programinterfacesmas01ci1
ipgif_input.smas01ci = ictif_cics_container_in

ictif_input_container_in = create smas01sr_programinterfacesmas01in1
ipgif_input.smas01in = ictif_input_container_in

ictif_output_container_out = create smas01sr_programinterfacesmas01ot
ipgif_output.smas01ot = ictif_output_container_out

ictif_program_container_in = create smas01sr_programinterfacesmas01pg1
ipgif_input.smas01pg = ictif_program_container_in

ictif_program_container_out = create smas01sr_programinterfacesmas01pg
ipgif_output.smas01pg = ictif_program_container_out

//Instantiate container variables
ict_sma000sr_cics_container = create smas01sr_programinterfacesmas01cisma000sr_cics_container1
ictif_cics_container_in.sma000sr_cics_container = ict_sma000sr_cics_container

ict_sma000sr_program_container_in  = create smas01sr_programinterfacesmas01pgsma000sr_program_container1
ictif_program_container_in.sma000sr_program_container = ict_sma000sr_program_container_in

ict_sma000sr_program_container_out  = create smas01sr_programinterfacesmas01pgsma000sr_program_container
ictif_program_container_out.sma000sr_program_container = ict_sma000sr_program_container_out

return True
end function

public function boolean uf_smas02er_get_transfer_customers (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner, ls_temp, ls_target_address


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS02ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S02E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_outputstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_outputstring = ''
		else	
			as_outputstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas00er_gettranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message,  ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS00ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S00E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_outputstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_outputstring = ''
		else	
			as_outputstring +=  trim(ipgif_output.smas01ot.value)
		end if
			
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas01er_updatetranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_headerstring, string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS01ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S01E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_headerstring + as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0


ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	Return False
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value 
end if	

Return True
end function

public function boolean uf_smas10er_fwh_sales_trans_freight_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS10ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S10E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas16er_fwh_div_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS16ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S16E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = ''

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas11er_fwh_sales_trans_feight_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS11ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S11E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas08er_fwh_sales_trans_divg_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS08ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S08E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if
Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas09er_fwh_sales_trans_divg_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS09ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S09E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = trim(as_headerstring) + '~t' + as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0


ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas13er_fwh_sku_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS13ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S13E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

// send dummy value so the get input container doesn't fail in RPC
ipgif_input.smas01in.value = "DUMMY"

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas12er_fwh_sku_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS12ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S12E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas15er_fwh_div_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS15ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S15E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas14er_fwh_div_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS14ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S14E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas18er_fwh_assign_maint_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS18ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S18E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas19er_fwh_assign_maint_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS19ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S19E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas03er_updaterebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS03ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S03E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

//pad out header stringt to 250 characters
as_headerstring = as_headerstring + space( 250 - len(as_headerstring))

ipgif_input.smas01in.value = as_headerstring + as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0


ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
//as_headerstring = ''	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	Return False
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	if isnull(ipgif_output.smas01ot) then
		//as_headerstring = ''
	else	
		as_headerstring =  trim(ipgif_output.smas01ot.value)
	end if 
end if	

Return True
end function

public function boolean uf_smas05er_getrebatelist (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS05ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S05E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = ""

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_outputstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_outputstring = ''
		else	
			as_outputstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas07er_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS07ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S07E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas17er_fwh_location_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS17ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S17E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas24er_productgroupcheck (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS24ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S24E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas25er_inq_appliedrebates (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS25ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S25E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_outputstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_outputstring = ''
		else	
			as_outputstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas27er_upd_prod_cat_maint (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS27ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S27E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas30er_inq_prod_cat_maint (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS30ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S30E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_outputstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_outputstring = ''
		else	
			as_outputstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas28er_inq_cat_by_product (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref string as_assignstring, ref string as_unassignstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner, ls_outputstring, ls_stringind, ls_output, ls_stringsep 

u_String_Functions		lu_string

smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS28ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S28E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
//as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		ls_outputstring = trim(ipgif_output.smas01ot.value)
		
		ls_stringsep = Left(ls_OutputString, 1)
		ls_StringInd = Mid(ls_OutputString, 2, 1)
		ls_outputstring = Mid(ls_OutputString, 3)	
		
		Do While Len(Trim(ls_OutputString)) > 0
			ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
			
			Choose Case ls_StringInd
				Case 'A'
					as_assignstring += ls_output
				Case 'U'
					as_unassignstring += ls_output
			End Choose
		
			ls_StringInd = Left(ls_OutputString, 1)
			ls_OutputString = Mid(ls_OutputString, 2)
		Loop
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas29er_upd_cat_by_product (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS29ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S29E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas26er_upd_appliedrebates (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS26ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S26E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	//	as_outputstring +=  ipgif_output.smas01ot.value
end if

Return True
end function

public function boolean uf_smas20er_list_groups (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS20ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S20E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas22er_list_group_items (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS22ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S22E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas23er_update_group_items (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, string as_inputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

Window iw_groupmaintenance

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS23ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S23E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_inputstring = ''


ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
//
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
elseif li_rval = 1 then
	ls_message =  ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_SetReturnCode(li_rval)	
	//li_rval = 0
	MessageBox('Error',ls_message)
else
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
//		if isnull(ipgif_output.smas01ot) then
//			as_detailstring = ''
//		else	
//			as_detailstring +=  trim(ipgif_output.smas01ot.value)
//		end if
end if


Return True
end function

public function boolean uf_smas21er_update_group (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_inputstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS21ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S21E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_inputstring = ''


ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
//
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else

	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	if isnull(ipgif_output.smas01ot) then
		as_inputstring = ''
	else	
		as_inputstring +=  trim(ipgif_output.smas01ot.value)
	end if
end if


Return True
end function

public function integer uf_utlu07er_getsecurity (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_procedurename, string as_userid, string as_password, string as_returncode, string as_message, string as_useridpassword, ref string as_actionindicator, ref u_abstracterrorcontext au_errorcontext);long		ll_ret 
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner

smas01sr_smas01srservice p_obj

if not uf_get_utlu07er_parms() Then
	Return -1
end if

ict_sma000sr_cics_container.sma000sr_req_password = is_utlu07er_password
ict_sma000sr_cics_container.sma000sr_req_userid = is_utlu07er_userid 
ict_sma000sr_cics_container.sma000sr_req_program = 'UTLU07ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'U07E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_useridpassword + '~t' + as_actionindicator + '~t'  

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(is_utlu07er_userid)+"~",Password=~""+string(is_utlu07er_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return 0
end if

ict_sma000sr_program_container_in.sma000sr_last_record_num = 0
ict_sma000sr_program_container_in.sma000sr_max_record_num = 0
ict_sma000sr_program_container_in.sma000sr_task_num = 0

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
			Return 0
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
else	
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	as_actionindicator =  ipgif_output.smas01ot.value
end if

Return li_rval

end function

public function boolean uf_get_utlu07er_parms ();
if pos(is_web_service_address, "cicstst") > 0 then
	is_utlu07er_userid = 'PBWSTST'
	is_utlu07er_password = 'WSTSTVF1'
else
	if pos(is_web_service_address, "cicspar") > 0 then
		is_utlu07er_userid = 'PBWSPAR'
		is_utlu07er_password =  'WSPARVF1'
	else
		if pos(is_web_service_address, "cics00b") > 0 then
			is_utlu07er_userid = 'PBWSPRD'
			is_utlu07er_password =  'WSPRDVF1'
		else
			MessageBox("IBP002.INI file error", "Unable to determine CICS region for web service")
			Return False
		end if
	end if
end if

Return True
end function

public function boolean uf_smas04er_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_headerstring, ref string as_detailstring, ref string as_customergroupstring, ref string as_customerstring, ref string as_divisionstring, ref string as_productstring, ref string as_productgroupstring, ref string as_locationstring, ref string as_rateseqstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval, li_len
string		ls_options, ls_message, ls_outputstring, ls_stringind, ls_output, ls_banner, ls_stringsep
Boolean	lb_FirstTime

u_String_Functions		lu_string

smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS04ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S04E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_headerstring 

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if

lb_FirstTime = True

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
//as_outputstring = ''

Do
	
	If lb_FirstTime Then
		as_headerstring = ''
		lb_FirstTime = False
	End If	
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
		Return False	
	else	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			ls_outputstring = ''
		else	
			ls_outputstring +=  trim(ipgif_output.smas01ot.value)
		end if		
		
		ls_stringsep = Left(ls_OutputString, 1)
		ls_StringInd = Mid(ls_OutputString, 2, 1)
		ls_outputstring = Mid(ls_OutputString, 3)
		
		Do While Len(Trim(ls_OutputString)) > 0
			ls_output = lu_string.nf_GetToken(ls_OutputString, ls_stringsep)
			
			Choose Case ls_StringInd
				Case 'H'
					as_HeaderString += ls_output
				Case 'D'
					as_DetailString += ls_output
				Case 'S'
					as_CustomerGroupString += ls_output
				Case 'C'
					as_CustomerString += ls_output
				Case 'V'
					as_DivisionString += ls_output
				Case 'G'
					as_ProductGroupString += ls_output
				Case 'P'
					as_ProductString += ls_output
				Case 'O'
					as_LocationString += ls_output					
				Case 'L'
					as_RateSeqstring += ls_output
	
			End Choose
			ls_StringInd = Left(ls_OutputString, 1)
			ls_OutputString = Mid(ls_OutputString, 2)
		Loop 	
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas31er_customergroupcheck (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS31ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S31E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = 0

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname) 
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True

end function

public function boolean uf_smas20er_list_groups (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext, integer ai_version_number);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS20ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S20E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = ai_version_number

ipgif_input.smas01in.value = as_detailstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_detailstring = ''

Do
	
	ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
	ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
	ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.smas01sroperation(ipgif_input)
		  catch (SoapException e)
				This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				Return False
	end try
		
	ictif_program_container_out = ipgif_output.smas01pg 
	li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
	if li_rval < 0 then
		ls_banner = trim(ipgif_output.smas01ot.value)
		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
	
		ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rval)		
		ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
		ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
		if isnull(ipgif_output.smas01ot) then
			as_detailstring = ''
		else	
			as_detailstring +=  trim(ipgif_output.smas01ot.value)
		end if
	end if

Loop while ll_last_record_num <> ll_max_record_num	

Return True
end function

public function boolean uf_smas21er_update_group (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_password, ref string as_inputstring, ref u_abstracterrorcontext au_errorcontext, integer ai_version_number);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_banner


smas01sr_smas01srservice p_obj

ict_sma000sr_cics_container.sma000sr_req_password = as_password
ict_sma000sr_cics_container.sma000sr_req_userid = as_userid
ict_sma000sr_cics_container.sma000sr_req_program = 'SMAS21ER'
ict_sma000sr_cics_container.sma000sr_req_tranid = 'S21E'

ict_sma000sr_program_container_in.sma000sr_rval = 0
ict_sma000sr_program_container_in.sma000sr_message = space(200)
ict_sma000sr_program_container_in.sma000sr_version_number = ai_version_number

ipgif_input.smas01in.value = as_inputstring

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string(as_userid)+"~",Password=~""+string(as_password)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "smas01sr_smas01srservice", is_web_service_address)
if ll_ret <> 0 then
	  MessageBox("Error", "Cannot create instance of proxy")
	  au_ErrorContext.uf_AppendText(ls_message)
	  au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
	//  return
end if


ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0
as_inputstring = ''


ict_sma000sr_program_container_in.sma000sr_last_record_num = ll_last_record_num
ict_sma000sr_program_container_in.sma000sr_max_record_num = ll_max_record_num
ict_sma000sr_program_container_in.sma000sr_task_num = ll_task_number

try
	  ipgif_output = p_obj.smas01sroperation(ipgif_input)
	  catch (SoapException e)
			This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
			Return False
end try
	
ictif_program_container_out = ipgif_output.smas01pg 
li_rval = ictif_program_container_out.sma000sr_program_container.sma000sr_rval
//
if li_rval < 0 then
	ls_banner = trim(ipgif_output.smas01ot.value)
	This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
elseif li_rval = 1 then
	ls_message =  ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_SetReturnCode(li_rval)	
	//li_rval = 0
	MessageBox('Error',ls_message)
else
	ls_message = ictif_program_container_out.sma000sr_program_container.sma000sr_message
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rval)		
	ll_last_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_last_record_num
	ll_max_record_num = ictif_program_container_out.sma000sr_program_container.sma000sr_max_record_num
	ll_task_number = ictif_program_container_out.sma000sr_program_container.sma000sr_task_num
	if isnull(ipgif_output.smas01ot) then
		as_inputstring = ''
	else	
		as_inputstring +=  trim(ipgif_output.smas01ot.value)
	end if
end if


Return True
end function

on u_smas01sr.create
call super::create
end on

on u_smas01sr.destroy
call super::destroy
end on

