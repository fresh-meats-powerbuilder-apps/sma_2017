HA$PBExportHeader$w_abstractframe.srw
forward
global type w_abstractframe from Window
end type
type mdi_1 from mdiclient within w_abstractframe
end type
end forward

global type w_abstractframe from Window
int X=1056
int Y=484
int Width=3031
int Height=1576
boolean Visible=false
boolean TitleBar=true
string Title="Frame"
string MenuName="m_abstractMenu"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
WindowType WindowType=mdihelp!
event ue_open ( )
event ue_close ( )
event ue_settings ( )
event ue_toolbar ( )
event ue_helpindex ( )
event ue_searchhelp ( )
event ue_about ( )
mdi_1 mdi_1
end type
global w_abstractframe w_abstractframe

type variables
u_AbstractClassFactory	iu_ClassFactory
u_AppController	iu_AppController
Boolean		ib_NoPromptOnExit = FALSE
Protected:
u_AbstractErrorContext	iu_ErrorContext
u_AbstractDefaultManager	iu_DefaultManager


end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstractcontroller au_appcontroller, ref u_abstracterrorcontext au_errorcontext)
public subroutine wf_setmostrecent (String as_description)
public subroutine wf_adjusttoolbar ()
end prototypes

event ue_open;u_AbstractParameterStack	lu_Stack
u_AbstractNotificationController lu_notification

iu_ErrorContext.uf_Initialize()

iu_classfactory.uf_GetObject("u_parameterstack", lu_stack, iu_errorcontext)
If Not iu_ErrorContext.uf_IsSuccessful() Then
	If Not iu_ClassFactory.uf_GetObject("u_notificationcontroller", lu_notification) Then
		lu_notification.uf_Initialize(iu_classfactory)
	End If
	If IsValid(lu_Notification) Then
		lu_Notification.uf_Display(iu_ErrorContext)
	End If
	Return
End If

iu_ErrorContext.uf_Initialize()

lu_Stack.uf_Initialize()
lu_Stack.uf_Push("u_ClassFactory", iu_classfactory)

iu_ClassFactory.uf_GetResponseWindow("w_open", lu_stack)

String	ls_parm

lu_stack.uf_Pop("String",ls_parm)

If Len(ls_parm) > 0 Then
	iu_appcontroller.Event Dynamic ue_open(ls_parm)
End If
end event

event ue_settings;u_AbstractParameterStack	lu_Stack
u_AbstractErrorContext		lu_ErrorContext
Window				lw_Frame

lw_Frame = This

iu_classfactory.uf_GetObject("u_parameterstack", lu_stack, iu_errorcontext)
lu_ErrorContext = iu_errorcontext
//TODO, Error checking

lu_Stack.uf_Initialize()


lu_Stack.uf_Push("u_abstractClassFactory",iu_classfactory)
lu_Stack.uf_Push("Window",lw_Frame)
lu_Stack.uf_Push("u_abstractErrorContext",lu_ErrorContext)
iu_ClassFactory.uf_GetResponseWindow("w_settings", lu_stack)
end event

event ue_helpindex;u_help	lu_help

If Not iu_classfactory.uf_GetObject("u_help", lu_help, iu_errorcontext) Then
	If IsValid(lu_help) Then
		If Not lu_help.uf_initialize(This, GetApplication().AppName  + ".HLP") Then
			// An error occurred, populate error context and return
			
		End If
		
		If Not lu_help.uf_contents() Then
			
		End If
	End If
End If


end event

event ue_searchhelp;u_help	lu_help

If Not iu_classfactory.uf_GetObject("u_help", lu_help, iu_errorcontext) Then
	If IsValid(lu_help) Then
		If Not lu_help.uf_initialize(This, Upper(GetApplication().AppName)  + ".HLP") Then
			// An error occurred, populate error context and return
			
		End If
		
		If Not lu_help.uf_keyword("") Then
			
		End If
	End If
End If


end event

event ue_about;u_AbstractParameterStack	lu_Stack

iu_classfactory.uf_GetObject("u_parameterstack", lu_stack, iu_errorcontext)
//TODO, Error checking

lu_Stack.uf_Initialize()
lu_Stack.uf_Push("string", This.Title)

iu_ClassFactory.uf_GetResponseWindow("w_about", lu_stack)


end event

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstractcontroller au_appcontroller, ref u_abstracterrorcontext au_errorcontext);m_abstractMenu	lm_menu

iu_classfactory = au_ClassFactory
iu_appcontroller = au_appcontroller
iu_errorcontext = au_errorcontext


lm_menu = This.MenuID
lm_menu.mf_GetLastUsed()

u_AbstractParameterStack	lu_ParameterStack



IF Not iu_ClassFactory.uf_GetObject("u_IniDefaultManager",iu_defaultmanager,au_ErrorContext) Then
	IF au_ErrorContext.uf_IsSuccessful()Then
		IF iu_DefaultManager.uf_Initialize(iu_classfactory,"ibpuser.ini",&
									GetApplication().appName+" System Settings",au_ErrorContext) Then
									
			wf_adjusttoolbar()
		ELSE
		au_ErrorContext.uf_AppendText("Unable initialize inidefaultManager")
		au_ErrorContext.uf_SetReturnCode(-1)
		lu_ParameterStack.uf_Initialize()
		lu_ParameterStack.uf_Push("u_AbstractErrorContext",au_ErrorContext)
		CloseWithReturn(This,lu_ParameterStack)
		END IF			
	ELSE
		au_ErrorContext.uf_AppendText("Unable to create inidefaultManager")
		au_ErrorContext.uf_SetReturnCode(-2)
		lu_ParameterStack.uf_Initialize()
		lu_ParameterStack.uf_Push("u_AbstractErrorContext",au_ErrorContext)
		CloseWithReturn(This,lu_ParameterStack)
	END IF
ELSE
	// GetObject says the object is allready initialized but dont know with what
	IF iu_DefaultManager.uf_Initialize(iu_classfactory,"ibpuser.ini",&
									GetApplication().appNAme+" System Settings",au_ErrorContext) Then
									
		wf_adjusttoolbar()
	ELSE
		au_ErrorContext.uf_AppendText("Unable initialize inidefaultManager")
		au_ErrorContext.uf_SetReturnCode(-1)
		lu_ParameterStack.uf_Initialize()
		lu_ParameterStack.uf_Push("u_AbstractErrorContext",au_ErrorContext)
		CloseWithReturn(This,lu_ParameterStack)
	END IF			
END IF
return true
end function

public subroutine wf_setmostrecent (String as_description);m_AbstractMenu	lm_Menu

lm_Menu = This.MenuID

lm_Menu.mf_SetLastUsed(as_Description)
end subroutine

public subroutine wf_adjusttoolbar ();
Application	lu_App
ArrangeOpen	lao_ArrangeOpen

Integer	li_Setting

String	ls_setting

u_ErrorContext	lu_ErrorContext

lu_App = GetApplication()





IF Not iu_classfactory.uf_GetObject("u_ErrorContext",lu_ErrorContext) Then
	IF Not lu_ErrorContext.uf_Initialize() Then
		Return
	END IF
END IF


iu_defaultmanager.uf_GetData("arrangeopen",ls_Setting,lu_ErrorContext)
Choose Case ls_Setting
	Case 'original'
		lao_ArrangeOpen = original!
	Case 'layered'
		lao_ArrangeOpen = layered!
	Case 'cascaded'
		lao_ArrangeOpen = cascaded!
	Case ELSE
		lao_ArrangeOpen = original!
End Choose 
iu_classfactory.uf_setarrangeopen(lao_ArrangeOpen)
lu_ErrorContext.uf_Initialize()

iu_defaultmanager.uf_GetData("ToolBarVisible",li_Setting,lu_ErrorContext)
IF lu_ErrorContext.uf_IsSuccessful() Then
	IF li_Setting = 0 then
		This.ToolBarVisible = False
	ELSE
		This.ToolBarVisible = True
	END IF
ELSE
	This.ToolBarVisible = True
END IF
lu_ErrorContext.uf_Initialize()


iu_defaultmanager.uf_GetData("ToolBarText",li_Setting,lu_ErrorContext)
IF li_Setting = 1 then
	lu_App.ToolBarText = TRUE
ELSE
	lu_App.ToolBarText = FALSE
END IF
lu_ErrorContext.uf_Initialize()

iu_defaultmanager.uf_GetData("ToolBarTips",li_Setting,lu_ErrorContext)
IF li_Setting = 1 then
	lu_App.ToolBarTips = True
ELSE
	lu_App.ToolBarTips = FALSE
END IF
lu_ErrorContext.uf_Initialize()

iu_defaultmanager.uf_GetData("ToolBarAlignMent",ls_Setting,lu_ErrorContext)
Choose CASE	ls_Setting
			CASE	'alignatleft'
				This.ToolBarAlignment = AlignAtLeft! 
			CASE	'alignatright'
				This.ToolBarAlignment = AlignAtRight!
			CASE	'alignattop'
				This.ToolBarAlignment = AlignAtTop!
			CASE	'Bottom'
				This.ToolBarAlignment = AlignAtBottom!
			CASE	'Floating'
				This.ToolBarAlignment = Floating!
End  Choose 

end subroutine

on w_abstractframe.create
if this.MenuName = "m_abstractMenu" then this.MenuID = create m_abstractMenu
this.mdi_1=create mdi_1
this.Control[]={this.mdi_1}
end on

on w_abstractframe.destroy
if IsValid(MenuID) then destroy(MenuID)
destroy(this.mdi_1)
end on

event closequery;u_settings	lu_settings
u_AbstractErrorContext lu_ErrorContext
u_AbstractNotificationController lu_notification
String	ls_value

iu_ClassFactory.uf_GetObject("u_ErrorContext", lu_ErrorContext)
lu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject("u_settings", lu_settings, lu_ErrorContext)
lu_Settings.uf_Initialize(iu_ClassFactory, lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then
	iu_ClassFactory.uf_GetObject("u_notificationcontroller", lu_notification)
	lu_notification.uf_Initialize(iu_ClassFactory)
	lu_notification.uf_Display(lu_ErrorContext)
	Return 0
End If

lu_Settings.uf_Get("Prompt on Exit", ls_value, lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then
	ls_value = "1"
	lu_settings.uf_Set("Prompt on Exit", ls_value, lu_ErrorContext)
	
	// It's okey-dokey if the setting's not in the INI file
	//iu_ClassFactory.uf_GetObject("u_notificationcontroller", lu_notification)
	//lu_notification.uf_Initialize(iu_ClassFactory)
	//lu_notification.uf_Display(lu_ErrorContext)
	//Return 0
End If

If ib_NoPromptOnExit Then Return 0

If ls_value <> "1" Then Return 0

If MessageBox(This.Title,"This will close the application.", Exclamation!, OKCancel!) = 2 Then Return 1
	
Return 0
end event

type mdi_1 from mdiclient within w_abstractframe
long BackColor=276856960
end type

