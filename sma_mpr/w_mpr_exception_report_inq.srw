HA$PBExportHeader$w_mpr_exception_report_inq.srw
forward
global type w_mpr_exception_report_inq from w_abstractresponseext
end type
type dw_date_time_inquire from datawindow within w_mpr_exception_report_inq
end type
end forward

global type w_mpr_exception_report_inq from w_abstractresponseext
integer width = 1518
integer height = 540
string title = "MPR Exception Report Inquire"
dw_date_time_inquire dw_date_time_inquire
end type
global w_mpr_exception_report_inq w_mpr_exception_report_inq

type variables
String							is_TransferCustomer, &
									is_title

u_abstracterrorcontext		iu_errorcontext

u_abstractparameterstack	iu_parameterstack

w_smaframe						iw_frame

//DataWindowChild				idwc_ParentTransferCustomer

//u_AbstractTranferCustomersDataAccess	iu_TransferCustomersDataAccess

u_AbstractClassFactory		iu_ClassFactory

u_NotificationController	iu_Notification	

DateTime							idt_begin_datetime, &
									idt_end_datetime

end variables

on w_mpr_exception_report_inq.create
int iCurrent
call super::create
this.dw_date_time_inquire=create dw_date_time_inquire
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_date_time_inquire
end on

on w_mpr_exception_report_inq.destroy
call super::destroy
destroy(this.dw_date_time_inquire)
end on

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

//iu_ParameterStack.uf_Pop('u_TransferCustomersDataAccess', iu_TransferCustomersDataAccess)
iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('window', iw_frame)
//iu_ParameterStack.uf_Pop('datawindowchild', idwc_ParentTransferCustomer)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('datetime', idt_end_datetime)
iu_ParameterStack.uf_Pop('datetime', idt_begin_datetime)
//iu_ParameterStack.uf_Pop('string', is_TransferCustomer)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

This.Title = is_title + ' Inquire'


//
//dw_customer.GetChild('Customer_number', ldwc_TransferCustomer)
//idwc_ParentTransferCustomer.ShareData(ldwc_TransferCustomer)
//
//dw_customer.GetChild('Customer_number_descr', ldwc_TransferCustomer)
//idwc_ParentTransferCustomer.ShareData(ldwc_TransferCustomer)
//
//dw_customer.SetItem(1, 'Customer_number', is_TransferCustomer)
//
//is_TransferCustomer = 'Cancel'
//
//dw_customer.SetFocus()
end event

event close;call super::close;iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
//iu_ParameterStack.uf_Push('string', is_TransferCustomer)


iu_ParameterStack.uf_Push('string', is_title)
//iu_ParameterStack.uf_Push('datawindowchild', ldwc_temp)
iu_ParameterStack.uf_Push('datetime', idt_begin_datetime)
iu_ParameterStack.uf_Push('datetime', idt_end_datetime)

iu_ParameterStack.uf_Push('window', iw_frame)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
//iu_ParameterStack.uf_Push('u_TransferCustomersDataAccess', iu_TransferCustomersDataAccess)

CloseWithReturn(This, iu_ParameterStack)

end event

event ue_postopen;call super::ue_postopen;
dw_date_time_inquire.SetITem(1, 'begin_date', Date(idt_begin_datetime))
dw_date_time_inquire.Setitem(1, 'begin_time', Time(idt_begin_datetime))
dw_date_time_inquire.SetITem(1, 'end_date', Date(idt_end_datetime))
dw_date_time_inquire.Setitem(1, 'end_time', Time(idt_end_datetime))
end event

type cb_help from w_abstractresponseext`cb_help within w_mpr_exception_report_inq
integer x = 987
integer y = 328
integer taborder = 40
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_mpr_exception_report_inq
integer x = 617
integer y = 328
integer taborder = 30
end type

event cb_cancel::clicked;call super::clicked;idt_begin_datetime = DateTime(Date('1900-01-01'),Time("00:00:00"))
idt_end_datetime = DateTime(Date('1900-01-01'),Time("00:00:00"))
Close(Parent)
end event

type cb_ok from w_abstractresponseext`cb_ok within w_mpr_exception_report_inq
integer x = 283
integer y = 328
integer taborder = 20
end type

event cb_ok::clicked;call super::clicked;DateTime		ldt_begin_datetime, &
				ldt_end_datetime
				

If dw_date_time_inquire.AcceptText() = -1 Then Return

If dw_date_time_inquire.getitemtime(1, "begin_time") = time('09:30:00') then
	ldt_begin_datetime = DateTime(dw_date_time_inquire.getitemdate(1, "begin_date"), time('08:00:00'))
	ldt_end_datetime = DateTime(dw_date_time_inquire.getitemdate(1, "end_date"), time('12:00:00'))
else
	if dw_date_time_inquire.getitemtime(1, "begin_time") = time('13:30:00') then
		ldt_begin_datetime = DateTime(dw_date_time_inquire.getitemdate(1, "begin_date"), time('13:00:00'))
		ldt_end_datetime = DateTime(dw_date_time_inquire.getitemdate(1, "end_date"), time('17:00:00'))	
	else	
		ldt_begin_datetime = DateTime(dw_date_time_inquire.getitemdate(1, "begin_date"), time('08:00:00'))
		ldt_end_datetime = DateTime(dw_date_time_inquire.getitemdate(1, "end_date"), time('17:00:00'))	
	end if
end if
	
if (ldt_begin_datetime > ldt_end_datetime) Then
	MessageBox('Date Time Range Error', 'Beginning Date/Time is after ending Date/Time')
	Return
end if

idt_begin_datetime = ldt_begin_datetime
idt_end_datetime = ldt_end_datetime

Close(Parent)
end event

type dw_date_time_inquire from datawindow within w_mpr_exception_report_inq
integer x = 27
integer y = 32
integer width = 1417
integer height = 224
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_mpr_exception_report_inquire"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;This.InsertRow(0)

This.SetItem(1,"begin_date", Today())
This.SetItem(1,"begin_time", Time('08:00:00'))

This.setitem(1,"end_date", Today())
This.SetItem(1,"end_time", Time('17:00:00'))

end event

event doubleclicked;u_AbstractParameterStack	lu_Stack
String							ls_Date

// This script opens up the calendar when the correct column is double-clicked
// Let's make sure they double-clicked on a date or datetime column
If row <= 0 Then Return
If String(dwo.Type) <> "column" Then Return

If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return

ls_date = String(Today())

iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)

// Set up the parameters for the calendar window
lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
lu_stack.uf_Push("String", ls_Date)
lu_Stack.uf_Push("String", "d_calendar")
lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)

// Open the calendar window
iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
lu_Stack = Message.PowerObjectParm
If Not Isvalid( lu_stack ) Then Return
lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)

// Was it successful?  If so, get the date passed back and set the column.
If iu_ErrorContext.uf_IsSuccessful() Then
	lu_Stack.uf_Pop("String",ls_Date)
	If Len(Trim(ls_Date)) = 0 Then Return
	This.SetText(ls_date)
	If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
END IF

end event

