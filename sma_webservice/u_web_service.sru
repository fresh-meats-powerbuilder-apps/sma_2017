HA$PBExportHeader$u_web_service.sru
forward
global type u_web_service from nonvisualobject
end type
end forward

global type u_web_service from nonvisualobject
end type
global u_web_service u_web_service

type variables



end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_checkwebserviceerror (ref u_abstracterrorcontext au_errorcontext, soapexception a_soapexception, string as_windowname, string as_functionname, string as_eventname)
public function boolean uf_checkrpcerror (ref u_abstracterrorcontext au_errorcontext, string as_windowname, string as_functionname, string as_eventname, integer al_rval, string as_banner)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext);Return True
end function

public function boolean uf_checkwebserviceerror (ref u_abstracterrorcontext au_errorcontext, soapexception a_soapexception, string as_windowname, string as_functionname, string as_eventname);
/* --------------------------------------------------------

<DESC>	Get Web Service error information and populate an
			ErrorContext object with the details.</DESC>

<ARGS>	au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function after a Web Service RPC call.
			Check the error context's uf_IsSuccessful() to
			determine if a Netwise error occurred during
			processing.  The error context's text contains
			the error numbers and messages returned by
			Netwise.</USAGE>
-------------------------------------------------------- */
			
au_ErrorContext.uf_AppendText("Web Service Message: " + a_soapexception.getmessage())

au_ErrorContext.uf_AppendText("Web Service Detail Message: " + a_soapexception.getdetailmessage())

au_ErrorContext.uf_AppendText("Window Name: " + as_windowname)

au_ErrorContext.uf_AppendText("Function Name: " + as_functionname)

au_ErrorContext.uf_AppendText("Event Name: " + as_eventname)

au_ErrorContext.uf_SetReturnCode(-1)

Return False

end function

public function boolean uf_checkrpcerror (ref u_abstracterrorcontext au_errorcontext, string as_windowname, string as_functionname, string as_eventname, integer al_rval, string as_banner);
/* --------------------------------------------------------

<DESC>	Get Web Service error information and populate an
			ErrorContext object with the details.</DESC>

<ARGS>	au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function after a Web Service RPC call.
			Check the error context's uf_IsSuccessful() to
			determine if a Netwise error occurred during
			processing.  The error context's text contains
			the error numbers and messages returned by
			Netwise.</USAGE>
-------------------------------------------------------- */
			
au_ErrorContext.uf_AppendText("Window Name: " + as_windowname)

au_ErrorContext.uf_AppendText("Function Name: " + as_functionname)

au_ErrorContext.uf_AppendText("Event Name: " + as_eventname)

au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,1,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,81,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,161,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,241,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,321,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,401,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,481,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,561,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,641,80))
au_ErrorContext.uf_AppendText("Banner Page: " + mid(as_banner,721,80))


au_ErrorContext.uf_SetReturnCode(-1)

Return False

end function

on u_web_service.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_web_service.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

