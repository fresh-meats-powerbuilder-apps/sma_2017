﻿$PBExportHeader$w_mpr_exception_report.srw
forward
global type w_mpr_exception_report from w_abstractsheetext
end type
type dw_detail from datawindow within w_mpr_exception_report
end type
end forward

global type w_mpr_exception_report from w_abstractsheetext
integer width = 2551
integer height = 1432
string title = "MPR Exception Report"
dw_detail dw_detail
end type
global w_mpr_exception_report w_mpr_exception_report

type variables
String							is_Title

u_AbstractClassFactory		iu_ClassFactory

u_ErrorContext					iu_ErrorContext

u_NotificationController	iu_Notification	

w_smaframe						iw_frame	

u_mprreportdataaccessmprdb	iu_MPRReportDataAccess

DateTime							idt_begin_datetime, & 
									idt_end_datetime
end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then Return False

//If Not iu_ClassFactory.uf_GetObject( "u_TransCustWindowDataAccess", iu_TransCustWindowDataAccess, iu_ErrorContext ) Then
//	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
//		iu_Notification.uf_Display(iu_ErrorContext)
//		Return False
//	End If
//End If
//
//dw_detail.ResetUpdate()

This.Event Post ue_inquire()

Return True

end function

on w_mpr_exception_report.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
end on

on w_mpr_exception_report.destroy
call super::destroy
destroy(this.dw_detail)
end on

event ue_inquire;call super::ue_inquire;DateTime					ldt_begin_datetime, & 
							ldt_end_datetime

u_ParameterStack		lu_ParameterStack

String					ls_mpr_report_string, &
							ls_period

Long						ll_row

iu_ErrorContext.uf_Initialize()

This.Event Trigger CloseQuery()

If Message.ReturnValue <> 0 Then Return False

//if (date(idt_begin_datetime) = date('01/01/1900')) and (date(idt_end_datetime) = date('01/01/1900')) then
ldt_begin_datetime = DateTime(Today(),Time("17:00:00"))
ldt_end_datetime = DateTime(Today(),Time("17:00:00"))
//else
//	ldt_begin_datetime = idt_begin_datetime
//	ldt_end_datetime = idt_end_datetime
//end if

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('datetime', ldt_begin_datetime)
lu_ParameterStack.uf_Push('datetime', ldt_end_datetime)
lu_ParameterStack.uf_Push('string', is_Title)
lu_ParameterStack.uf_Push('window', iw_frame)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
iu_ClassFactory.uf_GetResponseWindow( "w_mpr_exception_report_inq", lu_ParameterStack )
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('window', iw_frame)
lu_ParameterStack.uf_Pop('string', is_Title)
lu_ParameterStack.uf_Pop('datetime', ldt_end_datetime)
lu_ParameterStack.uf_Pop('datetime', ldt_begin_datetime)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

lu_ParameterStack.uf_initialize()

If Not iu_ClassFactory.uf_GetObject( "u_mprreportdataaccessmprdb", iu_MPRReportDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
//lu_ParameterStack.uf_push('u_abstractmprreportdataaccess', iu_MPRReportDataAccess)
iu_MPRReportDataAccess.uf_Initialize( lu_ParameterStack )
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
If Not iu_ErrorContext.uf_IsSuccessful() Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If 

lu_ParameterStack.uf_initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('datetime', ldt_begin_datetime)
lu_ParameterStack.uf_Push('datetime', ldt_end_datetime)

if (date(ldt_begin_datetime) = date('01/01/1900')) and (date(ldt_end_datetime) = date('01/01/1900')) then Return False

idt_begin_datetime = ldt_begin_datetime
idt_end_datetime = ldt_end_datetime

iu_MPRReportDataAccess.uf_Retrieve( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_mpr_report_string)

lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

if time(ldt_begin_datetime) = time('08:00:00') then
	if time(ldt_end_datetime) = time('12:00:00') then
		ls_period = '1'
	else
		ls_period = 'All'
	end if
else
	ls_period = '2'
end if

dw_detail.Object.t_begin_heading.Text = 'Begin Date: ' +  & 
	String(Date(ldt_begin_datetime),"mm/dd/yyyy") 
	
dw_detail.Object.t_end_heading.Text = 'End Date: ' +  & 
	String(Date(ldt_end_datetime),"mm/dd/yyyy") 
	
dw_detail.Object.t_Period.Text = 'Period: ' +  ls_period 	

dw_detail.Reset()
ll_row = dw_detail.ImportString(ls_mpr_report_string)

If ll_row > 0 Then
	SetMicroHelp(String(ll_row) + ' Rows Retrieved')
Else
	SetMicroHelp('No Rows Retrieved')
	dw_detail.InsertRow(0)
End If

dw_detail.ResetUpdate()

Return False
end event

event open;call super::open;is_title = This.title
end event

event ue_print;call super::ue_print;dw_detail.Print()
end event

event ue_printnodialog;call super::ue_printnodialog;dw_detail.Print()
end event

event resize;call super::resize;dw_detail.resize( This.width - 50 , This.height - 110)
if dw_detail.width > 3100 then dw_detail.width = 3350

end event

type dw_detail from datawindow within w_mpr_exception_report
integer x = 18
integer width = 2496
integer height = 1340
integer taborder = 10
string title = "none"
string dataobject = "d_mpr_exception_report"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylelowered!
end type

