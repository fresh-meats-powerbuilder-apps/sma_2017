﻿$PBExportHeader$u_mprreportdataaccessmprdb.sru
forward
global type u_mprreportdataaccessmprdb from u_abstractmprreportdataaccess
end type
end forward

global type u_mprreportdataaccessmprdb from u_abstractmprreportdataaccess
end type
global u_mprreportdataaccessmprdb u_mprreportdataaccessmprdb

type variables
u_AbstractTransaction	MPR
end variables

forward prototypes
public subroutine uf_reset_sqlca ()
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public subroutine uf_reset_sqlca ();string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

//SQLCA = Create transaction // Create a transaction object
SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")

//if Upper(SQLCA.dbms) = Upper("SNC SQL Native Client") Then					 // Connecting to a SQL Server 
	SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
	SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password
//Else
//	SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
//End If

if SQLCA.database = 'false' or SQLCA.dbParm = 'false' then 							 // "SMA DATABASE" Not found in the INI
	SQLCA.database = ProfileString(ls_inifile, "DEFAULT DATABASE", "database", "pblocaldb")
	SQLCA.dbParm = ProfileString(ls_inifile, "DEFAULT DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
end if

Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode <> 0 then
	Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
	Disconnect using SQLCA;
	Return
End if

Return
end subroutine

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);DateTime			ldt_begin_datetime, &
					ldt_end_datetime
					
DataStore		lds_mpr_report		

String			ls_mpr_report_string

Long				ll_ret

u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('datetime', ldt_end_datetime)
au_ParameterStack.uf_Pop('datetime', ldt_begin_datetime)

//au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

lds_mpr_report = Create DataStore
lds_mpr_report.DataObject = 'd_mpr_exception_report'
ll_ret = lds_mpr_report.SetTransObject(MPR)

ll_ret = lds_mpr_report.Retrieve(ldt_begin_datetime,ldt_end_datetime)
ls_mpr_report_string = lds_mpr_report.object.datawindow.data

//au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_mpr_report_string)

DisConnect Using MPR; 
//This.uf_Reset_sqlca()

return true

end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);string	ls_inifile, ls_section
// disconnect any connectioin that might be open
//DisConnect Using SQLCA;

ls_inifile = 'ibp002.ini'
ls_section = "MPR DATABASE"

//SQLCA = Create transaction // Create a transaction object
MPR.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
MPR.database = ProfileString(ls_inifile, ls_section, "database", "false")

//if Upper(MPR.dbms) = Upper("SNC SQL Native Client") Then					 // Connecting to a SQL Server 
	MPR.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	MPR.LogId = ProfileString(ls_inifile, ls_section, "LogId", "mpr_reports")			 // SQL User name
	MPR.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "mpr_reports")		 // SQL Password
//Else
//	MPR.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
//End If
//
Connect Using MPR; //Connect to database
If MPR.SQLCode <> 0 then
	Messagebox("Error","Connection to database failed: " + MPR.SQLErrText)
	Disconnect using MPR;
	Return False
End if

Return True
end function

on u_mprreportdataaccessmprdb.create
call super::create
end on

on u_mprreportdataaccessmprdb.destroy
call super::destroy
end on

event constructor;call super::constructor;MPR = Create u_odbctransaction

end event

event destructor;call super::destructor;Destroy MPR
end event

