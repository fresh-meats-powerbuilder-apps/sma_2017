﻿$PBExportHeader$w_genericdisplaynotification.srw
forward
global type w_genericdisplaynotification from window
end type
type cb_close from commandbutton within w_genericdisplaynotification
end type
type cb_email from commandbutton within w_genericdisplaynotification
end type
type cb_print from commandbutton within w_genericdisplaynotification
end type
type mle_message from multilineedit within w_genericdisplaynotification
end type
type st_1 from statictext within w_genericdisplaynotification
end type
type dw_info from datawindow within w_genericdisplaynotification
end type
end forward

global type w_genericdisplaynotification from window
integer x = 46
integer y = 64
integer width = 2807
integer height = 1536
boolean titlebar = true
string title = "Notification Facility"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 79741120
cb_close cb_close
cb_email cb_email
cb_print cb_print
mle_message mle_message
st_1 st_1
dw_info dw_info
end type
global w_genericdisplaynotification w_genericdisplaynotification

on w_genericdisplaynotification.create
this.cb_close=create cb_close
this.cb_email=create cb_email
this.cb_print=create cb_print
this.mle_message=create mle_message
this.st_1=create st_1
this.dw_info=create dw_info
this.Control[]={this.cb_close,&
this.cb_email,&
this.cb_print,&
this.mle_message,&
this.st_1,&
this.dw_info}
end on

on w_genericdisplaynotification.destroy
destroy(this.cb_close)
destroy(this.cb_email)
destroy(this.cb_print)
destroy(this.mle_message)
destroy(this.st_1)
destroy(this.dw_info)
end on

event open;
/* --------------------------------------------------------
w_genericdisplaynotification

<OBJECT>	This window displays all the information stored
			in an error context object.</OBJECT>
			
<USAGE>	This window is opened by the notification
			controller to display generically the information
			stored in an error context object.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */
u_abstractparameterstack	lu_ParameterStack
String				ls_ErrorInfo, &
						ls_Message

lu_ParameterStack = Message.PowerObjectParm

lu_ParameterStack.uf_Pop('string',ls_ErrorInfo)
lu_ParameterStack.uf_Pop('string',ls_Message)

dw_info.ImportString(ls_ErrorInfo)

mle_message.Text = ls_Message


end event

type cb_close from commandbutton within w_genericdisplaynotification
integer x = 2505
integer y = 360
integer width = 247
integer height = 108
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Close"
boolean cancel = true
boolean default = true
end type

event clicked;Close(Parent)
end event

type cb_email from commandbutton within w_genericdisplaynotification
integer x = 2505
integer y = 224
integer width = 247
integer height = 108
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Email"
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Email a notification message.</DESC>

<USAGE>	This script runs when the user clicks the Email
			button.  It sets up a mail message containing
			the error context information.</USAGE>
-------------------------------------------------------- */
Long				ll_RowCount, &
					ll_Row
String			ls_Text
mailSession 	ms_Session
mailReturnCode	mr_ReturnCode
mailMessage  	mm_Message

ll_RowCount = dw_info.RowCount()

ls_Text = "Event codes set by the application:~r~n"

For ll_Row = 1 to ll_RowCount
	ls_Text += dw_info.GetItemString(ll_Row,"key") + ":  " + dw_info.GetItemString(ll_Row,"value") + "~r~n"
Next

ls_Text += "~r~n" + "Information gathered about the event follows:~r~n" + mle_message.Text

// Create a mail session
ms_Session = CREATE mailSession

// Log on to the session
mr_ReturnCode = ms_Session.mailLogon()
IF mr_ReturnCode <> mailReturnSuccess! THEN
	If mr_ReturnCode <> mailReturnUserAbort! Then
		MessageBox ("Can't send mail", "Unable to create a mail session.", Exclamation!)
	End If
	Return
END IF

mr_ReturnCode = ms_Session.mailAddress(mm_Message)
IF mr_ReturnCode <> mailReturnSuccess! THEN
	If mr_ReturnCode <> mailReturnUserAbort! Then
		MessageBox ("Can't send mail", "Mail addressing failed.", Exclamation!)
	End If
	ms_Session.mailLogoff()
	Return
END IF

If UpperBound(mm_Message.Recipient) < 1 Then
	MessageBox ("Can't send mail", "There must be at least one recipient for the message.", Exclamation!)
	ms_Session.mailLogoff()
	Return
End If

// Create the mail
mm_Message.Subject = "Application Notification"
mm_Message.NoteText = ls_Text

SetPointer(HourGlass!)
// Send the mail
mr_ReturnCode = ms_Session.mailSend(mm_Message)
IF mr_ReturnCode <> mailReturnSuccess! THEN
	MessageBox ("Can't send mail", "An error occurred attempting to send the message.", Exclamation!)
	ms_Session.mailLogoff()
	Return
ELSE
	MessageBox ("Mail sent", "The message was sent successfully.", Information!)
END IF

ms_Session.mailLogoff()
Return 


end event

type cb_print from commandbutton within w_genericdisplaynotification
integer x = 2505
integer y = 88
integer width = 247
integer height = 108
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Print"
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Display a notification message.</DESC>

<USAGE>	This script runs when the Print button is
			clicked.  It prints a hardcopy version of the
			displayed window with the error context
			information.</USAGE>
-------------------------------------------------------- */
Long		ll_RowCount
DataStore	lds_Print

SetPointer(Hourglass!)

lds_Print = Create DataStore
lds_Print.DataObject = 'd_genericprintnotification'

ll_RowCount = lds_Print.ImportString(dw_info.Object.DataWindow.Data)

If ll_RowCount > 0 Then 
	lds_Print.SetItem(1,"message", mle_message.Text)
End If

If lds_Print.Print(False) = 1 Then
	MessageBox("Print succeeded","The notification printed successfully.", Information!)
Else
	MessageBox("Print failed","The notification could not be printed.", Exclamation!)
End If


end event

type mle_message from multilineedit within w_genericdisplaynotification
integer x = 32
integer y = 584
integer width = 2734
integer height = 880
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_genericdisplaynotification
integer x = 41
integer y = 16
integer width = 2738
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "An event which requires notification has occurred in your application.  The information gathered about the event is listed below:"
boolean focusrectangle = false
end type

type dw_info from datawindow within w_genericdisplaynotification
integer x = 32
integer y = 84
integer width = 2423
integer height = 480
string dataobject = "d_genericdisplaynotification"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

