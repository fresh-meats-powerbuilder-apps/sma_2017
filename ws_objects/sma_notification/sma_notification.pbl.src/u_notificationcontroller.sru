﻿$PBExportHeader$u_notificationcontroller.sru
forward
global type u_notificationcontroller from u_abstractnotificationcontroller
end type
end forward

global type u_notificationcontroller from u_abstractnotificationcontroller
end type
global u_notificationcontroller u_notificationcontroller

type variables
Private:
u_abstracterrorcontext	iu_ErrorContext
u_abstractclassfactory	iu_ClassFactory

end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory)
public function boolean uf_display (ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_displaygeneric (ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory);
/* --------------------------------------------------------

<DESC>	Initialize the notification controller.</DESC>

<ARGS>	au_abstractclassfactory: ClassFactory</ARGS>
			
<USAGE>	This function initializes the notification
			controller.  Call it first.</USAGE>
-------------------------------------------------------- */
iu_ClassFactory = au_ClassFactory

Return True
end function

public function boolean uf_display (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Display a notification message.
			The way the message is displayed is determined by the value returned by
			au_errorcontext.uf_GetReturnCode() (wich is set by au_errorcontext.uf_SetReturnCode(X)
			 0 				Displays no Message
			 Less than 0 	Display the generice error window
			 1 through 9	Message is displayed in the MicroHelp
			 Numbers Greater than 9 are displayed as MessagBoxes, IF AND ONLY IF THEY ARE VALID 
			 DIGIT COMBINATIONS. Invalid Digit will be displayed generically. For Example 38 would
			 Display generic because '8' is not a valid ones digit.
			 The ten's Digit specifies 
			 the Bitmap that will be displayed
			 <li>1 - Information</li>
			 <li>2 - StopSign</li>
			 <li>3 - Exclamation</li>
			 <li>4 - question</li>
			 <li>5 - None</li>
			 The One's digit specifies the button set that will be used
			 <li>0 - OK</li>
			 <li>1 - OKCancel</li>
			 <li>2 - YesNo</li>
			 <li>3 - yesNoCancel</li>
			 <li>4 - RetryCancel</li>
			 <li>5 - ABORTRETRYIGNORE</li>
			 When the user presses a button on the messagebox the value "MessageBoxReturnValue" will 
			 be set using au_errorcontext.uf_set("MessageBoxReturnValue",X) X an integer value determined
			 by the button they press. The following are the valid values and the button they represent.
			 <li>1 - OK</li>
			 <li>2 - Yes</li>
			 <li>3 - No</li>
			 <li>4 - Cancel</li>
			 <li>5 - Abort</li>
			 <li>6 - Retry</li>
			 <li>7 Ignore</li></DESC>
						 

<ARGS>	au_errorcontext: The error context to display</ARGS>
			
<USAGE>	Call this function to display a message for a given error
			context.  Typically this function is called when
			the error context's uf_IsSuccessful() call returns
			false.  This function calls the private function
			uf_displaygeneric() to actually open the window
			and handle the notification.</USAGE>
-------------------------------------------------------- */
Integer li_MessageRtn
Long ll_Rtn 

String	ls_ErrorInfo
Window	lw_Frame

ll_rtn = au_errorcontext.uf_GetReturnCode()


Choose Case True
	Case 	 ll_rtn = 0
		Return True
	Case 	ll_rtn < 0
		Return uf_displaygeneric(au_errorcontext)
	Case (ll_rtn > 0 AND ll_Rtn < 10)
		//Set The MicroHelp
		if iu_classfactory.uf_GetObject("window",lw_frame,au_errorcontext) Then
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		au_errorcontext.uf_IsSuccessful() 
			lw_Frame.SetMicroHelp(ls_ErrorInfo)
		ELSE
			MessageBox("Notification Facility", ls_ErrorInfo+"~r~nFrame not a long lived object can not set microhelp")
			au_errorContext.uf_Set("MessageBoxReturnValue",1)
		END IF
	Case ll_rtn  = 10
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		MessageBox("Notification Facility", &
					ls_ErrorInfo)
		au_errorContext.uf_Set("MessageBoxReturnValue",1)
	Case ll_rtn  = 11
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, INFORMATION!, OKCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",li_MessageRtn)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 12
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, INFORMATION!, YesNo!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",2)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",3)
		END IF
	Case ll_rtn  = 13
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, INFORMATION!, YesNoCancel!)
		au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 1)
	Case ll_rtn  = 14
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, INFORMATION!, RetryCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",6)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 15
				ls_ErrorInfo = au_ErrorContext.uf_GetText()
				li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, INFORMATION!, ABORTRETRYIGNORE!)
				au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 4)
	Case ll_rtn  = 20
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		MessageBox("Notification Facility",Ls_ErrorInfo,StopSign!,OK!)
		au_errorContext.uf_Set("MessageBoxReturnValue",1)
	Case ll_rtn  = 21
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, StopSign!, OKCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",li_MessageRtn)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 22
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, StopSign!, YesNo!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",2)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",3)
		END IF
	Case ll_rtn  = 23
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, StopSign!, YesNoCancel!)
		au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 1)
	Case ll_rtn  = 24
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, StopSign!, RetryCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",6)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 25
				ls_ErrorInfo = au_ErrorContext.uf_GetText()
				li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, StopSign!, ABORTRETRYIGNORE!)
				au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 4)
	Case ll_rtn  = 30
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		MessageBox("Notification Facility",Ls_ErrorInfo,Exclamation!,OK!)
		au_errorContext.uf_Set("MessageBoxReturnValue",1)
	Case ll_rtn  = 31
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Exclamation!, OKCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",li_MessageRtn)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 32
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Exclamation!, YesNo!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",2)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",3)
		END IF
	Case ll_rtn  = 33
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Exclamation!, YesNoCancel!)
		au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 1)
	Case ll_rtn  = 34
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Exclamation!, RetryCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",6)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 35
				ls_ErrorInfo = au_ErrorContext.uf_GetText()
				li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Exclamation!, ABORTRETRYIGNORE!)
				au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 4)
	Case ll_rtn  = 40
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		MessageBox("Notification Facility",Ls_ErrorInfo,Question!,OK!)
		au_errorContext.uf_Set("MessageBoxReturnValue",1)
	Case ll_rtn  = 41
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Question!, OKCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",li_MessageRtn)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 42
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Question!, YesNo!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",2)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",3)
		END IF
	Case ll_rtn  = 43
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Question!, YesNoCancel!)
		au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 1)
	Case ll_rtn  = 44
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Question!, RetryCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",6)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 45
				ls_ErrorInfo = au_ErrorContext.uf_GetText()
				li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, Question!, ABORTRETRYIGNORE!)
				au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 4)
	Case ll_rtn  = 50
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		MessageBox("Notification Facility",Ls_ErrorInfo,None!,OK!)
		au_errorContext.uf_Set("MessageBoxReturnValue",1)
	Case ll_rtn  = 51
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, None!, OKCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",li_MessageRtn)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 52
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, None!, YesNo!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",2)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",3)
		END IF
	Case ll_rtn  = 53
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, None!, YesNoCancel!)
		au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 1)
	Case ll_rtn  = 54
		ls_ErrorInfo = au_ErrorContext.uf_GetText()
		li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, None!, RetryCANCEL!)
		IF li_MessageRtn = 1 THEN
			au_errorContext.uf_Set("MessageBoxReturnValue",6)
		ELSE
			au_errorContext.uf_Set("MessageBoxReturnValue",4)
		END IF
	Case ll_rtn  = 55
				ls_ErrorInfo = au_ErrorContext.uf_GetText()
				li_MessageRtn = MessageBox("Notification Facility", ls_ErrorInfo, None!, ABORTRETRYIGNORE!)
				au_errorContext.uf_Set("MessageBoxReturnValue",li_messageRtn + 4)
	Case ELSE
		Return uf_displaygeneric(au_errorcontext)
END CHoose 
Return TRUE
end function

private function boolean uf_displaygeneric (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Display a generic notification window.</DESC>

<ARGS>	au_errorcontext: Error context object</ARGS>
			
<USAGE>	This function is called by uf_Display() to 
			open a generic notification window.  It opens
			the response window
			w_genericdisplaynotification, passing it
			a parameter stack with two strings: the error
			context's message text and the string returned
			from the error context's uf_GetAll() method call.
			It returns false if the parameter stack could
			not be created.</USAGE>
-------------------------------------------------------- */
String	ls_ErrorInfo
u_AbstractParameterStack	lu_ParameterStack

au_ErrorContext.uf_GetAll(ls_ErrorInfo)

If Not iu_classFactory.uf_GetObject('u_parameterstack',lu_ParameterStack) Then
	If IsValid(lu_ParameterStack) Then
		lu_ParameterStack.uf_Initialize()
	Else
		Return False
	End If
End If

lu_ParameterStack.uf_Push("string",au_ErrorContext.uf_GetText())
lu_ParameterStack.uf_Push("string",ls_ErrorInfo)
iu_classfactory.uf_GetResponseWindow("w_genericdisplaynotification",lu_ParameterStack)

Return True
end function

on u_notificationcontroller.create
TriggerEvent( this, "constructor" )
end on

on u_notificationcontroller.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;
/* --------------------------------------------------------
u_notificationcontroller

<OBJECT>	This object is responsible for notifying the user
			or others about messages generated and stored
			in an error context object.</OBJECT>
			
<USAGE>	Get one of these objects, initialize it, and
			when you have an error context object that
			needs notification, call uf_Display() to
			notify the user or others about messages
			generated and stored in the error context
			object.</USAGE>

<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */

end event

