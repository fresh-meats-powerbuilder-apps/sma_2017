﻿$PBExportHeader$w_fwh_div_mass_adjustment_inq.srw
forward
global type w_fwh_div_mass_adjustment_inq from w_abstractresponseext
end type
type dw_fwh from datawindow within w_fwh_div_mass_adjustment_inq
end type
type dw_location from datawindow within w_fwh_div_mass_adjustment_inq
end type
type st_1 from statictext within w_fwh_div_mass_adjustment_inq
end type
end forward

global type w_fwh_div_mass_adjustment_inq from w_abstractresponseext
int Width=1024
int Height=448
dw_fwh dw_fwh
dw_location dw_location
st_1 st_1
end type
global w_fwh_div_mass_adjustment_inq w_fwh_div_mass_adjustment_inq

type variables
u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractClassFactory		iu_ClassFactory
u_NotificationController		iu_Notification	
w_smaframe			iw_frame	
String				is_title, &
				is_division_group, &
				is_dest_plant_code
end variables

on w_fwh_div_mass_adjustment_inq.create
int iCurrent
call super::create
this.dw_fwh=create dw_fwh
this.dw_location=create dw_location
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fwh
this.Control[iCurrent+2]=this.dw_location
this.Control[iCurrent+3]=this.st_1
end on

on w_fwh_div_mass_adjustment_inq.destroy
call super::destroy
destroy(this.dw_fwh)
destroy(this.dw_location)
destroy(this.st_1)
end on

event close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_division_group)
iu_ParameterStack.uf_Push('string', is_dest_plant_code)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

event open;call super::open;string						ls_loc_string
DataWindowChild 			ldwc_child
long							rtncode

iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_dest_plant_code)
iu_ParameterStack.uf_Pop('string', is_division_group)
iu_ParameterStack.uf_Pop('string', ls_loc_string)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

rtncode = dw_location.GetChild('destplantcode', ldwc_child)
IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")
//ls_loc_string = "201" + "~t" + "CARCASS" + "~r~n" + &
//						"205" + "~t" + "VARIETY MEATS" + "~r~n" + &
//						"207" + "~t" + "TALLOW REFINERY" + "~r~n" + &
//						"211" + "~t" + "PROCESSING" + "~r~n" + &
//						"215" + "~t" + "COW PROCESSING / VM" + "~r~n" + &
//						"216" + "~t" + "VARIETY MEATS - COW" + "~r~n" + &
//						"218" + "~t" + "CARCASS - COW" + "~r~n" + &
//						"222" + "~t" + "DESIGN PRODUCTS" + "~r~n" + &
//						"223" + "~t" + "CONSUMER PRODUCTS" + "~r~n" + &
//						"231" + "~t" + "PORK PROCESSING" + "~r~n" + &
//						"232" + "~t" + "PORK VARIETY MEATS" + "~r~n" + &
//						"234" + "~t" + "MORELLI PORK PRODUCTS" + "~r~n" + &
//						"235" + "~t" + "SUPREME PROCESSED FOODS" + "~r~n" + &
//						"257" + "~t" + "CASE READY/PUMPED PRODUCT" + "~r~n"						

ldwc_child.importstring(ls_loc_string)

dw_fwh.setitem(1,1,is_division_group)
dw_location.setitem(1,1,is_dest_plant_code)

This.Title = is_title + ' Inquire'

this.setredraw(true)

end event

type cb_help from w_abstractresponseext`cb_help within w_fwh_div_mass_adjustment_inq
int X=736
int Y=232
int TabOrder=40
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_fwh_div_mass_adjustment_inq
int X=366
int Y=232
int TabOrder=30
boolean Cancel=true
end type

event cb_cancel::clicked;is_division_group = "Cancel"
Close(Parent) 
end event

type cb_ok from w_abstractresponseext`cb_ok within w_fwh_div_mass_adjustment_inq
int X=32
int Y=232
boolean Default=true
end type

event cb_ok::clicked;u_String_Functions		lu_StringFunctions

parent.dw_fwh.accepttext()
parent.dw_location.accepttext()
if lu_StringFunctions.nf_IsEmpty(parent.dw_fwh.getitemstring(1, 1)) and &
	lu_StringFunctions.nf_IsEmpty(parent.dw_location.getitemstring(1, 1))then
	messagebox("Required Field","Division Group or Location must be choosen.",StopSign!,OK!)
	parent.dw_fwh.setfocus()
	parent.dw_fwh.SelectText ( 1, 100 )
	return
end if

is_division_group = parent.dw_fwh.getitemstring(1, 1)
is_dest_plant_code = parent.dw_location.getitemstring(1, 1)

Close(Parent)
end event

type dw_fwh from datawindow within w_fwh_div_mass_adjustment_inq
int X=46
int Y=40
int Width=763
int Height=76
int TabOrder=20
boolean BringToTop=true
string DataObject="d_fwh_sales_trans_freight_divgrp_inq"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

type dw_location from datawindow within w_fwh_div_mass_adjustment_inq
int X=366
int Y=124
int Width=329
int Height=72
int TabOrder=20
boolean BringToTop=true
string DataObject="d_fwh_dest_location"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

type st_1 from statictext within w_fwh_div_mass_adjustment_inq
int X=91
int Y=124
int Width=265
int Height=76
boolean Enabled=false
boolean BringToTop=true
string Text="Destination:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

