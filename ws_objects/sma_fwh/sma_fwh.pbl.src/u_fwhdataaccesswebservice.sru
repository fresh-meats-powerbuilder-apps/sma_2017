﻿$PBExportHeader$u_fwhdataaccesswebservice.sru
forward
global type u_fwhdataaccesswebservice from u_fwhdataaccess
end type
end forward

global type u_fwhdataaccesswebservice from u_fwhdataaccess
end type
global u_fwhdataaccesswebservice u_fwhdataaccesswebservice

forward prototypes
public function boolean uf_fwh_str_divg_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_str_divg_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_str_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_str_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_div_adj_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_div_adj_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_sku_adj_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_sku_adj_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_div_list (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_loc_list (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_assign_maint_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_assign_maint_upd (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_fwh_str_divg_inq (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Retrieve the Data for the FWH Sales Transfer Freight by Division Group window	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess		lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas08er_fwh_sales_trans_divg_inq(ls_appname, &
													ls_windowname, &
													"uf_fwh_str_divg_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true

end function

public function boolean uf_fwh_str_divg_upd (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Update the Data for the FWH Sales Transfer Freight by Division Group window	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will update the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString , &
									ls_headerstring

				
u_smas01sr					lu_smas01sr

u_AbstractdataAccess		lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_headerstring)
au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
////TEST
//messagebox ("INPUT", ls_inquireString)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas09er_fwh_sales_trans_divg_upd(ls_appname, &
													ls_windowname, &
													"uf_fwh_str_divg_upd", &
													"", &
													ls_userid, &
													ls_password, &
													ls_headerstring, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true

end function

public function boolean uf_fwh_str_upd (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Update the Data for the FWH Sales Transfer Freight window	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will update the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas11er_fwh_sales_trans_feight_upd(ls_appname, &
													ls_windowname, &
													"uf_fwh_str_upd", &
													"", &
													ls_userid, &
													ls_password, &	
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true

end function

public function boolean uf_fwh_str_inq (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Retrieve the Data for the FWH Sales Transfer Freight window	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas10er_fwh_sales_trans_freight_inq(ls_appname, &
													ls_windowname, &
													"uf_fwh_str_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_fwh_div_adj_inq (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Retrieve the Data for the FWH Sales Transfer Freight window by division	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas14er_fwh_div_adj_inq(ls_appname, &
													ls_windowname, &
													"uf_fwh_div_adj_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_fwh_div_adj_upd (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Update the Data for the FWH Sales Transfer Freight window by division	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will update the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas15er_fwh_div_adj_upd(ls_appname, &
													ls_windowname, &
													"uf_fwh_div_adj_upd", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true

end function

public function boolean uf_fwh_sku_adj_inq (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Retrieve the Data for the FWH Sales Transfer Freight window by sku	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas13er_fwh_sku_adj_inq(ls_appname, &
													ls_windowname, &
													"uf_fwh_sku_adj_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_fwh_sku_adj_upd (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Update the Data for the FWH Sales Transfer Freight window by sku	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will update the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas12er_fwh_sku_adj_upd(ls_appname, &
													ls_windowname, &
													"uf_fwh_sku_adj_upd", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true

end function

public function boolean uf_fwh_div_list (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Retrieve the divisions assosiated with FWH	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> Retrieve the divisions assosiated with FWH
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas16er_fwh_div_list(ls_appname, &
													ls_windowname, &
													"uf_fwh_div_list", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_fwh_loc_list (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Retrieve the locations assosiated with FWH	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> Retrieve the locations assosiated with FWH
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas17er_fwh_location_list(ls_appname, &
													ls_windowname, &
													"uf_fwh_loc_list", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true

end function

public function boolean uf_fwh_assign_maint_inq (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquireString)
		Return False
	End If
End if

lu_smas01sr.uf_smas18er_fwh_Assign_Maint_inq(ls_appname, &
													ls_windowname, &
													"uf_fwh_assign_maint_inq", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquireString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquireString)

return true
end function

public function boolean uf_fwh_assign_maint_upd (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------

<DESC> Update the Data for the FWH Sales Transfer Freight window	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will update the FWH Sales Transfer Freight inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_updateString 

				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_updateString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_updateString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_updateString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_updateString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_updateString)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_updateString)
		Return False
	End If
End if

lu_smas01sr.uf_smas19er_fwh_assign_maint_upd(ls_appname, &
													ls_windowname, &
													"uf_fwh_assign_maint_upd", &
													"", &
													ls_userid, &
													ls_password, &
													ls_updateString, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_updateString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_updateString)

return true

end function

on u_fwhdataaccesswebservice.create
call super::create
end on

on u_fwhdataaccesswebservice.destroy
call super::destroy
end on

