﻿$PBExportHeader$w_fwh_sku_mass_adjustment_inq.srw
forward
global type w_fwh_sku_mass_adjustment_inq from w_abstractresponseext
end type
type dw_product from datawindow within w_fwh_sku_mass_adjustment_inq
end type
type dw_division from datawindow within w_fwh_sku_mass_adjustment_inq
end type
type dw_location from datawindow within w_fwh_sku_mass_adjustment_inq
end type
type st_1 from statictext within w_fwh_sku_mass_adjustment_inq
end type
type st_2 from statictext within w_fwh_sku_mass_adjustment_inq
end type
type st_3 from statictext within w_fwh_sku_mass_adjustment_inq
end type
end forward

global type w_fwh_sku_mass_adjustment_inq from w_abstractresponseext
int Width=1207
int Height=472
dw_product dw_product
dw_division dw_division
dw_location dw_location
st_1 st_1
st_2 st_2
st_3 st_3
end type
global w_fwh_sku_mass_adjustment_inq w_fwh_sku_mass_adjustment_inq

type variables
u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractClassFactory		iu_ClassFactory
u_NotificationController		iu_Notification	
w_smaframe			iw_frame	
String				is_title, &
				is_division_code, &
				is_product_code, &
				is_dest_plant_code
end variables

on w_fwh_sku_mass_adjustment_inq.create
int iCurrent
call super::create
this.dw_product=create dw_product
this.dw_division=create dw_division
this.dw_location=create dw_location
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_location
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.st_3
end on

on w_fwh_sku_mass_adjustment_inq.destroy
call super::destroy
destroy(this.dw_product)
destroy(this.dw_division)
destroy(this.dw_location)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
end on

event close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_product_code)
iu_ParameterStack.uf_Push('string', is_division_code)
iu_ParameterStack.uf_Push('string', is_dest_plant_code)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_dest_plant_code)
iu_ParameterStack.uf_Pop('string', is_division_code)
iu_ParameterStack.uf_Pop('string', is_product_code)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

dw_division.setitem(1,1,is_division_code)
dw_location.setitem(1,1,is_dest_plant_code)
dw_product.setitem(1,1,is_product_code)

This.Title = is_title + ' Inquire'

this.setredraw(true)

end event

type cb_help from w_abstractresponseext`cb_help within w_fwh_sku_mass_adjustment_inq
int X=837
int Y=212
int TabOrder=50
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_fwh_sku_mass_adjustment_inq
int X=466
int Y=212
int TabOrder=40
end type

event cb_cancel::clicked;is_division_code = "Cancel"
Close(Parent) 
end event

type cb_ok from w_abstractresponseext`cb_ok within w_fwh_sku_mass_adjustment_inq
int X=133
int Y=212
end type

event cb_ok::clicked;u_String_Functions		lu_StringFunctions

parent.dw_division.accepttext()
parent.dw_location.accepttext()
parent.dw_product.accepttext()
if lu_StringFunctions.nf_IsEmpty(parent.dw_division.getitemstring(1, 1)) and &
	lu_StringFunctions.nf_IsEmpty(parent.dw_location.getitemstring(1, 1)) and &
	lu_StringFunctions.nf_IsEmpty(parent.dw_product.getitemstring(1, 1)) then
	messagebox("Required Field","Division, Product or Location must be choosen.",StopSign!,OK!)
	parent.dw_division.setfocus()
	parent.dw_division.SelectText ( 1, 100 )
	return
end if

is_dest_plant_code = parent.dw_location.getitemstring(1, 1) 
is_division_code = parent.dw_division.getitemstring(1, 1)
is_product_code = parent.dw_product.getitemstring(1, 1)

Close(Parent)
end event

type dw_product from datawindow within w_fwh_sku_mass_adjustment_inq
int X=59
int Y=92
int Width=443
int Height=68
int TabOrder=20
boolean BringToTop=true
string DataObject="d_fwh_product"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

type dw_division from datawindow within w_fwh_sku_mass_adjustment_inq
int X=526
int Y=88
int Width=283
int Height=80
int TabOrder=30
boolean BringToTop=true
string DataObject="d_fwh_division"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

type dw_location from datawindow within w_fwh_sku_mass_adjustment_inq
int X=882
int Y=88
int Width=293
int Height=72
int TabOrder=30
boolean BringToTop=true
string DataObject="d_fwh_dest_location"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

type st_1 from statictext within w_fwh_sku_mass_adjustment_inq
int X=105
int Y=32
int Width=320
int Height=60
boolean Enabled=false
boolean BringToTop=true
string Text="Product Code:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_fwh_sku_mass_adjustment_inq
int X=512
int Y=32
int Width=306
int Height=56
boolean Enabled=false
boolean BringToTop=true
string Text="Division Code"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_fwh_sku_mass_adjustment_inq
int X=887
int Y=28
int Width=251
int Height=56
boolean Enabled=false
boolean BringToTop=true
string Text="Destination"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

