﻿$PBExportHeader$w_prod_cat_maint.srw
forward
global type w_prod_cat_maint from w_abstractsheetext
end type
type dw_prod_cat_maint from datawindow within w_prod_cat_maint
end type
end forward

global type w_prod_cat_maint from w_abstractsheetext
integer width = 2368
integer height = 1532
string title = "Product Category Maintenance"
dw_prod_cat_maint dw_prod_cat_maint
end type
global w_prod_cat_maint w_prod_cat_maint

type variables
w_prod_cat_maint		  iw_parent

string                 is_title
                       
boolean                ib_reinquire

u_ErrorContext		      		iu_ErrorContext
u_AbstractClassFactory	      iu_ClassFactory
u_NotificationController	   iu_Notification	
u_CategoryDataAccess	      	iu_CategoryDataAccess
w_smaframe		     				iw_frame	
u_odbctransaction					iu_odbctransaction

u_AbstractParameterStack		iu_ParameterStack

end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack)
public function string wf_buildupdatestring ()
public function boolean wf_reset_row ()
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack);string					ls_string
iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not iu_ClassFactory.uf_GetObject( "u_CategoryDataAccess", iu_CategoryDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()

This.Event Post ue_inquire()

Return True
end function

public function string wf_buildupdatestring ();Long					ll_ModifiedCount, &
						ll_NewCount, &
						ll_count
						
String				ls_UpdateString, &
						ls_new_row_ind, &
						ls_category_id

dwItemStatus		lis_UpdateStatus


ll_ModifiedCount = dw_prod_cat_maint.ModifiedCount()

If ll_ModifiedCount <= 0  Then
	SetMicroHelp('No Update Necessary')
	Return ''
End If

ls_UpdateString = ''

ll_Count = 0
IF ll_ModifiedCount > 0 Then
	ll_Count = dw_prod_cat_maint.GetNextModified(ll_Count, Primary!)
	Do
		dw_prod_cat_maint.SelectRow(0, False)
		dw_prod_cat_maint.SelectRow(ll_count, True)
		
		ls_new_row_ind = dw_prod_cat_maint.GetItemString(ll_count, 'new_row_ind')
		ls_category_id = dw_prod_cat_maint.GetItemString(ll_Count, 'category_id')
		
		If ls_category_id > ' ' Then
			Choose Case ls_new_row_ind
				Case 'N'
					ls_UpdateString += 'I' + '~t' + dw_prod_cat_maint.GetItemString(ll_Count, 'category_id') + '~t' + &
							dw_prod_cat_maint.GetItemString(ll_Count, 'description') + '~t' + &
							String(dw_prod_cat_maint.GetItemString(ll_Count, 'inc_req_ind')) + &
							'~r~n'
				Case Else
					ls_UpdateString += 'U' + '~t' + dw_prod_cat_maint.GetItemString(ll_Count, 'category_id') + '~t' + &
							dw_prod_cat_maint.GetItemString(ll_Count, 'description') + '~t' + &
							String(dw_prod_cat_maint.GetItemString(ll_Count, 'inc_req_ind')) + &
							'~r~n'
			End Choose
		End if
		ll_Count = dw_prod_cat_maint.GetNextModified(ll_Count, Primary!)
	Loop While ll_Count > 0
End If

dw_prod_cat_maint.SelectRow(0, False)

Return ls_UpdateString

end function

public function boolean wf_reset_row ();long				ll_row, &
					ll_count
string			ls_category


ll_count = dw_prod_cat_maint.RowCount()
ll_row = 1

This.Event Post ue_inquire()

if ll_count > 0 Then
	Do
		ls_category = dw_prod_cat_maint.GetItemString(ll_count, 'category_id')
		If ls_category > ' ' Then
			dw_prod_cat_maint.setitem(ll_count, "new_row_ind", ' ')
		End If
		ll_row = ll_row + 1
	Loop While ll_row < ll_count + 1
End If

return true

end function

on w_prod_cat_maint.create
int iCurrent
call super::create
this.dw_prod_cat_maint=create dw_prod_cat_maint
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prod_cat_maint
end on

on w_prod_cat_maint.destroy
call super::destroy
destroy(this.dw_prod_cat_maint)
end on

event ue_inquire;call super::ue_inquire;string 				ls_string, &
						ls_InquireString, &
						ls_OutputString, &
						ls_appname, &
						ls_windowname, &
						ls_description

long					ll_row, &
						ll_row_count

u_ParameterStack		lu_ParameterStack

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

dw_prod_cat_maint.reset()

ls_AppName = GetApplication().AppName
ls_WindowName = 'prodcat'
ls_InquireString = 'TSKUCATG' + "~t" +'~r~n'

iu_ErrorContext.uf_Initialize()

lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_InquireString)

iu_CategoryDataAccess.uf_inq_prod_cat_maint(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_OutputString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If

ll_row = dw_prod_cat_maint.ImportString(ls_OutputString)
ll_row_count = dw_prod_cat_maint.RowCount()

If ll_row_count > 0 Then
	SetMicroHelp(String(ll_row_count) + ' Rows Retrieved')
Else
	SetMicroHelp('No Rows Retrieved')
	This.SetRedraw(True)
	Return True
End If

dw_prod_cat_maint.ResetUpdate()

dw_prod_cat_maint.InsertRow(0)

ll_row_count = dw_prod_cat_maint.RowCount()
dw_prod_cat_maint.setitem(ll_row_count, "new_row_ind", 'N')

ll_row = 1
Do
	dw_prod_cat_maint.setitem(ll_row, "description",Trim(dw_prod_cat_maint.GetItemString(ll_row, "description")))
	
	ll_row = ll_row + 1
Loop While ll_row < ll_row_count + 1

This.SetRedraw(True)

Return True
end event

event ue_save;call super::ue_save;Long						ll_row
String					ls_detailstring

u_ParameterStack						lu_ParameterStack

u_String_Functions					lu_strings


iu_ErrorContext.uf_Initialize()

IF dw_prod_cat_maint.AcceptText() = -1 THEN Return False

This.SetRedraw(False)

ls_DetailString = wf_BuildUpdateString()

If lu_strings.nf_IsEmpty(ls_DetailString) Then 
	This.SetRedraw(True)
	Return False
End If

SetMicroHelp("Wait... Updating the Database")

SetPointer(HourGlass!)

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_CategoryDataAccess.uf_upd_prod_cat_maint(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

This.SetRedraw(True)

wf_reset_row() 

dw_prod_cat_maint.ResetUpdate()

SetMicroHelp("Update Successful")

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True
end event

event ue_delete;call super::ue_delete;Long										ll_row

String									ls_detailstring, &
											ls_appname, &
											ls_windowname, &
											ls_inquirestring

u_ParameterStack						lu_ParameterStack

u_String_Functions					lu_strings


iu_ErrorContext.uf_Initialize()

//ll_row = dw_prod_cat_maint.GetRow()

ll_row = dw_prod_cat_maint.GetSelectedRow(0)	

If ll_row <= 0 Then Return

ll_row = 0

If MessageBox('Delete Product Category', 'Are you sure you want to delete this Product Category?', Exclamation!, YesNo!, 2) = 1 Then
	
	ll_row = dw_prod_cat_maint.GetSelectedRow(0)
	
	do while ll_row > 0
		If (dw_prod_cat_maint.getitemstatus(ll_row, 0, Primary!) = NotModified!  or &
			dw_prod_cat_maint.getitemstatus(ll_row, 0, Primary!) = DataModified!)  then
			ls_detailstring +='D' + '~t' + dw_prod_cat_maint.GetItemString(ll_row, 'category_id') + '~t' + &
							dw_prod_cat_maint.GetItemString(ll_row, 'description') + '~t' + &
							String(dw_prod_cat_maint.GetItemString(ll_row, 'inc_req_ind')) + &
							'~r~n'
		End if
		dw_prod_cat_maint.SelectRow(ll_row, False)
		ll_row = dw_prod_cat_maint.GetSelectedRow(0)
		
	loop 
End If

If lu_strings.nf_IsEmpty(ls_DetailString) Then 
	This.SetRedraw(True)
	Return 
End If

SetMicroHelp("Wait... Updating the Database")

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_CategoryDataAccess.uf_upd_prod_cat_maint(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

This.SetRedraw(True)

SetMicroHelp("Delete Successful")

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
Else
	This.Event Post ue_inquire()
End If

This.SetRedraw(True)
dw_prod_cat_maint.ResetUpdate()
Return 


end event

event open;call super::open;
//iu_ClassFactory = au_ClassFactory

//iu_ErrorContext = au_ErrorContext

//If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
//	iu_Notification.uf_Initialize(iu_ClassFactory)
//End If
//
//If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
//	iu_notification.uf_display(iu_ErrorContext)
////	Return False
//End If
//
//iu_ErrorContext.uf_Initialize()

iu_ParameterStack = Message.PowerObjectParm

iw_parent = This
is_Title = This.Title

//Return True
end event

event close;call super::close;close(this)
end event

event resize;call super::resize;IF newheight > dw_prod_cat_maint.Y THEN
	dw_prod_cat_maint.Height = newheight - dw_prod_cat_maint.y - 20
END IF

IF newwidth > dw_prod_cat_maint.X THEN
	dw_prod_cat_maint.Width = newwidth - dw_prod_cat_maint.x - 20
END IF
end event

type dw_prod_cat_maint from datawindow within w_prod_cat_maint
event ue_post_constructor ( )
event ue_new ( )
event ue_delete ( )
integer x = 91
integer y = 108
integer width = 2226
integer height = 1152
integer taborder = 10
string title = "none"
string dataobject = "d_prod_cat_maint"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_new();long						ll_row_count


This.InsertRow(0)
ll_row_count = dw_prod_cat_maint.RowCount()

if ll_row_count > 0 Then
	dw_prod_cat_maint.setitem(ll_row_count, "new_row_ind", 'N')
End If


end event

event itemchanged;If row = This.RowCount() Then 
	This.Event ue_new()
End If
end event

event clicked;If This.IsSelected(row) Then
	This.SelectRow(row, False)
Else
	This.SelectRow(row, TRUE)
End If
end event

