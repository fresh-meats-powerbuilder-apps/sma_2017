﻿$PBExportHeader$u_categorydataaccess.sru
forward
global type u_categorydataaccess from nonvisualobject
end type
end forward

global type u_categorydataaccess from nonvisualobject
end type
global u_categorydataaccess u_categorydataaccess

forward prototypes
public function boolean uf_upd_prod_cat_maint (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_inq_prod_cat_maint (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_inq_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_upd_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_upd_prod_cat_maint (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_inq_prod_cat_maint (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_inq_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_upd_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_categorydataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_categorydataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

