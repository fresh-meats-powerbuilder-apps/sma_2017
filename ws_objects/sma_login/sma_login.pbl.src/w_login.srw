﻿$PBExportHeader$w_login.srw
forward
global type w_login from w_abstractresponseext
end type
type p_1 from picture within w_login
end type
type dw_login from datawindow within w_login
end type
type gb_1 from groupbox within w_login
end type
end forward

global type w_login from w_abstractresponseext
int X=293
int Y=148
int Width=1440
int Height=1400
boolean TitleBar=true
string Title="Login"
boolean ControlMenu=false
p_1 p_1
dw_login dw_login
gb_1 gb_1
end type
global w_login w_login

type variables
private:
u_parameterstack	iu_parameterstack
u_errorcontext	iu_errorcontext
u_logincontroller	iu_controller
boolean		ib_ChangingPassword

end variables

on w_login.create
int iCurrent
call super::create
this.p_1=create p_1
this.dw_login=create dw_login
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.p_1
this.Control[iCurrent+2]=this.dw_login
this.Control[iCurrent+3]=this.gb_1
end on

on w_login.destroy
call super::destroy
destroy(this.p_1)
destroy(this.dw_login)
destroy(this.gb_1)
end on

event closequery;Return( 0 )
end event

event ue_postopen;String	ls_UserID, &
			ls_Password
			
Boolean	lb_success

iu_ParameterStack.uf_Pop( 'u_logincontroller', iu_controller )
iu_ParameterStack.uf_Pop( 'u_ErrorContext', iu_ErrorContext )

dw_login.InsertRow( 0 )

// Note: Default object is initialized (if needed) in Login Controller 
// because this (Login) window does not have access to the iu_ClassFactory.

lb_success = iu_controller.uf_GetData( ls_UserID, ls_Password, iu_ErrorContext )

If Not lb_success Then
	// Let login controller display error message
	iu_ParameterStack.uf_Push('u_errorcontext',iu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
End If

dw_login.SetItem(1,"user_name", ls_UserID)
dw_login.SetItem(1,"password", ls_Password)

dw_login.SetFocus()

If Len(Trim(ls_UserID)) > 0 Then
	dw_login.SetColumn("password")
Else
	dw_login.SetColumn("user_name")
End If
end event

event open;call super::open;
/* --------------------------------------------------------
w_wls_login

<OBJECT>	This window is the user interface for logging
			in to the mainframe.  It allows the user to
			change his password when it expires.</OBJECT>
			
<USAGE>	A login controller should get this window from
			the ClassFactory.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */
String	ls_name

iu_ParameterStack = Message.PowerObjectParm

ls_Name = GetApplication().DisplayName
If Len(ls_Name) < 1 Then
	ls_Name = Upper(GetApplication().AppName)
End if

This.Title = ls_name  + " Login"
end event

type cb_help from w_abstractresponseext`cb_help within w_login
int X=1170
int Y=1192
int TabOrder=40
boolean Visible=false
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_login
int X=709
int Y=1128
int Width=457
int TabOrder=30
boolean Cancel=true
end type

event clicked;iu_ErrorContext.uf_SetReturnCode(1)
iu_ParameterStack.uf_Push('u_errorcontext',iu_ErrorContext)
CloseWithReturn(Parent, iu_ParameterStack)
end event

type cb_ok from w_abstractresponseext`cb_ok within w_login
int X=229
int Y=1128
int Width=457
int TabOrder=20
boolean Default=true
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Event script that runs when the OK button
			is pressed.</DESC>

-------------------------------------------------------- */
String	ls_UserID, &
			ls_Password, &
			ls_NewPassword, &
			ls_VerifyPassword

Boolean	lb_success
String	ls_action
String	ls_modify
			
If dw_login.AcceptText() = -1 Then Return	

SetPointer(Hourglass!)

ls_UserID = dw_login.GetItemString(1,"user_name")
ls_Password = dw_login.GetItemString(1,"password")

If Len(Trim(ls_UserID)) > 8 Then
	MessageBox("Cannot Login", "The user ID can only be eight characters or fewer.", Exclamation!)
	dw_login.SetFocus()
	dw_Login.SetColumn("user_name")
	Return
End If

// Make the User ID variable a fixed width eight characters.
ls_UserID = Trim(ls_UserID) //+ Space(8 - Len(Trim(ls_UserID)))

If Len(ls_password) < 5 Then
	MessageBox("Cannot Login","The password you enter must be at least five characters long.", Exclamation!)
	dw_login.SetFocus()
	dw_login.SetColumn("password")
	Return
End If

If ib_ChangingPassword Then
	ls_NewPassword = dw_login.GetItemString(1,"new_password")
	ls_VerifyPassword = dw_login.GetItemString(1,"verify_password")
	If ls_NewPassword <> ls_VerifyPassword Then
		MessageBox("Cannot Login", "The new password was not successfully verified.  Make sure that you type the password twice exactly the same.", Exclamation!)
		dw_login.SetItem(1,"new_password","")
		dw_login.SetItem(1,"verify_password","")		
		dw_login.SetFocus()
		dw_login.SetColumn("new_password")
		Return
	End If
	If Len(Trim(ls_NewPassword)) < 5 Then
		MessageBox("Cannot Login", "The new password must be at least five characters long.  Please enter and verify your new password.", Exclamation!)
		dw_login.SetItem(1,"new_password","")
		dw_login.SetItem(1,"verify_password","")
		dw_login.SetFocus()
		dw_login.SetColumn("new_password")
		Return
	End If
	If ls_NewPassword = ls_Password Then
		MessageBox("Cannot Login", "The new password must be different from the old password.  Please enter and verify your new password.", Exclamation!)
		dw_login.SetItem(1,"new_password","")
		dw_login.SetItem(1,"verify_password","")
		dw_login.SetFocus()
		dw_login.SetColumn("new_password")
		Return
	End If
	lb_success = iu_controller.uf_ChangePassword(ls_userID, ls_Password, ls_NewPassword, iu_ErrorContext, ls_action)
Else
	//Validate login here
	lb_success = iu_controller.uf_Validate(ls_UserID, ls_Password, iu_ErrorContext, ls_action)
End If

If Not iu_ErrorContext.uf_IsSuccessful() Then
	iu_ParameterStack.uf_Push("u_errorcontext", iu_ErrorContext)
	CloseWithReturn(Parent, iu_ParameterStack)
	Return 	
End If

If lb_success Then
	iu_ErrorContext.uf_SetReturnCode(0)
	iu_ParameterStack.uf_Push("u_errorcontext", iu_ErrorContext)
	CloseWithReturn(Parent, iu_ParameterStack)
	Return 
End If

Choose Case ls_action
	Case "X"
		// Password has expired, change password
		// Show "change password" fields and set mode to change password on next call.
		// Also grey out the old userid and password fields.
		MessageBox("Cannot Login","Your password has expired.  Please enter and verify a new password.", Information!)
		gb_1.Height = 432
		dw_login.Height = 360
		ls_modify = "user_name.Background.Mode = 1 user_name.Protect = 1 password.Protect = 1 password.Background.Mode = 1 new_password.Visible = 1 verify_password.Visible = 1 new_password_t.Visible = 1 verify_password_t.Visible = 1"
		ls_modify = dw_login.Modify(ls_modify)
		dw_login.SetTabOrder("new_password", 30)
		dw_login.SetTabOrder("verify_password", 40)
		dw_login.SetItem(1,"new_password","")
		dw_login.SetItem(1,"verify_password","")
		dw_login.SetFocus()
		dw_login.SetColumn("new_password")
		dw_login.SetFocus()
		ib_ChangingPassword = True
		Return
	Case Else
		// Invalid password, notify user
		MessageBox("Cannot Login",iu_ErrorContext.uf_GetText(), Exclamation!)
		iu_ErrorContext.uf_Initialize()
		dw_login.SetFocus()
		dw_login.SetColumn("password")
		Return			
End Choose

iu_ParameterStack.uf_Push("u_errorcontext", iu_ErrorContext)
CloseWithReturn(Parent, iu_ParameterStack)
Return 

end event

type p_1 from picture within w_login
int X=247
int Y=104
int Width=896
int Height=508
boolean Enabled=false
boolean BringToTop=true
string PictureName="ibp3d.bmp"
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
end type

type dw_login from datawindow within w_login
int X=306
int Y=688
int Width=786
int Height=188
int TabOrder=10
boolean BringToTop=true
string DataObject="d_login"
boolean Border=false
end type

event itemerror;String	ls_field

Choose Case String(dwo.Name)
	Case "userid"
		ls_field = "User ID"
	Case "password"
		ls_field = "password"
	Case "new_password"
		ls_field = "new password"
	Case "verify_password"
		ls_field = "new password"
	Case Else
		Return 1
End Choose

If Len(data) > 8 Then
	MessageBox("Invalid Entry", "The " + ls_field + " must be eight characters or fewer.", Exclamation!)
	dw_login.SelectText(1,10000)
	dw_login.SetFocus()
	Return 1			
End If		

end event

type gb_1 from groupbox within w_login
int X=256
int Y=636
int Width=896
int Height=272
string Text="Login"
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

