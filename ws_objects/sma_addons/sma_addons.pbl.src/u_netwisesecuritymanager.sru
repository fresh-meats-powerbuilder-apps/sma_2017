﻿$PBExportHeader$u_netwisesecuritymanager.sru
$PBExportComments$Requires u_utl001 and Netwise
forward
global type u_netwisesecuritymanager from u_abstractsecuritymanager
end type
end forward

global type u_netwisesecuritymanager from u_abstractsecuritymanager
end type
global u_netwisesecuritymanager u_netwisesecuritymanager

type variables
Protected:
Boolean		ib_DefaultInquire = True, &
		ib_DefaultModify = True, &
		ib_DefaultDelete = True, &
		ib_DefaultAdd = True

DataStore	ids_Windows

String		is_GroupID
end variables

forward prototypes
public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getpermissions (string as_objectname, ref boolean ab_inquire, ref boolean ab_modify, ref boolean ab_add, ref boolean ab_delete, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setmenupermissions (ref menu am_menu, ref u_abstracterrorcontext au_errorcontext)
protected subroutine uf_checkmenuitem (menu am_menu)
end prototypes

public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize()

<DESC> Initialize the Netwise Security System
</DESC>

<ARGS>	au_classfactory: ClassFactory
			au_errorcontext: ErrorContext
</ARGS>

<USAGE> This function will connect to the mainframe and 
			internally hold the permissions for all objects
</USAGE>
-------------------------------------------------------- */

String						ls_UserID, &
								ls_Password, &
								ls_Data, &
								ls_GroupID
								
u_utl001						lu_Utl001
u_user		lu_User


If Not au_ClassFactory.uf_GetObject("u_user", lu_user, au_ErrorContext) Then
	If Not au_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(au_ClassFactory, au_ErrorContext) Then return False
End if
	
lu_User.uf_retrieve(ls_userid, ls_password)
ls_userid = Trim(ls_Userid)
ls_Password = Trim(ls_Password)
is_GroupID = lu_User.uf_GetGroupID(au_ErrorContext)

If Not au_ErrorContext.uf_IsSuccessful() Then Return False

If Not au_ClassFactory.uf_GetObject("u_utl001", lu_Utl001, au_ErrorContext) Then
	If Not au_ErrorContext.uf_IsSuccessful() Then return False
	If Not lu_utl001.uf_Initialize(au_ClassFactory, ls_UserID, ls_Password, au_ErrorContext) Then Return False
End if

//lu_utl001.uf_utlu02ar_GetWindowAccess("u_NetwiseSecurityManager", &
//													"uf_initialize", &
//													"", &
//													ls_userid, &
//													ls_Data, &
//													au_ErrorContext)

If Not au_ErrorContext.uf_IsSuccessful() Then return False

ids_Windows = Create DataStore
ids_Windows.DataObject = 'd_windowsecurity'

ids_Windows.ImportString(ls_Data)

return True
end function

public function boolean uf_getpermissions (string as_objectname, ref boolean ab_inquire, ref boolean ab_modify, ref boolean ab_add, ref boolean ab_delete, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_GetPermissions

<DESC> Get the four permissions for an object
</DESC>

<ARGS>    as_objectname: Object to check
			ab_inquire: True if inquire is accessable
			ab_modify: True if modify is accessable
			ab_add: True if add is accessable
			ab_delete: True if delete is accessable
			au_errorcontext: Holds message if error occurs
</ARGS>

<USAGE> Pass an object name to this function.  It will
			return the the permissions for the four booleans.
</USAGE>
-------------------------------------------------------- */

Long		ll_Find
String	ls_Value

ll_Find = ids_windows.Find("(lower(window_name) = lower('" + as_objectname + "' ) or lower(menuid) = lower('" + &
						as_objectname + "')) and groupid = '" + is_groupid + "'", 1, ids_Windows.RowCount())
If ll_Find > 0 Then
	ls_Value = ids_Windows.GetItemString(ll_Find, 'inq_auth')
	If ls_Value = 'Y' Then
		ab_inquire = True
	Else
		ab_inquire = False
	End if

	ls_Value = ids_Windows.GetItemString(ll_Find, 'add_auth')
	If ls_Value = 'Y' Then
		ab_add = True
	Else
		ab_add = False
	End if

	ls_Value = ids_Windows.GetItemString(ll_Find, 'modify_auth')
	If ls_Value = 'Y' Then
		ab_modify = True
	Else
		ab_modify = False
	End if

	ls_Value = ids_Windows.GetItemString(ll_Find, 'delete_auth')
	If ls_Value = 'Y' Then
		ab_delete = True
	Else
		ab_delete = False
	End if
Else
	ab_inquire = ib_defaultinquire
	ab_modify = ib_defaultmodify
	ab_add = ib_defaultadd
	ab_delete = ib_defaultdelete
End if

return true
end function

public function boolean uf_setmenupermissions (ref menu am_menu, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_setmenuPermissions

<DESC> This function will loop through an entire menu and 
			disable all items where the user cannot use add, inquire
			update or delete.
</DESC>

<ARGS>    am_menu: Menu to check permissions on
			au_errorcontext: ErrorContext to contain Error Messages
</ARGS>

<USAGE> Call this function to disable all menu items in
			a menu that the user does not have permissions to use.
			The default values for inquire, modify, add, and delete 
			are true, meaning accessable.
</USAGE>
-------------------------------------------------------- */

Int	i, Maxi, &
		j, Maxj, &
		k, Maxk


ids_windows.SetFilter("groupid = '" + is_groupid + "'")
ids_Windows.Filter()

//If there aren't any rows left, and the default is not all protected, bail
If ids_Windows.RowCount() < 1 And &
		(ib_defaultadd or ib_defaultdelete or ib_defaultinquire or ib_defaultmodify) Then return True

maxi = UpperBound(am_menu.Item)
For i = 1 to Maxi
	//Check the status of am_menu.Item[i] here
	This.uf_CheckMenuItem(am_menu.Item[i])
	
	Maxj = UpperBound(am_menu.Item[i].Item)
	For j = 1 to Maxj
		// Check the status of am_menu.Item[i].Item[j] here
		This.uf_CheckMenuItem(am_menu.Item[i].Item[j])
		
		Maxk = UpperBound(am_menu.Item[i].Item[j].Item)
		For k = 1 to Maxk
			// Check the status of am_menu.Item[i].Item[j].Item[k] here
			This.uf_CheckMenuItem(am_menu.Item[i].Item[j].Item[k])			
		Next
	Next
Next

ids_Windows.SetFilter("")
ids_Windows.Filter()

return true
end function

protected subroutine uf_checkmenuitem (menu am_menu);
/* --------------------------------------------------------
uf_checkMenu

<DESC> This is a private function.  Don't worry what it does
</DESC>

<ARGS>    am_menu: Menuitem to check
</ARGS>

<USAGE> Pass a menuitem to check.  This function will look
			in the datastore and if all four indicators are
			false, it will disable the item.
</USAGE>
-------------------------------------------------------- */
Boolean	lb_Add, &
			lb_Modify, &
			lb_Delete, &
			lb_Inquire
			
Long		ll_Row

String	ls_value, &
			ls_ClassName


ls_ClassName = am_menu.Classname()
ll_Row = ids_windows.Find("lower(Menuid) = lower('" + ls_ClassName + "')", 1, 10000)

If ll_Row > 0 Then
	ls_Value = ids_Windows.GetItemString(ll_Row, 'inq_auth')
	If ls_Value = 'Y' Then
		lb_inquire = True
	Else
		lb_inquire = False
	End if

	ls_Value = ids_Windows.GetItemString(ll_Row, 'add_auth')
	If ls_Value = 'Y' Then
		lb_add = True
	Else
		lb_add = False
	End if

	ls_Value = ids_Windows.GetItemString(ll_Row, 'modify_auth')
	If ls_Value = 'Y' Then
		lb_modify = True
	Else
		lb_modify = False
	End if

	ls_Value = ids_Windows.GetItemString(ll_Row, 'delete_auth')
	If ls_Value = 'Y' Then
		lb_delete = True
	Else
		lb_delete = False
	End if
Else
	lb_Add = ib_defaultadd
	lb_modify = ib_defaultmodify
	lb_delete = ib_defaultdelete
	lb_inquire = ib_defaultinquire
End if

If Not (lb_Add or lb_Modify or lb_delete or lb_Inquire) Then
	am_menu.Disable()
End if

return 
end subroutine

on u_netwisesecuritymanager.create
call super::create
end on

on u_netwisesecuritymanager.destroy
call super::destroy
end on

event constructor;call super::constructor;
/* --------------------------------------------------------
u_NetwiseSecurityManager

<OBJECT> This object maintains all data data to manage object
			security.  It retrieves data from Netwise call utlu00ar 
			and utlu02ar. Use this object to determine the permissions 
			on any object.			
</OBJECT>

<USAGE> Create this function and initialize it.  You can then 
			protect menus and check the security of other objects.
</USAGE>

<AUTH> Tim Bornholtz </AUTH>
--------------------------------------------------------- */


end event

