﻿$PBExportHeader$u_registryservice.sru
forward
global type u_RegistryService from NonVisualObject
end type
end forward

global type u_RegistryService from NonVisualObject
end type
global u_RegistryService u_RegistryService

type variables
Boolean	ib_FileExists
String	is_FileName,&
	is_SectionName

u_AbstractClassFactory	iu_ClassFactory

end variables

forward prototypes
public function boolean uf_getdata (string as_key, ref string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, integer ai_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref long al_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, long al_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, real ar_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, date ad_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, datetime adt_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, time at_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref real ar_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref decimal ade_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref integer ai_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref time at_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, boolean ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, decimal adc_value, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_createfile (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref date ad_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_section, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref double adb_doublevalue, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_SetData (string as_key, Double adc_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref string as_value[], ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_SetData (string as_key, string as_value[], ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref blob ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref blob ab_value, Boolean ab_Unicode, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_SetData (string as_key, Blob ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_SetData (string as_key, blob ab_value, Boolean ab_Unicode, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref string as_value, boolean ab_unicode, boolean ab_environmentvariable, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, string as_value, boolean ab_Unicode, boolean ab_EnvironmentVariable, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_getdata (string as_key, ref string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a string value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)

IF ll_rtn = -1  Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	as_value = ""
	Return False
END IF	
as_Value = ls_Temp
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_setdata (string as_key, string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set String value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: String value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a String value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

ls_Temp = String(as_value)

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_setdata (string as_key, integer ai_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set an integer value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Integer value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a integer value in the registry
			Only Positive integers can be stored in the registry. If you need to 
			store a negative number, you must convert them to a a Real, Double,String,or Decimal.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */

Long		ll_rtn
uLong		lul_Value

IF ai_value < 0 then 
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + "Only positve integers numbers can be stored.")
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
END IF


lul_Value = ai_value
ll_rtn  = RegistrySet(is_sectionname, &
							as_key, Regulong!, lul_Value)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_getdata (string as_key, ref long al_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: Where the value will be placed Passed by ref
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Long value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_Rtn
uLong	lul_Long

ll_rtn = RegistryGet(is_SectionName, as_Key, ReguLong!,lul_Long)

IF ll_rtn = -1 Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry " )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	
al_value = lul_Long
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_setdata (string as_key, long al_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set a Long value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Long value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Long value in the registry
			Only Positive longs can be stored in the registry. If you need to 
			store a negative number, you must convert them to a a Real, Double,String,or Decimal.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */

Long		ll_rtn
uLong		lul_Value

lul_Value = al_value

IF al_value < 0 then 
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + "Only positve integers numbers can be stored.")
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
END IF

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, Regulong!, lul_Value)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_setdata (string as_key, real ar_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Real value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Real value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Real value in the registry
			Real values are converted to strings formated in the system's default format and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

ls_Temp = String(ar_value)

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_setdata (string as_key, date ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Date value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Date value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Date value in the registry
			Date values are converted to strings in the format "mm/dd/yyyy" and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn



ls_temp = String(ad_value,"mm/dd/yyyy")



ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True








end function

public function boolean uf_setdata (string as_key, datetime adt_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set DateTime value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: DateTime value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a DateTime value in the registry
			Date values are converted to strings formated in the system's default format and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn



ls_Temp = String(adt_value)

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_setdata (string as_key, time at_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Time value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: time value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Real value in the registry
			time values are converted to strings formated in the system's default format and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

ls_Temp = String(at_value)

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_getdata (string as_key, ref real ar_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get a Real value stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ar_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a real value stored in the registry.
			Real Values will be converted to stirngs and then stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_Rtn


ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)

IF ll_rtn = -1 Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry " )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	
ar_value = Real(ls_Temp)
au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_getdata (string as_key, ref decimal ade_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get decimal stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ade_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a decimal value stored in an INI file.
			If this function returns false, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_Rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, ReguLong!,ls_Temp)

ade_Value = Dec(ls_Temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry " )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End IF
IF ls_Temp = "Not Found" Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_getdata (string as_key, ref integer ai_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ai_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a integer value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_Rtn
uLong		lul_Value

ll_rtn = RegistryGet(is_SectionName, as_Key, ReguLong!,lul_Value)

IF ll_rtn = -1 Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	
ai_value = lul_Value
au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_getdata (string as_key, ref time at_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			at_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a time value stored in an INI file
			Time is stored as a string in the registry, in the following 
			format hh:mm:ss:ffff
			If this function returns false, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)

IF ll_rtn  = -1 Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	
at_Value = Time(ls_Temp)
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get datetime value stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			adt_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a DateTime value stored in the registry
			DateTime values are stored as string in the registry in the
			following format mm/dd/yyyy hh:mm:ss:fff
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_Rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)



IF ll_rtn = -1 Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry " )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

adt_value = DateTime(Date(Left(ls_temp,Pos(ls_temp, " ") - 1)),Time(Mid(ls_temp, Pos(ls_temp, " ") + 1)))
au_ErrorContext.uf_SetReturnCode(0)

Return True



end function

public function boolean uf_setdata (string as_key, boolean ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Boolean value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Boolean value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Boolean value in the registry
			Boolean values are converted to strings and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

IF ab_value Then
	ls_Temp = 'True'
Else
	ls_Temp = 'False'
End if


ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True




end function

public function boolean uf_setdata (string as_key, decimal adc_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Decimal value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Decimal value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Decimal value in the registry
			Decimal values are converted to strings formated in the system's default format and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

ls_Temp = String(adc_value)

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

private function boolean uf_createfile (ref u_abstracterrorcontext au_errorcontext);Integer	li_FileHandle,&
			li_BytesWritten

IF ib_fileexists Then Return TRUE

li_FileHandle = FileOpen(is_filename,LineMode!,Write!,LockReadWrite!,Append!)
IF li_FileHandle = -1 OR IsNull(li_FileHandle) Then 
	au_ErrorContext.uf_AppendText("Unable to Create file " + is_filename)
	Return FALSE
END IF
li_BytesWritten = FileWrite(li_FileHandle,"~r~n")

IF li_BytesWritten < 0 Then 
	au_ErrorContext.uf_AppendText("Unable to Write to file "+ is_filename)
	Return False
End if
fileclose(li_FileHandle)
Return TRUE



end function

public function boolean uf_getdata (string as_key, ref date ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get date stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a date value stored in the registry
			Dates are stored as Strings in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_Rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)

IF ll_rtn = -1 Then
	// An error occurred 
		au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" In the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End IF
ad_value = Date(ls_temp)
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_section, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the Default Manager. Set the file to read/write from and 
			sets the section </DESC>

<ARGS>	au_classfactory: Class Factory in case it needs other objects
			as_filename: File to access
			as_section: Section where values will be stored
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to initialize the INI
			defaults manager.  Set the INI file to be used
			as well as the section to read from.</USAGE>

-------------------------------------------------------- */
is_sectionname = as_Section
iu_classfactory = au_ClassFactory
Return True
end function

public function boolean uf_getdata (string as_key, ref double adb_doublevalue, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Double stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: Variable to store the boolean value by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Double value stored in the registry.
			Double Values are stored as Strings in the registry. 
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_rtn
ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)

IF ll_rtn = -1  Then
	// An error occurred 
		au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End IF
adb_Doublevalue = Double(ls_Temp)
au_ErrorContext.uf_SetReturnCode(0)
Return TRUE




end function

public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Boolean stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Boolean value stored in the registry
			Booleans are stored as Strings in the registry. "TRUE" and "T"
			Represent True anything else represents false
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp
Long		ll_Rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegString!,ls_Temp)

IF ll_rtn = -1 Then
	// An error occurred 
		au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" In the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End IF
IF Upper(ls_Temp) = 'TRUE' or Upper(ls_Temp) = 'T' Then
	ab_value = TRUE
ELSE
	ab_value = False
END IF
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_SetData (string as_key, Double adc_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Double value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Double value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Decimal value in the registry
			Double values are converted to strings formated in the system's default format and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

Long		ll_rtn

ls_Temp = String(adc_value)

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_getdata (string as_key, ref string as_value[], ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a string value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp[]

Long		ll_rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegMultiString!,ls_Temp)

IF ll_rtn = -1  Then
	au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" in the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	
as_Value = ls_Temp
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_SetData (string as_key, string as_value[], ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set String value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: String value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a String value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp[]

Long		ll_rtn

ls_Temp = as_value

ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegMultiString!, ls_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True


end function

public function boolean uf_getdata (string as_key, ref blob ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Boolean stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Boolean value stored in the registry
			Booleans are stored as Strings in the registry. "TRUE" and "T"
			Represent True anything else represents false
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Blob	lb_Temp
Long		ll_Rtn

ll_rtn = RegistryGet(is_SectionName, as_Key, RegBinary!,lb_Temp)

IF ll_rtn = -1 Then
	// An error occurred 
		au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" In the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End IF
ab_value = lb_Temp
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_getdata (string as_key, ref blob ab_value, Boolean ab_Unicode, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Boolean stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Boolean value stored in the registry
			Booleans are stored as Strings in the registry. "TRUE" and "T"
			Represent True anything else represents false
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Blob	lb_Temp
Long		ll_Rtn

IF Not ab_Unicode Then 
	Return This.uf_GetData(as_key,ab_value,au_errorcontext)
END IF	

ll_rtn = RegistryGet(is_SectionName, as_Key,RegLink!,lb_Temp)

IF ll_rtn = -1 Then
	// An error occurred 
		au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" In the registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End IF
ab_value = lb_Temp
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_SetData (string as_key, Blob ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Blob value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Blob value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Blob value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Blob	   lb_Temp

Long		ll_rtn

lb_Temp = ab_value
ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegBinary!, lb_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True




end function

public function boolean uf_SetData (string as_key, blob ab_value, Boolean ab_Unicode, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Blob value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Blob value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Blob value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Blob	   lb_Temp

Long		ll_rtn

IF Not  ab_Unicode Then
	Return This.uf_SetData(as_key,ab_value,au_errorcontext)
END IF 


lb_Temp = ab_value
ll_rtn  = RegistrySet(is_sectionname, &
							as_key, RegLink!, lb_Temp)


If ll_rtn = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to registry")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True




end function

public function boolean uf_getdata (string as_key, ref string as_value, boolean ab_unicode, boolean ab_environmentvariable, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: Where the value will be placed by reference
			ab_Unicode:True if you want to retieve a string stored as unicode
			ab_EnvironmentVariable:True if you want to retrieve A null-terminated 
			string that contains unexpanded references to environment variables
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a string value stored in a registry file
			You should only use the unicode option when porting to unicode version of PowerBuilder
			You can not store or get environment variables as unicode, therefore the logical
			'AND' of the unicode variable and the environment varaible must be FALSE.
			Example: uf_GetData(String,True,False,u_errorcontext)	- Valid
						uf_GetData(String,False,True,u_errorcontext)	- Valid
						uf_GetData(String,False,False,u_errorcontext)	- Valid
						uf_GetData(String,True,True,u_errorcontext)	- NOT Valid
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Blob		lb_Temp
String	ls_Temp

Long		ll_rtn


Choose Case True
	Case	(ab_Unicode and ab_EnvironmentVariable)
		au_errorcontext.uf_AppendText ("Error: Unable to read a string as unicode and an unexpanded references to environment variables")
		au_ErrorContext.uf_SetReturnCode(-3)
		Return False
		
	Case (ab_Unicode = False) AND (ab_EnvironmentVariable = False)	
		Return This.uf_GetData(as_key,as_value,au_errorcontext)
		
	Case ab_unicode
		ll_rtn = RegistryGet(is_SectionName, as_Key, RegLink!,ls_Temp)
		IF ll_rtn = -1  Then
			au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
			" in the registry")
			au_ErrorContext.uf_SetReturnCode(-1)
		Return False
		END IF	
		as_Value = String(Blob(ls_Temp,EncodingAnsi!))
	Case ab_environmentvariable
		ll_rtn = RegistryGet(is_SectionName, as_Key, RegExpandString!,ls_Temp)
		IF ll_rtn = -1  Then
			au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
			" in the registry")
			au_ErrorContext.uf_SetReturnCode(-1)
		Return False
		END IF	
		as_Value = ls_Temp
End Choose
au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_setdata (string as_key, string as_value, boolean ab_Unicode, boolean ab_EnvironmentVariable, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: Value to store
			ab_Unicode:True if you want to Store a string as unicode
			ab_EnvironmentVariable:True if you want to store A null-terminated 
			string that contains unexpanded references to environment variables
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to Sets a string value stored in a registry file
			You should only use the unicode option when porting to unicode version of PowerBuilder
			You can not store or get environment variables as unicode, therefore the logical
			'AND' of the unicode variable and the environment varaible must be FALSE.
			Example: uf_SetData(String,True,False,u_errorcontext)	- Valid
						uf_SetData(String,False,True,u_errorcontext)	- Valid
						uf_SetData(String,False,False,u_errorcontext)	- Valid
						uf_SetData(String,True,True,u_errorcontext)	- NOT Valid
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Blob		lb_Temp
String	ls_Temp

Long		ll_rtn


Choose Case True
	Case	(ab_Unicode and ab_EnvironmentVariable)
		au_errorcontext.uf_AppendText ("Error: Unable to read a string as unicode and an unexpanded references to environment variables")
		au_ErrorContext.uf_SetReturnCode(-3)
		Return False
		
	Case (ab_Unicode = False) AND (ab_EnvironmentVariable = False)	
		Return This.uf_SetData(as_key,as_value,au_errorcontext)
		
	Case ab_unicode
//		Uncomment this code if porting to unicode
//		lb_Temp = ToUnicode(Blob(as_value))
//		ll_rtn = RegistrySet(is_SectionName, as_Key, RegLink!,ls_Temp)
//		IF ll_rtn = -1  Then
//		au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
//			" in the registry")
			au_errorcontext.uf_AppendText ("Not unicode Version")			
			au_ErrorContext.uf_SetReturnCode(-1)
//		Return False
//		END IF	
		
	Case ab_environmentvariable
		ls_Temp = as_value
		ll_rtn = RegistrySet(is_SectionName, as_Key, RegExpandString!,ls_Temp)
		IF ll_rtn = -1  Then
			au_errorcontext.uf_AppendText ("Error: Unable to read key " + as_Key + " from section " + is_SectionName + &
			" in the registry")
			au_ErrorContext.uf_SetReturnCode(-1)
		Return False
		END IF	
	End Choose
au_ErrorContext.uf_SetReturnCode(0)
Return True
end function

on u_RegistryService.create
TriggerEvent( this, "constructor" )
end on

on u_RegistryService.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_RegistryService

<OBJECT>	Store and retrieve data in The registry file.</OBJECT>
			
<USAGE>	Initialize this object with a section name.  Call uf_Get() to retrieve
			and uf_Set() to write data in the Registry file.
			If an error occurs, the error context contains
			information about the error: The ReturnCode in the
			error context can be any of the following
			values:
			<LI> 0 - no error
			</USAGE>
			
<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
Return 0

end event

