﻿$PBExportHeader$u_abstractcalendar.sru
forward
global type u_abstractcalendar from nonvisualobject
end type
end forward

global type u_abstractcalendar from nonvisualobject
end type
global u_abstractcalendar u_abstractcalendar

forward prototypes
public function string uf_cancel ()
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref datawindow adw_control, string as_dwname, string as_InitDate, ref u_abstracterrorcontext au_errorcontext)
public function string uf_clicked (long as_row, string as_column)
public function string uf_keyenter ()
public function string uf_display ()
public function string uf_keydownarrow ()
public function string uf_keyleftarrow ()
public function string uf_keypagedown ()
public function string uf_keypageup ()
public function string uf_keyrightarrow ()
public function string uf_keyuparrow ()
public function string uf_next ()
public function string uf_previous ()
end prototypes

public function string uf_cancel ();
/* --------------------------------------------------------

<DESC>	the function is called when the cancel button of w_calendar
			is pressed</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Return a string that you want to represent 'Cancel'</USAGE>
-------------------------------------------------------- */

Return ""
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref datawindow adw_control, string as_dwname, string as_InitDate, ref u_abstracterrorcontext au_errorcontext);Return False
end function

public function string uf_clicked (long as_row, string as_column);
/* --------------------------------------------------------

<DESC>	Function called when a user clicks on the dw_control
			</DESC>

<ARGS>	as_row: Row the user clicked
			as_column: ColumnName
			</ARGS>
			
<USAGE>	If the user object returns a valid date the window will close. 
			</USAGE>
-------------------------------------------------------- */
Return ""
end function

public function string uf_keyenter ();
/* --------------------------------------------------------

<DESC>	Function called when keyenter pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ""
end function

public function string uf_display ();
/* --------------------------------------------------------

<DESC>	Displays the calendar. Returns the title that should 
			be displayed  
			</DESC>

<ARGS>	</ARGS>
			
<USAGE>	w_calendar calls this function to display the 
			appropriate calendar</USAGE>
-------------------------------------------------------- */
Return ""
end function

public function string uf_keydownarrow ();
/* --------------------------------------------------------

<DESC>	Function called when keyarrowdown pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ""
end function

public function string uf_keyleftarrow ();
/* --------------------------------------------------------

<DESC>	Function called when keyleftarrow pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

public function string uf_keypagedown ();
/* --------------------------------------------------------

<DESC>	Function called when keypagedown pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

public function string uf_keypageup ();
/* --------------------------------------------------------

<DESC>	Function called when keypageup pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

public function string uf_keyrightarrow ();
/* --------------------------------------------------------

<DESC>	Function called when keyrightarrow pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

public function string uf_keyuparrow ();
/* --------------------------------------------------------

<DESC>	Function called when keyarrowup pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

public function string uf_next ();
/* --------------------------------------------------------

<DESC>	Function called when "next" button pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

public function string uf_previous ();
/* --------------------------------------------------------

<DESC>	Function called when "Prvious" button pressed. 
			Returns title of Window</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return ''
end function

on u_abstractcalendar.create
TriggerEvent( this, "constructor" )
end on

on u_abstractcalendar.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;/* --------------------------------------------------------
u_AbstractCalendar

<OBJECT>	This object is the the interface definitions for 
			w_calendar.
			</OBJECT>
			
<USAGE>	Concrete descendants of this interface should implement these methods.</USAGE>

<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
end event

