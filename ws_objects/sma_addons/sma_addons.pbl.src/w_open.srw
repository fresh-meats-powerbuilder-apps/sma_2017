﻿$PBExportHeader$w_open.srw
forward
global type w_open from w_abstractresponse
end type
type dw_main from datawindow within w_open
end type
type tab_1 from tab within w_open
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_3 from userobject within tab_1
end type
type tabpage_3 from userobject within tab_1
end type
type tabpage_4 from userobject within tab_1
end type
type tabpage_4 from userobject within tab_1
end type
type tabpage_5 from userobject within tab_1
end type
type tabpage_5 from userobject within tab_1
end type
type tabpage_6 from userobject within tab_1
end type
type tabpage_6 from userobject within tab_1
end type
type tabpage_7 from userobject within tab_1
end type
type tabpage_7 from userobject within tab_1
end type
type tabpage_8 from userobject within tab_1
end type
type tabpage_8 from userobject within tab_1
end type
type tabpage_9 from userobject within tab_1
end type
type tabpage_9 from userobject within tab_1
end type
type tabpage_10 from userobject within tab_1
end type
type tabpage_10 from userobject within tab_1
end type
type tab_1 from tab within w_open
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
tabpage_7 tabpage_7
tabpage_8 tabpage_8
tabpage_9 tabpage_9
tabpage_10 tabpage_10
end type
end forward

global type w_open from w_abstractresponse
integer width = 2057
integer height = 816
string title = "Open Window"
long backcolor = 79416533
event ue_ok ( )
event ue_cancel ( )
dw_main dw_main
tab_1 tab_1
end type
global w_open w_open

type variables
Private:
Boolean			ib_OKToClose
u_AbstractClassFactory	iu_ClassFactory
u_AbstractErrorContext	iu_Errors
u_AbstractParameterStack	iu_ParameterStack
end variables

forward prototypes
private subroutine wf_deny_access ()
private function boolean wf_createerrorcontext ()
end prototypes

event ue_ok();
/* --------------------------------------------------------
ue_ok

<DESC> Close the open dialogue and send notification to the app controller
</DESC>

<USAGE> This event will be triggered when the usser clicks OK
</USAGE>
-------------------------------------------------------- */
Long			ll_Selected

String		ls_return, &
				ls_parameters, &
				ls_window_description, &
				ls_window_type

ll_Selected = dw_main.GetSelectedRow(0)
If ll_Selected < 1 Then ll_Selected = 1

If dw_main.RowCount() >= 1 Then 
	ls_return 					= dw_main.GetItemString(ll_Selected, 'window_name')
	ls_parameters 				= dw_main.GetItemString(ll_Selected, 'parameters')
	ls_window_description 	= dw_main.GetItemString(ll_Selected, 'window_description')
	ls_window_type 			= dw_main.GetItemString(ll_Selected, 'window_type')
ELSE
	ls_return 					= ''
	ls_parameters 				= ''
	ls_window_description 	= ''
	ls_window_type 			= ''
End if

If IsNull(ls_return) 				Then ls_return 				= ''
If IsNull(ls_parameters) 			Then ls_parameters 			= ''
If IsNull(ls_window_description) Then ls_window_description = ''
If IsNull(ls_window_type) 			Then ls_window_type 			= ''

iu_ParameterStack.uf_Initialize()
iu_ParameterStack.uf_Push("String", ls_return + "~t" + &
												ls_parameters + "~t" + &
												ls_window_description + "~t" + &
												ls_window_type)

ib_OKToClose = True
Close(This)
end event

event ue_cancel;
/* --------------------------------------------------------
ue_cancel

<DESC> Close the open window and return an empty string
</DESC>

<USAGE>This event will be triggered when the user selects cancel
</USAGE>
-------------------------------------------------------- */

iu_ParameterStack.uf_Initialize()
iu_ParameterStack.uf_Push("String","")
ib_OKToClose = True
Close(This)
end event

private subroutine wf_deny_access ();// This is a stub function to handle security
end subroutine

private function boolean wf_createerrorcontext ();Boolean	lb_return


If IsValid(iu_Errors) Then return True
If Not IsValid(iu_classfactory) Then return false

iu_ClassFactory.uf_GetObject('u_errorcontext', iu_errors)

Return IsValid(iu_Errors)

end function

on w_open.create
int iCurrent
call super::create
this.dw_main=create dw_main
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
this.Control[iCurrent+2]=this.tab_1
end on

on w_open.destroy
call super::destroy
destroy(this.dw_main)
destroy(this.tab_1)
end on

event ue_postopen;String	ls_DefaultButton, &
			ls_Find

Int		li_ret

Long		ll_row, &
			ll_PageCount, &
			ll_Counter

Window	lw_ActiveSheet


//gw_base_frame.SetMicroHelp("Wait..Checking security")
ll_PageCount = UpperBound(tab_1.Control[])

//Message.ib_netwise_error = FALSE

wf_deny_access()
//lw_ActiveSheet = gw_base_frame.GetActiveSheet()
If IsValid(lw_ActiveSheet) Then
	/*****************************************************************
	**
	** If there is a current sheet, we want the open box to have the 
	** last used button down by default.  'ls_find' is a find statement that
	** searches lds_main to find the button that the window belongs to.  
	** In lds_main, parameters are tacked to the window_name column, not a 
	** separate column, so to find a match, we need to chop off parameters 
	** if they exist.
	**
	******************************************************************/

	ls_find = "Trim( Lower( Left( window_name, Pos( window_name, ',') - " + &
				 "If( Pos( window_name, ',') > 0, 1, 0 - Len( Trim( window_name) ) ) ) ) ) = '" +&
					Lower(Trim(ClassName(lw_ActiveSheet))) + "'"
	ll_row = dw_main.Find( ls_Find, 1, dw_main.RowCount() )
	If ll_row > 0 Then 
		ls_DefaultButton = dw_main.GetItemString(ll_row, 'window_type')
	Else
		If ll_PageCount > 0 Then ls_DefaultButton = tab_1.Control[1].Text
	End if
Else
	If ll_PageCount > 0 Then ls_DefaultButton = tab_1.Control[1].Text
End if


For ll_Counter = 1 to ll_PageCount
	If Lower(ls_DefaultButton) = tab_1.Control[ll_Counter].Text Then
			tab_1.SelectTab(ll_Counter)
	End if
Next

dw_Main.SetRedraw(True)
//gw_base_Frame.SetMicroHelp("Ready")
dw_Main.SetFocus()
//cb_1.SetFocus()
end event

event open;call super::open;
/* --------------------------------------------------------
w_open

<OBJECT> This is a standard File Open Dialogue window.  
			It will display all of the windows that can be opened 
			by your application.  The windows can be organized by 
			groups.
</OBJECT>

<USAGE> Open this window with a parameter stack.  The order
			of the objects is (in Pop order): 
			"u_ClassFactory" of type u_AbstractClassFactory
			"u_AppController" of type u_AbstractController
			u_AppController must be the application controller.
			You must have a datawindow called "d_" + Application.AppName + "open",
			as an example, for wls it should be called "d_wlsopen"
			Here is the layout of the open datawindow
			window_description	string(100)		description of the window
			window_name				string(42)		name of the object to create
			window_type				string(25)		Title of the tab to group by
			sort_by					string(10)		Order of windows within a tab
			parameters				string(255)		Any parameters to pass to the window
			icon						string(255)		Filename of the icon to display on the tabpage
			
			This window will trigger the event "ue_open" on the application controller
			passing it a string which is the window_name + Tab + parameters			
</USAGE>

<AUTH> Tim Bornholtz </AUTH>
--------------------------------------------------------- */





iu_ParameterStack = Message.PowerObjectParm
If Not IsValid(iu_ParameterStack) Then Close(This)

IF iu_ParameterStack.uf_Pop("u_ClassFactory",iu_classfactory) then
	IF Not(iu_ClassFactory.uf_GetObject("u_AbstractErrorContext", iu_errors)) Then 
		IF IsNUll(iu_Errors) Then
			MessageBox("Error","Unable to create error Context")
			iu_ParameterStack.uf_Push("String","")
			CloseWithReturn(this,iu_ParameterStack)
		ELSE
			iu_Errors.uf_Initialize() 
		END IF
	END IF
ELSE
	// Fix This - Add Notification
	MessageBox("Error","Unable to pop the class factory")
	Close(This)
END IF

end event

event close;If Not ib_OKToClose Then
	iu_ParameterStack.uf_Initialize()
	iu_ParameterStack.uf_Push("String","")
End If

Message.PowerObjectParm = iu_ParameterStack
end event

type cb_help from w_abstractresponse`cb_help within w_open
integer x = 1760
integer y = 276
integer taborder = 40
boolean enabled = false
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_open
integer x = 1760
integer y = 152
integer width = 247
integer taborder = 30
boolean cancel = true
end type

event clicked;Parent.Event Trigger ue_cancel()
end event

type cb_ok from w_abstractresponse`cb_ok within w_open
integer x = 1760
integer y = 28
integer taborder = 20
boolean default = true
end type

event clicked;Parent.Event Trigger ue_ok()
end event

type dw_main from datawindow within w_open
event ue_keydown pbm_dwnkey
integer x = 18
integer y = 12
integer width = 1696
integer height = 580
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_open"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_keydown;Long	ll_CurrentRow,&
		ll_MaxRows
		
ll_MaxRows = This.RowCount()		
ll_CurrentRow = This.GetRow()
IF ll_CurrentRow = 0 Then 
	ll_CurrentRow = 1
	IF This.SetRow(ll_CurrentRow) < 1 Then Return
END IF

Choose Case Key
	Case KeyRightArrow!, KeyDownArrow!
		ll_CurrentRow++
	Case KeyLeftArrow!, KeyUpArrow!
		ll_CurrentRow --
	Case KeyHome!
		ll_CurrentRow = 1
	Case KeyEnd!
		ll_CurrentRow = ll_MaxRows
	Case KeyPageUp!
		ll_CurrentRow += - 10
	Case KeyPageDown!
		ll_CurrentRow += 10
End Choose	

IF ll_CurrentRow > ll_MaxRows Then ll_CurrentRow = ll_MaxRows
IF ll_CurrentRow < 1 then ll_CurrentRow = 1

This.SelectRow(0, False)
This.SelectRow( ll_CurrentRow, True)

Return 0
 
end event

event constructor;
String	ls_AppID

//This.is_selection = '1'
ls_Appid = GetApplication().AppName

If IsNull(ls_Appid) Or Len(Trim(ls_Appid)) < 1 Then ls_Appid = ''
This.DataObject = 'd_' + ls_Appid + 'Open'



end event

event doubleclicked;//call super::doubleclicked;
Parent.Event ue_ok()
end event

event clicked;If row > 0 Then 
	This.SelectRow(0, False)
	This.SelectRow(row, True)
End If
end event

type tab_1 from tab within w_open
integer x = 23
integer y = 592
integer width = 1696
integer height = 112
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 79741120
boolean raggedright = true
boolean focusonbuttondown = true
tabposition tabposition = tabsonbottom!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
tabpage_7 tabpage_7
tabpage_8 tabpage_8
tabpage_9 tabpage_9
tabpage_10 tabpage_10
end type

event constructor;Int		li_PageCounter
Long		ll_Row, &
			ll_IconRow, &
			ll_RowCount

String	ls_Find, &
			ls_name, &
			ls_IconFind
	

ll_RowCount = dw_main.RowCount()
If ll_RowCount < 1 Then return
ll_Row = 1
Do
	li_PageCounter ++
	
	If li_PageCounter > UpperBound(This.Control[]) Then return
	
	ls_Name = dw_main.GetItemString(ll_Row, 'window_type')
	
	This.Control[li_PageCounter].Text = ls_Name
	This.Control[li_PageCounter].Visible = True
	
	// Find a row in this DW where the window_type is what we 
	// just found and the icon is filled in
	ls_IconFind = "window_type = '" + ls_Name + "' and Len(icon) > 0"
//	MessageBox("Find Icon", ls_IconFind)
	ll_IconRow = dw_main.Find(ls_IconFind, 1, ll_RowCount)
	If ll_IconRow > 0 Then
		This.Control[li_PageCounter].PictureName = dw_Main.GetItemString(ll_IconRow, 'icon')
	End if
	
	If Len(ls_Find) > 0 Then ls_find += ' and '
	ls_Find += 'window_type <> "' + ls_Name + '"'
	
	ll_Row = dw_main.Find(ls_find, 1, dw_main.RowCount())
Loop While ll_Row > 0

end event

event selectionchanged;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("Lower(window_type) = '" + Trim(Lower(tab_1.Control[newindex].Text)) + "'")
dw_main.Filter()
//If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(0, False)
	dw_main.SelectRow(1, True)
//End If
dw_main.SetRedraw(True)
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.tabpage_6=create tabpage_6
this.tabpage_7=create tabpage_7
this.tabpage_8=create tabpage_8
this.tabpage_9=create tabpage_9
this.tabpage_10=create tabpage_10
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5,&
this.tabpage_6,&
this.tabpage_7,&
this.tabpage_8,&
this.tabpage_9,&
this.tabpage_10}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
destroy(this.tabpage_6)
destroy(this.tabpage_7)
destroy(this.tabpage_8)
destroy(this.tabpage_9)
destroy(this.tabpage_10)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = " "
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_3 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_4 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_5 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_6 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_7 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_8 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_9 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

type tabpage_10 from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 1659
integer height = -4
long backcolor = 79416533
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 79416533
long picturemaskcolor = 536870912
end type

