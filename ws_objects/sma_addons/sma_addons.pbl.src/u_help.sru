﻿$PBExportHeader$u_help.sru
forward
global type u_help from nonvisualobject
end type
end forward

global type u_help from nonvisualobject
end type
global u_help u_help

type prototypes
private:
function long WinHelpA(long hWndMain, string lpszHelp, uint uCommand, ref string dwData) library "user32.dll" alias for "WinHelpA;Ansi"
end prototypes

type variables
private:
long	il_window_handle
string	is_helpfile
end variables

forward prototypes
public function boolean uf_closehelp ()
public function boolean uf_contents ()
public function boolean uf_initialize (window aw_window, string as_helpfile)
public function boolean uf_keyword (string as_keyword)
end prototypes

public function boolean uf_closehelp ();
/* --------------------------------------------------------
uf_CloseHelp()

<DESC>	Close the help system for a given window.
</DESC>

<USAGE>	Call this function to tell the Windows Help
			system that the application no longer needs
			its services.  Windows Help will only stay
			active as long as applications request its
			services.  This function also has the effect
			of closing any open Help windows.
</USAGE>
-------------------------------------------------------- */
String	ls_null

SetNull(ls_null)

If il_window_handle <= 0 Then Return False
If Len(is_helpfile) = 0 Then Return False

WinHelpA(il_window_handle, is_helpfile, 2, ls_null)

Return True
end function

public function boolean uf_contents ();
/* --------------------------------------------------------
uf_contents()

<DESC>	Display help contents.
</DESC>

<USAGE>	Call this function to display the help contents.
</USAGE>
-------------------------------------------------------- */
String ls_null

SetNull(ls_null)

If il_window_handle <= 0 Then Return False
If Len(is_helpfile) = 0 Then Return False

WinHelpA(il_window_handle, is_helpfile, 11, ls_null)

Return True
end function

public function boolean uf_initialize (window aw_window, string as_helpfile);
/* --------------------------------------------------------
uf_initialize()

<DESC> Initialize the help system for a window.
</DESC>

<ARGS>	aw_window: The window that Help should associate with
			as_helpfile: The Windows Help compiled .HLP file
</ARGS>

<USAGE>	For a window, initialize the help system, call
			other help functions on this object, then close
			the help system with uf_CloseHelp().
</USAGE>
-------------------------------------------------------- */

il_window_handle = Handle(aw_window)
is_helpfile = as_helpfile

Return True
end function

public function boolean uf_keyword (string as_keyword);
/* --------------------------------------------------------
uf_keyword()

<DESC>	Display help for a specified keyword.
</DESC>

<USAGE>	Call this function to display the help for a given
			keyword.  Typically, the keyword will be a sheet's
			title.
</USAGE>
-------------------------------------------------------- */
If il_window_handle <= 0 Then Return False
If Len(is_helpfile) = 0 Then Return False

WinHelpA(il_window_handle, is_helpfile, 257, ref as_keyword)

Return True
end function

on u_help.create
TriggerEvent( this, "constructor" )
end on

on u_help.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_help

<OBJECT>	This object implements a Windows Help service.
</OBJECT>

<USAGE>	Initialize this object, call help functions, and
			finally call uf_CloseHelp() or close the
			associated window to finish using help services.
</USAGE>

<AUTH>	Conrad Engel</AUTH>
--------------------------------------------------------- */

end event

