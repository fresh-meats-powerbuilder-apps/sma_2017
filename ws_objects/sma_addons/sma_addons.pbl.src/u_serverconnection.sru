﻿$PBExportHeader$u_serverconnection.sru
forward
global type u_serverconnection from nonvisualobject
end type
end forward

global type u_serverconnection from nonvisualobject
end type
global u_serverconnection u_serverconnection

type variables
u_abstractclassfactory	iu_classfactory
String	is_driver, &
	is_application, &
	is_location, &
	is_defaultlocation
end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_connect (ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize()

<DESC>	Initialize the server connection object (this does
			NOT connect to the server).</DESC>

<ARGS>	au_classfactory: Class Factory
			au_errorcontext: Error Context</ARGS>

<USAGE>	Call this function after obtaining a reference
			to it from the class factory.  This function
			reads IBP002.INI, section "Distributed Services"
			to obtain "Driver" and "Application".
			The function then matches the first two
			characters of the user ID to a particular location
			by using the keys "MaxLocations", "UserIDPrefix<n>",
			and "Location<n>", where <n> is an integer from 1
			to "MaxLocations".  
			A "DefaultLocation" is obtained also.</USAGE>
-------------------------------------------------------- */

u_abstractdefaultmanager lu_defaults
Boolean	lb_successful, &
			lb_initialized, &
			lb_match = FALSE
u_user	lu_user
String	ls_userid, &
			ls_password, &
			ls_prefix
Long		ll_numlocations, &
			ll_counter

iu_classfactory = au_classfactory

lb_initialized = iu_classfactory.uf_GetObject("u_user", lu_user, au_errorcontext)
If Not lb_initialized Then
	au_errorcontext.uf_AppendText("Unable to obtain user information")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If
lu_user.uf_retrieve(ls_userid, ls_password)

iu_classfactory.uf_GetObject("u_inidefaultmanager", lu_defaults, au_errorcontext)
If Not au_errorcontext.uf_IsSuccessful() Then
	// Populate error context and return False
	// Since there are no settings for this that we can hard-code
	au_errorcontext.uf_AppendText("Unable to obtain default manager")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If

lu_defaults.uf_Initialize(iu_classfactory, "IBP002.INI", "Distributed Services", au_errorcontext)
If Not au_errorcontext.uf_IsSuccessful() Then Return False
lu_defaults.uf_GetData("Driver",is_driver, au_errorcontext)
If Not au_errorcontext.uf_IsSuccessful() Then Return False
lu_defaults.uf_GetData("Application",is_application, au_errorcontext)
If Not au_errorcontext.uf_IsSuccessful() Then Return False
lu_defaults.uf_GetData("DefaultLocation",is_defaultlocation, au_errorcontext)
If Not au_errorcontext.uf_IsSuccessful() Then Return False
lu_defaults.uf_GetData("MaxLocations", ll_numlocations, au_errorcontext)

For ll_counter = 1 to ll_numlocations
	lu_defaults.uf_GetData("UserIDPrefix" + String(ll_counter), ls_prefix, au_errorcontext)
	If Not au_errorcontext.uf_IsSuccessful() Then Continue
	If Lower(Left(ls_prefix,2)) = Lower(Left(ls_userid,2)) Then
		// We have a match on the first two characters of the user's ID
		lu_defaults.uf_GetData("Location" + String(ll_counter), is_location, au_errorcontext)
		If Not au_errorcontext.uf_IsSuccessful() Then Exit
		lb_match = TRUE
	End If
Next

If Not lb_match Or Len(Trim(is_location)) = 0 Then
	// Take the default location (specified in DefaultLocation, if you can believe that)
	is_location = is_defaultlocation
End If

If Len(Trim(is_driver)) = 0 Or Len(Trim(is_application)) = 0 Or Len(Trim(is_location)) = 0 Then
	// Populate error context and return False
	au_ErrorContext.uf_AppendText("Invalid distributed connection settings from defaults")
	au_ErrorContext.uf_Set("Driver",is_driver)
	au_ErrorContext.uf_Set("Application", is_application)
	au_ErrorContext.uf_Set("Location", is_location)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

Return True
end function

public function boolean uf_connect (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_connect()

<DESC>	Connect to a Distributed PowerBuilder server
			application.</DESC>

<ARGS>	au_errorcontext: Error Context</ARGS>

<USAGE>	Call this function to attempt a connection to
			the DPB server.  If the connect fails, the
			function attempts a connection to the default
			location.  If that connect failes, the
			error context contains the reason for the
			failure.  If the connect succeeds, the class
			factory is updated with the connection
			information.</USAGE>
-------------------------------------------------------- */
Connection	lu_connection
Long	ll_Result
iu_classfactory.uf_GetObject("connection", lu_connection, au_errorcontext)

lu_connection.Driver = is_driver
lu_connection.Application = is_application
lu_connection.Location = is_location

ll_result = lu_connection.ConnectToServer()
If ll_result <> 0 Then
	If is_location <> is_defaultlocation And Len(Trim(is_defaultlocation)) > 0 Then
		au_errorcontext.uf_AppendText("Distributed service error: attempting default server location")
		lu_connection.Location = is_defaultlocation
		ll_result = lu_connection.ConnectToServer()
	End If
End If

If ll_result <> 0 Then
	au_errorcontext.uf_AppendText("Distributed service error: " + lu_connection.ErrText)
	au_errorcontext.uf_Set("Distributed Service Error", lu_connection.ErrCode)
	au_errorcontext.uf_Set("Application", lu_connection.Application)
	au_errorcontext.uf_Set("Driver", lu_connection.Driver)
	au_errorcontext.uf_Set("Location", lu_connection.Location)
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If

iu_classfactory.uf_SetConnection(lu_connection)

Return True
end function

on u_serverconnection.create
TriggerEvent( this, "constructor" )
end on

on u_serverconnection.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_serverconnection

<OBJECT>	This object reads connection information for a
			Distributed PowerBuilder server application and
			connects a client application to the server.
</OBJECT>

<USAGE>	Initialize this object after obtaining it from
			the class factory.  Then call uf_connect() to
			connect to the server.
</USAGE>

<AUTH>	Conrad Engel</AUTH>
--------------------------------------------------------- */

end event

