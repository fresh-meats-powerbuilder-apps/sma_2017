﻿$PBExportHeader$w_about.srw
forward
global type w_about from Window
end type
type cb_ok from commandbutton within w_about
end type
type p_1 from picture within w_about
end type
type mle_environment from multilineedit within w_about
end type
end forward

type os_memorystatus from structure
	long		ul_length
	long		ul_memoryload
	long		ul_totalphys
	long		ul_availphys
	long		ul_totalpagefile
	long		ul_availpagefile
	long		ul_totalvirtual
	long		ul_availvirtual
end type

global type w_about from Window
int X=142
int Y=160
int Width=1755
int Height=1456
boolean TitleBar=true
long BackColor=79741120
boolean ControlMenu=true
WindowType WindowType=response!
cb_ok cb_ok
p_1 p_1
mle_environment mle_environment
end type
global w_about w_about

type prototypes
function long GetComputerNameA( ref String lpBuffer, ref long nSize) library "kernel32.dll" alias for "GetComputerNameA;Ansi"
function long GetUserNameA(ref String lpBuffer, ref long nSize) library "advapi32.dll" alias for "GetUserNameA;Ansi"
subroutine GlobalMemoryStatus(ref os_memorystatus memorystatus) library "kernel32.dll" alias for "GlobalMemoryStatus;Ansi"
function long GetModuleFileNameA(long hinst, ref string filename, long namesize) library "kernel32" alias for "GetModuleFileNameA;Ansi"

end prototypes

forward prototypes
public subroutine wf_getenvironment ()
end prototypes

public subroutine wf_getenvironment ();Environment	lu_Environment

String		ls_filename
String		ls_computer
String		ls_memory
String		ls_filepath
Long			ll_length

os_memorystatus	lstr_memorystatus

ls_computer = Space(50)
ll_length = 50

Constant Date ld_date = Today()
Constant Time lt_time = Now()

GetEnvironment(lu_Environment)

Choose Case lu_Environment.OSType
	Case Windows!
		If lu_Environment.Win16 Then
			mle_environment.Text = "PowerBuilder 16-bit"
		Else
			mle_environment.Text = "PowerBuilder 32-bit"
		End If

		If lu_Environment.OSMajorRevision = 4 Or &
				lu_Environment.OSMinorRevision = 95 Then
			mle_environment.Text += "~r~nRunning on Windows 95"
		Else
			mle_environment.Text += "~r~nRunning on Windows " + String(lu_Environment.OSMajorRevision) + &
								"." + String(lu_Environment.OSMinorRevision)
		End If
	Case windowsnt!
		If lu_Environment.Win16 Then
			mle_environment.Text = "PowerBuilder 16-bit"
		Else
			mle_environment.Text = "PowerBuilder 32-bit"
		End If
	
		mle_environment.Text += "~r~nRunning on Windows NT " + String(lu_Environment.OSMajorRevision) + &
							"." + String(lu_Environment.OSMinorRevision)							
	Case sol2!
		mle_environment.Text = "PowerBuilder UNIX"
		mle_environment.Text += "~r~nRunning on Solaris " +  + String(lu_Environment.OSMajorRevision) + &
							"." + String(lu_Environment.OSMinorRevision)	
	Case macintosh!
		mle_environment.Text = "PowerBuilder Macintosh"
		mle_environment.Text += "~r~nRunning on MacOS " +  + String(lu_Environment.OSMajorRevision) + &
							"." + String(lu_Environment.OSMinorRevision)	
End Choose

Choose Case lu_Environment.PBType
	Case desktop!
		mle_environment.Text += "~r~nPowerBuilder Desktop Version " + String(lu_Environment.PBMajorRevision) + &
						"." + String(lu_Environment.PBMinorRevision) + &
						"." + String(lu_Environment.PBFixesRevision)
	Case Enterprise!	
		mle_environment.Text += "~r~nPowerBuilder Enterprise Version " + String(lu_Environment.PBMajorRevision) + &
						"." + String(lu_Environment.PBMinorRevision) + &
						"." + String(lu_Environment.PBFixesRevision)
End Choose

GetComputerNameA(ls_computer, ll_length)
mle_environment.Text += "~r~nComputer Name: " + ls_computer
ls_computer = Space(50)
ll_length = 50
GetUserNameA(ls_computer, ll_length)
mle_environment.Text += "~r~nUser Name: " + Upper(ls_computer)

ls_filepath = Space(128)
If GetModuleFileNameA(Handle(GetApplication()), ls_filepath, 128) > 0 Then
	mle_Environment.Text += "~r~nModule Filename: " + ls_filepath
End If

GlobalMemoryStatus(lstr_memorystatus)
ls_memory = String(lstr_memorystatus.ul_availphys/1024) + "/" + &
	String(lstr_memorystatus.ul_totalphys/1024)
mle_Environment.Text += "~r~nPhysical Memory(Free/Total): " + ls_memory

ls_memory = String(lstr_memorystatus.ul_availvirtual/1024) + "/" + &
	String(lstr_memorystatus.ul_totalvirtual/1024)
mle_Environment.Text += "~r~nVirtual Memory(Free/Total): " + ls_memory

mle_environment.Text += "~r~nVideo: " + String(lu_Environment.screenwidth) + "x" + &
	String(lu_Environment.screenheight) + "x" + String(lu_Environment.Numberofcolors)
	
mle_environment.Text += "~r~nRelease Date/Time: " + String(ld_date) + " " + String(lt_time)

If lu_Environment.MachineCode Then
	mle_environment.Text += "~r~nCompiled Code"
Else
	mle_environment.Text += "~r~nP-Code"
End If

//mle_Environment.Text += "~r~nExecution Context: "
//
//ContextInformation	lu_Context
//String					ls_Stuff
//OLEObject				ole1
//PowerObject				po1
//
//this.GetContextService("ContextInformation",  lu_Context)
//lu_Context.GetCompanyName(ls_Stuff)
//mle_environment.Text += "~r~n  " + ls_Stuff
//lu_Context.GetName(ls_Stuff)
//mle_environment.Text += "~r~n  " + ls_Stuff
//lu_Context.GetVersionName(ls_Stuff)
//mle_environment.Text += "~r~n  Version " + ls_Stuff
//
//If lu_Context.GetHostObject(po1) = 1 Then
//	ole1 = po1
//	mle_environment.Text += "~r~nHost Location: " + ole1.LocationURL
//	mle_environment.Text += "~r~nHost Document Type: " + ole1.Type
//Else
//	mle_environment.Text += "~r~nHost Information Unknown"
//End If

Return
end subroutine

on w_about.create
this.cb_ok=create cb_ok
this.p_1=create p_1
this.mle_environment=create mle_environment
this.Control[]={this.cb_ok,&
this.p_1,&
this.mle_environment}
end on

on w_about.destroy
destroy(this.cb_ok)
destroy(this.p_1)
destroy(this.mle_environment)
end on

event open;String	ls_title
u_AbstractParameterStack	lu_Args

lu_Args = Message.PowerObjectParm

lu_Args.uf_Pop("string",ls_Title)

This.Title = "About " + ls_Title

This.wf_GetEnvironment()
end event

type cb_ok from commandbutton within w_about
int X=1335
int Y=20
int Width=370
int Height=108
int TabOrder=10
string Text="&OK"
boolean Default=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Close(Parent)
end event

type p_1 from picture within w_about
int X=23
int Y=16
int Width=1280
int Height=652
string PictureName="ibp3d.bmp"
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
boolean OriginalSize=true
end type

type mle_environment from multilineedit within w_about
int X=23
int Y=688
int Width=1280
int Height=652
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean AutoHScroll=true
boolean AutoVScroll=true
boolean DisplayOnly=true
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

