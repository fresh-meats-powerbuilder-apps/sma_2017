﻿$PBExportHeader$w_graph_control.srw
$PBExportComments$Window that encapsulated u_graph_control
forward
global type w_graph_control from Window
end type
type uo_control from u_graph_control within w_graph_control
end type
end forward

global type w_graph_control from Window
int X=0
int Y=0
int Width=2926
int Height=404
boolean TitleBar=true
string Title="Graph Control"
WindowType WindowType=popup!
uo_control uo_control
end type
global w_graph_control w_graph_control

forward prototypes
public subroutine wf_initialize (datawindow adw_graph, string as_name)
end prototypes

public subroutine wf_initialize (datawindow adw_graph, string as_name);uo_control.uf_Initialize(adw_Graph, as_Name)
end subroutine

on w_graph_control.create
this.uo_control=create uo_control
this.Control[]={this.uo_control}
end on

on w_graph_control.destroy
destroy(this.uo_control)
end on

event open;
/* --------------------------------------------------------
w_graph_control

<OBJECT> This is a wrapper object for <a href="u_graph_control.htm">u_graph_control</a>
</OBJECT>

<USAGE> Open this object and initiaize it with a datawindow
			and the name of the graph (gr_1)
</USAGE>

<AUTH> Tim Bornholtz</AUTH>
--------------------------------------------------------- */

end event

type uo_control from u_graph_control within w_graph_control
int X=0
int Y=0
int TabOrder=10
boolean BringToTop=true
end type

on uo_control.destroy
call u_graph_control::destroy
end on

