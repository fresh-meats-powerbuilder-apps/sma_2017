﻿$PBExportHeader$w_settings.srw
$PBExportComments$Window to set the application-level settings.  It uses the u_settings object.
forward
global type w_settings from w_abstractresponse
end type
type tab_1 from tab within w_settings
end type
type tabpage_settings from userobject within tab_1
end type
type dw_settings from datawindow within tabpage_settings
end type
type tabpage_toolbar from userobject within tab_1
end type
type dw_toolbar from datawindow within tabpage_toolbar
end type
type cb_apply from commandbutton within w_settings
end type
type tabpage_settings from userobject within tab_1
dw_settings dw_settings
end type
type tabpage_toolbar from userobject within tab_1
dw_toolbar dw_toolbar
end type
type tab_1 from tab within w_settings
tabpage_settings tabpage_settings
tabpage_toolbar tabpage_toolbar
end type
end forward

global type w_settings from w_abstractresponse
int Width=1513
int Height=892
boolean TitleBar=true
string Title="Settings"
tab_1 tab_1
cb_apply cb_apply
end type
global w_settings w_settings

type variables
Private:
Window			iw_Frame

u_AbstractClassFactory	iu_ClassFactory

u_Settings	iu_Settings
end variables

forward prototypes
public subroutine wf_getvalues ()
public function boolean wf_setvalues ()
public function boolean wf_apply ()
end prototypes

public subroutine wf_getvalues ();String	ls_Item

u_AbstractErrorContext	lu_ErrorContext
iu_classfactory.uf_GetObject("u_ErrorContext",lu_ErrorContext)
lu_ErrorContext.uf_Initialize()

iu_settings.uf_Get("Prompt on Exit", ls_item, lu_ErrorContext)
tab_1.tabpage_settings.dw_settings.SetItem(1, "prompt_on_exit", Integer(ls_Item))
iu_settings.uf_Get("ArrangeOpen", ls_item, lu_ErrorContext)
tab_1.tabpage_settings.dw_settings.SetItem(1, "always_openas", String(ls_item))
iu_settings.uf_Get("ToolBarVisible", ls_item, lu_ErrorContext)
tab_1.tabpage_toolbar.dw_toolbar.SetItem(1, "hide_orshow", Integer(ls_item))
iu_settings.uf_Get("ToolBarText", ls_item, lu_ErrorContext)
tab_1.tabpage_toolbar.dw_toolbar.SetItem(1, "show_text", Integer(ls_item))
iu_settings.uf_Get("ToolBarTips", ls_item, lu_ErrorContext)
tab_1.tabpage_toolbar.dw_toolbar.SetItem(1, "show_tips", Integer(ls_item))
iu_settings.uf_Get("ToolBarAlignMent", ls_item, lu_ErrorContext)
tab_1.tabpage_toolbar.dw_toolbar.SetItem(1, "toolbar_position", String(ls_item))

If Not lu_ErrorContext.uf_IsSuccessful() Then
	// Display the error messages, if you want
End If

Return
end subroutine

public function boolean wf_setvalues ();String	ls_Item
Integer	li_Item

u_AbstractErrorContext	lu_ErrorContext
iu_classfactory.uf_GetObject("u_ErrorContext",lu_ErrorContext)
lu_ErrorContext.uf_Initialize()

If tab_1.tabpage_settings.dw_settings.AcceptText() = -1 Then Return False
If tab_1.tabpage_toolbar.dw_toolbar.AcceptText() = -1 Then Return False

li_Item = tab_1.tabpage_settings.dw_settings.GetItemNumber(1, "prompt_on_exit")
iu_settings.uf_Set("Prompt on Exit", String(li_item), lu_ErrorContext)
ls_item = tab_1.tabpage_settings.dw_settings.GetItemString(1, "always_openas")
iu_settings.uf_Set("ArrangeOpen", ls_item, lu_ErrorContext)
li_Item = tab_1.tabpage_toolbar.dw_toolbar.GetItemNumber(1, "hide_orshow")
iu_settings.uf_Set("ToolBarVisible", String(li_item), lu_ErrorContext)
li_Item = tab_1.tabpage_toolbar.dw_toolbar.GetItemNumber(1, "show_text")
iu_settings.uf_Set("ToolBarText", String(li_item), lu_ErrorContext)
li_Item = tab_1.tabpage_toolbar.dw_toolbar.GetItemNumber(1, "show_tips")
iu_settings.uf_Set("ToolBarTips", String(li_item), lu_ErrorContext)
ls_Item = tab_1.tabpage_toolbar.dw_toolbar.GetItemString(1, "toolbar_position")
iu_settings.uf_Set("ToolBarAlignMent", ls_item, lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then
	// Display the error messages, if you want
End If

Return True

end function

public function boolean wf_apply ();Application	lu_App
ArrangeOpen	lao_ArrangeOpen
lu_App = GetApplication()

If tab_1.tabpage_settings.dw_settings.AcceptText() = -1 Then Return False
If tab_1.tabpage_toolbar.dw_toolbar.AcceptText() = -1 Then Return False

Choose CASE	tab_1.tabpage_toolbar.dw_toolbar.GetItemString(1,"toolbar_position")
			CASE	'alignatleft'
				iw_frame.ToolBarAlignment = AlignAtLeft! 
			CASE	'alignatright'
				iw_frame.ToolBarAlignment = AlignAtRight!
			CASE	'alignattop'
				iw_frame.ToolBarAlignment = AlignAtTop!
			CASE	'Bottom'
				iw_frame.ToolBarAlignment = AlignAtBottom!
			CASE	'Floating'
				iw_frame.ToolBarAlignment = Floating!
End  Choose 

IF tab_1.tabpage_toolbar.dw_toolbar.GetItemNumber(1,'show_text') = 1 then
	lu_App.ToolBarText = True
ELSE
	lu_App.ToolBarText = False
END IF

IF tab_1.tabpage_toolbar.dw_toolbar.GetItemNumber(1,'show_tips') = 1 then
	lu_App.ToolBarTips = True
ELSE
	lu_App.ToolBarTips = False
END IF

IF tab_1.tabpage_toolbar.dw_toolbar.GetItemNumber(1,'hide_orshow') = 1 then
	iw_frame.ToolBarVisible = True
ELSE
	iw_frame.ToolBarVisible = False
END IF

Choose Case tab_1.tabpage_settings.dw_settings.GetItemString(1,"always_openas")
	Case 'original'
		lao_ArrangeOpen = original!
	Case 'layered'
		lao_ArrangeOpen = layered!
	Case 'cascaded'
		lao_ArrangeOpen = cascaded!
	Case ELSE
		lao_ArrangeOpen = original!
End Choose 
		
iu_classfactory.uf_setarrangeopen(lao_ArrangeOpen)

Return True
end function

on w_settings.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.cb_apply=create cb_apply
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.cb_apply
end on

on w_settings.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.cb_apply)
end on

event open;call super::open;
/* --------------------------------------------------------
w_settings

<OBJECT> This window sets and retrieves system settings,
			like how windows will open, where the toolbar
			is placed, and whether the user is prompted on
			close or exit.</OBJECT>

<USAGE>	The frame's menu should open this response window
			when the user asks to set the system settings.
			It uses the u_settings object to maintain the
			settings information persistently.  To change the
			set of settings saved, modify u_settings as well
			as this window.</USAGE>

<AUTH> Conrad Engel </AUTH>
--------------------------------------------------------- */

u_AbstractParameterStack	lu_ParameterStack
u_AbstractErrorContext	lu_ErrorContext

lu_ParameterStack = Message.PowerObjectParm

lu_ParameterStack.uf_Pop("u_abstractErrorContext",lu_ErrorContext)
lu_ParameterStack.uf_Pop("Window",iw_frame)
lu_ParameterStack.uf_Pop("u_abstractClassFactory",iu_classfactory)

iu_ClassFactory.uf_GetObject("u_Settings", iu_settings, lu_ErrorContext)
IF lu_ErrorContext.uf_IsSuccessful() Then
	IF iu_Settings.uf_Initialize(iu_ClassFactory, lu_ErrorContext) Then
		wf_GetValues()
	ELSE
		lu_ErrorContext.uf_AppendText("Unable to initialize the settings manager.")
		lu_ErrorContext.uf_SetReturnCode(-1)
		lu_ParameterStack.uf_Initialize()
		lu_ParameterStack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)
		CloseWithReturn(This,lu_ParameterStack)
	END IF			
ELSE
	lu_ErrorContext.uf_AppendText("Unable to create the settings manager.")
	lu_ErrorContext.uf_SetReturnCode(-2)
	lu_ParameterStack.uf_Initialize()
	lu_ParameterStack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)
	CloseWithReturn(This,lu_ParameterStack)
END IF

Return

end event

type cb_help from w_abstractresponse`cb_help within w_settings
int X=1216
int Y=660
int TabOrder=40
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_settings
int X=649
int Y=660
int TabOrder=30
boolean Cancel=true
end type

event clicked;Close(Parent)
end event

type cb_ok from w_abstractresponse`cb_ok within w_settings
int X=389
int Y=660
boolean Default=true
end type

event clicked;wf_SetValues()
wf_Apply()
Close(Parent)
end event

type tab_1 from tab within w_settings
int X=37
int Y=24
int Width=1431
int Height=604
int TabOrder=20
boolean BringToTop=true
boolean RaggedRight=true
int SelectedTab=1
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_settings tabpage_settings
tabpage_toolbar tabpage_toolbar
end type

on tab_1.create
this.tabpage_settings=create tabpage_settings
this.tabpage_toolbar=create tabpage_toolbar
this.Control[]={this.tabpage_settings,&
this.tabpage_toolbar}
end on

on tab_1.destroy
destroy(this.tabpage_settings)
destroy(this.tabpage_toolbar)
end on

type tabpage_settings from userobject within tab_1
int X=18
int Y=100
int Width=1394
int Height=488
long BackColor=79741120
string Text="Settings"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_settings dw_settings
end type

on tabpage_settings.create
this.dw_settings=create dw_settings
this.Control[]={this.dw_settings}
end on

on tabpage_settings.destroy
destroy(this.dw_settings)
end on

type dw_settings from datawindow within tabpage_settings
int X=9
int Y=12
int Width=1394
int Height=472
int TabOrder=30
boolean BringToTop=true
string DataObject="d_settings"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;This.InsertRow(0)
end event

event itemchanged;cb_apply.Enabled = TRUE
end event

type tabpage_toolbar from userobject within tab_1
int X=18
int Y=100
int Width=1394
int Height=488
long BackColor=79741120
string Text="Toolbar"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_toolbar dw_toolbar
end type

on tabpage_toolbar.create
this.dw_toolbar=create dw_toolbar
this.Control[]={this.dw_toolbar}
end on

on tabpage_toolbar.destroy
destroy(this.dw_toolbar)
end on

type dw_toolbar from datawindow within tabpage_toolbar
int X=9
int Width=1390
int Height=484
int TabOrder=40
boolean BringToTop=true
string DataObject="d_toolbars"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;This.InsertRow(0)
end event

event itemchanged;cb_apply.Enabled = true
end event

type cb_apply from commandbutton within w_settings
int X=942
int Y=660
int Width=261
int Height=108
int TabOrder=50
boolean Enabled=false
boolean BringToTop=true
string Text="&Apply"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;wf_SetValues()
wf_Apply()
This.Enabled = False
end event

