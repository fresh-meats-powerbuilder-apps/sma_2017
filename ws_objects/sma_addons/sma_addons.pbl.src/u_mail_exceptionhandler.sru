﻿$PBExportHeader$u_mail_exceptionhandler.sru
forward
global type u_mail_exceptionhandler from oleobject
end type
end forward

global type u_mail_exceptionhandler from oleobject
end type
global u_mail_exceptionhandler u_mail_exceptionhandler

event externalexception;

/* --------------------------------------------------------
<Desc>Occurs when an OLE automation command caused an exception on the OLE server.
		Argument	
		resultcode: 	UnsignedLong by value (a PowerBuilder number identifying the exception that occurred on the server)
		exceptioncode:	UnsignedLong by value (a number identifying the error that occurred on the server. For the meaning of the code, see the server documentation)
		source:			String by value (the name of the server (provided by the server))
		description:	String by value (a description of the exception (provided by the server))
		helpfile:		String by value (the name of a Help file containing information about the exception (provided by the server))
		helpcontext:	UnsignedLong by value (the context ID of a Help topic in helpfile containing information about the exception (provided by the server))
		action:			ExceptionAction by referenceA value you specify to control the application's course of action as a result of the error. Values are:¨	ExceptionFail! — Fail as if this script were not implemented. The error condition triggers the SystemError event¨	ExceptionIgnore! — Ignore this error and return as if no error occurred (use this option with caution because the conditions that caused the error can cause another error)¨	ExceptionRetry! — Execute the function or evaluate the expression again in case the OLE server was not ready¨	ExceptionSubstituteReturnValue! — Use the value specified in the returnvalue argument instead of the value returned by the OLE server or DataWindow and cancel the error condition
		returnvalue:	Any by referenceA value whose data type matches the expected value that the OLE server would have returned. This value is used when the value of action is ExceptionSubstituteReturnValue!


		Return value 
		None (do not use a RETURN statement)

		Usage 
		OLE objects are dynamic. Expressions that refer to data and properties of these objects may be valid under some runtime conditions but not others. If the expression causes an exception on the server, PowerBuilder triggers the ExternalException event. The ExternalException event gives you information about the error that occurred on the OLE server.
		The server defines what it considers exceptions. Some errors, such as mismatched data types, generally do not cause an exception but do trigger the Error event. In some cases you may not consider the cause of the exception to be an error. To determine the reason for the exception, see the documentation for the server. 

		When an exception occurs because of a call to an OLE server, error handling occurs like this:

		1	The ExternalException event occurs
		2	If the ExternalException event has no script or its action argument is set to ExceptionFail!, the Error event occurs
		3	If the Error event has no script or its action argument is set to ExceptionFail!, the SystemError event occurs
		4	If the SystemError event has no script, an application error occurs and the application is terminated
			
	  **  Most of These are not implemented because .. I am not what should be done yet

</Desc>
--------------------------------------------------------- */


Choose Case resultcode
   Case 32000	//mapSuccessSuccess
		action = ExceptionIgnore! 
	Case 32001 //User Abort
		action = ExceptionIgnore! 
   Case 32002 //mapFailure
   Case 32003 //mapLoginFail
   Case 32004 //mapDiskFull
   Case 32005 //mapInsufficientMem 
   Case 32006 //mapAccessDenied 
   Case 32007 //mapGeneralFailure 
   Case 32008 //mapTooManySessions
   Case 32009 // mapTooManyFiles 
   Case 32010 //mapTooManyRecipients 
   Case 32011 //mapAttachmentNotFound 
   Case 32012 //mapAttachmentOpenFailure 
   Case 32013 //mapAttachmentWriteFailure 
   Case 32014 //mapUnknownRecipient 
   Case 32015 //mapBadRecipType 
   Case 32016 //mapNoMessages 
   Case 32017 //mapInvalidMessage 
   Case 32018 //mapTextTooLarge 
   Case 32019 //mapInvalidSession 
   Case 32020 //mapTypeNotSupported 
   Case 32021 //mapAmbiguousRecipient 
   Case 32022 //mapMessageInUse 
   Case 32023 //mapNetworkFailure 
   Case 32024 //mapInvalidEditFields 
   Case 32025 //mapInvalidRecips 
   Case 32026 //mapNotSupported 
   Case 32050 //mapSessionExist 
   Case 32051 //mapInvalidBuffer 
   Case 32052 //mapInvalidReadBufferAction 
   Case 32053 //mapNoSession 
   Case 32054 //mapInvalidRecipient 
   Case 32055 //mapInvalidComposeBufferAction 
   Case 32056 //mapControlFailure 
   Case 32057 //mapNoRecipients 
   Case 32058 //mapNoAttachment 
End Choose 
end event

on u_mail_exceptionhandler.create
call oleobject::create
TriggerEvent( this, "constructor" )
end on

on u_mail_exceptionhandler.destroy
call oleobject::destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_Mail_ExceptionHandler

<OBJECT>	This MAPI external exception handler.</OBJECT>
			
<USAGE>	The object must go with u_mail, It is the error handling routines.</USAGE>

<AUTH>	Jim Weier</AUTH>

--------------------------------------------------------- */

end event

