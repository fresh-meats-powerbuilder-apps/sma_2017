﻿$PBExportHeader$u_resize.sru
forward
global type u_resize from nonvisualobject
end type
end forward

global type u_resize from nonvisualobject
end type
global u_resize u_resize

type variables
Private:
u_AbstractClassFactory	iu_ClassFactory
GraphicObject	igo_Object

u_AbstractDefaultManager	iu_DefaultManager

Long	il_X,&
	il_Y,&
	il_Width,&
	il_Height
String	is_WindowState

Boolean	ib_Resize = TRUE

end variables

forward prototypes
public function boolean uf_update (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_launch (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref window aw_window, boolean ab_resize, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref dragobject ago_dragobject, boolean ab_resize, ref u_abstracterrorcontext au_errorcontext)
private function long uf_getheight ()
private function long uf_getwidth ()
private function long uf_getx ()
private function long uf_gety ()
private function boolean uf_move ()
private function boolean uf_resize ()
private function boolean uf_setheight (long al_height, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_setstate (windowstate aws_state, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_setwidth (long al_width, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_setx (long al_x, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_sety (long al_y, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_update (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_update()

<DESC>	This function remembers the position and size of
			a window or dragobject.</DESC>

<ARGS>	au_errorcontext: Error context</ARGS>

<USAGE>	For windows that need to remember their last-saved
			size and position, call this function prior to
			their closing.  Check the error context for
			information about any problems that occurred.</USAGE>
-------------------------------------------------------- */
Window		lw_Window
DragObject	ldo_Object
WindowState	lws_State
Long			ll_X, ll_Y, ll_Width, ll_Height

Choose Case igo_Object.TypeOf()
	Case Window!
		lw_Window = igo_Object
		lws_State = lw_Window.WindowState
		This.uf_SetState(lws_State, au_errorcontext)
		lw_Window.WindowState = Normal!
		ll_Height = lw_Window.Height
		ll_Width = lw_Window.Width
		ll_X = lw_Window.X
		ll_Y = lw_Window.Y
	Case DragObject!
		ldo_Object = igo_Object
		ll_Height = ldo_Object.Height
		ll_Width = ldo_Object.Width
		ll_X = ldo_Object.X
		ll_Y = ldo_Object.Y
	Case Else
		Return False
End Choose

This.uf_SetHeight(ll_Height, au_errorcontext)
This.uf_SetWidth(ll_Width, au_errorcontext)
This.uf_SetX(ll_X, au_errorcontext)
This.uf_SetY(ll_Y, au_errorcontext)

Return True
end function

public function boolean uf_launch (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_launch()

<DESC>	This function resizes the window or dragobject
			to the last saved position (if the resize service
			was initialized with TRUE for the ab_resize
			parameter).</DESC>

<ARGS>	au_errorcontext: Error context</ARGS>

<USAGE>	Call this function to resize the window or
			dragobject to its last saved position and size.</USAGE>
-------------------------------------------------------- */
Long ll_X, ll_Y, ll_Height, ll_Width
WindowState	lws_State
Window	lw_Window
DragObject ldo_Object

Choose Case igo_Object.TypeOf()
	Case Window!
		lw_Window = igo_object
		ll_X = lw_Window.X
		ll_Y = lw_Window.Y
		ll_Height = lw_Window.Height
		ll_Width = lw_Window.Width
		lws_State = lw_Window.WindowState
	Case DragObject!
		ldo_Object = igo_Object
		ll_X = ldo_Object.X
		ll_Y = ldo_Object.Y
		ll_Height = ldo_Object.Height
		ll_Width = ldo_Object.Width
	Case Else
		igo_object.Show()
		Return False
End Choose
		
au_ErrorContext.uf_Initialize()
IF Not iu_defaultmanager.uf_GetData(igo_Object.ClassName() + "X", il_X, au_ErrorContext) Then
	// Check to see if it's there ... set il_X as original X if not
	If Not au_ErrorContext.uf_IsSuccessful() Then
		il_x = ll_X		
	END IF
END IF

au_ErrorContext.uf_Initialize()
IF Not iu_defaultmanager.uf_GetData(igo_Object.ClassName() + "Y", il_Y, au_ErrorContext) Then
	// Check to see if it's there ... set il_Y as original Y if not
	If Not au_ErrorContext.uf_IsSuccessful() Then
		il_Y = ll_Y
	END IF
END IF

au_ErrorContext.uf_Initialize()
IF Not iu_defaultmanager.uf_GetData(igo_Object.ClassName() + "Width", il_width, au_ErrorContext) Then
	// Check to see if it's there ... set il_Width as original width if not
	If Not au_ErrorContext.uf_IsSuccessful() Then
		il_Width = ll_Width
	END IF
END IF

au_ErrorContext.uf_Initialize()
IF Not iu_defaultmanager.uf_GetData(igo_Object.ClassName() + "Height", il_height, au_ErrorContext) Then
	// Check to see if it's there ... set il_Height as original height if not
	If Not au_ErrorContext.uf_IsSuccessful() Then
		il_Height = ll_Height
	END IF
END IF

au_ErrorContext.uf_Initialize()
IF Not iu_defaultmanager.uf_GetData(igo_Object.ClassName() + "State", is_WindowState, au_ErrorContext) Then
	// Check to see if it's there ... set is_WindowState as original state if not
	If Not au_ErrorContext.uf_IsSuccessful() Then
		Choose Case lws_State
			Case Normal!
				is_WindowState = "normal"
			Case Maximized!
				is_WindowState = "maximized"
			Case Minimized!
				is_WindowState = "minimized"
			Case Else
				is_WindowState = "normal"
		End Choose
	END IF
END IF

This.uf_Move()
This.uf_Resize()

igo_object.Show()

Return True
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref window aw_window, boolean ab_resize, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize()

<DESC>	This function initializes the resize service.</DESC>

<ARGS>	au_classfactory: Class factory
			aw_window: Window to resize
			ab_resize: Whether to do the actual resize or not
			au_errorcontext: Error context
</ARGS>

<USAGE>	Use this function to initialize the resize
			service.  For sheet windows, pass ab_resize as
			TRUE only when the arrangeopen setting is
			"last saved".  After this function is called,
			call either the uf_launch() function to resize
			the window, or call the uf_update() function
			to remember the current size and position.</USAGE>
-------------------------------------------------------- */

ib_resize = ab_resize
iu_classfactory = au_classfactory
igo_object = aw_window

iu_ClassFactory.uf_GetObject("u_iniDefaultManager",iu_defaultmanager, au_ErrorContext)
IF Not au_ErrorContext.uf_IsSuccessful() Then 
	au_ErrorContext.uf_AppendText("Unable to obtain a default manager.")
	Return False
END IF

iu_defaultManager.uf_initialize(iu_ClassFactory,"IBPUSER.INI","WinCoordinates", au_ErrorContext)
IF Not au_ErrorContext.uf_IsSuccessful() Then 
	au_ErrorContext.uf_AppendText("Unable to initialize the default manager.")
	Return False
END IF

Return True

end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref dragobject ago_dragobject, boolean ab_resize, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize()

<DESC>	This function initializes the resize service.</DESC>

<ARGS>	au_classfactory: Class factory
			ago_dragobject: Dragobject to resize
			ab_resize: Whether to do the actual resize or not
			au_errorcontext: Error context
</ARGS>

<USAGE>	Use this function to initialize the resize
			service.  For sheet windows, pass ab_resize as
			TRUE only when the arrangeopen setting is
			"last saved". After this function is called,
			call either the uf_launch() function to resize
			the dragobject, or call the uf_update() function
			to remember the current size and position.</USAGE>
-------------------------------------------------------- */

ib_resize = ab_resize
iu_classfactory = au_classfactory
igo_object = ago_dragobject

iu_ClassFactory.uf_GetObject("u_iniDefaultManager",iu_defaultmanager, au_ErrorContext)
IF Not au_ErrorContext.uf_IsSuccessful() Then 
	au_ErrorContext.uf_AppendText("Unable to obtain a default manager.")
	Return False
END IF

iu_defaultManager.uf_initialize(iu_ClassFactory,"IBPUSER.INI","WinCoordinates", au_ErrorContext)
IF Not au_ErrorContext.uf_IsSuccessful() Then 
	au_ErrorContext.uf_AppendText("Unable to initialize the default manager.")
	Return False
END IF

Return True

end function

private function long uf_getheight ();Return il_height
end function

private function long uf_getwidth ();Return il_width
end function

private function long uf_getx ();Return il_x
end function

private function long uf_gety ();Return il_y
end function

private function boolean uf_move ();
/* <EXCLUDE> */

Window		lw_Window
DragObject	ldo_DragObject

Choose Case igo_object.TypeOf()
	Case Window!
		If ib_Resize Then
			lw_Window = igo_object
			lw_Window.Move(il_X,il_Y)
		End If
	Case DragObject!
		If ib_Resize Then
			ldo_dragObject = igo_object
			ldo_DragObject.Move(il_X,il_Y)
		End If
End Choose

Return True
end function

private function boolean uf_resize ();
/* <EXCLUDE> */

Window	lw_Window
DragObject	ldo_DragObject

Choose Case igo_object.TypeOf()
	Case Window!
		If ib_Resize Then
			lw_Window = igo_object
			lw_Window.Resize(il_Width,il_Height)
			Choose Case Lower(is_WindowState)
				Case "normal"
					lw_Window.WindowState = Normal!
				Case "maximized"
					lw_Window.WindowState = Maximized!
				Case "minimized"
					lw_Window.WindowState = Minimized!
				Case Else
					lw_Window.WindowState = Normal!
			End Choose
		End If
	Case DragObject!
		If ib_Resize Then
			ldo_dragObject = igo_object
			ldo_DragObject.Resize(il_width,il_height)
		End If
End Choose

Return True
end function

private function boolean uf_setheight (long al_height, ref u_abstracterrorcontext au_errorcontext);
iu_defaultmanager.uf_SetData(igo_object.ClassName()+"Height", al_Height,au_ErrorContext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	au_ErrorContext.uf_AppendText("Unable to set the height of the " + igo_object.ClassName())
	Return False
ELSE
	il_height = al_Height
END IF

Return True

end function

private function boolean uf_setstate (windowstate aws_state, ref u_abstracterrorcontext au_errorcontext);String	ls_State
Window	lw_window

If igo_object.TypeOf() <> Window! Then Return True

lw_window = igo_object

Choose Case aws_State
	Case Maximized!
		ls_State = "maximized"
	Case Minimized!
		ls_state = "minimized"
	Case Normal!
		ls_state = "normal"
	Case Else
		ls_state = "normal"
End Choose

iu_defaultmanager.uf_SetData(lw_window.ClassName()+"State", ls_state, au_ErrorContext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	au_ErrorContext.uf_AppendText("Unable to set the window state of the " + lw_window.ClassName() + " window.")
	Return False
ELSE
	is_windowstate = ls_state
END IF

Return True

end function

private function boolean uf_setwidth (long al_width, ref u_abstracterrorcontext au_errorcontext);
iu_defaultmanager.uf_SetData(igo_object.ClassName()+"Width", al_Width, au_ErrorContext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	au_ErrorContext.uf_AppendText("Unable to set the width of the " + igo_object.ClassName() + " object.")
	Return False
ELSE
	il_width = al_Width
END IF

Return True

end function

private function boolean uf_setx (long al_x, ref u_abstracterrorcontext au_errorcontext);
/* <EXCLUDE> */

iu_defaultmanager.uf_SetData(igo_object.ClassName()+"X", al_X,au_ErrorContext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	au_ErrorContext.uf_AppendText("Unable to set the x-coordinate of the " + igo_object.ClassName() + " object.")
	Return False
ELSE
	il_X = al_X
END IF

Return True

end function

private function boolean uf_sety (long al_y, ref u_abstracterrorcontext au_errorcontext);
/* <EXCLUDE> */

iu_defaultmanager.uf_SetData(igo_object.ClassName()+"Y", al_Y,au_ErrorContext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	au_ErrorContext.uf_AppendText("Unable to set the y-coordinate of the " + igo_object.ClassName() + " object.")
	Return False
ELSE
	il_Y = al_Y
END IF

Return True

end function

on u_resize.create
TriggerEvent( this, "constructor" )
end on

on u_resize.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_resize

<OBJECT>	This object is a resize service for windows and
			dragobjects.  It serializes the size and position
			of windows and drag objects in persistent storage
			(currently that's in IBPUSER.INI).
</OBJECT>

<USAGE>	After opening a window or dragobject, create this
			object and initialize it with the window or
			dragobject.  Then call uf_launch() to resize the
			window to its last saved size and position. When
			the window is about to close, create this object
			and initialize it with the window or dragobject.
			Then call uf_update() to save the size and 
			position of the window to persistent storage.</USAGE>

<AUTH>	Conrad Engel</AUTH>
--------------------------------------------------------- */

end event

