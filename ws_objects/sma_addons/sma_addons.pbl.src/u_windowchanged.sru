﻿$PBExportHeader$u_windowchanged.sru
$PBExportComments$This object will check to see if any datawindows on a window have updates necessary
forward
global type u_windowchanged from nonvisualobject
end type
end forward

global type u_windowchanged from nonvisualobject
end type
global u_windowchanged u_windowchanged

forward prototypes
public function integer uf_check (window aw_window)
private function integer uf_checktab (tab at_tab)
public function boolean uf_initialize ()
end prototypes

public function integer uf_check (window aw_window);
/* --------------------------------------------------------
uf_check()

<DESC> Determine if any datawindows on the passed window have any changes
</DESC>

<ARGS> aw_window: window to check for changes
</ARGS>

<USAGE> This function will return 1 if any datawindows
			on the passed window have changed.
			It will return 0 if no changes have been found
			If any of the DataWindows fail validation, this will return 2
</USAGE>
-------------------------------------------------------- */

DataWindow	ldw_temp

Int	li_Counter, &
		li_UpperBound, &
		li_return

Tab	lt_temp

		
li_UpperBound = UpperBound(aw_window.Control)

For li_Counter = 1 to li_UpperBound
	Choose Case aw_window.Control[li_Counter].TypeOf()
		Case DataWindow!
			ldw_temp = aw_window.Control[li_Counter]
			
			If ldw_Temp.AcceptText() <> 1 Then return 2
			If ldw_temp.ModifiedCount() + ldw_temp.DeletedCount() > 0 And &
				Lower(ldw_temp.Object.DataWindow.ReadOnly) <> "yes" Then
					return 1
			End If
		Case Tab!
			lt_temp = aw_window.Control[li_Counter]
			li_return = This.uf_CheckTab(lt_temp)
			If li_return <> 0 Then
				return li_Return
			End if
	End Choose
Next
return 0
end function

private function integer uf_checktab (tab at_tab);/* --------------------------------------------------------
uf_CheckTab()

<DESC> This function is private.  Don't worry about what it does.
</DESC>

<ARGS>   at_tab: Tab Control to loop through
</ARGS>

<USAGE> This is a private function called by uf_check.
			This will loop through the passed tab control 
			to check for changed datawindows.  This function is 
			recursive.  If it finds another tab control, it 
			will call itself passing the nested tab control.
</USAGE>
-------------------------------------------------------- */

// loop through the control array and check to see if any datawindows have changed
// This returns 1 if any objects are changed

DataWindow	ldw_temp

Int	li_Counter, &
		li_SubCounter, &
		li_UpperBound, &
		li_SubUpperBound

Tab	lt_temp

UserObject	ltp_temp
Object		lo_temp
		
li_UpperBound = UpperBound(at_tab.Control)

For li_Counter = 1 to li_UpperBound
	ltp_temp = at_tab.Control[li_Counter]
	li_SubUpperBound = UpperBound(ltp_Temp.Control)
	For li_SubCounter = 1 to li_SubUpperBound
		lo_temp = at_tab.Control[li_Counter].TypeOf()
		Choose Case at_tab.Control[li_Counter].TypeOf()
			Case DataWindow!
				ldw_Temp = at_tab.Control[li_Counter].Control[li_SubCounter]
				If ldw_Temp.AcceptText() <> 1 Then return 2

				If ldw_temp.ModifiedCount() + ldw_temp.DeletedCount() > 0 Then
					return 1
				End if
			Case Tab!
				lt_temp = at_tab.Control[li_Counter].Control[li_SubCounter]
				This.uf_CheckTab(lt_temp)
		End Choose
	Next
Next

Return 0

end function

public function boolean uf_initialize ();
/* --------------------------------------------------------
uf_initialize()

<DESC> This function does nothing 
</DESC>


<USAGE> This function does nothing, but it does it good.
</USAGE>
-------------------------------------------------------- */

return true
end function

event constructor;
/* --------------------------------------------------------
u_windowchanged

<OBJECT> This object is used to determine if any of the datawindows
			on a window have changed
</OBJECT>

<USAGE> Get this object from the ClassFactory. 
		The function uf_check() will return true if changes were found
</USAGE>

<AUTH> Tim Bornholtz </AUTH>
--------------------------------------------------------- */


end event

on u_windowchanged.create
TriggerEvent( this, "constructor" )
end on

on u_windowchanged.destroy
TriggerEvent( this, "destructor" )
end on

