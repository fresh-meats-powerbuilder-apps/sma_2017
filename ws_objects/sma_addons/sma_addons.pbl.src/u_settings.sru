﻿$PBExportHeader$u_settings.sru
$PBExportComments$This object encapsulates persistent storage of application-wide interface settings.
forward
global type u_settings from nonvisualobject
end type
end forward

global type u_settings from nonvisualobject
end type
global u_settings u_settings

type variables
Private:
u_AbstractClassFactory	iu_ClassFactory
u_AbstractDefaultManager	iu_DefaultManager
end variables

forward prototypes
public function boolean uf_get (string as_name, ref string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_set (string as_name, string as_value, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_get (string as_name, ref string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_get()

<DESC>	This function gets a setting from persistent storage,
			or a default value.  
</DESC>

<ARGS>	as_name: Setting to get
			as_value: Variable by reference to hold the setting's value
			au_errorcontext: Error Context
</ARGS>

<USAGE>	Call this function after calling uf_Initialize().
			Use it to obtain an application setting. The
			valid settings you can obtain are:
			<UL>
			<LI>Prompt on Exit</LI>
			<LI>ArrangeOpen</LI>
			<LI>ToolBarVisible</LI>
			<LI>ToolBarText</LI>
			<LI>ToolBarTips</LI>
			<LI>ToolBarAlignMent</LI>
			</UL>
			This function returns a default value for the
			setting if the setting is not obtained from
			persistent storage.
</USAGE>
-------------------------------------------------------- */
Boolean lb_Success

lb_Success = iu_defaultmanager.uf_GetData(as_name, as_value, au_errorcontext)

If Not lb_Success Then
	// Not found, use defaults
	Choose Case as_Name
		Case "Prompt on Exit"
			as_value = "1"
		Case "ArrangeOpen"
			as_value = "original"
		Case "ToolBarVisible"
			as_value = "1"
		Case "ToolBarText"
			as_value = "0"
		Case "ToolBarTips"
			as_value = "1"
		Case "ToolBarAlignMent"
			as_value = "alignattop"
		Case Else
			as_value = ""
			Return False
	End Choose
End If

Return True

end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize()

<DESC>	This function initializes the settings object.  It
			opens the persistent storage file (currently that
			is IBPUSER.INI).
</DESC>

<ARGS>	au_classfactory: Class Factory
			au_errorcontext: Error Context
</ARGS>

<USAGE>	Call this function after getting the object from
			the class factory.  Call this function before
			calling either uf_Get() or uf_Set().
</USAGE>
-------------------------------------------------------- */
iu_ClassFactory = au_ClassFactory

iu_ClassFactory.uf_GetObject("u_IniDefaultManager", iu_defaultmanager, au_ErrorContext)
IF au_ErrorContext.uf_IsSuccessful() Then
	IF iu_DefaultManager.uf_Initialize(iu_classfactory,"IBPUSER.INI",&
								GetApplication().appName + " System Settings", au_ErrorContext) Then
								
		Return True
	ELSE
		au_ErrorContext.uf_AppendText("Unable to initialize the default manager.")
		au_ErrorContext.uf_SetReturnCode(-1)
		Return False
	END IF			
ELSE
	au_ErrorContext.uf_AppendText("Unable to create the default manager.")
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
END IF

Return True
end function

public function boolean uf_set (string as_name, string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_get()

<DESC>	This function sets a setting into persistent storage.
</DESC>

<ARGS>	as_name: Setting to set
			as_value: Value of the setting to set
			au_errorcontext: Error Context
</ARGS>

<USAGE>	Call this function after calling uf_Initialize().
			Use it to reset an application setting. The
			valid settings you can set are:
			<UL>
			<LI>Prompt on Exit</LI>
			<LI>ArrangeOpen</LI>
			<LI>ToolBarVisible</LI>
			<LI>ToolBarText</LI>
			<LI>ToolBarTips</LI>
			<LI>ToolBarAlignMent</LI>
			</UL>
			This function returns false if the setting 
			is not one of these items.
</USAGE>
-------------------------------------------------------- */
Choose Case as_Name
	Case "Prompt on Exit"
	Case "ArrangeOpen"
	Case "ToolBarVisible"
	Case "ToolBarText"
	Case "ToolBarTips"
	Case "ToolBarAlignMent"
	Case Else
		// Not one of the settings that we track
		Return False
End Choose

Return iu_defaultmanager.uf_SetData(as_name, as_value, au_errorcontext)

end function

on u_settings.create
TriggerEvent( this, "constructor" )
end on

on u_settings.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_settings

<OBJECT>	This object encapsulates persistence of application-wide
			settings, such as toolbar placement and whether to
			prompt the user on exit.
</OBJECT>

<USAGE>	Initialize this object and call uf_get() to retrieve
			settings from persistent storage (currently that
			is in IBPUSER.INI under a section name made up of
			the application's name plus " System Settings".
			Call uf_set() to update the settings in persistent
			storage.  Currently, the valid settings you can 
			get and set using this object are:
			<UL>
			<LI>Prompt on Exit</LI>
			<LI>ArrangeOpen</LI>
			<LI>ToolBarVisible</LI>
			<LI>ToolBarText</LI>
			<LI>ToolBarTips</LI>
			<LI>ToolBarAlignMent</LI>
			</UL>
</USAGE>

<AUTH>	Conrad Engel</AUTH>
--------------------------------------------------------- */

end event

