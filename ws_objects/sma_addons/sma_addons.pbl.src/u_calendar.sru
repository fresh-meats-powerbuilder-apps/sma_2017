﻿$PBExportHeader$u_calendar.sru
forward
global type u_calendar from u_abstractcalendar
end type
end forward

global type u_calendar from u_abstractcalendar
end type
global u_calendar u_calendar

type variables
Private:
Date	id_initdate
DataWindow	idw_Calendar
Long	il_NonActiveColor, &
	il_ActiveColor
long	il_oldrow, il_oldcol
end variables

forward prototypes
public function string uf_cancel ()
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref datawindow adw_control, string as_dwobjectname, string as_initdate, ref u_abstracterrorcontext au_errorcontext)
public function string uf_clicked (long al_row, string as_dwoname)
public function string uf_keyenter ()
public function string uf_display ()
public function string uf_keydownarrow ()
public function string uf_keyleftarrow ()
public function string uf_keypagedown ()
public function string uf_next ()
public function string uf_previous ()
public function string uf_keypageup ()
public function string uf_keyrightarrow ()
public function string uf_keyuparrow ()
public function string uf_showbox ()
end prototypes

public function string uf_cancel ();
/* --------------------------------------------------------

<DESC>	Returns an empty string.   
			</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Function is called when user presses the cancel button. 
			The object that requested the calendar to open must 
			handle the empty string.</USAGE>
-------------------------------------------------------- */

Return ""
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref datawindow adw_control, string as_dwobjectname, string as_initdate, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the calendar object.
			</DESC>

<ARGS>	au_classfactory: ClassFactory
			adw_control: The DataWindow control on the window
			as_dwobjectname: Name of the DataWindow object you will use
			as_initdate: Initial date
			au_errorcontext: Error Context</ARGS>
			
<USAGE>	Call this function after obtaining the object from the class factory.
			This function sets up the calendar object to display its calendar
			in the DataWindow control adw_control.  It displays using a
			DataObject of as_dwobjectname.  The date the calendar starts with
			is passed in as as_initdate. It must be a valid date.
			If an error occurs, the error context will contain the reason,
			and this function will return false.  Call uf_Display() to show the
			calendar.
			</USAGE>
			
-------------------------------------------------------- */

idw_calendar = adw_control
idw_calendar.DataObject = as_dwobjectname
idw_Calendar.InsertRow(0)
IF IsDate(as_InitDate) then
	id_initdate = Date(as_InitDate)
	Return True
ELSE
	au_errorcontext.uf_AppendText("Invalid Date")
	Return False
END IF


end function

public function string uf_clicked (long al_row, string as_dwoname);
/* --------------------------------------------------------

<DESC>	Returns the date value of the column clicked in string 
			format "mm/dd/yyyy".
			</DESC>

<ARGS>	al_row: Row that was clicked
			as_dwoname: Column clicked
			
<USAGE>	Returns the date of column clicked. Returns an
			empty string if it is an invalid
			row or column.</USAGE>
-------------------------------------------------------- */

IF al_row <> 1 Then return ""
IF Lower(Left(as_DwoName,3)) <> "day" Then Return ""
Return String(idw_calendar.GetItemDate(al_Row, as_DWOName))
end function

public function string uf_keyenter ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the Enter key.
			Returns a string representing the highlighted date.
			</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return String(id_initdate,"mm/dd/yyyy")
end function

public function string uf_display ();
/* --------------------------------------------------------

<DESC>	Function to display the calendar
			</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Call this function every time a calendar view needs 
			to change.  This function will return a string 
			containing the month and year, suitable for use
			as a window title.</USAGE>
-------------------------------------------------------- */

Int	li_Month, &
		li_Year, &
		li_DayOFWeek, &
		li_RelativeDays, &
		li_Loop, &
		li_Loop2, &
		li_Count

Date	ld_TheFirst, &
		ld_FirstDateToDisplay

string	ls_Debug
String	ls_modify
u_win32api	lu_win32

If il_NonActiveColor = 0 And il_ActiveColor = 0 Then
	lu_win32 = Create u_win32api
	// Gray Text
	il_NonActiveColor = lu_Win32.uf_getSysColor(17)
	// Window Text
	il_ActiveColor = lu_Win32.uf_getSysColor(8)
End If

li_Month = Month(id_initdate)
li_Year = Year(id_initdate)

ld_TheFirst = Date(String(li_Month) + "/01/" + String(li_Year))

li_DayOFweek = DayNumber(ld_TheFirst)
li_RelativeDays = 0 - ( li_DayOfWeek - 1)
ld_FirstDateToDisplay = RelativeDate( ld_TheFirst, li_RelativeDays)

ls_modify = ""
li_count = 0
For li_loop = 1 to 6
	For li_loop2 = 1 to 7 
		idw_calendar.SetItem(1, "day" + String(li_Loop2) + "_" + String(li_Loop), &
								RelativeDate(ld_FirstDateToDisplay, li_Count))
						
		if Month(RelativeDate(ld_FirstDateToDisplay, li_Count)) <> li_Month Then 
			ls_modify += " " + "day" + String(li_Loop2) + "_" + string(li_Loop) + ".Color=" + String(il_NonActiveColor)
		ELSE
			ls_modify += " " + "day" + String(li_Loop2) + "_" + string(li_Loop) + ".Color=" + String(il_ActiveColor)
		END IF

//		IF RelativeDate(ld_FirstDateToDisplay, li_Count) = id_initdate Then 
//			ls_modify += " " + "day" + String(li_Loop2) + "_" + string(li_Loop) + ".Border=TRUE"
//		ELSE
//			ls_modify += " " + "day" + String(li_Loop2) + "_" + string(li_Loop) + ".Border=False"
//		End IF	
		li_count ++
	Next
Next

idw_calendar.Modify(ls_modify)
ls_modify = ""

Return uf_showbox()
end function

public function string uf_keydownarrow ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the down arrow key.
			This function sets the highlighted date a week
			forward.  It calls uf_Display() to update the
			display.
			</DESC>

<ARGS>	</ARGS>
			
-------------------------------------------------------- */

id_initdate = RelativeDate(id_initdate,+7)
Return uf_Display()

end function

public function string uf_keyleftarrow ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the left arrow key.
			This function sets the highlight to the day
			prior.  It calls uf_display() to update the 
			display.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */
id_initdate = RelativeDate(id_initdate,-1)
Return uf_Display()
end function

public function string uf_keypagedown ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the Page Down key.
			This function calls uf_Next().
			</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return uf_Next()

end function

public function string uf_next ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the "Next" button.
			This function sets the highlight to the next
			month and updates the display with a call to
			uf_Display().</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

integer	li_Month,&
			li_Year,&
			li_DAY
String	ls_Date			
			
li_month = Month(id_initdate)
li_month ++
if li_month = 13 Then
	li_Month = 1
	li_Year = Year(id_initdate) + 1
ELSE
	li_Year = Year(id_initdate)
END IF
li_Day = Day(id_initdate)
ls_Date = String(li_Month) +"/"+String(li_Day)+"/"+String(li_Year)
Do While Not IsDate(ls_Date)
	id_InitDate = relativeDate(id_initdate,-1)
	li_month = Month(id_initdate)
	li_month ++
	if li_month = 13 Then
		li_Month = 1
		li_Year = Year(id_initdate) + 1
	ELSE
		li_Year = Year(id_initdate)
	END IF
	li_Day = Day(id_initdate)
	ls_Date = String(li_Month) +"/"+String(li_Day)+"/"+String(li_Year)
Loop
id_initdate = Date(ls_Date)
Return uf_Display()

end function

public function string uf_previous ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the "Previous" button.
			This function sets the highlight to the previous
			month and updates the display with a call to
			uf_Display().</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */
integer	li_Month,&
			li_Year,&
			li_DAY
String	ls_Date			
			
li_month = Month(id_initdate)
li_month --
if li_month = 0 Then
	li_Month = 12
	li_Year = Year(id_initdate) - 1
ELSE
	li_Year = Year(id_initdate)
END IF
li_Day = Day(id_initdate)
ls_Date = String(li_Month) +"/"+String(li_Day)+"/"+String(li_Year)
Do While Not IsDate(ls_Date)
	id_InitDate = relativeDate(id_initdate,-1)
	li_month = Month(id_initdate)
	li_month --
	if li_month = 0 Then
		li_Month = 12
		li_Year = Year(id_initdate) - 1
	ELSE
		li_Year = Year(id_initdate)
	END IF
	li_Day = Day(id_initdate)
	ls_Date = String(li_Month) +"/"+String(li_Day)+"/"+String(li_Year)
Loop
id_initdate = Date(ls_Date)
Return uf_Display()

end function

public function string uf_keypageup ();
/* --------------------------------------------------------

<DESC>	Called when the user presses the Page Up key.
			This function calls uf_Previous().</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

Return uf_Previous()
end function

public function string uf_keyrightarrow ();

/* --------------------------------------------------------

<DESC>	Called when the user presses the right arrow key.
			This function sets the highlight to the next
			day and calls uf_Display() to update the display.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */

id_initdate = RelativeDate(id_initdate,+1)
Return	uf_Display()

end function

public function string uf_keyuparrow ();

/* --------------------------------------------------------

<DESC>	Called when the user presses the up arrow key.
			This function sets the highlight to the date a
			week prior.  It then calls uf_Display() to
			update the display.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	</USAGE>
-------------------------------------------------------- */
id_initdate = RelativeDate(id_initdate,-7)
Return uf_Display()
end function

public function string uf_showbox ();Int	li_Month, &
		li_Year, &
		li_DayOFWeek, &
		li_RelativeDays, &
		li_count, &
		li_loop, &
		li_loop2

Date	ld_TheFirst, &
		ld_FirstDateToDisplay

String ls_modify

li_Month = Month(id_initdate)
li_Year = Year(id_initdate)

ld_TheFirst = Date(String(li_Month) + "/01/" + String(li_Year))

li_DayOFweek = DayNumber(ld_TheFirst)
li_RelativeDays = 0 - ( li_DayOfWeek - 1)
ld_FirstDateToDisplay = RelativeDate( ld_TheFirst, li_RelativeDays)

li_count = DaysAfter(ld_FirstDateToDisplay, id_initdate)
li_loop = Int(li_count / 7) + 1
li_loop2 = Mod(li_count, 7) + 1

ls_modify = "day" + String(li_Loop2) + "_" + string(li_Loop) + ".Border=TRUE"
If il_oldcol > 0 And il_oldrow > 0 Then
	ls_modify += " " + "day" + String(il_oldcol) + "_" + string(il_oldrow) + ".Border=False"
End If

il_oldcol = li_Loop2
il_oldrow = li_Loop

idw_calendar.Modify(ls_modify)

Return String(ld_TheFirst, "mmmm, yyyy")

end function

on u_calendar.create
TriggerEvent( this, "constructor" )
end on

on u_calendar.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;
/* --------------------------------------------------------
u_Calendar

<OBJECT>	Object used to display calendar.</OBJECT>
			
<USAGE>	To change the behavior of the calendar, implement
			the methods of the u_AbstractCalendar interface.
			The w_calendar response window calls these methods to
			determine its behavior. </USAGE>
			
<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
end event

