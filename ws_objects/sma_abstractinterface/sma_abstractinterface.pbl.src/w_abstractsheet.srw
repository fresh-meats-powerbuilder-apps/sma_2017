﻿$PBExportHeader$w_abstractsheet.srw
forward
global type w_abstractsheet from Window
end type
end forward

global type w_abstractsheet from Window
int X=1056
int Y=484
int Width=3031
int Height=1576
boolean Visible=false
boolean TitleBar=true
string Title="WLS Sheet"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
event ue_postopen ( )
event ue_delete ( )
event type boolean ue_inquire ( )
event ue_new ( )
event type boolean ue_save ( )
event ue_saveas ( )
event ue_import ( )
event ue_print ( )
event ue_printsetup ( )
event ue_printpreview ( )
event ue_printscreen ( )
event ue_send ( )
event ue_undo ( )
event ue_cut ( )
event ue_copy ( )
event ue_paste ( )
event ue_clear ( )
event ue_clearwindow ( )
event ue_find ( )
event ue_replace ( )
event ue_next ( )
event ue_previous ( )
event ue_sort ( )
event ue_filter ( )
event ue_contexthelp ( )
event ue_printnodialog ( )
end type
global w_abstractsheet w_abstractsheet

on w_abstractsheet.create
end on

on w_abstractsheet.destroy
end on

