﻿$PBExportHeader$u_wrkbenchexternals.sru
forward
global type u_wrkbenchexternals from nonvisualobject
end type
end forward

global type u_wrkbenchexternals from nonvisualobject
end type
global u_wrkbenchexternals u_wrkbenchexternals

type variables
u_wrkbench	iu_wrkbench

end variables

forward prototypes
public function integer uf_wbgetapiverrel (ref integer version, ref integer release)
public function integer uf_wbgetverrel (ref integer version, ref integer release)
public function integer uf_wbgetcommhandle (ref integer commhnd)
public function integer uf_wbreleasecommhandle (integer commhnd)
public function integer uf_wbsetserveralias (string serveralias, integer commhnd)
public function integer uf_wbgeterrorinfo (ref integer commerror, ref string commerrmsg, ref long neterror, ref string neterrmsg, ref long primaryerror, ref long secondaryerror, integer commhnd)
public function integer uf_wbsetsecurity (string userid, string password, string key, integer commhnd)
public function integer uf_wbsetuserinfo (long userinfo, integer commhnd)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function integer uf_wbgetapiverrel (ref integer version, ref integer release);return iu_wrkbench.WBGetAPIVerRel(version, release)
end function

public function integer uf_wbgetverrel (ref integer version, ref integer release);return iu_wrkbench.WBGetVerRel(version, release)
end function

public function integer uf_wbgetcommhandle (ref integer commhnd);Return iu_wrkbench.WBGetCommHandle(commhnd)
end function

public function integer uf_wbreleasecommhandle (integer commhnd);Return iu_wrkbench.WBReleaseCommHandle(commhnd)
end function

public function integer uf_wbsetserveralias (string serveralias, integer commhnd);Return iu_wrkbench.WBSetServerAlias(serveralias, commhnd)
end function

public function integer uf_wbgeterrorinfo (ref integer commerror, ref string commerrmsg, ref long neterror, ref string neterrmsg, ref long primaryerror, ref long secondaryerror, integer commhnd);Return iu_wrkbench.WBGetErrorInfo(commerror, commerrmsg, neterror, neterrmsg, primaryerror, secondaryerror, commhnd)
end function

public function integer uf_wbsetsecurity (string userid, string password, string key, integer commhnd);Return iu_wrkbench.WBSetSecurity(userid, password, key, commhnd)
end function

public function integer uf_wbsetuserinfo (long userinfo, integer commhnd);Return iu_wrkbench.WBSetUserInfo(userinfo, commhnd)
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);Environment	lenv_env
Boolean	lb_initialized
GetEnvironment( lenv_env)

IF lenv_env.Win16 Then
	lb_initialized = au_classfactory.uf_GetObject("u_wrkbench16",iu_wrkbench, au_errorcontext)
	If Not au_errorcontext.uf_IsSuccessful() Then 
		// Couldn't get 16-bit wrkbench externals
		// Populate error context and return False
		
		Return False
	End If
	
	If Not lb_initialized Then
		// u_wrkbench16 needs no initialization so continue
	End If
Else
	lb_initialized = au_classfactory.uf_GetObject("u_wrkbench32",iu_wrkbench, au_errorcontext)
	If Not au_errorcontext.uf_IsSuccessful() Then 
		// Couldn't get 32-bit wrkbench externals
		// Populate error context and return False
		
		Return False
	End If
	
	If Not lb_initialized Then
		// u_wrkbench32 needs no initialization so continue
	End If
End if	

Return True
end function

on u_wrkbenchexternals.create
TriggerEvent( this, "constructor" )
end on

on u_wrkbenchexternals.destroy
TriggerEvent( this, "destructor" )
end on

