﻿$PBExportHeader$u_netwise.sru
forward
global type u_netwise from nonvisualobject
end type
end forward

global type u_netwise from nonvisualobject
end type
global u_netwise u_netwise

type variables
protected:
u_wrkbenchexternals	iu_wrkbench
u_abstractdefaultmanager	iu_defaultmanager
integer			ii_CommHnd = -1

string			is_ServerPrefix
string			is_ServerSuffix
end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_checkrpcerror (ref u_abstracterrorcontext au_errorcontext)
protected function string uf_servername ()
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the Netwise subsystem (get a CommHnd,
			set UserID and password, and set the server name).  
			</DESC>

<ARGS>	au_classfactory: ClassFactory
			as_userid: Mainframe UserID for WBSetSecurity call
			as_password: Password for WBSetSecurity call
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function after getting the descendant
			object from the ClassFactory.  If this function
			returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Boolean	lb_initialized

lb_initialized = au_classfactory.uf_GetObject("u_wrkbenchexternals", iu_wrkbench, au_errorcontext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	Return False
End If

If Not lb_initialized Then
	If Not iu_wrkbench.uf_initialize(au_classfactory,au_errorcontext) Then
		// Error initializing wrkbench externals
		// Populate error context and return False
		au_errorcontext.uf_AppendText("Can't initialize Workbench externals")
		au_errorcontext.uf_SetReturnCode(-1)
		Return False
	End If
End If

lb_initialized = au_classfactory.uf_GetObject("u_inidefaultmanager", iu_defaultmanager, au_errorcontext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	Return False
End If

// We initialize it no matter what
If Not iu_defaultmanager.uf_Initialize(au_classfactory, "ibp002.ini", "Netwise Server Info", au_errorcontext) Then
	// Error initializing default manager
	// Populate error context and return False
	au_errorcontext.uf_AppendText("Can't initialize default manager for server info")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If

If Not iu_defaultmanager.uf_GetData("ServerPrefix", is_ServerPrefix, au_errorcontext) Then
	// Error getting server prefix
	// Populate error context and return false
	au_errorcontext.uf_AppendText("Unable to obtain server prefix")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
End If

If Not iu_defaultmanager.uf_GetData("ServerSuffix",is_ServerSuffix, au_errorcontext) Then
	// Error getting server suffix
	// Populate error context and return false
	au_errorcontext.uf_AppendText("Unable to obtain server suffix")
	au_errorcontext.uf_SetReturnCode(-1)	
	Return False
End If

If ii_CommHnd >= 0 Then
	If iu_wrkbench.uf_WBReleaseCommHandle(ii_CommHnd) < 0 Then
			// Error occurred releasing old CommHandle
			// Populate error context and return False
			au_errorcontext.uf_AppendText("An error occurred attempting to release the old CommHandle")
			au_errorcontext.uf_SetReturnCode(-1)
			Return False
	End If
End If

If iu_wrkbench.uf_WBGetCommHandle(ii_CommHnd) < 0 Then
	// Error occurred getting a new CommHandle
	// Populate error context (with uf_CheckRPCError) and return False
	This.uf_CheckRPCError(au_errorcontext)
	au_errorcontext.uf_AppendText("An error occurred attempting to get a new CommHandle")
	au_errorcontext.uf_SetReturnCode(-1)	
	Return False
End If

If iu_wrkbench.uf_WBSetSecurity(as_userid,as_password,"NETWISE",ii_CommHnd) < 0 Then
	// Error occurred setting security
	// Populate error context (with uf_CheckRPCError) and return False
	This.uf_CheckRPCError(au_errorcontext)
	au_errorcontext.uf_AppendText("An error occurred attempting to set the security")
	au_errorcontext.uf_SetReturnCode(-1)		
	Return False
End If

If iu_wrkbench.uf_WBSetServerAlias(This.uf_ServerName(),ii_CommHnd) < 0 Then
	// Error occurred setting server alias
	// Populate error context (with uf_CheckRPCError) and return False
	This.uf_CheckRPCError(au_errorcontext)
	au_errorcontext.uf_AppendText("An error occurred attempting to set the server alias")
	au_errorcontext.uf_SetReturnCode(-1)		
	Return False
End If

Return True
end function

public function boolean uf_checkrpcerror (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Netwise error information and populate an
			ErrorContext object with the details.</DESC>

<ARGS>	au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function after a Netwise RPC call.
			Check the error context's uf_IsSuccessful() to
			determine if a Netwise error occurred during
			processing.  The error context's text contains
			the error numbers and messages returned by
			Netwise.</USAGE>
-------------------------------------------------------- */
String	ls_CommErrorMsg, &
			ls_NetErrorMsg
			
Integer	li_CommError

Long		ll_NetError, &
			ll_PrimaryError, &
			ll_SecondaryError

ls_CommErrorMsg = Space(100)
ls_NetErrorMsg = Space(100)

If iu_wrkbench.uf_WBGetErrorInfo(li_CommError, ls_CommErrorMsg, ll_NetError, ls_NetErrorMsg, &
	ll_PrimaryError, ll_SecondaryError, ii_CommHnd) <> 0 Then
	// Error occurred trying to get error info
	// Populate error context and return False
	au_ErrorContext.uf_AppendText("Netwise:  Can't get error information")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

// If li_CommError or ll_NetError are nonzero, populate error context and return False
// If both are zero, just return True
If li_CommError <> 0 Then
	au_ErrorContext.uf_Set("CommError", li_CommError)
	au_ErrorContext.uf_AppendText(ls_CommErrorMsg)
	If ll_NetError <> 0 Then
		au_ErrorContext.uf_Set("NetError",ll_NetError)
		au_ErrorContext.uf_AppendText(ls_NetErrorMsg)
	End If
	If li_CommError = 108 Then  // Could have primary/secondary error
		au_ErrorContext.uf_Set("PrimaryError",ll_PrimaryError)
		au_ErrorContext.uf_AppendText("Primary Error: " + String(ll_PrimaryError))
		au_ErrorContext.uf_Set("SecondaryError",ll_SecondaryError)
		au_ErrorContext.uf_AppendText("Secondary Error: " + String(ll_SecondaryError))
	End If
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If
	
Return True
end function

protected function string uf_servername ();
/* --------------------------------------------------------

<DESC>	Get the Netwise server name.</DESC>

<USAGE>	The server name is obtained by using the
			ClassName of the object (without the leading "u_")
			and adding a prefix and suffix obtained in the
			uf_initialize() call. This function is
			protected.</USAGE>
-------------------------------------------------------- */
Return is_serverprefix + Lower(Mid(This.ClassName(),3)) + is_serversuffix
end function

on u_netwise.create
TriggerEvent( this, "constructor" )
end on

on u_netwise.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_netwise

<OBJECT>	This object is a utility object for connecting
			to Netwise. It reads from a u_inidefaultsmanager
			to figure out the Netwise server name from the
			prefix and suffix given by the defaults manager.
			</OBJECT>
			
<USAGE>	Inherit from this object, define external
			Netwise functions, and wrap them with PowerBuilder
			functions.  In an application, get your inherited
			object from the ClassFactory and call
			uf_Initialize().  To check errors from Netwise
			calls, call uf_CheckRPCError().</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */

end event

