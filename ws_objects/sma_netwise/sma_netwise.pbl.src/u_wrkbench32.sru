﻿$PBExportHeader$u_wrkbench32.sru
forward
global type u_wrkbench32 from u_wrkbench
end type
end forward

global type u_wrkbench32 from u_wrkbench
end type
global u_wrkbench32 u_wrkbench32

type prototypes
public:
function int WBGetAPIVerRel( ref int version, ref int release ) &
    library "wrkbe32.dll"

function int WBGetVerRel( ref int version, ref int release ) &
    library "wrkbe32.dll"

function int WBGetCommHandle( ref int CommHnd ) &
    library "wrkbe32.dll"

function int WBReleaseCommHandle( int CommHnd ) &
    library "wrkbe32.dll"

function int WBSetServerAlias( string serveralias, int CommHnd ) &
    library "wrkbe32.dll" alias for "WBSetServerAlias;Ansi"

function int WBGetErrorInfo( ref int commerror, ref string commerrmsg, &
                             ref long neterror, ref string neterrmsg, &
                             ref long primaryerror, ref long secondaryerror, &
                             int CommHnd ) &
    library "wrkbe32.dll" alias for "WBGetErrorInfo;Ansi"

function int WBSetSecurity( string userid, string password, &
                            string key, int CommHnd ) &
    library "wrkbe32.dll" alias for "WBSetSecurity;Ansi"

function int WBSetUserInfo( long userinfo, int CommHnd ) &
    library "wrkbe32.dll"

function int WBDispNumToDP( string str, ref double dval ) &
    library "wrkbe32.dll" alias for "WBDispNumToDP;Ansi"

function int WBDPToDispNum( double dval, ref string str, int len ) &
    library "wrkbe32.dll" alias for "WBDPToDispNum;Ansi"

function int WBPackDecToDP( string str, ref double dval ) &
    library "wrkbe32.dll" alias for "WBPackDecToDP;Ansi"

function int WBDPToPackDec( double dval, ref string str, int len ) &
    library "wrkbe32.dll" alias for "WBDPToPackDec;Ansi"
end prototypes

on u_wrkbench32.create
TriggerEvent( this, "constructor" )
end on

on u_wrkbench32.destroy
TriggerEvent( this, "destructor" )
end on

