﻿$PBExportHeader$u_abstractdataaccess.sru
forward
global type u_abstractdataaccess from nonvisualobject
end type
end forward

global type u_abstractdataaccess from nonvisualobject
end type
global u_abstractdataaccess u_abstractdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_retrieve (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_update (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory);return false
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);return false
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext);return false
end function

public function boolean uf_update (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext);return false
end function

on u_abstractdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_abstractdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

