﻿$PBExportHeader$u_abstracttransaction.sru
forward
global type u_abstracttransaction from transaction
end type
end forward

global type u_abstracttransaction from transaction
end type
global u_abstracttransaction u_abstracttransaction

forward prototypes
public function Boolean uf_connect (ref u_AbstractErrorContext au_ErrorContext)
public function Boolean uf_disconnect (ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_AbstractDefaultManager au_DefaultManager, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function Boolean uf_connect (ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function Boolean uf_disconnect (ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_AbstractDefaultManager au_DefaultManager, ref u_abstracterrorcontext au_errorcontext);Return FALSE
end function

on u_abstracttransaction.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_abstracttransaction.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

