﻿$PBExportHeader$u_abstractresize.sru
forward
global type u_abstractresize from nonvisualobject
end type
end forward

global type u_abstractresize from nonvisualobject
end type
global u_abstractresize u_abstractresize

type variables


end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, DragObject ago_Object)
public function long uf_getheight ()
public function long uf_getwidth ()
public function long uf_getx ()
public function long uf_gety ()
public function boolean uf_resize ()
public function Boolean uf_launch ()
public function boolean uf_setx (long al_x, ref u_abstractErrorContext au_ErrorContext)
public function boolean uf_sety (long al_y, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setheight (long al_height, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setwidth (long al_width, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref window ago_object, ref u_AbstractErrorContext au_ErrorContext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, DragObject ago_Object);Return FALSE
end function

public function long uf_getheight ();Return -1
end function

public function long uf_getwidth ();Return -1
end function

public function long uf_getx ();Return -1
end function

public function long uf_gety ();Return -1
end function

public function boolean uf_resize ();Return False
end function

public function Boolean uf_launch ();Return False
end function

public function boolean uf_setx (long al_x, ref u_abstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_sety (long al_y, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setheight (long al_height, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setwidth (long al_width, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref window ago_object, ref u_AbstractErrorContext au_ErrorContext);Return FALSE
end function

on u_abstractresize.create
TriggerEvent( this, "constructor" )
end on

on u_abstractresize.destroy
TriggerEvent( this, "destructor" )
end on

