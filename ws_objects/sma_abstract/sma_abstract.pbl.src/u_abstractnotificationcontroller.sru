﻿$PBExportHeader$u_abstractnotificationcontroller.sru
forward
global type u_abstractnotificationcontroller from nonvisualobject
end type
end forward

global type u_abstractnotificationcontroller from nonvisualobject
end type
global u_abstractnotificationcontroller u_abstractnotificationcontroller

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_abstractclassfactory)
public function boolean uf_display (ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_abstractclassfactory);
/* --------------------------------------------------------

<DESC>	Initialize the notification controller.</DESC>

<ARGS>	au_abstractclassfactory: ClassFactory</ARGS>
			
<USAGE>	This function initializes the notification
			controller.  Call it first.</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_display (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Display a notification message.</DESC>

<ARGS>	au_errorcontext: The error context to display</ARGS>
			
<USAGE>	Call this function to display a message for a given error
			context.  Typically this function is called when
			the error context's uf_IsSuccessful() call returns
			false.</USAGE>
-------------------------------------------------------- */
Return False
end function

on u_abstractnotificationcontroller.create
TriggerEvent( this, "constructor" )
end on

on u_abstractnotificationcontroller.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_abstractnotificationcontroller

<OBJECT>	This object is responsible for notifying the user
			or others about messages generated and stored
			in an error context object.</OBJECT>
			
<USAGE>	Get one of these objects, initialize it, and
			when you have an error context object that
			needs notification, call uf_Display() to
			notify the user or others about messages
			generated and stored in the error context
			object.</USAGE>

<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */

end event

