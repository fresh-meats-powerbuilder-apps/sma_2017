﻿$PBExportHeader$u_abstractprintfunctions.sru
forward
global type u_abstractprintfunctions from nonvisualobject
end type
end forward

global type u_abstractprintfunctions from nonvisualobject
end type
global u_abstractprintfunctions u_abstractprintfunctions

type prototypes
Function int showPrintDlg(ULONG hwndOwner, ref string as_printer, &
		ref boolean ab_all_pages,  &
		ref boolean ab_collate, &
		ref boolean ab_disable_printtofile,  &
		ref boolean ab_hide_printtofile, &
		ref boolean ab_nopagenums,  &
		ref boolean ab_noselection, &
		ref boolean ab_nowarning,  &
		ref boolean ab_pagenums,  &
		ref boolean ab_printsetup,  &
		ref boolean ab_printtofile, &
		ref boolean ab_returndefault,  &
		ref boolean ab_selection, &
		ref boolean ab_showhelp,  &
		ref int ai_frompage,  &
		ref int ai_topage, &
		ref int ai_minpage,  &
		ref int ai_maxpage,  &
		ref int ai_copies)  &
library "printdlg.dll" alias for "showPrintDlg;Ansi"

Function int showPageSetupDlg(long hwndOwner,  &
			ref string as_printer, &
			ref boolean ab_default_min_margins, &
			ref boolean ab_disable_margins, &
			ref boolean ab_disable_orientation, &
			ref boolean ab_disable_paper, &
			ref boolean ab_disable_printer, &
			ref boolean ab_margins, &
			ref boolean ab_min_margins, &
			ref boolean ab_nowarning, &
			ref boolean ab_return_default, &
			ref boolean ab_show_help, &
			ref long al_size_x, &
			ref long al_size_y, &
			ref long al_min_left, &
			ref long al_min_right, &
			ref long al_min_top, &
			ref long al_min_bottom, &
			ref long al_left, &
			ref long al_right, &
			ref long al_top, &
			ref long al_bottom) &
library "printdlg.dll" alias for "showPageSetupDlg;Ansi"
end prototypes

type variables
Protected:
string 	is_default, &
	is_driver, &
	is_port, &
	is_printer,&
	is_OrigPrinter

u_AbstractClassFactory	iu_ClassFactory
u_AbstractErrorContext	iu_ErrorContext

end variables

forward prototypes
public function boolean uf_printdialog (ref string as_printer_name, ref boolean ab_all_pages, ref boolean ab_collate, ref boolean ab_disable_printtofile, ref boolean ab_hide_printtofile, ref boolean ab_nopagenums, ref boolean ab_noselection, ref boolean ab_nowarning, ref boolean ab_pagenums, ref boolean ab_printsetup, ref boolean ab_printtofile, ref boolean ab_returndefault, ref boolean ab_selection, ref boolean ab_showhelp, ref integer ai_frompage, ref integer ai_topage, ref integer ai_minpage, ref integer ai_maxpage, ref integer ai_copies)
public function boolean uf_pagesetupdialog (ref string as_printer_name, ref boolean ab_default_min_margins, ref boolean ab_disable_margins, ref boolean ab_disable_orientation, ref boolean ab_disable_paper, ref boolean ab_disable_printer, ref boolean ab_margins, ref boolean ab_min_margins, ref boolean ab_nowarning, ref boolean ab_return_default, ref boolean ab_show_help, ref long al_size_x, ref long al_size_y, ref long al_min_left, ref long al_min_right, ref long al_min_top, ref long al_min_bottom, ref long al_left, ref long al_right, ref long al_top, ref long al_bottom)
public function boolean uf_printdialog (ref boolean ab_collate, ref integer ai_from_page, ref integer ai_to_page, ref integer ai_min_page, ref integer ai_max_page, ref integer ai_copies)
public function string uf_getdefaultprinter ()
public function boolean uf_intialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_resetprinter ()
public function boolean uf_setprinter (string as_printer)
public function boolean uf_saveprinter (string as_printername)
public function boolean uf_getsavedprinter (ref String as_PrinterName)
end prototypes

public function boolean uf_printdialog (ref string as_printer_name, ref boolean ab_all_pages, ref boolean ab_collate, ref boolean ab_disable_printtofile, ref boolean ab_hide_printtofile, ref boolean ab_nopagenums, ref boolean ab_noselection, ref boolean ab_nowarning, ref boolean ab_pagenums, ref boolean ab_printsetup, ref boolean ab_printtofile, ref boolean ab_returndefault, ref boolean ab_selection, ref boolean ab_showhelp, ref integer ai_frompage, ref integer ai_topage, ref integer ai_minpage, ref integer ai_maxpage, ref integer ai_copies);Return False
end function

public function boolean uf_pagesetupdialog (ref string as_printer_name, ref boolean ab_default_min_margins, ref boolean ab_disable_margins, ref boolean ab_disable_orientation, ref boolean ab_disable_paper, ref boolean ab_disable_printer, ref boolean ab_margins, ref boolean ab_min_margins, ref boolean ab_nowarning, ref boolean ab_return_default, ref boolean ab_show_help, ref long al_size_x, ref long al_size_y, ref long al_min_left, ref long al_min_right, ref long al_min_top, ref long al_min_bottom, ref long al_left, ref long al_right, ref long al_top, ref long al_bottom);Return False
end function

public function boolean uf_printdialog (ref boolean ab_collate, ref integer ai_from_page, ref integer ai_to_page, ref integer ai_min_page, ref integer ai_max_page, ref integer ai_copies);// This function supplies most default arguments to PrintDialog().
// Arguments are:					
//		boolean	ab_collate			Whether collating is set on or not
//		integer	ai_from_page
//		integer	ai_to_page
//		integer	ai_min_page			Minimum page number the user can choose
//		integer	ai_max_page			Maximum page number the user can choose
//		integer	ai_copies	
// 
// On entry, these arguments populate the PrintDialog dialog box initially.
// On exit, these arguments contain the final values the user selected in the dialog box.

String ls_printer_name
Boolean	lb_true = True, &
			lb_false = False

return uf_printdialog(ls_printer_name, &
					lb_True, &
					ab_collate, &
					lb_False, &
					lb_True, &
					lb_False, &
					lb_True, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					ai_from_page, &
					ai_to_page, &
					ai_min_page, &
					ai_max_page, &
					ai_copies)
				
end function

public function string uf_getdefaultprinter ();/* --------------------------------------------------------

<DESC>	Function used to get Windows default printer</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Concreate implementations return the name of the OS 
			default printer</USAGE>
			
-------------------------------------------------------- */
Return ''
end function

public function boolean uf_intialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);/* --------------------------------------------------------

<DESC>	Function used to set concrete implementation for use</DESC>

<ARGS>	au_classfactory: Class Factory 
			au_errorcontext: Error Context</ARGS>

			
<USAGE>	Call this function to set the concrete implementation of
			a print object. ClassFactory and error context because it ia assumed
			that the print object will NOT be able to its work with out
			creating other objects
			printer</USAGE>
			
-------------------------------------------------------- */
Return True
end function

public function boolean uf_resetprinter ();Return False
end function

public function boolean uf_setprinter (string as_printer);Return False
end function

public function boolean uf_saveprinter (string as_printername);RETURN False
end function

public function boolean uf_getsavedprinter (ref String as_PrinterName);/* --------------------------------------------------------

<DESC>	Function to obtain the default printer for the application</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Concrete implementation should return a string of the name of the printer
			the application last used</USAGE>
			
-------------------------------------------------------- */
Return False
end function

on u_abstractprintfunctions.create
TriggerEvent( this, "constructor" )
end on

on u_abstractprintfunctions.destroy
TriggerEvent( this, "destructor" )
end on

