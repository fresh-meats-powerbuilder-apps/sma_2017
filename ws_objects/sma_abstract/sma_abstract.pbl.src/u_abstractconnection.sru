﻿$PBExportHeader$u_abstractconnection.sru
forward
global type u_abstractconnection from connection
end type
end forward

global type u_abstractconnection from connection
end type
global u_abstractconnection u_abstractconnection

forward prototypes
private function boolean uf_initialize (ref u_abstractclassfactory au_classfactory)
end prototypes

private function boolean uf_initialize (ref u_abstractclassfactory au_classfactory);return true

end function

on u_abstractconnection.create
call connection::create
TriggerEvent( this, "constructor" )
end on

on u_abstractconnection.destroy
call connection::destroy
TriggerEvent( this, "destructor" )
end on

