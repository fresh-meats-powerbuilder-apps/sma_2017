﻿$PBExportHeader$u_locationdataaccess.sru
forward
global type u_locationdataaccess from nonvisualobject
end type
end forward

global type u_locationdataaccess from nonvisualobject
end type
global u_locationdataaccess u_locationdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievelocations (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrievelocations (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_locationdataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_locationdataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

