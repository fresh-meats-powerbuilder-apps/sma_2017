﻿$PBExportHeader$u_salespersondataaccess.sru
forward
global type u_salespersondataaccess from nonvisualobject
end type
end forward

global type u_salespersondataaccess from nonvisualobject
end type
global u_salespersondataaccess u_salespersondataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievesalespersons (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrievesalespersons (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_salespersondataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_salespersondataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

