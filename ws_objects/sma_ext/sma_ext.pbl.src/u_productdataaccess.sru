﻿$PBExportHeader$u_productdataaccess.sru
forward
global type u_productdataaccess from nonvisualobject
end type
end forward

global type u_productdataaccess from nonvisualobject
end type
global u_productdataaccess u_productdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieveproducts (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieveproductdivision (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrieveproducts (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrieveproductdivision (ref u_abstractparameterstack au_parameterstack);return true
end function

on u_productdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_productdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

