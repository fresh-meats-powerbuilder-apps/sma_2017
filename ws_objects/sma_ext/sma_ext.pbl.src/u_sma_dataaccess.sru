﻿$PBExportHeader$u_sma_dataaccess.sru
forward
global type u_sma_dataaccess from u_abstractdataaccess
end type
end forward

global type u_sma_dataaccess from u_abstractdataaccess
end type
global u_sma_dataaccess u_sma_dataaccess

forward prototypes
public subroutine uf_getloadgroups (ref datawindow adw_data_window)
public subroutine uf_getservicecenterdddw (ref datawindowchild adwc_data_window_child)
public function boolean uf_findcustid (string as_cust_id, string as_location)
public function boolean uf_getservicecenter (string as_service_center)
public function boolean uf_findcorpid (string as_cust_id)
public subroutine uf_getmarkettype (ref datawindowchild adw_markettype)
public subroutine uf_getsalesperson (ref datawindowchild adwc_sales_person, string as_service_center, string as_employee_type)
public subroutine uf_initialize ()
end prototypes

public subroutine uf_getloadgroups (ref datawindow adw_data_window);adw_data_window.SetTransObject(SQLCA)
adw_data_window.Retrieve( "WLSLDGRP" )
//is_selection = '3' // Multiple rows w/ ctrl+alt support.

end subroutine

public subroutine uf_getservicecenterdddw (ref datawindowchild adwc_data_window_child);Integer li_rc

adwc_data_window_child.SetTransObject( SQLCA )
adwc_data_window_child.Retrieve( )

end subroutine

public function boolean uf_findcustid (string as_cust_id, string as_location);// Purpose:   To find the input customer id/location combination in the database.
// Called by: Activity Controller ue_GetCustomers event.

String ls_data, &
       ls_title = "uf_FindCustId"

Select customers.customer_id
	Into  :ls_data
	From  customers
	Where Trim( customer_id ) = :as_cust_id AND
			Trim( location )    = :as_location;

If ( SQLCA.SQLCode = - 1 ) Then
	MessageBox( ls_title, "Error: " + SQLCA.SQLErrText, Exclamation! )
	Return( False )
ElseIf (SQLCA.SQLCode <> 0 ) Then
	MessageBox( ls_title, "Customer ID " + as_cust_id + " is not in the Customer Master Database." )
	Return( False )
End If

Return True
end function

public function boolean uf_getservicecenter (string as_service_center);Select salesman.smanloc 
	Into  :as_service_center
	From  salesman
	Where salesman.userid = :SQLCA.userID ;
		
If ( SQLCA.SQLCode = - 1 ) Then
	MessageBox( "Window ue_postopen", "Error: " + SQLCA.SQLErrText, Exclamation! )
	as_service_center = ""
ElseIf (SQLCA.SQLCode <> 0 ) Then
	MessageBox( "uf_GetServiceCenter", "SQLCA Userid " + SQLCA.userID + &
		" is not in the Salesman Database." )
	as_service_center = ""
//	Else // Location is in DB, grey field and protect it.
//		dw_1.Object.service_center.Background.Color = 12632256
//		dw_1.Object.service_center.Protect = 1
End If

Return True
end function

public function boolean uf_findcorpid (string as_cust_id);String ls_data, ls_title = "uf_FindCorpId"

Select corpcust.corp_id
	Into  :ls_data
	From  corpcust
	Where Trim( corp_id ) = :as_cust_id;
				
If ( SQLCA.SQLCode = - 1 ) Then
	MessageBox( ls_title, "Error: " + SQLCA.SQLErrText, Exclamation! )
	Return False
ElseIf (SQLCA.SQLCode <> 0 ) Then
	MessageBox( ls_title, "Customer ID " + as_cust_id + " is not in the Corporate Customer Database." )
	Return False
End If
end function

public subroutine uf_getmarkettype (ref datawindowchild adw_markettype);adw_markettype.SetTransObject(SQLCA)
adw_markettype.Retrieve( "MKTTYPE" )
//is_selection = '3' // Multiple rows w/ ctrl+alt support.
end subroutine

public subroutine uf_getsalesperson (ref datawindowchild adwc_sales_person, string as_service_center, string as_employee_type);adwc_sales_person.SetTransObject(SQLCA)
adwc_sales_person.Retrieve(as_service_center,as_employee_type)

end subroutine

public subroutine uf_initialize ();// ** IBDKEEM ** 03/09/2002 ** 
// Changed all code in function from a hard coded connection string to INI file read.
// SQL Serever change for SMA
// **** 
//// Where does all the transaction stuff go?
//SQLCA = Create transaction
//
//SQLCA.DBMS = "ODBC"
//SQLCA.Database = "pblocaldb"
//SQLCA.DbParm = "Connectstring = 'DSN=pblocaldb'"
//
//Connect Using SQLCA;
//
//If SQLCA.SQLCode <> 0 then
//	Messagebox("Error","Connect Failed: " + SQLCA.SQLErrText)
//	Disconnect using SQLCA;
//	Return
//End if
//
//// Set SQLCA.userid from ibpuser.ini file.
//SQLCA.Userid = ProfileString( "ibpuser.ini", "System Settings", "UserId", "IBDKKAM" )
//
// ** END ** IBDKEEM ** 03/09/2002 ** 

string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

SQLCA = Create transaction // Create a transaction object
SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")

//if Upper(SQLCA.dbms) = Upper("SNC SQL Native Client") Then					 // Connecting to a SQL Server 
	SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
	SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password
//Else
	//SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
//End If

if SQLCA.database = 'false' or SQLCA.dbParm = 'false' then 							 // "SMA DATABASE" Not found in the INI
	SQLCA.database = ProfileString(ls_inifile, "DEFAULT DATABASE", "database", "pblocaldb")
	SQLCA.dbParm = ProfileString(ls_inifile, "DEFAULT DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
end if

Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode <> 0 then
	Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
	Disconnect using SQLCA;
	Return
End if

end subroutine

on u_sma_dataaccess.create
call super::create
end on

on u_sma_dataaccess.destroy
call super::destroy
end on

