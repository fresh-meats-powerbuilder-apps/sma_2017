﻿$PBExportHeader$w_smaframe.srw
forward
global type w_smaframe from w_abstractframe
end type
end forward

global type w_smaframe from w_abstractframe
boolean TitleBar=true
string Title="Sales and Marketing Accounting"
end type
global w_smaframe w_smaframe

on w_smaframe.create
call super::create
end on

on w_smaframe.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

event close;u_Resize	lu_Resize
Window				lw_Window

lw_Window = This

If Not iu_ClassFactory.uf_GetObject( "u_Resize", lu_Resize, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful() Then
		lu_Resize.uf_Initialize( iu_Classfactory, lw_Window, True, iu_ErrorContext)
		lu_Resize.uf_Update(iu_ErrorContext)
	End If
End If
end event

