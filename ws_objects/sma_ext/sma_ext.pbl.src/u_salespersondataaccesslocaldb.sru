﻿$PBExportHeader$u_salespersondataaccesslocaldb.sru
forward
global type u_salespersondataaccesslocaldb from u_salespersondataaccess
end type
end forward

global type u_salespersondataaccesslocaldb from u_salespersondataaccess
end type
global u_salespersondataaccesslocaldb u_salespersondataaccesslocaldb

type variables
u_odbctransaction		iu_odbctransaction
end variables
forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievesalespersons (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);u_AbstractClassFactory					lu_ClassFactory

u_AbstractErrorContext				 	lu_ErrorContext

u_AbstractDefaultManager				lu_DefaultManager


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject( "u_ODBCTransaction", iu_ODBCTransaction, lu_ErrorContext ) Then
	If Not lu_ClassFactory.uf_GetObject( "u_iniDefaultManager", lu_DefaultManager, lu_ErrorContext ) Then
		If lu_ErrorContext.uf_IsSuccessful( ) Then
			lu_DefaultManager.uf_Initialize( lu_ClassFactory, 'ibp002.ini', 'SMA DATABASE', lu_ErrorContext )
			If Not lu_ErrorContext.uf_IsSuccessful() Then
				au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
				Return False
			End If
		End If
	End If

	If lu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ODBCTransaction.uf_Initialize( lu_ClassFactory, lu_DefaultManager, lu_ErrorContext )
		If Not lu_ErrorContext.uf_IsSuccessful() Then
			au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
			au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
			Return False
		End If
	End If
End If

Return True
end function

public function boolean uf_retrievesalespersons (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the division divisions	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve salespersons from the
</USAGE>  pblocaldb database
-------------------------------------------------------- */
Integer							li_Count
//
//Long								ll_Rtn, &
//									ll_findrow

DataStore						lds_salesperson

String								ls_salespersonstring, &
									ls_ErrorString

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

lds_salesperson = Create DataStore
lds_salesperson.DataObject = 'd_salesperson'
lds_salesperson.SetTransObject(iu_ODBCTransaction)

lds_salesperson.Retrieve()
ls_salespersonstring = lds_salesperson.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_salespersonstring)

return true

end function

on u_salespersondataaccesslocaldb.create
call super::create
end on

on u_salespersondataaccesslocaldb.destroy
call super::destroy
end on

