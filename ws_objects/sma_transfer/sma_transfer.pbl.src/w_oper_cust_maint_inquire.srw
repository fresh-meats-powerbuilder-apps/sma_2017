﻿$PBExportHeader$w_oper_cust_maint_inquire.srw
forward
global type w_oper_cust_maint_inquire from w_abstractresponseext
end type
type dw_opercustomer from datawindow within w_oper_cust_maint_inquire
end type
end forward

global type w_oper_cust_maint_inquire from w_abstractresponseext
integer width = 1897
integer height = 876
string title = "Operator Customer Maintenance Inquire"
dw_opercustomer dw_opercustomer
end type
global w_oper_cust_maint_inquire w_oper_cust_maint_inquire

type variables
String			is_InquireOption, &
                  is_CustType, &
				is_CustID, &
				is_title

u_abstracterrorcontext		iu_errorcontext

u_abstractparameterstack	iu_parameterstack

w_smaframe					iw_frame

u_OperCustomerDataAccess	iu_OperCustomerDataAccess

u_AbstractClassFactory		iu_ClassFactory

u_NotificationController			iu_Notification	

end variables

forward prototypes
public function boolean wf_getopercustomer ()
end prototypes

public function boolean wf_getopercustomer ();DataWindowChild					ldwc_OperCustomer, &
                                             ldwc_ParentIdentifierCustomer													

String								ls_opercustomerstring, &
									ls_current_id, ls_previous_id
									
Long								ll_row_cnt, ll_cust_row, ll_current_row

DataStore	                       lds_custids

u_AbstractParameterStack		lu_ParameterStack	

u_String_Functions					lu_String


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)

iu_OperCustomerDataAccess.uf_Retrieve( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_operCustomerString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

If lu_String.nf_IsEmpty(ls_operCustomerString) Then
	Return False
End If

dw_opercustomer.GetChild('customer_id', ldwc_ParentIdentifierCustomer)
dw_opercustomer.GetChild('oper_customer_id', ldwc_operCustomer)


lds_custids = Create datastore
lds_custids.DataObject = 'd_customerparentiddddw'
lds_custids.Reset()
ll_row_cnt = lds_custids.ImportString(ls_operCustomerString)

// populate Operator customer id dddw - omit duplicates
ls_previous_id = ''
ll_cust_row = 1

For ll_current_row = 1 to ll_row_cnt
	ls_current_id =lds_custids.GetItemString(ll_current_row, 'oper_customer_id')
	If ls_current_id <> ls_previous_id Then
		ldwc_operCustomer.InsertRow(0)
		ldwc_operCustomer.SetItem(ll_cust_row, 'oper_customer_id', ls_current_id)
		ll_cust_row++
	End If
	ls_previous_id = ls_current_id
Next	


// populate ParentIdentifier customer id dddw - omit duplicates
ls_previous_id = ''
ll_cust_row = 1

For ll_current_row = 1 to ll_row_cnt
	ls_current_id =lds_custids.GetItemString(ll_current_row, 'customer_id')
	If ls_current_id <> ls_previous_id Then
		ldwc_ParentIdentifierCustomer.InsertRow(0)
		ldwc_ParentIdentifierCustomer.SetItem(ll_cust_row, 'customer_id', ls_current_id)
		ldwc_ParentIdentifierCustomer.SetItem(ll_cust_row, 'customer_type', lds_custids.GetItemString(ll_current_row, 'customer_type'))
		ldwc_ParentIdentifierCustomer.SetItem(ll_cust_row, 'oper_customer_id', lds_custids.GetItemString(ll_current_row, 'oper_customer_id'))
		ldwc_ParentIdentifierCustomer.SetItem(ll_cust_row, 'oper_customer_type', lds_custids.GetItemString(ll_current_row, 'oper_customer_type'))
		ll_cust_row++
	End If
	ls_previous_id = ls_current_id
Next	

Return True

end function

on w_oper_cust_maint_inquire.create
int iCurrent
call super::create
this.dw_opercustomer=create dw_opercustomer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_opercustomer
end on

on w_oper_cust_maint_inquire.destroy
call super::destroy
destroy(this.dw_opercustomer)
end on

event open;call super::open;DataWindowChild		ldwc_operCustomer

iu_ParameterStack = Message.PowerObjectParm


If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_OperCustomerDataAccess', iu_OperCustomerDataAccess)
iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('window', iw_frame)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_CustID)
iu_ParameterStack.uf_Pop('string', is_CustType)
iu_ParameterStack.uf_Pop('string', is_InquireOption)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)


This.Title = is_title + ' Inquire'

wf_getopercustomer()

is_InquireOption = 'Cancel'  

dw_opercustomer.object.oper_customer_id.Visible = false
dw_opercustomer.object.oper_customer_id_t.Visible=false
dw_opercustomer.object.customer_id.Visible = false
dw_opercustomer.object.customer_id_parentid_t.Visible=false
dw_opercustomer.object.customer_type.Visible = false
dw_opercustomer.object.customer_type_parentid_t.Visible = false

dw_opercustomer.SetFocus()
dw_opercustomer.SetRedraw(True)
This.SetRedraw(True)
end event

event close;call super::close;DataWindowChild		ldwc_temp


iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_InquireOption)
iu_ParameterStack.uf_Push('string', is_CustType)
iu_ParameterStack.uf_Push('string', is_CustID)
iu_ParameterStack.uf_Push('string', is_title)
iu_ParameterStack.uf_Push('window', iw_frame)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Push('u_OperCustomerDataAccess', iu_operCustomerDataAccess)

CloseWithReturn(This, iu_ParameterStack)

end event

type cb_help from w_abstractresponseext`cb_help within w_oper_cust_maint_inquire
boolean visible = false
integer taborder = 0
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_oper_cust_maint_inquire
integer x = 901
integer y = 636
integer taborder = 30
boolean cancel = true
end type

event cb_cancel::clicked;call super::clicked;
is_InquireOption = 'Cancel'

Close(Parent)

end event

type cb_ok from w_abstractresponseext`cb_ok within w_oper_cust_maint_inquire
integer x = 567
integer y = 636
end type

event cb_ok::clicked;call super::clicked;String					ls_operCustomer, &
                           ls_CustomerIDB, &
						ls_CustomerTypeB			
									
u_string_functions	lu_string


If dw_opercustomer.AcceptText() = -1 Then Return

// if All operator Customers is selected then there will be no other data to pass back other than customer_option 'A' 
// customer_option
// customer_type
// customer_id

is_InquireOption = dw_opercustomer.GetItemString(1,'customer_option')

Choose Case is_InquireOption
	Case 'A'
		is_CustID = ''
		is_CustType = ''
	Case 'O'
		ls_operCustomer = dw_opercustomer.GetItemString(1, 'oper_customer_id')
		If lu_string.nf_IsEmpty(ls_operCustomer) Then
			iw_frame.SetMicroHelp('oper Customer ID is required')
			dw_opercustomer.SetFocus()
			dw_opercustomer.SetColumn('oper_customer_id')
			dw_opercustomer.SelectText(1,10000)
			Return
		End If			
		is_CustID = ls_operCustomer
		is_CustType = ''
	Case 'P'
		ls_CustomerIDB = dw_opercustomer.GetItemString(1, 'customer_id')
		If lu_string.nf_IsEmpty(ls_CustomerIDB) Then
			iw_frame.SetMicroHelp('Parent Identifier Customer ID is required')
			dw_opercustomer.SetFocus()
			dw_opercustomer.SetColumn('customer_id')
			dw_opercustomer.SelectText(1,10000)
			Return
		End If			
		is_CustID = ls_CustomerIDB
		is_CustType = dw_opercustomer.GetItemString(1, 'customer_type')
End Choose

Close(Parent)
end event

type dw_opercustomer from datawindow within w_oper_cust_maint_inquire
integer x = 32
integer y = 16
integer width = 1792
integer height = 568
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_oper_customer_inquire"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event constructor;
If This.rowcount() = 0 Then
	This.InsertRow(0)
End If



end event

event getfocus;This.SelectText(1,1000)



end event

event itemerror;Return 1
end event

event itemchanged;DataWindowChild				ldwc_child_custtypeb, ldwc_child_custidb
Window							lw_parentwindow
String								ls_ColumnName, ls_str

ls_ColumnName = dwo.Name

Choose Case ls_ColumnName
	Case 'customer_option'
		If data = 'A' then  // All customers
			This.object.oper_customer_id.Visible = false
			This.object.oper_customer_id_t.Visible=false
			This.object.customer_id.Visible = false
			This.object.customer_id_parentid_t.Visible=false
			This.object.customer_type.Visible = false
			This.object.customer_type_parentid_t.Visible = false
		End If	
		If data = 'O' then  // Operator customer
			This.object.oper_customer_id.Visible = true
			This.object.oper_customer_id_t.Visible=true
			This.object.customer_id.Visible = false
			This.object.customer_id_parentid_t.Visible=false
			This.object.customer_type.Visible = false
			This.object.customer_type_parentid_t.Visible = false
		End If
		If data = 'P' then  //Parent Identifier customer
			This.object.oper_customer_id.Visible = false
			This.object.oper_customer_id_t.Visible=false
			This.object.customer_id.Visible = true
			This.object.customer_id_parentid_t.Visible=true
			This.object.customer_type.Visible = true
			This.object.customer_type_parentid_t.Visible = true
			dw_opercustomer.GetChild('customer_type', ldwc_child_custtypeb)
			dw_opercustomer.GetChild('customer_id', ldwc_child_custidb)
			ls_str = ldwc_child_custtypeb.GetItemString(1, 'customer_type')
			ls_str = dw_opercustomer.GetItemString(1, 'customer_type') 
			If IsNull(dw_opercustomer.GetItemString(1, 'customer_type'))  Then
				ldwc_child_custidb.SetFilter('customer_type = "B"')
	      		ldwc_child_custidb.Filter()			
				dw_opercustomer.SetItem(1,'customer_type', 'B')
			End If
		End If	
	Case 'customer_type'
			dw_opercustomer.GetChild('customer_type', ldwc_child_custtypeb)
			dw_opercustomer.GetChild('customer_id', ldwc_child_custidb)
			dw_opercustomer.SetItem (1,'customer_id', '')
			Choose Case data
				Case 'S'
					ldwc_child_custidb.SetFilter('customer_type = "S"')
   		      		ldwc_child_custidb.Filter()
				Case 'B'
					ldwc_child_custidb.SetFilter('customer_type = "B"')
   		      		ldwc_child_custidb.Filter()					
				Case 'C'
					ldwc_child_custidb.SetFilter('customer_type = "C"')
   		      		ldwc_child_custidb.Filter()					
			End Choose
End Choose		
end event

