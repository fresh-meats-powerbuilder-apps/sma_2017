﻿$PBExportHeader$w_transfercustomerinquire.srw
forward
global type w_transfercustomerinquire from w_abstractresponseext
end type
type dw_customer from u_abstractdwext within w_transfercustomerinquire
end type
end forward

global type w_transfercustomerinquire from w_abstractresponseext
integer width = 1961
integer height = 420
dw_customer dw_customer
end type
global w_transfercustomerinquire w_transfercustomerinquire

type variables
String			is_TransferCustomer, &
			is_title

u_abstracterrorcontext	iu_errorcontext

u_abstractparameterstack	iu_parameterstack

w_smaframe		iw_frame

DataWindowChild		idwc_ParentTransferCustomer

u_TransferCustomerDataAccess	iu_TransferCustomerDataAccess

u_AbstractClassFactory		iu_ClassFactory

u_NotificationController			iu_Notification	

end variables

forward prototypes
public function boolean wf_gettransfercustomer (string as_transfercustomer)
end prototypes

public function boolean wf_gettransfercustomer (string as_transfercustomer);DataWindowChild					ldwc_TransferCustomer

String								ls_transfercustomerstring

u_AbstractParameterStack		lu_ParameterStack	

u_String_Functions					lu_String


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

as_TransferCustomer = as_transfercustomer + '~tS'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', as_TransferCustomer)

iu_TransferCustomerDataAccess.uf_Retrieve( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_TransferCustomerString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

If lu_String.nf_IsEmpty(ls_TransferCustomerString) Then
	Return False
End If

dw_customer.GetChild('customer_number', ldwc_TransferCustomer)
ldwc_TransferCustomer.ImportString(ls_TransferCustomerString)

Return True

end function

on w_transfercustomerinquire.create
int iCurrent
call super::create
this.dw_customer=create dw_customer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_customer
end on

on w_transfercustomerinquire.destroy
call super::destroy
destroy(this.dw_customer)
end on

event open;call super::open;DataWindowChild		ldwc_TransferCustomer

iu_ParameterStack = Message.PowerObjectParm


If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_TransferCustomerDataAccess', iu_TransferCustomerDataAccess)
iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('window', iw_frame)
iu_ParameterStack.uf_Pop('datawindowchild', idwc_ParentTransferCustomer)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_TransferCustomer)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)


This.Title = is_title + ' Inquire'

dw_customer.GetChild('Customer_number', ldwc_TransferCustomer)
idwc_ParentTransferCustomer.ShareData(ldwc_TransferCustomer)

dw_customer.GetChild('Customer_number_descr', ldwc_TransferCustomer)
idwc_ParentTransferCustomer.ShareData(ldwc_TransferCustomer)

dw_customer.SetItem(1, 'Customer_number', is_TransferCustomer)

is_TransferCustomer = 'Cancel'

dw_customer.SetFocus()
end event

event close;DataWindowChild		ldwc_temp


iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_TransferCustomer)
iu_ParameterStack.uf_Push('string', is_title)
iu_ParameterStack.uf_Push('datawindowchild', ldwc_temp)
iu_ParameterStack.uf_Push('window', iw_frame)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Push('u_TransferCustomerDataAccess', iu_TransferCustomerDataAccess)

CloseWithReturn(This, iu_ParameterStack)

end event

type cb_help from w_abstractresponseext`cb_help within w_transfercustomerinquire
integer x = 1147
integer y = 172
integer taborder = 40
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_transfercustomerinquire
integer x = 777
integer y = 172
integer taborder = 30
boolean cancel = true
end type

event clicked;String					ls_TransferCustomer


is_TransferCustomer = 'Cancel'

Close(Parent)

end event

type cb_ok from w_abstractresponseext`cb_ok within w_transfercustomerinquire
integer x = 443
integer y = 172
integer taborder = 20
boolean default = true
end type

event clicked;String					ls_TransferCustomer

u_string_functions	lu_string


If dw_customer.AcceptText() = -1 Then Return

is_TransferCustomer = dw_customer.GetItemString(1, 'customer_number')

If lu_string.nf_IsEmpty(is_TransferCustomer) Then
	iw_frame.SetMicroHelp('Customer ID is a required field')
	dw_customer.SetFocus()
	dw_customer.SetColumn('customer_number')
	dw_customer.SelectText(1, 10000)
	Return
End If

Close(Parent)
end event

type dw_customer from u_abstractdwext within w_transfercustomerinquire
integer y = 28
integer width = 1778
integer height = 88
boolean bringtotop = true
string dataobject = "d_customer"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event constructor;DataWindowChild					ldwc_code, &
										ldwc_descr
										
This.InsertRow(0)

This.object.customer_number.background.color = 16777215
This.object.customer_number.Protect = 0



end event

event itemchanged;DataWindowChild				ldwc_child
Window							lw_parentwindow


dw_customer.GetChild('customer_number', ldwc_child)

//I set Pointer back to an arrow! because it wasn't doing it itself.  Maybe a pb6.0 bug.
If ldwc_child.Find('customer_number = "' + data + '"', 1, ldwc_child.RowCount() + 1) <= 0 Then
	If Not wf_GetTransferCustomer(data) Then
		iw_frame.SetMicroHelp('This is a invalid Customer Number')
		This.SetFocus()
		This.SelectText(1, 1000)
		SetPointer(Arrow!)
		Return 1
	Else
		SetPointer(Arrow!)
	End IF
End IF


end event

event itemerror;Return 1
end event

event itemfocuschanged;This.SelectText(1,1000)
end event

event getfocus;This.SelectText(1,1000)
end event

