﻿$PBExportHeader$w_oper_customer.srw
forward
global type w_oper_customer from w_abstractsheetext
end type
type dw_oper_customers from datawindow within w_oper_customer
end type
end forward

global type w_oper_customer from w_abstractsheetext
integer width = 3607
integer height = 1704
string title = "Operator Customer Maintenance"
event type boolean ue_reinquire ( )
dw_oper_customers dw_oper_customers
end type
global w_oper_customer w_oper_customer

type variables
String					is_Title, & 
						is_CustID, & 
						is_CustType, &
						is_InquireOption, &
						is_whatwastyped
	String					is_DeleteInd
                    

u_AbstractClassFactory				iu_ClassFactory

u_OperCustomerDataAccess			iu_OperCustomerDataAccess

u_OperCustWindowDataAccess		iu_OperCustWindowDataAccess

u_ErrorContext							iu_ErrorContext

u_NotificationController				iu_Notification	

w_smaframe							iw_frame	

DataStore                       			ids_shiptos, ids_billtos, ids_corporates

u_CustomerDataAccess			    iu_CustomerDataAccess
u_SalespersonDataAccess			iu_SalespersonDataAccess
end variables

forward prototypes
public subroutine wf_dbconnect ()
public function string wf_buildupdatestring ()
public function boolean wf_validate (long al_row)
public function boolean wf_getsalespersons ()
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function string wf_builddeletestring ()
end prototypes

event type boolean ue_reinquire();String         ls_OperCustomers, &
				ls_OperCustWindowString

Long			ll_row				

u_ParameterStack		lu_ParameterStack

dw_oper_customers.Reset()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

SetPointer(HourGlass!)

ls_OperCustomers = is_InquireOption + '~t' + is_CustType + '~t' + is_CustID
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_OperCustomers)
iu_OperCustWindowDataAccess.uf_Retrieve( lu_ParameterStack )
lu_ParameterStack.uf_Pop('string', ls_OperCustWindowString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_oper_customers.SetRedraw(false)
This.SetRedraw(false)

ll_row = dw_oper_customers.ImportString(ls_OperCustWindowString)

If ll_row > 0 Then
	SetMicroHelp(String(ll_row) + ' Rows Retrieved')
Else
	SetMicroHelp('No Rows Retrieved')
End If

If is_InquireOption <> 'O' Then
	dw_oper_customers.InsertRow(0)
End If

dw_oper_customers.ResetUpdate()

dw_oper_customers.SetRedraw(true)

This.SetRedraw(true)

dw_oper_customers.SetRow(1)
dw_oper_customers.SetColumn(2)

end event

public subroutine wf_dbconnect ();string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")
SQLCA.dbparm = ProfileString(ls_inifile, ls_section, "dbParm", "false")

SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password

Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode = -1 Then
	// do nothing
Else
	If SQLCA.SQLCode <> 0 then
		Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
		Disconnect using SQLCA;
		Return
	End If
End if
end subroutine

public function string wf_buildupdatestring ();Long					ll_ModifiedCount, &
						ll_count
						
String				ls_UpdateString, &
					ls_temp
					
u_String_Functions		lu_string					

dwItemStatus		lis_UpdateStatus


ll_ModifiedCount = dw_oper_customers.ModifiedCount()

If ll_ModifiedCount <= 0 Then
	SetMicroHelp('No Update Necessary')
	Return ''
End If

ls_UpdateString = ''

ll_Count = 0
IF ll_ModifiedCount > 0 Then
	ll_Count =dw_oper_customers.GetNextModified(ll_Count, Primary!)
	Do
		If Not wf_validate(ll_Count) Then 
			dw_oper_customers.SelectRow(0, False)
			dw_oper_customers.SelectRow(ll_Count, True)
			Return ''
		End If
		dw_oper_customers.SelectRow(0, False)
		dw_oper_customers.SelectRow(ll_count, True)
		lis_UpdateStatus = dw_oper_customers.GetItemStatus(ll_Count, 0, Primary!)
		Choose Case lis_UpdateStatus
			Case NewModified!
				
				ls_temp = dw_oper_customers.GetItemString(ll_count, 'parentid_customer_id')
				If Not lu_string.nf_IsEmpty(ls_temp) Then 
								
					ls_UpdateString += dw_oper_customers.GetItemString(ll_Count, 'oper_customer_id') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'oper_customer_name') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'salesperson_code') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'parentid_customer_id') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'parentid_customer_type') + '~t' + &
										'I~r~n'
				End If
			Case DataModified!
					ls_UpdateString += dw_oper_customers.GetItemString(ll_Count, 'oper_customer_id') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'oper_customer_name') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'salesperson_code') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'parentid_customer_id') + '~t' + &
										dw_oper_customers.GetItemString(ll_Count, 'parentid_customer_type') + '~t' + &
										'U~r~n'
			Case Else
				SetMicroHelp('Update Status is ' + String(lis_UpdateStatus) + &
				'.  Please Call Applications.')
				Return ''
		End Choose
		ll_Count = dw_oper_customers.GetNextModified(ll_Count, Primary!)
	Loop While ll_Count > 0
End If	

dw_oper_customers.SelectRow(0, False)

Return ls_UpdateString

end function

public function boolean wf_validate (long al_row);String							ls_temp

u_String_Functions		lu_string


ls_temp = dw_oper_customers.GetItemString(al_row, 'oper_customer_id')
  
If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Oper Customer is a required field')
	dw_oper_customers.SetFocus()
	dw_oper_customers.SetColumn('oper_customer_id')
	Return False
End If

ls_temp = dw_oper_customers.GetItemString(al_row, 'oper_customer_name')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Oper Customer Name is a required field')
	dw_oper_customers.SetFocus()
	dw_oper_customers.SetColumn('oper_customer_name')
	Return False
End If

ls_temp = dw_oper_customers.GetItemString(al_row, 'salesperson_code')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Salesperson is a required field')
	dw_oper_customers.SetFocus()
	dw_oper_customers.SetColumn('salesperson_code')
	Return False
End If

ls_temp = dw_oper_customers.GetItemString(al_row, 'parentid_customer_type')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Parent Customer Type is a required field')
	dw_oper_customers.SetFocus()
	dw_oper_customers.SetColumn('parentid_customer_type')
	Return False
End If


ls_temp = dw_oper_customers.GetItemString(al_row, 'parentid_customer_id')
	
If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Parent Identifier ID is a required field')
	dw_oper_customers.SetFocus()
	dw_oper_customers.SetColumn('parentid_customer_id')
	Return False
End If


Return True
end function

public function boolean wf_getsalespersons ();DataWindowChild					ldwc_SalespersonCode

String									ls_SalespersonString

Long                                      ll_temp

u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_SalespersonDataAccess", iu_SalespersonDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_SalespersonDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)

iu_SalespersonDataAccess.uf_RetrieveSalespersons(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_SalespersonString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_oper_customers.GetChild('salesperson_code', ldwc_SalespersonCode)
ll_temp = ldwc_SalespersonCode.ImportString(ls_SalespersonString)

Return True

end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then Return False

If Not wf_GetSalespersons() Then Return False

If Not iu_ClassFactory.uf_GetObject( "u_operCustWindowDataAccess", iu_operCustWindowDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

dw_oper_customers.ResetUpdate()

This.Event Post ue_inquire()

Return True

end function

public function string wf_builddeletestring ();Long					ll_DeletedCount, &
						ll_count
						
String				ls_DeleteString
					
u_String_Functions		lu_string					


ll_DeletedCount = dw_oper_customers.DeletedCount()

If ll_DeletedCount <= 0 Then
	SetMicroHelp('New Row Deleted')
	Return ''
End If

ls_DeleteString = ''

IF ll_DeletedCount > 0 Then
	dw_oper_customers.RowsMove( 1, 10000, Primary!, dw_oper_customers, 10000, Filter!)
	dw_oper_customers.RowsMove( 1, 10000, Delete!, dw_oper_customers, 10000, Primary!)
	For ll_count = 1 to dw_oper_customers.RowCount()
		ls_DeleteString += dw_oper_customers.GetItemString(ll_Count, 'oper_customer_id') + '~t' + &
								dw_oper_customers.GetItemString(ll_Count, 'oper_customer_name') + '~t' + &
								dw_oper_customers.GetItemString(ll_Count, 'salesperson_code') + '~t' + &
								dw_oper_customers.GetItemString(ll_Count, 'parentid_customer_id') + '~t' + &
								dw_oper_customers.GetItemString(ll_Count, 'parentid_customer_type') + '~t' + &
								'D~r~n'
	Next
	dw_oper_customers.RowsMove( 1, 10000, Primary!, dw_oper_customers, 100000, Delete!)
	dw_oper_customers.RowsMove( 1, 10000, Filter!, dw_oper_customers, 100000, Primary!)
End If	

dw_oper_customers.SelectRow(0, False)

Return ls_DeleteString

end function

on w_oper_customer.create
int iCurrent
call super::create
this.dw_oper_customers=create dw_oper_customers
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_oper_customers
end on

on w_oper_customer.destroy
call super::destroy
destroy(this.dw_oper_customers)
end on

event open;call super::open;is_Title = This.Title
end event

event closequery;call super::closequery;
If dw_oper_customers.ModifiedCount()  > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then  // Update failed - do not close window
			Return 1
		End If
	CASE 2	// Do not save changes
//	    
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If
Return 0
end event

event ue_inquire;call super::ue_inquire;String				      	ls_OperCustomers, &
							ls_OperCustWindowString, & 
							ls_InquireOption
							
Long						ll_row

Boolean  lb_rtn

u_ParameterStack		lu_ParameterStack


iu_ErrorContext.uf_Initialize()

This.Event Trigger CloseQuery()

If Message.ReturnValue <> 0 Then Return False

If Not iu_ClassFactory.uf_GetObject( "u_OperCustomerDataAccess", iu_OperCustomerDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If
If Not iu_ClassFactory.uf_GetObject( "u_OperCustWindowDataAccess", iu_OperCustWindowDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', is_InquireOption)
lu_ParameterStack.uf_Push('string', is_CustType)
lu_ParameterStack.uf_Push('string', is_CustID)
lu_ParameterStack.uf_Push('string', is_Title)
lu_ParameterStack.uf_Push('window', iw_frame)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Push('u_OperCustomerDataAccess', iu_OperCustomerDataAccess)

iu_ClassFactory.uf_GetResponseWindow( "w_oper_cust_maint_inquire", lu_ParameterStack )

lu_ParameterStack.uf_Pop('u_OperCustomerDataAccess', iu_OperCustomerDataAccess)
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('window', iw_frame)
lu_ParameterStack.uf_Pop('string', is_Title)
lu_ParameterStack.uf_Pop('string', is_CustID)
lu_ParameterStack.uf_Pop('string', is_CustType)
lu_ParameterStack.uf_Pop('string', is_InquireOption)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

If is_InquireOption = 'Cancel' Then Return False

dw_oper_customers.Reset()

SetPointer(HourGlass!)

ls_OperCustomers = is_InquireOption + '~t' + is_CustType + '~t' + is_CustID
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_OperCustomers)
iu_OperCustWindowDataAccess.uf_Retrieve( lu_ParameterStack )
lu_ParameterStack.uf_Pop('string', ls_OperCustWindowString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_oper_customers.SetRedraw(false)

ll_row = dw_oper_customers.ImportString(ls_OperCustWindowString)

If ll_row > 0 Then
	SetMicroHelp(String(ll_row) + ' Rows Retrieved')
Else
	SetMicroHelp('No Rows Retrieved')
End If

If is_InquireOption <> 'O' Then
	ll_row = dw_oper_customers.InsertRow(0)
End If

dw_oper_customers.ResetUpdate()

dw_oper_customers.SetRedraw(true)

This.SetRedraw(true)

dw_oper_customers.SetRow(1)
dw_oper_customers.SetColumn(2)

Return False

end event

event ue_postopen;call super::ue_postopen;String   ls_shipstring, ls_billstring, ls_corpstring
Long ll_rows

wf_dbconnect()

ids_shiptos = Create DataStore
ids_shiptos.DataObject = 'd_shipto_list'
ids_shiptos.SetTransObject(SQLCA)
ll_rows = ids_shiptos.Retrieve()

ls_shipstring = ids_shiptos.Object.DataWindow.Data

ids_billtos = Create DataStore
ids_billtos.DataObject = 'd_billto_list'
ids_billtos.SetTransObject(SQLCA)
ids_billtos.Retrieve()

ls_billstring = ids_billtos.Object.DataWindow.Data

ids_corporates = Create DataStore
ids_corporates.DataObject = 'd_corp_list'
ids_corporates.SetTransObject(SQLCA)
ids_corporates.Retrieve()

ls_corpstring = ids_corporates.Object.DataWindow.Data

is_DeleteInd = 'N'
end event

event ue_save;call super::ue_save;String					ls_OperCustomersString
							
Long						ll_row

u_ParameterStack						lu_ParameterStack

u_String_Functions					lu_strings


iu_ErrorContext.uf_Initialize()

IF dw_oper_customers.AcceptText() = -1 THEN Return False

This.SetRedraw(False)

// Save/Update/Insert or Delete 
// when deleting, leave the updates and inserts for the save
If is_DeleteInd = 'Y' Then
	ls_OperCustomersString = wf_BuildDeleteString()
Else
	ls_OperCustomersString = wf_BuildUpdateString()
End If


If lu_strings.nf_IsEmpty(ls_OperCustomersString) Then 
	This.SetRedraw(True)
	Return False
End If

SetMicroHelp("Wait... Updating the Database")

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

SetPointer(HourGlass!)

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_OperCustomersString)

iu_OperCustWindowDataAccess.uf_Update( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_OperCustomersString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

This.SetRedraw(True)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	// delete is not allowed if there is existing data related to operator/parent identifier
	// this will allow any other changes to be saved for updates/inserts later without reinquiring
	If is_DeleteInd = 'Y' Then 
		is_DeleteInd = 'N'
	End If
	Return False
End If

If is_DeleteInd = 'N'  Then
	dw_oper_customers.ResetUpdate()
	This.Event ue_reinquire()
	SetMicroHelp("Save Successful")
Else
	SetMicroHelp("Delete Successful")
End If

Return True

end event

event ue_delete;call super::ue_delete;Long				ll_row


ll_row = dw_oper_customers.GetSelectedRow(0)

If ll_row <= 0 Then Return

dw_oper_customers.SelectRow(ll_row, True)

If MessageBox('Delete Customer', 'Are you sure you want to delete this Customer?', Exclamation!, YesNo!, 2) = 1 Then
	If dw_oper_customers.GetItemStatus(ll_row, 0, Primary!) = NewModified! Then
		dw_oper_customers.DeleteRow(ll_row)
	Else		
		dw_oper_customers.DeleteRow(ll_row)
		is_DeleteInd = 'Y'
		This.Event ue_save()
	End If
Else
	dw_oper_customers.SelectRow(ll_row, False)
End If

is_DeleteInd = 'N'
dw_oper_customers.SetItemStatus(ll_row, 0, Delete!, NotModified!)


end event

type dw_oper_customers from datawindow within w_oper_customer
event ue_keydown pbm_dwnkey
integer x = 41
integer y = 28
integer width = 3465
integer height = 1504
integer taborder = 10
string title = "none"
string dataobject = "d_oper_customers"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event ue_keydown;Choose Case key
  	Case Key1!
		is_whatwastyped += '1'
	Case Key2!
		is_whatwastyped += '2'
	Case Key3!
		is_whatwastyped += '3'
	Case Key4!
		is_whatwastyped += '4'
	Case Key5!
		is_whatwastyped += '5'
	Case Key6!
		is_whatwastyped += '6'
	Case Key7!
		is_whatwastyped += '7'
	Case Key8!
		is_whatwastyped += '8'
	Case Key9!
		is_whatwastyped += '9'
	Case Key0!
		is_whatwastyped += '0'
	Case KeyA!
		is_whatwastyped += 'A'
	Case KeyB!
		is_whatwastyped += 'B'
	Case KeyC!
		is_whatwastyped += 'C'		
	Case KeyD!
		is_whatwastyped += 'D'
	Case KeyE!
		is_whatwastyped += 'E'
	Case KeyF!
		is_whatwastyped += 'F'		
	Case KeyG!
		is_whatwastyped += 'G'
	Case KeyH!
		is_whatwastyped += 'H'
	Case KeyI!
		is_whatwastyped += 'I'		
	Case KeyJ!
		is_whatwastyped += 'J'
	Case KeyK!
		is_whatwastyped += 'K'
	Case KeyL!
		is_whatwastyped += 'L'		
	Case KeyM!
		is_whatwastyped += 'M'
	Case KeyN!
		is_whatwastyped += 'N'
	Case KeyO!
		is_whatwastyped += 'O'		
	Case KeyP!
		is_whatwastyped += 'P'
	Case KeyQ!
		is_whatwastyped += 'Q'
	Case KeyR!
		is_whatwastyped += 'R'		
	Case KeyS!
		is_whatwastyped += 'S'
	Case KeyT!
		is_whatwastyped += 'T'
	Case KeyU!
		is_whatwastyped += 'U'		
	Case KeyV!
		is_whatwastyped += 'V'
	Case KeyW!
		is_whatwastyped += 'W'		
	Case KeyX!
		is_whatwastyped += 'X'
	Case KeyY!
		is_whatwastyped += 'Y'	
	Case KeyZ!
		is_whatwastyped += 'Z'			
//	Case KeySpaceBar!
//	is_whatwastyped += ' '			
END choose

end event

event clicked;Long		ll_current_row, ll_row_cnt
String     ls_oper_custid, ls_oper_custname, ls_salespersoncode

DatawindowChild   ldwc_CustomerList

dw_oper_customers.GetChild('parentid_customer_id', ldwc_CustomerList)

If dwo.name = 'b_add' then
	
	ls_oper_custid = This.GetItemString(row, 'oper_customer_id')
	ls_oper_custname = This.GetItemString(row, 'oper_customer_name')
	ls_salespersoncode = This.GetItemString(row, 'salesperson_code')
	ll_current_row = This.InsertRow(row + 1)
	This.ScrollToRow(ll_current_row)
	This.SetItem(ll_current_row, 'oper_customer_id', ls_oper_custid)
	This.SetItem(ll_current_row, 'oper_customer_name', ls_oper_custname)
	This.SetItem(ll_current_row, 'customer_option', is_InquireOption)
	
	This.SetItem(ll_current_row, 'salesperson_code', ls_salespersoncode)
	
	If isNull(This.GetItemString(ll_current_row,'parentid_customer_type')) or This.GetItemString(ll_current_row,'parentid_customer_type')  <= ' ' Then
			This.SetItem(ll_current_row, 'parentid_customer_type', "B")
			ldwc_CustomerList.Reset()
			ll_row_cnt = ldwc_CustomerList.ImportString(ids_billtos.Object.DataWindow.Data) 
	End If
	This.SelectRow(0, False)
	This.SelectRow(row + 1, True)
	
	This.SetColumn(4)
	
Else
	This.SelectRow(0, False)
	This.SelectRow(row, True)
End If

end event

event constructor;									
This.InsertRow(0)
end event

event itemchanged;DatawindowChild   ldwc_CustomerList, ldwc_temp
Long					ll_row_cnt, ll_count, ll_rows
String 				ls_find_string

dw_oper_customers.GetChild('parentid_customer_id', ldwc_CustomerList)

Choose Case This.GetColumnName()
	Case 'parentid_customer_type'
		Choose Case data
			Case 'S'
				ldwc_CustomerList.Reset()
				ll_row_cnt = ldwc_CustomerList.ImportString(ids_shiptos.Object.DataWindow.Data) 
			Case 'B'
				ldwc_CustomerList.Reset()
				ll_row_cnt = ldwc_CustomerList.ImportString(ids_billtos.Object.DataWindow.Data) 
			Case 'C'
				ldwc_CustomerList.Reset()
				ll_row_cnt = ldwc_CustomerList.ImportString(ids_corporates.Object.DataWindow.Data) 
		End Choose
	Case 'oper_customer_id'  //new row - Insert
		This.SetItem(row, 'customer_option', is_InquireOption)
		If is_InquireOption = 'P' Then
			This.SetItem(row, 'parentid_customer_type', is_CustType)
			This.SetItem(row, 'parentid_customer_id', is_CustID)
		Else
			If isNull(This.GetItemString(row,'parentid_customer_type')) or This.GetItemString(row,'parentid_customer_type')  <= ' ' Then
				This.SetItem(row, 'parentid_customer_type', "B")
				ldwc_CustomerList.Reset()
				ll_row_cnt = ldwc_CustomerList.ImportString(ids_billtos.Object.DataWindow.Data) 
			End If
			If is_InquireOption = 'A' Then
				ll_rows = row - 1
				For ll_count = 1 to ll_rows
					If This.GetItemString(ll_count, 'oper_customer_id') = data Then
						This.SetItem(row, 'oper_customer_name', This.GetItemString(ll_count, 'oper_customer_name'))
						This.SetItem(row, 'salesperson_code', This.GetItemString(ll_count, 'salesperson_code'))
						SetMicroHelp('Operator ' + data + ' already exists, name and salesperson ignored.')
						Exit
					End If
				Next
			End If
		End If
	Case 'salesperson_code'
		SetMicroHelp('')
		If data > '   ' Then 
			This.GetChild('salesperson_code', ldwc_temp)
			ls_find_string = "smancode = '" + data + "'"
			If ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount() ) <= 0 Then
				SetMicroHelp( 'Salesperson Code is Not Valid!')
				Return 1
			End If
		End If
	Case 'parentid_customer_id'
		SetMicroHelp('')
		If data > '   ' Then 
			This.GetChild('parentid_customer_id', ldwc_temp)
			ls_find_string = "customer_id = '" + data + "'"
			If ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount() ) <= 0 Then
				SetMicroHelp( 'Parent Identifier is Not Valid!')
				Return 1
			End If
		End If		
End Choose

If row = This.RowCount() and is_InquireOption <> 'O' Then 
	This.InsertRow(0)
End If
end event

event itemerror;Return 1
end event

event editchanged;String		 ls_FindString, ls_temp

Long		ll_Row

DataWindowChild	ldwc_temp
	
IF KEYDOWN(KeyBack!) OR KEYDOWN(KeyLeftArrow!) OR KEYDOWN(KeyEnd!) OR  KEYDOWN(KeyHome!) OR  KEYDOWN( KeyRightArrow!)&
							OR KEYDOWN(Keydelete!) Then RETURN
							
if isnull(dwo) then
	Return
else
	Choose Case String(dwo.Name)
		Case 'salesperson_code'
				ls_FindString = "salesperson_code >= '"+Trim(Left(is_whatwastyped,len(is_whatwastyped)))+"'"
		Case 'parentid_customer_id'
				ls_FindString = "parentid_customer_id >= '"+Trim(Left(is_whatwastyped,len(is_whatwastyped)))+"'"				
		Case Else
			Return
	End Choose
end if	

if is_whatwastyped > '' Then
	
	If dwo.Name = 'salesperson_code' Then
		This.GetChild('salesperson_code', ldwc_temp)
	
		ll_Row = ldwc_temp.Find( ls_FindString, 1, ldwc_temp.RowCount()+1)
	
		ls_temp = ldwc_temp.Describe("Datawindow.Data")
	
		if ll_Row > 0 Then
			this.SetItem(row, "salesperson_code", ldwc_temp.GetItemString(ll_Row, "salesperson_code"))
			ldwc_temp.ScrollToRow(ll_row)
			ldwc_temp.SelectRow(0,FALSE)
			ldwc_temp.SelectRow( ll_Row, TRUE)
		End If
	End If
	
	If dwo.Name = 'parentid_customer_id' Then
		This.GetChild('parentid_customer_id', ldwc_temp)
	
		ll_Row = ldwc_temp.Find( ls_FindString, 1, ldwc_temp.RowCount()+1)
	
		ls_temp = ldwc_temp.Describe("Datawindow.Data")
	
		if ll_Row > 0 Then
			this.SetItem(row, "parentid_customer_id", ldwc_temp.GetItemString(ll_Row, "parentid_customer_id"))
			ldwc_temp.ScrollToRow(ll_row)
			ldwc_temp.SelectRow(0,FALSE)
			ldwc_temp.SelectRow( ll_Row, TRUE)
		End If
	End If
	
End If	

Return 0
end event

