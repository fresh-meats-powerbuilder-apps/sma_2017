﻿$PBExportHeader$u_transcustwindownetwise.sru
forward
global type u_transcustwindownetwise from u_transcustwindowdataaccess
end type
end forward

global type u_transcustwindownetwise from u_transcustwindowdataaccess
end type
global u_transcustwindownetwise u_transcustwindownetwise

forward prototypes
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the Transfer Customer Window Data
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Transfer Customer
</USAGE> Window Data from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_InputString, &
									ls_OutputString, &
									ls_GroupID
								
u_sma001							lu_sma001

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_inputstring)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		return False
	End If
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		return False
	End if
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if

//lu_sma001.uf_smas00ar_GetTransCustWindowData("w_TransferCustomer", &
//													"uf_retrieve", &
//													"", &
//													ls_userid, &
//													ls_inputstring, &
//													ls_outputstring, &
//													lu_ErrorContext)

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_outputstring)

If Not lu_ErrorContext.uf_IsSuccessful() Then return False

return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------
uf_Update()

<DESC> Update the Transfer Customer Window Data
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will Update the Transfer Customer
</USAGE> Window Data from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_HeaderString, &
									ls_DetailString, &
									ls_GroupID
								
u_sma001							lu_sma001

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_DetailString)
au_ParameterStack.uf_Pop('string', ls_HeaderString)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

//lu_sma001.uf_smas01ar_updateTransCustWindowData("w_TransferCustomer", &
//													"uf_update", &
//													"", &
//													ls_userid, &
//													ls_headerstring, &
//													ls_detailstring, &
//													lu_ErrorContext)

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_HeaderString)
au_ParameterStack.uf_Push('string', ls_DetailString)

If Not lu_ErrorContext.uf_IsSuccessful() Then return False

return True
end function

on u_transcustwindownetwise.create
call super::create
end on

on u_transcustwindownetwise.destroy
call super::destroy
end on

