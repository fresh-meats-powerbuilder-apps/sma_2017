﻿$PBExportHeader$u_opercustwindowdataaccess.sru
forward
global type u_opercustwindowdataaccess from nonvisualobject
end type
end forward

global type u_opercustwindowdataaccess from nonvisualobject
end type
global u_opercustwindowdataaccess u_opercustwindowdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_opercustwindowdataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_opercustwindowdataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

