﻿$PBExportHeader$u_transcustwindowdataaccess.sru
forward
global type u_transcustwindowdataaccess from nonvisualobject
end type
end forward

global type u_transcustwindowdataaccess from nonvisualobject
end type
global u_transcustwindowdataaccess u_transcustwindowdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_transcustwindowdataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_transcustwindowdataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

