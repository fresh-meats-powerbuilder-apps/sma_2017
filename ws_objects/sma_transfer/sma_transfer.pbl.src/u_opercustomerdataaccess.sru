﻿$PBExportHeader$u_opercustomerdataaccess.sru
forward
global type u_opercustomerdataaccess from nonvisualobject
end type
end forward

global type u_opercustomerdataaccess from nonvisualobject
end type
global u_opercustomerdataaccess u_opercustomerdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_opercustomerdataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_opercustomerdataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

