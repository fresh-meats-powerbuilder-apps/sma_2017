﻿$PBExportHeader$u_opercustomerwebservice.sru
forward
global type u_opercustomerwebservice from u_opercustomerdataaccess
end type
end forward

global type u_opercustomerwebservice from u_opercustomerdataaccess
end type
global u_opercustomerwebservice u_opercustomerwebservice

forward prototypes
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the Operator Customer
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Operator Customers
</USAGE> from a rpc on the mainframe using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_OutputString, &
									ls_GroupID
								
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then    
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if

lu_smas01sr.uf_smas32er_get_oper_customers("w_oper_cust_maint_inquire", &
													"uf_retrieve", &
													"", &
													ls_userid, &
													ls_password, &
													ls_outputstring, &
													lu_ErrorContext)
													

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('string', ls_outputstring)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_outputstring)

return True
end function

on u_opercustomerwebservice.create
call super::create
end on

on u_opercustomerwebservice.destroy
call super::destroy
end on

