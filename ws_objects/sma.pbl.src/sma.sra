﻿$PBExportHeader$sma.sra
forward
global type sma from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type sma from application
string appname = "sma"
long richtextedittype = 2
long richtexteditversion = 1
string richtexteditkey = ""
end type
global sma sma

type variables
u_AbstractClassFactory	iu_ClassFactory
end variables

on sma.create
appname="sma"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on sma.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;u_ErrorContext			lu_ErrorContext
u_ObjectList	lu_ObjectList
u_AppController	lu_AppController
u_abstractConnection	lu_Connection


This.displayname = 'Sales and Marketing Accounting'

lu_ObjectList	=	CREATE	u_ObjectList
lu_ObjectList.uf_Initialize()

lu_Connection = CREATE u_AbstractConnection
iu_ClassFactory = CREATE u_ClassFactory
iu_ClassFactory.uf_Initialize(lu_ObjectList,lu_Connection)
iu_ClassFactory.uf_GetObject('ApplicationController',lu_AppController)

if isvalid(lu_AppController) then
	lu_AppController.uf_Initialize(iu_ClassFactory)
else
	MessageBox('Error','Application Controller can not be initialized')
end if



end event

event systemerror;u_abstracterrorcontext lu_errorcontext
u_abstractnotificationcontroller lu_notification

iu_classfactory.uf_GetObject("u_errorcontext", lu_errorcontext)
lu_errorcontext.uf_Initialize()

If Not iu_classfactory.uf_GetObject("u_systemerrornotificationcontroller", lu_notification) Then
	If IsValid(lu_notification) Then
		lu_notification.uf_Initialize(iu_classfactory)
	Else
		Return
	End If
Else
	Return
End If

lu_notification.uf_Display(lu_errorcontext)
end event

