﻿$PBExportHeader$w_rebateselect.srw
forward
global type w_rebateselect from w_abstractresponse
end type
type dw_select from datawindow within w_rebateselect
end type
type dw_selected from datawindow within w_rebateselect
end type
type cb_1 from commandbutton within w_rebateselect
end type
type cb_2 from commandbutton within w_rebateselect
end type
type dw_productcodeshort from datawindow within w_rebateselect
end type
type st_1 from statictext within w_rebateselect
end type
type dw_selectowner from datawindow within w_rebateselect
end type
type dw_selectlocationtype from datawindow within w_rebateselect
end type
end forward

global type w_rebateselect from w_abstractresponse
integer x = 59
integer y = 248
integer width = 4251
integer height = 1548
string title = ""
event ue_selectcustomers ( )
event ue_get_data ( string as_data )
dw_select dw_select
dw_selected dw_selected
cb_1 cb_1
cb_2 cb_2
dw_productcodeshort dw_productcodeshort
st_1 st_1
dw_selectowner dw_selectowner
dw_selectlocationtype dw_selectlocationtype
end type
global w_rebateselect w_rebateselect

type variables
String		is_selected, &
			is_type, &
             is_whatwastyped, &
             is_userid, &
             is_password, &
             is_owner, &
	         is_exclude_ind_div, &
   	        	is_exclude_ind_grp, &
              is_product, &
			is_rebate_type, &
			is_upd_product_code, &
			is_ext_product_code, &
			is_rebate_option, &
			is_upd_customer_code, &
			is_ext_customer_code

Date			idt_header_start_date, idt_header_end_date

Long			il_last_seq_no_product, il_last_seq_no_group, il_rebate_id, il_upd_group_id, il_ext_group_id

decimal		idc_detail_rebate_rate

Integer     ii_keystyped, &
            ii_last_row_highlighted = 0

datastore   ids_division, &
                 ids_productgroup, &
			    ids_product, &
			   ids_location, &
				ids_customergroup, &
				ids_group_list
				

u_AbstractErrorContext				iu_ErrorContext
u_RebateDataAccess					iu_RebateDataAccess
u_ClassFactory							iu_ClassFactory
u_AbstractNotificationController	iu_Notification
u_AbstractParameterStack			iu_ParameterStack
u_DivisionDataAccess					iu_DivisionDataAccess
u_ProductDataAccess					iu_ProductDataAccess
u_LocationDataAccess				iu_LocationDataAccess
u_GroupDataAccess					iu_GroupDataAccess
u_AbstractTutlTypeDataAccess		iu_TutlTypeDataAccess
u_dwselect								iu_DwSelect
end variables

forward prototypes
public function boolean wf_updateselectlist ()
public function boolean wf_getdivisions (string as_division, ref string as_divisionstring)
public function boolean wf_fillrbtcreditdddw ()
public function boolean wf_fillownerdddw ()
public function boolean wf_getproducts (string as_product, ref string as_productstring)
public function boolean wf_productgroupcheck (string as_group_id, string as_record_type)
public function boolean wf_fillprodcurrcode ()
public function boolean wf_fillproduom ()
public subroutine wf_dbconnection ()
protected function long wf_check_dates ()
public function boolean wf_getlocations (string as_location, ref string as_locationstring)
public function boolean wf_customergroupcheck (string as_group_id, string as_record_type)
public function string wf_retrievegrouplist (string as_group_type_ind)
public function boolean wf_filterlocations (string as_loctype)
end prototypes

public function boolean wf_updateselectlist ();Long				ll_Count, &
					ll_SelectedCount, &
					ll_SelectCount, &
					ll_FindRow
					
String			ls_Select, &
					ls_FindString


ll_SelectedCount = dw_selected.RowCount()
ll_SelectCount = dw_Select.RowCount()

For ll_count = 1 to ll_SelectedCount
		ls_Select = dw_selected.GetItemString(ll_count, 1)
		ls_FindString = 'Trim(#1) = "' + Trim(ls_Select) + '"'
		ll_FindRow = dw_Select.Find(ls_FindString, 1, ll_SelectCount + 1)
		If ll_FindRow > 0 Then
			dw_Select.DeleteRow(ll_FindRow)
		End If
Next

Return True

end function

public function boolean wf_getdivisions (string as_division, ref string as_divisionstring);u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_DivisionDataAccess", iu_DivisionDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_DivisionDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_division)

iu_DivisionDataAccess.uf_RetrieveDivisions(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_DivisionString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_fillrbtcreditdddw ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'REBCRTYP'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_selected.GetChild('tonnage_rebate_credit', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillownerdddw ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'OWNERGRP'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_selectowner.GetChild('owner', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_getproducts (string as_product, ref string as_productstring);u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ProductDataAccess", iu_ProductDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_ProductDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_Product)

iu_ProductDataAccess.uf_RetrieveProducts(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_ProductString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_productgroupcheck (string as_group_id, string as_record_type);String					ls_HeaderString,&
							ls_DetailString, &
							ls_AppName, &
							ls_WindowName, &
							ls_temp

Long						ll_num

u_ParameterStack		lu_ParameterStack
u_String_Functions	lu_strings

SetPointer(HourGlass!)
 
ls_WindowName = 'RbtSelect'

If as_record_type = 'S' Then
	ls_DetailString += string(il_rebate_id) + '~t' + as_group_id + '~r~n'
Else
	ls_DetailString = as_group_id
End If

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_RebateDataAccess", iu_RebateDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_parameterstack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_AppName)
lu_ParameterStack.uf_Push('string', ls_WindowName)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_RebateDataAccess.uf_productgroupcheck(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('string', ls_WindowName)
lu_ParameterStack.uf_Pop('string', ls_AppName)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

If ls_DetailString  >  ' ' then
	FOR ll_num = 1 TO 6
		ls_temp = lu_strings.nf_gettoken(ls_DetailString,'~t')
		CHOOSE CASE ll_num
			CASE 1  
				Continue
			CASE 2  
				il_upd_group_id = Long(ls_temp)
			CASE 3  
				is_upd_product_code = ls_temp
			CASE 4  
				Continue
			CASE 5  
				il_ext_group_id = Long(ls_temp)
			CASE 6  
				is_ext_product_code = ls_temp
		END CHOOSE
	NEXT
	MessageBox("Invalid Selection", String("Selected Group & Product: " + string(il_upd_group_id) + & 
					" & " + is_upd_product_code + "Exist in the following Group & Product: " + string(il_ext_group_id) + &
					" & " + is_ext_product_code))
	Return False
End If

Return True
end function

public function boolean wf_fillprodcurrcode ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'CURRCODE'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_selected.GetChild('currency_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillproduom ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()
//dmk changed to rebuom from puom
ls_TutlType = 'REBUOM'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_selected.GetChild('unit_of_measure', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public subroutine wf_dbconnection ();string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

//SQLCA = Create transaction // Create a transaction object
SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")

//if Upper(SQLCA.dbms) = Upper("SNC SQL Native Client") Then					 // Connecting to a SQL Server 
	SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
	SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password
//Else
	//SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
//End If

if SQLCA.database = 'false' or SQLCA.dbParm = 'false' then 							 // "SMA DATABASE" Not found in the INI
	SQLCA.database = ProfileString(ls_inifile, "DEFAULT DATABASE", "database", "pblocaldb")
	SQLCA.dbParm = ProfileString(ls_inifile, "DEFAULT DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
end if

Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode = -1 Then
	// do nothing
Else
	If SQLCA.SQLCode <> 0 then
		Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
		Disconnect using SQLCA;
		Return
	End If
End if
end subroutine

protected function long wf_check_dates ();String						ls_current_key, ls_next_key
Long							ll_row_count, ll_count, ll_count2
Date							ldt_next_start, ldt_current_end, ldt_current_rec_start, ldt_current_rec_end
u_projectfunctions		lu_projectfunctions
datawindow					ldw_datawindow

//  Return Types 0 = successful; 1 = header date range error; 2 = detail date overlapping

dw_selected.SetReDraw(False)

// P = Products; G = Product Groups; S = Customer Groups; D = Divisions; O = Locations

If is_rebate_option = 'A' Then
	Choose Case is_type
		Case 'P'
			dw_selected.SetSort("#1 A, #6 A, #7 A")
		Case 'G', 'S' 
			dw_selected.SetSort("#1 A, #8 A, #9 A")
		Case 'O'
			dw_selected.SetSort("#1 A, #6 A, #7 A")
		Case 'D'
			dw_selected.SetSort("#1 A, #6 A, #7 A")
	End Choose
End If

dw_selected.Sort()
dw_selected.SetReDraw(True)

ll_row_count = dw_selected.rowcount()
If ll_row_count > 0 then	
//  Debugging purposes.
			string ls_debug
			ls_debug = dw_selected.describe("DataWindow.Data")

			For ll_count = 1 To ll_row_count
				//If ll_count = ll_row_count Then
				//	Continue
				//Else
//  Make sure the start & end dates are within the same date range as the header.
					ldt_current_rec_start = dw_selected.getitemdate(ll_count, "start_date")
					ldt_current_rec_end = dw_selected.getitemdate(ll_count, "end_date")
					If IsNull(ldt_current_rec_start)	or IsNull(ldt_current_rec_end) Then						
						Return 4
					End If		
					If idt_header_start_date > ldt_current_rec_start Then
							dw_selected.SelectRow(ll_count, True)
							Return 1
					End If
					
					If	idt_header_end_date < ldt_current_rec_end Then
							dw_selected.SelectRow(ll_count, True)
							Return 1
					End If
					
					If ldt_current_rec_start > ldt_current_rec_end Then
							dw_selected.SelectRow(ll_count, True)
							Return 3
					End If
				//End If
			Next

			For ll_count = 1 To ll_row_count
				If ll_count = ll_row_count Then
					Continue
				else
					Choose Case is_type
						Case 'P'
							ls_current_key = Trim(dw_selected.GetItemString(ll_count, "product_code"))
							ls_next_key = Trim(dw_selected.GetItemString(ll_count + 1, "product_code"))
						Case 'G', 'S'
							ls_current_key = Trim(dw_selected.GetItemString(ll_count, "group_id"))
							ls_next_key = Trim(dw_selected.GetItemString(ll_count + 1, "group_id"))
						Case 'O'
							ls_current_key = Trim(dw_selected.GetItemString(ll_count, "location_code")) + Trim(dw_selected.GetItemString(ll_count, "location_type"))
							ls_next_key = Trim(dw_selected.GetItemString(ll_count + 1, "location_code")) + Trim(dw_selected.GetItemString(ll_count + 1, "location_type"))						
						Case 'D'
							ls_current_key = Trim(dw_selected.GetItemString(ll_count, "division_code"))
							ls_next_key = Trim(dw_selected.GetItemString(ll_count + 1, "division_code"))							
					End Choose
					//RightTrim(ls_current_key)
					//LeftTrim(ls_current_key)
					//RightTrim(ls_next_key)
					//LeftTrim(ls_next_key)
					//ls_current_key = Trim(ls_current_key)
					//ls_next_key = Trim(ls_next_key)
//  Make sure the product or group or location or division is of the same kind.
					If ls_current_key  =  ls_next_key  Then
						ldt_current_end = dw_selected.getitemdate(ll_count, "end_date")
						ldt_next_start = dw_selected.getitemdate(ll_count + 1, "start_date")
//  Make sure the start & end dates are not overlapping.
						If ldt_current_end >= ldt_next_start Then
							dw_selected.SelectRow(ll_count, True)
							Return 2
						End If
					End If
				End If
			Next
End If
return 0
end function

public function boolean wf_getlocations (string as_location, ref string as_locationstring);u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_LocationDataAccess", iu_LocationDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_LocationDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_location)

iu_LocationDataAccess.uf_RetrieveLocations(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_LocationString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_customergroupcheck (string as_group_id, string as_record_type);String					ls_HeaderString,&
							ls_DetailString, &
							ls_AppName, &
							ls_WindowName, &
							ls_temp

Long						ll_num

u_ParameterStack		lu_ParameterStack
u_String_Functions	lu_strings

SetPointer(HourGlass!)

ls_WindowName = 'RbtSelect'

If as_record_type = 'S' Then
	ls_DetailString += string(il_rebate_id) + '~t' + as_group_id + '~r~n'
Else
	ls_DetailString = as_group_id
End If

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_RebateDataAccess", iu_RebateDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_parameterstack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_AppName)
lu_ParameterStack.uf_Push('string', ls_WindowName)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_RebateDataAccess.uf_customergroupcheck(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('string', ls_WindowName)
lu_ParameterStack.uf_Pop('string', ls_AppName)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

If ls_DetailString  >  ' ' then
	FOR ll_num = 1 TO 6
		ls_temp = lu_strings.nf_gettoken(ls_DetailString,'~t')
		CHOOSE CASE ll_num
			CASE 1  
				Continue
			CASE 2  
				il_upd_group_id = Long(ls_temp)
			CASE 3  
				is_upd_customer_code = ls_temp
			CASE 4  
				Continue
			CASE 5  
				il_ext_group_id = Long(ls_temp)
			CASE 6  
				is_ext_customer_code = ls_temp
		END CHOOSE
	NEXT
	MessageBox("Invalid Selection", String("Selected Group & Customer: " + string(il_upd_group_id) + & 
					" & " + is_upd_customer_code + "Exist in the following Group & Customer: " + string(il_ext_group_id) + &
					" & " + is_ext_customer_code))
	Return False
End If

Return True
end function

public function string wf_retrievegrouplist (string as_group_type_ind);u_ParameterStack          lu_ParameterStack

String    ls_appname, ls_windowname, ls_inquirestring, ls_grouptype, ls_group_id

Long      ll_ret, ll_sub

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_GroupDataAccess", iu_GroupDataAccess, iu_ErrorContext ) Then
     If Not iu_ErrorContext.uf_IsSuccessful( ) Then
           iu_Notification.uf_Display(iu_ErrorContext)
            Return ls_inquirestring
     End If
End If

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = as_group_type_ind

ls_appname = GetApplication().AppName
ls_windowname = "w_rebateselect"
ls_inquirestring = ls_grouptype

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_inquirestring)
iu_GroupDataAccess.uf_list_groups(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_inquireString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)   

If Not iu_ErrorContext.uf_IsSuccessful() Then 
                lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
                lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
                lu_ParameterStack.uf_Push('string', ls_appname)
                lu_ParameterStack.uf_Push('string', ls_windowname)
                lu_ParameterStack.uf_Push('string', ls_inquireString)
                return ls_inquirestring
End If

If as_group_type_ind = 'P' Then  // Product Group
	ids_productgroup = Create datastore
	ids_productgroup.DataObject = "d_rebateprodgrpselect"
	ids_productgroup.Reset()

	ids_group_list = Create datastore
    ids_group_list.DataObject = "d_group_list"
    ids_group_list.Reset()
    ll_ret = ids_group_list.ImportString(ls_inquirestring)
    
   For ll_sub = 1 to ll_ret
		ids_productgroup.InsertRow(0)
		ls_group_id = string(long(ids_group_list.GetItemString(ll_sub,'group_id')))
		ids_productgroup.SetItem( ll_sub, 'group_id', ls_group_id )
		ids_productgroup.SetItem( ll_sub, 'group_description', ids_group_list.GetItemString(ll_sub,'group_desc'))
		ids_productgroup.SetItem( ll_sub, 'group_owner', ids_group_list.GetItemString(ll_sub,'group_owner'))
	Next		
	
	Return ids_productgroup.describe( "DataWindow.Data")
	
Else	// 'C'  Customer Group
	ids_customergroup = Create datastore
	ids_customergroup.dataobject = "d_rebatecustgrpselect"
	ids_customergroup.reset( )
	
	ids_group_list = Create datastore
    ids_group_list.DataObject = "d_group_list"
    ids_group_list.Reset()
    ll_ret = ids_group_list.ImportString(ls_inquirestring)

   For ll_sub = 1 to ll_ret
		ids_customergroup.InsertRow(0)
		ls_group_id = string(long(ids_group_list.GetItemString(ll_sub,'group_id')))
		ids_customergroup.SetItem( ll_sub, 'group_id', ls_group_id )
		ids_customergroup.SetItem( ll_sub, 'group_description', ids_group_list.GetItemString(ll_sub,'group_desc'))
		ids_customergroup.SetItem( ll_sub, 'group_owner', ids_group_list.GetItemString(ll_sub,'group_owner'))
	Next		
	
	Return ids_customergroup.describe( "DataWindow.Data")
	
End If
	
Return ls_inquirestring
end function

public function boolean wf_filterlocations (string as_loctype);String  ls_FilterString, ls_datastring

u_String_Functions lu_StringFunctions


dw_select.SetRedraw( false)
dw_select.SetFilter( '')
dw_select.Filter( )
		
ls_FilterString = 'location_type = "' + as_loctype + '"'
dw_select.SetFilter( ls_FilterString)

If as_loctype = 'C' Then
	dw_select.SetSort("name asc")
Else
	dw_select.SetSort("location_code asc")
End If

dw_select.Filter( )
dw_select.Sort( )

ls_datastring = dw_select.object.datawindow.data

dw_select.SetRedraw( true)

Return true
end function

on w_rebateselect.create
int iCurrent
call super::create
this.dw_select=create dw_select
this.dw_selected=create dw_selected
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_productcodeshort=create dw_productcodeshort
this.st_1=create st_1
this.dw_selectowner=create dw_selectowner
this.dw_selectlocationtype=create dw_selectlocationtype
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_select
this.Control[iCurrent+2]=this.dw_selected
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.dw_productcodeshort
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.dw_selectowner
this.Control[iCurrent+8]=this.dw_selectlocationtype
end on

on w_rebateselect.destroy
call super::destroy
destroy(this.dw_select)
destroy(this.dw_selected)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_productcodeshort)
destroy(this.st_1)
destroy(this.dw_selectowner)
destroy(this.dw_selectlocationtype)
end on

event close;iu_ErrorContext.uf_initialize()
iu_ParameterStack.uf_Initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('String', is_Selected)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

iu_ParameterStack.uf_Push('datastore', ids_division)
iu_ParameterStack.uf_Push('datastore', ids_product)
iu_ParameterStack.uf_Push('datastore', ids_productgroup)
iu_ParameterStack.uf_Pop('datastore', ids_location)
iu_ParameterStack.uf_Pop('datastore', ids_customergroup)

iu_ParameterStack.uf_Push('Long', il_last_seq_no_group)
iu_ParameterStack.uf_Push('Long', il_last_seq_no_product)

iu_ParameterStack.uf_Push('String', is_exclude_ind_grp)
iu_ParameterStack.uf_Push('String', is_exclude_ind_div)	

iu_ParameterStack.uf_Push('String', is_type)
CloseWithReturn(This, iu_ParameterStack)

end event

event open;call super::open;u_string_functions		lu_String

iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('Long', il_rebate_id)
iu_ParameterStack.uf_Pop('Long', il_last_seq_no_group)
iu_ParameterStack.uf_Pop('Long', il_last_seq_no_product)
iu_ParameterStack.uf_Pop('Decimal', idc_detail_rebate_rate)
iu_ParameterStack.uf_Pop('Date', idt_header_end_date)
iu_ParameterStack.uf_Pop('Date', idt_header_start_date)

iu_ParameterStack.uf_Pop('String', is_type)
iu_ParameterStack.uf_Pop('String', is_rebate_option)
iu_ParameterStack.uf_Pop('String', is_rebate_type)

iu_ParameterStack.uf_Pop('String', is_exclude_ind_div)
iu_ParameterStack.uf_Pop('String', is_exclude_ind_grp)

iu_ParameterStack.uf_Pop('datastore', ids_productgroup)
iu_ParameterStack.uf_Pop('datastore', ids_product)
iu_ParameterStack.uf_Pop('datastore', ids_division)
iu_ParameterStack.uf_Pop('datastore', ids_location)
iu_ParameterStack.uf_Pop('datastore', ids_customergroup)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('String', is_selected)

iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_dwselect', iu_DwSelect)


end event

event ue_postopen;String					ls_String, ls_owner, ls_loctype

u_string_functions	lu_string

SetPointer(HourGlass!)
This.SetRedraw(False)

Choose Case is_type
	Case 'D'
		This.Title = 'Division Selection'
		dw_select.DataObject = 'd_rebatedivisionselect'
		
		if is_rebate_option = "V" then
			dw_selected.DataObject = 'd_rebatedivisionselected'
		dw_selected.object.tonnage_rebate_credit.protect = 0
		else
			dw_selected.DataObject = 'd_rebatedivisionselectedaccrual'
			If is_rebate_type = 'Q' then
				dw_selected.object.exclude_ind.protect = 1
				dw_selected.object.rebate_rate.protect = 1
			End If
//			If is_rebate_type = 'P' then
//				dw_selected.object.exclude_ind.protect = 1
//				dw_selected.object.rebate_rate.protect = 1
//				dw_selected.object.start_date.protect = 1
//				dw_selected.object.end_date.protect = 1
//			End If			
		end if

		If Not wf_GetDivisions('', ls_String) Then 
			This.SetRedraw(True)
			Return
		End If
	Case 'G'  //Product Group
		dw_selectowner.visible = True
		wf_fillownerdddw()
		This.Title = 'Product Group Selection'
		dw_select.DataObject = 'd_rebateprodgrpselect'
		
		if is_rebate_option = "V" then
			dw_selected.DataObject = 'd_rebateprodgrpselected'
			dw_selected.object.tonnage_rebate_credit.protect = 0
		else
			dw_selected.DataObject = 'd_rebateprodgrpselectedaccrual'
			If is_rebate_type = 'Q' then
				dw_selected.object.exclude_ind.protect = 1
				dw_selected.object.rebate_rate.protect = 1
			End If
		end if

		ls_owner = dw_selectowner.GetItemString(1, 'owner')

		dw_select.Reset()   
		ls_String = wf_RetrieveGroupList('P')
		
		IF lu_String.nf_IsEmpty('is_selected') Then
			This.SetRedraw(True)
			Return
		End If
		
	Case 'P'
		This.Title = 'Product Selection'
		dw_select.DataObject = 'd_rebateproductselect'	
		
		if is_rebate_option = "V" then
			dw_selected.DataObject = 'd_rebateproductselected'
			dw_selected.object.tonnage_rebate_credit.protect = 0
		else
			dw_selected.DataObject = 'd_rebateproductselected2'
			If is_rebate_type = 'Q' then
				dw_selected.object.include_ind.protect = 1
				dw_selected.object.rebate_rate.protect = 1
			End If
			If is_rebate_type = 'P' then
				dw_selected.object.include_ind.protect = 1
				dw_selected.object.rebate_rate.protect = 1
			End If			
		end if
		dw_selected.object.include_ind.protect = 0
		If Not wf_GetProducts('', ls_String) Then 
			This.SetRedraw(True)
			Return
		End If
	Case 'O'
		dw_selectlocationtype.visible = True
		This.Title = 'Location Selection'
		dw_select.DataObject = 'd_rebatelocationselect'
		dw_selected.DataObject = 'd_rebatelocationselected'
		If is_rebate_type = 'Q' then
			dw_selected.object.exclude_ind.protect = 1
			dw_selected.object.rebate_rate.protect = 1
		End If
		If is_rebate_type = 'P' then
			dw_selected.object.exclude_ind.protect = 1
			dw_selected.object.rebate_rate.protect = 1
//			dw_selected.object.start_date.protect = 1
//			dw_selected.object.end_date.protect = 1
		End If		
		
		If Not wf_GetLocations('', ls_String) Then 
			This.SetRedraw(True)
			Return
		End If		
	Case 'S'  // Customer Group 
		dw_selectowner.visible = True
		wf_fillownerdddw()
		This.Title = 'Customer Group Selection'
		dw_select.DataObject = 'd_rebatecustgrpselect'
		dw_selected.DataObject = 'd_rebatecustgrpselected'

		ls_owner = dw_selectowner.GetItemString(1, 'owner')

		dw_select.Reset()   
		ls_string = wf_RetrieveGroupList('C')
		
 		If is_rebate_type = 'Q' then
			dw_selected.object.exclude_ind.protect = 1
			dw_selected.object.rebate_rate.protect = 1
		End If
 		If is_rebate_type = 'P' then
//			dw_selected.object.exclude_ind.protect = 1
			dw_selected.object.rebate_rate.protect = 1
//			dw_selected.object.start_date.protect = 1
//			dw_selected.object.end_date.protect = 1
		End If		
		
		IF lu_String.nf_IsEmpty('is_selected') Then
			This.SetRedraw(True)
			Return
		End If
		
End Choose

If Not wf_fillrbtcreditdddw() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	This.SetRedraw(True)
	Return
End If

If Not wf_fillprodcurrcode() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	This.SetRedraw(True)
	Return
End If
If Not wf_fillproduom() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	This.SetRedraw(True)
	Return
End If

dw_select.ImportString(ls_String)

If is_type = 'G' or is_type = 'S' Then	
	dw_select.SetSort("long(group_id) asc")
	dw_select.SetFilter("group_owner = '"+ ls_owner +" ' ")
	dw_select.Filter()
	dw_select.Sort()
End If

// default to Plant locations
If is_type = 'O' Then
	ls_loctype = 'P'
	dw_select.SetFilter( "location_type = '"+ ls_loctype +" ' ")
	dw_select.SetSort("location_code asc")
	dw_select.Filter( )
	dw_select.Sort( )	
End If

If Not lu_String.nf_IsEmpty('is_selected') Then
	dw_selected.ImportString(is_selected)
End If

is_selected = 'Cancel'
dw_select.ResetUpdate()

If is_rebate_option = 'V' Then
	If Len(Trim(dw_selected.object.datawindow.data)) > 0 Then
		This.wf_UpdateSelectList()
	End If
End If

This.SetRedraw(True)

end event

type cb_help from w_abstractresponse`cb_help within w_rebateselect
integer x = 2843
integer y = 1316
integer taborder = 90
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_rebateselect
integer x = 2546
integer y = 1316
integer taborder = 80
end type

event cb_cancel::clicked;Close(Parent)

end event

type cb_ok from w_abstractresponse`cb_ok within w_rebateselect
integer x = 2286
integer y = 1316
end type

event cb_ok::clicked;Long			ll_row, ll_row_count, ll_wf_chk_dlt
String		ls_group_id, ls_detailstring

//DMK added check for 'C' and 'O'
//dmk check rebate option
//If is_rebate_type = 'A' or is_rebate_type = 'C' or is_rebate_type = 'O'  or is_rebate_type = 'I' Then
If is_rebate_option = 'A' Then
	If is_type = 'G' Then

		ll_row_count = dw_selected.rowcount()
		ll_row = 1
	
		do until ll_row > ll_row_count
			ls_group_id	= dw_selected.GetItemstring(ll_row, 'group_id')
			ls_DetailString += string(il_rebate_id) + '~t' + ls_group_id + '~r~n'
			ll_row ++
		Loop
		
		If ll_row  >  1 Then
//			If wf_ProductGroupCheck(ls_detailstring, 'M') = False Then
//				Return 1
//			End If
		End If
	End If
End If

If is_rebate_option = 'A' Then
	If is_type = 'P' Or is_type = 'G' or is_type = 'O' or is_type = 'D' or is_type = 'S' Then
		ll_wf_chk_dlt = wf_check_dates()
	End If
	Choose Case is_type
		Case 'P' 
			If ll_wf_chk_dlt = 2 Then
				MessageBox("Product Detail Date Error", "The date periods can not overlap with any other Product Detail date period.", Exclamation!, OK!, 1)
				Return 1
			Else
				If ll_wf_chk_dlt = 1 Then
					Messagebox("Product Detail Date Error", "The detail date periods have to fall within the header start & end Dates.", Exclamation!, OK!, 1)
					Return 1
				Else
					If ll_wf_chk_dlt = 3 Then
					 	Messagebox("Product Detail Date Error", "The end date must be greater than the start date.", Exclamation!, OK!, 1)
						Return 1
					Else
						If ll_wf_chk_dlt = 4 Then
						 	Messagebox("Product Detail Date Error", "Invalid Date.", Exclamation!, OK!, 1)
							Return 1
						End If	
					End If
				End If
			End If
		Case 'G'
			If ll_wf_chk_dlt = 2 Then
				MessageBox("Product Group Detail Date Error", "The date periods can not overlap with any other Product Group Detail date period.", Exclamation!, OK!, 1)
				Return 1
			Else
				If ll_wf_chk_dlt = 1 Then
					Messagebox("Product Group Detail Date Error", "The detail date periods have to fall within the header start & end Dates.", Exclamation!, OK!, 1)
					Return 1
				Else
					If ll_wf_chk_dlt = 3 Then
				 		Messagebox("Product Group Detail Date Error", "The end date must be greater than the start date.", Exclamation!, OK!, 1)
						Return 1
					Else
						If ll_wf_chk_dlt = 4 Then
							Messagebox("Product Group Detail Date Error", "Invalid Date.", Exclamation!, OK!, 1)
							Return 1
						End If
					End If		
				End If
			End If
		Case 'S' 
			If ll_wf_chk_dlt = 2 Then
				MessageBox("Customer Group Detail Date Error", "The date periods can not overlap with any other Customer Group Detail date period.", Exclamation!, OK!, 1)
				Return 1
			Else
				If ll_wf_chk_dlt = 1 Then
					Messagebox("Customer Group Detail Date Error", "The detail date periods have to fall within the header start & end Dates.", Exclamation!, OK!, 1)
					Return 1
				Else
					If ll_wf_chk_dlt = 3 Then
				 		Messagebox("Customer Group Detail Date Error", "The end date must be greater than the start date.", Exclamation!, OK!, 1)
						Return 1
					Else
						If ll_wf_chk_dlt = 4 Then
							Messagebox("Customer Group Detail Date Error", "Invalid Date.", Exclamation!, OK!, 1)
							Return 1
						End If
					End If		
				End If	
			End If
		Case 'O'  
			If ll_wf_chk_dlt = 2 Then
				MessageBox("Location Detail Date Error", "The date periods can not overlap with any other Location Detail date period.", Exclamation!, OK!, 1)
				Return 1
			Else
				If ll_wf_chk_dlt = 1 Then
					Messagebox("Location Detail Date Error", "The detail date periods have to fall within the header start & end Dates.", Exclamation!, OK!, 1)
					Return 1
				Else
					If ll_wf_chk_dlt = 3 Then
				 		Messagebox("Location Detail Date Error", "The end date must be greater than the start date.", Exclamation!, OK!, 1)
						Return 1
					Else
						If ll_wf_chk_dlt = 4 Then
							Messagebox("Location Detail Date Error", "Invalid Date.", Exclamation!, OK!, 1)
							Return 1
						End If
					End If		
				End If	
			End If
		Case 'D'
			If ll_wf_chk_dlt = 2 Then
				MessageBox("Division Detail Date Error", "The date periods can not overlap with any other Division Detail date period.", Exclamation!, OK!, 1)
				Return 1
			Else
				If ll_wf_chk_dlt = 1 Then
					Messagebox("Division Detail Date Error", "The detail date periods have to fall within the header start & end Dates.", Exclamation!, OK!, 1)
					Return 1
				Else
					If ll_wf_chk_dlt = 3 Then
				 		Messagebox("Division Detail Date Error", "The end date must be greater than the start date.", Exclamation!, OK!, 1)
						Return 1
					Else
						If ll_wf_chk_dlt = 4 Then
							Messagebox("Division Detail Date Error", "Invalid Date.", Exclamation!, OK!, 1)
							Return 1
						End If
					End If		
				End If	
			End If
	End Choose
End If

If is_rebate_option = 'A' Then
	If is_type = 'P' or is_type = 'D' Then
		dw_selected.SetSort("#1 A, #7 A")  
		dw_selected.Sort()
	Else
		If is_type = 'O' Then
			dw_selected.SetSort("#1 A, #3 A, #7 A")  
			dw_selected.Sort()
		Else	
			If is_type = 'G' or is_type = 'S' Then
				dw_selected.SetSort("#1 A, #9 A")
				dw_selected.Sort()
			Else
				dw_selected.SetSort("#1 A")
				dw_selected.Sort()
			End If
		End If
	End If
Else
	dw_selected.SetSort("#1 A")
	dw_selected.Sort()
End If

is_selected = dw_selected.object.datawindow.data 

Close(Parent)
end event

type dw_select from datawindow within w_rebateselect
integer x = 37
integer y = 132
integer width = 1257
integer height = 1152
integer taborder = 40
boolean bringtotop = true
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;DataWindow				ldw_datawindow


IF row = 0 Then Return

ldw_datawindow = This
iu_DwSelect.uf_initialize(ldw_datawindow, 2)
iu_dwSelect.uf_select(row)
end event

event doubleclicked;string					ls_groupid
any						la_view_data

u_ParameterStack		lu_ParameterStack

window					lw_window

Choose Case is_type
	Case 'D'
		Return
	
	Case 'G'
		la_view_data = string(dwo.name)
		la_view_data = dw_select.getrow()
		if string(dwo.name) = 'group_id' or string(dwo.name) = 'group_description' then
			ls_groupid = dw_select.GetItemString(dw_select.getrow(), 'group_id')
			
			iu_ErrorContext.uf_Initialize()
			iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
			lu_ParameterStack.uf_Initialize()

			lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
			lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Push('string', is_userid)
			lu_ParameterStack.uf_Push('string', is_password)
			lu_ParameterStack.uf_Push('string', ls_groupid)
			lu_ParameterStack.uf_Push('string', is_owner)
			lu_ParameterStack.uf_Push('window', parent)
			
			iu_ClassFactory.uf_GetResponseWindow( "w_productpopup", lu_ParameterStack )
			
			lu_ParameterStack.uf_Pop('window', lw_window)
			lu_ParameterStack.uf_Pop('string', is_owner)
			lu_ParameterStack.uf_Pop('string', ls_groupid)
			lu_ParameterStack.uf_Pop('string', is_password)
			lu_ParameterStack.uf_Pop('string', is_userid)
			lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
			
			iu_ErrorContext.uf_initialize()

		Return
		End If
	
	Case 'P'
		Return
	
End Choose
end event

type dw_selected from datawindow within w_rebateselect
integer x = 1431
integer y = 132
integer width = 2766
integer height = 1152
integer taborder = 30
boolean bringtotop = true
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;String				ls_indicator
DataWindow			ldw_datawindow

IF row = 0 Then Return

if is_type = 'P' then //Products
	if this.GetClickedColumn() = 3 then
		If is_rebate_type <> 'Q' then
			ls_indicator = This.GetItemString(row, 'include_ind')
			Choose Case this.getitemstring(row, 'include_ind')
				Case 'N'
					this.setitem(row, 'include_ind', 'Y')
				Case 'Y'
					this.setitem(row, 'include_ind', 'N')
			End choose
			dw_selected.AcceptText()
		End If
	End if	
end if

//  RevGLL  // *(Begin)
If is_type = 'D' then  // Divisions
	If This.GetClickedColumn() = 4 then
		If (is_rebate_type <> 'Q') and (is_rebate_type <> 'P') then
			ls_indicator = This.GetItemString(row, 'exclude_ind')
			Choose Case This.GetItemString(row, 'exclude_ind')
				Case 'N'
					This.SetItem(row, 'exclude_ind', 'Y')
				Case 'Y'
					This.SetItem(row, 'exclude_ind', 'N')
			End Choose
			dw_selected.AcceptText()
		End If
	End if
End If

//dmk
If is_type = 'G' then  //Product Groups
	If this.GetClickedColumn() = 8 then
		If is_rebate_type <> 'Q' then
			ls_indicator = This.GetItemString(row, 'exclude_ind')
			Choose Case This.GetItemString(row, 'exclude_ind')
				Case 'N'
					This.SetItem(row, 'exclude_ind', 'Y')
				Case 'Y'
					This.SetItem(row, 'exclude_ind', 'N')
			End Choose
			dw_selected.AcceptText()
		End If
	End If
End If

If is_type = 'S' then  //Customer Groups
	If this.GetClickedColumn() = 8 then
		If is_rebate_type <> 'Q' then
			ls_indicator = This.GetItemString(row, 'exclude_ind')
			Choose Case This.GetItemString(row, 'exclude_ind')
				Case 'N'
					This.SetItem(row, 'exclude_ind', 'Y')
				Case 'Y'
					This.SetItem(row, 'exclude_ind', 'N')
			End Choose
			dw_selected.AcceptText()
		End If
	End If
End If

//  RevGLL  // *(End)

If is_type = 'O' then  // Locations
	If This.GetClickedColumn() = 4 then
		If (is_rebate_type <> 'Q') and (is_rebate_type <> 'P') then
			ls_indicator = This.GetItemString(row, 'exclude_ind')
			Choose Case This.GetItemString(row, 'exclude_ind')
				Case 'N'
					This.SetItem(row, 'exclude_ind', 'Y')
				Case 'Y'
					This.SetItem(row, 'exclude_ind', 'N')
			End Choose
			dw_selected.AcceptText()
		End If
	End if
End If

ldw_datawindow = This
iu_DwSelect.uf_initialize(ldw_datawindow, 2)
iu_dwSelect.uf_select(row)

Return

end event

event doubleclicked;String							ls_Date
String							ls_groupid
Any								la_view_data

u_ParameterStack				lu_ParameterStack
u_AbstractParameterStack	lu_Stack

window							lw_window

Choose Case is_type
	Case 'D'  // Division
		// This script opens up the calendar when the correct column is double-clicked
		// Let's make sure they double-clicked on a date or datetime column
		If row <= 0 Then Return
		If String(dwo.Type) <> "column" Then Return
		
		If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return
		
		ls_date = String(Today())
		
		iu_ErrorContext.uf_Initialize()
		iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
		iu_ErrorContext.uf_Initialize()
		lu_Stack.uf_Initialize()

		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)
		
		// Open the calendar window
		iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)
		
		// Was it successful?  If so, get the date passed back and set the column.
		If iu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
			This.SetText(ls_date)
			If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
		END IF	
		
	Case 'O'  // Location
		// This script opens up the calendar when the correct column is double-clicked
		// Let's make sure they double-clicked on a date or datetime column
		If row <= 0 Then Return
		If String(dwo.Type) <> "column" Then Return
		
		If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return
		
		ls_date = String(Today())
		
		iu_ErrorContext.uf_Initialize()
		iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
		iu_ErrorContext.uf_Initialize()
		lu_Stack.uf_Initialize()

		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)
		
		// Open the calendar window
		iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)
		
		// Was it successful?  If so, get the date passed back and set the column.
		If iu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
			This.SetText(ls_date)
			If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
		END IF	
		
	Case 'G' // Product Group
		la_view_data = string(dwo.name)
		la_view_data = dw_selected.getrow()
		if string(dwo.name) = 'group_id' or string(dwo.name) = 'group_description' then
			ls_groupid = dw_selected.GetItemString(dw_selected.getrow(), 'group_id')
			
			iu_ErrorContext.uf_Initialize()
			iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
			lu_ParameterStack.uf_Initialize()

			lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
			lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Push('string', is_userid)
			lu_ParameterStack.uf_Push('string', is_password)
			lu_ParameterStack.uf_Push('string', ls_groupid)
			lu_ParameterStack.uf_Push('string', is_owner)
			lu_ParameterStack.uf_Push('window', parent)
			
			iu_ClassFactory.uf_GetResponseWindow( "w_productpopup", lu_ParameterStack )
			
			lu_ParameterStack.uf_Pop('window', lw_window)
			lu_ParameterStack.uf_Pop('string', is_owner)
			lu_ParameterStack.uf_Pop('string', ls_groupid)
			lu_ParameterStack.uf_Pop('string', is_password)
			lu_ParameterStack.uf_Pop('string', is_userid)
			lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
			
			iu_ErrorContext.uf_initialize()

		Return
		End If
		
		// This script opens up the calendar when the correct column is double-clicked
		// Let's make sure they double-clicked on a date or datetime column
		If row <= 0 Then Return
		If String(dwo.Type) <> "column" Then Return
		
		If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return
		
		ls_date = String(Today())
		
		iu_ErrorContext.uf_Initialize()
		iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
		iu_ErrorContext.uf_Initialize()
		lu_Stack.uf_Initialize()

		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)
		
		// Open the calendar window
		iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)
		
		// Was it successful?  If so, get the date passed back and set the column.
		If iu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
			This.SetText(ls_date)
			If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
		END IF
		
	Case 'S' // Customer Group
		la_view_data = string(dwo.name)
		la_view_data = dw_selected.getrow()
		if string(dwo.name) = 'group_id' or string(dwo.name) = 'group_description' then
			ls_groupid = dw_selected.GetItemString(dw_selected.getrow(), 'group_id')
			
			iu_ErrorContext.uf_Initialize()
			iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
			lu_ParameterStack.uf_Initialize()

			lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
			lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Push('string', is_userid)
			lu_ParameterStack.uf_Push('string', is_password)
			lu_ParameterStack.uf_Push('string', ls_groupid)
			lu_ParameterStack.uf_Push('string', is_owner)
			lu_ParameterStack.uf_Push('window', parent)
			
			iu_ClassFactory.uf_GetResponseWindow( "w_productpopup", lu_ParameterStack )
			
			lu_ParameterStack.uf_Pop('window', lw_window)
			lu_ParameterStack.uf_Pop('string', is_owner)
			lu_ParameterStack.uf_Pop('string', ls_groupid)
			lu_ParameterStack.uf_Pop('string', is_password)
			lu_ParameterStack.uf_Pop('string', is_userid)
			lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
			
			iu_ErrorContext.uf_initialize()

		Return
		End If
		
		// This script opens up the calendar when the correct column is double-clicked
		// Let's make sure they double-clicked on a date or datetime column
		If row <= 0 Then Return
		If String(dwo.Type) <> "column" Then Return
		
		If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return
		
		ls_date = String(Today())
		
		iu_ErrorContext.uf_Initialize()
		iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
		iu_ErrorContext.uf_Initialize()
		lu_Stack.uf_Initialize()

		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)
		
		// Open the calendar window
		iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)
		
		// Was it successful?  If so, get the date passed back and set the column.
		If iu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
			This.SetText(ls_date)
			If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
		END IF	
		
	Case 'P'  //Products
		// This script opens up the calendar when the correct column is double-clicked
		// Let's make sure they double-clicked on a date or datetime column
		If row <= 0 Then Return
		If String(dwo.Type) <> "column" Then Return
		
		If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return
		
		ls_date = String(Today())
		
		iu_ErrorContext.uf_Initialize()
		iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
		iu_ErrorContext.uf_Initialize()
		lu_Stack.uf_Initialize()
		
		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)
		
		// Open the calendar window
		iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)
		
		// Was it successful?  If so, get the date passed back and set the column.
		If iu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
			This.SetText(ls_date)
			If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
		END IF
	
End Choose
end event

event itemchanged;Date							ldt_date
u_projectfunctions		lu_projectfunctions
datawindow					ldw_datawindow

ldt_date = Date(data)

Choose Case dwo.Name
	Case 'start_date'
		dw_selected.AcceptText()
		dw_selected.SetItem(row, 'start_date', 1)
		ldw_datawindow = dw_selected
		lu_projectfunctions.uf_changerowstatus(ldw_datawindow, row + 1, New!)
	Case 'end_date'
		dw_selected.AcceptText()
		dw_selected.SetItem(row, 'end_date', 1)
		ldw_datawindow = dw_selected
		lu_projectfunctions.uf_changerowstatus(ldw_datawindow, row + 1, New!)
	Case 'rebate_rate'
//		IF Not Double(data) > 0.0000 Then
//			SetMicroHelp('The Rate must be greater than 0.0000')
//			This.SelectText(1, 1000)
//			Return 1
//		End If
//		if this.getitemnumber(row,'rebate_rate') < 0 then 
//			SetMicroHelp('The Rate can not be a negative number.') 
//			This.SelectText(1, 1000)
//			Return 1
//		end if
		dw_selected.AcceptText()
	Case 'include_ind'
		dw_selected.AcceptText()
End Choose
end event

event editchanged;Date							ldt_date
u_projectfunctions		lu_projectfunctions
datawindow					ldw_datawindow

ldt_date = Date(data)

Choose Case dwo.Name
	Case 'start_date'
		dw_selected.AcceptText()
		dw_selected.SetItem(row, 'start_date', 1)
		ldw_datawindow = dw_selected
		lu_projectfunctions.uf_changerowstatus(ldw_datawindow, row + 1, New!)
	Case 'end_date'
		dw_selected.AcceptText()
		dw_selected.SetItem(row, 'end_date', 1)
		ldw_datawindow = dw_selected
		lu_projectfunctions.uf_changerowstatus(ldw_datawindow, row + 1, New!)
	Case 'rebate_rate'
//		IF Not Double(data) > 0.0000 Then
//			SetMicroHelp('The Rate must be greater than 0.0000')
//			This.SelectText(1, 1000)
//			Return 1
//		End If
//		if this.getitemnumber(row,'rebate_rate') < 0 then 
//			SetMicroHelp('The Rate can not be a negative number.') 
//			This.SelectText(1, 1000)
//			Return 1
//		end if
		dw_selected.AcceptText()
	Case 'include_ind'
		dw_selected.AcceptText()		
	End Choose

end event

type cb_1 from commandbutton within w_rebateselect
integer x = 1307
integer y = 356
integer width = 114
integer height = 84
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = ">"
end type

event clicked;String				ls_ExcludeIncludeInd, ls_CustomerId, ls_CustomerType, ls_temp, ls_text, ls_owner
Boolean				lb_prodgrpchk_err_ind, lb_custgrpchk_err_ind
Long					ll_row, ll_row2
Date              ldt_default_date

ldt_default_date = Date('01/01/0001')

lb_prodgrpchk_err_ind = False
lb_custgrpchk_err_ind = False

ll_row = dw_select.GetSelectedRow(0)
IF ll_row = 0 Then Return

SetPointer(HourGlass!)
Parent.SetRedraw(False)

Do
	Choose Case is_type
	   Case 'P' // Products
			dw_select.SetItem(ll_row, 'include_ind', 'Y')
			dw_select.SetItem(ll_row, 'tonnage_rebate_credit', 'R')
			dw_select.SetItem(ll_row, 'start_date', idt_header_start_date)
			dw_select.SetItem(ll_row, 'end_date', idt_header_end_date)
			dw_select.SetItem(ll_row, 'rebate_rate', 0)
			
			If (is_rebate_type = 'Q') or (is_rebate_type = 'P') Then
				dw_select.SetItem(ll_row, 'include_ind', 'N')
			Else
				dw_select.SetItem(ll_row, 'include_ind', 'Y')
			End If
			
			If il_last_seq_no_product = 0 Then
				il_last_seq_no_product ++
			End If
			
			dw_select.setitem(ll_row, 'seq_no', il_last_seq_no_product)
			il_last_seq_no_product ++
		Case 'G'  //Product Group
			lb_prodgrpchk_err_ind = False
//			If wf_ProductGroupCheck(dw_select.GetItemstring(ll_row, 'group_id'), 'S') then
				ls_owner = trim(dw_selectowner.GetItemString(1, 'owner'))
				dw_select.SetItem(ll_row, 'group_owner', ls_owner)
				dw_select.SetItem(ll_row, 'tonnage_rebate_credit', 'R')
				dw_select.SetItem(ll_row, 'start_date', idt_header_start_date)
				dw_select.SetItem(ll_row, 'end_date', idt_header_end_date)
				dw_select.SetItem(ll_row, 'rebate_rate', 0)
				
				If (is_rebate_type = 'Q') or (is_rebate_type = 'P') Then
					dw_select.SetItem(ll_row, 'exclude_ind', 'N')
				else
					dw_select.SetItem(ll_row, 'exclude_ind', 'Y')
				End If
			
				If il_last_seq_no_group = 0 Then
					il_last_seq_no_group ++
				End If
				
				dw_select.SetItem(ll_row, 'seq_no', il_last_seq_no_group)
				il_last_seq_no_group ++
//			Else
//				lb_prodgrpchk_err_ind = True
//			End If
		Case 'D'  // Divisions
			dw_select.SetItem(ll_row, 'tonnage_rebate_credit', 'R')
			dw_select.SetItem(ll_row, 'exclude_ind', 'Y')
			dw_select.SetItem(ll_row, 'start_date', idt_header_start_date)
			dw_select.SetItem(ll_row, 'end_date', idt_header_end_date)
			dw_select.SetItem(ll_row, 'rebate_rate', 0)
			//new row for insert 
			dw_select.SetItem(ll_row, 'original_start_date', ldt_default_date)
			If (is_rebate_type = 'P') Then
				dw_selected.object.exclude_ind.Protect = 1
			End If
		Case 'O'  // Locations
			//dw_select.SetItem(ll_row, 'exclude_ind', 'Y')
			dw_select.SetItem(ll_row, 'start_date', idt_header_start_date)
			dw_select.SetItem(ll_row, 'end_date', idt_header_end_date)
			dw_select.SetItem(ll_row, 'rebate_rate', 0)		
			
			If (is_rebate_type = 'P') Then
				dw_select.SetItem(ll_row, 'exclude_ind', 'N')
			else
				dw_select.SetItem(ll_row, 'exclude_ind', 'Y')
			End If			
			//new row for insert
			dw_select.SetItem(ll_row,'original_start_date', ldt_default_date)
		Case 'S'  //Customer Group
			lb_custgrpchk_err_ind = False
//			If wf_CustomerGroupCheck(dw_select.GetItemstring(ll_row, 'group_id'), 'S') then
				ls_owner = trim(dw_selectowner.GetItemString(1, 'owner'))
				dw_select.SetItem(ll_row, 'group_owner', ls_owner)
				dw_select.SetItem(ll_row, 'tonnage_rebate_credit', 'R')
				dw_select.SetItem(ll_row, 'start_date', idt_header_start_date)
				dw_select.SetItem(ll_row, 'end_date', idt_header_end_date)
				dw_select.SetItem(ll_row, 'rebate_rate', 0)

				If (is_rebate_type = 'Q') Then
					dw_select.SetItem(ll_row, 'exclude_ind', 'N')
				else
					dw_select.SetItem(ll_row, 'exclude_ind', 'Y')
				End If
			
				If il_last_seq_no_group = 0 Then
					il_last_seq_no_group ++
				End If
				
				dw_select.SetItem(ll_row, 'seq_no', il_last_seq_no_group)
				il_last_seq_no_group ++
//			Else
//				lb_custgrpchk_err_ind = True
//			End If
			
	End Choose

	If lb_prodgrpchk_err_ind = False and lb_custgrpchk_err_ind = False Then
		If is_rebate_option = 'A' Then
			If is_type = 'G' Or is_type = 'P' Or is_type = 'S' Then
				ll_row2 = rowcount(dw_selected)
				ll_row2 ++
			End If
		End If

		If is_rebate_option = 'V' Then
			dw_select.RowsMove ( ll_row, ll_row, primary!, dw_selected, 100000, Primary! )	
		Else
			If is_type = 'P' Or is_type = 'G' Or is_type = 'S' Then
				dw_select.RowsCopy ( ll_row, ll_row, primary!, dw_selected, 100000, Primary! )	

				If is_rebate_option = 'A' Then
					If is_type = 'G' or is_type = 'S' Then
						ls_text = dw_selected.GetItemstring(ll_row2, 'group_id')
						ls_text = Right('         ' + String(ls_text), 9)
						dw_selected.SetItem(ll_row2, 'group_id', ls_text)
					Else
						ls_text = dw_selected.GetItemString(ll_row2, 'product_code')
						ls_text = Left(String(ls_text) + '          ', 10)
						dw_selected.SetItem(ll_row2, 'product_code', ls_text)
					End If
				End If
				dw_select.SelectRow( ll_row, FALSE )
			Else
				dw_select.RowsMove ( ll_row, ll_row, primary!, dw_selected, 100000, Primary! )	
			End If
		End If
		ll_row = dw_select.GetSelectedRow(0)
	Else
		ll_row = 0
	End If
Loop While ll_row > 0

//If is_rebate_type = 'A' or is_rebate_type = 'O' or is_rebate_type = 'C' or is_rebate_type = 'C' Then
If is_rebate_option = 'A' Then
	If is_type = 'P' Then
		dw_selected.SetSort("#1 A, #8 A, #9 A")
		dw_selected.Sort()
	Else
		If is_type = 'G' or is_type = 'S' Then
			dw_selected.SetSort("#1 A, #10 A, #11 A")
			dw_selected.Sort()
		Else
			If is_type = 'O' Then
				//dw_selected.SetSort("#1 A, #3 A")
				dw_selected.SetSort("#1 A, #3 A, #6 A")
				dw_selected.Sort()
			Else
				//dw_selected.SetSort("#1 A") 
				dw_selected.SetSort("#1 A, #6 A")
				dw_selected.Sort()
			End If
		End If
	End If
Else
	dw_selected.SetSort("#1 A")
	dw_selected.Sort()
End If

// This will justify the descriptions on dw_selected window for all types.
ll_row2 = RowCount(dw_selected)
ll_row = 1
Do
	If is_type = 'D' Then
		ls_text = dw_selected.GetItemstring(ll_row2, 'division_description')
		ls_text = Left(String(ls_text) + '                                        ', 40)
		dw_selected.SetItem(ll_row2, 'division_description', ls_text)
	Else
		If is_type = 'G' or is_type = 'S' Then
			ls_text = dw_selected.GetItemstring(ll_row2, 'group_description')
			ls_text = Left(String(ls_text) + '                                        ', 40)
			dw_selected.SetItem(ll_row2, 'group_description', ls_text)
		Else
			If is_type = 'P' Then
				ls_text = dw_selected.GetItemstring(ll_row2, 'product_description')
				ls_text = Left(String(ls_text) + '                                        ', 40)
				dw_selected.SetItem(ll_row2, 'product_description', ls_text)
			End If
		End If
	End If
	ll_row ++
Loop Until ll_row > ll_row2

Parent.SetRedraw(True)

end event

type cb_2 from commandbutton within w_rebateselect
integer x = 1307
integer y = 460
integer width = 114
integer height = 84
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<"
end type

event clicked;Boolean				lb_found
Long					ll_row, &
						ll_newrow, &
						ll_Count
String				ls_temp, &
						ls_tempType
						
ll_row = dw_selected.GetSelectedRow(0)
IF ll_row = 0 Then Return 

SetPointer(HourGlass!)
Parent.SetRedraw(False)

Do
//dmk check rebate option instead of rebate type
//	If is_rebate_type = 'A' or is_rebate_type = 'O' or is_rebate_type = 'C' or is_rebate_type = 'I'Then
If is_rebate_option = 'A' Then
		If is_type = 'D' Then
			dw_selected.RowsMove( ll_row, ll_row, Primary!, dw_select, 100000, Primary! )
		Else
			dw_selected.RowsDiscard( ll_row, ll_row, primary! )
		End If
	Else
		dw_selected.RowsMove( ll_row, ll_row, Primary!, dw_select, 100000, Primary! )
	End If
	ll_row = dw_selected.GetSelectedRow(0)	
Loop While ll_row > 0

Parent.SetRedraw(True)

end event

type dw_productcodeshort from datawindow within w_rebateselect
event ue_postconstructor ( )
event ue_keydown pbm_dwnkey
integer x = 306
integer y = 1296
integer width = 928
integer height = 144
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_productcodeshort"
boolean border = false
boolean livescroll = true
end type

event ue_postconstructor;if is_type = 'P' then
	dw_productcodeshort.visible = True
	st_1.visible = True
Else
	dw_productcodeshort.visible = False
	st_1.visible = False
End if
end event

event ue_keydown;// This will get set to true in the editchanged event
//This.SetRedraw(False)
//
//Choose Case key
//	Case	KeyBack!
//		is_whatwastyped = Left(is_whatwastyped, Len(is_whatwastyped) - 1)
//		ii_keystyped = Len(is_whatwastyped) - 1
//		If ii_keystyped < 0 Then ii_keystyped = 0
//		This.SetItem(1, 'product_code', is_whatwastyped)
//	Case KeyLeftArrow!   
//		ii_keystyped --
//		if ii_keystyped < 2 then ii_keystyped = 2
//	Case	KeyRightArrow!		
//		ii_keystyped ++
//		if ii_keystyped > 10 then ii_keystyped = 10
//	Case Keydelete!		
//		ii_keystyped = Len(This.GetText())
//END choose
end event

event constructor;this.insertrow(0)
this.PostEvent ( 'ue_postconstructor' )
end event

event itemerror;return 1
end event

event editchanged;String	ls_FoundProduct, &
			ls_productcode
integer	li_row, li_rowcount


If Len(data) = 0 Or String(dwo.Name) <> 'product_code' Then 
	This.SetRedraw(True)
	Return
End if
ii_keystyped ++

If ii_keystyped > Len(data) Then ii_keystyped = Len(data)
If is_whatwastyped <> Data Then
	is_whatwastyped += Right(Data,1)
End if
li_row = 1 
li_rowcount = dw_select.rowcount()

do until Data <= Left(trim(dw_select.GetItemString(li_row, "product_code")), Len(Data)) or li_row = li_rowcount
	li_row ++
loop 

if  ii_last_row_highlighted <> 0 then
	dw_select.selectrow(ii_last_row_highlighted, False)
end if

if Data = Left(trim(dw_select.GetItemString(li_row, "product_code")), Len(Data)) then
	ii_last_row_highlighted = li_row
	dw_select.selectrow(li_row, True)
	This.setitem(1,'product_description', dw_select.getitemstring( li_row, 'product_description'))
else
	ii_last_row_highlighted = 0
	This.setitem(1,'product_description', '   ')
end if

dw_select.ScrollToRow ( li_row )
this.setfocus()


This.SetRedraw(True)

end event

type st_1 from statictext within w_rebateselect
integer x = 114
integer y = 1356
integer width = 187
integer height = 72
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Product:"
boolean focusrectangle = false
end type

type dw_selectowner from datawindow within w_rebateselect
event ue_postitemchanged ( )
boolean visible = false
integer x = 46
integer y = 28
integer width = 430
integer height = 96
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_selectowner"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;This.InsertRow(0)
This.SetItem(1, 'owner', 'SMACC')
end event

event itemchanged;String ls_owner

ls_owner = trim(data)

dw_select.SetSort("long(group_id) asc")
dw_select.SetFilter("group_owner = '"+ ls_owner +" ' ")
dw_select.Filter()
dw_select.Sort()

end event

type dw_selectlocationtype from datawindow within w_rebateselect
boolean visible = false
integer x = 46
integer y = 24
integer width = 434
integer height = 88
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_location_type"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;This.InsertRow(0)
This.SetItem(1, 'location_type', 'P')
end event

event itemchanged;String   ls_string

ls_string = trim(data)

Parent.wf_filterlocations(ls_string)





end event

