﻿$PBExportHeader$w_pricing_ind_resp.srw
forward
global type w_pricing_ind_resp from w_abstractresponseext
end type
type dw_pricing_ind from datawindow within w_pricing_ind_resp
end type
end forward

global type w_pricing_ind_resp from w_abstractresponseext
integer width = 869
integer height = 1024
string title = "Pricing Indicators"
boolean controlmenu = false
windowtype windowtype = child!
dw_pricing_ind dw_pricing_ind
end type
global w_pricing_ind_resp w_pricing_ind_resp

type variables
string		is_PriceInds, &
				is_selected_ind_array, &
				is_modified
			
long			il_selected_count, &
				il_selected_max = 15		
			
boolean		ib_updated, &
				ib_modified

window		iw_parent
datastore	ids_selected_inds
			
u_AbstractClassFactory				iu_ClassFactory
u_ErrorContext							iu_ErrorContext
u_NotificationController			iu_Notification	
end variables

on w_pricing_ind_resp.create
int iCurrent
call super::create
this.dw_pricing_ind=create dw_pricing_ind
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pricing_ind
end on

on w_pricing_ind_resp.destroy
call super::destroy
destroy(this.dw_pricing_ind)
end on

event open;call super::open;long		ll_temp
string	ls_pricinginds
integer	li_xpos, li_ypos, li_rtn

iw_parent = Message.PowerObjectParm

iw_parent.DYNAMIC event ue_get_data("error")
iu_ErrorContext = Message.PowerObjectParm

iw_parent.DYNAMIC event ue_get_data("ClassFactory")
iu_ClassFactory = Message.PowerObjectParm

iw_parent.DYNAMIC event ue_get_data("Notify")
iu_Notification = Message.PowerObjectParm

iw_parent.DYNAMIC event ue_get_data("PriceInd")
is_PriceInds = Message.StringParm

iw_parent.DYNAMIC event ue_get_data("selected")
is_selected_ind_array = Message.StringParm

ll_temp = dw_pricing_ind.ImportString(is_PriceInds)
dw_pricing_ind.SetRedraw(True)

Return 

end event

event ue_postopen();call super::ue_postopen;String 					ls_ind, &
							ls_find
u_string_functions	lu_string_function
long						ll_rowcount, ll_count, ll_row, ll_rowcnt


ll_rowcnt = dw_pricing_ind.rowcount()

ids_selected_inds = Create DataStore
ids_selected_inds.DataObject = 'd_pricing_type_code'
ll_rowcount = ids_selected_inds.importstring(is_selected_ind_array)

il_selected_count = 0
if ll_rowcount > 0 then
	for ll_count = 1 to ll_rowcount
		ls_ind = ids_selected_inds.getitemstring(ll_count, 'type_code')
		ls_find = "type_code = '" + ls_ind + "'"
		ll_row = dw_pricing_ind.find(ls_find,0,ll_rowcnt)
		if ll_row > 0 and il_selected_count < il_selected_max then 
			dw_pricing_ind.selectrow(ll_row, true)
			il_selected_count ++
		ELSE
			IF ll_row = 0 then
				ids_selected_inds.setitem(ll_count, 'type_code', '   ')
				ib_modified = true
			end if
		end if
	next
end if
end event

type cb_help from w_abstractresponseext`cb_help within w_pricing_ind_resp
boolean visible = false
integer x = 759
integer y = 952
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_pricing_ind_resp
integer y = 772
end type

event cb_cancel::clicked;call super::clicked;close(parent)
end event

type cb_ok from w_abstractresponseext`cb_ok within w_pricing_ind_resp
integer y = 772
boolean default = true
end type

event cb_ok::clicked;call super::clicked;long	ll_row, ll_count1, ll_count2

if ib_modified then is_selected_ind_array = ""

ll_row = dw_pricing_ind.GetSelectedRow(0)
do while ll_row > 0 and ib_modified
	is_selected_ind_array += mid(dw_pricing_ind.getitemstring( ll_row, 'type_code'),1,1) + "~r~n"
	ll_row = dw_pricing_ind.GetSelectedRow(ll_row)
loop
iw_parent.dynamic event ue_set_data("selected",is_selected_ind_array)

if ib_modified then
	is_modified = "true"
else
	is_modified = "false"
end if
iw_parent.dynamic event ue_set_data("modified",is_modified)

if ib_modified then iw_parent.dynamic wf_set_selected_pricinginds()

Close(parent)
end event

event cb_ok::getfocus;call super::getfocus;This.SetFocus()
end event

type dw_pricing_ind from datawindow within w_pricing_ind_resp
integer x = 59
integer y = 68
integer width = 736
integer height = 640
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_pricing_window"
boolean vscrollbar = true
boolean border = false
end type

event clicked;IF Row > 0 THEN
	if dw_pricing_ind.isselected(row) then
		dw_pricing_ind.SelectRow(row, false)
		il_selected_count --
		ib_modified = true
	else
		dw_pricing_ind.SelectRow( row, true)
		il_selected_count ++
		ib_modified = true
	end if
END IF

end event

