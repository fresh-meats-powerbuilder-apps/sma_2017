﻿$PBExportHeader$w_rebatecustomerselect.srw
forward
global type w_rebatecustomerselect from w_abstractresponse
end type
type dw_customers from datawindow within w_rebatecustomerselect
end type
type dw_selected from datawindow within w_rebatecustomerselect
end type
type cb_1 from commandbutton within w_rebatecustomerselect
end type
type cb_2 from commandbutton within w_rebatecustomerselect
end type
type dw_types from datawindow within w_rebatecustomerselect
end type
type dw_customershort from datawindow within w_rebatecustomerselect
end type
type dw_customer_filter from datawindow within w_rebatecustomerselect
end type
end forward

global type w_rebatecustomerselect from w_abstractresponse
integer x = 59
integer y = 248
integer width = 3232
integer height = 1568
string title = "Customer Selection "
event ue_selectcustomers ( )
dw_customers dw_customers
dw_selected dw_selected
cb_1 cb_1
cb_2 cb_2
dw_types dw_types
dw_customershort dw_customershort
dw_customer_filter dw_customer_filter
end type
global w_rebatecustomerselect w_rebatecustomerselect

type variables
Date				idt_HeaderStartDate, &
				idt_HeaderEndDate
decimal		idc_detail_rebate_rate				
Integer                                                     ii_keystyped, &
                                                                ii_keystyped2, &
                                                                ii_last_row_highlighted = 0
                                                          
String				is_SelectAllInd, &
					is_RebateType, &
					is_RebateOption, &
					is_whatwastyped

DataWindow			idw_selected

DataStore                                                ids_SortNames

u_AbstractErrorContext		iu_ErrorContext

u_ClassFactory			iu_ClassFactory

u_AbstractNotificationController	iu_Notification

u_AbstractParameterStack		iu_ParameterStack

u_CustomerDataAccess		iu_CustomerDataAccess

u_dwselect			iu_DwSelect
// Datawindow Row Selection (0,1,2,3)
// 	0 - No rows selected (default)
//	1 - One row selected
//	2 - Multiple rows selected
//	3 - Multiple rows with CTRL and ALT support
string	is_selection= "0"

// Contains the row number of the current row
long	il_dwRow

// Holds the number of the last clicked row
// Used for selecting rows with Shift Key
long	il_last_clicked_row

// Holds the name of the object at pointer the user clicked on
string	is_ObjectAtPointer

// Holds the name of the band the user clicked in
string	is_BandAtPointer



end variables

forward prototypes
public function boolean wf_getshiptos (string as_shipto, ref string as_shiptostring)
public function boolean wf_getcorps (string as_corp, ref string as_corpstring)
public function boolean wf_getbilltos (string as_billto, ref string as_billtostring)
public function boolean wf_updatecustomerlist ()
public function boolean wf_checkforsubordinate (long al_row)
public function boolean wf_unselectcustomers ()
public function boolean wf_getselectedcustomer (string as_includeind, string as_customerid, string as_customertype, long al_customerrow)
public function string wf_displayfilter (string as_filterstring, string as_displaytype)
public function boolean wf_filtercustomers (string as_type, string as_includeexcludeind, string as_selectallind)
public subroutine wf_dbconnection ()
public function boolean wf_check_vendor_reqd ()
end prototypes

public function boolean wf_getshiptos (string as_shipto, ref string as_shiptostring);u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_shipto)

iu_CustomerDataAccess.uf_RetrieveShiptos(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_ShiptoString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_getcorps (string as_corp, ref string as_corpstring);u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_Corp)

iu_CustomerDataAccess.uf_RetrieveCorps(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_CorpString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_getbilltos (string as_billto, ref string as_billtostring);u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_billto)

iu_CustomerDataAccess.uf_Retrievebilltos(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_billtoString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_updatecustomerlist ();Long				ll_Count, &
					ll_SelectedCount, &
					ll_CustomerCount, &
					ll_FindRow
					
String			ls_Customer, &
					ls_CustomerType


ll_SelectedCount = dw_selected.RowCount()
ll_CustomerCount = dw_customers.RowCount()

For ll_count = 1 to ll_SelectedCount
	ls_Customer = dw_selected.GetItemString(ll_count, 'customer_number')
	ls_CustomerType = dw_selected.GetItemString(ll_count, 'customer_type')
	ll_FindRow = dw_customers.Find('customer_id = "' + ls_customer + &
			'" And customer_type = "' + ls_CustomerType + '"', 1, ll_CustomerCount + 1)
	If ll_FindRow > 0 Then
		dw_customers.DeleteRow(ll_FindRow)
//	Else
//		Return False
	End If
Next

Return True

end function

public function boolean wf_checkforsubordinate (long al_row);String					ls_customer, &
							ls_FindString, &
							ls_
							
Long						ll_FindRow, &
							ll_SelectedCount


ls_customer = dw_customers.GetItemString(al_row, 'customer_id')

ll_SelectedCount = dw_selected.RowCount()

If dw_types.GetItemString(1, 'select_all_ind') = 'Y' Then
	Choose Case dw_types.GetItemString(1, 'customer_type')
		Case 'S' 
			Return False
		Case 'B'
			ls_FindString = 'include_exclude = "N" And parent_id = "' + ls_customer + '"'
		Case 'C'
			ls_FindString = 'include_exclude = "N" And parent_id = "' + &
					ls_customer + '" Or corp_id = "' + ls_customer + '"'
	End Choose		
Else
	Choose Case dw_types.GetItemString(1, 'customer_type')
		Case 'S' 
			Return False
		Case 'B'
			ls_FindString = 'include_exclude = "' + dw_types.GetItemString(1, 'exclude_include_ind') + &
					'" And parent_id = "' + ls_customer + &
					'" And customer_type <> "' + dw_types.GetItemString(1, 'customer_type') + '"'
		Case 'C'
			ls_FindString = 'include_exclude = "' + dw_types.GetItemString(1, 'exclude_include_ind') + &
					'" And parent_id = "' + ls_customer + '" Or corp_id = "' + ls_customer + &
					'"And customer_type <> "' + dw_types.GetItemString(1, 'customer_type') + '"'
	End Choose		
End If

ll_FindRow = dw_selected.Find(ls_FindString, 1, ll_SelectedCount + 1)

If ll_FindRow > 0 Then 
	this.setredraw(true)
	IF MessageBox('Invalid Selection', 'There are already child customers of ' + &
			ls_customer + ' included.  Do you want to remove these child customers from the ' + &
			'included list?  If you choose No, ' + ls_customer + ' will not be included.' &
			, Exclamation!, YesNo!) = 2 Then
		this.setredraw(true)
		this.setfocus()
			
		dw_customers.SelectRow(al_row, False)
		Return True
	End If
	this.setredraw(true)
	this.setfocus()
	Do 
		dw_selected.SelectRow(ll_FindRow, True)
		ll_FindRow = dw_selected.Find(ls_FindString, ll_FindRow + 1, ll_SelectedCount + 1)
	Loop While ll_FindRow > 0
	This.wf_UnselectCustomers()
End If

Return False

end function

public function boolean wf_unselectcustomers ();Boolean				lb_found
Long					ll_row, &
						ll_newrow, &
						ll_Count, &
						ll_findRow, &
						ll_SelectedCount, &
						ll_returncode
						
String				ls_customer, &
						ls_CustomerType, &
						ls_ExcludeInd

						
ll_SelectedCount = dw_selected.RowCount()

ll_row = dw_selected.GetSelectedRow(0)

IF ll_row = 0 Then Return True

Do
	ls_customer = dw_selected.GetItemString(ll_row, 'customer_number')
	ls_CustomerType = dw_selected.GetItemString(ll_row, 'customer_type')
	ls_ExcludeInd = dw_selected.GetItemString(ll_row, 'include_exclude')
	If ls_ExcludeInd = 'Y' Then
		ll_FindRow = dw_selected.Find('include_exclude = "N" And parent_id = "' + &
				ls_customer + '" Or corp_id = "' + ls_customer + '"', 1, &
				ll_SelectedCount + 1)
	
		If ll_FindRow > 0 And Not dw_selected.IsSelected (ll_FindRow) Then 
			this.setredraw(true)
			IF MessageBox('Excluding Customers', 'There are child customers of ' + &
					ls_customer + ' excluded.  Do you want to remove these child customers from the ' + &
					'included list also?  If you choose No, ' + ls_customer + ' will not be removed.' &
					, Exclamation!, YesNo!) = 1 Then
				this.setredraw(False)
				this.setfocus()
				Do 
					dw_selected.SelectRow(ll_FindRow, True)
					ll_FindRow ++
					if ls_CustomerType = "B" then
						ll_FindRow = dw_selected.Find('include_exclude = "N" And parent_id = "' + &
								ls_customer + '"', ll_FindRow, &
								ll_SelectedCount + 1)
					else
						ll_FindRow = dw_selected.Find('include_exclude = "N" And corp_id = "' &
								+ ls_customer + '"', ll_FindRow, &
								ll_SelectedCount + 1)
					end if
				Loop While ll_FindRow > 0
			Else
				this.setfocus()
				dw_selected.SelectRow(ll_row, False)
				ll_row = dw_selected.GetSelectedRow(0)	
				Continue
			End If
		End If
	End If
	For ll_count = 1 to dw_customers.DeletedCount()
		If dw_customers.GetItemString(ll_count, 'customer_id', Delete!, False) = ls_customer And &
				dw_customers.GetItemString(ll_count, 'customer_Type', Delete!, False) = ls_CustomerType Then
			dw_Customers.RowsMove( ll_count, ll_count, Delete!, dw_customers, 100000, Primary! )
			lb_found = true
		End If
	Next
	If Not lb_found Then 
		this.setredraw(true)
		ls_customer = dw_selected.getitemstring(ll_row, 'customer_number')
		ll_returncode = MessageBox('Process Option',string('Customer ' + ls_customer + &
											' is currently inactive.  Once removed, you '  + &
											'will not be able to add the customer until activated again. Do you want to remove?'), &
											Information!, YesNo!, 2 )
		this.setredraw(false)									
		this.setfocus()
		if ll_returncode = 1 then
			dw_selected.DeleteRow(ll_row)
			ll_row = dw_selected.GetSelectedRow(0)
		else
			dw_selected.selectrow(ll_row,False)
			ll_row = dw_selected.GetSelectedRow(0)
		end if
	else
		dw_selected.DeleteRow(ll_row)
		ll_row = dw_selected.GetSelectedRow(0)
	End If
	lb_found = false
	ll_SelectedCount = dw_selected.RowCount()
Loop While ll_row > 0

This.wf_FilterCustomers(dw_types.GetItemString(1, 'customer_type'), &
		dw_types.GetItemString(1, 'exclude_include_ind'), &
		dw_types.GetItemString(1, 'select_all_ind'))

dw_customers.Sort()


Return True

end function

public function boolean wf_getselectedcustomer (string as_includeind, string as_customerid, string as_customertype, long al_customerrow);String					ls_IncludeExclude
Long						ll_FindRow, &
							ll_NewRow, &
							ll_SelectedCount



//ls_temp = dw_selected
For ll_FindRow = 1 to dw_selected.DeletedCount()
	If dw_selected.GetItemString(ll_FindRow, 'customer_number', Delete!, False) = as_CustomerId And &
			dw_selected.GetItemString(ll_FindRow, 'customer_Type', Delete!, False) = as_CustomerType Then
		dw_selected.RowsMove(ll_FindRow, ll_FindRow, Delete!, dw_selected, 100000, Primary!)
		ll_SelectedCount = dw_selected.RowCount()
		ls_IncludeExclude = dw_selected.GetItemString(ll_SelectedCount, 'include_exclude')
		If Not as_IncludeInd = ls_IncludeExclude Then
			dw_selected.SetItem(ll_SelectedCount, 'include_exclude', as_IncludeInd)
		End If
		Return True
	End If
Next

ll_newrow = dw_selected.InsertRow(0)
If (is_RebateType = 'Q') and (is_RebateOption = 'A') Then
	dw_selected.SetItem(ll_newrow, 'include_exclude', 'N')
Else
	dw_selected.SetItem(ll_newrow, 'include_exclude', as_includeind)
End If
dw_selected.SetItem(ll_newrow, 'customer_number', as_customerid)
dw_selected.SetItem(ll_newrow, 'name', dw_customers.GetItemString(al_CustomerRow, 'customer_name'))
dw_selected.SetItem(ll_newrow, 'customer_type', as_CustomerType)
dw_selected.SetItem(ll_newrow, 'city', dw_customers.GetItemString(al_CustomerRow, 'customer_city'))
dw_selected.SetItem(ll_newrow, 'state', dw_customers.GetItemString(al_CustomerRow, 'customer_state'))
dw_selected.SetItem(ll_newrow, 'parent_id', dw_customers.GetItemString(al_CustomerRow, 'parent_id'))
dw_selected.SetItem(ll_newrow, 'corp_id', dw_customers.GetItemString(al_CustomerRow, 'corp_id'))

if wf_check_vendor_reqd() Then
	dw_selected.SetItem(ll_newrow, 'vendor_id',  ' ')
Else
	dw_selected.SetItem(ll_newrow, 'vendor_id',  '0')
End If

dw_selected.SetItem(ll_newrow, 'start_date', idt_HeaderStartDate)
dw_selected.SetItem(ll_newrow, 'end_date', idt_HeaderEndDate)
dw_selected.SetItem(ll_newrow, 'rebate_rate', 0)

Return True

end function

public function string wf_displayfilter (string as_filterstring, string as_displaytype);string					ls_filterstring
u_String_Functions 	lu_StringFunctions

ls_filterstring = as_filterstring

choose case as_displaytype
	case "C"
		dw_customer_filter.setitem(1,'customerdisplaytype', 'N')
	case "B"	
		choose case dw_customer_filter.getitemstring(1,'customerfiltertype')
			case "C"
				If lu_StringFunctions.nf_IsEmpty(as_filterstring) Then
					ls_filterstring = 'parent_id = "' + &
								dw_customer_filter.getitemstring(1,'customerfilter') + '"'
				else	
					ls_filterstring = '( ' + as_filterstring + ') And parent_id = "' + &
								dw_customer_filter.getitemstring(1,'customerfilter') + '"'
				end if
			case "B"
				dw_customer_filter.setitem(1,'customerdisplaytype', 'N')
			case "S"
				dw_customer_filter.setitem(1,'customerdisplaytype', 'N')
			end choose
	case "S"
		choose case dw_customer_filter.getitemstring(1,'customerfiltertype')
			case "C"
				If lu_StringFunctions.nf_IsEmpty(as_filterstring) Then
					ls_filterstring= 'corp_id = "' + &
								dw_customer_filter.getitemstring(1,'customerfilter') + '"'
				else
					ls_filterstring= '( ' + as_filterstring + ') And corp_id = "' + &
									dw_customer_filter.getitemstring(1,'customerfilter') + '"'
				end if
			case "B"
				If lu_StringFunctions.nf_IsEmpty(as_filterstring) Then
					ls_filterstring= 'parent_id = "' + &
						dw_customer_filter.getitemstring(1,'customerfilter') + '"'
				else
					ls_filterstring= '( ' + as_filterstring + ') And parent_id = "' + &
						dw_customer_filter.getitemstring(1,'customerfilter') + '"'
				end if
			case "S"
				dw_customer_filter.setitem(1,'customerdisplaytype', 'N')
		end choose
end choose

if lu_StringFunctions.nf_IsEmpty(as_filterstring) and dw_types.getitemstring(1,"exclude_include_ind") = "N" Then
	ls_filterstring = ""
end if

return ls_filterstring
end function

public function boolean wf_filtercustomers (string as_type, string as_includeexcludeind, string as_selectallind);Long					ll_FindRow, &
						ll_RowCount, &
						ll_index
						
String				ls_FilterString, &
						ls_FilterStringCorp, &
						ls_FilterStringbill, &
						ls_ParentId, &
						ls_CorpId, ls_datastring, &
						ls_CustomerId

u_String_Functions lu_StringFunctions


dw_customers.SetRedraw(False)
dw_customers.SetFilter('')
dw_customers.Filter()
ll_RowCount = dw_selected.RowCount()
ls_datastring = dw_customers.object.datawindow.data

If as_IncludeExcludeInd = 'Y' or as_SelectAllInd = 'Y' Then
	ls_FilterString = 'customer_type = "' + as_type + '"'
	If ll_RowCount > 0 Then
		Choose Case as_type
			Case 'C'
			Case 'B'
				ll_FindRow = 0
				Do 
					ll_FindRow = dw_selected.Find('include_exclude = "' + as_IncludeExcludeInd + &
							'" And customer_type = "C"', ll_FindRow + 1 , ll_RowCount + 1)
					If ll_FindRow > 0 Then
						ls_CustomerId = dw_selected.GetItemString(ll_findrow, 'customer_number')
						ls_FilterString += ' And parent_id <> "' + ls_CustomerId + '"'
					End If
				Loop While ll_FindRow > 0
			Case 'S'
				ll_FindRow = 0
				Do 
					ll_FindRow = dw_selected.Find('include_exclude = "' + as_IncludeExcludeInd + &
							'" And (customer_type = "C" Or customer_type = "B")', &
							ll_FindRow + 1 , ll_RowCount + 1)
					If ll_FindRow > 0 Then
						ls_CustomerId = dw_selected.GetItemString(ll_findrow, 'customer_number')
						if dw_selected.GetItemString(ll_findrow, 'customer_type') = "B" then
							ls_FilterString += ' And parent_id <> "' + ls_CustomerId + '"' 
						else
			//				ls_FilterString +=' And corp_id <> "' + ls_CustomerId + '"'
							ls_FilterString += " And "
							ls_filterString += "("
							ls_FilterString += 'corp_id <> "' + ls_CustomerId + '"'
							ls_filterstring += ' Or isnull(corp_id) '
							ls_filterstring += ")"
						end if
					End If
				Loop While ll_FindRow > 0
		End Choose
	End If
	if dw_customer_filter.getitemstring(1,'customerdisplaytype') = 'Y' then
		ls_FilterString = wf_displayfilter(ls_FilterString, as_type)
	end if
	dw_customers.SetFilter(ls_FilterString)
	dw_customers.Filter()
	ls_datastring = dw_customers.object.datawindow.data
Else
	Choose Case as_type
		Case 'C'
				ls_FilterString += 'customer_type = " "'
				dw_customers.SetFilter(ls_FilterString)
				dw_customers.Filter()
				ls_datastring = dw_customers.object.datawindow.data
				if dw_customer_filter.getitemstring(1,'customerdisplaytype') = 'Y' then
					dw_customer_filter.setitem(1,'customerdisplaytype', 'N')
				end if
		Case 'B'
			ls_FilterString = 'customer_type = "' + as_type +'"'
			ls_FilterStringbill = ''
			If ll_RowCount > 0 Then
				ll_FindRow = 0
				Do
					ll_FindRow = dw_Selected.Find('include_exclude = "Y" And customer_type = "C"', ll_FindRow + 1, &
							ll_RowCount + 1)
					If ll_FindRow > 0 Then
						ls_CustomerId = dw_selected.GetItemString(ll_FindRow, 'customer_number')
						ls_FilterStringbill += ' Or parent_id = "' + ls_CustomerId + '"'
					End If
				Loop While ll_FindRow > 0
			End If
			If lu_StringFunctions.nf_IsEmpty(ls_FilterStringbill) Then
				//This is to get rid of all rows.  you can only exclude customers 
				//it their parent is included.
				ls_FilterString = 'customer_type = " "'
			Else 
				ls_FilterStringbill = Mid(ls_FilterStringbill, 5)
				ls_FilterString = ls_FilterString + 'And (' + ls_FilterStringbill + ')'
			End If
			if dw_customer_filter.getitemstring(1,'customerdisplaytype') = 'Y' then
				ls_FilterString = wf_displayfilter(ls_FilterString, as_type)
			end if
			dw_customers.SetFilter(ls_FilterString)
			dw_customers.Filter()
			ls_datastring = dw_customers.object.datawindow.data
		Case 'S'
			ls_FilterString = 'customer_type = "' + as_type + '"'
			dw_customers.SetFilter(ls_FilterString)
			dw_customers.Filter()
			ls_datastring = dw_customers.object.datawindow.data
			ls_FilterString = ''
			If ll_RowCount > 0 Then
				ll_FindRow = 0
				ls_FilterStringcorp = ''
				ls_FilterStringbill = ''
				Do
					ll_FindRow = dw_Selected.Find('include_exclude = "Y" And (customer_type = "C"' + &
							'Or customer_type = "B")', ll_FindRow + 1, ll_RowCount + 1)	
					If ll_FindRow > 0 Then
						ls_CustomerId = dw_selected.GetItemString(ll_FindRow, 'customer_number')
						if dw_selected.GetItemString(ll_FindRow, 'customer_type') = "C" then
							If not lu_StringFunctions.nf_IsEmpty(ls_FilterStringcorp) Then
								ls_FilterStringcorp += " Or "
							end if
							ls_FilterStringcorp += 'corp_id = "' + ls_CustomerId + '"' 
						else
							If not lu_StringFunctions.nf_IsEmpty(ls_FilterStringbill) Then
								ls_FilterStringbill += " Or "
							end if
							ls_FilterStringbill += 'parent_id = "' + ls_CustomerId + '"'
						end if
//						ls_FilterString += ' And customer_type = "' + as_type + &
//								'" And (parent_id = "' + ls_CustomerId + &
//								'" Or corp_id = "' + ls_CustomerId + '")'
					End If
				Loop While ll_FindRow > 0

				ll_FindRow = 0
				ls_FilterString = ''
				Do
					ll_FindRow = dw_Selected.Find('include_exclude = "N" And customer_type = "B"' &
							, ll_FindRow + 1, ll_RowCount + 1)	
					If ll_FindRow > 0 Then
						ls_CustomerId = dw_selected.GetItemString(ll_FindRow, 'customer_number')
						If not lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
							ls_FilterString += " And "
						end if
						ls_FilterString += 'parent_id <> "' + ls_CustomerId + '"'
					End If
				Loop While ll_FindRow > 0				
				If not lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
					dw_customers.SetFilter(ls_FilterString)
					dw_customers.Filter()
				end if
				ls_datastring = dw_customers.object.datawindow.data
			End If
//			If lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
////				This is to get rid of all rows.  you can only exclude customers 
////				it their parent is included.
//				ls_FilterString = 'customer_type = " "'
//			Else 
//				ls_FilterString = Mid(ls_FilterString, 6)
//			End If
//		
//			dw_customers.SetFilter(ls_FilterString)
//			dw_customers.Filter()
			If lu_StringFunctions.nf_IsEmpty(ls_FilterStringcorp) Then
				if not lu_StringFunctions.nf_IsEmpty(ls_FilterStringbill) Then
					if lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
						ls_FilterString = ls_FilterStringbill
					else
						ls_FilterString = ls_FilterStringbill + " And " + ls_FilterString
					end if
				end if
			else
				if lu_StringFunctions.nf_IsEmpty(ls_FilterStringbill) Then
					if lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
						ls_FilterString = ls_FilterStringcorp
					else
						ls_FilterString = ls_FilterStringcorp + " And " + ls_FilterString
					end if
				else
					if lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
						ls_FilterString = ls_FilterStringcorp + " Or " + ls_FilterStringbill
					else
						ls_FilterString = "(" + ls_FilterStringcorp + " Or " + ls_FilterStringbill + ")" + ls_FilterString
					end if
				end if
			end if
				
			if dw_customer_filter.getitemstring(1,'customerdisplaytype') = 'Y' then
				ls_FilterString = wf_displayfilter(ls_FilterString, as_type)
			end if
			If lu_StringFunctions.nf_IsEmpty(ls_FilterString) Then
//				This is to get rid of all rows.  you can only exclude customers 
//				it their parent is included.
				ls_FilterString = 'customer_type = " "'
			End If
			dw_customers.SetFilter(ls_FilterString)
			dw_customers.Filter()
			ls_datastring = dw_customers.object.datawindow.data
	End Choose
End If


dw_customers.Sort()

//ll_rowcount = dw_customers.rowcount()
//for ll_index = 1 to ll_rowcount
//	dw_customers.SelectRow ( ll_index, False)
//next
dw_customers.SetRedraw(True)
Return True


end function

public subroutine wf_dbconnection ();string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

//SQLCA = Create transaction // Create a transaction object
SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")
SQLCA.dbparm = ProfileString(ls_inifile, ls_section, "dbParm", "false")

//if Upper(SQLCA.dbms) = Upper("SNC SQL Native Client") Then					 // Connecting to a SQL Server 
	SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
	SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password
//Else
//	SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
//End If

//if SQLCA.database = 'false' or SQLCA.dbParm = 'false' then 							 // "SMA DATABASE" Not found in the INI
//	SQLCA.database = ProfileString(ls_inifile, "DEFAULT DATABASE", "database", "pblocaldb")
//	SQLCA.dbParm = ProfileString(ls_inifile, "DEFAULT DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
//end if


Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode = -1 Then
	// do nothing
Else
	If SQLCA.SQLCode <> 0 then
		Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
		Disconnect using SQLCA;
		Return
	End If
End if
end subroutine

public function boolean wf_check_vendor_reqd ();Long		ll_sub, &
			ll_RowCount
			
String		ls_rebate_option, &
			ls_type_short_desc

Boolean	lb_rebate_option_found

wf_dbconnection()

ls_rebate_option = is_RebateOption

SELECT tutltypes.type_short_desc
INTO :ls_type_short_desc  
FROM tutltypes

WHERE ( tutltypes.record_type = 'REBASINT' ) AND  
			( tutltypes.type_code = :ls_rebate_option);

If SQLCA.SQLCode = 00 THEN
	lb_rebate_option_found = True
Else
	lb_rebate_option_found = False
END IF

If lb_rebate_option_found or (is_RebateOption = 'V') then
	Return True
Else
	Return False
End IF

end function

on w_rebatecustomerselect.create
int iCurrent
call super::create
this.dw_customers=create dw_customers
this.dw_selected=create dw_selected
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_types=create dw_types
this.dw_customershort=create dw_customershort
this.dw_customer_filter=create dw_customer_filter
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_customers
this.Control[iCurrent+2]=this.dw_selected
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.dw_types
this.Control[iCurrent+6]=this.dw_customershort
this.Control[iCurrent+7]=this.dw_customer_filter
end on

on w_rebatecustomerselect.destroy
call super::destroy
destroy(this.dw_customers)
destroy(this.dw_selected)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_types)
destroy(this.dw_customershort)
destroy(this.dw_customer_filter)
end on

event close;idw_Selected.ShareDataOff()

iu_ParameterStack.uf_initialize()
iu_ParameterStack.uf_Push('String', is_SelectAllInd)

CloseWithReturn(This, iu_ParameterStack)

//iu_ErrorContext.uf_initialize()


end event

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('String', is_RebateType)
iu_ParameterStack.uf_Pop('String', is_RebateOption)
iu_ParameterStack.uf_Pop('String', is_SelectAllInd)
iu_ParameterStack.uf_Pop('Decimal', idc_detail_rebate_rate)
iu_ParameterStack.uf_Pop('Date', idt_HeaderEndDate)
iu_ParameterStack.uf_Pop('Date', idt_HeaderStartDate)
iu_ParameterStack.uf_Pop('datawindow', idw_Selected)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_dwselect', iu_DwSelect)

idw_Selected.ShareData(dw_selected)

end event

event ue_postopen;String			ls_String
Long			ll_row_cnt

dw_types.InsertRow(0)

dw_types.SetItem(1, 'customer_type', 'S')
dw_types.SetItem(1, 'select_all_ind', is_SelectAllInd)

If is_SelectAllInd = 'Y' Then
	dw_types.SetItem(1, 'exclude_include_ind', 'N')
	dw_types.object.exclude_include_ind.protect = 1
Else
	dw_types.SetItem(1, 'exclude_include_ind', 'Y')
End If
	
If is_RebateType = 'Q' Then	
	dw_selected.object.include_exclude.protect = 1
End If

//If is_RebateType = 'P' Then	
//	dw_selected.object.include_exclude.protect = 1
//End If

This.SetRedraw(False)

If Not wf_GetCorps('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If
dw_customers.ImportString(ls_String)

If Not wf_Getbilltos('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If
dw_customers.ImportString(ls_String)

If Not wf_GetShiptos('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If
dw_customers.ImportString(ls_String)

dw_Customers.ResetUpdate()

If Len(Trim(dw_selected.object.datawindow.data)) > 0 Then
	If Not This.wf_UpdateCustomerList() Then
		This.SetRedraw(True)
		iu_ErrorContext.uf_SetText("There was an error deleting customers")
		iu_ErrorContext.uf_SetReturnCode(10)
		iu_Notification.uf_display(iu_ErrorContext)
		return
	End If
	dw_selected.Sort()
End If

This.wf_FilterCustomers('S', dw_types.GetItemString(1, 'exclude_include_ind'), &
		dw_types.GetItemString(1, 'select_all_ind'))

This.SetRedraw(True)


end event

type cb_help from w_abstractresponse`cb_help within w_rebatecustomerselect
integer x = 2331
integer y = 1340
integer taborder = 90
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_rebatecustomerselect
boolean visible = false
integer x = 2034
integer y = 1320
integer taborder = 100
end type

event cb_cancel::clicked;Close(Parent)

end event

type cb_ok from w_abstractresponse`cb_ok within w_rebatecustomerselect
integer x = 2071
integer y = 1340
integer taborder = 80
string text = "&Close"
end type

event cb_ok::clicked;Close(Parent)
end event

type dw_customers from datawindow within w_rebatecustomerselect
event ue_keydwn pbm_dwnkey
integer x = 41
integer y = 308
integer width = 1257
integer height = 992
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_rebate_customer_select"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_keydwn;DataWindow				ldw_datawindow
Long						ll_row


ll_row = This.GetRow()

Choose Case key
	Case keydownarrow!
		ll_row ++
		If ll_row > This.RowCount() Then Return
		ldw_datawindow = This
		iu_DwSelect.uf_initialize(ldw_datawindow, 3)
		iu_dwSelect.uf_select(GetRow() + 1)
	Case keyuparrow!
		ll_row --
		If ll_row <= 0 Then Return
		ldw_datawindow = This
		iu_DwSelect.uf_initialize(ldw_datawindow, 3)
		iu_dwSelect.uf_select(GetRow() - 1)
End Choose

end event

event clicked;DataWindow				ldw_datawindow


IF row = 0 Then Return

ldw_datawindow = This
iu_DwSelect.uf_initialize(ldw_datawindow, 2)
iu_dwSelect.uf_select(row)
end event

type dw_selected from datawindow within w_rebatecustomerselect
event ue_keydwn pbm_dwnkey
integer x = 1426
integer y = 312
integer width = 1728
integer height = 992
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_selectedcustomer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_keydwn;DataWindow				ldw_datawindow
Long						ll_row


ll_row = This.GetRow()

Choose Case key
	Case keydownarrow!
		ll_row ++
		If ll_row > This.RowCount() Then Return
		ldw_datawindow = This
		iu_DwSelect.uf_initialize(ldw_datawindow, 3)
		iu_dwSelect.uf_select(GetRow() + 1)
	Case keyuparrow!
		ll_row --
		If ll_row <= 0 Then Return
		ldw_datawindow = This
		iu_DwSelect.uf_initialize(ldw_datawindow, 3)
		iu_dwSelect.uf_select(GetRow() - 1)
End Choose

end event

event clicked;DataWindow				ldw_datawindow

IF row = 0 Then Return

ldw_datawindow = This
iu_DwSelect.uf_initialize(ldw_datawindow, 2)
iu_dwSelect.uf_select(row)
end event

type cb_1 from commandbutton within w_rebatecustomerselect
integer x = 1307
integer y = 636
integer width = 114
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = ">"
end type

event clicked;String				ls_ExcludeIncludeInd, &
						ls_CustomerId, &
						ls_CustomerType

Long					ll_row, &
						ll_newrow
						
						
ll_row = dw_customers.GetSelectedRow(0)

IF ll_row = 0 Then Return

SetPointer(HourGlass!)

idw_Selected.SetRedraw(False)
Parent.SetRedraw(False)

dw_types.AcceptText()
ls_ExcludeIncludeInd = dw_types.GetItemString(1, 'exclude_include_ind')
ls_CustomerType = dw_types.GetItemString(1, 'customer_type')

Do
	ls_CustomerId = dw_customers.GetItemString(ll_row, 'customer_id')
	If Not Parent.wf_CheckForSubordinate(ll_row) Then 
		ll_row = dw_customers.Find("customer_id = '" + ls_CustomerId + "'", 1, dw_customers.RowCount())
//		ls_CustomerId = dw_customers.GetItemString(ll_row, 'customer_id')
		Parent.wf_GetSelectedCustomer(ls_ExcludeIncludeInd, ls_CustomerId, ls_CustomerType, ll_row)

		dw_customers.DeleteRow(ll_row)
	End If
	ll_row = dw_customers.GetSelectedRow(0)	
Loop While ll_row > 0

dw_selected.Sort()

Parent.SetRedraw(True)
idw_Selected.SetRedraw(True)

end event

type cb_2 from commandbutton within w_rebatecustomerselect
integer x = 1307
integer y = 740
integer width = 114
integer height = 84
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<"
end type

event clicked;long ll_index, ll_rowcount

SetPointer(HourGlass!)

Parent.SetRedraw(False)
idw_selected.SetRedraw(False)

Parent.wf_UnselectCustomers()

Parent.SetRedraw(True)
idw_selected.SetRedraw(True)

ll_rowcount = dw_customers.rowcount()

for ll_index = 1 to ll_rowcount
	dw_customers.SelectRow ( ll_index, False)
next


end event

type dw_types from datawindow within w_rebatecustomerselect
integer x = 1486
integer y = 28
integer width = 1504
integer height = 252
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_selected_types"
boolean border = false
boolean livescroll = true
end type

event itemchanged;String					ls_String
long						ll_rowcount, ll_index

Choose Case dwo.name
	Case 'customer_type'
		Parent.wf_FilterCustomers(Data, This.GetItemString(1, 'exclude_include_ind'), &
				This.GetItemString(1, 'select_all_ind'))
	Case 'exclude_include_ind'
		Parent.wf_FilterCustomers(This.GetItemString(1, 'customer_type'), Data, &
				This.GetItemString(1, 'select_all_ind'))
	Case 'select_all_ind'
		If data = 'Y' Then
			If dw_selected.RowCount() > 0 Then
				If MessageBox('Select All Customers', 'Checking this option will remove ' + &
						'all selected customers.  Do you want to continue?', Exclamation!, &
								YesNo!) = 2 Then Return 2
				SetPointer(HourGlass!)
				Parent.SetRedraw(False)
				dw_selected.SelectRow(0, True)
				Parent.wf_UnselectCustomers()
				Parent.SetRedraw(True)
			End If
			This.object.exclude_include_ind.protect = 1
			This.SetItem(1, 'exclude_include_ind', 'N')
		Else
			If dw_selected.RowCount() > 0 Then
				If MessageBox('Select All Customers', 'Unchecking this option will remove ' + &
						'all selected customers.  Do you want to continue?', Exclamation!, &
								YesNo!) = 2 Then Return 2
				SetPointer(HourGlass!)
				Parent.SetRedraw(False)
				dw_selected.SelectRow(0, True)
				Parent.wf_UnselectCustomers()
				Parent.SetRedraw(True)
			End If
			This.object.exclude_include_ind.protect = 0
			This.SetItem(1, 'exclude_include_ind', 'Y')
		End If
		is_SelectAllInd = Data
		Parent.wf_FilterCustomers(This.GetItemString(1, 'customer_type'), &
				This.GetItemString(1, 'exclude_include_ind'), Data)
End Choose
ll_rowcount = dw_customers.rowcount()
for ll_index = 1 to ll_rowcount
	dw_customers.SelectRow ( ll_index, False)
next
end event

event constructor;this.setitem(1,"customer_type", "S")

end event

type dw_customershort from datawindow within w_rebatecustomerselect
event ue_postconstructor ( )
integer x = 183
integer y = 1328
integer width = 914
integer height = 136
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_customershort"
boolean border = false
boolean livescroll = true
end type

event editchanged;String	ls_FoundProduct, &
			ls_productcode, &
			ls_just_see, &
			ls_just_see_num, &
			ls_just_see_num2, &
			ls_just_see_nam, &
			ls_just_see_nam2
integer	li_row, &
         li_max_row, &
			li_rowcount
boolean	lb_match

if  dw_customers.rowcount() = 0 then 
	is_whatwastyped = Data
	This.SetRedraw(True)
	return
end if


If Len(data) = 0 Or (String(dwo.Name)  <> 'customer_number' and String(dwo.Name) <> 'customer_name') Then
	is_whatwastyped = Data
	This.SetRedraw(True)
	Return
End if

ids_SortNames.Reset()
ls_just_see = dw_customers.object.datawindow.data
ids_SortNames.importstring(dw_customers.object.datawindow.data)

if String(dwo.Name) = 'customer_number' then
	ls_just_see_num = ids_SortNames.object.datawindow.data
	ids_SortNames.SetSort('customer_id')
	ids_SortNames.Sort()
	ls_just_see_num2 = ids_SortNames.object.datawindow.data
	ii_keystyped ++
	If ii_keystyped > Len(data) Then ii_keystyped = Len(data)
	If is_whatwastyped <> Data Then
		is_whatwastyped = Data
	End if
	li_row = 1 
	li_rowcount = ids_SortNames.rowcount()
	do until Upper(Data) <= Left(trim(ids_SortNames.GetItemString(li_row, "customer_id")), Len(Data)) or li_row = li_rowcount
		li_row ++
	loop 
	li_row = dw_customers.Find("customer_id = '" + ids_SortNames.Getitemstring(li_row,'customer_id') + "'", 1, li_rowcount)
	if  ii_last_row_highlighted <> 0 then
		dw_customers.selectrow(ii_last_row_highlighted, False)
	end if
	
	if Upper(Data) = Left(trim(ids_SortNames.GetItemString(li_row, "customer_id")), Len(Data)) then
		ii_last_row_highlighted = li_row
		dw_customers.selectrow(li_row, True)
		This.setitem(1,'customer_name', dw_customers.getitemstring( li_row, 'customer_name'))
	else
		ii_last_row_highlighted = 0
		This.setitem(1,'customer_name', '    ')
	end if
	
	dw_customers.ScrollToRow (li_row)
	this.setfocus()

	This.SetRedraw(True)
Else
	ls_just_see_num = ids_SortNames.object.datawindow.data
	
	ids_SortNames.SetSort('customer_name D')
	
	ids_SortNames.Sort()
	ls_just_see_nam2 = ids_SortNames.object.datawindow.data
	ii_keystyped2 ++
	If ii_keystyped2 > Len(data) Then ii_keystyped2 = Len(data)
	If is_whatwastyped <> Data Then
		is_whatwastyped = Data
	End if
	li_max_row = ids_SortNames.rowcount()
	li_row = 1 
	lb_match = false
	
	do until lb_match or li_row >= li_max_row
		if Left(trim(ids_SortNames.GetItemString(li_row, 'customer_name')), 1) = " " then
			if Upper(Data) >=  mid(ids_SortNames.GetItemString(li_row, 'customer_name'),2, Len(Data)) then
				lb_match = true
			end if
		else
		 	if Upper(Data) >= Left(trim(ids_SortNames.GetItemString(li_row, 'customer_name')), Len(Data)) then
				lb_match = true
			end if
		end if
		li_row ++ 
	loop
	
	li_row = dw_customers.Find("customer_id = '" + ids_SortNames.Getitemstring(li_row,'customer_id') + "'", 1, li_max_row)
	if  ii_last_row_highlighted <> 0 then
		dw_customers.selectrow(ii_last_row_highlighted, False)
	end if
	
	//make changes
	ii_last_row_highlighted = li_row
	dw_customers.selectrow(li_row, True)
	This.setitem(1,'customer_number', dw_customers.getitemstring( li_row, 'customer_id'))
	dw_customers.ScrollToRow ( li_row )
	this.setfocus()

	This.SetRedraw(True)
End If
end event

event constructor;this.insertrow(0)
ids_SortNames = create datastore
ids_SortNames.Dataobject = "d_rebate_customer_select"
end event

type dw_customer_filter from datawindow within w_rebatecustomerselect
event ue_postitemchange ( )
integer x = 50
integer y = 24
integer width = 1243
integer height = 224
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_customer_filter"
end type

event ue_postitemchange;	wf_filtercustomers(dw_types.getitemstring(1,'customer_type'), &
									dw_types.getitemstring(1,'exclude_include_ind'), &
									dw_types.getitemstring(1,'select_all_ind'))
end event

event constructor;this.insertrow(0)
dw_customer_filter.setitem(1, 'customerdisplaytype', 'N')
end event

event itemchanged;long			ll_FindRow, ll_RowCount, ll_index
string		ls_findstring, ls_debuguse
u_String_Functions lu_StringFunctions

choose case dwo.name
	case 'customerdisplaytype'
		if lu_StringFunctions.nf_IsEmpty(this.getitemstring(1,"customerfilter")) then
			this.setfocus()
			messagebox("Required Field", &
				"Customer ID is a required field for filter. Use Filter will be set to No.",StopSign!,OK!)
//			this.setitem(1,'customerdisplaytype','N')
			return 2
		end if
		if lu_StringFunctions.nf_IsEmpty(this.getitemstring(1,"customerfiltertype")) then
			this.setfocus()
			messagebox("Required Field", &
				"Customer Type is a required field for filter. Use Filter will be set to No.",StopSign!,OK!)
//			this.setitem(1,'customerdisplaytype','N')
			return 2
		end if
		this.postevent("ue_postitemchange")
		
	case 'customerfilter'
		if this.getitemstring(1,"customerdisplaytype") = "Y" then
			this.postevent("ue_postitemchange")
		end if
		
	case "customerfiltertype"
		if this.getitemstring(1,"customerdisplaytype") = "Y" then
			this.postevent("ue_postitemchange")
		end if
		
end choose
end event

