﻿$PBExportHeader$w_rebateheaderinquire.srw
forward
global type w_rebateheaderinquire from w_abstractresponseext
end type
type dw_rebateheader from u_rebate_header within w_rebateheaderinquire
end type
end forward

global type w_rebateheaderinquire from w_abstractresponseext
int X=96
int Y=376
int Width=2208
int Height=432
dw_rebateheader dw_rebateheader
end type
global w_rebateheaderinquire w_rebateheaderinquire

type variables
String			is_RebateCode, &
			is_title

u_abstracterrorcontext	iu_errorcontext

u_abstractparameterstack	iu_parameterstack

u_AbstractClassFactory	iu_ClassFactory

u_NotificationController	iu_Notification	

end variables

on w_rebateheaderinquire.create
int iCurrent
call super::create
this.dw_rebateheader=create dw_rebateheader
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_rebateheader
end on

on w_rebateheaderinquire.destroy
call super::destroy
destroy(this.dw_rebateheader)
end on

event open;call super::open;String					ls_RebateCode

DataWindowChild		ldwc_TransferCustomer

u_String_Functions	lu_strings
Integer					il_temp


iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_RebateCode)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

This.Title = is_title + ' Inquire'

dw_rebateheader.uf_initialize(iu_classfactory, iu_ErrorContext, iu_notification, True, True)

If Len(Trim(is_RebateCode)) > 0 Then
	ls_RebateCode = lu_strings.nf_GetToken(is_RebateCode, '~t')
	dw_rebateheader.uf_SetRebateCode(ls_RebateCode)
End If

is_RebateCode = 'Cancel'

dw_rebateheader.SetFocus()
end event

event close;DataWindowChild		ldwc_temp


iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_RebateCode)
iu_ParameterStack.uf_Push('string', is_title)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)

end event

type cb_help from w_abstractresponseext`cb_help within w_rebateheaderinquire
int X=1911
int Y=184
int TabOrder=40
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_rebateheaderinquire
int X=1614
int Y=184
int TabOrder=30
boolean Cancel=true
end type

event cb_cancel::clicked;is_RebateCode = 'Cancel'

Close(Parent)

end event

type cb_ok from w_abstractresponseext`cb_ok within w_rebateheaderinquire
int X=1349
int Y=184
int TabOrder=20
boolean Default=true
end type

event cb_ok::clicked;If dw_rebateheader.uf_validate(is_RebateCode) Then Close(Parent)


end event

type dw_rebateheader from u_rebate_header within w_rebateheaderinquire
event ue_post_constructor ( )
int X=27
int Y=32
int Width=2098
boolean BringToTop=true
end type

