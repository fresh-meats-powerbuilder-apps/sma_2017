﻿$PBExportHeader$w_rebatemain.srw
forward
global type w_rebatemain from w_abstractsheetext
end type
type dw_rebate_insert_items from datawindow within w_rebatemain
end type
type dw_header_display from datawindow within w_rebatemain
end type
type tab_1 from tab within w_rebatemain
end type
type tabpage_header from userobject within tab_1
end type
type dw_header from datawindow within tabpage_header
end type
type tabpage_header from userobject within tab_1
dw_header dw_header
end type
type tabpage_detail from userobject within tab_1
end type
type dw_detail from datawindow within tabpage_detail
end type
type tabpage_detail from userobject within tab_1
dw_detail dw_detail
end type
type tabpage_customergroup from userobject within tab_1
end type
type dw_customergroup from datawindow within tabpage_customergroup
end type
type tabpage_customergroup from userobject within tab_1
dw_customergroup dw_customergroup
end type
type tabpage_customer from userobject within tab_1
end type
type dw_customer from datawindow within tabpage_customer
end type
type tabpage_customer from userobject within tab_1
dw_customer dw_customer
end type
type tabpage_division from userobject within tab_1
end type
type dw_division from datawindow within tabpage_division
end type
type dw_divisionexclude from datawindow within tabpage_division
end type
type tabpage_division from userobject within tab_1
dw_division dw_division
dw_divisionexclude dw_divisionexclude
end type
type tabpage_productgroup from userobject within tab_1
end type
type dw_prodgrp_exclude from datawindow within tabpage_productgroup
end type
type dw_productgroupexclude from datawindow within tabpage_productgroup
end type
type dw_include_all_volume from datawindow within tabpage_productgroup
end type
type dw_productgroup from datawindow within tabpage_productgroup
end type
type tabpage_productgroup from userobject within tab_1
dw_prodgrp_exclude dw_prodgrp_exclude
dw_productgroupexclude dw_productgroupexclude
dw_include_all_volume dw_include_all_volume
dw_productgroup dw_productgroup
end type
type tabpage_product from userobject within tab_1
end type
type dw_product from datawindow within tabpage_product
end type
type tabpage_product from userobject within tab_1
dw_product dw_product
end type
type tabpage_location from userobject within tab_1
end type
type dw_locationexclude from datawindow within tabpage_location
end type
type dw_location from datawindow within tabpage_location
end type
type tabpage_location from userobject within tab_1
dw_locationexclude dw_locationexclude
dw_location dw_location
end type
type tab_1 from tab within w_rebatemain
tabpage_header tabpage_header
tabpage_detail tabpage_detail
tabpage_customergroup tabpage_customergroup
tabpage_customer tabpage_customer
tabpage_division tabpage_division
tabpage_productgroup tabpage_productgroup
tabpage_product tabpage_product
tabpage_location tabpage_location
end type
end forward

global type w_rebatemain from w_abstractsheetext
integer x = 5
integer y = 4
integer width = 3026
integer height = 1948
string title = "Charge Main"
long backcolor = 67108864
event ue_selectcustomers ( )
event ue_selectdivisions ( )
event ue_selectproductgroups ( )
event ue_selectproducts ( string as_tab_page )
event ue_get_data ( string as_value )
event ue_set_data ( string as_data_item,  string as_value )
event ue_selectlocations ( )
event ue_selectcustomergroups ( )
dw_rebate_insert_items dw_rebate_insert_items
dw_header_display dw_header_display
tab_1 tab_1
end type
global w_rebatemain w_rebatemain

type variables
 Boolean			ib_DeleteHeader, &
					ib_changes, &
					ib_UpdatedDivisions, &
					ib_UpdatedProducts, &
					ib_UpdatedProductGroups, &
					ib_UpdatedLocations, &
					ib_UpdatedCustomerGroups, &
					ib_ReInquire, &
                      ib_exclude_ind_div, &
                      ib_exclude_ind_grp, &
					ib_save_flag, &
					ib_modified

DataStore		ids_divisions, &
					ids_products, &
					ids_productgroups, &
					ids_locations, &
					ids_customergroups, &
					ids_org_div, &
					ids_org_prdgrp, &
					ids_org_prod, &
					ids_org_loc, &
					ids_org_custgrp, &
					ids_RebateList, &
					ids_creditmet, &
					ids_customers
					

//dmk
String			is_Title, &
					is_RebateCode, &
					is_RebateType, &
					is_dddw_uom_string, &
					is_dddw_volperiod_string, &
					is_dddw_payperiod_string, &
					is_dddw_rebate_list_string, &
					is_dddw_pricing_string, &
					is_dddw_currency_string, &
					is_PricingString, &
					is_selected_ind_array, &
					is_saved_pricing_array, &
					is_RebateOption

Decimal			idc_detail_rebate_rate

Long				il_last_seq_no_product, &
					il_last_seq_no_group, &
					il_selected_ind_max, &
					il_selected_row

Date				idt_header_start_date, &
					idt_header_end_date
					
Window									iw_This
u_AbstractClassFactory				iu_ClassFactory
u_AbstractPlantDataAccess			iu_PlantDataAccess
u_AbstractTutlTypeDataAccess		iu_TutlTypeDataAccess
u_RebateDataAccess					iu_RebateDataAccess
u_ErrorContext							iu_ErrorContext
u_NotificationController			iu_Notification	
u_abstractcalendar					iu_calendar	
w_smaframe								iw_frame	
w_rebatemain							iw_parent
u_AbstractTransaction				iu_ODBCTransaction

end variables

forward prototypes
public function boolean wf_getrebatedescription (string as_rebatecode, ref string as_description)
public function boolean wf_buildcustomerstring (ref string as_updatestring)
public function boolean wf_defaultstartdates (date adt_date)
public function boolean wf_defaultenddates (date adt_date)
public function boolean wf_builddivisionstring (string as_type, ref string as_updatestring)
public function boolean wf_check_volumes (string as_volumes, integer as_row, long al_volume)
public function boolean wf_check_dates (integer as_row, date as_date, string as_string)
public function string wf_getupdateind (long al_row, datawindow adw_datawindow)
public subroutine wf_setfocus ()
public function boolean wf_fillcredmeth ()
public function boolean wf_filldetpayperiod ()
public function boolean wf_filldetrebcrtyp ()
public function boolean wf_filldetvolperiod ()
public function boolean wf_fillrebatedropdown ()
public function boolean wf_updatedivisions (string as_type, ref string as_updatestring)
public function boolean wf_fillpricingind ()
public subroutine wf_get_selected_pricinginds (datawindow adw_datawindow)
public subroutine wf_set_selected_pricinginds ()
public subroutine wf_saveproduct_pricinginds (string as_selected_tab)
public subroutine wf_setproduct_pricinginds (string as_selected_tab)
public function boolean wf_fillcountrycode ()
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean wf_buildheaderstring (ref string as_updatestring)
public function boolean wf_fillrebatetype ()
public subroutine wf_dbconnection ()
public function boolean wf_builddetailstring (ref string as_updatestring)
public function boolean wf_updateproducts (string as_type, ref string as_updatestring)
public function boolean wf_createoriginaldatastore (string as_type)
public function boolean wf_updateproductgroups (string as_type, ref string as_updatestring)
public function boolean wf_fillrebatedateoption ()
public function boolean wf_updatelocations (string as_type, ref string as_updatestring)
public function boolean wf_updatecustomergroups (string as_type, ref string as_updatestring)
public function boolean wf_fillheadcurr ()
public function boolean wf_fillheaduom ()
public function boolean wf_check_vendor ()
public function boolean wf_fillbusiness ()
public function boolean wf_fillsaleslocation ()
public function boolean wf_fillloadinginstruction ()
end prototypes

event ue_selectcustomers();Date					ldt_StartDate,	ldt_EndDate

String				ls_SelectAllInd, ls_Rate
u_ParameterStack	lu_ParameterStack
u_String_Functions	lu_StringFunctions

tab_1.tabpage_header.dw_header.AcceptText()
ldt_StartDate = tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date')
ldt_EndDate = tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date')

If idc_detail_rebate_rate = 0 Then
	ls_Rate = String(tab_1.tabpage_detail.dw_detail.GetItemNumber(1, 'rate'))
	If lu_StringFunctions.nf_IsEmpty(ls_Rate) Then 
		idc_detail_rebate_rate = 0
	Else
		idc_detail_rebate_rate = tab_1.tabpage_detail.dw_detail.GetItemDecimal(1, 'rate')
	End If
End If

ls_SelectAllInd = tab_1.tabpage_header.dw_header.GetItemString(1, 'customer_select_all_ind')

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('datawindow', tab_1.tabpage_customer.dw_customer)
lu_ParameterStack.uf_Push('Date', ldt_StartDate)
lu_ParameterStack.uf_Push('Date', ldt_EndDate)
lu_ParameterStack.uf_Push('Decimal', idc_detail_rebate_rate)
lu_ParameterStack.uf_Push('String', ls_SelectAllInd)
lu_ParameterStack.uf_Push('String', is_RebateOption)
lu_ParameterStack.uf_Push('String', is_RebateType)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

iu_ClassFactory.uf_GetResponseWindow( "w_rebatecustomerselect", lu_ParameterStack )
lu_ParameterStack.uf_Pop('String', ls_SelectAllInd)

tab_1.tabpage_header.dw_header.SetItem(1, 'customer_select_all_ind', ls_SelectAllInd)

end event

event ue_selectdivisions();String					ls_Divisions, ls_Type, ls_exclude_ind_div, ls_exclude_ind_grp
Long						ll_rebate_id
window               lw_window_name
u_ParameterStack		lu_ParameterStack

tab_1.tabpage_header.dw_header.AcceptText()

ls_divisions = tab_1.tabpage_division.dw_division.object.datawindow.data
ll_rebate_id = tab_1.tabpage_header.dw_header.GetItemNumber(1, "rebate_id")

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('String', ls_divisions)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

lu_ParameterStack.uf_Push('datastore', ids_divisions)
lu_ParameterStack.uf_Push('datastore', ids_products)
lu_ParameterStack.uf_Push('datastore', ids_productgroups)
lu_ParameterStack.uf_Push('datastore', ids_customergroups)
lu_ParameterStack.uf_Push('datastore', ids_locations)

lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_productgroup.dw_productgroupexclude.getitemstring(1, 'exclude_ind'))
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_division.dw_divisionexclude.getitemstring(1, 'exclude_ind'))
//dmk use rebate option 
//choose case tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')
choose case is_RebateOption
	case "V"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
//	case "C"
//		lu_ParameterStack.uf_Push('String', 'C')
	case "A"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
//	case "O"
//		lu_ParameterStack.uf_Push('String', 'O')
//	case "I"
//		lu_ParameterStack.uf_Push('String', 'I')	
	case else
		lu_ParameterStack.uf_Push('String', '  ')
end choose

lu_ParameterStack.uf_Push('String', 'D')

//  RevGLL  // *(Begin)
lu_ParameterStack.uf_Push('Date', idt_header_start_date)
lu_ParameterStack.uf_Push('Date', idt_header_end_date)
lu_ParameterStack.uf_Push('Decimal', idc_detail_rebate_rate)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Push('Long', ll_rebate_id)
//  RevGLL  // *(End)

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetResponseWindow( "w_rebateselect", lu_ParameterStack )

lu_ParameterStack.uf_Pop('String', ls_Type)

lu_ParameterStack.uf_Pop('String', ls_exclude_ind_div)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_grp)

lu_ParameterStack.uf_Pop('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_group)

lu_ParameterStack.uf_Pop('datastore', ids_org_loc)
lu_ParameterStack.uf_Pop('datastore', ids_org_custgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prdgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prod)
lu_ParameterStack.uf_Pop('datastore', ids_org_div)

lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('String', ls_Divisions)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If ls_divisions = 'Cancel' Then
	Return
End If

tab_1.tabpage_division.dw_division.Reset()
tab_1.tabpage_division.dw_division.ImportString(ls_Divisions)

end event

event ue_selectproductgroups();String					ls_productgroups, &
							ls_Type, &
							ls_hold_string, &
							ls_rate, &
							ls_exclude_ind_div, &
							ls_exclude_ind_grp

Long						ll_rebate_id

window               lw_window_name
u_ParameterStack		lu_ParameterStack
u_String_Functions	lu_StringFunctions

tab_1.tabpage_header.dw_header.AcceptText()
ls_productgroups = tab_1.tabpage_productgroup.dw_productgroup.object.datawindow.data
ll_rebate_id = tab_1.tabpage_header.dw_header.GetItemNumber(1, "rebate_id")

If idc_detail_rebate_rate = 0 Then
	ls_Rate = String(tab_1.tabpage_detail.dw_detail.GetItemNumber(1, 'rate'))
	If lu_StringFunctions.nf_IsEmpty(ls_Rate) Then 
		idc_detail_rebate_rate = 0
	Else
		idc_detail_rebate_rate = tab_1.tabpage_detail.dw_detail.GetItemDecimal(1, 'rate')
	End If
End If

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()
lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('String', ls_productgroups)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Push('datastore', ids_divisions)
lu_ParameterStack.uf_Push('datastore', ids_products)
lu_ParameterStack.uf_Push('datastore', ids_productgroups)
lu_ParameterStack.uf_Push('datastore', ids_customergroups)
lu_ParameterStack.uf_Push('datastore', ids_locations)
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_productgroup.dw_productgroupexclude.getitemstring(1, 'exclude_ind'))
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_division.dw_divisionexclude.getitemstring(1, 'exclude_ind'))

//dmk change to use rebate_option
//choose case tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')
//	case "V"
//		lu_ParameterStack.uf_Push('String', 'V')
//	case "A"
//		lu_ParameterStack.uf_Push('String', 'A')
//		wf_saveproduct_pricinginds("ProductGroup")
//	case "C"
//		lu_ParameterStack.uf_Push('String', 'C')
//		wf_saveproduct_pricinginds("ProductGroup")
//	case "O"
//		lu_ParameterStack.uf_Push('String', 'O')	
//		wf_saveproduct_pricinginds("ProductGroup")
//	case "I"
//		lu_ParameterStack.uf_Push('String', 'I')	
//		wf_saveproduct_pricinginds("ProductGroup")		
//	case else
//		lu_ParameterStack.uf_Push('String', ' ')
//end choose

choose case is_RebateOption 
	case "V"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
		wf_saveproduct_pricinginds("ProductGroup")
	case "A"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
		wf_saveproduct_pricinginds("ProductGroup")
	case else
		lu_ParameterStack.uf_Push('String', '  ')
end Choose

lu_ParameterStack.uf_Push('String', 'G')

//  RevGLL  // *(Begin)
lu_ParameterStack.uf_Push('Date', idt_header_start_date)
lu_ParameterStack.uf_Push('Date', idt_header_end_date)
lu_ParameterStack.uf_Push('Decimal', idc_detail_rebate_rate)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Push('Long', ll_rebate_id)
//  RevGLL  // *(End)
iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetResponseWindow( "w_rebateselect", lu_ParameterStack )
lu_ParameterStack.uf_Pop('String', ls_Type)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_div)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_grp)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Pop('datastore', ids_org_loc)
lu_ParameterStack.uf_Pop('datastore', ids_org_custgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prdgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prod)
lu_ParameterStack.uf_Pop('datastore', ids_org_div)
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('String', ls_productgroups)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If ls_productgroups = 'Cancel' Then	
	Return 
End If

tab_1.tabpage_productgroup.dw_productgroup.Reset()
tab_1.tabpage_productgroup.dw_productgroup.ImportString(ls_productgroups)
//dmk use rebate option
//If (tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'A' OR &
//	 tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'C' OR & 
//	 tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'O' OR &
//	 tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'I') Then
//If is_RebateOption = 'A' Then
wf_setproduct_pricinginds("ProductGroup")
//End If

end event

event ue_selectproducts(string as_tab_page);String					ls_product, &
							ls_Type, ls_exlude_ind, &
							ls_exclude_ind_div, &
							ls_exclude_ind_grp, &
							ls_hold_string, &
							ls_rate

Long						ll_rebate_id

u_ParameterStack		lu_ParameterStack
u_String_Functions	lu_StringFunctions

tab_1.tabpage_header.dw_header.AcceptText()
ls_product = tab_1.tabpage_product.dw_product.object.datawindow.data
ll_rebate_id = tab_1.tabpage_header.dw_header.GetItemNumber(1, "rebate_id")

If idc_detail_rebate_rate = 0 Then
	ls_Rate = String(tab_1.tabpage_detail.dw_detail.GetItemNumber(1, 'rate'))
	If lu_StringFunctions.nf_IsEmpty(ls_Rate) Then 
		idc_detail_rebate_rate = 0
	Else
		idc_detail_rebate_rate = tab_1.tabpage_detail.dw_detail.GetItemDecimal(1, 'rate')
	End If
End If

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()
lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('String', ls_product)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Push('datastore', ids_divisions)
lu_ParameterStack.uf_Push('datastore', ids_products)
lu_ParameterStack.uf_Push('datastore', ids_productgroups)
lu_ParameterStack.uf_Push('datastore', ids_customergroups)
lu_ParameterStack.uf_Push('datastore', ids_locations)
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_productgroup.dw_productgroupexclude.getitemstring(1, 'exclude_ind'))
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_division.dw_divisionexclude.getitemstring(1, 'exclude_ind'))
//dmk use rebate option  
//choose case tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')
//	case "V"
//		lu_ParameterStack.uf_Push('String', 'V')
//	case "A"
//		lu_ParameterStack.uf_Push('String', 'A')
//		//Save off Pricing data for each row
//		wf_saveproduct_pricinginds("Product")
//	case "C"
//		lu_ParameterStack.uf_Push('String', 'C')
//		//Save off Pricing data for each row
//		wf_saveproduct_pricinginds("Product")
//	case "O"
//		lu_ParameterStack.uf_Push('String', 'O')	
//		//Save off Pricing data for each row
//		wf_saveproduct_pricinginds("Product")
//	case "I"
//		lu_ParameterStack.uf_Push('String', 'I')	
//		//Save off Pricing data for each row
//		wf_saveproduct_pricinginds("Product")
//	case else
//		lu_ParameterStack.uf_Push('String', ' ')
//end choose	 
choose case is_RebateOption
	case 'V'
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
	case 'A'
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
		//Save off Pricing data for each row
		wf_saveproduct_pricinginds("Product")
	case else
		lu_ParameterStack.uf_Push('String', '  ')
	
end choose

lu_ParameterStack.uf_Push('String', 'P')

//  RevGLL  // *(Begin)
lu_ParameterStack.uf_Push('Date', idt_header_start_date)
lu_ParameterStack.uf_Push('Date', idt_header_end_date)
lu_ParameterStack.uf_Push('Decimal', idc_detail_rebate_rate)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Push('Long', ll_rebate_id)
//  RevGLL  // *(End)
iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetResponseWindow( "w_rebateselect", lu_ParameterStack )
lu_ParameterStack.uf_Pop('String', ls_Type)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_div)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_grp)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Pop('datastore', ids_org_loc)
lu_ParameterStack.uf_Pop('datastore', ids_org_custgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prdgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prod)
lu_ParameterStack.uf_Pop('datastore', ids_org_div)
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('String', ls_product)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If ls_product = 'Cancel' Then
	Return
End If

tab_1.tabpage_product.dw_product.Reset()
tab_1.tabpage_product.dw_product.ImportString(ls_product)
//dmk check rebate option instead of rebate type
//If (tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'A' OR & 
//    tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'C' OR & 
//	 tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'O' OR &
//	 tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'I') Then
If is_RebateOption = 'A' Then
		wf_setproduct_pricinginds("Product")
End If

end event

event ue_get_data(string as_value);Choose Case as_value
	Case "error"
		Message.PowerObjectParm = iu_ErrorContext
	Case "ClassFactory"
		Message.PowerObjectParm = iu_ClassFactory
	Case "Notify"
		Message.PowerObjectParm = iu_Notification		
	Case "PriceInd"
		Message.StringParm = is_PricingString
	Case "selected"
		Message.StringParm = is_selected_ind_array
	Case else
		Message.StringParm = ''
End Choose
		
end event

event ue_set_data(string as_data_item, string as_value);choose case as_data_item
	case "selected"
		is_selected_ind_array = as_value
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
end choose
end event

event ue_selectlocations();String					     ls_Locations, ls_Type, ls_exclude_ind_div, ls_exclude_ind_grp
Long						ll_rebate_id 
window                    lw_window_name
u_ParameterStack		lu_ParameterStack

tab_1.tabpage_header.dw_header.AcceptText()

ls_locations = tab_1.tabpage_location.dw_location.object.datawindow.data
ll_rebate_id = tab_1.tabpage_header.dw_header.GetItemNumber(1, "rebate_id")

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('String', ls_locations)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

lu_ParameterStack.uf_Push('datastore', ids_divisions)
lu_ParameterStack.uf_Push('datastore', ids_products)
lu_ParameterStack.uf_Push('datastore', ids_productgroups)
lu_ParameterStack.uf_Push('datastore', ids_customergroups)
lu_ParameterStack.uf_Push('datastore', ids_locations)

lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_productgroup.dw_productgroupexclude.getitemstring(1, 'exclude_ind'))
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_location.dw_locationexclude.getitemstring(1, 'exclude_ind'))
//dmk use rebate option 
//choose case tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')
choose case is_RebateOption
	case "V"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
//	case "C"
//		lu_ParameterStack.uf_Push('String', 'C')
	case "A"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
//	case "O"
//		lu_ParameterStack.uf_Push('String', 'O')
//	case "I"
//		lu_ParameterStack.uf_Push('String', 'I')	
	case else
		lu_ParameterStack.uf_Push('String', '  ')
end choose

lu_ParameterStack.uf_Push('String', 'O')

lu_ParameterStack.uf_Push('Date', idt_header_start_date)
lu_ParameterStack.uf_Push('Date', idt_header_end_date)
lu_ParameterStack.uf_Push('Decimal', idc_detail_rebate_rate)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Push('Long', ll_rebate_id)

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetResponseWindow( "w_rebateselect", lu_ParameterStack )

lu_ParameterStack.uf_Pop('String', ls_Type)

lu_ParameterStack.uf_Pop('String', ls_exclude_ind_div)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_grp)

lu_ParameterStack.uf_Pop('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_group)

lu_ParameterStack.uf_Pop('datastore', ids_org_loc)
lu_ParameterStack.uf_Pop('datastore', ids_org_custgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prdgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prod)
lu_ParameterStack.uf_Pop('datastore', ids_org_div)

lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('String', ls_Locations)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If ls_locations = 'Cancel' Then
	Return
End If

tab_1.tabpage_location.dw_location.Reset()
tab_1.tabpage_location.dw_location.ImportString(ls_Locations)

end event

event ue_selectcustomergroups();String					ls_customergroups, &
							ls_Type, &
							ls_hold_string, &
							ls_rate, &
							ls_exclude_ind_div, &
							ls_exclude_ind_grp

Long						ll_rebate_id

window               lw_window_name
u_ParameterStack		lu_ParameterStack
u_String_Functions	lu_StringFunctions

tab_1.tabpage_header.dw_header.AcceptText()
ls_customergroups = tab_1.tabpage_customergroup.dw_customergroup.object.datawindow.data
ll_rebate_id = tab_1.tabpage_header.dw_header.GetItemNumber(1, "rebate_id")

If idc_detail_rebate_rate = 0 Then
	ls_Rate = String(tab_1.tabpage_detail.dw_detail.GetItemNumber(1, 'rate'))
	If lu_StringFunctions.nf_IsEmpty(ls_Rate) Then 
		idc_detail_rebate_rate = 0
	Else
		idc_detail_rebate_rate = tab_1.tabpage_detail.dw_detail.GetItemDecimal(1, 'rate')
	End If
End If

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()
lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('String', ls_customergroups)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Push('datastore', ids_divisions)
lu_ParameterStack.uf_Push('datastore', ids_products)
lu_ParameterStack.uf_Push('datastore', ids_productgroups)
lu_ParameterStack.uf_Push('datastore', ids_customergroups)
lu_ParameterStack.uf_Push('datastore', ids_locations)
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_productgroup.dw_productgroupexclude.getitemstring(1, 'exclude_ind'))
lu_ParameterStack.uf_Push('String', this.tab_1.tabpage_division.dw_divisionexclude.getitemstring(1, 'exclude_ind'))


choose case is_RebateOption 
	case "V"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
	case "A"
		lu_ParameterStack.uf_Push('String', is_RebateType)
		lu_ParameterStack.uf_Push('String', is_RebateOption)
	case else
		lu_ParameterStack.uf_Push('String', '  ')
end Choose

lu_ParameterStack.uf_Push('String', 'S')

lu_ParameterStack.uf_Push('Date', idt_header_start_date)
lu_ParameterStack.uf_Push('Date', idt_header_end_date)
lu_ParameterStack.uf_Push('Decimal', idc_detail_rebate_rate)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Push('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Push('Long', ll_rebate_id)

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetResponseWindow( "w_rebateselect", lu_ParameterStack )
lu_ParameterStack.uf_Pop('String', ls_Type)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_div)
lu_ParameterStack.uf_Pop('String', ls_exclude_ind_grp)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_product)
lu_ParameterStack.uf_Pop('Long', il_last_seq_no_group)
lu_ParameterStack.uf_Pop('datastore', ids_org_loc)
lu_ParameterStack.uf_Pop('datastore', ids_org_custgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prdgrp)
lu_ParameterStack.uf_Pop('datastore', ids_org_prod)
lu_ParameterStack.uf_Pop('datastore', ids_org_div)
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('String', ls_customergroups)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If ls_customergroups = 'Cancel' Then	
	Return 
End If

tab_1.tabpage_customergroup.dw_customergroup.Reset()
tab_1.tabpage_customergroup.dw_customergroup.ImportString(ls_customergroups)


end event

public function boolean wf_getrebatedescription (string as_rebatecode, ref string as_description);Long				ll_rowcount, &
					ll_row

ll_rowcount = ids_rebatelist.RowCount()

If ll_rowcount > 0 Then
	ll_row = ids_rebatelist.Find('rebate_id = ' + as_rebatecode, 1, ll_rowcount + 1)
	If ll_row > 0 Then
		as_description = ids_rebatelist.GetItemString(ll_row, 'description')
		Return True
	End If
End If

Return False
end function

public function boolean wf_buildcustomerstring (ref string as_updatestring);Long							ll_DeletedCount, &
								ll_row, &
								ll_row2, &
								ll_OriginalRowCount
								
String						    ls_UpdateString, ls_vendor_id, &
								ls_org_start_date, &
								ls_charge_rate
								
Date							ld_org_start_date								
								
u_String_Functions		lu_StringFunctions

ls_UpdateString = ''

	
ll_DeletedCount = tab_1.tabpage_Customer.dw_Customer.DeletedCount()
If ll_DeletedCount > 0 Then
	For ll_row = 1 to ll_DeletedCount
				
		as_UpdateString += 'C' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'customer_number', Delete!, False) + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'customer_type', Delete!, False) + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'vendor_id', Delete!, False) + '~t' + &
				String(tab_1.tabpage_Customer.dw_Customer.GetItemDate(ll_row, 'start_date', Delete!, False)) + '~t' + &
				String(tab_1.tabpage_Customer.dw_Customer.GetItemDate(ll_row, 'end_date', Delete!, False)) + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'include_exclude', Delete!, False) + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'rebate_rate', Delete!, False) + '~t' + &
				String(tab_1.tabpage_Customer.dw_Customer.GetItemDate(ll_row, 'start_date', Delete!, False)) + '~t' + &
				'D~r~n'
	Next
End IF


ll_row = tab_1.tabpage_Customer.dw_Customer.GetNextModified(ll_row, Primary!)
Do While ll_row > 0
	ls_vendor_id = tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'vendor_id')
	If (is_RebateType = 'Q') or (is_RebateType = 'P') Then
		ls_vendor_id = ''
	Else
		If lu_StringFunctions.nf_IsEmpty(ls_vendor_id) Then 
			Tab_1.SelectedTab = 4
			tab_1.tabpage_Customer.dw_Customer.SetColumn('vendor_id')
			tab_1.tabpage_Customer.dw_Customer.Setrow(ll_row)
			tab_1.tabpage_Customer.dw_Customer.Setfocus()
			tab_1.tabpage_Customer.dw_Customer.SelectText(1, 1000)
		
			SetMicroHelp('Vendor ID is a required field')
			Return False
		End If
	End If
	ll_row = tab_1.tabpage_Customer.dw_Customer.GetNextModified(ll_row, Primary!)
Loop 

ll_row = 0
Do
	ll_row = tab_1.tabpage_Customer.dw_Customer.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		ls_org_start_date = '0001-01-01'
		
		if IsValid(ids_customers) then
			ll_OriginalRowCount = ids_customers.RowCount()
			For ll_row2 = 1 to ll_OriginalRowCount
				if (ids_customers.GetItemString(ll_row2, 1) = tab_1.tabpage_customer.dw_customer.GetItemString(ll_row, 1))  AND &
				   (ids_customers.GetItemString(ll_row2, 2) = tab_1.tabpage_customer.dw_customer.GetItemString(ll_row, 2)) Then
					ls_org_start_date = ids_customers.GetItemString(ll_row2, 4)
					ld_org_start_date = Date(ls_org_start_date)
					ls_org_start_date = String(ld_org_start_date, 'YYYY-MM-DD')
				end if
			Next
		end if		
		
		If is_RebateType = 'Q' Then
			ls_charge_rate = '0'
		Else
			ls_charge_rate = String(tab_1.tabpage_Customer.dw_Customer.GetItemNumber(ll_row, 'rebate_rate')) 
		End If
				
		as_UpdateString += 'C' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'customer_number') + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'customer_type') + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'vendor_id') + '~t' + &
				String(tab_1.tabpage_Customer.dw_Customer.GetItemDate(ll_row, 'start_date'),'YYYY-MM-DD') + '~t' + &
				String(tab_1.tabpage_Customer.dw_Customer.GetItemDate(ll_row, 'end_date'),'YYYY-MM-DD') + '~t' + &
				tab_1.tabpage_Customer.dw_Customer.GetItemString(ll_row, 'include_exclude') + '~t' + &
				ls_charge_rate + '~t' + &
				ls_org_start_date + '~t' + &
				wf_GetUpdateInd(ll_row, tab_1.tabpage_Customer.dw_Customer) + '~r~n'
	End If
Loop While ll_row > 0
		

Return True

end function

public function boolean wf_defaultstartdates (date adt_date);Long			ll_count
Date            ldt_Original_Header_Start_Date

dwItemStatus	lis_temp

u_ProjectFunctions	lu_ProjectFunctions

ldt_Original_Header_Start_Date = dw_header_display.GetItemDate(1,'start_date')

For ll_count = 1 to tab_1.tabpage_detail.dw_detail.RowCount()
	lis_temp = tab_1.tabpage_detail.dw_detail.GetItemStatus(ll_count, 0, Primary!)
	If tab_1.tabpage_detail.dw_detail.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
		tab_1.tabpage_detail.dw_detail.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_detail.dw_detail.GetItemDate(ll_count, 'end_date') < adt_date Then
		tab_1.tabpage_detail.dw_detail.SetItem(ll_count, 'end_date', adt_date)
	End If
	If lis_temp = New! Then
		lu_projectfunctions.uf_changerowstatus(tab_1.tabpage_detail.dw_detail, ll_count, New!)
	End If
Next

For ll_count = 1 to tab_1.tabpage_customergroup.dw_customergroup.RowCount()
	If tab_1.tabpage_customergroup.dw_customergroup.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
		tab_1.tabpage_customergroup.dw_customergroup.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_customergroup.dw_customergroup.GetItemDate(ll_count, 'end_date') < adt_date Then
		tab_1.tabpage_customergroup.dw_customergroup.SetItem(ll_count, 'end_date', adt_date)
	End If
Next

For ll_count = 1 to tab_1.tabpage_customer.dw_customer.RowCount()
	If tab_1.tabpage_customer.dw_customer.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
		tab_1.tabpage_customer.dw_customer.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_customer.dw_customer.GetItemDate(ll_count, 'end_date') < adt_date Then
		tab_1.tabpage_customer.dw_customer.SetItem(ll_count, 'end_date', adt_date)
	End If
Next

If is_RebateOption = 'A' then
	For ll_count = 1 to tab_1.tabpage_division.dw_division.RowCount()
		If tab_1.tabpage_division.dw_division.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
			tab_1.tabpage_division.dw_division.SetItem(ll_count, 'start_date', adt_date)
		End If
		If tab_1.tabpage_division.dw_division.GetItemDate(ll_count, 'end_date') < adt_date Then
			tab_1.tabpage_division.dw_division.SetItem(ll_count, 'end_date', adt_date)
		End If
	Next
End If

If is_RebateOption = 'A' then
	For ll_count = 1 to tab_1.tabpage_productgroup.dw_productgroup.RowCount()
		If tab_1.tabpage_productgroup.dw_productgroup.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
			tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'start_date', adt_date)
		End If
		If tab_1.tabpage_productgroup.dw_productgroup.GetItemDate(ll_count, 'end_date') < adt_date Then
			tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'end_date', adt_date)
		End If
	Next
End If

If is_RebateOption = 'A' then
	For ll_count = 1 to tab_1.tabpage_product.dw_product.RowCount()
		If tab_1.tabpage_product.dw_product.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
			tab_1.tabpage_product.dw_product.SetItem(ll_count, 'start_date', adt_date)
		End If
		If tab_1.tabpage_product.dw_product.GetItemDate(ll_count, 'end_date') < adt_date Then
			tab_1.tabpage_product.dw_product.SetItem(ll_count, 'end_date', adt_date)
		End If
	Next
End If

For ll_count = 1 to tab_1.tabpage_location.dw_location.RowCount()
	If tab_1.tabpage_location.dw_location.GetItemDate(ll_count, 'start_date') = ldt_Original_Header_Start_Date Then
		tab_1.tabpage_location.dw_location.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_location.dw_location.GetItemDate(ll_count, 'end_date') < adt_date Then
		tab_1.tabpage_location.dw_location.SetItem(ll_count, 'end_date', adt_date)
	End If
Next



Return True
end function

public function boolean wf_defaultenddates (date adt_date);Long			ll_count

Date           ldt_Original_Header_End_Date

dwItemStatus	lis_temp

u_ProjectFunctions	lu_ProjectFunctions

ldt_Original_Header_End_Date = dw_header_display.GetItemDate(1,'end_date')

For ll_count = 1 to tab_1.tabpage_detail.dw_detail.RowCount()
	lis_temp = tab_1.tabpage_detail.dw_detail.GetItemStatus(ll_count, 0, Primary!)

	If tab_1.tabpage_detail.dw_detail.GetItemDate(ll_count, 'start_date') > adt_date Then
		tab_1.tabpage_detail.dw_detail.SetItem(ll_count, 'start_date', adt_date)
	End If
     If tab_1.tabpage_detail.dw_detail.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then		
		tab_1.tabpage_detail.dw_detail.SetItem(ll_count, 'end_date', adt_date)
	End If
	If lis_temp = New! Then
		lu_projectfunctions.uf_changerowstatus(tab_1.tabpage_detail.dw_detail, ll_count, New!)
	End If
Next

For ll_count = 1 to tab_1.tabpage_customergroup.dw_customergroup.RowCount()
	If tab_1.tabpage_customergroup.dw_customergroup.GetItemDate(ll_count, 'start_date') > adt_date Then
		tab_1.tabpage_customergroup.dw_customergroup.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_customergroup.dw_customergroup.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then
		tab_1.tabpage_customergroup.dw_customergroup.SetItem(ll_count, 'end_date', adt_date)
	End If
Next

For ll_count = 1 to tab_1.tabpage_customer.dw_customer.RowCount()
	If tab_1.tabpage_customer.dw_customer.GetItemDate(ll_count, 'start_date') > adt_date Then
		tab_1.tabpage_customer.dw_customer.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_customer.dw_customer.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then	
		tab_1.tabpage_customer.dw_customer.SetItem(ll_count, 'end_date', adt_date)
	End If
Next

If is_RebateOption = 'A' then
	For ll_count = 1 to tab_1.tabpage_division.dw_division.RowCount()
		If tab_1.tabpage_division.dw_division.GetItemDate(ll_count, 'start_date') > adt_date Then
			tab_1.tabpage_division.dw_division.SetItem(ll_count, 'start_date', adt_date)
		End If
		If tab_1.tabpage_division.dw_division.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then
			tab_1.tabpage_division.dw_division.SetItem(ll_count, 'end_date', adt_date)
		End If
	Next
End If

If is_RebateOption = 'A' then
	For ll_count = 1 to tab_1.tabpage_productgroup.dw_productgroup.RowCount()
		If tab_1.tabpage_productgroup.dw_productgroup.GetItemDate(ll_count, 'start_date') > adt_date Then
			tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'start_date', adt_date)
		End If
		If tab_1.tabpage_productgroup.dw_productgroup.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then
			tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'end_date', adt_date)
		End If
	Next
End If

If is_RebateOption = 'A' then
	For ll_count = 1 to tab_1.tabpage_product.dw_product.RowCount()
		If tab_1.tabpage_product.dw_product.GetItemDate(ll_count, 'start_date') > adt_date Then
			tab_1.tabpage_product.dw_product.SetItem(ll_count, 'start_date', adt_date)
		End If
		If tab_1.tabpage_product.dw_product.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then	
			tab_1.tabpage_product.dw_product.SetItem(ll_count, 'end_date', adt_date)
		End If
	Next
End If

For ll_count = 1 to tab_1.tabpage_location.dw_location.RowCount()
	If tab_1.tabpage_location.dw_location.GetItemDate(ll_count, 'start_date') > adt_date Then
		tab_1.tabpage_location.dw_location.SetItem(ll_count, 'start_date', adt_date)
	End If
	If tab_1.tabpage_location.dw_location.GetItemDate(ll_count, 'end_date') = ldt_Original_Header_End_Date Then
		tab_1.tabpage_location.dw_location.SetItem(ll_count, 'end_date', adt_date)
	End If
Next

Return True
end function

public function boolean wf_builddivisionstring (string as_type, ref string as_updatestring);// NOT USED

DataStore			lds_datastore
DataWindow		ldw_datawindow
Long					ll_NewRowCount, &
						ll_OriginalRowCount, &
						ll_NewCount, &
						ll_OriginalCount, &
						ll_v_rebate_rate
String				     ls_DivisionCode, &
						ls_OriginalCode, &
						ls_rebate_ind, &	
						ls_org_rbt_ind, &
						ls_exclude_ind, &
						ls_rebate_rate, &
						ls_start_date, &
						ls_end_date, &
      					ls_v_start_date, &
						ls_v_end_date, &
						ls_credit_type

Choose Case as_type
	Case 'V'
		lds_datastore = ids_divisions
		ldw_datawindow = tab_1.tabpage_division.dw_division
End Choose

ll_NewRowCount = ldw_datawindow.RowCount()

// set VMR defaults for as_UpdateString - VMR has no rebate_rate, start_date or end_date
ll_v_rebate_rate = 0
ls_v_start_date = '0001-01-01'
ls_v_end_date = '2999-12-31'

If Not IsValid(lds_datastore) Then
// All New divisions
	For ll_NewCount = 1 to ll_NewRowCount
		ls_credit_type = ldw_datawindow.GetItemString(ll_NewCount, 3) 
		
		If is_RebateOption = 'A' Then	
			If is_RebateType = 'Q' Then
				ls_rebate_rate = String(ll_v_rebate_rate)
				ls_credit_type = ''
			Else
				ls_rebate_rate = String(ldw_datawindow.GetItemNumber(ll_NewCount, 'rebate_rate'))
			End If			
			
			as_UpdateString += as_type + ldw_datawindow.GetItemString(ll_NewCount, 1) + '~t' + &
					ls_credit_type + '~t' + &
					ldw_datawindow.GetItemString(ll_NewCount, 4) + '~t' + &
					ls_rebate_rate + '~t' + &
					String(ldw_datawindow.GetItemDate(ll_NewCount, 'start_date'), 'YYYY-MM-DD') + '~t' + &
					String(ldw_datawindow.GetItemDate(ll_NewCount, 'end_date'), 'YYYY-MM-DD') + '~t' + &
					 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ldw_datawindow.GetItemString(ll_NewCount, 1) + '~t' + &
					ls_credit_type + '~t' + &
					ldw_datawindow.GetItemString(ll_NewCount, 4) + '~t' + &
					String(ll_v_rebate_rate) + '~t' + &
					ls_v_start_date + '~t' + &
					ls_v_end_date + '~t' + &
					 'I' + '~r~n'		
		End If
	Next	
	Return True
End If

ll_OriginalRowCount = lds_datastore.RowCount()

// No divisions
If Not ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	Return True
End If

// All New divisions
If ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	For ll_NewCount = 1 to ll_NewRowCount
		ls_credit_type = ldw_datawindow.GetItemString(ll_NewCount, 3)
		
		If is_RebateOption = 'A' Then
			If is_RebateType = 'Q' Then
				ls_rebate_rate = String(ll_v_rebate_rate)
				ls_credit_type = ''
			Else
				ls_rebate_rate = String(ldw_datawindow.GetItemNumber(ll_NewCount, 'rebate_rate'))
			End If						
		
			as_UpdateString += as_type + ldw_datawindow.GetItemString(ll_NewCount, 1) + '~t' + &
					ls_credit_type + '~t' + &
					ldw_datawindow.GetItemString(ll_NewCount, 4) + '~t' + &
					ls_rebate_rate + '~t' + &
					String(ldw_datawindow.GetItemDate(ll_NewCount, 'start_date'), 'YYYY-MM-DD') + '~t' + &
					String(ldw_datawindow.GetItemDate(ll_NewCount, 'end_date'), 'YYYY-MM-DD') + '~t' + &
					 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ldw_datawindow.GetItemString(ll_NewCount, 1) + '~t' + &
					ls_credit_type + '~t' + &
					ldw_datawindow.GetItemString(ll_NewCount, 4) + '~t' + &
					String(ll_v_rebate_rate) + '~t' + &
					ls_v_start_date + '~t' + &
					ls_v_end_date + '~t' + &
					 'I' + '~r~n'		
		End If					
	Next	
	Return True
End If

// Delete All Divisions
If (Not ll_NewRowCount > 0 And ll_OriginalRowCount > 0) or ib_DeleteHeader Then
	For ll_OriginalCount = 1 to ll_OriginalRowCount
		as_UpdateString += as_type + lds_datastore.GetItemString(ll_OriginalCount, &
				1) + '~t' + lds_datastore.GetItemString(ll_OriginalCount, 3) + '~t' + &
				ls_rebate_rate + '~t' + '~t' + '~t' + 'D' + '~r~n'
	Next	
	Return True
End If


ll_OriginalCount = 1

For ll_NewCount = 1 to ll_NewRowCount
	ls_DivisionCode = ldw_datawindow.GetItemString(ll_NewCount, 1)
	ls_rebate_ind = ldw_datawindow.GetItemString(ll_NewCount, 3)
	ls_exclude_ind = ldw_datawindow.GetItemString(ll_NewCount, 4)
	If is_RebateOption = 'A' Then
		
		If is_rebateType = 'Q' Then
				ls_rebate_rate = String(ll_v_rebate_rate)
				ls_rebate_ind = ''
			Else
				ls_rebate_rate = String(ldw_datawindow.GetItemNumber(ll_NewCount, 'rebate_rate'))
			End If
			
		ls_start_date = String(ldw_datawindow.GetItemDate(ll_NewCount, 'start_date'), 'YYYY-MM-DD')
		ls_end_date = String(ldw_datawindow.GetItemDate(ll_NewCount, 'end_date'), 'YYYY-MM-DD')
	End If	
	
// Reached the end of the Original Buffer Insert the rest.
	If ll_OriginalCount > ll_OriginalRowCount Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_DivisionCode + '~t' + ls_rebate_ind + '~t' + ls_exclude_ind + '~t' + ls_rebate_rate  + '~t' + ls_start_date + '~t' + ls_end_date + '~t' + 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ls_DivisionCode + '~t' + ls_rebate_ind + '~t' + ls_exclude_ind + '~t' + string(ll_v_rebate_rate)  + '~t' + ls_v_start_date + '~t' + ls_v_end_date + '~t' + 'I' + '~r~n'			
		End If
		Continue
	End If
		
	ls_OriginalCode = lds_datastore.GetItemString(ll_OriginalCount, 'division_code')
	ls_org_rbt_ind = lds_datastore.GetItemString(ll_OriginalCount, 3) 
// They equal so don't send it to be updated.
	If ls_DivisionCode = ls_OriginalCode then
		ll_OriginalCount ++
		Continue
	End If
// This is a new division send it to be inserted.
	If ls_DivisionCode < ls_OriginalCode Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_DivisionCode + '~t' + ls_rebate_ind + '~t' + ls_exclude_ind + '~t' + ls_rebate_rate  + '~t' + ls_start_date + '~t' + ls_end_date  + '~t' + 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ls_DivisionCode + '~t' + ls_rebate_ind + '~t' + ls_exclude_ind + '~t' + string(ll_v_rebate_rate)  + '~t' + ls_v_start_date + '~t' + ls_v_end_date + '~t' + 'I' + '~r~n'			
		End If
		Continue
	End If
// The original was deleted send it to be deleted.	
	If ls_DivisionCode > ls_OriginalCode Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_OriginalCode  + '~t' + ls_org_rbt_ind + '~t' + ls_exclude_ind + '~t' + ls_rebate_rate  + '~t' + ls_start_date + '~t' + ls_end_date  + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type + ls_OriginalCode  + '~t' + ls_org_rbt_ind + '~t' + ls_exclude_ind + '~t' + String(ll_v_rebate_rate)  + '~t' + ls_v_start_date + '~t' + ls_v_end_date  + '~t' + 'D' + '~r~n'
		End If
		ll_NewCount --
		ll_OriginalCount ++
		Continue
	End IF
Next

//Delete the rest of the Original Buffer
If ll_OriginalCount <= ll_OriginalRowCount Then
	For ll_OriginalCount = ll_OriginalCount to ll_OriginalRowCount
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + lds_datastore.GetItemString(ll_OriginalCount, &
					1)  + '~t' + ls_org_rbt_ind + '~t' + ls_exclude_ind + '~t' + ls_rebate_rate  + '~t' + ls_start_date + '~t' + ls_end_date  + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type + lds_datastore.GetItemString(ll_OriginalCount, &
					1)  + '~t' + ls_org_rbt_ind + '~t' + ls_exclude_ind + '~t' + String(ls_rebate_rate)  + '~t' + ls_v_start_date + '~t' + ls_v_end_date  + '~t' + 'D' + '~r~n'		
		End If
	Next
End IF
		
Return True
end function

public function boolean wf_check_volumes (string as_volumes, integer as_row, long al_volume);long							ll_row_count, ll_count, ll_count2, ll_volume
u_projectfunctions		lu_projectfunctions
datawindow					ldw_datawindow

ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount() - 1
if ll_row_count > 0 then	
	choose case as_volumes
		case "modified"		
			for ll_count = 1 to ll_row_count
				if as_row = ll_count then
					continue
				else
					if tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count, "minimum_target_volume") <= al_volume and &
						tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count, "maximum_target_volume") >= al_volume then
						return False
					end if
				end if
			next
			if as_row = ll_row_count then
				tab_1.tabpage_header.dw_header.AcceptText()
				tab_1.tabpage_detail.dw_detail.SetItem(as_row + 1, 'minimum_target_volume', &
						tab_1.tabpage_detail.dw_detail.GetItemnumber(as_row,'maximum_target_volume') + 1)
				tab_1.tabpage_detail.dw_detail.SetItem(as_row + 1, 'maximum_target_volume', &
						tab_1.tabpage_detail.dw_detail.GetItemnumber(as_row,'maximum_target_volume') + 10000)
				ldw_datawindow = tab_1.tabpage_detail.dw_detail
				lu_projectfunctions.uf_changerowstatus(ldw_datawindow, as_row + 1, New!)
			end if
		case "all"
			for ll_count = 1 to ll_row_count
				ll_volume = tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count, "minimum_target_volume")
				for ll_count2 = 1 to ll_row_count
					if ll_count = ll_count2 then
						continue
					else
						if tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count2, "minimum_target_volume") <= ll_volume and &
							tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count2, "maximum_target_volume") >= ll_volume then
							Tab_1.SelectedTab = 2
							Tab_1.tabpage_detail.dw_detail.Setfocus()
							Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_count2)
							Tab_1.tabpage_detail.dw_detail.SetColumn('minimum_target_volume')
							Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
							SetMicroHelp('Volumes cannot overlap')
							return False
						end if
					end if
				next				
				ll_volume = tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count, "maximum_target_volume")
				for ll_count2 = 1 to ll_row_count
					if ll_count = ll_count2 then
						continue
					else
						if tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count2, "minimum_target_volume") <= ll_volume and &
							tab_1.tabpage_detail.dw_detail.getitemnumber(ll_count2, "maximum_target_volume") >= ll_volume then
							Tab_1.SelectedTab = 2
							Tab_1.tabpage_detail.dw_detail.Setfocus()
							Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_count2)
							Tab_1.tabpage_detail.dw_detail.SetColumn('maximum_target_volume')
							Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
							SetMicroHelp('Volumes cannot overlap')
							return False
						end if
					end if
				next
			next			
	end choose		
end if
return true
end function

public function boolean wf_check_dates (integer as_row, date as_date, string as_string);long							ll_row_count, ll_count, ll_count2
date							ldt_start, ldt_end
u_projectfunctions		lu_projectfunctions
datawindow					ldw_datawindow

RelativeDate(tab_1.tabpage_header.dw_header.GetItemDate(1,'start_date'),1)

ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount() - 1
if ll_row_count > 0 then	
	choose case as_string
		case "modified"		
			for ll_count = 1 to ll_row_count
				if as_row = ll_count then
					continue
				else
					if tab_1.tabpage_detail.dw_detail.getitemdate(ll_count, "start_date") <= as_date and &
						tab_1.tabpage_detail.dw_detail.getitemdate(ll_count, "end_date") >= as_date then
						return False
					end if
				end if
			next
			if as_row = ll_row_count then
				tab_1.tabpage_header.dw_header.AcceptText()
				tab_1.tabpage_detail.dw_detail.SetItem(as_row + 1, 'start_date', &
						RelativeDate(tab_1.tabpage_detail.dw_detail.GetItemDate(as_row,'end_date'),1))
				tab_1.tabpage_detail.dw_detail.SetItem(as_row + 1, 'end_date', &
						RelativeDate(tab_1.tabpage_detail.dw_detail.GetItemDate(as_row,'end_date'),2))
				ldw_datawindow = tab_1.tabpage_detail.dw_detail
				lu_projectfunctions.uf_changerowstatus(ldw_datawindow, as_row + 1, New!)
			end if
		case "all"
			for ll_count = 1 to ll_row_count
				ldt_start = tab_1.tabpage_detail.dw_detail.getitemdate(ll_count, "start_date")
				for ll_count2 = 1 to ll_row_count
					if ll_count = ll_count2 then
						continue
					else
						if tab_1.tabpage_detail.dw_detail.getitemdate(ll_count2, "start_date") <= ldt_start and &
							tab_1.tabpage_detail.dw_detail.getitemdate(ll_count2, "end_date") >= ldt_start then
							Tab_1.SelectedTab = 2
							Tab_1.tabpage_detail.dw_detail.Setfocus()
							Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_count2)
							Tab_1.tabpage_detail.dw_detail.SetColumn('start_date')
							Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
							SetMicroHelp('Date periods cannot overlap')
							return False
						end if
					end if
				next				
				ldt_end = tab_1.tabpage_detail.dw_detail.getitemdate(ll_count, "end_date")
				for ll_count2 = 1 to ll_row_count
					if ll_count = ll_count2 then
						continue
					else
						if tab_1.tabpage_detail.dw_detail.getitemdate(ll_count2, "start_date") <= ldt_end and &
							tab_1.tabpage_detail.dw_detail.getitemdate(ll_count2, "end_date") >= ldt_end then
							Tab_1.SelectedTab = 2
							Tab_1.tabpage_detail.dw_detail.Setfocus()
							Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_count2)
							Tab_1.tabpage_detail.dw_detail.SetColumn('end_date')
							Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
							SetMicroHelp('Date periods cannot overlap')
							return False
						end if
					end if
				next
			next			
	end choose		
end if
return true
end function

public function string wf_getupdateind (long al_row, datawindow adw_datawindow);dwItemStatus	l_status

l_status = adw_datawindow.GetItemStatus(al_row, 0, Primary!)

Choose Case adw_datawindow.GetItemStatus(al_row, 0, Primary!)
	Case NewModified!
		Return 'I'
	Case DataModified!
		Return 'U'
	Case Else
		Return ' '
End Choose
end function

public subroutine wf_setfocus ();Choose Case tab_1.SelectedTab
	Case 1
		tab_1.tabpage_header.dw_header.SetFocus()
	Case 2
		tab_1.tabpage_detail.dw_detail.SetFocus()
	Case 4
		tab_1.tabpage_customer.dw_customer.SetFocus()
End Choose

end subroutine

public function boolean wf_fillcredmeth ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Initialize()
iu_ErrorContext.uf_Initialize()

ls_TutlType = 'REBCDMET'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('credit_method', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

dw_header_display.GetChild('credit_method', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_filldetpayperiod ();Long									ll_temp
String								ls_tutltype, ls_TransferTypeString
DataWindowChild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'REBPYPER'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

is_dddw_payperiod_string = ls_TransferTypeString
tab_1.tabpage_detail.dw_detail.GetChild('payout_period', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

public function boolean wf_filldetrebcrtyp ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'REBCRTYP'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_division.dw_division.GetChild('tonnage_rebate_credit', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

tab_1.tabpage_productgroup.dw_productgroup.GetChild('tonnage_rebate_credit', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

tab_1.tabpage_product.dw_product.GetChild('tonnage_rebate_credit', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_filldetvolperiod ();Long									ll_temp
String								ls_tutltype, ls_TransferTypeString
DataWindowChild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then		
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'REBVLPER'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)
iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

is_dddw_volperiod_string = ls_TransferTypeString
tab_1.tabpage_detail.dw_detail.GetChild('volume_period', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

public function boolean wf_fillrebatedropdown ();Boolean								lb_temp

Long									ll_temp

String								ls_appname, &
										ls_windowname, &
										ls_RebateListString
							
DataWindowChild					ldwc_RebateCode
							
u_AbstractParameterStack		lu_ParameterStack	

u_rebateDataAccess				lu_rebateDataAccess

iu_ErrorContext.uf_Initialize()

If Not iu_classfactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_errorcontext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_classfactory.uf_GetObject( "u_RebateDataAccess", lu_rebateDataAccess, iu_errorcontext ) Then
	If Not iu_errorcontext.uf_IsSuccessful( ) Then
		iu_notification.uf_Display(iu_errorcontext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_appname = GetApplication().appname
ls_windowname = 'w_rebatemain'
ls_RebateListString = ''

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_classfactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_errorcontext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_RebateListString)

lu_RebateDataAccess.uf_RetrieveList( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_RebateListString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_errorcontext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_classfactory)

If Not iu_errorcontext.uf_IsSuccessful( ) Then
	iu_notification.uf_Display(iu_errorcontext)
	Return False
End If

is_dddw_rebate_list_string = ls_RebateListString
ids_RebateList.Reset()
ids_RebateList.ImportString(ls_RebateListString)
ids_RebateList.Sort()

Return True

end function

public function boolean wf_updatedivisions (string as_type, ref string as_updatestring);Boolean				lb_Return
Long					ll_NewRowCount, &
						ll_OriginalRowCount, &
						ll_NewCount, &
						ll_OriginalCount, &
						ll_row, &
						ll_row2, &
						ll_v_rebate_rate,&
						ll_org_row
String				ls_NewCode, &
						ls_rebate_ind, &
						ls_OriginalCode, &
						ls_org_rbt_ind, &
						ls_org_exclude_ind, &
						ls_org_rebate_rate, &
						ls_org_start_date, &
						ls_org_end_date, &
						ls_new_exclude_ind, &
						ls_rebate_rate, &
						ls_start_date, &
						ls_end_date, &
						ls_v_start_date, &
						ls_v_end_date, &
						ls_ORIGINAL_start_date, &
						ls_ORIGINAL_ds_start_date

Date					ld_org_start_date, &
						ld_ORIGINAL_start_date, &
						ld_ORIGINAL_ds_start_date

dwItemStatus			lis_temp
DataWindow				ldw_datawindow
u_projectfunctions	lu_projectfunctions

// set VMR defaults for as_UpdateString - VMR has no rebate_rate, start_date or end_date
ll_v_rebate_rate = 0
ls_v_start_date = '0001-01-01'
ls_v_end_date = '2999-12-31'

lb_Return = False

ll_NewRowCount = tab_1.tabpage_division.dw_division.RowCount()

If Not IsValid(ids_divisions) Then
	If ll_NewRowCount > 0 Then
		// All New divisions
		For ll_NewCount = 1 to ll_NewRowCount
			ls_NewCode = tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 1)
			ls_rebate_ind = tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 3)
			ls_new_exclude_ind = tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 4)
			If is_RebateOption = 'A' Then
				If is_RebateType = 'Q' Then
					ls_rebate_rate = String(ll_v_rebate_rate)
					ls_rebate_ind = ''
				Else
					ls_rebate_rate = String(tab_1.tabpage_division.dw_division.GetItemNumber(ll_NewCount, 5))
				End If
				ls_start_date = String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 6), 'YYYY-MM-DD')
				ls_end_date = String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 7), 'YYYY-MM-DD')
				as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' + ls_start_date+ '~t' + 'I' + '~r~n'
			Else
				as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + String(ll_v_rebate_rate) + '~t' + ls_v_start_date + '~t' + ls_v_end_date + '~t' + ls_v_start_date + '~t' + 'I' + '~r~n'
			End If
		Next	
		Return True
	Else
		Return False
	End If
End If

ll_OriginalRowCount = ids_divisions.RowCount()

// No divisions
If Not ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	Return False
End If

// All New divisions
If ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	For ll_NewCount = 1 to ll_NewRowCount
		If is_RebateOption = 'A' Then
			If is_RebateType = 'Q' Then
				ls_rebate_rate = String(ll_v_rebate_rate)
				ls_rebate_ind = ''
			Else
				ls_rebate_rate = String(tab_1.tabpage_division.dw_division.GetItemNumber(ll_NewCount, 5))
				ls_rebate_ind = tab_1.tabpage_division.dw_division.GetItemString( ll_NewCount, 3)
			End If
			as_UpdateString += as_type + tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 1) + '~t' + &
										ls_rebate_ind + '~t' + &
   									    tab_1.tabpage_division.dw_division.GetItemString( ll_NewCount, 4) + '~t' + &
										ls_rebate_rate + '~t' + &
										String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 6), 'YYYY-MM-DD') + '~t' + &
										String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 7), 'YYYY-MM-DD') + '~t' + &
										ids_divisions.GetItemString(ll_NewCount, 6) + '~t' + &
										'I' + '~r~n'
		Else
			as_UpdateString += as_type + tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 1) + '~t' + &
		      							tab_1.tabpage_division.dw_division.GetItemString( ll_NewCount, 3) + '~t' + &
   									    tab_1.tabpage_division.dw_division.GetItemString( ll_NewCount, 4) + '~t' + &
										String(ll_v_rebate_rate) + '~t' + &
										ls_v_start_date + '~t' + &
										ls_v_end_date + '~t' + &
										ls_v_start_date + '~t' + &
										'I' + '~r~n'
		End If									
	Next	
	Return True
End If

string ls_debug
ls_debug = ids_divisions.describe("DataWindow.Data")

// Delete All divisions
If Not ll_NewRowCount > 0 And ll_OriginalRowCount > 0 Then
	For ll_OriginalCount = 1 to ll_OriginalRowCount
		ls_OriginalCode = ids_divisions.GetItemString(ll_OriginalCount, 1)
		If is_RebateOption = 'A' Then
//			ls_start_date =  String(tab_1.tabpage_division.dw_division.GetItemDate(ll_OriginalCount, 6), 'YYYY-MM-DD')
			
			//use new ORIGINAL datastore start date column instead - column 8
			ls_ORIGINAL_ds_start_date =  ids_divisions.GetItemString(ll_OriginalCount, 8)
			ld_ORIGINAL_ds_start_date = Date(ls_ORIGINAL_ds_start_date)
			ls_ORIGINAL_ds_start_date = String(ld_ORIGINAL_ds_start_date, 'YYYY-MM-DD')		
			
			ls_org_start_date =  String(ids_divisions.GetItemString(ll_OriginalCount, 6))
			ld_org_start_date = Date(ls_org_start_date)
			ls_org_start_date = String(ld_org_start_date, 'YYYY-MM-DD')
			ls_start_date = ls_org_start_date 
			as_UpdateString += as_type + ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_start_date + '~t' + '~t' + ls_ORIGINAL_ds_start_date + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type + ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_v_start_date + '~t' + '~t' +  ls_v_start_date + '~t' + 'D' + '~r~n'
		End If
	Next
	Return True
End If

ll_OriginalCount = 1


// get a row from the datawindow and compare to the same position in the datastore 
For ll_NewCount = 1 to ll_NewRowCount

	ls_NewCode = tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 1)
	ls_rebate_ind = tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 3)
	ls_new_exclude_ind = tab_1.tabpage_division.dw_division.GetItemString(ll_NewCount, 4)
	
	If is_RebateOption = 'A' Then
		If is_RebateType = 'Q' Then
			ls_rebate_rate = String(ll_v_rebate_rate)
			ls_rebate_ind = ''
		Else
			ls_rebate_rate = String(tab_1.tabpage_division.dw_division.GetItemNumber(ll_NewCount, 5))
		End If			
		ls_start_date = String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 6), 'YYYY-MM-DD')
		ls_end_date = String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 7), 'YYYY-MM-DD')
		//use new original start date column instead - column 8 
		ls_ORIGINAL_start_date = String(tab_1.tabpage_division.dw_division.GetItemDate(ll_NewCount, 8), 'YYYY-MM-DD')
	End If

// Reached the end of the Original Buffer Insert the rest.
	If ll_OriginalCount > ll_OriginalRowCount Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' + ls_start_date + '~t' + 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + String(ll_v_rebate_rate) + '~t' + ls_v_start_date + '~t' + ls_v_end_date + '~t' + ls_v_start_date + '~t' + 'I' + '~r~n'
		End If
		lb_Return = True
		Continue
	End If
		
	ls_OriginalCode = ids_divisions.GetItemString(ll_OriginalCount, 1)
	ls_org_rbt_ind = ids_divisions.GetItemString(ll_OriginalCount, 3)
	ls_org_exclude_ind = ids_divisions.GetItemString(ll_OriginalCount, 4)
	If is_RebateOption = 'A' Then
		If is_RebateType = 'Q' Then
			ls_org_rebate_rate = String(ll_v_rebate_rate)
			ls_org_rbt_ind = ''
		Else
			ls_org_rebate_rate = ids_divisions.GetItemString(ll_OriginalCount, 5)
		End If
		ls_org_start_date = ids_divisions.GetItemString(ll_OriginalCount, 6)
		ld_org_start_date = Date(ls_org_start_date)
		ls_org_start_date = String(ld_org_start_date, 'YYYY-MM-DD')
		ls_org_end_date = ids_divisions.GetItemString(ll_OriginalCount, 7)
		
		ls_ORIGINAL_ds_start_date = ids_divisions.GetItemString(ll_OriginalCount, 8)
		ld_ORIGINAL_ds_start_date = Date(ls_ORIGINAL_ds_start_date)
		ls_ORIGINAL_ds_start_date = String(ld_ORIGINAL_ds_start_date, 'YYYY-MM-DD')
			
	End If

// They equal so don't send it to be updated.
	//If ls_NewCode = ls_OriginalCode then  
	If ((ls_NewCode = ls_OriginalCode and is_RebateOption <> 'A') &
	   OR  (ls_NewCode = ls_OriginalCode and is_RebateOption = 'A' and ls_ORIGINAL_start_date = ls_ORIGINAL_ds_start_date)) Then
		ll_OriginalCount ++
		Continue
	End If
	
//division codes are the same but start dates do not equal
If ls_NewCode = ls_OriginalCode and is_RebateOption = 'A' Then 	
      If ls_ORIGINAL_start_date < ls_ORIGINAL_ds_start_date Then
         //New division - Insert
		 as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' +  ls_start_date + '~t' + 'I' + '~r~n'		  
      Else
          //original deleted send it to be deleted 
	    //ls_ORIGINAL_start_date > ls_ORIGINAL_ds_start_date
	     as_UpdateString += as_type +  ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_org_start_date + '~t' + '~t' +  ls_ORIGINAL_ds_start_date + '~t' + 'D' + '~r~n'  
         ll_NewCount --
	     ll_OriginalCount ++
     End If
     lb_Return = True
    Continue
End If
	
// This is a new division send it to be inserted.
	If ls_NewCode < ls_OriginalCode Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' +  ls_start_date + '~t' + 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_new_exclude_ind + '~t' + String(ll_v_rebate_rate) + '~t' + ls_v_start_date + '~t' + ls_v_end_date + '~t' + ls_v_start_date + '~t' +  'I' + '~r~n'
		End If
		lb_Return = True
		Continue
	End If
// The original was deleted send it to be deleted.	
	If ls_NewCode > ls_OriginalCode Then
		If is_RebateOption = 'A' Then
			//as_UpdateString += as_type +  ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_start_date + '~t' + '~t' +  ls_start_date + '~t' + 'D' + '~r~n'
			//as_UpdateString += as_type +  ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_org_start_date + '~t' + '~t' +  ls_org_start_date + '~t' + 'D' + '~r~n'
			as_UpdateString += as_type +  ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_org_start_date + '~t' + '~t' +  ls_ORIGINAL_ds_start_date + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type +  ls_OriginalCode + '~t' + "X" + '~t' + "X" + '~t' + '~t' +  ls_v_start_date + '~t' + '~t' + ls_v_start_date + '~t' + 'D' + '~r~n'			
		End If
		lb_Return = True
		ll_NewCount --
		ll_OriginalCount ++
		Continue
	End IF
Next




//Delete the rest of the Original Buffer
If ll_OriginalCount <= ll_OriginalRowCount Then
	For ll_OriginalCount = ll_OriginalCount to ll_OriginalRowCount
		If is_RebateOption = 'A' Then
			//as_UpdateString += as_type + ids_divisions.GetItemString(ll_OriginalCount, 1) + &
			//	'~t'+ "X" + '~t' + 'N' + "X" + '~t' + '~t' + ids_divisions.GetItemString(ll_OriginalCount, 6) + '~t' + '~t' + ids_divisions.GetItemString(ll_OriginalCount, 6) + '~t' + 'D' + '~r~n'		
			as_UpdateString += as_type + ids_divisions.GetItemString(ll_OriginalCount, 1) + &
				'~t'+ "X" + '~t' + 'N' + "X" + '~t' + '~t' + ids_divisions.GetItemString(ll_OriginalCount, 6) + '~t' + '~t' + ids_divisions.GetItemString(ll_OriginalCount, 8) + '~t' + 'D' + '~r~n'					
		Else
			as_UpdateString += as_type + ids_divisions.GetItemString(ll_OriginalCount, 1) + &
				'~t'+ "X" + '~t' + 'N' + "X" + '~t' + '~t' + String(ls_v_start_date) +  '~t' + '~t' + String(ls_v_start_date) + '~t' + 'D' + '~r~n'							
		End If
	Next
	lb_Return = True
End IF

// update division
ll_OriginalCount = ids_divisions.RowCount()
ll_row = tab_1.tabpage_division.dw_division.GetNextModified(0, Primary!)
Do While ll_row > 0
                If is_RebateOption = 'A' Then
                                ls_org_start_date = '0001-01-01'	
//                                            if division found in original datastore, use the start date from the original datastore, otherwise set to '0001-01-01'                       
                                if IsValid(ids_divisions) then
                                                ll_OriginalRowCount = ids_divisions.RowCount()
                                                For ll_row2 = 1 to ll_OriginalRowCount
														//add check for column 8 original start date
														 ls_ORIGINAL_ds_start_date = ids_divisions.GetItemString(ll_row2, 8)
 													     ld_ORIGINAL_ds_start_date = Date(ls_ORIGINAL_ds_start_date)
                                                                ls_ORIGINAL_ds_start_date = String(ld_ORIGINAL_ds_start_date, 'YYYY-MM-DD')
																					 
                                                                If ids_divisions.GetItemString(ll_row2, 1) = tab_1.tabpage_division.dw_division.GetItemString(ll_row, 1) AND & 
															ls_ORIGINAL_ds_start_date = String(tab_1.tabpage_division.dw_division.GetItemDate(ll_row, 8), 'YYYY-MM-DD')	Then
                                                                                //ls_org_start_date = ids_divisions.GetItemString(ll_row2, 6)
                                                                                //ld_org_start_date = Date(ls_org_start_date)
                                                                                //ls_org_start_date = String(ld_org_start_date, 'YYYY-MM-DD')
		                                                                       ls_org_start_date = ls_ORIGINAL_ds_start_date
                                                                End If
                                                Next
                                end if
										  
							if is_RebateType = 'Q' Then
								ls_rebate_rate = String(ll_v_rebate_rate)
								ls_rebate_ind = ''
							Else
								ls_rebate_rate = String(tab_1.tabpage_division.dw_division.GetItemNumber(ll_row, 5)) 
								ls_rebate_ind = tab_1.tabpage_division.dw_division.GetItemString(ll_row, 3)
							End If
                                
                                as_UpdateString += as_type + &
                                                                tab_1.tabpage_division.dw_division.GetItemString(ll_row, 1)  + '~t' + &
                                                                ls_rebate_ind  + '~t' + &
                                                                tab_1.tabpage_division.dw_division.GetItemString(ll_row, 4)  + '~t' + &
                                                                ls_rebate_rate + '~t' + &
                                                                String(tab_1.tabpage_division.dw_division.GetItemDate(ll_row, 6), 'YYYY-MM-DD') + '~t' + &
                                                                String(tab_1.tabpage_division.dw_division.GetItemDate(ll_row, 7), 'YYYY-MM-DD') + '~t' + &
                                                                ls_org_start_date + '~t' + &
                                                                'U' + '~r~n'
                Else
                                as_UpdateString += as_type + &
                                                                tab_1.tabpage_division.dw_division.GetItemString(ll_row, 1)  + '~t' + &
                                                                tab_1.tabpage_division.dw_division.GetItemString(ll_row, 3)  + '~t' + &
                                                                tab_1.tabpage_division.dw_division.GetItemString(ll_row, 4)  + '~t' + &
                                                                String(ll_v_rebate_rate) + '~t' + &
                                                                ls_v_start_date + '~t' + &
                                                                ls_v_end_date + '~t' + &
                                                                ls_v_start_date + '~t' + &
                                                                'U' + '~r~n'                          
                End If
                                                                
                ll_row = tab_1.tabpage_division.dw_division.GetNextModified(ll_row, Primary!)
                lb_Return = True
Loop


Return lb_Return

end function

public function boolean wf_fillpricingind ();Long									ll_temp
String								ls_tutltype, ls_TransferTypeString
DataWindowChild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'PRICEIND'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)
iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

is_PricingString = ls_TransferTypeString

Return True

end function

public subroutine wf_get_selected_pricinginds (datawindow adw_datawindow);long				ll_count, ll_length
string			ls_ind, &
					ls_type, &
					ls_pos

is_selected_ind_array = ""
ls_ind = adw_datawindow.getitemstring(il_selected_row, "pricing_ind")
if isnull(ls_ind) then
	ls_ind = Space(il_selected_ind_max)
	ll_length = 0
else
	ll_length = len(ls_ind)
	ls_ind = ls_ind + Space(il_selected_ind_max - ll_length)
end if
for ll_count = 1 to il_selected_ind_max
	ls_type = Mid(ls_ind, ll_count, 1)
	if isnull(ls_type) then
		is_selected_ind_array += " " + "~r~n"
	else
		is_selected_ind_array += ls_type + "~r~n"
	end if
next
Return
end subroutine

public subroutine wf_set_selected_pricinginds ();
long				ll_count, ll_length, ll_rtn
string			     ls_ind, &
					ls_type, &
					ls_pos
					
ls_ind = is_selected_ind_array
ll_length = len(ls_ind)
if isnull(ls_ind) then
	ls_ind = Space(il_selected_ind_max)
	ll_length = 0
else
	for ll_count = 1 to ll_length STEP 3
		ls_type += Mid(ls_ind, ll_count, 1)
	next
	ll_length = len(ls_type)
	ls_type = ls_type + Space(il_selected_ind_max - ll_length)
end if	
	
CHOOSE CASE tab_1.SelectedTab
	CASE 2
		ll_rtn = tab_1.tabpage_detail.dw_detail.setitem(il_selected_row, "pricing_ind", ls_type)
	CASE 6
		ll_rtn = tab_1.tabpage_productgroup.dw_productgroup.setitem(il_selected_row, "pricing_ind", ls_type)
	CASE 7
		ll_rtn = tab_1.tabpage_product.dw_product.setitem(il_selected_row, "pricing_ind", ls_type)
END CHOOSE
end subroutine

public subroutine wf_saveproduct_pricinginds (string as_selected_tab);long				ll_count
string			     ls_ind, &
					ls_incl_excl, &
					ls_seq_no

is_saved_pricing_array = ""

If as_selected_tab = "Product" Then
	for ll_count = 1 to tab_1.tabpage_product.dw_product.RowCount()
		ls_seq_no = String(tab_1.tabpage_product.dw_product.getitemnumber(ll_count, "seq_no"))
		ls_incl_excl = tab_1.tabpage_product.dw_product.getitemstring(ll_count, "pricing_incl_excl")
		ls_ind = tab_1.tabpage_product.dw_product.getitemstring(ll_count, "pricing_ind")
		is_saved_pricing_array += ls_seq_no + '~t' + ls_incl_excl + '~t' + ls_ind + "~r~n"
	next
Else
	If as_selected_tab = "ProductGroup" Then
		for ll_count = 1 to tab_1.tabpage_productgroup.dw_productgroup.RowCount()
			ls_seq_no = String(tab_1.tabpage_productgroup.dw_productgroup.getitemnumber(ll_count, "seq_no"))
			ls_incl_excl = tab_1.tabpage_productgroup.dw_productgroup.getitemstring(ll_count, "pricing_incl_excl")
			ls_ind = tab_1.tabpage_productgroup.dw_productgroup.getitemstring(ll_count, "pricing_ind")
			is_saved_pricing_array += ls_seq_no + '~t' + ls_incl_excl + '~t' + ls_ind + "~r~n"
		next
	End If
End If
Return
end subroutine

public subroutine wf_setproduct_pricinginds (string as_selected_tab);String 					ls_incl_excl, &
							ls_pricing_ind, &
							ls_find
u_string_functions	lu_string_function
DataStore				lds_saved_pricinginds
long						ll_rowcount, ll_count, ll_row, ll_rowcnt, ll_seq_no


if is_saved_pricing_array = "" then Return

lds_saved_pricinginds = Create DataStore
lds_saved_pricinginds.DataObject = 'd_product_pricing'
ll_rowcount = lds_saved_pricinginds.importstring(is_saved_pricing_array)

If as_selected_tab = "Product" Then
	ll_rowcnt = tab_1.tabpage_product.dw_product.rowcount()

	if ll_rowcount > 0 then
		for ll_count = 1 to ll_rowcount
			ll_seq_no = lds_saved_pricinginds.getitemNumber(ll_count, 'seq_no')
			ls_find = "seq_no = " + String(ll_seq_no)
			ll_row = tab_1.tabpage_product.dw_product.find(ls_find ,0,ll_rowcnt)
			if ll_row > 0 then 
				tab_1.tabpage_product.dw_product.SetItem(ll_row, 'seq_no', ll_seq_no)
				ls_incl_excl = lds_saved_pricinginds.getitemString(ll_count, 'pricing_incl_excl')
				ls_pricing_ind = lds_saved_pricinginds.getitemString(ll_count, 'pricing_ind')
				tab_1.tabpage_product.dw_product.SetItem(ll_row, 'pricing_incl_excl', ls_incl_excl)
				tab_1.tabpage_product.dw_product.SetItem(ll_row, 'pricing_ind', ls_pricing_ind)
			end if
		next
	end if
Else
	If as_selected_tab = "ProductGroup" Then
		ll_rowcnt = tab_1.tabpage_productgroup.dw_productgroup.rowcount()

		if ll_rowcount > 0 then
			for ll_count = 1 to ll_rowcount
				ll_seq_no = lds_saved_pricinginds.getitemNumber(ll_count, 'seq_no')
				ls_find = "seq_no = " + String(ll_seq_no)
				ll_row = tab_1.tabpage_productgroup.dw_productgroup.find(ls_find ,0,ll_rowcnt)
				if ll_row > 0 then 
					
					tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_row, 'seq_no', ll_seq_no)
					ls_incl_excl = lds_saved_pricinginds.getitemString(ll_count, 'pricing_incl_excl')
					ls_pricing_ind = lds_saved_pricinginds.getitemString(ll_count, 'pricing_ind')
					tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_row, 'pricing_incl_excl', ls_incl_excl)
					tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_row, 'pricing_ind', ls_pricing_ind)
					
//						tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'seq_no', ll_seq_no)
//						ls_incl_excl = lds_saved_pricinginds.getitemString(ll_count, 'pricing_incl_excl')
//						ls_pricing_ind = lds_saved_pricinginds.getitemString(ll_count, 'pricing_ind')
//						tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'pricing_incl_excl', ls_incl_excl)
//						tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'pricing_ind', ls_pricing_ind)
					
				end if
			next
		end if
	End If
End If
end subroutine

public function boolean wf_fillcountrycode ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Initialize()
iu_ErrorContext.uf_Initialize()

ls_TutlType = 'REBCTYCD'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('country_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

dw_header_display.GetChild('country_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);DataWindowChild			ldwc_header, &
								ldwc_display

iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillcredmeth() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

//dmk
If Not wf_fillcountrycode() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillrebatetype() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillheaduom() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_filldetrebcrtyp() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If
If Not wf_filldetvolperiod() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If
If Not wf_filldetpayperiod() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

//dmk
If Not wf_fillheadcurr() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If


If Not wf_fillpricingind() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillrebatedateoption() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

//new field
If Not wf_fillbusiness() Then
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

//new field
If Not wf_fillsaleslocation() Then
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

//new field
If Not wf_fillloadinginstruction() Then
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_RebateDataAccess", iu_RebateDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ids_RebateList = Create DataStore
ids_RebateList.DataObject = 'd_rebatelist'

tab_1.tabpage_header.dw_header.Event ue_new()
//tab_1.tabpage_detail.dw_detail.Event ue_new()

This.Event Post ue_inquire()

Return True

end function

public function boolean wf_buildheaderstring (ref string as_updatestring);Boolean				lb_modified

Date					ldt_StartDate

Long					ll_DeletedCount, &
						ll_findrow

String				    ls_Description, &
						ls_RebateType, &
						ls_RequestedBy, &
						ls_CreditMethod, &
						ls_CountryCode, &
						ls_RebateDateOption, &
						ls_UnitOfMeasure, &
						ls_CurrencyCode, &
						ls_pricing_incl_excl, &
						ls_pricing_ind, &
						ls_BusinessUnit, &
						ls_saleslocation, &
						ls_loading_instruction, &
						ls_unit_of_measure
						
dwItemStatus		lis_temp

u_String_Functions		lu_StringFunctions


as_UpdateString = ''

If ib_DeleteHeader Then
	as_UpdateString += String(tab_1.tabpage_header.dw_header.GetItemNumber(1, 'rebate_id', Delete!, False)) + '~t' + &
			tab_1.tabpage_header.dw_header.GetItemString(1, 'description', Delete!, False) + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
			 + '~t' + &
              + '~t' + &
			 + '~t' + &	
			 + '~t' + &
			 + '~t' + &		
			 + '~t' + &			 
			'D' + '~r~n'
Else
	lb_modified = (tab_1.tabpage_header.dw_header.ModifiedCount() > 0)
	ls_Description = tab_1.tabpage_header.dw_header.GetItemString(1, 'description')
	If lu_StringFunctions.nf_IsEmpty(ls_description) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('description')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Description is a required field')
		Return False
	End If

	ls_RebateType = tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')
	If lu_StringFunctions.nf_IsEmpty(ls_Rebatetype) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('rebate_type')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Rebate Type is a required field')
		Return False
	End If
	ls_RequestedBy = tab_1.tabpage_header.dw_header.GetItemString(1, 'requested_by')
	If lu_StringFunctions.nf_IsEmpty(ls_RequestedBy) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('requested_by')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Requested By is a required field')
		Return False
	End If
	ls_CreditMethod = tab_1.tabpage_header.dw_header.GetItemString(1, 'credit_method')
	If is_RebateType = 'Q' Then
		ls_CreditMethod = ''
	Else
		If lu_StringFunctions.nf_IsEmpty(ls_CreditMethod) Then 
			Tab_1.SelectedTab = 1
			Tab_1.tabpage_header.dw_header.Setfocus()
			Tab_1.tabpage_header.dw_header.SetColumn('credit_method')
			Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
			SetMicroHelp('Credit Method is a required field')
			Return False
		End If
	End If
	//dmk
	ls_CountryCode = tab_1.tabpage_header.dw_header.GetItemString(1, 'country_code')
	If lu_StringFunctions.nf_IsEmpty(ls_CountryCode) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('country_code')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Country Code is a required field')
		Return False
	End If
	ls_RebateDateOption = tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_date_option')
	If lu_StringFunctions.nf_IsEmpty(ls_RebateDateOption) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('rebate_date_option')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Rebate Date Option is a required field')
		Return False
	End If
	ls_UnitOfMeasure = tab_1.tabpage_header.dw_header.GetItemString(1, 'unit_of_measure')
	If lu_StringFunctions.nf_IsEmpty(ls_UnitOfMeasure) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('unit_of_measure')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Unit Of Measure is a required field')
		Return False
	End If
	ls_CurrencyCode = tab_1.tabpage_header.dw_header.GetItemString(1, 'currency_code')
	If lu_StringFunctions.nf_IsEmpty(ls_CurrencyCode) Then 
		Tab_1.SelectedTab = 1
		Tab_1.tabpage_header.dw_header.Setfocus()
		Tab_1.tabpage_header.dw_header.SetColumn('currency_code')
		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
		SetMicroHelp('Currency Code is a required field')
		Return False
	End If
//NEW FIELD
	ls_BusinessUnit = tab_1.tabpage_header.dw_header.GetItemString(1,'business_unit')
	If is_RebateType = 'Q' Then
		ls_BusinessUnit = ''
	Else
		If lu_StringFunctions.nf_IsEmpty(ls_BusinessUnit) Then 
			Tab_1.SelectedTab = 1
			Tab_1.tabpage_header.dw_header.Setfocus()
			Tab_1.tabpage_header.dw_header.SetColumn('business_unit')
			Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
			SetMicroHelp('Business Unit is a required field')
			Return False
		End If
	End If
	
	If (is_RebateType = 'P') and (Trim(Tab_1.tabpage_header.dw_header.GetItemString(1, 'loading_instruction')) = 'B') Then
		ls_unit_of_measure = Trim(ls_UnitOfMeasure)
		If (ls_unit_of_measure = '03') or (ls_unit_of_measure = '04') Then
			//  continue
		Else
			Tab_1.SelectedTab = 1
			Tab_1.tabpage_header.dw_header.Setfocus()
			Tab_1.tabpage_header.dw_header.SetColumn('unit_of_measure')
			Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
			SetMicroHelp('Only QUANTITY or PIECE are allowed for charge type of LOADING PLATFORM, HIDE BAGS')
			Return False
		End If
	End If	
	
	If is_RebateType = 'Q' Then
		ls_saleslocation = tab_1.tabpage_header.dw_header.GetItemString(1,'sales_location')
	Else
		ls_saleslocation = '   '
	End If
	
	If is_RebateType = 'P' Then
		ls_loading_instruction = tab_1.tabpage_header.dw_header.GetItemString(1,'loading_instruction')
	Else
		ls_loading_instruction = ' '
	End If	
	
//		ls_pricing_incl_excl = tab_1.tabpage_header.dw_header.GetItemString(1, 'pricing_incl_excl')
//	If lu_StringFunctions.nf_IsEmpty(ls_pricing_incl_excl) Then 
//		Tab_1.SelectedTab = 1
//		Tab_1.tabpage_header.dw_header.Setfocus()
//		Tab_1.tabpage_header.dw_header.SetColumn('pricing_incl_excl')
//		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
////		SetMicroHelp('Pricing  is a required field')
//		Return False
//	End If
//			ls_pricing_ind = tab_1.tabpage_header.dw_header.GetItemString(1, 'pricing_ind')
//	If lu_StringFunctions.nf_IsEmpty(ls_pricing_ind) Then 
//		Tab_1.SelectedTab = 1
//		Tab_1.tabpage_header.dw_header.Setfocus()
//		Tab_1.tabpage_header.dw_header.SetColumn('pricing_ind')
//		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
////		SetMicroHelp('Pricing  is a required field')
//		Return False
//	End If
	
	
// removed so changes can be made to rebates started before current date

ldt_StartDate = tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date') 
//If ls_RebateType = 'A'  And ldt_StartDate < Today() and lb_modified Then
//		Tab_1.SelectedTab = 1
//		Tab_1.tabpage_header.dw_header.Setfocus()
//		Tab_1.tabpage_header.dw_header.SetColumn('start_date')
//		Tab_1.tabpage_header.dw_header.SelectText(1, 1000)
//		SetMicroHelp('Start Date must be greater than or equal to Today')
//		Return False
//End If	
// 		removed form following statement was between product_exclude_ind and customer_select_all_ind
//			
//			tab_1.tabpage_header.dw_header.GetItemString(1, 'product_exclude_ind') + '~t' + &
//

//dmk

as_UpdateString += String(tab_1.tabpage_header.dw_header.GetItemNumber(1, 'rebate_id')) + '~t' + &
			ls_Description + '~t' + &
			ls_RebateType + '~t' + &
			ls_RequestedBy + '~t' + &
			ls_CreditMethod + '~t' + &
			String(ldt_StartDate, 'YYYY-MM-DD') + '~t' + &
			String(tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date'), 'YYYY-MM-DD') + '~t' + &
			tab_1.tabpage_header.dw_header.GetItemString(1, 'division_exclude_ind') + '~t' + &
			tab_1.tabpage_header.dw_header.GetItemString(1, 'product_group_exclude_ind') + '~t' + &
			tab_1.tabpage_header.dw_header.GetItemString(1, 'customer_select_all_ind') + '~t' + &
			ls_CountryCode + '~t' + &
			ls_RebateDateOption + '~t' + &
			ls_UnitOfMeasure + '~t' + &
			ls_CurrencyCode + '~t' + &
			ls_BusinessUnit + '~t' + &
			ls_saleslocation + '~t' + &		
			ls_loading_instruction + '~t' + &			
			wf_GetUpdateInd(1, tab_1.tabpage_header.dw_header) + '~r~n'
End if		

Return True
end function

public function boolean wf_fillrebatetype ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
										
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'REBTYPE'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('rebate_type', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

dw_header_display.GetChild('rebate_type', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public subroutine wf_dbconnection ();string	ls_inifile, ls_section

ls_inifile = 'ibp002.ini'
ls_section = "SMA DATABASE"

//SQLCA = Create transaction // Create a transaction object
SQLCA.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
SQLCA.database = ProfileString(ls_inifile, ls_section, "database", "false")
SQLCA.dbparm = ProfileString(ls_inifile, ls_section, "dbParm", "false")

//if Upper(SQLCA.dbms) = Upper("SNC SQL Native Client") Then					 // Connecting to a SQL Server 
	SQLCA.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ") // SQL Database Server Name
	SQLCA.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")			 // SQL User name
	SQLCA.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")		 // SQL Password
//Else
//	SQLCA.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
//End If

//if SQLCA.database = 'false' or SQLCA.dbParm = 'false' then 							 // "SMA DATABASE" Not found in the INI
//	SQLCA.database = ProfileString(ls_inifile, "DEFAULT DATABASE", "database", "pblocaldb")
//	SQLCA.dbParm = ProfileString(ls_inifile, "DEFAULT DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
//end if


Connect Using SQLCA; //Connect to database
If SQLCA.SQLCode = -1 Then
	// do nothing
Else
	If SQLCA.SQLCode <> 0 then
		Messagebox("Error","Connection to database failed: " + SQLCA.SQLErrText)
		Disconnect using SQLCA;
		Return
	End If
End if
end subroutine

public function boolean wf_builddetailstring (ref string as_updatestring);Long					ll_DeletedCount, &
						ll_row
String				     ls_UpdateString, &
						ls_Rate, &
						ls_MinimumTarget, &
						ls_MaximumTarget, &
						ls_wgt_ind, &
						ls_VolumePeriod, &
						ls_PayoutPeriod, &
						ls_incl_excl, &
						ls_pricing_ind
						
date					ldt_date,ld_check_date,ld_detail_date

u_String_Functions	lu_StringFunctions

as_UpdateString = ''

ll_DeletedCount = tab_1.tabpage_detail.dw_detail.DeletedCount()
If ll_DeletedCount > 0 Then
	For ll_row = 1 to ll_DeletedCount
		as_UpdateString += 'D' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'detail_id', Delete!, False)) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'start_date', Delete!, False), 'YYYY-MM-DD') + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'end_date', Delete!, False), 'YYYY-MM-DD') + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDecimal(ll_row, 'rate', Delete!, False)) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'minimum_target_volume', Delete!, False)) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'maximum_target_volume', Delete!, False)) + '~t' + &
				tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'volume_period', Delete!, False) + '~t' + &
				tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'payout_period', Delete!, False) + '~t' + &
				tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'incl_min_wgt_ind', Delete!, False) + '~t' + &
				+ '~t' + &
			 	+ '~t' + &
				'D' + '~r~n'
		//		tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_incl_excl', Delete!, False) + '~t' + &
		//		tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind', Delete!, False) + '~t' + &
			 
	Next				
End IF

ll_row = 0
//check rebate option
//Choose case tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') 

Choose case is_RebateOption 
	case "A"
		if not wf_check_dates(ll_row, ldt_date, "all") then
			Return False
		End If
//	case "C"
//		if not wf_check_dates(ll_row, ldt_date, "all") then
//			Return False
//		End If
//	case "O"
//		if not wf_check_dates(ll_row, ldt_date, "all") then
//			Return False
//		End If	
//	case "I"
//		if not wf_check_dates(ll_row, ldt_date, "all") then
//			Return False
//		End If	
	case "V"
		if not wf_check_volumes('all',ll_row, 0) then
			return false
		end if
end choose
Do
	ll_row = tab_1.tabpage_detail.dw_detail.GetNextModified(ll_row, Primary!)
	
	If ll_row > 0 Then		
		ls_Rate = String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'rate'))
		If lu_StringFunctions.nf_IsEmpty(ls_Rate) Then 
			Tab_1.SelectedTab = 2
			Tab_1.tabpage_detail.dw_detail.Setfocus()
			Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_row)
			Tab_1.tabpage_detail.dw_detail.SetColumn('rate')
			Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
			SetMicroHelp('Rate is a required field')
			Return False
		Else
			If idc_detail_rebate_rate = 0 Then
				If ll_row = 1 Then
					idc_detail_rebate_rate = tab_1.tabpage_detail.dw_detail.GetItemDecimal(ll_row, 'rate')
				End If
			End If
		End If

		ls_wgt_ind = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'incl_min_wgt_ind')
		If lu_StringFunctions.nf_IsEmpty(ls_wgt_ind) Then 
			Tab_1.SelectedTab = 2
			Tab_1.tabpage_detail.dw_detail.Setfocus()
			Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_row)
			Tab_1.tabpage_detail.dw_detail.SetColumn('incl_min_wgt_ind')
			Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
			SetMicroHelp('Include Min Weight indicator is a required field')
			Return False
		End If
		ls_VolumePeriod = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'volume_period')
		If lu_StringFunctions.nf_IsEmpty(ls_VolumePeriod) Then
			If (is_RebateOption = 'A') AND (is_RebateType = 'P') Then 
				ls_VolumePeriod = ''
			Else
				Tab_1.SelectedTab = 2
				Tab_1.tabpage_detail.dw_detail.Setfocus()
				Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_row)
				Tab_1.tabpage_detail.dw_detail.SetColumn('volume_period')
				Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
				SetMicroHelp('Volume Period is a required field')
				Return False
			End If
		End If
		ls_PayoutPeriod = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'payout_period')
		If lu_StringFunctions.nf_IsEmpty(ls_PayoutPeriod) Then 
			If (is_RebateOption = 'A') AND (is_RebateType = 'P') Then 
				ls_PayoutPeriod = ''
			Else			
				Tab_1.SelectedTab = 2
				Tab_1.tabpage_detail.dw_detail.Setfocus()
				Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_row)
				Tab_1.tabpage_detail.dw_detail.SetColumn('payout_period')
				Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
				SetMicroHelp('Payout Period is a required field')
				Return False
			End If
		End If
		
		//dmk use rebate option 
//		If tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'A' OR & 
//		   tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'C' or & 
//		   tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'O' or &
//			tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'I' Then 
		If is_RebateOption = 'A' Then
			If IsNull(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_incl_excl')) Then
				ls_incl_excl = ' '
			Else
				ls_incl_excl = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_incl_excl')
				If ls_incl_excl = 'I' or ls_incl_excl = 'E' Then
					If IsNull(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind')) or (Len(trim(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind'))) = 0) Then
						Tab_1.SelectedTab = 2
						Tab_1.tabpage_detail.dw_detail.Setfocus()
						Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_row)
						Tab_1.tabpage_detail.dw_detail.SetColumn('pricing_ind')
						Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
						SetMicroHelp('Pricing Indicator is a required field when Incl/Excl indicator is selected')
						Return False
					End If
				End If
									
				
			End If
			If IsNull(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind')) Then
				ls_pricing_ind = ' '
			Else
				ls_pricing_ind = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind')
			End If
			
			//dmk
			as_UpdateString += 'D' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'detail_id')) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'start_date'), 'YYYY-MM-DD') + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'end_date'), 'YYYY-MM-DD') + '~t' + &
				ls_Rate + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'minimum_target_volume')) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'maximum_target_volume')) + '~t' + &
				ls_VolumePeriod + '~t' + &
				ls_PayoutPeriod + '~t' + &
				ls_wgt_ind + '~t' + &
				ls_incl_excl + '~t' + &
				ls_pricing_ind + '~t' + &
				wf_GetUpdateInd(ll_row, tab_1.tabpage_detail.dw_detail) + '~t' + '~r~n'
		Else
			If IsNull(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_incl_excl')) Then
				ls_incl_excl = ' '
			Else
				ls_incl_excl = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_incl_excl')
				If ls_incl_excl = 'I' or ls_incl_excl = 'E' Then
					If IsNull(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind')) or (Len(trim(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind'))) = 0) Then
						Tab_1.SelectedTab = 2
						Tab_1.tabpage_detail.dw_detail.Setfocus()
						Tab_1.tabpage_detail.dw_detail.ScrollToRow(ll_row)
						Tab_1.tabpage_detail.dw_detail.SetColumn('pricing_ind')
						Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
						SetMicroHelp('Pricing Indicator is a required field when Incl/Excl indicator is selected')
						Return False
					End If
				End If				
			End If
			If IsNull(tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind')) Then
				ls_pricing_ind = ' '
			Else
				ls_pricing_ind = tab_1.tabpage_detail.dw_detail.GetItemString(ll_row, 'pricing_ind')
			End If
			as_UpdateString += 'D' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'detail_id')) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'start_date'), 'YYYY-MM-DD') + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'end_date'), 'YYYY-MM-DD') + '~t' + &
				ls_Rate + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'minimum_target_volume')) + '~t' + &
				String(tab_1.tabpage_detail.dw_detail.GetItemNumber(ll_row, 'maximum_target_volume')) + '~t' + &
				ls_VolumePeriod + '~t' + &
				ls_PayoutPeriod + '~t' + &
				ls_wgt_ind + '~t' + &
				ls_incl_excl + '~t' + &
				ls_pricing_ind + '~t' + &
				wf_GetUpdateInd(ll_row, tab_1.tabpage_detail.dw_detail) + '~t' + '~r~n'
		End If
				
		//dld	add all the following to the end if
		ld_detail_date = tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'end_date')
//	Else
//		ll_row = tab_1.tabpage_detail.dw_detail.RowCount( ) - 1
//		
//		If ll_row  = 0 Then ll_row = 1
//		
//		ld_detail_date = tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row, 'end_date')   
//		ll_row = 0
	End If
Loop While ll_row > 0


ld_check_date = tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date')

// No longer needed
//If ld_detail_date <> ld_check_date Then
//	tab_1.SelectTab ("tabpage_detail")
//	Tab_1.tabpage_detail.dw_detail.ScrollToRow(tab_1.tabpage_detail.dw_detail.RowCount( ) - 1)
//	Tab_1.tabpage_detail.dw_detail.SetColumn('end_date')
//	Tab_1.tabpage_detail.dw_detail.SelectText(1, 1000)
//	SetMicroHelp('The End Date of the last Detail row must equal the Header End Date')
//	Return False
//End If
		
Return True
end function

public function boolean wf_updateproducts (string as_type, ref string as_updatestring);Boolean				lb_Return
Long					ll_NewRowCount, &
						ll_OriginalRowCount, &
						ll_NewCount, &
						ll_OriginalCount, &
						ll_row, &
						ll_row2, &
						ll_seq_no, &
						ll_org_seq_no,&
						ll_org_row

Decimal				ldc_rebate_rate

String				ls_NewCode, &
						ls_OriginalCode, &
						ls_include_ind, &
						ls_org_include_ind, &
						ls_rebate_ind, &
						ls_org_rbt_ind, &
						ls_hold_string, &
						ls_incl_excl, &
						ls_pricing_ind

Date					dt_start_date, dt_end_date

lb_Return = False
ll_NewRowCount = tab_1.tabpage_Product.dw_Product.RowCount()

// This must be in this order for this update to work.
//dmk changed sort from #8 to #10
//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
If is_RebateOption = 'A' then
	tab_1.tabpage_product.dw_product.SetSort("#8 A") //changed back to #8 seq_no - removed 2 columns
	tab_1.tabpage_product.dw_product.Sort()

Else
	tab_1.tabpage_product.dw_product.SetSort("#1 A")
	tab_1.tabpage_product.dw_product.Sort()
End If

If Not IsValid(ids_Products) Then
	If ll_NewRowCount > 0 Then
		// All New Products
		For ll_NewCount = 1 to ll_NewRowCount
			//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I'Then
			If is_RebateOption = 'A' Then
				ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
				ls_NewCode = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'product_code')
				ls_rebate_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'tonnage_rebate_credit')
				dt_start_date = tab_1.tabpage_Product.dw_Product.GetItemDate(ll_NewCount, 'start_date')
				dt_end_date = tab_1.tabpage_Product.dw_Product.GetItemDate(ll_NewCount, 'end_date')
				ll_seq_no = tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_NewCount, 'seq_no')
				ldc_rebate_rate = tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_NewCount, 'rebate_rate')
				if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_incl_excl')) then
					ls_incl_excl = ' '
				else
					ls_incl_excl = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_incl_excl')
				end if
				if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_ind')) then
					ls_pricing_ind = ' '
				else
					ls_pricing_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_ind')
				end if

				as_UpdateString += as_type + ls_NewCode + '~t' + ls_include_ind + '~t' &
									 + ls_rebate_ind + '~t' + string(ldc_rebate_rate) + '~t' + string((dt_start_date), "YYYY-MM-DD") + '~t'  &
									 + string((dt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
									 + ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' +'~r~n'
			Else
				ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
				if ls_include_ind = "" then
					ls_include_ind = " "
				end if
				ls_NewCode = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'product_code')
				ls_rebate_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'tonnage_rebate_credit')
				as_UpdateString += as_type + ls_NewCode + '~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t' + 'I' + '~r~n'
			End If
		Next	
		Return True
	Else
		Return False
	End If
End If

ll_OriginalRowCount = ids_Products.RowCount()

// No Products
If Not ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	Return False
End If

// All New Products
If ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	For ll_NewCount = 1 to ll_NewRowCount
		//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
		If is_RebateOption = 'A' Then
			ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
			ls_rebate_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'tonnage_rebate_credit')
			dt_start_date = tab_1.tabpage_Product.dw_Product.GetItemDate(ll_NewCount, 'start_date')
			dt_end_date = tab_1.tabpage_Product.dw_Product.GetItemDate(ll_NewCount, 'end_date')
			ll_seq_no = tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_NewCount, 'seq_no')
			ldc_rebate_rate = tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_NewCount, 'rebate_rate')
				if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_incl_excl')) then
					ls_incl_excl = ' '
				else
					ls_incl_excl = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_incl_excl')
				end if
				if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_ind')) then
					ls_pricing_ind = ' '
				else
					ls_pricing_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_ind')
				end if		
		
			as_UpdateString += as_type + tab_1.tabpage_Product.dw_Product.GetItemString( &
					ll_NewCount, 1) + '~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t' &
					+ string(ldc_rebate_rate) + '~t' + string((dt_start_date), "YYYY-MM-DD") + '~t'  &
					+ string((dt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
					+ ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' + '~r~n'
		Else
			ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
			if ls_include_ind = "" then
				ls_include_ind = " " 
			end if
			ls_rebate_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'tonnage_rebate_credit')
			as_UpdateString += as_type + tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 1) + &
			'~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t' + 'I' + '~r~n'

		End If
	Next	
	Return True
End If

// Delete All Products
//dmk change from 10 to 12
If Not ll_NewRowCount > 0 And ll_OriginalRowCount > 0 Then
	For ll_OriginalCount = 1 to ll_OriginalRowCount
		ll_org_seq_no = ids_Products.GetItemNumber(ll_OriginalCount, 12)
		If is_RebateOption = 'A' Then 
			as_UpdateString += as_type + ids_Products.GetItemString(ll_OriginalCount, 1) + '~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' +  string(ll_org_seq_no) + '~t' + "X" + '~t' + "X" + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type + ids_Products.GetItemString(ll_OriginalCount, 1) + '~t' + "X" + '~t' + "X" + '~t' + 'D' + '~r~n'
		End If
	Next
	Return True
End If

ll_OriginalCount = 1

For ll_NewCount = 1 to ll_NewRowCount

	ls_NewCode = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'product_code')
	ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
	If ls_include_ind = "" Then
		ls_include_ind = " " 
	End If
	ls_rebate_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'tonnage_rebate_credit')

	//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then	
		dt_start_date = tab_1.tabpage_Product.dw_Product.GetItemDate(ll_NewCount, 'start_date')
		dt_end_date = tab_1.tabpage_Product.dw_Product.GetItemDate(ll_NewCount, 'end_date')
		ll_seq_no = tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_NewCount, 'seq_no')
		ldc_rebate_rate = tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_NewCount, 'rebate_rate')
		if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_incl_excl')) then
			ls_incl_excl = ' '
		else
			ls_incl_excl = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_incl_excl')
		end if
		if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_ind')) then
			ls_pricing_ind = ' '
		else
			ls_pricing_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'pricing_ind')
		end if
	End If
		
// Reached the end of the Original Buffer Insert the rest.
	If ll_OriginalCount > ll_OriginalRowCount Then
//dmk
		ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
		//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t' &
			+ string(ldc_rebate_rate) + '~t' + string((dt_start_date), "YYYY-MM-DD") + '~t'  &
			+ string((dt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
			+ ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' + '~r~n'
			lb_Return = True
			Continue
		Else
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t' + 'I' + '~r~n'
			lb_Return = True
			Continue
		End If			
	End If
		
	ls_OriginalCode = ids_Products.GetItemString(ll_OriginalCount, 1) 
	ls_org_include_ind = ids_Products.GetItemString(ll_OriginalCount, 3)
	ls_org_rbt_ind = ids_Products.GetItemString(ll_OriginalCount, 4)

	//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		ll_row2 = ids_Products.RowCount()
		If IsValid(ids_Products) Then
			string ls_debug, ls_temp
			ls_debug = ids_Products.describe("DataWindow.Data")
			ls_temp = ids_Products.GetItemString(ll_OriginalCount, 5)
			ls_temp = ids_Products.GetItemString(ll_OriginalCount, 6)
			ls_temp = ids_Products.GetItemString(ll_OriginalCount, 7)
			ls_temp = ids_Products.GetItemString(ll_OriginalCount, 9)
			ls_temp = ids_Products.GetItemString(ll_OriginalCount, 10)
			ll_org_seq_no = ids_Products.GetItemNumber(ll_OriginalCount, 12)
		End If
	End If

// They equal so don't send it to be updated.
	//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' then
		If ll_seq_no  =  ll_org_seq_no Then
			ll_OriginalCount ++
			Continue
		End If
	Else
		If ls_NewCode = ls_OriginalCode Then
			ll_OriginalCount ++
			Continue
		End If
	End If
	
// This is a new Product send it to be inserted.
	//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		If ll_seq_no  <  ll_org_seq_no Then
			ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
		
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t'&
			+ string(ldc_rebate_rate) + '~t' + string((dt_start_date), "YYYY-MM-DD") + '~t'  &
			+ string((dt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t'  &
			+ ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' +'~r~n'
			lb_Return = True
			Continue
		End If
	Else
		If ls_NewCode < ls_OriginalCode Then
			ls_include_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_NewCount, 'include_ind')
			if ls_include_ind = "" then
				ls_include_ind = " " 
			end if
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_include_ind + '~t' + ls_rebate_ind + '~t' + 'I' + '~r~n'
			lb_Return = True
			Continue
		End If		
	End If
	
// The original was deleted send it to be deleted.	
	//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		If ll_seq_no  >  ll_org_seq_no Then
			ls_include_ind = 'X' //  X shows that is should not be on the DB TRBTPROD
			//dmk added two column delimiters
//			as_UpdateString += as_type + ids_Products.GetItemString(ll_OriginalCount, 1)  + '~t' + "X" + '~t' + "X" + '~t' + '~t' + '~t' 
//			+ "X" + '~t' + "X" + '~t' + "X" + '~t' + string(ids_Products.GetItemNumber(ll_OriginalCount, 12)) + '~t' + '~t' + '~t' + 'D' +'~r~n'

             // removed 2 ~t in middle for 2 columns removed
			as_UpdateString += as_type + ids_Products.GetItemString(ll_OriginalCount, 1) + &
				'~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' + string(ids_Products.GetItemNumber(ll_OriginalCount, 12)) + '~t' + "X" + '~t' + "X" + '~t' + 'D' + '~r~n'
			lb_Return = True
			ll_NewCount --
			ll_OriginalCount ++
			Continue
		End IF
	Else
		If ls_NewCode > ls_OriginalCode Then
			ls_include_ind = 'X' //  X shows that is should not be on the DB TRBTPROD
			as_UpdateString += as_type + ls_OriginalCode  + '~t' + "X" + '~t' + "X" + '~t' + 'D' + '~r~n'
			lb_Return = True
			ll_NewCount --
			ll_OriginalCount ++
			Continue
		End IF
	End If
Next

//Delete the rest of the Original Buffer
//dmk changed from 10 to 12
If ll_OriginalCount <= ll_OriginalRowCount Then
	For ll_OriginalCount = ll_OriginalCount to ll_OriginalRowCount
		ls_include_ind = 'X' //  X shows that is should not be on the DB TRBTPROD
		//if is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then 
		If is_RebateOption = 'A' then 
	//dmk added two column delimiters for new columns
	  // removed 2 ~t in middle for 2 columns removed
	  string ls_t
	         ls_t = ids_Products.GetItemString(ll_OriginalCount, 1)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 2)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 3)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 4)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 5)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 6)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 7)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 8)
			ls_t = ids_Products.GetItemString(ll_OriginalCount, 9)
			//ls_t = ids_Products.GetItemString(ll_OriginalCount, 10)
			ls_t = string(ids_Products.GetItemNumber(ll_OriginalCount, 12))
			as_UpdateString += as_type + ids_Products.GetItemString(ll_OriginalCount, 1) + &
				'~t' + "X" + '~t' + "X" +  '~t' + "X" + '~t' + "X" + '~t' + "X" + '~t' + string(ids_Products.GetItemNumber(ll_OriginalCount, 12)) + '~t' + "X" + '~t' + "X" + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type + ids_Products.GetItemString(ll_OriginalCount, 1) + '~t' + "X" + '~t' + "X" + '~t' + 'D' + '~r~n'
		End If
	Next
	lb_Return = True
End IF

// update products
ll_OriginalRowCount = ids_Products.RowCount()
ll_row = tab_1.tabpage_product.dw_product.GetNextModified(ll_row, Primary!)
Do While ll_row > 0
	//If is_RebateType = 'A' or is_RebateType = 'O' or is_RebateType = 'C' or is_RebateType = 'I' Then
If is_RebateOption = 'A' Then
	For ll_org_row = 1 to ll_OriginalRowCount
		IF(tab_1.tabpage_Product.dw_Product.GetItemNumber(ll_row, 'seq_no') = ids_Products.GetItemNumber(ll_org_row,12) AND &
			tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'product_code') = ids_Products.GetItemString(ll_org_row,1) ) Then
			IF(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'include_ind') <> ids_Products.GetItemString(ll_org_row,3) OR &
			  String(tab_1.tabpage_Product.dw_Product.GetItemDecimal(ll_row, 'rebate_rate')) <> ids_Products.GetItemString(ll_org_row,5) OR &
			  String(tab_1.tabpage_Product.dw_Product.GetItemDate(ll_row, 'start_date')) <> ids_Products.GetItemString(ll_org_row,6) OR &
			  String(tab_1.tabpage_Product.dw_Product.GetItemDate(ll_row, 'end_date')) <> ids_Products.GetItemString(ll_org_row,7) OR &
			  TRIM(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'pricing_incl_excl')) <> TRIM(ids_Products.GetItemString(ll_org_row,10)) OR &
			  TRIM(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'pricing_ind')) <> TRIM(ids_Products.GetItemString(ll_org_row,11)) )then
						if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'pricing_incl_excl')) then
							ls_incl_excl = ' '
						else
							ls_incl_excl = tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'pricing_incl_excl')
						end if
						if IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'pricing_ind')) then
							ls_pricing_ind = ' '
						else
							ls_pricing_ind = tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'pricing_ind')
						end if

						as_UpdateString += 'P' + &
							tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'product_code')  + '~t' + &
							tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'include_ind')  + '~t' + &
							tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'tonnage_rebate_credit')  + '~t' + &
							string(tab_1.tabpage_product.dw_Product.GetItemNumber(ll_row, 'rebate_rate')) + '~t' + &				
							string(tab_1.tabpage_product.dw_Product.GetItemDate(ll_row, 'start_date'), "YYYY-MM-DD") + '~t' + &
							string(tab_1.tabpage_product.dw_Product.GetItemDate(ll_row, 'end_date'), "YYYY-MM-DD") + '~t' + &
							string(tab_1.tabpage_product.dw_Product.GetItemNumber(ll_row, 'seq_no')) + '~t' + &
							ls_incl_excl + '~t' + &
							ls_pricing_ind + '~t' + &
							'U' + '~r~n'
			
			END IF
		ll_org_row = ll_OriginalRowCount
		END IF
		
	NEXT
ELSE
	For ll_org_row = 1 to ll_OriginalRowCount
		IF(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'product_code') = ids_Products.GetItemString(ll_org_row,1) ) Then
			IF(tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'include_ind') <> ids_Products.GetItemString(ll_org_row,3) OR &
			   tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'tonnage_rebate_credit') <> ids_Products.GetItemString(ll_org_row,4) ) THEN
				as_UpdateString += 'P' + &
					tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'product_code')  + '~t' + &
					tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'include_ind')  + '~t' + &
					tab_1.tabpage_Product.dw_Product.GetItemString(ll_row, 'tonnage_rebate_credit')  + '~t' + &
					'U' + '~r~n'
			END IF
		ll_org_row = ll_OriginalRowCount
		END IF		
	NEXT
END IF
	ll_row = tab_1.tabpage_product.dw_product.GetNextModified(ll_row, Primary!)
	lb_Return = True
Loop
		
Return lb_Return

end function

public function boolean wf_createoriginaldatastore (string as_type);DataStore					lds_DataStore
DataWindow					ldw_DataWindow
Long							ll_Count, &
								ll_code, &
								ll_NewRow, &
								ll_code10
								
String						ls_code

//All resets changed to destroy because not all the data gets cleared out
Choose Case as_type
	Case 'V'
//		If IsValid(ids_Divisions) Then
//			ids_Divisions.Reset()
//		Else
//			ids_Divisions = Create datastore
//			ids_Divisions.DataObject = 'd_datastore'
//		End If
//		lds_datastore = ids_Divisions
//		ldw_datawindow = tab_1.tabpage_division.dw_division
		If IsValid(ids_Divisions) Then
			Destroy ids_Divisions
		End If
		ids_Divisions = Create datastore
		ids_Divisions.DataObject = 'd_datastore'		
		lds_datastore = ids_Divisions
		ldw_datawindow = tab_1.tabpage_division.dw_division
	Case 'P'
//		If IsValid(ids_Products) Then
//			ids_Products.Reset()
//		Else
//			ids_Products = Create datastore
//			ids_Products.DataObject = 'd_datastore'
//		End If		
//		lds_datastore = ids_Products
//		ldw_datawindow = tab_1.tabpage_product.dw_product
		If IsValid(ids_Products) Then
			Destroy ids_Products
		End If
		ids_Products = Create datastore
		ids_Products.DataObject = 'd_datastore'				
		lds_datastore = ids_Products
		ldw_datawindow = tab_1.tabpage_product.dw_product
	Case 'G'
//		If IsValid(ids_ProductGroups) Then
//			ids_ProductGroups.Reset()
//		Else
//			ids_ProductGroups = Create datastore
//			ids_ProductGroups.DataObject = 'd_datastore'
//		End IF
//		lds_datastore = ids_ProductGroups
//		ldw_datawindow = tab_1.tabpage_productgroup.dw_productgroup
		If IsValid(ids_ProductGroups) Then
			Destroy ids_ProductGroups
		End IF
		ids_ProductGroups = Create datastore
		ids_ProductGroups.DataObject = 'd_datastore'		
		lds_datastore = ids_ProductGroups
		ldw_datawindow = tab_1.tabpage_productgroup.dw_productgroup
	Case 'S'
//		If IsValid(ids_CustomerGroups) Then
//			ids_CustomerGroups.Reset()
//		Else
//			ids_CustomerGroups = Create datastore
//			ids_CustomerGroups.DataObject = 'd_datastore'
//		End If
//		lds_datastore = ids_CustomerGroups
//		ldw_datawindow = tab_1.tabpage_customergroup.dw_customergroup
		If IsValid(ids_CustomerGroups) Then
			Destroy ids_CustomerGroups
		End If
		ids_CustomerGroups = Create datastore
		ids_CustomerGroups.DataObject = 'd_datastore'		
		lds_datastore = ids_CustomerGroups
		ldw_datawindow = tab_1.tabpage_customergroup.dw_customergroup
	Case 'O'
//		If IsValid(ids_Locations) Then
//			ids_Locations.Reset()
//		Else
//			ids_Locations = Create datastore
//			ids_Locations.DataObject = 'd_datastore'
//		End If
//		lds_datastore = ids_Locations
//		ldw_datawindow = tab_1.tabpage_location.dw_location
		If IsValid(ids_Locations) Then
			Destroy ids_Locations
		End If
		ids_Locations = Create datastore
		ids_Locations.DataObject = 'd_datastore'		
		lds_datastore = ids_Locations
		ldw_datawindow = tab_1.tabpage_location.dw_location
	Case 'C'	
//		If IsValid(ids_customers) Then
//			ids_customers.Reset()
//		Else
//			ids_customers = Create datastore
//			ids_customers.DataObject = 'd_datastore'
//		End If
//		lds_datastore = ids_customers
//		ldw_datawindow = tab_1.tabpage_customer.dw_customer		
		If IsValid(ids_customers) Then
			Destroy ids_customers
		End If
		ids_customers = Create datastore
		ids_customers.DataObject = 'd_datastore'		
		lds_datastore = ids_customers
		ldw_datawindow = tab_1.tabpage_customer.dw_customer
End Choose

//dmk added two new columns for 'G' and 'P' for accruals
For ll_Count = 1 to ldw_datawindow.RowCount()
	ll_NewRow = lds_DataStore.InsertRow(0)
//	If (Is_RebateType = 'A') or (is_RebateType = 'C') or (Is_RebateType = 'O') or (Is_RebateType = 'I')Then
If is_RebateOption = 'A' Then
		If as_type = 'V' Then  //Division
			ls_code = ldw_datawindow.GetItemString(ll_count, 1) //division code
			lds_datastore.SetItem(ll_NewRow, 1, ls_code)
			ls_code = ldw_datawindow.GetItemString(ll_count, 2)  //division short description
			lds_datastore.SetItem(ll_NewRow, 2, ls_code)
			ls_code = ldw_datawindow.GetItemString(ll_count, 3) // tonnage rebate credit code
			lds_datastore.SetItem(ll_NewRow, 3, ls_code)
			ls_code = ldw_datawindow.GetItemString(ll_count, 4)  //include ind
			lds_datastore.SetItem(ll_NewRow, 4, ls_code)
			ls_code = String(ldw_datawindow.GetItemDecimal(ll_count, 5)) //rebate rate
			lds_datastore.SetItem(ll_NewRow, 5, ls_code)
			ls_code = String(ldw_datawindow.GetItemDate(ll_count, 6), 'YYYY-MM-DD') //start date
			lds_datastore.SetItem(ll_NewRow, 6, ls_code)
			ls_code = String(ldw_datawindow.GetItemDate(ll_count, 7), 'YYYY-MM-DD') //end date
			lds_datastore.SetItem(ll_NewRow, 7, ls_code)
			ls_code = String(ldw_datawindow.GetItemDate(ll_count, 8), 'YYYY-MM-DD') //ORIGINAL start date
			lds_datastore.SetItem(ll_NewRow, 8, ls_code)			
		Else
			If as_type = 'G' Then  //Product Group
				ls_code = ldw_datawindow.GetItemString(ll_count, 1) //group id
				lds_datastore.SetItem(ll_NewRow, 1, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 2) //group description
				lds_datastore.SetItem(ll_NewRow, 2, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 3) //owner 
				lds_datastore.SetItem(ll_NewRow, 3, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 4) //tonnage rebate credit code
				lds_datastore.SetItem(ll_NewRow, 4, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 5) //include all product group ind
				lds_datastore.SetItem(ll_NewRow, 5, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 6) //exclude ind
				lds_datastore.SetItem(ll_NewRow, 6, ls_code)
				ls_code = String(ldw_datawindow.GetItemDecimal(ll_count, 7)) //rebate rate
				lds_datastore.SetItem(ll_NewRow, 7, ls_code)
				ls_code = String(ldw_datawindow.GetItemDate(ll_count, 8)) //start date
				lds_datastore.SetItem(ll_NewRow, 8, ls_code)
				ls_code = String(ldw_datawindow.GetItemDate(ll_count, 9)) //end date
				lds_datastore.SetItem(ll_NewRow, 9, ls_code)
				ll_code10 = ldw_datawindow.GetItemNumber(ll_count, 10) //seq no
				lds_datastore.SetItem(ll_NewRow, 12, ll_code10)
				ls_code = String(ldw_datawindow.GetItemString(ll_count, 11)) //pricing_incl_excl
				lds_datastore.SetItem(ll_NewRow, 10, ls_code)
				ls_code = String(ldw_datawindow.GetItemString(ll_count, 12)) //pricing_ind
				lds_datastore.SetItem(ll_NewRow, 11, ls_code)
			Else
				If as_type = 'P' Then //Product
					ls_code = ldw_datawindow.GetItemString(ll_count, 1) //product code
					lds_datastore.SetItem(ll_NewRow, 1, ls_code)
					ls_code = ldw_datawindow.GetItemString(ll_count, 2) //product description
					lds_datastore.SetItem(ll_NewRow, 2, ls_code)
					ls_code = ldw_datawindow.GetItemString(ll_count, 3) //include ind
					lds_datastore.SetItem(ll_NewRow, 3, ls_code)
					ls_code = ldw_datawindow.GetItemString(ll_count, 4) //tonnage rebate credit code
					lds_datastore.SetItem(ll_NewRow, 4, ls_code)
					ls_code = String(ldw_datawindow.GetItemDecimal(ll_count, 5)) //rebate rate
					lds_datastore.SetItem(ll_NewRow, 5, ls_code)
					ls_code = String(ldw_datawindow.GetItemDate(ll_count, 6)) //start date
					lds_datastore.SetItem(ll_NewRow, 6, ls_code)
					ls_code = String(ldw_datawindow.GetItemDate(ll_count, 7)) //end date
					lds_datastore.SetItem(ll_NewRow, 7, ls_code)
					ll_code10 = ldw_datawindow.GetitemNumber(ll_count, 8) //seq no
					lds_datastore.SetItem(ll_NewRow, 12, ll_code10)
					ls_code = String(ldw_datawindow.GetItemString(ll_count, 11)) //pricing_incl_excl
					lds_datastore.SetItem(ll_NewRow, 10, ls_code)
					ls_code = String(ldw_datawindow.GetItemString(ll_count, 12)) //pricing_ind
					lds_datastore.SetItem(ll_NewRow, 11, ls_code)
				Else
					If as_type = 'S' Then //Customer Group
						ls_code = ldw_datawindow.GetItemString(ll_count, 1) //group id
						lds_datastore.SetItem(ll_NewRow, 1, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 2) //group description
						lds_datastore.SetItem(ll_NewRow, 2, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 3) //owner 
						lds_datastore.SetItem(ll_NewRow, 3, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 4) //tonnage rebate credit code
						lds_datastore.SetItem(ll_NewRow, 4, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 5) //include all product group ind
						lds_datastore.SetItem(ll_NewRow, 5, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 6) //exclude ind
						lds_datastore.SetItem(ll_NewRow, 6, ls_code)
						ls_code = String(ldw_datawindow.GetItemDecimal(ll_count, 7)) //rebate rate
						lds_datastore.SetItem(ll_NewRow, 7, ls_code)
						ls_code = String(ldw_datawindow.GetItemDate(ll_count, 8)) //start date
						lds_datastore.SetItem(ll_NewRow, 8, ls_code)
						ls_code = String(ldw_datawindow.GetItemDate(ll_count, 9)) //end date
						lds_datastore.SetItem(ll_NewRow, 9, ls_code)
						ll_code10 = ldw_datawindow.GetItemNumber(ll_count, 10) //seq no
						lds_datastore.SetItem(ll_NewRow, 12, ll_code10)
					Else
						If as_type = 'O' Then  //Location
							ls_code = ldw_datawindow.GetItemString(ll_count, 1)  //location code
							lds_datastore.SetItem(ll_NewRow, 1, ls_code)
							ls_code = ldw_datawindow.GetItemString(ll_count, 2) //location name
							lds_datastore.SetItem(ll_NewRow, 2, ls_code)
							ls_code = ldw_datawindow.GetItemString(ll_count, 3) //location type
		 					lds_datastore.SetItem(ll_NewRow, 3, ls_code)
							ls_code = ldw_datawindow.GetItemString(ll_count, 4)  //include ind
							lds_datastore.SetItem(ll_NewRow, 4, ls_code)							 
							ls_code = String(ldw_datawindow.GetItemDecimal(ll_count, 5)) //rebate rate
							lds_datastore.SetItem(ll_NewRow, 5, ls_code)
							ls_code = String(ldw_datawindow.GetItemDate(ll_count, 6), 'YYYY-MM-DD') //start date
							lds_datastore.SetItem(ll_NewRow, 6, ls_code)
							ls_code = String(ldw_datawindow.GetItemDate(ll_count, 7), 'YYYY-MM-DD') //end date
							lds_datastore.SetItem(ll_NewRow, 7, ls_code)
							ls_code = String(ldw_datawindow.GetItemDate(ll_count, 8), 'YYYY-MM-DD') //ORIGINAL start date
							lds_datastore.SetItem(ll_NewRow, 8, ls_code)							
						Else	
							If as_type = 'C' Then  //Customer  only store 1st 5 fields
								ls_code = ldw_datawindow.GetItemString(ll_count, 1)  //customer number
								lds_datastore.SetItem(ll_NewRow, 1, ls_code)
								ls_code = ldw_datawindow.GetItemString(ll_count, 2) //customer type
								lds_datastore.SetItem(ll_NewRow, 2, ls_code)
								ls_code = ldw_datawindow.GetItemString(ll_count, 3) //vendor id
								lds_datastore.SetItem(ll_NewRow, 3, ls_code)
								ls_code = String(ldw_datawindow.GetItemDate(ll_count, 4)) //start date
								lds_datastore.SetItem(ll_NewRow, 4, ls_code)
								ls_code = String(ldw_datawindow.GetItemDate(ll_count, 5)) //end date
							End If						
						End If	
					End If
				End If
			End If
		End If
	Else
		If as_type = 'V' Then  //Division (VMR)
			ls_code = ldw_datawindow.GetItemString(ll_count, 1) //division code
			lds_datastore.SetItem(ll_NewRow, 1, ls_code)
			ls_code = ldw_datawindow.GetItemString(ll_count, 2) //division description
			lds_datastore.SetItem(ll_NewRow, 2, ls_code)
			ls_code = ldw_datawindow.GetItemString(ll_count, 3) //tonnage rebate credit code
			lds_datastore.SetItem(ll_NewRow, 3, ls_code)
			ls_code = ldw_datawindow.GetItemString(ll_count, 4)  //include ind
			lds_datastore.SetItem(ll_NewRow, 4, ls_code)
		Else
			If as_type = 'G' Then  //Product group (VMR)
				ls_code = ldw_datawindow.GetItemString(ll_count, 1) //group id
				lds_datastore.SetItem(ll_NewRow, 1, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 2) //group description
				lds_datastore.SetItem(ll_NewRow, 2, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 3) //owner
				lds_datastore.SetItem(ll_NewRow, 3, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 4) //tonnage rebate credit code
				lds_datastore.SetItem(ll_NewRow, 4, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 5) //include all volume
				lds_datastore.SetItem(ll_NewRow, 5, ls_code)
				ls_code = ldw_datawindow.GetItemString(ll_count, 6) //include ind
				lds_datastore.SetItem(ll_NewRow, 6, ls_code)
				ls_code = String(ldw_datawindow.GetItemString(ll_count, 11)) //pricing_incl_excl
				lds_datastore.SetItem(ll_NewRow, 10, ls_code)
				ls_code = String(ldw_datawindow.GetItemString(ll_count, 12)) //pricing_ind
				lds_datastore.SetItem(ll_NewRow, 11, ls_code)
			Else
				If as_type = 'P' Then //Product (VMR)
					ls_code = ldw_datawindow.GetItemString(ll_count, 1) //product code
					lds_datastore.SetItem(ll_NewRow, 1, ls_code)
					ls_code = ldw_datawindow.GetItemString(ll_count, 2) //product description
					lds_datastore.SetItem(ll_NewRow, 2, ls_code)
					ls_code = ldw_datawindow.GetItemString(ll_count, 3) //include ind
					lds_datastore.SetItem(ll_NewRow, 3, ls_code)
					ls_code = ldw_datawindow.GetItemString(ll_count, 4) //tonnage rebate credit code
					lds_datastore.SetItem(ll_NewRow, 4, ls_code)
				Else
					If as_type = 'C' Then  //Customer  only store 1st 5 fields
						ls_code = ldw_datawindow.GetItemString(ll_count, 1)  //customer number
						lds_datastore.SetItem(ll_NewRow, 1, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 2) //customer type
						lds_datastore.SetItem(ll_NewRow, 2, ls_code)
						ls_code = ldw_datawindow.GetItemString(ll_count, 3) //vendor id
						lds_datastore.SetItem(ll_NewRow, 3, ls_code)
						ls_code = String(ldw_datawindow.GetItemDate(ll_count, 4)) //start date
						lds_datastore.SetItem(ll_NewRow, 4, ls_code)
						ls_code = String(ldw_datawindow.GetItemDate(ll_count, 5)) //end date
					End If							
				End If
			End If
		End If
	End If
Next

//If (Is_RebateType = 'A') or (is_RebateType = 'C') or (Is_RebateType = 'O') or (Is_RebateType = 'I') Then
If is_RebateOption = 'A' Then
	If as_type = 'P' Then
		lds_DataStore.SetSort("#12 A")  // seq_no number
		lds_DataStore.Sort()
	Else
		If as_type = 'G'  Or as_type = 'S' Then
			lds_DataStore.SetSort("#12 A") // seq_no number
			lds_DataStore.Sort()
		Else
			If as_type = 'O' Then
				//lds_DataStore.SetSort("#1 A, #3 A") // location code- added location type
				lds_DataStore.SetSort("#1 A, #3 A, #6 A") // location code- location type - added start date
				lds_DataStore.Sort()
			Else
				If as_type = 'V'  Then
					//lds_DataStore.SetSort("1 A") 
					lds_DataStore.SetSort("#1 A, #6 A") // added start date
					lds_DataStore.Sort()
				End If
			End If
		End If
	End If
Else
	lds_DataStore.Sort()
End If

string ls_debug
ls_debug = lds_DataStore.describe("Datawindow.Data")

Return True

end function

public function boolean wf_updateproductgroups (string as_type, ref string as_updatestring);Boolean				lb_Return

Long					ll_NewRowCount, &
						ll_OriginalRowCount, &
						ll_NewCount, &
						ll_OriginalCount, &
						ll_row, &
						ll_seq_no, &
						ll_org_seq_no, &
						ll_org_row
						
String			     	ls_NewCode, &
						ls_rebate_ind, &
						ls_OriginalCode, &
						ls_org_rbt_ind, &
						ls_Include_all_volume, &
						ls_exclude_ind, &
						ls_hold_string, &
						ls_incl_excl, &
						ls_pricing_ind

Date					ldt_start_date, ldt_end_date

Decimal				ldc_rebate_rate

lb_Return = False
ll_NewRowCount = tab_1.tabpage_ProductGroup.dw_ProductGroup.RowCount()

//add for Include_all_volume ind.
ls_Include_all_volume = tab_1.tabpage_ProductGroup.dw_include_all_volume.getitemstring(1,1)
// end


If is_RebateOption = 'A' Then
	tab_1.tabpage_ProductGroup.dw_ProductGroup.SetSort("#10 A")  //changed back to #10 - 2 columns removed
	tab_1.tabpage_ProductGroup.dw_ProductGroup.Sort()
Else
	tab_1.tabpage_ProductGroup.dw_ProductGroup.SetSort("#1 A")
	tab_1.tabpage_ProductGroup.dw_ProductGroup.Sort()
End If

If Not IsValid(ids_ProductGroups) Then
	If ll_NewRowCount > 0 Then
		// All New ProductGroups
		For ll_NewCount = 1 to ll_NewRowCount
			ls_NewCode = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'group_id')
			ls_rebate_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'tonnage_rebate_credit')			
			ls_exclude_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'exclude_ind')
			ldc_rebate_rate = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_NewCount, 'rebate_rate')
			ldt_start_date = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_NewCount, 'start_date')
			ldt_end_date = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_NewCount, 'end_date')
			ll_seq_no = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_NewCount, 'seq_no')
			if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_incl_excl')) then
				ls_incl_excl = ' '
				ls_pricing_ind = ' '
			else
				ls_incl_excl = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_incl_excl')
			end if
			if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_ind')) then
				ls_pricing_ind = ' '
			else
				ls_pricing_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_ind')
			end if
			If is_RebateOption = 'A' Then



				as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + &
										+ '~t' + ls_Include_all_volume + '~t' +  ls_exclude_ind + '~t' &
										+ string(ldc_rebate_rate) + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' &
										+ string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
										+ ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' + '~r~n'
			Else
				as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t' + ls_exclude_ind + '~t' +  ls_incl_excl + '~t' + ls_pricing_ind + '~t' + string(ll_seq_no) + '~t' + 'I' + '~r~n'
			End If
		Next	
		Return True
	Else
		Return False
	End If
End If

ll_OriginalRowCount = ids_ProductGroups.RowCount()

// No ProductGroups
If Not ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	Return False
End If

// All New ProductGroups
If ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	For ll_NewCount = 1 to ll_NewRowCount
			ls_NewCode = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'group_id')
			ls_rebate_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'tonnage_rebate_credit')
			ls_exclude_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'exclude_ind')
			ldc_rebate_rate = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemdecimal(ll_NewCount, 'rebate_rate')
			ldt_start_date = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_NewCount, 'start_date')
			ldt_end_date = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_NewCount, 'end_date')
			ll_seq_no = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_NewCount, 'seq_no')
			if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_incl_excl')) then
				ls_incl_excl = ' '
				ls_pricing_ind = ' '
			else
					ls_incl_excl = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_incl_excl')
			end if
			if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_ind')) then
					ls_pricing_ind = ' '
			else
					ls_pricing_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_ind')
			end if		
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t'  &	
									+ ls_exclude_ind + '~t' + string(ldc_rebate_rate) + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
									string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
									+ ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' + '~r~n'									
		Else
			as_UpdateString += as_type + tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString( &
					ll_NewCount, 'group_id') + '~t' + tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString( &
					ll_NewCount, 'tonnage_rebate_credit') + '~t' +  ls_Include_all_volume + '~t' + ls_exclude_ind + '~t' + &
					+ ls_incl_excl + '~t' + ls_pricing_ind + '~t' + &
					string(ll_seq_no) + '~t' +&
					'I' + '~r~n'
		End If
	Next	
	Return True
End If

// Delete All ProductGroups
If Not ll_NewRowCount > 0 And ll_OriginalRowCount > 0 or ib_DeleteHeader Then
	For ll_OriginalCount = 1 to ll_OriginalRowCount

		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ids_ProductGroups.GetItemString(ll_OriginalCount, 1) + &
					'~t' + "X" + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + & 
					String(ids_productgroups.GetItemNumber(ll_OriginalCount, 12)) + '~t' + '~t' + '~t' + 'D' + '~r~n'
		Else

				as_UpdateString += as_type + ids_ProductGroups.GetItemString(ll_OriginalCount, 1) +  &
			  '~t' + "X" + '~t'  + "X" + '~t' + "X"+ '~t' +  ls_incl_excl + '~t' +  ls_pricing_ind + '~t'  +&
			  '~t' +  &
			  'D' + '~r~n'
			
			
		End If
	Next
	Return True
End If

ll_OriginalCount = 1

For ll_NewCount = 1 to ll_NewRowCount

	ls_NewCode = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'group_id')
	ls_rebate_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'tonnage_rebate_credit')
	ls_exclude_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'exclude_ind')	
	//If is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		ldc_rebate_rate = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemdecimal(ll_NewCount, 'rebate_rate')
		ldt_start_date = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_NewCount, 'start_date')
		ldt_end_date = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_NewCount, 'end_date')
		ll_seq_no = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_NewCount, 'seq_no')
		if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_incl_excl')) then
			ls_incl_excl = ' '
			ls_pricing_ind = ' '
		else
			ls_incl_excl = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_incl_excl')
		end if
		if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_ind')) then
			ls_pricing_ind = ' '
		else
			ls_pricing_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_NewCount, 'pricing_ind')
		end if
	End If
// Reached the end of the Original Buffer Insert the rest.

//dmk
	If ll_OriginalCount > ll_OriginalRowCount Then
		//if is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t' + &
									ls_exclude_ind + '~t' + string(ldc_rebate_rate) + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
									string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' + &
									ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' + '~r~n'
		Else
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t' + &
									ls_exclude_ind + '~t' +  ls_incl_excl + '~t' +  ls_pricing_ind + '~t'  + &
									string(ll_OriginalCount - 1) + '~t' +  &
									'I' + '~r~n'
		End If
		lb_Return = True
		Continue
	End If
		
	ls_OriginalCode = ids_ProductGroups.GetItemString(ll_OriginalCount, 1)
	ls_org_rbt_ind = ids_ProductGroups.GetItemString(ll_OriginalCount, 4)
	
	//dmk changed count from 10 to 12
	//If is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		If IsValid(ids_ProductGroups) Then
			string ls_debug
			ls_debug = ids_productgroups.describe("DataWindow.Data")
			ll_org_seq_no = ids_productgroups.GetItemNumber(ll_OriginalCount, 12)
		End If
	End If

// They equal so don't send it to be updated.
	//if is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		If ll_seq_no = ll_org_seq_no Then
			ll_OriginalCount ++
			Continue
		End If
	Else
		If ls_NewCode = ls_OriginalCode then
			ll_OriginalCount ++
			Continue
		End If
	End If
	
	
// This is a new ProductGroup send it to be inserted.
	//If is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
	//dmk
		If ll_seq_no < ll_org_seq_no Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t' + &
									ls_exclude_ind + '~t' + string(ldc_rebate_rate) + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
									string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' + &
									ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'I' + '~r~n'
			lb_Return = True
			Continue
		End If
	Else
		If ls_NewCode < ls_OriginalCode Then
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume+ '~t' + &
									ls_exclude_ind +  '~t' + ls_incl_excl + '~t' +  ls_pricing_ind + '~t' +&
									 string(ll_seq_no) + '~t' +  &
									'I' + '~r~n'
			lb_Return = True
			Continue
		End IF
	End If

// The original was deleted send it to be deleted.	
	//If is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
	If is_RebateOption = 'A' Then
		If ll_seq_no > ll_org_seq_no Then
//dmk
			as_UpdateString += as_type + ls_OriginalCode  + '~t' + "X" + '~t' + ls_Include_all_volume + '~t' + &
									ls_exclude_ind + '~t' + string(ldc_rebate_rate) + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
									string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_org_seq_no) + '~t' + &
									ls_incl_excl + '~t' + ls_pricing_ind + '~t' + 'D' + '~r~n'
			lb_Return = True
			ll_NewCount --
			ll_OriginalCount ++
			Continue
		End IF
	Else
		If ls_NewCode > ls_OriginalCode Then
			as_UpdateString += as_type + ls_OriginalCode  + '~t' + "X" + '~t' + ls_Include_all_volume + '~t' + &
									ls_exclude_ind + '~t' +  ls_incl_excl + '~t' +  ls_pricing_ind + '~t'  + &
									string(ll_org_seq_no) + '~t' +  &
									'D' + '~r~n'
			lb_Return = True
			ll_NewCount --
			ll_OriginalCount ++
			Continue
		End If
	End If
	
Next

//Delete the rest of the Original Buffer

If ll_OriginalCount <= ll_OriginalRowCount Then
  	For ll_OriginalCount = ll_OriginalCount to ll_OriginalRowCount
		//If is_rebatetype = 'A' or is_rebatetype = 'O' or is_rebatetype = 'C' or is_RebateType = 'I' Then
		If is_RebateOption = 'A' Then
			as_UpdateString += as_type + ids_ProductGroups.GetItemString(ll_OriginalCount, 1) + &
					'~t' + "X" + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' +  &
					string(ids_productgroups.GetItemNumber(ll_OriginalCount, 12)) + '~t' + &
					'~t' + '~t' + 'D' + '~r~n'
		Else
			as_UpdateString += as_type + ids_ProductGroups.GetItemString(ll_OriginalCount, 1) +  &
			  '~t' + "X" + '~t'  + "X" + '~t' + "X"+ '~t' +  ls_incl_excl + '~t' +  ls_pricing_ind + '~t'  +&
			  '~t' +  &
			  'D' + '~r~n'

		End If
	Next
	lb_Return = True
End IF

// update productgroup
ll_OriginalRowCount = ids_ProductGroups.RowCount()
ll_row = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetNextModified(ll_row, Primary!)
Do While ll_row > 0
	if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_incl_excl')) then
		ls_incl_excl = ' '
		ls_pricing_ind = ' '
	else
		ls_incl_excl = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_incl_excl')
	end if
	if IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_ind')) then
		ls_pricing_ind = ' '
	else
		ls_pricing_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_ind')
	end if
	If is_RebateOption = 'A' Then
		For ll_org_row = 1 to ll_OriginalRowCount
			
				IF(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_row, 'seq_no') = ids_ProductGroups.GetItemNumber(ll_org_row,12) AND &
					tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'group_id') = ids_ProductGroups.GetItemString(ll_org_row,1) )THEN
				
					IF(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'exclude_ind') <> ids_ProductGroups.GetItemString(ll_org_row,6) OR &
						String(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDecimal(ll_row, 'rebate_rate')) <> ids_ProductGroups.GetItemString(ll_org_row,7) OR &
						String(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_row, 'start_date')) <> ids_ProductGroups.GetItemString(ll_org_row,8) OR &
						String(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_row, 'end_date')) <> ids_ProductGroups.GetItemString(ll_org_row,9) OR &
						tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_incl_excl') <> ids_ProductGroups.GetItemString(ll_org_row,10) OR &
						tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_ind') <> ids_ProductGroups.GetItemString(ll_org_row,11))then 	
                                as_UpdateString += as_type + &
													  tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'group_id')  + '~t' + &
													  tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'tonnage_rebate_credit')  + '~t' + &
													  ls_Include_all_volume  + '~t' + &
													  tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'exclude_ind')  + '~t' + &
												  	 string(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemnumber(ll_row, 'rebate_rate')) + '~t' + &
													 string(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_row, 'start_date'), "YYYY-MM-DD") + '~t' + &
													 string(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemDate(ll_row, 'end_date'), "YYYY-MM-DD") + '~t' + &
													 string(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_row, 'seq_no')) + '~t' +  &
													 ls_incl_excl + '~t' + &
                                                            ls_pricing_ind  + '~t' + &
                                                           'U' + '~r~n'                                                        
                             
					END IF
					
					ll_org_row = ll_OriginalRowCount
				END IF
				
		Next			
	Else
		For ll_org_row = 1 to ll_OriginalRowCount
			
			IF(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'group_id') = ids_ProductGroups.GetItemString(ll_org_row,1) )THEN
			
				IF(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'exclude_ind') <> ids_ProductGroups.GetItemString(ll_org_row,6) OR &
					 tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'tonnage_rebate_credit') <> ids_ProductGroups.GetItemString(ll_org_row,4) OR &
					tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_incl_excl') <> ids_ProductGroups.GetItemString(ll_org_row,10) OR &
					tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'pricing_ind') <> ids_ProductGroups.GetItemString(ll_org_row,11))then 
                                ls_exclude_ind = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'exclude_ind') 
                                as_UpdateString += as_type + &
														  tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'group_id')  + '~t' + &
														  tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_row, 'tonnage_rebate_credit')  + '~t' + &
														  ls_Include_all_volume                                                                          + '~t' + &
														  ls_exclude_ind  + '~t' + &
														  ls_incl_excl + '~t' + &
														 ls_pricing_ind  + '~t' + &
														  string(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemNumber(ll_row, 'seq_no')) + '~t' +  &
														  'U' + '~r~n'

				END IF
				
				ll_org_row = ll_OriginalRowCount
			END IF
			
		Next
					
	End If
	ll_row = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetNextModified(ll_row, Primary!)
	lb_Return = True
Loop
		
Return lb_Return

end function

public function boolean wf_fillrebatedateoption ();Long									ll_temp

String								ls_tutltype, &
									ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Initialize()
iu_ErrorContext.uf_Initialize()

ls_TutlType = 'REBDTOPT'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('rebate_date_option', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

dw_header_display.GetChild('rebate_date_option', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_updatelocations (string as_type, ref string as_updatestring);Boolean				lb_Return
Long					ll_NewRowCount, &
						ll_OriginalRowCount, &
						ll_NewCount, &
						ll_OriginalCount, &
						ll_row, &
						ll_row2
						
String				    ls_NewLocCode, &
						ls_OriginalLocCode, &
						ls_org_exclude_ind, &
						ls_orig_location_type, &
						ls_orig_end_date, &
						ls_new_exclude_ind, &
						ls_location_type, &
						ls_rebate_rate, &
						ls_start_date, &
						ls_end_date, &
						ls_orig_start_date, &
						ls_v_start_date, &
						ls_ds_start_date, &
						ls_ds_end_date, &
						ls_ds_rebate_rate, &
						ls_ORIGINAL_start_date, &
						ls_ORIGINAL_ds_start_date, &
						ls_ORIGINAL_dw_start_date
						
Date					dt_start_date, dt_end_date, &	
						ld_orig_start_date, &
						ld_orig_end_date, &
						ld_ORIGINAL_start_date, &
						ld_ORIGINAL_ds_start_date

Decimal				ldc_rebate_rate

dwItemStatus			lis_temp
DataWindow				ldw_datawindow
u_projectfunctions	lu_projectfunctions

lb_Return = False

ls_v_start_date = '0001-01-01'

ll_NewRowCount = tab_1.tabpage_location.dw_location.RowCount()

If Not IsValid(ids_locations) Then
	If ll_NewRowCount > 0 Then
		// All New locations
		For ll_NewCount = 1 to ll_NewRowCount				  
			as_UpdateString += as_type + &	 					
					tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'location_code') + '~t' + &
					tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'location_type') + '~t' + &
						tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'exclude_ind') + '~t' + &
					string(tab_1.tabpage_location.dw_location.GetItemnumber(ll_NewCount, 'rebate_rate')) + '~t' + &
					string((tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'start_date')), "YYYY-MM-DD") + '~t' + &
					string((tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'end_date')), "YYYY-MM-DD") + '~t' + &	
					ls_v_start_date + '~t' + &						
					'I' + '~r~n'
			Next	
		Return True
	Else
		Return False
	End If
End If

ll_OriginalRowCount = ids_locations.RowCount()

// No locations
If Not ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	Return False
End If

// All New locations
If ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	For ll_NewCount = 1 to ll_NewRowCount
		as_UpdateString += as_type + &
					tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'location_code') + '~t' + &
					tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'location_type') + '~t' + &
					tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'exclude_ind') + '~t' + &
					string(tab_1.tabpage_location.dw_location.GetItemnumber(ll_NewCount, 'rebate_rate')) + '~t' + &
					string((tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'start_date')), "YYYY-MM-DD") + '~t' + &
					string((tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'end_date')), "YYYY-MM-DD") + '~t' + &	
					ls_v_start_date + '~t' + &					
					'I' + '~r~n'
	Next	
	Return True
End If

string ls_debug
ls_debug = ids_locations.describe("DataWindow.Data")
string ls_debug_page
ls_debug_page = tab_1.tabpage_location.dw_location.describe("DataWindow.Data")

// Delete All locations
If Not ll_NewRowCount > 0 And ll_OriginalRowCount > 0 Then
	For ll_OriginalCount = 1 to ll_OriginalRowCount
		ls_OriginalLocCode = ids_locations.GetItemString(ll_OriginalCount, 1)
		ls_orig_location_type= ids_locations.GetItemString(ll_OriginalCount,3)
		ls_orig_start_date =  ids_locations.GetItemString(ll_OriginalCount, 6)
		ld_orig_start_date = Date(ls_orig_start_date)
		ls_orig_start_date = String(ld_orig_start_date, 'YYYY-MM-DD')
		
		ls_ORIGINAL_ds_start_date =  ids_locations.GetItemString(ll_OriginalCount, 8)
		ld_ORIGINAL_ds_start_date = Date(ls_ORIGINAL_ds_start_date)
		ls_ORIGINAL_ds_start_date = String(ld_ORIGINAL_ds_start_date, 'YYYY-MM-DD')		
						
		as_UpdateString += as_type + ls_OriginalLocCode + '~t' + ls_orig_location_type + '~t' + "X" + '~t' + "X" + '~t' + ls_orig_start_date + '~t' + "X" + '~t' + ls_ORIGINAL_ds_start_date + '~t' + 'D' + '~r~n'
	Next
	Return True
End If

ll_OriginalCount = 1

For ll_NewCount = 1 to ll_NewRowCount

	ls_NewLocCode = tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'location_code')	
	ls_location_type = tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'location_type')
	ls_new_exclude_ind = tab_1.tabpage_location.dw_location.GetItemString(ll_NewCount, 'exclude_ind')
     ls_rebate_rate = String(tab_1.tabpage_location.dw_location.GetItemnumber(ll_NewCount, 'rebate_rate'))
	ls_start_date = String(tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'start_date'), 'YYYY-MM-DD')
	ls_end_date = String(tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'end_date'), 'YYYY-MM-DD') 
	ls_ORIGINAL_start_date = String(tab_1.tabpage_location.dw_location.GetItemDate(ll_NewCount, 'original_start_date'), 'YYYY-MM-DD')

// Reached the end of the Original Buffer Insert the rest.
	If ll_OriginalCount > ll_OriginalRowCount Then		
		as_UpdateString += as_type + ls_NewLocCode + '~t' + ls_location_type + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' + ls_start_date + '~t' + 'I' + '~r~n'
		lb_Return = True
		Continue
	End If
	
	// original values in datastore
	ls_OriginalLocCode = ids_locations.GetItemString(ll_OriginalCount, 1)
	ls_org_exclude_ind = ids_locations.GetItemString(ll_OriginalCount, 4)
	ls_orig_location_type = ids_locations.GetItemString(ll_OriginalCount,3)
	
	ls_orig_start_date = ids_locations.GetItemString(ll_OriginalCount, 6)
	ld_orig_start_date = Date(ls_orig_start_date)
	ls_orig_start_date = String(ld_orig_start_date, 'YYYY-MM-DD')
	
	ls_orig_end_date = ids_locations.GetItemString(ll_OriginalCount, 7)
	ld_orig_end_date = Date(ls_orig_end_date)
	ls_orig_end_date = String(ld_orig_end_date, 'YYYY-MM-DD')
	
	ls_ORIGINAL_ds_start_date = ids_locations.GetItemString(ll_OriginalCount, 8)
	ld_ORIGINAL_ds_start_date = Date(ls_ORIGINAL_ds_start_date)
	ls_ORIGINAL_ds_start_date = String(ld_ORIGINAL_ds_start_date, 'YYYY-MM-DD')
	
// They equal so don't send it to be updated.
	If ls_NewLocCode = ls_OriginalLocCode  and ls_location_type = ls_orig_location_type and ls_ORIGINAL_start_date = ls_ORIGINAL_ds_start_date then
		ll_OriginalCount ++
		Continue
	End If
	
//new scenario
//start dates do not equal
If ls_NewLocCode = ls_OriginalLocCode Then 	
      If ls_ORIGINAL_start_date < ls_ORIGINAL_ds_start_date Then
         //New location - Insert
		 as_UpdateString += as_type + ls_NewLocCode + '~t' + ls_location_type + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' + ls_start_date + '~t' + 'I' + '~r~n'
      Else
          //original deleted - send it to be deleted - 
	    //ls_ORIGINAL_start_date > ls_ORIGINAL_ds_start_date
		as_UpdateString += as_type + ls_OriginalLocCode + '~t' + ls_orig_location_type + '~t' + "X" + '~t' + "X" + '~t' + ls_orig_start_date + '~t' + "X" + '~t' + ls_ORIGINAL_ds_start_date + '~t' + 'D' + '~r~n'
         ll_NewCount --
	     ll_OriginalCount ++
     End If
     lb_Return = True
    Continue
End If			
	
// This is a new location send it to be inserted.
//	If ls_NewLocCode < ls_OriginalLocCode or ls_location_type < ls_orig_location_type Then		
	If ls_NewLocCode < ls_OriginalLocCode  Then		
		as_UpdateString += as_type + ls_NewLocCode + '~t' + ls_location_type + '~t' + ls_new_exclude_ind + '~t' + ls_rebate_rate + '~t' + ls_start_date + '~t' + ls_end_date + '~t' + ls_start_date + '~t' + 'I' + '~r~n'
		lb_Return = True
		Continue
	End If
// The original was deleted send it to be deleted.	
//	If ls_NewLocCode > ls_OriginalLocCode or ls_location_type > ls_orig_location_type Then	
	If ls_NewLocCode > ls_OriginalLocCode Then	
		as_UpdateString += as_type + ls_OriginalLocCode  + '~t' + ls_orig_location_type + '~t' + "X" + '~t' + "X" + '~t' + ls_orig_start_date + '~t' + ls_ORIGINAL_ds_start_date + '~t' + '~t' + 'D' + '~r~n'
		lb_Return = True
		ll_NewCount --
		ll_OriginalCount ++
		Continue
	End IF
Next

//Delete the rest of the Original Buffer
If ll_OriginalCount <= ll_OriginalRowCount Then
	For ll_OriginalCount = ll_OriginalCount to ll_OriginalRowCount
		ls_orig_start_date = ids_locations.GetItemString(ll_OriginalCount, 6)
		ld_orig_start_date = Date(ls_orig_start_date)
		ls_orig_start_date = String(ld_orig_start_date, 'YYYY-MM-DD')
		as_UpdateString += as_type + ids_locations.GetItemString(ll_OriginalCount, 1) + &
				'~t'+   ids_locations.GetItemString(ll_OriginalCount,3) + '~t' + "X" + '~t' + "X" + '~t' +ls_orig_start_date + '~t' + "X" + '~t' + ls_ORIGINAL_ds_start_date + '~t' + 'D' + '~r~n'
	Next
	lb_Return = True
End IF

// update location
ll_row = tab_1.tabpage_location.dw_location.GetNextModified(0, Primary!)
Do While ll_row > 0
	ls_orig_start_date = '0001-01-01'
	ls_ORIGINAL_dw_start_date = String(tab_1.tabpage_location.dw_location.GetItemDate(ll_row, 'original_start_date'), "YYYY-MM-DD")
//	if location found in original datastore, use the start date from the original datastore, otherwise set to '0001-01-01'		
	if IsValid(ids_locations) then
		ll_OriginalRowCount = ids_locations.RowCount()
		For ll_row2 = 1 to ll_OriginalRowCount
			
			ls_ds_start_date = ids_locations.GetItemString(ll_row2, 6)
			ld_orig_start_date = Date(ls_ds_start_date)
			ls_ds_start_date = String(ld_orig_start_date, 'YYYY-MM-DD')
			
			ls_ds_end_date = ids_locations.GetItemString(ll_row2, 7)
			ld_orig_end_date = Date(ls_ds_end_date)
			ls_ds_end_date = String(ld_orig_end_date, 'YYYY-MM-DD')
			
			ls_ORIGINAL_ds_start_date = ids_locations.GetItemString(ll_row2, 8)
			ld_ORIGINAL_ds_start_date = Date(ls_ORIGINAL_ds_start_date)
			ls_ORIGINAL_ds_start_date = String(ld_ORIGINAL_ds_start_date, 'YYYY-MM-DD')
						
			ls_ds_rebate_rate =  ids_locations.GetItemString(ll_row2, 5)
			ldc_rebate_rate = Dec(ls_ds_rebate_rate)
		//ls_ORIGINAL_ds_start_date = String(tab_1.tabpage_location.dw_location.GetItemDate(ll_row, 'original_start_date'), "YYYY-MM-DD")  and &	
  		    //add check for original start date - if 01/01/0001 then it is already being sent as an insert
			if ids_locations.GetItemString(ll_row2, 1) = tab_1.tabpage_location.dw_location.GetItemString(ll_row, 1) and &
			   ids_locations.GetItemString(ll_row2, 3) = tab_1.tabpage_location.dw_location.GetItemString(ll_row, 3)  and &
				ls_ORIGINAL_ds_start_date = ls_ORIGINAL_dw_start_date and &
				(NOT ls_ORIGINAL_dw_start_date = '0001-01-01')  Then
				If (NOT ls_ds_start_date =  string((tab_1.tabpage_location.dw_location.GetItemDate(ll_row, 'start_date')), "YYYY-MM-DD") or &
	  				NOT ls_ds_end_date =  string((tab_1.tabpage_location.dw_location.GetItemDate(ll_row, 'end_date')), "YYYY-MM-DD") or &
					NOT ldc_rebate_rate =  tab_1.tabpage_location.dw_location.GetItemNumber(ll_row, 'rebate_rate')) Then
				  	 as_UpdateString += as_type + &			
						tab_1.tabpage_location.dw_location.GetItemString( ll_row, 'location_code') + '~t' + &
						tab_1.tabpage_location.dw_location.GetItemString(ll_row, 'location_type') + '~t' + &
						tab_1.tabpage_location.dw_location.GetItemString(ll_row, 'exclude_ind') + '~t' + &
						string(tab_1.tabpage_location.dw_location.GetItemnumber(ll_row, 'rebate_rate')) + '~t' + &
						string((tab_1.tabpage_location.dw_location.GetItemDate(ll_row, 'start_date')), "YYYY-MM-DD") + '~t' + &	
						string((tab_1.tabpage_location.dw_location.GetItemDate(ll_row, 'end_date')), "YYYY-MM-DD") + '~t' + &
						ls_ORIGINAL_ds_start_date + '~t' + &
						'U' + '~r~n'
					End If
			end if
		Next
	end if
	ll_row = tab_1.tabpage_location.dw_location.GetNextModified(ll_row, Primary!)
	lb_Return = True
Loop

Return lb_Return

end function

public function boolean wf_updatecustomergroups (string as_type, ref string as_updatestring);Boolean				lb_Return

Long					ll_NewRowCount, &
						ll_OriginalRowCount, &
						ll_NewCount, &
						ll_OriginalCount, &
						ll_row, &
						ll_seq_no, &
						ll_org_seq_no, &
						ll_org_row
						
String			     	ls_NewCode, &
						ls_rebate_ind, &
						ls_OriginalCode, &
						ls_org_rbt_ind, &
						ls_Include_all_volume, &
						ls_exclude_ind, &
						ls_hold_string, &
						ls_rebate_rate

Date					ldt_start_date, ldt_end_date

Decimal				ldc_rebate_rate, rebate_rate

// Customer Group tab is hidden for VMR so is_RebateOption is always = 'A'

lb_Return = False
ll_NewRowCount = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.RowCount()

// Must be in this order for the update to work.
tab_1.tabpage_CustomerGroup.dw_CustomerGroup.SetSort("#10 A") 
tab_1.tabpage_CustomerGroup.dw_CustomerGroup.Sort()

If Not IsValid(ids_CustomerGroups) Then
	If ll_NewRowCount > 0 Then
		// All New CustomerGroups
		For ll_NewCount = 1 to ll_NewRowCount
			
			If is_RebateType = 'Q' Then
				ls_rebate_ind = ''
				ls_rebate_rate = '0'
			Else
				ls_rebate_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'tonnage_rebate_credit')	
				ls_rebate_rate = String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_NewCount, 'rebate_rate'))
			End If
			
			ls_NewCode = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'group_id')
			ls_exclude_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'exclude_ind')
			ldt_start_date = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_NewCount, 'start_date')
			ldt_end_date = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_NewCount, 'end_date')
			ll_seq_no = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_NewCount, 'seq_no')
				
			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + &
										+ '~t' + ls_Include_all_volume + '~t' +  ls_exclude_ind + '~t' &
										+ ls_rebate_rate + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' &
										+ string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
										+ 'I' + '~r~n'
		Next	
		Return True
	Else
		Return False
	End If
End If

ll_OriginalRowCount = ids_CustomerGroups.RowCount()

// No CustomerGroups
If Not ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	Return False
End If

// All New CustomerGroups
If ll_NewRowCount > 0 And Not ll_OriginalRowCount > 0 Then
	For ll_NewCount = 1 to ll_NewRowCount
		
			If is_RebateType = 'Q' Then
				ls_rebate_ind = ''
				ls_rebate_rate = '0'
			Else
				ls_rebate_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'tonnage_rebate_credit')	
				ls_rebate_rate = String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_NewCount, 'rebate_rate'))
			End If		
		
			ls_NewCode = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'group_id')
			ls_exclude_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'exclude_ind')
			ldt_start_date = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_NewCount, 'start_date')
			ldt_end_date = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_NewCount, 'end_date')
			ll_seq_no = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_NewCount, 'seq_no')

			as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t'  &	
									+ ls_exclude_ind + '~t' + ls_rebate_rate + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
									string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' &
									+ 'I' + '~r~n'									
	Next	
	Return True
End If

// Delete All CustomerGroups
If Not ll_NewRowCount > 0 And ll_OriginalRowCount > 0 or ib_DeleteHeader Then
	For ll_OriginalCount = 1 to ll_OriginalRowCount
		as_UpdateString += as_type + ids_CustomerGroups.GetItemString(ll_OriginalCount, 1) + &
				'~t' + "X" + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + & 
				String(ids_CustomerGroups.GetItemNumber(ll_OriginalCount, 12)) + '~t' + 'D' + '~r~n'
	Next
	Return True
End If

ll_OriginalCount = 1

For ll_NewCount = 1 to ll_NewRowCount
	
	If is_RebateType = 'Q' Then
		ls_rebate_ind = ''
		ls_rebate_rate = '0'
	Else
		ls_rebate_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'tonnage_rebate_credit')	
		ls_rebate_rate = String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_NewCount, 'rebate_rate'))
	End If		

	ls_NewCode = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'group_id')
	ls_exclude_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_NewCount, 'exclude_ind')	
	ldt_start_date = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_NewCount, 'start_date')
	ldt_end_date = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_NewCount, 'end_date')
	ll_seq_no = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_NewCount, 'seq_no')

// Reached the end of the Original Buffer Insert the rest.
	If ll_OriginalCount > ll_OriginalRowCount Then
		as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t' + &
								ls_exclude_ind + '~t' + ls_rebate_rate + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
								string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' + &
								'I' + '~r~n'
		lb_Return = True
		Continue
	End If
		
	ls_OriginalCode = ids_CustomerGroups.GetItemString(ll_OriginalCount, 1)
	ls_org_rbt_ind = ids_CustomerGroups.GetItemString(ll_OriginalCount, 4)
	
	If IsValid(ids_CustomerGroups) Then
		string ls_debug
		ls_debug = ids_CustomerGroups.describe("DataWindow.Data")
		ll_org_seq_no = ids_CustomerGroups.GetItemNumber(ll_OriginalCount, 12)
	End If

// They equal so don't send it to be updated.
	If ll_seq_no = ll_org_seq_no Then
		ll_OriginalCount ++
		Continue
	End If	
	
// This is a new CustomerGroup send it to be inserted.

	If ll_seq_no < ll_org_seq_no Then
		as_UpdateString += as_type + ls_NewCode + '~t' + ls_rebate_ind + '~t' + ls_Include_all_volume + '~t' + &
								ls_exclude_ind + '~t' + ls_rebate_rate + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
								string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_seq_no) + '~t' + &
								'I' + '~r~n'
		lb_Return = True
		Continue
	End If

// The original was deleted send it to be deleted.	
	If ll_seq_no > ll_org_seq_no Then
		as_UpdateString += as_type + ls_OriginalCode  + '~t' + "X" + '~t' + ls_Include_all_volume + '~t' + &
								ls_exclude_ind + '~t' + ls_rebate_rate + '~t' + string((ldt_start_date), "YYYY-MM-DD") + '~t' + &
								string((ldt_end_date), "YYYY-MM-DD") + '~t' + string(ll_org_seq_no) + '~t' + &
								'D' + '~r~n'
		lb_Return = True
		ll_NewCount --
		ll_OriginalCount ++
		Continue
	End If	
Next

//Delete the rest of the Original Buffer
If ll_OriginalCount <= ll_OriginalRowCount Then
  	For ll_OriginalCount = ll_OriginalCount to ll_OriginalRowCount
		as_UpdateString += as_type + ids_CustomerGroups.GetItemString(ll_OriginalCount, 1) + &
				'~t' + "X" + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' +  &
				String(ids_CustomerGroups.GetItemNumber(ll_OriginalCount, 12)) + '~t' + &
				'D' + '~r~n'
	Next
	lb_Return = True
End IF

// update CustomerGroup
ll_OriginalCount = ids_CustomerGroups.RowCount()
ll_row = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetNextModified(ll_row, Primary!)
Do While ll_row > 0	
	For ll_org_row = 1 to ll_OriginalRowCount
		IF(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_row, 'seq_no') = ids_CustomerGroups.GetItemNumber(ll_org_row,12) AND &
			tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_row, 'group_id') = ids_CustomerGroups.GetItemString(ll_org_row,1)) Then
		
			IF(	tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_row, 'exclude_ind') <> ids_CustomerGroups.GetItemString(ll_org_row,6) OR &
				String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDecimal(ll_row, 'rebate_rate')) <> ids_CustomerGroups.GetItemString(ll_org_row,7) OR &
				String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_row, 'start_date')) <> ids_CustomerGroups.GetItemString(ll_org_row,8) OR &
				String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_row, 'end_date')) <> ids_CustomerGroups.GetItemString(ll_org_row,9) 	)then 

					If is_RebateType = 'Q' Then
						ls_rebate_ind = ''
						ls_rebate_rate = '0'
					Else
						ls_rebate_ind = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_row, 'tonnage_rebate_credit')	
						ls_rebate_rate = String(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_row, 'rebate_rate'))
					End If		
					
					as_UpdateString += as_type + &
												tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_row, 'group_id')  + '~t' + &
												ls_rebate_ind  + '~t' + &
												ls_Include_all_volume																+ '~t' + &
												tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemString(ll_row, 'exclude_ind')  + '~t' + &
												ls_rebate_rate + '~t' + &
												string(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_row, 'start_date'), "YYYY-MM-DD") + '~t' + &
												string(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemDate(ll_row, 'end_date'), "YYYY-MM-DD") + '~t' + &
												string(tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetItemNumber(ll_row, 'seq_no')) + '~t' +  &
												'U' + '~r~n'		
					ll_org_row = ll_OriginalRowCount
			END IF	
			
		End IF
	Next
		

	ll_row = tab_1.tabpage_CustomerGroup.dw_CustomerGroup.GetNextModified(ll_row, Primary!)
	lb_Return = True
Loop
		
Return lb_Return

end function

public function boolean wf_fillheadcurr ();Long									ll_temp
String								ls_tutltype, ls_TransferTypeString
DataWindowChild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'CURRCODE'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)
iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

is_dddw_currency_string = ls_TransferTypeString
tab_1.tabpage_header.dw_header.GetChild('currency_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

dw_header_display.GetChild('currency_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillheaduom ();Long									ll_temp
String								ls_tutltype, ls_TransferTypeString
DataWindowChild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

//dmk change to rebuom from puom
ls_TutlType = 'REBUOM'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)
iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

is_dddw_uom_string = ls_TransferTypeString
tab_1.tabpage_header.dw_header.GetChild('unit_of_measure', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

dw_header_display.GetChild('unit_of_measure', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_check_vendor ();Long		ll_sub, &
			ll_RowCount
			
String		ls_rebate_type, &
			ls_type_short_desc

Boolean	lb_rebate_type_found

u_string_functions	lu_string_functions

wf_dbconnection()

ls_rebate_type = is_RebateType

SELECT tutltypes.type_short_desc
INTO :ls_type_short_desc  
FROM tutltypes

WHERE ( tutltypes.record_type = 'REBASINT' ) AND  
			( tutltypes.type_code = :ls_rebate_type);

If SQLCA.SQLCode = 00 THEN
	lb_rebate_type_found = True
Else
	lb_rebate_type_found = False
END IF

If lb_rebate_type_found or (is_RebateType = 'V') then

	ll_RowCount = tab_1.tabpage_customer.dw_customer.RowCount()
	
	For ll_sub = 1 to ll_Rowcount
		If lu_string_functions.nf_isempty(tab_1.tabpage_customer.dw_customer.GetItemString(ll_sub, "vendor_id")) Then
			MessageBox("Vendor ID Required", "Vendor IDs are required for this rebate type")
			Return False
		End If
	Next	

End If		

Return True
end function

public function boolean wf_fillbusiness ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Initialize()
iu_ErrorContext.uf_Initialize()

ls_TutlType = 'REBBUSUN'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('business_unit', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

public function boolean wf_fillsaleslocation ();Long									ll_temp

String									ls_tutltype, &
										ls_SalesLocationString
										
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'SCREGION'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_SalesLocationString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('sales_location', ldwc_child)
ldwc_child.Reset()

ls_SalesLocationString = 'ALL         ' + '~t' + 'ALL     ' + '~t' + 'SCREGION' + '~t' + 'ALL SERVICE CENTERS                   ' + '~r~n' + ls_SalesLocationString
ll_temp = ldwc_child.ImportString(ls_SalesLocationString)

//dw_header_display.GetChild('rebate_type', ldwc_child)
//ldwc_child.Reset()
//ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

public function boolean wf_fillloadinginstruction ();Long									ll_temp

String									ls_tutltype, &
										ls_LoadingInstructionString
										
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'LOADINST'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_LoadingInstructionString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

tab_1.tabpage_header.dw_header.GetChild('loading_instruction', ldwc_child)
ldwc_child.Reset()

ll_temp = ldwc_child.ImportString(ls_LoadingInstructionString)

//dw_header_display.GetChild('rebate_type', ldwc_child)
//ldwc_child.Reset()
//ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

event closequery;tab_1.tabpage_header.dw_header.AcceptText()
tab_1.tabpage_detail.dw_detail.AcceptText()
tab_1.tabpage_customer.dw_customer.AcceptText()
tab_1.tabpage_customergroup.dw_customergroup.AcceptText()
tab_1.tabpage_division.dw_division.AcceptText()
tab_1.tabpage_product.dw_product.AcceptText()
tab_1.tabpage_productgroup.dw_productgroup.AcceptText()
tab_1.tabpage_location.dw_location.AcceptText()

If tab_1.tabpage_header.dw_header.ModifiedCount() + tab_1.tabpage_header.dw_header.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 1
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_detail.dw_detail.ModifiedCount() + tab_1.tabpage_detail.dw_detail.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 2
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_customergroup.dw_customergroup.ModifiedCount() + tab_1.tabpage_customergroup.dw_customergroup.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 3
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_customer.dw_customer.ModifiedCount() + tab_1.tabpage_customer.dw_customer.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 4
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_division.dw_division.ModifiedCount() + tab_1.tabpage_division.dw_division.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 5
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_productgroup.dw_productgroup.ModifiedCount() + tab_1.tabpage_productgroup.dw_productgroup.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 6
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_product.dw_product.ModifiedCount() + tab_1.tabpage_product.dw_product.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 7
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

If tab_1.tabpage_location.dw_location.ModifiedCount() + tab_1.tabpage_location.dw_location.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes at this time?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
			tab_1.SelectedTab = 8
			Return 1
		End If

	CASE 2	// Do not save changes
		Return 0
	CASE 3	// Cancel the closing of window
		Return 1
	END CHOOSE
End If

Return 0

end event

on w_rebatemain.create
int iCurrent
call super::create
this.dw_rebate_insert_items=create dw_rebate_insert_items
this.dw_header_display=create dw_header_display
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_rebate_insert_items
this.Control[iCurrent+2]=this.dw_header_display
this.Control[iCurrent+3]=this.tab_1
end on

on w_rebatemain.destroy
call super::destroy
destroy(this.dw_rebate_insert_items)
destroy(this.dw_header_display)
destroy(this.tab_1)
end on

event open;call super::open;DataWindowChild			ldwc_header, &
								ldwc_display


iw_parent = This
iw_This = This
is_Title = This.Title
is_RebateCode = ' '
ib_DeleteHeader = False
ib_ReInquire = False
ib_exclude_ind_div = false
ib_exclude_ind_grp = false
ib_save_flag = false
il_selected_ind_max = 15

tab_1.tabpage_header.dw_header.ShareData(dw_header_display)

tab_1.SelectedTab = 1


end event

event ue_delete;GetFocus().TriggerEvent('ue_Delete')

end event

event ue_save;String					    ls_HeaderString,&
							ls_DetailString, &
							ls_AppName, &
							ls_WindowName, &
							ls_incl_excl, &
							ls_temp
							
Long						ll_rowcount, &
							ll_count
							
u_ParameterStack		lu_ParameterStack
u_String_Functions	lu_strings

SetPointer(HourGlass!)

ib_save_flag = true

ls_temp = tab_1.tabpage_header.dw_header.object.datawindow.data

ib_changes = False
IF tab_1.tabpage_header.dw_header.AcceptText() = -1 THEN 
	Tab_1.SelectedTab = 1
	tab_1.tabpage_header.dw_header.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_detail.dw_detail.AcceptText() = -1 THEN 
	ib_changes = False
	Tab_1.SelectedTab = 2
	tab_1.tabpage_detail.dw_detail.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_customergroup.dw_customergroup.AcceptText() = -1  THEN 
	ib_changes = False
	Tab_1.SelectedTab = 3
	tab_1.tabpage_customergroup.dw_customergroup.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_customer.dw_customer.AcceptText() = -1 THEN 
	ib_changes = False
	Tab_1.SelectedTab = 4
	tab_1.tabpage_customer.dw_customer.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_division.dw_division.AcceptText() = -1 THEN 
	ib_changes = False
	Tab_1.SelectedTab = 5
	tab_1.tabpage_division.dw_division.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_productgroup.dw_productgroup.AcceptText() = -1  THEN 
	ib_changes = False
	Tab_1.SelectedTab = 6
	tab_1.tabpage_productgroup.dw_productgroup.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_product.dw_product.AcceptText() = -1  THEN 
	ib_changes = False
	Tab_1.SelectedTab = 7
	tab_1.tabpage_product.dw_product.SetFocus()
	Return False
End IF
ib_changes = False
IF tab_1.tabpage_location.dw_location.AcceptText() = -1  THEN 
	ib_changes = False
	Tab_1.SelectedTab = 8
	tab_1.tabpage_location.dw_location.SetFocus()
	Return False
End IF

This.SetRedraw(False)

If Not wf_BuildHeaderString(ls_headerstring) Then 
	This.SetRedraw(True)
	Return False
End If

If Not wf_check_vendor() Then 
	This.SetRedraw(True)
	Return False
End If

If Not ib_DeleteHeader Then
	ls_DetailString = ''
	If Not wf_BuildDetailString(ls_detailstring) Then
		This.SetRedraw(True)
		Return False
	End If
	If Not wf_BuildCustomerString(ls_detailstring) Then
		tab_1.tabpage_Customer.dw_Customer.Setfocus()
		tab_1.tabpage_Customer.dw_Customer.SetColumn('vendor_id')
		tab_1.tabpage_Customer.dw_Customer.SelectText(1, 1000)
		This.SetRedraw(True)
		Return False
	End If

//Build Division String
	ib_UpdatedDivisions = wf_UpdateDivisions('V', ls_detailstring)
	
//Build Product String
	ib_UpdatedProducts = wf_UpdateProducts('P', ls_detailstring)	
	//Check for pricing inds
	//Ind required if incl or excl is selected
//dmk use rebate option
//	If tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'A' or &
//		tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'C' or &
//	   tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'O' or &
//		tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'I' Then
	If is_RebateOption = 'A' Then
		ll_RowCount = tab_1.tabpage_Product.dw_Product.RowCount()
		For ll_Count = 1 to ll_RowCount
			If NOT IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_Count, 'pricing_incl_excl')) Then
				ls_incl_excl = tab_1.tabpage_Product.dw_Product.GetItemString(ll_Count, 'pricing_incl_excl')
				If ls_incl_excl = 'I' or ls_incl_excl = 'E' Then
					If IsNull(tab_1.tabpage_Product.dw_Product.GetItemString(ll_Count, 'pricing_ind')) or (Len(trim(tab_1.tabpage_Product.dw_Product.GetItemString(ll_Count, 'pricing_ind'))) = 0) Then
						Tab_1.SelectedTab = 7
						tab_1.tabpage_Product.dw_Product.Setfocus()
						tab_1.tabpage_Product.dw_Product.ScrollToRow(ll_Count)
						tab_1.tabpage_Product.dw_Product.SetColumn('pricing_ind')
						tab_1.tabpage_Product.dw_Product.SelectText(1, 1000)
						SetMicroHelp('Pricing Indicator is a required field when Incl/Excl indicator is selected')
						This.SetRedraw(True)
						Return False
					End If
				End If												
			End If
		Next
	End If

//Build Product Group String
	ib_UpdatedProductGroups = wf_UpdateProductGroups('G', ls_detailstring)
	//Check for pricing inds
	//Ind required if incl or excl is selected
// dmk use rebate option 
//	If tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'A' or &
//		tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'C' or &
//	   tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'O' or &
//		tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'I' Then
//	If is_RebateOption = 'A' Then
	ll_RowCount = tab_1.tabpage_ProductGroup.dw_ProductGroup.RowCount()
	For ll_Count = 1 to ll_RowCount
		If NOT IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_Count, 'pricing_incl_excl')) Then
			ls_incl_excl = tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_Count, 'pricing_incl_excl')
			If ls_incl_excl = 'I' or ls_incl_excl = 'E' Then
				If IsNull(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_Count, 'pricing_ind')) or (Len(trim(tab_1.tabpage_ProductGroup.dw_ProductGroup.GetItemString(ll_Count, 'pricing_ind'))) = 0) Then
					Tab_1.SelectedTab = 6
					tab_1.tabpage_ProductGroup.dw_ProductGroup.Setfocus()
					tab_1.tabpage_ProductGroup.dw_ProductGroup.ScrollToRow(ll_Count)
					tab_1.tabpage_ProductGroup.dw_ProductGroup.SetColumn('pricing_ind')
					tab_1.tabpage_ProductGroup.dw_ProductGroup.SelectText(1, 1000)
					SetMicroHelp('Pricing Indicator is a required field when Incl/Excl indicator is selected')
					This.SetRedraw(True)
					Return False
				End If
			End If												
		End If
	Next
//	End If
	
	//Build Location String
	ib_UpdatedLocations = wf_UpdateLocations('O', ls_detailstring)
	
	//Build Customer Group String
	ib_UpdatedCustomerGroups = wf_UpdateCustomerGroups('S', ls_detailstring)

	If lu_strings.nf_IsEmpty(ls_DetailString) And wf_GetUpdateInd(1, tab_1.tabpage_header.dw_header) = ' ' Then 
		SetMicroHelp('No update necessary')
		This.SetRedraw(True)
		Return False
	Else 
		If lu_strings.nf_IsEmpty(ls_DetailString) Then
			ls_DetailString = ' '
		End IF
	End If
End IF

SetMicroHelp("Wait... Updating the Database")

ls_WindowName = 'RbtMain'

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_AppName)
lu_ParameterStack.uf_Push('string', ls_WindowName)
lu_ParameterStack.uf_Push('string', ls_HeaderString)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_RebateDataAccess.uf_Update( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('string', ls_HeaderString)
lu_ParameterStack.uf_Pop('string', ls_AppName)
lu_ParameterStack.uf_Pop('string', ls_WindowName)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

This.SetRedraw(True)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

IF ib_DeleteHeader Then
	tab_1.tabpage_header.dw_header.Event ue_new()
	tab_1.tabpage_detail.dw_detail.Reset()
	tab_1.tabpage_detail.dw_detail.Event ue_new()
	tab_1.tabpage_customergroup.dw_customergroup.Reset()
	tab_1.tabpage_customer.dw_customer.Reset()
	tab_1.tabpage_division.dw_division.Reset()
	tab_1.tabpage_productgroup.dw_productgroup.Reset()
	tab_1.tabpage_product.dw_product.Reset()
	tab_1.tabpage_location.dw_location.Reset()
	If IsValid(ids_Divisions) Then ids_Divisions.Reset()
	If IsValid(ids_ProductGroups) Then ids_ProductGroups.Reset()
	If IsValid(ids_Products) Then ids_Products.Reset()
	If IsValid(ids_Locations) Then ids_Locations.Reset()
	If IsValid(ids_CustomerGroups) Then ids_CustomerGroups.Reset()
	ib_DeleteHeader = False
	is_RebateCode = ''
	If ib_UpdatedDivisions Then
		wf_CreateOriginalDataStore('V')
	End If
	
	If ib_UpdatedProducts Then
		wf_CreateOriginalDataStore('P')
	End If
	
	If ib_UpdatedProductGroups Then
		wf_CreateOriginalDataStore('G')
	End If
	
	If ib_UpdatedLocations Then
		wf_CreateOriginalDataStore('O')
	End If
	
	If ib_UpdatedCustomerGroups Then
		wf_CreateOriginalDataStore('S')
	End If
	
	If Not iw_parent.wf_fillrebatedropdown() Then	
		iu_notification.uf_display(iu_ErrorContext)
	End If
	
	tab_1.tabpage_header.dw_header.ResetUpdate()
	tab_1.tabpage_detail.dw_detail.ResetUpdate()
	tab_1.tabpage_customergroup.dw_customergroup.ResetUpdate()
	tab_1.tabpage_customer.dw_customer.ResetUpdate()
	tab_1.tabpage_division.dw_division.ResetUpdate()
	tab_1.tabpage_product.dw_product.ResetUpdate()
	tab_1.tabpage_productgroup.dw_productgroup.ResetUpdate()
	tab_1.tabpage_location.dw_location.ResetUpdate()
Else
	ib_ReInquire = True
	is_RebateCode = ls_HeaderString
	This.Event ue_Inquire()
End If

SetMicroHelp("Update Successful")
ib_save_flag = false
Return True

end event

event ue_inquire;String					     ls_RebateCode, &
							ls_RebateHeaderString, &
							ls_RebateDetailString, &
							ls_RebateCustomerGroupString, &
							ls_RebateCustomerString, &
							ls_RebateDivisionString, &
							ls_RebateProductGroupString, &
							ls_RebateProductString, &
							ls_RebateLocationString, &
							ls_RateSeqString, &
							ls_AppName, &
							ls_WindowName, &
							ls_description, &
							ls_RebateInquireString
							
Long						ll_row, &
							ll_sub


//dmk
DataWindowChild		ldwc_TransferCustomer, ldwc_child_uom, ldwc_child_volperiod, ldwc_child_payperiod, ldwc_child_currency
u_ParameterStack		lu_ParameterStack

ib_save_flag = True

IF Not ib_ReInquire Then
	iu_ErrorContext.uf_Initialize()
	iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
	lu_ParameterStack.uf_Initialize()

	
	This.Event Trigger CloseQuery()
	
	If Message.ReturnValue <> 0 Then Return False
	
	ls_RebateCode = is_RebateCode
	
	lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_RebateCode)
	lu_ParameterStack.uf_Push('string', is_Title)
	lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
	
	iu_ErrorContext.uf_Initialize()
	
	iu_ClassFactory.uf_GetResponseWindow( "w_rebateheaderInquire", lu_ParameterStack )
	
	lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
	lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Pop('string', is_Title)
	lu_ParameterStack.uf_Pop('string', ls_RebateCode)
	lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
	
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
	is_RebateCode = ls_RebateCode	
	If ls_RebateCode = 'Cancel' Then 
		If Not This.wf_fillrebatedropdown() Then	
			iu_notification.uf_display(iu_ErrorContext)
			Return False
		End If
		wf_SetFocus()
		Return False
	End If
End If

SetMicroHelp('Wait... Inquiring')

This.SetRedraw(False)

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_Initialize()

tab_1.tabpage_header.dw_header.Reset()
tab_1.tabpage_detail.dw_detail.Reset()
tab_1.tabpage_customergroup.dw_customergroup.Reset()
tab_1.tabpage_customer.dw_customer.Reset()
tab_1.tabpage_division.dw_division.Reset()
tab_1.tabpage_ProductGroup.dw_ProductGroup.Reset()
tab_1.tabpage_Product.dw_Product.Reset()
tab_1.tabpage_location.dw_location.Reset()

If IsValid(ids_Divisions) Then ids_Divisions.Reset()
If IsValid(ids_Products) Then ids_Products.Reset()
If IsValid(ids_ProductGroups) Then ids_ProductGroups.Reset()
If IsValid(ids_CustomerGroups) Then ids_CustomerGroups.Reset()
If IsValid(ids_Locations) Then ids_Locations.Reset()
If IsValid(ids_customers) Then ids_customers.Reset()
	
ls_AppName = GetApplication().AppName
ls_WindowName = 'rbtmain'

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_RateSeqString)
lu_ParameterStack.uf_Push('string', ls_RebateLocationString)
lu_ParameterStack.uf_Push('string', ls_RebateProductString)
lu_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
lu_ParameterStack.uf_Push('string', ls_RebateDivisionString)
lu_ParameterStack.uf_Push('string', ls_RebateCustomerString)
lu_ParameterStack.uf_Push('string', ls_RebateCustomerGroupString)
lu_ParameterStack.uf_Push('string', ls_RebateDetailString)
lu_ParameterStack.uf_Push('string', ls_RebateHeaderString)
lu_ParameterStack.uf_Push('string', is_RebateCode)

iu_RebateDataAccess.uf_Retrieve( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_RebateInquireString)
lu_ParameterStack.uf_Pop('string', ls_RebateHeaderString)
lu_ParameterStack.uf_Pop('string', ls_RebateDetailString)
lu_ParameterStack.uf_Pop('string', ls_RebateCustomerGroupString)
lu_ParameterStack.uf_Pop('string', ls_RebateCustomerString)
lu_ParameterStack.uf_Pop('string', ls_RebateDivisionString)
lu_ParameterStack.uf_Pop('string', ls_RebateProductGroupString)
lu_ParameterStack.uf_Pop('string', ls_RebateProductString)
lu_ParameterStack.uf_Pop('string', ls_RebateLocationString)
lu_ParameterStack.uf_Pop('string', ls_RateSeqString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If

If IsNull(is_dddw_currency_string) Then
	If Not wf_fillheadcurr() Then 	
		iu_notification.uf_display(iu_ErrorContext)
		Return False
	End If
Else
	tab_1.tabpage_header.dw_header.GetChild('currency_code', ldwc_child_currency)
	ldwc_child_currency.Reset()
	ldwc_child_currency.ImportString(is_dddw_currency_string)
End If

If IsNull(is_dddw_uom_string) Then
	If Not wf_fillheaduom() Then 	
		iu_notification.uf_display(iu_ErrorContext)
		Return False
	End If
Else
	tab_1.tabpage_header.dw_header.GetChild('unit_of_measure', ldwc_child_uom)
	ldwc_child_uom.Reset()
	ldwc_child_uom.ImportString(is_dddw_uom_string)
End If

tab_1.tabpage_header.dw_header.ImportString(ls_RebateHeaderString)

tab_1.tabpage_division.dw_divisionexclude.SetItem(1, 'exclude_ind', &
		tab_1.tabpage_header.dw_header.GetItemString(1, 'division_exclude_ind'))
tab_1.tabpage_productgroup.dw_productgroupexclude.SetItem(1, 'exclude_ind', &
		tab_1.tabpage_header.dw_header.GetItemString(1, 'product_group_exclude_ind'))
tab_1.tabpage_header.dw_header.object.rebate_type.Protect = 1
tab_1.tabpage_productgroup.dw_prodgrp_exclude.Object.exclude_ind.Protect=1
//tab_1.tabpage_productgroup.dw_include_all_volume.Object.include_all_volume.Protect=1
tab_1.tabpage_division.dw_divisionexclude.Object.exclude_ind.Protect=1
tab_1.tabpage_header.dw_header.object.rebate_type.Background.Color = 67108864
tab_1.tabpage_division.dw_divisionexclude.Object.exclude_ind.Background.Color= 67108864
tab_1.tabpage_productgroup.dw_prodgrp_exclude.Object.exclude_ind.Background.Color = 67108864
tab_1.tabpage_productgroup.dw_include_all_volume.Object.include_all_volume.Background.Color = 67108864
tab_1.tabpage_location.dw_locationexclude.visible = false

is_RebateType = tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')

//dmk added rebate option to replace check on rebatetype
wf_dbconnection()

 SQLCA.SQLCode = 00
 
SELECT rtrim(tutltypes.type_short_desc)
INTO :is_RebateOption  
FROM tutltypes

WHERE ( tutltypes.record_type = 'REBTYPE' ) AND  
  	   ( tutltypes.type_code = :is_RebateType );

If SQLCA.SQLCode = 00 THEN
	//do nothing
Else
	Messagebox("Error","Rebate Type not found in database: " + is_RebateType)
	Return False
END IF

iw_parent.tab_1.tabpage_productgroup.enabled = True
iw_parent.tab_1.tabpage_product.enabled = True

//use Rebate Option
//If tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type') = 'V' Then
If is_RebateOption = 'V' Then

	iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.visible = true
	iw_parent.tab_1.tabpage_header.dw_header.object.credit_method_t.visible = true		
	iw_parent.tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 1
	iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.visible = true
	iw_parent.tab_1.tabpage_header.dw_header.object.business_unit_t.visible = true	
	iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.Protect = 0
	iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215
		
	iw_parent.tab_1.tabpage_detail.visible = true
	iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_vmr'
		
	iw_parent.tab_1.tabpage_customergroup.visible = false
				
	iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = true
	iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = true
	iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = true		
	iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = true	
	iw_parent.tab_1.tabpage_customer.dw_customer.object.include_exclude.protect = false
						
	iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = true
	iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision_vmr'
	iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = true
	iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = true
								
	iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp_vmr'
	iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = true
	iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = true
					
	iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct_vmr'
	
	iw_parent.tab_1.tabpage_location.visible = false
	iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = false
	
else
	If is_RebateType = 'Q' Then
		iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.visible = false
		iw_parent.tab_1.tabpage_header.dw_header.object.credit_method_t.visible = false	
		iw_parent.tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 1
		iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.visible = false
		iw_parent.tab_1.tabpage_header.dw_header.object.business_unit_t.visible = false	
		iw_parent.tab_1.tabpage_header.dw_header.SetItem(1,'credit_method', '')
		iw_parent.tab_1.tabpage_header.dw_header.SetItem(1,'business_unit', '')
		iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.Protect = 0
		iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215		
		
		//iw_parent.tab_1.tabpage_detail.visible = false	
		iw_parent.tab_1.tabpage_detail.visible = true
		iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_accrual'
		iw_parent.tab_1.tabpage_detail.dw_detail.object.rate.visible = False
		iw_parent.tab_1.tabpage_detail.dw_detail.object.rate_t.visible = False
		iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period.visible = False
		iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period_t.visible = False		
		iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period.visible = False
		iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period_t.visible = False		
		iw_parent.tab_1.tabpage_detail.dw_detail.object.t_3.visible = False
		
		
		
		iw_parent.tab_1.tabpage_customergroup.visible = true
		iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.tonnage_rebate_credit.visible = false
		iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.credit_type_h.visible = false
		iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate.visible = false		
		iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate_t.visible = false	
		iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.exclude_ind.protect = true
				
		iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = false
		iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = false
		iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = false		
		iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = false	
		iw_parent.tab_1.tabpage_customer.dw_customer.object.include_exclude.protect = true
						
		iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = false
		iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision'
		iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = false
		iw_parent.tab_1.tabpage_division.dw_division.object.credit_type_h.visible = false		
		iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate.visible = false
		iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate_t.visible = false		
		iw_parent.tab_1.tabpage_division.dw_division.object.exclude_ind.protect = true
						
		iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp'
		iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = false
		iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = false
		
		iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct'
		iw_parent.tab_1.tabpage_product.dw_product.object.include_ind.protect = true
		
		iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = false
		iw_parent.tab_1.tabpage_location.dw_location.object.exclude_ind.Protect = true
		iw_parent.tab_1.tabpage_location.visible = true

	Else
	
		iw_parent.tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 1
		iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.visible = true
		iw_parent.tab_1.tabpage_header.dw_header.object.credit_method_t.visible = true
		iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.visible = true
		iw_parent.tab_1.tabpage_header.dw_header.object.business_unit_t.visible = true
		
		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.Protect = 1
			iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.BackGround.Color = 12632256
			iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.Protect = 1
			iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.BackGround.Color = 12632256
			iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.Protect = 1	
			iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 12632256			
		Else
			iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.Protect = 0
			iw_parent.tab_1.tabpage_header.dw_header.object.credit_method.BackGround.Color = 16777215
			iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.Protect = 0
			iw_parent.tab_1.tabpage_header.dw_header.object.business_unit.BackGround.Color = 16777215
			iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.Protect = 0	
			iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215			
		End If
		
		iw_parent.tab_1.tabpage_detail.visible = true
		iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_accrual'
		
		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period.Protect = 1
			iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period.BackGround.Color = 12632256
			iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period.Protect = 1
			iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period.BackGround.Color = 12632256	
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_incl_excl.Protect = 1
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_incl_excl.BackGround.Color = 12632256
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_ind.Protect = 1
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_ind.BackGround.Color = 12632256			
		Else
			iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period.Protect = 0
			iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period.BackGround.Color = 16777215
			iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period.Protect = 0
			iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period.BackGround.Color = 16777215	
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_incl_excl.Protect = 0
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_incl_excl.BackGround.Color = 16777215
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_ind.Protect = 0
			iw_parent.tab_1.tabpage_detail.dw_detail.object.pricing_ind.BackGround.Color = 16777215			
		End If
		
		iw_parent.tab_1.tabpage_customergroup.visible = true
		iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.exclude_ind.protect = false
		
		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate.visible = false	
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.tonnage_rebate_credit.visible = false
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.credit_type_h.visible = false
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate_t.visible = false
		Else
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate.visible = true	
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.tonnage_rebate_credit.visible = true
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.credit_type_h.visible = true
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate_t.visible = true
		End If
				
		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = false
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = false
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = false		
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = false		
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = false
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = false			
//			iw_parent.tab_1.tabpage_customer.dw_customer.object.start_date.Protect = 1
//			iw_parent.tab_1.tabpage_customer.dw_customer.object.end_date.Protect = 1
		Else
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = true
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = true
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = true		
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = true	
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = true
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = true			
//			iw_parent.tab_1.tabpage_customer.dw_customer.object.start_date.Protect = 0
//			iw_parent.tab_1.tabpage_customer.dw_customer.object.end_date.Protect = 0			
		End If
		
	
		iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = false
		iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision'
		
		If is_RebateType = 'P' Then	
			iw_parent.tab_1.tabpage_division.dw_division.object.credit_type_h.visible = false
			iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = false
			iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate.visible = false
			iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate_t.visible = false			
			iw_parent.tab_1.tabpage_division.dw_division.object.exclude_ind.Protect = 1
		Else
			iw_parent.tab_1.tabpage_division.dw_division.object.credit_type_h.visible = true
			iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = true
			iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate.visible = true
			iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate_t.visible = true	
			iw_parent.tab_1.tabpage_division.dw_division.object.exclude_ind.Protect = 0
		End If
		
		
		iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp'
		iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = false
		iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = false
		
//		If is_RebateType = 'P' Then
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.credit_type_h.visible = false
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.tonnage_rebate_credit.visible = false
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.rebate_rate_t.visible = false
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.rebate_rate.visible = false			
//		Else
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.credit_type_h.visible = true
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.tonnage_rebate_credit.visible = true		
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.rebate_rate_t.visible = true
//			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.Object.rebate_rate.visible = true			
//		End If
		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_productgroup.enabled = False
		Else
			iw_parent.tab_1.tabpage_productgroup.enabled = True
		End If
	
	     iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct'
	
//		If is_RebateType = 'P' Then
//			iw_parent.tab_1.tabpage_product.dw_product.Object.credit_type_h.visible = false
//			iw_parent.tab_1.tabpage_product.dw_product.Object.tonnage_rebate_credit.visible = false
//			iw_parent.tab_1.tabpage_product.dw_product.Object.rebate_rate_t.visible = false
//			iw_parent.tab_1.tabpage_product.dw_product.Object.rebate_rate.visible = false			
//		Else
//			iw_parent.tab_1.tabpage_product.dw_product.Object.credit_type_h.visible = true
//			iw_parent.tab_1.tabpage_product.dw_product.Object.tonnage_rebate_credit.visible = true
//			iw_parent.tab_1.tabpage_product.dw_product.Object.rebate_rate_t.visible = true
//			iw_parent.tab_1.tabpage_product.dw_product.Object.rebate_rate.visible = true			
//		End If

		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_product.enabled = False
		Else
			iw_parent.tab_1.tabpage_product.enabled = True
		End If
		
		iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = false
		iw_parent.tab_1.tabpage_location.dw_location.object.exclude_ind.Protect = false
		iw_parent.tab_1.tabpage_location.visible = true	
		
		If is_RebateType = 'P' Then
			iw_parent.tab_1.tabpage_location.dw_location.Object.rebate_rate_t.visible = false
			iw_parent.tab_1.tabpage_location.dw_location.Object.rebate_rate.visible = false
		Else
			iw_parent.tab_1.tabpage_location.dw_location.Object.rebate_rate_t.visible = true
			iw_parent.tab_1.tabpage_location.dw_location.Object.rebate_rate.visible = true
		End If		
		
	End If
End If

If IsNull(is_dddw_rebate_list_string) Then
	If Not This.wf_fillrebatedropdown() Then	
		iu_notification.uf_display(iu_ErrorContext)
		Return False
	End If
Else
	ids_RebateList.Reset()
	ids_RebateList.ImportString(is_dddw_rebate_list_string)
	ids_RebateList.Sort()
End If

If IsNull(is_dddw_volperiod_string) Then
	If Not wf_filldetvolperiod() Then 	
		iu_notification.uf_display(iu_ErrorContext)
		Return False
	End If
Else
	tab_1.tabpage_detail.dw_detail.GetChild('volume_period', ldwc_child_volperiod)
	ldwc_child_volperiod.Reset()
	ldwc_child_volperiod.ImportString(is_dddw_volperiod_string)
End If

If IsNull(is_dddw_payperiod_string) Then
	If Not wf_filldetpayperiod() Then 	
		iu_notification.uf_display(iu_ErrorContext)
		Return False
	End If
Else
	tab_1.tabpage_detail.dw_detail.GetChild('payout_period', ldwc_child_payperiod)
	ldwc_child_payperiod.Reset()
	ldwc_child_payperiod.ImportString(is_dddw_payperiod_string)
End If

If Not wf_fillpricingind() Then
		iu_notification.uf_display(iu_ErrorContext)
		Return False
End If

ll_row = tab_1.tabpage_detail.dw_detail.ImportString(ls_RebateDetailString)

tab_1.tabpage_detail.dw_detail.SetSort("detail_id A")
IF ll_row > 0 Then tab_1.tabpage_detail.dw_detail.Sort()
tab_1.tabpage_detail.dw_detail.event ue_new()

// Loading Customer Tab Information
ll_row = tab_1.tabpage_customer.dw_customer.ImportString(ls_RebateCustomerString)

If is_RebateType = 'Q' Then  // default to exclude
	For ll_sub = 1 to ll_row
		tab_1.tabpage_customer.dw_customer.SetItem(ll_sub, 'include_exclude', 'N')
	Next
End If

IF ll_row > 0 Then 
	tab_1.tabpage_customer.dw_customer.Sort()
	wf_CreateOriginalDataStore('C')
End IF

//  Loading Division Tab Information.
ll_row = tab_1.tabpage_division.dw_division.ImportString(ls_RebateDivisionString)

IF ll_row > 0 Then 
	//tab_1.tabpage_division.dw_division.SetSort("#1 A") 
	tab_1.tabpage_division.dw_division.SetSort("#1 A, #6 A") // added #6 start date
	tab_1.tabpage_division.dw_division.Sort()
End If

If ll_row > 0 Then
	wf_CreateOriginalDataStore('V')
End If

//  Loading Product Group Tab Information.
ll_row = tab_1.tabpage_productgroup.dw_productgroup.ImportString(ls_RebateProductGroupString)

// add for Include_all_volume ind.
If ll_row > 0 Then
	If tab_1.tabpage_productgroup.dw_productgroup.getitemstring(1,"Include_all_volume") = "Y" then
		tab_1.tabpage_productgroup.dw_include_all_volume.setitem(1,"Include_all_volume","Y")
	Else
		tab_1.tabpage_productgroup.dw_include_all_volume.setitem(1,"Include_all_volume","N")
	End If
Else
	tab_1.tabpage_productgroup.dw_include_all_volume.setitem(1,"Include_all_volume","N")
End If
// end

IF ll_row > 0 Then 
	//If (is_rebatetype = 'A') or (is_RebateType = 'C') or (is_rebatetype = 'O') or is_RebateType = 'I' Then
//dmk	
	If (is_RebateOption = 'A') Then		
//dmk changed from #10 to #12
		tab_1.tabpage_productgroup.dw_productgroup.SetSort("#10 A") //changed back to #10 - removed 2 columns
	Else
		tab_1.tabpage_productgroup.dw_productgroup.SetSort("#1 A")
	End If
	tab_1.tabpage_productgroup.dw_productgroup.Sort()
End If

If ll_row > 0 Then
	wf_CreateOriginalDataStore('G')
End If

//dmk changed from #9 to #11
IF ll_row > 0 Then 
//	If (is_rebatetype = 'A') or (is_RebateType = 'C') or (is_rebatetype = 'O') or is_RebateType = 'I' Then
	If (is_RebateOption = 'A') Then
		tab_1.tabpage_productgroup.dw_productgroup.SetSort("#1 A, #9 A") //changed back to #9 - removed 2 columns
	Else
		tab_1.tabpage_productgroup.dw_productgroup.SetSort("#1 A")
	End If
	tab_1.tabpage_productgroup.dw_productgroup.Sort()
End If




//  Loading Product Tab Information.
ll_row = tab_1.tabpage_product.dw_product.ImportString(ls_RebateProductString)

IF ll_row > 0 Then 
//	If (is_rebatetype = 'A') or (is_RebateType = 'C') or (is_rebatetype = 'O') or is_RebateType = 'I' Then
	If (is_RebateOption = 'A') Then
		//dmk changed sort from #8 to #10
		tab_1.tabpage_product.dw_product.SetSort("#1 A, #6 D") //change back to #8 seq_no - removed 2 columns
// 		SCS 05/10/2017 changed sort to product ascending, start date descending
//		For ll_sub = 1 to ll_row
//			If tab_1.tabpage_product.dw_product.GetItemNumber(ll_sub,"rebate_rate") < 0 Then
//				tab_1.tabpage_product.dw_product.object.rebate_rate.BackGround.Color = 65535				
//			Else
//				tab_1.tabpage_product.dw_product.object.rebate_rate.BackGround.Color = 67108864	
//			End If							
//		Next	
	Else
		tab_1.tabpage_product.dw_product.SetSort("#1 A")
	End If
	tab_1.tabpage_product.dw_product.Sort()
End If

If ll_row > 0 Then
	wf_CreateOriginalDataStore('P')
End If

IF ll_row > 0 Then 
//	If (is_rebatetype = 'A') or (is_RebateType = 'C') or (is_rebatetype = 'O') or is_RebateType = 'I' Then
	If (is_RebateOption = 'A') Then
		//dmk changed sort from #7 to #9
		tab_1.tabpage_product.dw_product.SetSort("#1 A, #6 D")
// 		SCS 05/10/2017 changed sort to product ascending, start date descending
	Else
		tab_1.tabpage_product.dw_product.SetSort("#1 A")
	End If
	tab_1.tabpage_product.dw_product.Sort()
End If

// SCS not sure why trim in required in PB12, but it fixes the import error
ll_row = dw_rebate_insert_items.ImportString(trim(ls_RateSeqString))

If ll_row > 0 then
	If ib_ReInquire = False Then
		If il_last_seq_no_product < 1 Then
			il_last_seq_no_product = dw_rebate_insert_items.getitemnumber(1, "last_seq_no_product")
		End If
		If il_last_seq_no_group < 1 Then
			il_last_seq_no_group = dw_rebate_insert_items.getitemnumber(1, "last_seq_no_group")
		End If
		idc_detail_rebate_rate = dw_rebate_insert_items.getitemdecimal(1, "detail_rebate_rate")
	End If
End If

//  Loading Location Tab Information.
ll_row = tab_1.tabpage_location.dw_location.ImportString(ls_RebateLocationString)

IF ll_row > 0 Then 
	//tab_1.tabpage_location.dw_location.SetSort("#1 A, #3 A")
	tab_1.tabpage_location.dw_location.SetSort("#1 A, #3 A, #6 A") // added #6 start date
	tab_1.tabpage_location.dw_location.Sort()
End If

If ll_row > 0 Then
	wf_CreateOriginalDataStore('O')
End If

//  Loading Customer Group Tab Information.
ll_row = tab_1.tabpage_customergroup.dw_customergroup.ImportString(ls_RebateCustomerGroupString)

If is_RebateType = 'Q' Then  // default to exclude
	For ll_sub = 1 to ll_row
		tab_1.tabpage_customergroup.dw_customergroup.SetItem(ll_sub, 'exclude_ind', 'N')
	Next
End If

IF ll_row > 0 Then 
	If (is_RebateOption = 'A') Then		
		tab_1.tabpage_customergroup.dw_customergroup.SetSort("#10 A") 
	Else
		tab_1.tabpage_customergroup.dw_customergroup.SetSort("#1 A")
	End If
	tab_1.tabpage_customergroup.dw_customergroup.Sort()
End If

If ll_row > 0 Then
	wf_CreateOriginalDataStore('S')
End If

IF ll_row > 0 Then 
	If (is_RebateOption = 'A') Then
		tab_1.tabpage_customergroup.dw_customergroup.SetSort("#1 A, #9 A") 
	Else
		tab_1.tabpage_customergroup.dw_customergroup.SetSort("#1 A")
	End If
	tab_1.tabpage_customergroup.dw_customergroup.Sort()
End If

//If is_RebateOption = 'V' Then
//	tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 1
//End If

idt_header_start_date = tab_1.tabpage_header.dw_header.getitemdate(1, "start_date")
idt_header_end_date = tab_1.tabpage_header.dw_header.getitemdate(1, "end_date")

This.SetRedraw(True)

tab_1.tabpage_Header.dw_header.ResetUpdate()
tab_1.tabpage_detail.dw_detail.ResetUpdate()
tab_1.tabpage_customergroup.dw_customergroup.ResetUpdate()
tab_1.tabpage_customer.dw_customer.ResetUpdate()
tab_1.tabpage_division.dw_division.ResetUpdate()
tab_1.tabpage_productgroup.dw_productgroup.ResetUpdate()
tab_1.tabpage_product.dw_product.ResetUpdate()
tab_1.tabpage_location.dw_location.ResetUpdate()

ib_ReInquire = False
ib_changes = False
ib_save_flag = False

Return True
end event

event ue_new;
tab_1.SelectTab(tab_1.tabpage_header)
tab_1.tabpage_header.dw_header.SetFocus()
is_RebateCode = 'Cancel'
idc_detail_rebate_rate = 0
il_last_seq_no_product = 0
il_last_seq_no_group = 0

Choose Case tab_1.SelectedTab 
	Case 1
		This.Event CloseQuery()
		tab_1.tabpage_header.dw_header.Event ue_new()
		tab_1.tabpage_header.dw_header.AcceptText()
	Case 2
		tab_1.tabpage_detail.dw_detail.Event ue_new()
		tab_1.tabpage_detail.dw_detail.AcceptText()
End Choose

iw_parent.tab_1.tabpage_productgroup.enabled = True
iw_parent.tab_1.tabpage_product.enabled = True

iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.Protect = 0	
iw_parent.tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215	
end event

type dw_rebate_insert_items from datawindow within w_rebatemain
boolean visible = false
integer x = 32
integer y = 1636
integer width = 1627
integer height = 644
integer taborder = 20
string title = "none"
string dataobject = "d_rebate_insert_items"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_header_display from datawindow within w_rebatemain
integer x = 5
integer width = 2875
integer height = 296
string dataobject = "d_rebateheaderdisplay"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type tab_1 from tab within w_rebatemain
integer y = 328
integer width = 2981
integer height = 1460
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean raggedright = true
integer selectedtab = 1
tabpage_header tabpage_header
tabpage_detail tabpage_detail
tabpage_customergroup tabpage_customergroup
tabpage_customer tabpage_customer
tabpage_division tabpage_division
tabpage_productgroup tabpage_productgroup
tabpage_product tabpage_product
tabpage_location tabpage_location
end type

on tab_1.create
this.tabpage_header=create tabpage_header
this.tabpage_detail=create tabpage_detail
this.tabpage_customergroup=create tabpage_customergroup
this.tabpage_customer=create tabpage_customer
this.tabpage_division=create tabpage_division
this.tabpage_productgroup=create tabpage_productgroup
this.tabpage_product=create tabpage_product
this.tabpage_location=create tabpage_location
this.Control[]={this.tabpage_header,&
this.tabpage_detail,&
this.tabpage_customergroup,&
this.tabpage_customer,&
this.tabpage_division,&
this.tabpage_productgroup,&
this.tabpage_product,&
this.tabpage_location}
end on

on tab_1.destroy
destroy(this.tabpage_header)
destroy(this.tabpage_detail)
destroy(this.tabpage_customergroup)
destroy(this.tabpage_customer)
destroy(this.tabpage_division)
destroy(this.tabpage_productgroup)
destroy(this.tabpage_product)
destroy(this.tabpage_location)
end on

event selectionchanging;String			ls_rebate_type, &
					ls_rebate_option
Boolean			lb_return

If ib_changes Then
	IF Parent.Event CloseQuery() = 1 Then 
		Return 1
	End IF
	ib_changes = False
End If

If is_RebateCode = 'Cancel' Then
	tab_1.tabpage_header.dw_header.AcceptText()
	lb_return = (ls_rebate_type = tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type'))
		
	If IsNull(lb_return) Then
		Messagebox("Informational","You must have Header Information to continue to other Tabs.")
		Return 1
	Else
		ls_rebate_type = tab_1.tabpage_header.dw_header.GetItemString(1, 'rebate_type')
		wf_dbconnection()

		SELECT tutltypes.type_short_desc
		INTO :ls_Rebate_Option  
		FROM tutltypes

		WHERE ( tutltypes.record_type = 'REBTYPE' ) AND  
  	   		( tutltypes.type_code = :ls_rebate_type );

	
		If SQLCA.SQLCode = 00 THEN
			ls_Rebate_Option = Trim(ls_Rebate_Option)
		Else
			Messagebox("Error","Rebate Type not found in database: " + is_RebateType)
			Return 1
		END IF
		
		tab_1.tabpage_productgroup.dw_prodgrp_exclude.Object.exclude_ind.Protect=1
//		tab_1.tabpage_productgroup.dw_include_all_volume.Object.include_all_volume.Protect=1
		tab_1.tabpage_division.dw_divisionexclude.Object.exclude_ind.Protect=1
		tab_1.tabpage_division.dw_divisionexclude.Object.exclude_ind.Background.Color= 67108864
		tab_1.tabpage_productgroup.dw_prodgrp_exclude.Object.exclude_ind.Background.Color = 67108864
		tab_1.tabpage_productgroup.dw_include_all_volume.Object.include_all_volume.Background.Color = 67108864
		tab_1.tabpage_location.dw_locationexclude.Object.exclude_ind.Protect=1
		tab_1.tabpage_location.dw_locationexclude.Object.exclude_ind.Background.Color= 67108864
		//If ls_rebate_type = 'V' Then
		If ls_rebate_option = 'V' Then
			iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_vmr'
			iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = true
			iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = true
			iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = true
			iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = false
			iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct_vmr'
			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp_vmr'
			iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision_vmr'
		Else
			//If (ls_rebate_type = 'A') or (ls_rebate_type = 'C') or (ls_rebate_type = 'O') or (ls_rebate_type = 'I') Then
			If (ls_rebate_option = 'A') Then
				If is_RebateType = 'Q' Then
					iw_parent.tab_1.tabpage_detail.dw_detail.Visible = false
				Else
					iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_accrual'
				End If
					
				iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = false
				iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = false
				iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = false
				iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = false
				iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct'
				iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp'
				iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision'
			
			Else
				Messagebox("Informational","You must save Header Information to continue to other Tabs.")
				Return 1
			End If
		End If
	End If
	
End If
end event

type tabpage_header from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Header"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_header dw_header
end type

on tabpage_header.create
this.dw_header=create dw_header
this.Control[]={this.dw_header}
end on

on tabpage_header.destroy
destroy(this.dw_header)
end on

type dw_header from datawindow within tabpage_header
event ue_dwndropdown pbm_dwndropdown
event ue_new ( )
event ue_delete ( )
event ue_postconstructor ( )
integer x = 5
integer y = 4
integer width = 2939
integer height = 1384
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_rebateheader"
boolean border = false
borderstyle borderstyle = styleraised!
end type

event ue_new();String							ls_userid

DataWindow				ldw_datawindow

u_ProjectFunctions		lu_projectfunctions


Parent.SetRedraw(False)

This.Reset()

This.InsertRow(0)

This.Object.credit_method.visible = true
This.Object.credit_method_t.visible = true		
This.Object.rebate_date_option.Protect = 0
This.Object.business_unit.visible = true
This.Object.business_unit_t.visible = true	
	
This.SetItem(1, 'rebate_id', 0)

If Not lu_projectfunctions.uf_GetUserId(ls_userid, iu_ClassFactory, iu_ErrorContext) Then
	iu_notification.uf_display(iu_errorcontext)
	Parent.SetRedraw(True)
	Return
End If

This.SetItem(1, 'initiated_by', ls_userid)

This.SetItem(1, 'start_date', Today())
This.SetItem(1, 'end_date', Date('2999-12-31'))

This.SetItem(1, 'division_exclude_ind', 'Y')
This.SetItem(1, 'product_group_exclude_ind', 'Y')
This.SetItem(1, 'customer_select_all_ind', 'N')

This.SetItem(1, 'credit_method', 'C')

This.SetItem(1, 'country_code', 'ALL')

This.SetItem(1, 'rebate_date_option', 'S')

This.SetItem(1, 'unit_of_measure', '01')
This.SetItem(1, 'currency_code', 'USD')
This.SetItem(1, 'sales_location', 'ALL')

tab_1.tabpage_division.dw_divisionexclude.SetItem(1, 'exclude_ind', 'N')
tab_1.tabpage_productgroup.dw_productgroupexclude.SetItem(1, 'exclude_ind', 'N')
tab_1.tabpage_location.dw_locationexclude.SetItem(1, 'exclude_ind', 'N')

This.object.rebate_type.Protect = 0
This.object.rebate_type.Background.Color = 16777215

ldw_datawindow = This
lu_projectfunctions.uf_changerowstatus(ldw_datawindow, 1, New!)

tab_1.tabpage_detail.visible = true
tab_1.tabpage_detail.dw_detail.Reset()
tab_1.tabpage_detail.dw_detail.Event ue_new()

If IsValid(ids_Divisions) Then Destroy(ids_Divisions)

If IsValid(ids_Products) Then Destroy(ids_Products)

If IsValid(ids_ProductGroups) Then Destroy(ids_ProductGroups)

If IsValid(ids_Locations) Then Destroy(ids_Locations)

If IsValid(ids_CustomerGroups) Then Destroy(ids_CustomerGroups)


tab_1.tabpage_customergroup.dw_customergroup.Reset()
tab_1.tabpage_customer.dw_customer.Reset()
tab_1.tabpage_division.dw_division.Reset()
tab_1.tabpage_productgroup.dw_productgroup.Reset()
tab_1.tabpage_product.dw_product.Reset()
tab_1.tabpage_location.dw_location.Reset()

this.setfocus()

Parent.SetRedraw(True)

ib_changes = True

end event

event ue_delete;window							lw_parent


IF This.GetItemNumber(1, 'Rebate_id') <= 0 Then Return

If MessageBox('Delete Rebate', 'This function will delete all Detail Lines, Customers, ' + &
		'Product Groups, and Products for this Rebate.  Are you sure you want to delete ' + &
		'this Rebate and everything that goes with it?', Exclamation!, YesNo!, 2) = 1 Then
	This.DeleteRow(1)
	ib_DeleteHeader = True
	iw_parent.TriggerEvent('ue_save')
	wf_fillrebatedropdown()
End If

ib_changes = True


end event

event itemfocuschanged;This.SelectText(1, 1000)

SetMicroHelp('Ready')
end event

event getfocus;This.SelectText(1, 1000)
end event

event itemchanged;Date							ldt_Date
Long							ll_findrow
String						ls_rebate_option
u_string_functions		lu_StringFunctions


ldt_Date = Date(data)

Choose Case dwo.name
	Case 'description'
		ll_findrow = ids_RebateList.Find('Trim(description) = "' + Trim(data) + '"', 1, ids_RebateList.RowCount() + 1)
		If ll_findrow > 0 Then
			SetMicroHelp('The Description must be unique')
			This.SelectText(1, 1000)
			Return 1
		End If
	Case 'rebate_type'
		wf_dbconnection()

		SELECT tutltypes.type_short_desc
		INTO :ls_Rebate_Option  
		FROM tutltypes

		WHERE ( tutltypes.record_type = 'REBTYPE' ) AND  
  	   		( tutltypes.type_code = :data );

		is_RebateType = trim(data)
	
		If SQLCA.SQLCode = 00 THEN
			//do nothing
		Else
			Messagebox("Error","Rebate Type not found in database: " + is_RebateType)
			Return 1
		END IF
		
		is_RebateOption = trim(ls_rebate_option)
		
		//If data = 'V' Then
		If trim(ls_rebate_option) = 'V' Then
			
			tab_1.tabpage_header.dw_header.object.credit_method.visible = true
			tab_1.tabpage_header.dw_header.object.credit_method_t.visible = true		
			tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 1
			tab_1.tabpage_header.dw_header.object.business_unit.visible = true
			tab_1.tabpage_header.dw_header.object.business_unit_t.visible = true	
			tab_1.tabpage_header.dw_header.object.unit_of_measure.protect = 0
			tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215
		
			iw_parent.tab_1.tabpage_detail.visible = true
			iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_vmr'	
		
			iw_parent.tab_1.tabpage_customergroup.visible = true
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.tonnage_rebate_credit.visible = true
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.credit_type_h.visible = true
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate.visible = true		
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate_t.visible = true	
			iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.exclude_ind.protect = false
				
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = true
			iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = true
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = true		
			iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = true	
			iw_parent.tab_1.tabpage_customer.dw_customer.object.include_exclude.protect = false
						
			iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = true
			iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision_vmr'
			iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = true
			iw_parent.tab_1.tabpage_division.dw_division.object.credit_type_h.visible = true		
						
			iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp_vmr'
			iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = true
			iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = true	
			
			iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct_vmr'
			
			iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = true
			iw_parent.tab_1.tabpage_location.dw_location.object.exclude_ind.Protect = false
			//iw_parent.tab_1.tabpage_location.visible = true							
			
		else
			If is_RebateType = 'Q' Then
								
				tab_1.tabpage_header.dw_header.object.credit_method.visible = false
				tab_1.tabpage_header.dw_header.object.credit_method_t.visible = false		
				tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 0
				tab_1.tabpage_header.dw_header.SetItem(1, 'rebate_date_option', 'P')
				tab_1.tabpage_header.dw_header.object.business_unit.visible = false
				tab_1.tabpage_header.dw_header.object.business_unit_t.visible = false	
				tab_1.tabpage_header.dw_header.SetItem(1,'credit_method', '')
				tab_1.tabpage_header.dw_header.SetItem(1,'business_unit', '')
				tab_1.tabpage_header.dw_header.SetItem(1,'sales_location', 'ALL')
				tab_1.tabpage_header.dw_header.object.unit_of_measure.protect = 0
				tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215
			
				//iw_parent.tab_1.tabpage_detail.visible = false	
				iw_parent.tab_1.tabpage_detail.visible = true
				iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_accrual'
				iw_parent.tab_1.tabpage_detail.dw_detail.object.rate.visible = False
				iw_parent.tab_1.tabpage_detail.dw_detail.object.rate_t.visible = False
				iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period.visible = False
				iw_parent.tab_1.tabpage_detail.dw_detail.object.volume_period_t.visible = False		
				iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period.visible = False
				iw_parent.tab_1.tabpage_detail.dw_detail.object.payout_period_t.visible = False		
				iw_parent.tab_1.tabpage_detail.dw_detail.object.t_3.visible = False
		
				iw_parent.tab_1.tabpage_customergroup.visible = true
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.tonnage_rebate_credit.visible = false
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.credit_type_h.visible = false
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate.visible = false		
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate_t.visible = false	
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.exclude_ind.protect = true
				
				iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = false
				iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = false
				iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = false		
				iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = false	
				iw_parent.tab_1.tabpage_customer.dw_customer.object.include_exclude.protect = true
						
				iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = false
				iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision'
				iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = false
				iw_parent.tab_1.tabpage_division.dw_division.object.credit_type_h.visible = false		
				iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate.visible = false
				iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate_t.visible = false		
				iw_parent.tab_1.tabpage_division.dw_division.object.exclude_ind.protect = true
						
				iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp'
				iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = false
				iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = false	
				
				iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct'
			
				iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = false
				iw_parent.tab_1.tabpage_location.dw_location.object.exclude_ind.Protect = true
				iw_parent.tab_1.tabpage_location.visible = true			
				
				
								
			
			Else
								
				tab_1.tabpage_header.dw_header.object.credit_method.visible = true
				tab_1.tabpage_header.dw_header.object.credit_method_t.visible = true		
				tab_1.tabpage_header.dw_header.object.rebate_date_option.Protect = 0
				
				If is_RebateType = 'P' Then
					tab_1.tabpage_header.dw_header.SetItem(1, 'rebate_date_option', 'S')
				Else	
					tab_1.tabpage_header.dw_header.SetItem(1, 'rebate_date_option', 'P')
				End If
				
				tab_1.tabpage_header.dw_header.object.unit_of_measure.protect = 0
				tab_1.tabpage_header.dw_header.object.unit_of_measure.BackGround.Color = 16777215
				
				tab_1.tabpage_header.dw_header.object.business_unit.visible = true
				tab_1.tabpage_header.dw_header.object.business_unit_t.visible = true	
		
				iw_parent.tab_1.tabpage_detail.visible = true
				iw_parent.tab_1.tabpage_detail.dw_detail.DataObject = 'd_rebatedetail_accrual'
		
				iw_parent.tab_1.tabpage_customergroup.visible = true
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.tonnage_rebate_credit.visible = true
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.credit_type_h.visible = true
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate.visible = true		
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.rebate_rate_t.visible = true	
				iw_parent.tab_1.tabpage_customergroup.dw_customergroup.object.exclude_ind.protect = false
				
				iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id.visible = true
				iw_parent.tab_1.tabpage_customer.dw_customer.object.vendor_id_t.visible = true
				iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate.visible = true		
				iw_parent.tab_1.tabpage_customer.dw_customer.object.rebate_rate_t.visible = true	
				iw_parent.tab_1.tabpage_customer.dw_customer.object.include_exclude.protect = false
						
				iw_parent.tab_1.tabpage_division.dw_divisionexclude.visible = true
				iw_parent.tab_1.tabpage_division.dw_division.DataObject = 'd_rebatedivision'
				iw_parent.tab_1.tabpage_division.dw_division.object.tonnage_rebate_credit.visible = true
				iw_parent.tab_1.tabpage_division.dw_division.object.credit_type_h.visible = true		
				iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate.visible = true
				iw_parent.tab_1.tabpage_division.dw_division.object.rebate_rate_t.visible = true		
				iw_parent.tab_1.tabpage_division.dw_division.object.exclude_ind.protect = false
						
				iw_parent.tab_1.tabpage_productgroup.dw_productgroup.DataObject = 'd_rebateprodgrp'
				iw_parent.tab_1.tabpage_productgroup.dw_include_all_volume.visible = false
				iw_parent.tab_1.tabpage_productgroup.dw_prodgrp_exclude.visible = true	
				
				iw_parent.tab_1.tabpage_product.dw_product.DataObject = 'd_rebateproduct'
			
				iw_parent.tab_1.tabpage_location.dw_locationexclude.visible = true
				iw_parent.tab_1.tabpage_location.dw_location.object.exclude_ind.Protect = false
				iw_parent.tab_1.tabpage_location.visible = true				
												
			End If
		End If
		
		iw_parent.tab_1.tabpage_detail.dw_detail.insertrow(0)
		
		If Not wf_fillheaduom() Then 	
			iu_notification.uf_display(iu_ErrorContext)
			Return 
		end if
		If Not wf_fillheadcurr() Then 	
			iu_notification.uf_display(iu_ErrorContext)
			Return 
		end if
		If Not wf_filldetvolperiod() Then 	
			iu_notification.uf_display(iu_ErrorContext)
			Return
		End If
		If Not wf_filldetpayperiod() Then 	
			iu_notification.uf_display(iu_ErrorContext)
			Return
		End If
	Case 'requested_by'
		IF lu_StringFunctions.nf_IsEmpty(data) Then
			SetMicroHelp('The Requested By must be greater than spaces')
			This.SelectText(1, 1000)
			Return 1
		End If
	Case 'start_date'
//dmk use rebate option 
//		If (This.GetItemString(1, 'rebate_type') = 'A') OR &
//		   (This.GetItemString(1, 'rebate_type') = 'C') OR &
//			(This.GetItemString(1, 'rebate_type') = 'O') OR &
//			(This.GetItemString(1, 'rebate_type') = 'I') Then
		If is_RebateOption = 'A' Then
			If ldt_date < Today() or ldt_date > This.GetItemDate(row, 'end_date') Then
				SetMicroHelp('The Start Date must be greater than or equal to today and less' + &
				' than or equal to the End Date')
				This.SelectText(1, 1000)
				Return 1
			End If
		Else
			If ldt_date > This.GetItemDate(row, 'end_date') Then
				SetMicroHelp('The Start Date must be less than or equal to the End Date')
				This.SelectText(1, 1000)
				Return 1
			End If
		End If
		wf_DefaultStartDates(ldt_Date)		
	Case 'end_date'
		If ldt_date < This.GetItemDate(row, 'start_date') Then
			SetMicroHelp('The End Date must be greater than or equal to Start Date')
			This.SelectText(1, 1000)
			Return 1
		End If
		wf_DefaultEndDates(ldt_Date)		
	Case 'currency_code'
		This.AcceptText()
		if This.GetItemstring(row, 'currency_code') = ' ' then	 
			This.SetItem(row, 'currency_code', ' ')	
		end if
	Case 'rebate_date_option'		
		If (trim(data) = 'P') and (is_RebateType = 'P') Then
			SetMicroHelp('Price Date Option not valid for Pallet type charges')
			This.SelectText(1, 1000)
			Return 1
		End If
	Case 'unit_of_measure'		
		If (trim(data) = '03') or (trim(data) = '04') Then
//			If (is_RebateType = 'P') and (Trim(This.GetItemString(1, 'loading_instruction')) = 'B') Then
//				// continue
//			Else
//				SetMicroHelp('Only QUANTITY or PIECE are allowed for charge type of LOADING PLATFORM, HIDE BAGS')
//				This.SelectText(1, 1000)
//				Return 1
//			End If
		Else	
			If (is_RebateType = 'P') and (Trim(This.GetItemString(1, 'loading_instruction')) = 'B') Then
				SetMicroHelp('Only QUANTITY or PIECE are allowed for charge type of LOADING PLATFORM, HIDE BAGS')
				This.SelectText(1, 1000)
				Return 1
			End If
		End If
End Choose

ib_changes = True

end event

event itemerror;Return 1

end event

event doubleclicked;u_AbstractParameterStack	lu_Stack
String							ls_Date

// This script opens up the calendar when the correct column is double-clicked
// Let's make sure they double-clicked on a date or datetime column
If row <= 0 Then Return
If String(dwo.Type) <> "column" Then Return

If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return

ls_date = String(Today())

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack', lu_Stack, iu_ErrorContext)
lu_Stack.uf_Initialize()

// Set up the parameters for the calendar window
lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
lu_stack.uf_Push("String", ls_Date)
lu_Stack.uf_Push("String", "d_calendar")
lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)

// Open the calendar window
iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
lu_Stack = Message.PowerObjectParm
If Not Isvalid( lu_stack ) Then Return
lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)

// Was it successful?  If so, get the date passed back and set the column.
If iu_ErrorContext.uf_IsSuccessful() Then
	lu_Stack.uf_Pop("String",ls_Date)
	If Len(Trim(ls_Date)) = 0 Then Return
	This.SetText(ls_date)
	If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
END IF

end event

type tabpage_detail from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Detail"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_detail dw_detail
end type

on tabpage_detail.create
this.dw_detail=create dw_detail
this.Control[]={this.dw_detail}
end on

on tabpage_detail.destroy
destroy(this.dw_detail)
end on

type dw_detail from datawindow within tabpage_detail
event ue_new ( )
event ue_delete ( )
integer y = 16
integer width = 2930
integer height = 1096
integer taborder = 20
string dataobject = "d_rebatedetail_vmr"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_new();Long							ll_row
DataWindow					ldw_datawindow
u_projectfunctions		lu_projectfunctions

//dmk
ll_row = This.InsertRow(0)
//dmk check rebate option
//choose case tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type')
choose case is_RebateOption
	case "A"
		This.SetItem(ll_row, 'detail_id', 0)
		This.SetItem(ll_row, 'minimum_target_volume', 1)
		This.SetItem(ll_row, 'maximum_target_volume', 99999999)
		this.setitem(ll_row, 'incl_min_wgt_ind', "Y")
		if ll_row = 1 then
			tab_1.tabpage_header.dw_header.AcceptText()
			This.SetItem(ll_row,'start_date', tab_1.tabpage_header.dw_header.GetItemDate &
																							(1, 'start_date'))
			This.SetItem(ll_row,'end_date', tab_1.tabpage_header.dw_header.GetItemDate &
																							(1,'end_date'))
			ldw_datawindow = This
			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
		else
			tab_1.tabpage_header.dw_header.AcceptText()
			This.SetItem(ll_row,'start_date',RelativeDate &
									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),1))
			This.SetItem(ll_row,'end_date',RelativeDate &
									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),2))
			ldw_datawindow = This
			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
		end if
		
//	case "C"
//		This.SetItem(ll_row, 'detail_id', 0)
//		This.SetItem(ll_row, 'minimum_target_volume', 1)
//		This.SetItem(ll_row, 'maximum_target_volume', 99999999)
//		this.setitem(ll_row, 'incl_min_wgt_ind', "Y")
//		This.SetItem(ll_row, 'currency_code', "USD")
//		if ll_row = 1 then
//			tab_1.tabpage_header.dw_header.AcceptText()
//			This.SetItem(ll_row,'start_date', tab_1.tabpage_header.dw_header.GetItemDate &
//																							(1, 'start_date'))
//			This.SetItem(ll_row,'end_date', RelativeDate(tab_1.tabpage_header.dw_header.GetItemDate &
//																							(1,'start_date'),1))
//			ldw_datawindow = This
//			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
//		else
//			tab_1.tabpage_header.dw_header.AcceptText()
//			This.SetItem(ll_row,'start_date',RelativeDate &
//									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),1))
//			This.SetItem(ll_row,'end_date',RelativeDate &
//									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),2))
//			ldw_datawindow = This
//			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
//		end if		
//				
//	case "O"
//		This.SetItem(ll_row, 'detail_id', 0)
//		This.SetItem(ll_row, 'minimum_target_volume', 1)
//		This.SetItem(ll_row, 'maximum_target_volume', 99999999)
//		this.setitem(ll_row, 'incl_min_wgt_ind', "Y")
//		This.SetItem(ll_row, 'currency_code', "USD")
//		if ll_row = 1 then
//			tab_1.tabpage_header.dw_header.AcceptText()
//			This.SetItem(ll_row,'start_date', tab_1.tabpage_header.dw_header.GetItemDate &
//																							(1, 'start_date'))
//			This.SetItem(ll_row,'end_date', RelativeDate(tab_1.tabpage_header.dw_header.GetItemDate &
//																							(1,'start_date'),1))
//			ldw_datawindow = This
//			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
//		else
//			tab_1.tabpage_header.dw_header.AcceptText()
//			This.SetItem(ll_row,'start_date',RelativeDate &
//									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),1))
//			This.SetItem(ll_row,'end_date',RelativeDate &
//									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),2))
//			ldw_datawindow = This
//			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
//		end if	
//				
//	case "I"
//		This.SetItem(ll_row, 'detail_id', 0)
//		This.SetItem(ll_row, 'minimum_target_volume', 1)
//		This.SetItem(ll_row, 'maximum_target_volume', 99999999)
//		this.setitem(ll_row, 'incl_min_wgt_ind', "Y")
//		This.SetItem(ll_row, 'currency_code', "USD")
//		if ll_row = 1 then
//			tab_1.tabpage_header.dw_header.AcceptText()
//			This.SetItem(ll_row,'start_date', tab_1.tabpage_header.dw_header.GetItemDate &
//																							(1, 'start_date'))
//			This.SetItem(ll_row,'end_date', RelativeDate(tab_1.tabpage_header.dw_header.GetItemDate &
//																							(1,'start_date'),1))
//			ldw_datawindow = This
//			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
//		else
//			tab_1.tabpage_header.dw_header.AcceptText()
//			This.SetItem(ll_row,'start_date',RelativeDate &
//									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),1))
//			This.SetItem(ll_row,'end_date',RelativeDate &
//									(tab_1.tabpage_detail.dw_detail.GetItemDate(ll_row - 1,'end_date'),2))
//			ldw_datawindow = This
//			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
//		end if	
//		
	case "V"
		This.SetItem(ll_row, 'detail_id', 0)
		This.SetItem(ll_row, 'start_date', tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date'))
		This.SetItem(ll_row, 'end_date', tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date'))
		this.setitem(ll_row, 'incl_min_wgt_ind', "Y")
		if ll_row =1 then
			tab_1.tabpage_header.dw_header.AcceptText()
			This.SetItem(ll_row, 'minimum_target_volume', 1)
			This.SetItem(ll_row, 'maximum_target_volume', 10000)
			ldw_datawindow = This
			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
		else
			tab_1.tabpage_header.dw_header.AcceptText()
			This.SetItem(ll_row, 'minimum_target_volume', &
					tab_1.tabpage_detail.dw_detail.GetItemnumber(ll_row - 1,'maximum_target_volume') + 1)
			This.SetItem(ll_row, 'maximum_target_volume', &
					tab_1.tabpage_detail.dw_detail.GetItemnumber(ll_row - 1,'maximum_target_volume') + 10000)
			ldw_datawindow = This
			lu_projectfunctions.uf_changerowstatus(ldw_datawindow, ll_row, New!)
		end if
		
end choose



end event

event ue_delete;Long				ll_row


ll_row = This.GetRow()

If ll_row <= 0 Then Return

IF This.GetItemStatus(ll_row, 0, Primary!) = new! Then Return

This.SelectRow(ll_row, True)

If MessageBox('Delete Rebate', 'Are you sure you want to delete this Rebate Detail?', Exclamation!, YesNo!, 2) = 1 Then
	This.DeleteRow(ll_row)
	Return
End If

This.SelectRow(ll_row, False)

ib_changes = True

end event

event itemfocuschanged;This.SelectText(1, 1000)

SetMicroHelp('Ready')

end event

event getfocus;This.SelectText(1, 1000)
end event

event itemchanged;Date					ldt_date

ldt_date = Date(data)

Choose Case dwo.Name
	Case 'start_date'
		If Not IsDate(data) or IsNull(data)	Then
			SetMicroHelp('Invalid Date.')
			This.SelectText(1,1000)
			Return 1
		End If
		tab_1.tabpage_header.dw_header.AcceptText()
		If ldt_date < tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date') Or &
				ldt_date > tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date') Or &
				ldt_date > This.GetItemDate(row, 'end_date') Then
			SetMicroHelp('The Start Date must be within the Rebate Header' + &
			' Dates and less than or equal to the End Date')
			This.SelectText(1, 1000)
			Return 1
		End If
		//dmk use rebate option
//		if (tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'A') or &
//		   (tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'C') or &
//		   (tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'O') or &
//			(tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'I') then
		If is_RebateOption = 'A' Then
			if wf_check_dates(row, ldt_date, "modified") then
			else
				SetMicroHelp('The date period can not overlap with any other detail date period.') 
				This.SelectText(1, 1000)
				Return 1
			end if
		end if
	Case 'end_date'
		If Not IsDate(data) or IsNull(data)	Then
			SetMicroHelp('Invalid Date.')
			This.SelectText(1,1000)
			Return 1
		End If		
		tab_1.tabpage_header.dw_header.AcceptText()
		If ldt_date < tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date') Or &
				ldt_date > tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date') Or &
				ldt_date < This.GetItemDate(row, 'start_date') Then
			SetMicroHelp('The Start Date must be within the Rebate Header' + &
			' Dates and greater than or equal to the Start Date')
			This.SelectText(1, 1000)
			Return 1
		End If
	//dmk use rebate option 
//		if (tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'A') or &
//		   (tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'C') or &
//			(tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'O') or &
//			(tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'I') then
		If is_RebateOption = 'A' Then
			if wf_check_dates(row, ldt_date, "modified") then
			else
				SetMicroHelp('The date period can not overlap with any other detail date period.') 
				This.SelectText(1, 1000)
				Return 1
			end if
		end if			
	Case 'minimum_target_volume'
		IF Long(data) >= This.GetItemNumber(row, 'maximum_target_volume') Then
			SetMicroHelp('The Minimum Target Volume must be less than ' + &
			'the Maximum Target Volume')
			This.SelectText(1, 1000)
			Return 1
		End If
		//if tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'V' then	
		if is_RebateOption = 'V' Then
			if wf_check_volumes('modified', row, long(data)) then
			else
				SetMicroHelp('The volume can not overlap with any other detail volume.') 
				This.SelectText(1, 1000)
				Return 1
			end if
		end if
	Case 'maximum_target_volume'
		IF Long(data) <= This.GetItemNumber(row, 'minimum_target_volume') Then
			SetMicroHelp('The Maximum Target Volume must be greater than ' + &
			'the Minimum Target Volume')
			This.SelectText(1, 1000)
			Return 1
		End If
		//if tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'V' then
		if is_RebateOption = 'V' Then
			if wf_check_volumes('modified', row, long(data)) then
	
			else
				SetMicroHelp('The volume can not overlap with any other detail volume.') 
				This.SelectText(1, 1000)
				Return 1
			end if
		end if

	Case 'pricing_incl_excl'
		This.AcceptText()
		if This.GetItemstring(row, 'pricing_incl_excl') = ' ' then	 
			This.SetItem(row, 'pricing_ind', ' ')	
			This.AcceptText()
		end if
End Choose

If (dwo.Name = "start_date") or (dwo.Name = "end_date") or (dwo.Name = "pricing_incl_excl") or(dwo.Name = "pricing_ind") Then
	If tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'Q' Then
		This.SetItem(row, "rate", 0)
		This.SetItem(row, "minimum_target_volume", 1)
		This.SetItem(row, "maximum_target_volume", 99999999)
		This.SetItem(row, "volume_period", 'MTLY    ')
		This.SetItem(row, "payout_period", 'MTLY    ')
	End If
End If

If (dwo.Name = "start_date") or (dwo.Name = "end_date") or (dwo.Name = "pricing_incl_excl") or(dwo.Name = "pricing_ind") Then
	If tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') = 'P' Then
		This.SetItem(row, "rate", 0)
		This.SetItem(row, "minimum_target_volume", 1)
		This.SetItem(row, "maximum_target_volume", 99999999)
		This.SetItem(row, "volume_period", 'MTLY    ')
		This.SetItem(row, "payout_period", 'MTLY    ')
	End If
End If

ib_changes = True

If row = This.RowCount() Then 
	This.Event ue_new()
End If



		
end event

event itemerror;Return 1

end event

event doubleclicked;u_AbstractParameterStack	lu_Stack
String							ls_Date

// This script opens up the calendar when the correct column is double-clicked
// Let's make sure they double-clicked on a date or datetime column
If row <= 0 Then Return
If String(dwo.Type) <> "column" Then Return

If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return

ls_date = String(Today())

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
lu_Stack.uf_Initialize()

// Set up the parameters for the calendar window
lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
lu_stack.uf_Push("String", ls_Date)
lu_Stack.uf_Push("String", "d_calendar")
lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)

// Open the calendar window
iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
lu_Stack = Message.PowerObjectParm
If Not Isvalid( lu_stack ) Then Return
lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)

// Was it successful?  If so, get the date passed back and set the column.
If iu_ErrorContext.uf_IsSuccessful() Then
	lu_Stack.uf_Pop("String",ls_Date)
	If Len(Trim(ls_Date)) = 0 Then Return
	This.SetText(ls_date)
// I put this in to fix a problem, however it caused it to trigger itemchanged
// twice which added two new lines.  When I took it out the original problem
// didn't show up.  I will leave it like this until the problems shows up.
//	If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
END IF

end event

event rowfocuschanging;u_String_Functions	lu_StringFunctions
long						ll_row_count

ll_row_count = tab_1.tabpage_header.dw_header.rowcount()

if Tab_1.SelectedTab = 2 and ib_save_flag = false and ll_row_count > 1 then
//	choose case tab_1.tabpage_header.dw_header.GetItemstring(1, 'rebate_type') 
	choose case is_RebateOption
	case "A"
			ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount()
			if ll_row_count > 1 then
				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
															(currentrow, 'start_date'), "modified") then
				else
					SetMicroHelp('The date period can not overlap with any other detail date period.') 
					This.SelectText(1, 1000)
					Return 1
				end if
				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
																(currentrow, 'end_date'), "modified") then
				else
					SetMicroHelp('The date period can not overlap with any other detail date period.') 
					This.SelectText(1, 1000)
					Return 1
				end if
			end if
			
//		case "C"
//			ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount()
//			if ll_row_count > 1 then
//				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
//															(currentrow, 'start_date'), "modified") then
//				else
//					SetMicroHelp('The date period can not overlap with any other detail date period.') 
//					This.SelectText(1, 1000)
//					Return 1
//				end if
//				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
//																(currentrow, 'end_date'), "modified") then
//				else
//					SetMicroHelp('The date period can not overlap with any other detail date period.') 
//					This.SelectText(1, 1000)
//					Return 1
//				end if
//			end if
//			
//		case "O"
//			ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount()
//			if ll_row_count > 1 then
//				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
//															(currentrow, 'start_date'), "modified") then
//				else
//					SetMicroHelp('The date period can not overlap with any other detail date period.') 
//					This.SelectText(1, 1000)
//					Return 1
//				end if
//				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
//																(currentrow, 'end_date'), "modified") then
//				else
//					SetMicroHelp('The date period can not overlap with any other detail date period.') 
//					This.SelectText(1, 1000)
//					Return 1
//				end if
//			end if	
//		
//		case "I"
//			ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount()
//			if ll_row_count > 1 then
//				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
//															(currentrow, 'start_date'), "modified") then
//				else
//					SetMicroHelp('The date period can not overlap with any other detail date period.') 
//					This.SelectText(1, 1000)
//					Return 1
//				end if
//				if wf_check_dates(currentrow, tab_1.tabpage_detail.dw_detail.GetItemdate &
//																(currentrow, 'end_date'), "modified") then
//				else
//					SetMicroHelp('The date period can not overlap with any other detail date period.') 
//					This.SelectText(1, 1000)
//					Return 1
//				end if
//			end if				
//			
		case "V"
			ll_row_count = tab_1.tabpage_detail.dw_detail.rowcount()
			if ll_row_count > 1 then
				if wf_check_volumes('modified', currentrow, tab_1.tabpage_detail.dw_detail.GetItemnumber &
																(currentrow, 'minimum_target_volume')) then
				else
					SetMicroHelp('The volume can not overlap with any other detail volume.') 
					This.SelectText(1, 1000)
					Return 1
				end if
				if wf_check_volumes('modified', currentrow, tab_1.tabpage_detail.dw_detail.GetItemnumber &
																(currentrow, 'maximum_target_volume')) then
				else
					SetMicroHelp('The volume can not overlap with any other detail volume.') 
					This.SelectText(1, 1000)
					Return 1
				end if
			end if
	end choose
end if
end event

event clicked;u_AbstractParameterStack	lu_Stack

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
lu_Stack.uf_Initialize()

IF Row > 0 Then
	Choose Case dwo.name
		Case 'pricing_ind'
			If is_rebateoption = 'A' and is_rebatetype = 'P' Then
				//bypass - column should be protected
			Else	
				il_selected_row = row
				wf_get_selected_pricinginds(this)
				OpenWithParm(w_pricing_ind_resp, iw_this)
			End If
	End Choose
End If
			
			
end event

type tabpage_customergroup from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Customer Group"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_customergroup dw_customergroup
end type

on tabpage_customergroup.create
this.dw_customergroup=create dw_customergroup
this.Control[]={this.dw_customergroup}
end on

on tabpage_customergroup.destroy
destroy(this.dw_customergroup)
end on

type dw_customergroup from datawindow within tabpage_customergroup
integer x = 18
integer y = 32
integer width = 2875
integer height = 1084
integer taborder = 30
string title = "none"
string dataobject = "d_rebatecustomergrp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;u_AbstractParameterStack	lu_Stack

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
lu_Stack.uf_Initialize()

IF Row > 0 Then
	Choose Case dwo.name
		Case 'pricing_ind'
			il_selected_row = row
			wf_get_selected_pricinginds(this)
			OpenWithParm(w_pricing_ind_resp, iw_this)
	End Choose
End If
			
end event

event itemchanged;If dwo.name = 'pricing_incl_excl' Then
	This.AcceptText()
	if This.GetItemstring(row, 'pricing_incl_excl') = ' ' then	 
		This.SetItem(row, 'pricing_ind', ' ')	
	end if
End If

ib_changes = True

end event

event rbuttondown;m_sma_popup	lm_popmenu

this.setredraw(True)

lm_popmenu = Create m_sma_popup
lm_popmenu.m_popup.m_selectcustomergroups.Enabled = True
lm_popmenu.m_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

ib_changes = True
end event

event itemerror;Return 1
end event

type tabpage_customer from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Customer"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_customer dw_customer
end type

on tabpage_customer.create
this.dw_customer=create dw_customer
this.Control[]={this.dw_customer}
end on

on tabpage_customer.destroy
destroy(this.dw_customer)
end on

type dw_customer from datawindow within tabpage_customer
integer y = 8
integer width = 2930
integer height = 1116
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_rebatecustomer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rbuttondown;m_sma_popup	lm_popmenu

lm_popmenu = Create m_sma_popup
lm_popmenu.m_popup.m_selectcustomers.Enabled = True
lm_popmenu.m_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

ib_changes = True

end event

event itemchanged;Date					ldt_date


ldt_date = Date(data)

Choose Case dwo.Name
	Case 'start_date'
		If Not IsDate(data) or IsNull(data)	Then
			SetMicroHelp('Invalid Date.')
			This.SelectText(1,1000)
			Return 1
		End If
		tab_1.tabpage_header.dw_header.AcceptText()
		If ldt_date < tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date') Or &
				ldt_date > tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date') Or &
				ldt_date > This.GetItemDate(row, 'end_date') Then
			SetMicroHelp('The Start Date must be within the Rebate Header' + &
			' Dates and less than or equal to the End Date')
			This.SelectText(1, 1000)
			Return 1
		End If
	Case 'end_date'
		If Not IsDate(data) or IsNull(data)	Then
			SetMicroHelp('Invalid Date.')
			This.SelectText(1,1000)
			Return 1
		End If
		tab_1.tabpage_header.dw_header.AcceptText()
		If ldt_date < tab_1.tabpage_header.dw_header.GetItemDate(1, 'start_date') Or &
				ldt_date > tab_1.tabpage_header.dw_header.GetItemDate(1, 'end_date') Or &
				ldt_date < This.GetItemDate(row, 'start_date') Then
			SetMicroHelp('The Start Date must be within the Rebate Header' + &
			' Dates and greater than or equal to the Start Date')
			This.SelectText(1, 1000)
			Return 1
		End If
End Choose

ib_changes = True

end event

event doubleclicked;u_AbstractParameterStack	lu_Stack
String							ls_Date

// This script opens up the calendar when the correct column is double-clicked
// Let's make sure they double-clicked on a date or datetime column
If row <= 0 Then Return
If String(dwo.Type) <> "column" Then Return

If Not (String(dwo.ColType) = 'date' or String(dwo.ColType) = 'datetime') Then Return

ls_date = String(Today())

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
lu_Stack.uf_Initialize()

// Set up the parameters for the calendar window
lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
lu_stack.uf_Push("String", ls_Date)
lu_Stack.uf_Push("String", "d_calendar")
lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)

// Open the calendar window
iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
lu_Stack = Message.PowerObjectParm
If Not Isvalid( lu_stack ) Then Return
lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)

// Was it successful?  If so, get the date passed back and set the column.
If iu_ErrorContext.uf_IsSuccessful() Then
	lu_Stack.uf_Pop("String",ls_Date)
	If Len(Trim(ls_Date)) = 0 Then Return
	This.SetText(ls_date)
	If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
END IF

end event

event itemerror;Return 1

end event

event getfocus;This.SelectText(1, 1000)
end event

event itemfocuschanged;This.SelectText(1, 1000)

SetMicroHelp('Ready')

end event

type tabpage_division from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Division"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_division dw_division
dw_divisionexclude dw_divisionexclude
end type

on tabpage_division.create
this.dw_division=create dw_division
this.dw_divisionexclude=create dw_divisionexclude
this.Control[]={this.dw_division,&
this.dw_divisionexclude}
end on

on tabpage_division.destroy
destroy(this.dw_division)
destroy(this.dw_divisionexclude)
end on

type dw_division from datawindow within tabpage_division
integer y = 8
integer width = 2670
integer height = 1084
integer taborder = 40
boolean bringtotop = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;ib_changes = True

end event

event rbuttondown;m_sma_popup	lm_popmenu

lm_popmenu = Create m_sma_popup
lm_popmenu.m_popup.m_selectdivisions.Enabled = True
lm_popmenu.m_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

ib_changes = True

end event

event itemerror;Return 1
end event

type dw_divisionexclude from datawindow within tabpage_division
integer x = 2697
integer y = 32
integer width = 183
integer height = 324
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_checktoexclude"
boolean border = false
boolean livescroll = true
end type

event constructor;This.InsertRow(0)

end event

event itemchanged;tab_1.tabpage_header.dw_header.SetItem(1, 'division_exclude_ind', Data)
if ib_exclude_ind_div then
	ib_exclude_ind_div = false
else
	ib_exclude_ind_div = true
end if

end event

type tabpage_productgroup from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Product Group"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_prodgrp_exclude dw_prodgrp_exclude
dw_productgroupexclude dw_productgroupexclude
dw_include_all_volume dw_include_all_volume
dw_productgroup dw_productgroup
end type

on tabpage_productgroup.create
this.dw_prodgrp_exclude=create dw_prodgrp_exclude
this.dw_productgroupexclude=create dw_productgroupexclude
this.dw_include_all_volume=create dw_include_all_volume
this.dw_productgroup=create dw_productgroup
this.Control[]={this.dw_prodgrp_exclude,&
this.dw_productgroupexclude,&
this.dw_include_all_volume,&
this.dw_productgroup}
end on

on tabpage_productgroup.destroy
destroy(this.dw_prodgrp_exclude)
destroy(this.dw_productgroupexclude)
destroy(this.dw_include_all_volume)
destroy(this.dw_productgroup)
end on

type dw_prodgrp_exclude from datawindow within tabpage_productgroup
integer x = 2661
integer y = 252
integer width = 160
integer height = 296
integer taborder = 20
string dataobject = "d_checktoexclude"
boolean border = false
boolean livescroll = true
end type

type dw_productgroupexclude from datawindow within tabpage_productgroup
boolean visible = false
integer x = 2199
integer y = 16
integer width = 658
integer height = 96
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_checktoexclude"
boolean border = false
boolean livescroll = true
end type

event constructor;This.InsertRow(0)

end event

event itemchanged;tab_1.tabpage_header.dw_header.SetItem(1, 'product_group_exclude_ind', Data)
if ib_exclude_ind_grp then
	ib_exclude_ind_grp = false
else
	ib_exclude_ind_grp = true
end if

end event

type dw_include_all_volume from datawindow within tabpage_productgroup
integer x = 2638
integer width = 183
integer height = 248
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_include_all_volume"
boolean border = false
boolean livescroll = true
end type

event constructor;This.InsertRow(0)
end event

event itemchanged;long 	ll_rowcount, ll_count



ll_rowcount = dw_productgroup.rowcount()

for ll_count = 1 to ll_rowcount
	dw_productgroup.setitem(ll_count,"Include_all_volume",data)
next
end event

type dw_productgroup from datawindow within tabpage_productgroup
integer x = 9
integer y = 8
integer width = 2629
integer height = 1092
integer taborder = 50
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;If dwo.name = 'pricing_incl_excl' Then
	This.AcceptText()
	if This.GetItemstring(row, 'pricing_incl_excl') = ' ' then	 
		This.SetItem(row, 'pricing_ind', ' ')	
	end if
End If

ib_changes = True

end event

event rbuttondown;long 	ll_RowCount, &
		ll_count

if is_RebateOption = 'V' then
	ll_RowCount = tab_1.tabpage_productgroup.dw_productgroup.RowCount()
	for ll_count = 1 to ll_RowCount
		tab_1.tabpage_productgroup.dw_productgroup.SetItem(ll_count, 'seq_no',Integer(ll_count))
	next
end if

m_sma_popup	lm_popmenu

this.setredraw(True)

lm_popmenu = Create m_sma_popup
lm_popmenu.m_popup.m_selectproductgroups.Enabled = True
lm_popmenu.m_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

ib_changes = True

end event

event itemerror;Return 1
end event

event clicked;u_AbstractParameterStack	lu_Stack

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
lu_Stack.uf_Initialize()

IF Row > 0 Then
	Choose Case dwo.name
		Case 'pricing_ind'
			il_selected_row = row
			wf_get_selected_pricinginds(this)
			OpenWithParm(w_pricing_ind_resp, iw_this)
	End Choose
End If
			
end event

type tabpage_product from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Product"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_product dw_product
end type

on tabpage_product.create
this.dw_product=create dw_product
this.Control[]={this.dw_product}
end on

on tabpage_product.destroy
destroy(this.dw_product)
end on

type dw_product from datawindow within tabpage_product
integer width = 2725
integer height = 1108
integer taborder = 30
boolean bringtotop = true
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;If dwo.name = 'pricing_incl_excl' Then
	This.AcceptText()
	if This.GetItemstring(row, 'pricing_incl_excl') = ' ' then	 
		This.SetItem(row, 'pricing_ind', ' ')	
	end if
End If

ib_changes = True

end event

event rbuttondown;m_sma_popup	lm_popmenu

lm_popmenu = Create m_sma_popup
lm_popmenu.m_popup.m_selectproducts.Enabled = True
lm_popmenu.m_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

ib_changes = True
end event

event clicked;u_AbstractParameterStack	lu_Stack

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)
lu_Stack.uf_Initialize()

IF Row > 0 Then
	Choose Case dwo.name
		Case 'pricing_ind'
			il_selected_row = row
			wf_get_selected_pricinginds(this)
			OpenWithParm(w_pricing_ind_resp, iw_this)
	End Choose
End If
			
end event

event itemerror;Return 1
end event

type tabpage_location from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2944
integer height = 1332
long backcolor = 79741120
string text = "Location"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_locationexclude dw_locationexclude
dw_location dw_location
end type

on tabpage_location.create
this.dw_locationexclude=create dw_locationexclude
this.dw_location=create dw_location
this.Control[]={this.dw_locationexclude,&
this.dw_location}
end on

on tabpage_location.destroy
destroy(this.dw_locationexclude)
destroy(this.dw_location)
end on

type dw_locationexclude from datawindow within tabpage_location
integer x = 2734
integer y = 84
integer width = 183
integer height = 324
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_checktoexclude"
boolean border = false
boolean livescroll = true
end type

event constructor;This.InsertRow(0)

end event

event itemchanged;tab_1.tabpage_header.dw_header.SetItem(1, 'division_exclude_ind', Data)
if ib_exclude_ind_div then
	ib_exclude_ind_div = false
else
	ib_exclude_ind_div = true
end if

end event

type dw_location from datawindow within tabpage_location
integer x = 23
integer y = 24
integer width = 2693
integer height = 1092
integer taborder = 40
string title = "none"
string dataobject = "d_rebatelocation"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rbuttondown;m_sma_popup	lm_popmenu

lm_popmenu = Create m_sma_popup
lm_popmenu.m_popup.m_selectlocations.Enabled = True
lm_popmenu.m_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

ib_changes = True

end event

event itemerror;Return 1
end event

event clicked;ib_changes = True
end event

