﻿$PBExportHeader$u_plantlocaldb.sru
forward
global type u_plantlocaldb from u_abstractplantdataaccess
end type
end forward

global type u_plantlocaldb from u_abstractplantdataaccess
end type
global u_plantlocaldb u_plantlocaldb

type variables
u_AbstractTransaction	iu_ODBCTransaction


end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);u_AbstractClassFactory					lu_ClassFactory

u_AbstractErrorContext				 	lu_ErrorContext

u_AbstractDefaultManager				lu_DefaultManager


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject( "u_ODBCTransaction", iu_ODBCTransaction, lu_ErrorContext ) Then
	If Not lu_ClassFactory.uf_GetObject( "u_iniDefaultManager", lu_DefaultManager, lu_ErrorContext ) Then
		If lu_ErrorContext.uf_IsSuccessful( ) Then
			lu_DefaultManager.uf_Initialize( lu_ClassFactory, 'ibp002.ini', 'SMA DATABASE', lu_ErrorContext )
			If Not lu_ErrorContext.uf_IsSuccessful() Then
				au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
				Return False
			End If
		End If
	End If

	If lu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ODBCTransaction.uf_Initialize( lu_ClassFactory, lu_DefaultManager, lu_ErrorContext )
		If Not lu_ErrorContext.uf_IsSuccessful() Then
			au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
			au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
			Return False
		End If
	End If
End If

Return True
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_plants

String							ls_PlantCode, &
									ls_PlantCodeString, &
									ls_ErrorString, &
									ls_PlantType, &
									ls_BeginLocation, &
									ls_EndLocation, &
									lsa_PlantTypes[]

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('string', ls_PlantCode)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If lu_string.nf_IsEmpty(ls_PlantCode) Then
	ls_BeginLocation = '000'
	ls_EndLocation = 'ZZZ'
Else
	ls_BeginLocation = ls_PlantCode
	ls_EndLocation = ls_PlantCode
End If

  SELECT tutltypes.type_short_desc
    INTO :ls_planttype  
    FROM tutltypes

   WHERE ( tutltypes.record_type = 'PLTGROUP' ) AND  
         ( tutltypes.type_desc = 'ALL PLANTS  ' )   
	Using iu_ODBCTransaction;

lds_Plants = Create DataStore
lds_Plants.DataObject = 'd_plants'
lds_Plants.SetTransObject(iu_ODBCTransaction)

ls_PlantType = Trim(ls_PlantType)
li_Count = 0
Do While Len(ls_PlantType) > 0
	li_Count ++
	lsa_PlantTypes[li_Count] = Right(ls_PlantType, 1)
	ls_PlantType = Left(ls_PlantType, Len(ls_PlantType) - 1)
Loop


lds_Plants.Retrieve(lsa_planttypes[], ls_BeginLocation, ls_EndLocation)
ls_PlantCodeString = lds_Plants.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_PlantCodeString)

return true

end function

on u_plantlocaldb.create
call super::create
end on

on u_plantlocaldb.destroy
call super::destroy
end on

