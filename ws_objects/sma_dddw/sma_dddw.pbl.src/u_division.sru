﻿$PBExportHeader$u_division.sru
$PBExportComments$DDDW for div with code and mathing descr field
forward
global type u_division from userobject
end type
type dw_division from datawindow within u_division
end type
end forward

global type u_division from userobject
integer width = 1253
integer height = 108
long backcolor = 79741120
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_division dw_division
end type
global u_division u_division

type variables
u_abstracterrorcontext		iu_errorcontext
//u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractTutlTypeDataAccess	iu_TutlTypeDataAccess
u_AbstractClassFactory		iu_ClassFactory
w_smaframe			iw_frame	
u_NotificationController		iu_Notification	
end variables

forward prototypes
public function boolean uf_fillindivision ()
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_fillindivision ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	

lu_ParameterStack = iu_parameterstack

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'DIVCODE'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_division.GetChild('div_code', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

iu_parameterstack = au_parameterstack

//If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
//	iu_Notification.uf_Initialize(iu_ClassFactory)
//End If
//
If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

return true
end function

on u_division.create
this.dw_division=create dw_division
this.Control[]={this.dw_division}
end on

on u_division.destroy
destroy(this.dw_division)
end on

type dw_division from datawindow within u_division
integer x = 5
integer y = 12
integer width = 1239
integer height = 88
integer taborder = 10
string dataobject = "d_divisiondddw"
boolean border = false
boolean livescroll = true
end type

event constructor;DataWindowChild					ldwc_code, &
										ldwc_descr
										

This.InsertRow(0)

this.GetChild('div_code', ldwc_code)
this.GetChild('div_description', ldwc_descr)
ldwc_code.ShareData(ldwc_descr)

this.setitem(1,"div_description",this.getitemstring(1,"div_code"))
end event

event itemchanged;DataWindowChild						ldwc_divList

This.GetChild('div_code', ldwc_divList)

If ldwc_divList.Find('type_code = "' + data + '"', &
		1, ldwc_divList.RowCount() + 1) <= 0 Then
	messagebox('Invalid Data','This is an invalid Division')
	This.SetFocus()
	This.SelectText(1, 1000)
	Return 1
End If

this.setitem(1,"div_description",data)
end event

event itemerror;Return 1
end event

event itemfocuschanged;this.accepttext()
end event

