﻿$PBExportHeader$u_abstractplantdataaccess.sru
forward
global type u_abstractplantdataaccess from nonvisualobject
end type
end forward

global type u_abstractplantdataaccess from nonvisualobject
end type
global u_abstractplantdataaccess u_abstractplantdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return true

end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_abstractplantdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_abstractplantdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

