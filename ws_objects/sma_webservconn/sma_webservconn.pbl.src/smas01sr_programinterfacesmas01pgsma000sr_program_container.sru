﻿$PBExportHeader$smas01sr_programinterfacesmas01pgsma000sr_program_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterfaceSmas01pgSma000sr_program_container from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterfaceSmas01pgSma000sr_program_container from nonvisualobject
end type

type variables
    int sma000sr_rval
    string sma000sr_message
    ulong sma000sr_task_num
    ulong sma000sr_max_record_num
    ulong sma000sr_last_record_num
    uint sma000sr_version_number
end variables

on smas01sr_ProgramInterfaceSmas01pgSma000sr_program_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterfaceSmas01pgSma000sr_program_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

