﻿$PBExportHeader$smas01sr_charcontainertype.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_charContainerType from nonvisualobject
    end type
end forward

global type smas01sr_charContainerType from nonvisualobject
end type

type variables
    string Value
end variables

on smas01sr_charContainerType.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_charContainerType.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

