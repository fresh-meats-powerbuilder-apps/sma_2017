﻿$PBExportHeader$w_groupmaintcpy.srw
forward
global type w_groupmaintcpy from w_abstractresponse
end type
type sle_1 from singlelineedit within w_groupmaintcpy
end type
type st_2 from statictext within w_groupmaintcpy
end type
type st_1 from statictext within w_groupmaintcpy
end type
end forward

global type w_groupmaintcpy from w_abstractresponse
integer x = 439
integer y = 352
integer width = 1234
integer height = 660
string title = ""
sle_1 sle_1
st_2 st_2
st_1 st_1
end type
global w_groupmaintcpy w_groupmaintcpy

type variables
Boolean			save 
String			is_original, is_option

end variables

on w_groupmaintcpy.create
int iCurrent
call super::create
this.sle_1=create sle_1
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_1
end on

on w_groupmaintcpy.destroy
call super::destroy
destroy(this.sle_1)
destroy(this.st_2)
destroy(this.st_1)
end on

event open;call super::open;//iu_grpmaint = Message.PowerObjectParm

is_original = Message.StringParm


end event

event ue_postopen;String			ls_text, ls_groupdesc, ls_grouptype


u_String_Functions		lu_string

This.SetRedraw(False)

//If isValid(iu_grpmaint) Then
//	is_original = iu_grpmaint.uf_getstring("DESCRIPTION")
//	is_option = iu_grpmaint.uf_getstring("ADDMOD")
//	ls_text = iu_grpmaint.uf_getstring("COPYTEXT")
//Else
//	MessageBox("Error","invalid parameters")
//End If

ls_groupdesc = lu_string.nf_GetToken(is_original, '~t')
ls_grouptype = lu_string.nf_GetToken(is_original, '~t')

sle_1.Text = Trim(ls_groupdesc)

sle_1.SelectText(1, Len(Trim(ls_groupdesc)) )
sle_1.SetFocus()

If ls_grouptype = 'P' Then
	st_1.Text = "Enter Name For New Product Group"
	This.Title = "Creating New Product Group"
Else
	If ls_grouptype = 'C' Then
		st_1.Text = "Enter Name For New Customer Group"
		This.Title = "Creating New Customer Group"
	Else
		st_1.Text = "Enter Name For New Location Group"
		This.Title = "Creating New Location Group"
	End If
End If

This.SetRedraw(True)
	
end event

type cb_help from w_abstractresponse`cb_help within w_groupmaintcpy
integer x = 1275
integer y = 396
integer width = 238
integer taborder = 0
string text = "Help"
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_groupmaintcpy
integer x = 878
integer y = 384
integer taborder = 30
boolean cancel = true
end type

event cb_cancel::clicked;CloseWithReturn(Parent, "")
end event

type cb_ok from w_abstractresponse`cb_ok within w_groupmaintcpy
integer x = 576
integer y = 384
integer width = 279
integer taborder = 20
boolean default = true
end type

event cb_ok::clicked;String	ls_return


//IF sle_1.AcceptText() = -1 THEN
//	Return
//END IF

ls_return = sle_1.Text

//If trim(ls_return) = trim(is_original) Then
//	MessageBox("Error","Description must be different than original")
//	Return
//End If

CloseWithReturn(Parent, ls_return)
	
end event

type sle_1 from singlelineedit within w_groupmaintcpy
integer x = 46
integer y = 232
integer width = 1115
integer height = 88
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean autohscroll = false
textcase textcase = upper!
integer limit = 40
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_groupmaintcpy
integer x = 46
integer y = 168
integer width = 311
integer height = 56
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Description:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_groupmaintcpy
integer x = 46
integer y = 48
integer width = 1102
integer height = 84
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
boolean focusrectangle = false
end type

