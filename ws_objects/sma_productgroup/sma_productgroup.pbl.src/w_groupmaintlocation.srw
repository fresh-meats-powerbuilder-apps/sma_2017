﻿$PBExportHeader$w_groupmaintlocation.srw
forward
global type w_groupmaintlocation from w_abstractresponse
end type
type dw_locations from u_abstractdw within w_groupmaintlocation
end type
type dw_header from u_abstractdw within w_groupmaintlocation
end type
type dw_selected_loc from u_abstractdw within w_groupmaintlocation
end type
type cb_select_loc from commandbutton within w_groupmaintlocation
end type
type cb_remove from commandbutton within w_groupmaintlocation
end type
type cb_remove_all from commandbutton within w_groupmaintlocation
end type
type dw_grouptype from u_abstractdw within w_groupmaintlocation
end type
type st_comments from statictext within w_groupmaintlocation
end type
type mle_comment from multilineedit within w_groupmaintlocation
end type
end forward

global type w_groupmaintlocation from w_abstractresponse
integer x = 14
integer y = 40
integer width = 3054
integer height = 1752
string title = "Location Group Maintenance Definition"
dw_locations dw_locations
dw_header dw_header
dw_selected_loc dw_selected_loc
cb_select_loc cb_select_loc
cb_remove cb_remove
cb_remove_all cb_remove_all
dw_grouptype dw_grouptype
st_comments st_comments
mle_comment mle_comment
end type
global w_groupmaintlocation w_groupmaintlocation

type variables
Boolean	save 
Long	il_CurrentRowSelected, il_grp_id

String	is_WhatWasTyped, is_owner, is_option
Integer	ii_keysTyped = 2

u_grpmaintenance		iu_grpmaint
end variables

forward prototypes
public function boolean wf_update_header ()
public function boolean wf_update_locations ()
public function boolean wf_location_selected (string as_location)
public function string wf_reformat_text (string as_reformat)
end prototypes

public function boolean wf_update_header ();String	ls_desc, ls_string, ls_dummy, ls_grouptypecode, ls_comment
Boolean	lb_rtn
Long		ll_idx

ls_dummy = space(1)
ls_desc = trim(dw_header.Object.location_grp_name[1])
ls_grouptypecode = dw_grouptype.GetItemString(1, "group_type")
ls_comment = wf_reformat_text(mle_comment.Text)

If len(ls_desc) > 0 Then
	If is_option = "MOD" Then
		lb_rtn = iu_grpmaint.uf_update_db(is_option,"H",ls_desc, ls_grouptypecode, "", ls_comment)
		Return lb_rtn
	End If
Else
	iu_grpmaint.uf_setmicrohelp("No Description Specified")
	Return False
End If
//
ll_idx = dw_selected_loc.GetNextModified(0, Primary!)
If ll_idx > 0 then
	ls_string = dw_selected_loc.Object.location_code[ll_idx] + '~t' 
	ls_string += ls_dummy + '~t'
	ll_idx = dw_selected_loc.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += dw_selected_loc.Object.location_code[ll_idx]  + '~t' 
		ls_string += ls_dummy + '~t'
		ll_idx = dw_selected_loc.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"H", ls_desc, ls_grouptypecode, ls_string, ls_comment)
End If

if dw_header.GetItemString(1 , 'location_grp_name') = "NEW LOCATIONS" Then
	ls_string = ""
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"H", ls_desc, ls_grouptypecode, ls_string, ls_comment)
End if

Return lb_rtn
end function

public function boolean wf_update_locations ();Long			ll_idx, ll_count
String		ls_string, ls_dummy
Boolean		lb_rtn

ls_dummy = space(1)
If is_option = "NEW" Then Return True
lb_rtn = True
// Deleteing Locations
ll_count = dw_selected_loc.DeletedCount()
If ll_count > 0 Then
	ls_string = dw_selected_loc.GetItemString(1, "location_code", Delete!, False) + '~t'
	
	If ll_count > 1 Then
		For ll_idx = 2 To ll_count
			ls_string += "~t" + dw_selected_loc.GetItemString(ll_idx, "location_code", Delete!, False)
			ls_string += ls_dummy + '~t'
		Next
	End If
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"D",'', '', ls_string, '')
End If

//////////////////////////////
// Adding - Modify Locations //
//////////////////////////////
ll_idx = dw_selected_loc.GetNextModified(0, Primary!)

If ll_idx > 0 Then
	ls_string = dw_selected_loc.Object.location_code[ll_idx] + '~t'
	ls_string += ls_dummy + '~t'
	
	ll_idx = dw_selected_loc.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += dw_selected_loc.Object.location_code[ll_idx] + '~t'
		ls_string += ls_dummy + '~t'
		ll_idx = dw_selected_loc.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"P",'', '', ls_string, '')
End If

Return lb_rtn
// // //
end function

public function boolean wf_location_selected (string as_location);Long		ll_row
String	ls_expesion


ls_expesion = "location_code = '" + as_location + "'"

ll_row = dw_selected_loc.Find(ls_expesion, 1, dw_selected_loc.RowCount())

If ll_row > 0 Then
	Return True
Else
	Return False
End IF
end function

public function string wf_reformat_text (string as_reformat);Long		ll_PlaceHolder, ll_len

String		ls_ReturnString, ls_first_line, ls_second_line, ls_third_line, ls_fourth_line, ls_fifth_line

ll_PLaceHolder = Pos(as_reformat, "~r~n")

If ll_PlaceHolder < 1 then
	ls_ReturnString = as_reformat
else
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_first_line = mid(as_reformat, 1, 40)
		as_reformat = mid(as_reformat, 41)	
	else
		ls_first_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_first_line)
	ls_first_line = ls_first_line + space (40 - ll_len)

	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_second_line = mid(as_reformat, 1, 40)
		as_reformat = mid(as_reformat, 41)	
	else
		ls_second_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if		
	ll_len = len(ls_second_line)
	ls_second_line = ls_second_line + space (40 - ll_len)
		
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_third_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		ls_third_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_third_line)
	ls_third_line = ls_third_line + space (40 - ll_len)
	
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_fourth_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		ls_fourth_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_fourth_line)
	ls_fourth_line = ls_fourth_line + space (40 - ll_len)
	
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_fifth_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		as_reformat = mid(as_reformat, ll_PlaceHolder - 1)
		ls_fifth_line = mid(as_reformat, 1, ll_PlaceHolder + 2)
	end if		
	ll_len = len(ls_fifth_line)
	ls_fifth_line = ls_fifth_line + space (40 - ll_len)
	
	ls_ReturnString = ls_first_line + ls_second_line + ls_third_line + ls_fourth_line + ls_fifth_line
	
end if	

Return ls_ReturnString
	
end function

on w_groupmaintlocation.create
int iCurrent
call super::create
this.dw_locations=create dw_locations
this.dw_header=create dw_header
this.dw_selected_loc=create dw_selected_loc
this.cb_select_loc=create cb_select_loc
this.cb_remove=create cb_remove
this.cb_remove_all=create cb_remove_all
this.dw_grouptype=create dw_grouptype
this.st_comments=create st_comments
this.mle_comment=create mle_comment
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_locations
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.dw_selected_loc
this.Control[iCurrent+4]=this.cb_select_loc
this.Control[iCurrent+5]=this.cb_remove
this.Control[iCurrent+6]=this.cb_remove_all
this.Control[iCurrent+7]=this.dw_grouptype
this.Control[iCurrent+8]=this.st_comments
this.Control[iCurrent+9]=this.mle_comment
end on

on w_groupmaintlocation.destroy
call super::destroy
destroy(this.dw_locations)
destroy(this.dw_header)
destroy(this.dw_selected_loc)
destroy(this.cb_select_loc)
destroy(this.cb_remove)
destroy(this.cb_remove_all)
destroy(this.dw_grouptype)
destroy(this.st_comments)
destroy(this.mle_comment)
end on

event open;call super::open;
This.SetRedraw(False)

iu_grpmaint = Message.PowerObjectParm

If isValid(iu_grpmaint) Then
	is_owner = iu_grpmaint.uf_getstring("OWNER")
	is_option = iu_grpmaint.uf_getstring("ADDMOD")
Else
	MessageBox("Error","invalid parameters")
End If

dw_locations.SetTransObject (SQLCA)
dw_locations.Retrieve()


end event

event ue_postopen;String			ls_div, ls_filter, ls_name, ls_locations, ls_groupcomment

dw_header.InsertRow(0)
dw_header.SetRow(1)

If is_option = "NEW" Then
	dw_header.SetColumn(3)
Else
	ls_locations = iu_grpmaint.uf_getstring("LOCATIONS")	
	dw_selected_loc.ImportString(ls_locations)
	
	ls_name = iu_grpmaint.uf_getstring("DESCRIPTION")	
	dw_header.Object.location_grp_name[1] = Trim(ls_name)
	dw_header.SetColumn(5)
	
	ls_groupcomment = iu_grpmaint.uf_getstring("GROUP_COMMENT")	
	mle_comment.Text = righttrim(ls_groupcomment)	
End If

dw_selected_loc.ResetUpdate()

dw_header.SetFocus()

This.SetRedraw(True)
end event

type cb_help from w_abstractresponse`cb_help within w_groupmaintlocation
integer x = 2706
integer y = 280
integer width = 279
integer taborder = 0
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_groupmaintlocation
integer x = 2706
integer y = 148
integer taborder = 90
boolean cancel = true
end type

event clicked;CloseWithReturn(Parent, "CANCEL")
end event

type cb_ok from w_abstractresponse`cb_ok within w_groupmaintlocation
integer x = 2706
integer y = 20
integer width = 279
integer taborder = 80
end type

event cb_ok::clicked;String	ls_header, ls_locations
Long		ll_rows

if isnull(dw_grouptype.GetItemString(1, "group_type")) or (dw_grouptype.GetItemString(1, "group_type") <= "        ") then
	dw_grouptype.SetItem(1, "group_type",  "        ")
end if

IF dw_selected_loc.AcceptText() = -1 THEN
	SetMicroHelp("error in location list")
	Return
END IF
IF dw_header.AcceptText() = -1 THEN
	iu_grpmaint.uf_setmicrohelp("Error in Group Description")
	Return
END IF

ll_rows = dw_selected_loc.RowCount()

//If ll_rows < 2 and dw_header.GetItemString(1 , 'location_grp_name') <> "NEW LOCATIONS" Then 
//	iu_grpmaint.uf_setmicrohelp("Must select 2 or more locations")
//	MessageBox("Group Maintenance","Please select 2 or more locations.")
//	Return
//End If

SetPointer(HourGlass!)

If wf_update_header() Then
	If wf_update_locations() Then
		SetPointer(Arrow!)
		CloseWithReturn(Parent, "OK")
	End If
End If
SetPointer(Arrow!)
end event

type dw_locations from u_abstractdw within w_groupmaintlocation
integer y = 528
integer width = 1385
integer height = 1128
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_select_location"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;SetMicroHelp("Loading Location Codes")
SetPointer(HourGlass!)
This.SetTransObject(sqlca)
This.Retrieve()
SetPointer(Arrow!)
SetMicroHelp("Ready")

end event

event clicked;If row > 0 Then
	If This.IsSelected(row) Then
		This.SelectRow(row,False)
	Else
		This.SelectRow(row,True)
	End If
End If
end event

type dw_header from u_abstractdw within w_groupmaintlocation
event ue_another_key pbm_dwnkey
integer width = 1157
integer height = 332
boolean bringtotop = true
string dataobject = "d_locgrpdefheader"
borderstyle borderstyle = stylebox!
end type

event ue_another_key;Choose Case key
	Case	KeyBack!
		save = False
		ii_keystyped = Len(This.GetText())+ 1
	Case KeyLeftArrow!   
		ii_keystyped --
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		save = False		
		ii_keystyped = Len(This.GetText())
END choose

end event

event constructor;DataWindowChild	ldwc_div

//This.InsertRow(0)

end event

event itemchanged;String		ls_filter, ls_div

u_TreeViewFunctions		lu_TreeViewFunctions


Choose Case dwo.name
	Case "location_grp_name"
		this.object.business_rule[row] = "M"
		If Not iu_grpmaint.uf_checkfordupdescr(data) Then 
			MessageBox("Error","You cannot have duplicate Location Group Descriptions.")
			This.SetFocus()
			This.SelectText(1, 1000)
			Return 1
		End If
	Case Else
		
End Choose
end event

event editchanged;Long		ll_row, ll_first_row, ll_last_row
String	ls_FindString			

IF KEYDOWN(KeyBack!) OR KEYDOWN(KeyLeftArrow!) OR KEYDOWN(KeyEnd!) OR  KEYDOWN(KeyHome!) OR  KEYDOWN( KeyRightArrow!)&
							OR KEYDOWN(Keydelete!) Then RETURN

ii_keystyped++

If ii_keystyped > Len(data) Then ii_keystyped = Len(data) + 1

Choose Case String(dwo.Name)
	Case 'location_code'
			save = Not Save			
			IF Not save Then Return
			is_whatwastyped += Right(Data,1)
			ls_FindString = "location_code >= '"+Trim(Left(Data,ii_keystyped))+"'"
	Case Else
		Return
End Choose
ll_Row = dw_locations.Find( ls_FindString, 1, dw_locations.RowCount()+1)

If ll_row > 0 Then 
	if String(dwo.Name) =  'location_code' Then
		This.SetText(dw_locations.GetItemString(ll_row,"location_code"))
		This.SelectText(ii_keystyped,10)
		IF ll_row + 1 <= dw_locations.RowCount() Then
			dw_locations.SelectRow( il_CurrentRowSelected, fALSE)
			dw_locations.SelectRow( ll_row, TRUE)
			il_CurrentRowSelected = ll_row
		END IF
	END IF
	dw_locations.ScrollToRow(ll_row)
	
	dw_locations.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_locations.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_locations.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_locations.SetRedraw(False)
	dw_locations.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_locations.ScrollToRow(ll_row)
	dw_locations.SetRow(ll_row + 1)
	dw_locations.SetRedraw(True)
End If

ii_keystyped++


end event

event itemerror;Return 1
end event

event itemfocuschanged;ii_keystyped = 2
end event

type dw_selected_loc from u_abstractdw within w_groupmaintlocation
integer x = 1742
integer y = 528
integer width = 1234
integer height = 1128
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_locationselected"
boolean vscrollbar = true
end type

event clicked;If row > 0 Then
	If this.IsSelected(row) Then
		This.SelectRow(row,False)
	Else
		This.SelectRow(row,True)
	End If	
End If
end event

type cb_select_loc from commandbutton within w_groupmaintlocation
integer x = 1472
integer y = 752
integer width = 160
integer height = 108
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = ">"
end type

event clicked;Integer		li_row = 0, li_new
String		ls_location, ls_userid

ls_userid = iu_grpmaint.uf_getuserid()

li_row = dw_locations.GetSelectedRow(li_row)
If li_row = 0 Then Return
Do
	ls_location = dw_locations.Object.location_code[li_row]
	If Not wf_location_selected(ls_location) Then
		li_new = dw_selected_loc.InsertRow(0)
		dw_selected_loc.object.location_code[li_new] = ls_location
		dw_selected_loc.object.last_update_date[li_new] = today()
		dw_selected_loc.object.last_update_userid[li_new] = ls_userid		
	End If
	li_row = dw_locations.GetSelectedRow(li_row)
Loop Until li_row = 0

dw_locations.SelectRow(0,False)
dw_selected_loc.Sort()

end event

type cb_remove from commandbutton within w_groupmaintlocation
integer x = 1472
integer y = 1184
integer width = 160
integer height = 108
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<<"
end type

event clicked;
//////////////////////dw_selected_loc.Reset()

Integer		li_row

This.SetRedraw(False)
//li_row = dw_selected_loc.RowCount()

//For li_new = 1 to li_row
////	dw_selected_loc.SelectRow(li_new, True)
//	dw_selected_loc.DeleteRow(li_new)
//Next
Do
	li_row = dw_selected_loc.RowCount()
	If li_row > 0 Then dw_selected_loc.DeleteRow(1)
Loop Until li_row <= 0
//dw_selected_loc.SelectRow(0,False)
This.SetRedraw(TRUE)

end event

type cb_remove_all from commandbutton within w_groupmaintlocation
integer x = 1472
integer y = 1048
integer width = 160
integer height = 108
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<"
end type

event clicked;Integer		li_row, li_new

li_row = dw_selected_loc.GetSelectedRow(0)
If li_row = 0 Then Return
Do
	dw_selected_loc.DeleteRow(li_row)
	li_row = dw_selected_loc.GetSelectedRow(0)
Loop Until li_row = 0

dw_selected_loc.SelectRow(0,False)

end event

type dw_grouptype from u_abstractdw within w_groupmaintlocation
integer y = 424
integer width = 1559
integer height = 88
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_group_type"
end type

event constructor;call super::constructor;DataWindowChild	ldwc_child
Long	ll_row

If This.RowCount() = 0 Then This.InsertRow(0)

dw_grouptype.GetChild('group_type', ldwc_child)

ldwc_child.SetTransObject(sqlca)
ldwc_child.Reset()
ldwc_child.Retrieve('GRPTYPEL')
// add blank row for group type <= spaces
ll_row = ldwc_child.InsertRow(0)
ldwc_child.SetItem(ll_row, "type_code", '        ')
ldwc_child.SetItem(ll_row, "type_desc", 'GroupType not found or No Group Entered')
end event

type st_comments from statictext within w_groupmaintlocation
integer x = 1179
integer width = 434
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Group Comments"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type mle_comment from multilineedit within w_groupmaintlocation
integer x = 1166
integer y = 76
integer width = 1518
integer height = 332
integer taborder = 100
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Fixedsys"
long textcolor = 33554432
textcase textcase = upper!
integer limit = 200
borderstyle borderstyle = stylelowered!
end type

