﻿$PBExportHeader$u_treeviewfunctions.sru
forward
global type u_treeviewfunctions from nonvisualobject
end type
end forward

global type u_treeviewfunctions from nonvisualobject autoinstantiate
end type

forward prototypes
public function boolean uf_checkfordupdescriptions (string as_description, long al_parenthandle, treeview atv_tree)
public function long uf_getselectedparent (treeview atv_tree)
end prototypes

public function boolean uf_checkfordupdescriptions (string as_description, long al_parenthandle, treeview atv_tree);Long						ll_handle

TreeViewItem			ltvi_group


ll_handle = atv_tree.FindItem(ChildTreeItem!, al_parenthandle)
If  ll_handle < 0 Then Return True

Do While ll_handle > 0
	atv_tree.GetItem(ll_handle, ltvi_group)
	If Trim(as_description) = Trim(String(ltvi_group.label)) Then Return False
	ll_handle = atv_tree.FindItem(NextTreeItem!, ll_handle)
Loop

Return True
end function

public function long uf_getselectedparent (treeview atv_tree);Long				ll_handle

ll_handle = atv_tree.FindItem(CurrentTreeItem!, 0)
If ll_handle <= 0 Then Return -1

Return atv_tree.FindItem(ParentTreeItem!, ll_handle)
end function

on u_treeviewfunctions.create
TriggerEvent( this, "constructor" )
end on

on u_treeviewfunctions.destroy
TriggerEvent( this, "destructor" )
end on

