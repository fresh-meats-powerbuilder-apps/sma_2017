﻿$PBExportHeader$u_grpmaintenance.sru
forward
global type u_grpmaintenance from nonvisualobject
end type
end forward

global type u_grpmaintenance from nonvisualobject
end type
global u_grpmaintenance u_grpmaintenance

type variables
u_ProdGrpList		ilv_list
u_ProdGrpTree		itv_tree
w_groupmaintenance	iw_grpmaint
u_st_scroll			ist_scroll

u_groupdataaccess	iu_groupdataaccess
u_NotificationController		iu_Notification	

u_AbstractClassFactory			iu_ClassFactory
u_ErrorContext				iu_ErrorContext


long		il_treehandle, &
			il_currenthandle, &
			il_ownerhandle, &
			il_groupind
			
String		is_updateowner, &
			is_copytext, &
			is_selected_owner, &
			is_currentlabel
			
Boolean		ib_newgroup

Datastore	ids_group_list, &
				ids_group_item_list

end variables

forward prototypes
public function boolean uf_inquire ()
public subroutine uf_resize (window aw_window)
public function boolean uf_new ()
public subroutine uf_delete ()
public subroutine uf_tl_populate_grps (string as_owner, long al_handle)
public subroutine uf_tl_selectionchange (long al_handle)
public subroutine uf_list_groups (string as_owner)
public function string uf_getproddescr (string as_product)
public function string uf_getstring (string as_type)
public subroutine uf_pointerxy (ref long pointerx, ref long pointery)
public subroutine uf_setmicrohelp (string as_input)
public subroutine uf_expandupdateowner (long al_handle)
public function long uf_getownerhandle ()
public function boolean uf_checkfordupdescr (string as_descr)
public subroutine uf_load_root ()
public subroutine uf_retrieve_productgroups ()
public subroutine uf_retrieveproductgroups ()
public subroutine uf_retrievegroups ()
public function long uf_retrievegroupitems (long al_group_id)
public subroutine uf_list_products (long al_groupid)
public function boolean uf_initializesma (string as_grouptype, ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_add_group_items (long al_groupid, string as_itemstring)
public function long uf_update_group (long al_group_id, string as_group_name)
public function boolean uf_delete_group_items (long al_groupid, string as_itemstring)
public function string uf_getgrouptype ()
public subroutine uf_list_customers (long al_groupid)
public function string uf_getcustdescr (string as_customer_code, string as_customer_type)
public subroutine uf_setgroupind (long al_groupind)
public subroutine uf_setnewgroupind (boolean ab_newgroupind)
public subroutine uf_list_locations (long al_groupid)
public function string uf_getlocdescr (string as_location)
public subroutine uf_tl_newgrp ()
public function boolean uf_deletegroup (long al_grpid)
public function string uf_getuserid ()
public function long uf_add_group (string as_group_desc, string as_group_type_desc, string as_group_comment)
public function boolean uf_update_db (string as_option, string as_type, string as_description, string as_grouptypecode, string as_detail, readonly string as_comment)
public subroutine uf_copygroup (string as_description, long al_groupind, string as_comment)
public function string uf_getgroupcomment (long al_group_id)
public function boolean uf_initialize (listview alv, treeview atv, window aw, statictext ast)
public function long uf_update_group (long al_group_id, string as_group_desc, string as_group_type_code, string as_comment)
public function boolean uf_initializecas (string as_grouptype, ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_inquire ();Long		ll_handle


SetPointer(HourGlass!)

// reload root level (sales, sched, smacc, etc.)
uf_load_root()
ll_handle = il_ownerhandle
// clear list view
ilv_list.EVENT DYNAMIC ue_clear()

SetPointer(HourGlass!)
// retrieve product groups

uf_retrievegroups()

uf_expandupdateowner(ll_handle)

SetPointer(Arrow!)

Return True
end function

public subroutine uf_resize (window aw_window);itv_tree.X = 1
itv_tree.Resize(ilv_list.x - 20, aw_window.height - itv_tree.y - 150)

ilv_list.X = ist_scroll.x + 20
ilv_list.Resize(aw_window.width - itv_tree.x + 20,  aw_window.height - ilv_list.y - 150)

ist_scroll.Resize(20, aw_window.height - itv_tree.y - 150)
end subroutine

public function boolean uf_new ();
uf_tl_newgrp()

Return True
end function

public subroutine uf_delete ();//ilv_list.EVENT ue_delete()
end subroutine

public subroutine uf_tl_populate_grps (string as_owner, long al_handle);treeviewitem	ltvi_child
Long		 		ll_count, ll_index, ll_pic_idx , ll_handle

as_owner = righttrim(as_owner)
Choose Case String(as_owner)
	Case "SALES"
		ll_pic_idx = 5
	Case "SCHED"
		ll_pic_idx = 6
	Case "SMACC"
		ll_pic_idx = 7
	Case Else
		ll_pic_idx = 4
End Choose

ltvi_child.PictureIndex = ll_pic_idx
ltvi_child.SelectedPictureIndex = 2
ltvi_child.Children = False

ll_count = ids_group_list.RowCount()

FOR ll_index = 1 to ll_count
	If trim(ids_group_list.GetItemString(ll_index, "group_owner")) = trim(as_owner) Then
		ltvi_child.Label =   trim(ids_group_list.GetItemString(ll_index,"group_desc"))  + ' (' + string(long(ids_group_list.GetItemString(ll_index,"group_id"))) + ')'

		ltvi_child.Data = Long(ids_group_list.GetItemString(ll_index,"group_id"))
		ll_handle = itv_tree.InsertItemLast(al_handle,ltvi_child)
		If ltvi_child.label = is_currentlabel Then
			il_currenthandle = ll_handle
		End if
	End If
NEXT

end subroutine

public subroutine uf_tl_selectionchange (long al_handle);treeviewitem	ltvi_item, ltvi_parent
String			ls_owner, ls_grouptype
Long				ll_parent_handle


SetPointer(HourGlass!)

il_treehandle = al_handle

ilv_list.EVENT DYNAMIC ue_clear()

ls_grouptype = uf_getgrouptype()

itv_tree.GetItem(al_handle,ltvi_item)

CHOOSE CASE ltvi_item.Level		
	CASE 1			
		uf_list_groups(ltvi_item.Data)
	CASE 2		
		ll_parent_handle = itv_tree.FindItem (ParentTreeItem!, al_handle)
		itv_tree.GetItem(ll_parent_handle,ltvi_parent)
		//	ls_owner = righttrim(ltvi_parent.Data)
		//need to filter by parent.data

		If ls_grouptype = 'P' Then
			uf_list_products(ltvi_item.Data)
		Else
			If ls_grouptype = 'C' Then
				uf_list_customers(ltvi_item.Data)
			Else
				uf_list_locations(ltvi_item.Data)
			End If
		End If
	Case 3
		//ilv_list.EVENT DYNAMIC ue_product_descr(ltvi_item.Data)
END CHOOSE

SetPointer(Arrow!)
end subroutine

public subroutine uf_list_groups (string as_owner);String			ls_label, &
				ls_filterstring
				
Long				ll_pic_idx, &
					ll_count, &
					ll_index, &
					ll_handle, &
					ll_listindex
listviewitem 	llvi_groupitem


// Add column headers for the Report view
ilv_list.AddColumn ("Group Description", Left!, 1150)
ilv_list.AddColumn ("Id", Right!, 250)
ilv_list.AddColumn ("User Id", Left!, 325)
ilv_list.AddColumn ("Date Updated", Left!, 375)

as_owner = righttrim(as_owner)
Choose Case String(as_owner)
	Case "SALES"
		ll_pic_idx = 5
	Case "SCHED"
		ll_pic_idx = 6
	Case "SMACC"
		ll_pic_idx = 7
	Case Else
		ll_pic_idx = 4
End Choose

ll_count = ids_group_list.RowCount()

FOR ll_index = 1 TO ll_count
	If trim(ids_group_list.GetItemString(ll_index, "group_owner")) = trim(as_owner) Then
	
		ls_label =	ids_group_list.GetItemString(ll_index, "group_desc") + "~t" + &
						string(long(ids_group_list.GetItemString(ll_index, "group_id")))	 + "~t" + &
						ids_group_list.GetItemString(ll_index, "group_last_update_userid")	 + "~t" + &
						String(ids_group_list.GetItemDate(ll_index, "group_last_update_date"),"mm/dd/yyyy")		
		
		llvi_groupitem.Label = ls_label
		llvi_groupitem.Data = long(ids_group_list.GetItemString(ll_index, "group_id"))	
		llvi_groupitem.PictureIndex = ll_pic_idx
		
		ll_listindex = ilv_list.AddItem(llvi_groupitem)
	//	If Trim(is_currentlabel) = Trim(llvi_groupitem.label) Then
	////		il_currentidex = ll_listindex
	//	End If
	End If
NEXT

ilv_list.uf_setdata(1,0)
end subroutine

public function string uf_getproddescr (string as_product);String		ls_descr


Select long_description
Into :ls_descr
From sku_products
Where sku_product_code = :as_product
Using SQLCA;

If SQLCA.SQLCode <> 0 THEN ls_descr = ""

Return ls_descr
end function

public function string uf_getstring (string as_type);String		ls_string, ls_find_string, ls_temp

Long			ll_index, ll_count, ll_found

Choose Case as_type
	Case "DESCRIPTION"
		ls_temp = ids_group_list.describe("DataWindow.Data")
		ls_find_string = "group_id = " + "'" + string(il_groupind, "000000000") + "'" + " and group_owner = " + "'" + is_updateowner + "'"
		ll_found = ids_group_list.Find(ls_find_string, 1, ids_group_list.RowCount())
		If ll_found > 0 Then
			Return ids_group_list.GetItemString(ll_found, "group_desc")
		Else
			Return ""
		End If

	Case "GROUP_TYPE"
		ls_temp = ids_group_list.describe("DataWindow.Data")
		ls_find_string = "group_id = " + "'" + string(il_groupind, "000000000") + "'" + " and group_owner = " + "'" + is_updateowner + "'"
		ll_found = ids_group_list.Find(ls_find_string, 1, ids_group_list.RowCount())
		If ll_found > 0 Then
			Return ids_group_list.GetItemString(ll_found, "group_type_code") 
		Else
			Return ""
		End If
		
	Case "GROUP_COMMENT"
		ls_temp = ids_group_list.describe("DataWindow.Data")
		ls_find_string = "group_id = " + "'" + string(il_groupind, "000000000") + "'" + " and group_owner = " + "'" + is_updateowner + "'"
		ll_found = ids_group_list.Find(ls_find_string, 1, ids_group_list.RowCount())
		If ll_found > 0 Then
			Return ids_group_list.GetItemString(ll_found, "group_comment") 
		Else
			Return ""
		End If
		
	Case "PRODUCTS"
		ls_string = ""
		If il_groupind > 0 Then
			ll_count = uf_retrievegroupitems(il_groupind)
			For ll_index = 1 To ll_count
				ls_string += ids_group_item_list.GetItemString(ll_index, "component_code") + "~t"
				ls_string += string(ids_group_item_list.GetItemDate(ll_index, "component_last_update_date"), "yyyy/mm/dd") + "~t"
				ls_string += ids_group_item_list.GetItemString(ll_index, "component_last_update_userid") + "~r~n"
			Next
		End If

		Return ls_string
		
	Case "CUSTOMERS"
		ls_string = ""
		If il_groupind > 0 Then
			ll_count = uf_retrievegroupitems(il_groupind)
			For ll_index = 1 To ll_count
				ls_string += ids_group_item_list.GetItemString(ll_index, "component_code") + "~t"
				ls_string += ids_group_item_list.GetItemString(ll_index, "component_type") +  "~t"
				ls_string += string(ids_group_item_list.GetItemDate(ll_index, "component_last_update_date"), "yyyy/mm/dd") + "~t"
				ls_string += ids_group_item_list.GetItemString(ll_index, "component_last_update_userid") + "~r~n"				
			Next
		End If
		
		Return ls_string

	Case "LOCATIONS"
		ls_string = ""
		If il_groupind > 0 Then
			ll_count = uf_retrievegroupitems(il_groupind)
			For ll_index = 1 To ll_count
				ls_string += mid(ids_group_item_list.GetItemString(ll_index, "component_code"),1,3) + + "~t"
				ls_string += string(ids_group_item_list.GetItemDate(ll_index, "component_last_update_date"), "yyyy/mm/dd") + "~t"
				ls_string += ids_group_item_list.GetItemString(ll_index, "component_last_update_userid") + "~r~n"					
			Next
		End If

		Return ls_string
				
	Case "OWNER"
		Return is_updateowner
		
	Case "ADDMOD"
		If ib_newgroup Then
			Return "NEW"
		Else
			Return "MOD"
		End If
		
	Case "COPYTEXT"
		Return is_copytext
		
	Case Else
		Return ""
		
End Choose
end function

public subroutine uf_pointerxy (ref long pointerx, ref long pointery);iw_grpmaint.wf_pointerposition(pointerx, pointery)
end subroutine

public subroutine uf_setmicrohelp (string as_input);iw_grpmaint.wf_setmicrohelp(as_input)
end subroutine

public subroutine uf_expandupdateowner (long al_handle);// set focus on for is_updateowner


itv_tree.SelectItem(al_handle)

itv_tree.ExpandItem(al_handle)

uf_tl_selectionchange(al_handle)
end subroutine

public function long uf_getownerhandle ();Return il_ownerhandle
end function

public function boolean uf_checkfordupdescr (string as_descr);u_TreeViewFunctions		lu_TreeFunctions

String						ls_return


If Not lu_TreeFunctions.uf_checkfordupdescriptions(as_descr, il_ownerhandle, itv_tree) Then 
	Return False
End If

Return True
end function

public subroutine uf_load_root ();Long					ll_rowcount, ll_index, ll_handle
TreeViewItem		ltvi_ownergrp
DataStore			lds_root_items
String				ls_data


// deleting tree
ll_handle = itv_tree.FindItem(RootTreeItem!, 0)
Do While ll_handle <> -1
	itv_tree.DeleteItem(ll_handle)
	ll_handle = itv_tree.FindItem(RootTreeItem!, 0)
Loop

lds_root_items =  Create DataStore
lds_root_items.DataObject = "d_ownergroup"
lds_root_items.Reset()
lds_root_items.SetTransObject(SQLCA)
ll_rowcount = lds_root_items.Retrieve()

ltvi_ownergrp.PictureIndex = 1
ltvi_ownergrp.SelectedPictureIndex = 2

//	Cycle through to populate owners
FOR ll_index = 1 TO ll_rowcount
	
	ltvi_ownergrp.Label = String(lds_root_items.object.type_short_desc[ll_index])
	ls_data = Trim(String(lds_root_items.object.type_code[ll_index]))
	
	if ls_data = 'CASE' then
		ls_data = 'CASE '
	end if

	
	ltvi_ownergrp.Data = ls_data
	ltvi_ownergrp.Children = TRUE
	
	ll_handle = itv_tree.InsertItemSort(0,ltvi_ownergrp)
	
	If righttrim(ls_data) = righttrim(is_updateowner) Then
		il_ownerhandle = ll_handle
	End If
	
NEXT

end subroutine

public subroutine uf_retrieve_productgroups ();
end subroutine

public subroutine uf_retrieveproductgroups ();
end subroutine

public subroutine uf_retrievegroups ();u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_inquirestring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_inquirestring = ls_grouptype

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_inquirestring)
iu_GroupDataAccess.uf_list_groups(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_inquireString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_inquireString)
	return
End If

ids_group_list = Create datastore
ids_group_list.DataObject = "d_group_list"
ids_group_list.Reset()
ll_ret = ids_group_list.ImportString(ls_inquirestring)

end subroutine

public function long uf_retrievegroupitems (long al_group_id);u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_inquirestring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

ls_grouptype = uf_getgrouptype()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_inquirestring = ls_grouptype + ' ' + string(al_group_id, "00000")

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_inquirestring)
iu_GroupDataAccess.uf_list_group_items(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_inquireString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_inquireString)
	return 0
End If

ids_group_item_list = Create datastore
ids_group_item_list.DataObject = "d_group_item_list"
ids_group_item_list.Reset()
ll_ret = ids_group_item_list.ImportString(ls_inquirestring)

if ll_ret > 0 Then
	Return ll_ret
Else
	Return 0
End If
	

end function

public subroutine uf_list_products (long al_groupid);Long 				ll_count, ll_index
String 			ls_label, ls_id, ls_descr, ls_product, ls_update_date, ls_update_user
listviewitem	 	llvi_item

ll_count = uf_retrievegroupitems(al_groupid)

// Add column headers for the Report view
ilv_list.AddColumn ("SKU Product", Left!, 500)
ilv_list.AddColumn ("SKU Description", Left!, 1000)
ilv_list.AddColumn ("Update Date", Left!, 500)
ilv_list.AddColumn ("Update User", Left!, 500)

FOR ll_index = 1 TO ll_count
	ls_product = ids_group_item_list.GetItemString(ll_index, "component_code")
	ls_descr = uf_getproddescr(ls_product)
	ls_update_date = String(ids_group_item_list.GetItemDate(ll_index, "component_last_update_date"), "mm/dd/yyyy")
	ls_update_user = ids_group_item_list.GetItemString(ll_index, "component_last_update_userid")		
	ls_label = ls_product + "~t" + ls_descr + "~t" + ls_update_date + "~t" + ls_update_user
	
	llvi_item.Label = ls_label
	llvi_item.Data = al_groupid
	llvi_item.PictureIndex = 2
	
	ilv_list.AddItem (llvi_item)
	
NEXT

ilv_list.uf_setdata(2,al_groupid)

end subroutine

public function boolean uf_initializesma (string as_grouptype, ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);String		ls_app_name

iu_ClassFactory = au_ClassFactory
iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "u_GroupDataAccess", iu_GroupDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_app_name = GetApplication().Appname

Choose Case ls_app_name
	Case "pas" 
		is_updateowner = "SCHED"
	Case "orp" 
		is_updateowner = "SALES"
	Case "sma"
		is_updateowner = "SMACC"
	Case Else
		is_updateowner = "APPER"
End Choose

Return True
end function

public function boolean uf_add_group_items (long al_groupid, string as_itemstring);
u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_detailstring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_detailstring = ls_grouptype + ' ' + string(al_groupid, "00000")  + 'I' + as_itemstring 

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_detailstring)
iu_GroupDataAccess.uf_update_group_items(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_detailstring)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_detailstring)
	Return False
End If

Return True
//uf_inquire()

end function

public function long uf_update_group (long al_group_id, string as_group_name);
u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_detailstring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_detailstring = ls_grouptype + ' ' + string(al_group_id, '00000') + 'U' + '~t' + as_group_name + '~t' + &
						is_updateowner + '~t' + space(8) + '~t' + space(3) + '~t'

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_detailstring)
iu_GroupDataAccess.uf_update_group(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_detailstring)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_detailstring)
	return 0
Else
	return Long(ls_detailstring)
End If

//uf_inquire()


	

end function

public function boolean uf_delete_group_items (long al_groupid, string as_itemstring);
u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_detailstring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_detailstring = ls_grouptype + ' ' + string(al_groupid, "00000")  + 'D' + as_itemstring + '~t'

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_detailstring)
iu_GroupDataAccess.uf_update_group_items(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_detailstring)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_detailstring)
	Return False
End If

Return True
//uf_inquire()

end function

public function string uf_getgrouptype ();String		ls_grouptype

ls_grouptype =  iw_grpmaint.is_grouptype

If Isnull(ls_grouptype) or (ls_grouptype < ' ') Then
	MessageBox ("Group Type Missing", ls_grouptype)
End If

Return ls_grouptype



end function

public subroutine uf_list_customers (long al_groupid);Long 				ll_count, ll_index
String 			ls_label, ls_id, ls_descr, ls_customer_code, ls_customer_type, ls_update_date, ls_update_user
listviewitem	 	llvi_item

ll_count = uf_retrievegroupitems(al_groupid)

// Add column headers for the Report view
ilv_list.AddColumn ("CustomerID", Left!, 500)
ilv_list.AddColumn ("Customer Type", Left!, 200)
ilv_list.AddColumn ("Customer Name", Left!, 1000)
ilv_list.AddColumn ("Update Date", Left!, 500)
ilv_list.AddColumn ("Update User", Left!, 500)


FOR ll_index = 1 TO ll_count
	ls_customer_code = ids_group_item_list.GetItemString(ll_index, "component_code")
	ls_customer_type = ids_group_item_list.GetItemString(ll_index, "component_type")
	ls_descr = uf_getcustdescr(ls_customer_code, ls_customer_type)
	ls_update_date = String(ids_group_item_list.GetItemDate(ll_index, "component_last_update_date"), "mm/dd/yyyy")
	ls_update_user = ids_group_item_list.GetItemString(ll_index, "component_last_update_userid")		
	ls_label = ls_customer_code + "~t" + ls_customer_type  + "~t" + ls_descr + "~t" + ls_update_date + "~t" + ls_update_user
	
	llvi_item.Label = ls_label
	llvi_item.Data = al_groupid
	llvi_item.PictureIndex = 2
	
	ilv_list.AddItem (llvi_item)
	
NEXT

ilv_list.uf_setdata(2,al_groupid)

end subroutine

public function string uf_getcustdescr (string as_customer_code, string as_customer_type);String		ls_descr

If as_customer_type = 'S' Then
	Select short_name
	Into :ls_descr
	From customers
	Where customer_id = :as_customer_code
	Using SQLCA;
	
	If SQLCA.SQLCode <> 0 THEN ls_descr = ""

	Return ls_descr
	
Else
	If as_customer_type = 'B' Then
		Select name
		Into :ls_descr
		From billtocust
		Where bill_to_id = :as_customer_code
		Using SQLCA;
		
		If SQLCA.SQLCode <> 0 THEN ls_descr = ""
	
		Return ls_descr
		
	Else
		If as_customer_type = 'C' Then
			Select name
			Into :ls_descr
			From corpcust
			Where corp_id = :as_customer_code
			Using SQLCA;
			
			If SQLCA.SQLCode <> 0 THEN ls_descr = ""
		
			Return ls_descr
		
		Else
			MessageBox("Invalid customer type", "Invalid customer type passed = " + as_customer_type)
			Return ""
		End If
	End If
End If

		
		
end function

public subroutine uf_setgroupind (long al_groupind);il_groupind = al_groupind
end subroutine

public subroutine uf_setnewgroupind (boolean ab_newgroupind);ib_newgroup = ab_newgroupind
end subroutine

public subroutine uf_list_locations (long al_groupid);Long 				ll_count, ll_index
String 			ls_label, ls_id, ls_descr, ls_location, ls_update_date, ls_update_user
listviewitem	 	llvi_item

ll_count = uf_retrievegroupitems(al_groupid)

// Add column headers for the Report view
ilv_list.AddColumn ("Location Code", Left!, 500)
ilv_list.AddColumn ("Name", Left!, 1000)
ilv_list.AddColumn ("Update Date", Left!, 500)
ilv_list.AddColumn ("Update User", Left!, 500)

FOR ll_index = 1 TO ll_count
	ls_location = ids_group_item_list.GetItemString(ll_index, "component_code")
	ls_descr = uf_getlocdescr(ls_location)
	ls_label = ls_location + "~t" + ls_descr + "~t" + ls_update_date + "~t" + ls_update_user
	
	llvi_item.Label = ls_label
	llvi_item.Data = al_groupid
	llvi_item.PictureIndex = 2
	
	ilv_list.AddItem (llvi_item)
	
NEXT

ilv_list.uf_setdata(2,al_groupid)

end subroutine

public function string uf_getlocdescr (string as_location);String		ls_descr


Select location_name
Into :ls_descr
From locations
Where location_code = :as_location
Using SQLCA;

If SQLCA.SQLCode <> 0 THEN ls_descr = ""

Return ls_descr
end function

public subroutine uf_tl_newgrp ();String			ls_rtn, ls_grouptype


ib_newgroup = True
il_groupind = 0

ls_grouptype = uf_getgrouptype()

If ls_grouptype = 'P' Then
	OpenWithParm(w_groupmaintproduct, This)
Else
	If ls_grouptype = 'C' Then
		OpenWithParm(w_groupmaintcustomer, This)
	Else
		OpenWithParm(w_groupmaintlocation, This)
	End If
End If
	
ls_rtn = Message.StringParm

itv_tree.FindItem(CurrentTreeItem!, il_treehandle)
If ls_rtn = "OK" Then 
	uf_inquire()
End If

end subroutine

public function boolean uf_deletegroup (long al_grpid);
u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_detailstring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_detailstring = ls_grouptype + ' ' + string(al_grpid, "00000") + 'D' + '~t' + space(40) + '~t' + &
						is_updateowner + '~t' + space(8) + '~t' + space(3) + '~t'

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_detailstring)
iu_GroupDataAccess.uf_update_group(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_detailstring)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_detailstring)
	return False
End If

uf_inquire()

Return True
	

end function

public function string uf_getuserid ();String		ls_userid

ls_userid =  iw_grpmaint.is_userid

If Isnull(ls_userid) or (ls_userid < ' ') Then
	MessageBox ("UserID Missing", ls_userid)
End If

Return ls_userid



end function

public function long uf_add_group (string as_group_desc, string as_group_type_desc, string as_group_comment);
u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_detailstring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_detailstring = ls_grouptype + ' ' + '00000' + 'I' + '~t' + as_group_desc + '~t' + &
						is_updateowner + '~t' + space(8) + '~t' + as_group_type_desc + '~t' + &
						as_group_comment + '~t'

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_detailstring)
iu_GroupDataAccess.uf_update_group(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_detailstring)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_detailstring)
	return 0
Else
	lu_ParameterStack.uf_Push('u_AbstractErrorContext',iu_ErrorContext)
	return Long(ls_detailstring)
End If

//uf_inquire()


	

end function

public function boolean uf_update_db (string as_option, string as_type, string as_description, string as_grouptypecode, string as_detail, readonly string as_comment);
Boolean		lb_rtn

Long	ll_new_groupid, ll_rtn

lb_rtn = True

Choose Case as_option
	Case "MOD"
		Choose Case as_type
			Case "H"
				//uf_update_group(il_groupind, as_description, as_grouptypecode, as_comment)
				ll_rtn = uf_update_group(il_groupind, as_description, as_grouptypecode, as_comment)
				if ll_rtn <> 0 then
					lb_rtn = false
				end if
			Case "P"
				//uf_add_group_items(il_groupind, as_detail)
				lb_rtn = uf_add_group_items(il_groupind, as_detail) 
			Case "D"
				uf_delete_group_items(il_groupind, as_detail)
		End Choose
		
	CASE "NEW"
		Choose Case as_type
			Case "H"
				ll_new_groupid = uf_add_group(as_description, as_grouptypecode, as_comment)
				if ll_new_groupid <> 0 then 
					lb_rtn = uf_add_group_items(ll_new_groupid, as_detail)
					If lb_rtn = False then
						uf_deletegroup(ll_new_groupid)
					end if
				end if
			Case "P"
				//uf_add_group_items(il_groupind, as_detail)
				lb_rtn = uf_add_group_items(il_groupind, as_detail) 
			Case "D"
				// No Need To Do Anything
		End Choose
		
End Choose

Return lb_rtn
end function

public subroutine uf_copygroup (string as_description, long al_groupind, string as_comment);Long	ll_count, ll_new_groupid, ll_index

String	ls_itemstring, ls_grouptypecode

SetPointer(hourglass!)

ls_grouptypecode = uf_getstring("GROUP_TYPE")

ll_count = uf_retrievegroupitems(al_groupind)

ls_itemstring = ''
For ll_index = 1 To ll_count
	ls_itemstring += ids_group_item_list.GetItemString(ll_index, "component_code") + "~t"
	ls_itemstring += ids_group_item_list.GetItemString(ll_index, "component_type") + "~r~n"
Next

ll_new_groupid = uf_add_group(as_description, ls_grouptypecode, as_comment)
uf_add_group_items(ll_new_groupid, ls_itemstring)


uf_inquire()

end subroutine

public function string uf_getgroupcomment (long al_group_id);String		ls_groupcomment, ls_find_string

Long		ll_found


ls_find_string = "long(group_id) = " + string(al_group_id) 
ll_found = ids_group_list.Find(ls_find_string, 1, ids_group_list.RowCount())
if ll_found > 0 Then
	ls_groupcomment = RightTrim(ids_group_list.GetItemString(ll_found, "group_comment"))
else
	ls_groupcomment = ''
end if

Return ls_groupcomment



end function

public function boolean uf_initialize (listview alv, treeview atv, window aw, statictext ast);
ilv_list = alv
itv_tree = atv
ist_scroll = ast
iw_grpmaint = aw

IF NOT ilv_list.uf_initialize(atv,This) Then Return False
IF NOT itv_tree.uf_initialize(alv,This) Then Return False

Return True

end function

public function long uf_update_group (long al_group_id, string as_group_desc, string as_group_type_code, string as_comment);
u_ParameterStack	lu_ParameterStack

String	ls_appname, ls_windowname, ls_detailstring, ls_grouptype

Long	ll_ret

iu_ErrorContext.uf_Initialize()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_grouptype = uf_getgrouptype()

ls_appname = GetApplication().AppName
ls_windowname = "w_group_maintenance"
ls_detailstring = ls_grouptype + ' ' + string(al_group_id, '00000') + 'U' + '~t' + as_group_desc + '~t' + &
						is_updateowner + '~t' + space(8) + '~t' + as_group_type_code + '~t' + as_comment + '~t'

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_detailstring)
iu_GroupDataAccess.uf_update_group(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_detailstring)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

If Not iu_ErrorContext.uf_IsSuccessful() Then 
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', ls_detailstring)
	//return 0
	return 1

Else
	return Long(ls_detailstring)
End If

//uf_inquire()


	

end function

public function boolean uf_initializecas (string as_grouptype, ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);String		ls_app_name

iu_ClassFactory = au_ClassFactory
iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "u_GroupDataAccess", iu_GroupDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_app_name = GetApplication().Appname

Choose Case ls_app_name
	Case "pas" 
		is_updateowner = "SCHED"
	Case "orp" 
		is_updateowner = "SALES"
	Case "sma"
		is_updateowner = "SMACC"
	Case "cas"
		is_updateowner = "CASE "
	Case Else
		is_updateowner = "APPER"
End Choose

Return True
end function

on u_grpmaintenance.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_grpmaintenance.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

