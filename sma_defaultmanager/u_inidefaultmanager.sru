HA$PBExportHeader$u_inidefaultmanager.sru
forward
global type u_inidefaultmanager from u_abstractdefaultmanager
end type
end forward

global type u_inidefaultmanager from u_abstractdefaultmanager
end type
global u_inidefaultmanager u_inidefaultmanager

type variables
Boolean	ib_FileExists
String	is_FileName,&
	is_SectionName

u_AbstractClassFactory	iu_ClassFactory

end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_filename, string as_section, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, integer ai_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref long al_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, long al_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, real ar_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, date ad_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, datetime adt_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, time at_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref real ar_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref decimal ade_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref integer ai_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref time at_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, boolean ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, decimal adc_value, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_createfile (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref date ad_value, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_filename, string as_section, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the Default Manager. Set the file to read/write from and 
			sets the section </DESC>

<ARGS>	au_classfactory: Class Factory in case it needs other objects
			as_filename: File to access
			as_section: Section where values will be stored
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to initialize the INI
			defaults manager.  Set the INI file to be used
			as well as the section to read from.</USAGE>

-------------------------------------------------------- */
//is_filename = as_FileName
//is_sectionname = as_Section
//iu_classfactory = au_ClassFactory
//ib_FileExists = FileExists(as_fileName)
//Return True

// Code to support terminal server - ibdkdld
String 	ls_filepath,ls_filename			

u_win32api	lu_sdkCalls

is_filename = as_FileName
If upper(as_filename) = "IBPUSER.INI" Then
	lu_sdkcalls = Create u_win32api
	ls_FilePath = lu_sdkcalls.uf_getwindowsdirectory()
	is_filename = ls_FilePath + "\" + as_filename
End If
is_sectionname = as_Section
iu_classfactory = au_ClassFactory
ib_FileExists = FileExists(is_fileName)
Return True


end function

public function boolean uf_getdata (string as_key, ref string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a string value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")

as_Value = ls_Temp

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = "Not Found" Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	as_value = ""
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_setdata (string as_key, string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: The string value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a string value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(as_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_Set("INI File Access", True)
	Return False
END IF

Return True

end function

public function boolean uf_setdata (string as_key, integer ai_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ai_value: The integer value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set an integer value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(ai_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True

end function

public function boolean uf_getdata (string as_key, ref long al_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: Where the value will be placed Passed by ref
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Long value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")

al_Value = Long(ls_Temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = 'Not Found' Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_setdata (string as_key, long al_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			al_value: The long value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a long value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(al_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True

end function

public function boolean uf_setdata (string as_key, real ar_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ar_value: The real value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a real value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(ar_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True

end function

public function boolean uf_setdata (string as_key, date ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: The date value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a date value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_temp = String(ad_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True



end function

public function boolean uf_setdata (string as_key, datetime adt_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			adt_value: The datetime value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a datetime value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(adt_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True

end function

public function boolean uf_setdata (string as_key, time at_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			at_value: The time value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a time value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(at_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True

end function

public function boolean uf_getdata (string as_key, ref real ar_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ar_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a real value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")

ar_Value = Real(ls_Temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = "Not Found" Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_getdata (string as_key, ref decimal ade_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ade_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a decimal value stored in an INI file.
			If this function returns false, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")

ade_Value = Dec(ls_Temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = "Not Found" Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_getdata (string as_key, ref integer ai_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ai_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a integer value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")

ai_Value = Integer(ls_Temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = 'Not Found' Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_getdata (string as_key, ref time at_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			at_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a time value stored in an INI file
			If this function returns false, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")
at_Value = Time(ls_Temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = 'Not Found' Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			adt_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a DateTime value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")
adt_value = DateTime(Date(Left(ls_temp,Pos(ls_temp, " ") - 1)),Time(Mid(ls_temp, Pos(ls_temp, " ") + 1)))
If Pos(ls_temp, " ") <= 0 Then
	SetNull(adt_value)
End if

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = 'Not Found' Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: Variable to store the value by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a boolean value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")
IF Upper(ls_Temp)  = 'TRUE' OR Upper(ls_Temp)  = 'T' Then 
	ab_value = True
ELSE
	ab_Value = False
END IF

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = 'Not Found' Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	
au_ErrorContext.uf_SetReturnCode(0)
Return True




end function

public function boolean uf_setdata (string as_key, boolean ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: The boolean value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Boolean value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

IF ab_value Then
	ls_Temp = 'True'
Else
	ls_Temp = 'False'
End if

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True




end function

public function boolean uf_setdata (string as_key, decimal adc_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			adc_value: The decimal value to set
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a decimal value in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

IF Not uf_CreateFile(au_errorcontext) Then Return False

ls_Temp = String(adc_value)

If SetProfileString(is_FileName, is_SectionName, as_Key, ls_Temp) = -1 Then
	// An error occurred
	au_errorcontext.uf_AppendText ("Unable to write key " + as_Key + " of section " + &
		is_SectionName + " to file " + is_filename )
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF

Return True

end function

private function boolean uf_createfile (ref u_abstracterrorcontext au_errorcontext);Integer	li_FileHandle,&
			li_BytesWritten

IF ib_fileexists Then Return TRUE

li_FileHandle = FileOpen(is_filename,LineMode!,Write!,LockReadWrite!,Append!)
IF li_FileHandle = -1 OR IsNull(li_FileHandle) Then 
	au_ErrorContext.uf_AppendText("Unable to Create file " + is_filename)
	Return FALSE
END IF
li_BytesWritten = FileWrite(li_FileHandle,"~r~n")

IF li_BytesWritten < 0 Then 
	au_ErrorContext.uf_AppendText("Unable to Write to file "+ is_filename)
	Return False
End if
fileclose(li_FileHandle)
Return TRUE



end function

public function boolean uf_getdata (string as_key, ref date ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in an INI file
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a date value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
String	ls_Temp

ls_Temp = ProfileString(is_FileName, is_SectionName, as_Key, "Not Found")

ad_value = Date(ls_temp)

IF ls_Temp = "" Then
	// An error occurred 
	au_errorcontext.uf_AppendText ("There was an error attempting to read the file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End IF
IF ls_Temp = 'Not Found' Then
	au_errorcontext.uf_AppendText ("INI: Unable to read key " + as_Key + " from section " + is_SectionName + &
		" from file " + is_FileName)
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
END IF	

au_ErrorContext.uf_SetReturnCode(0)
Return True



end function

on u_inidefaultmanager.create
TriggerEvent( this, "constructor" )
end on

on u_inidefaultmanager.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_IniDefaultManager

<OBJECT>	Store and retrieve data in an INI file.</OBJECT>
			
<USAGE>	Initialize this object with an INI file name
			and a section name.  Call uf_Get() to retrieve
			and uf_Set() to write data in the INI file.
			If an error occurs, the error context contains
			information about the error: The ReturnCode in the
			error context can be any of the following
			values:
			<LI> 0 - no error
			<LI> -1 - section, key, or file not found reading or writing a value
			<LI> -2 - a file system error trying to read a value
			</USAGE>
			
<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
Return 0

end event

