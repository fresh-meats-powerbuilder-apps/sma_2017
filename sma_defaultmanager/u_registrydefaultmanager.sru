HA$PBExportHeader$u_registrydefaultmanager.sru
forward
global type u_registrydefaultmanager from u_abstractdefaultmanager
end type
end forward

global type u_registrydefaultmanager from u_abstractdefaultmanager
end type
global u_registrydefaultmanager u_registrydefaultmanager

type variables
String	is_Key

u_AbstractClassFactory	iu_ClassFactory
u_RegistryService		iu_RegistryService

end variables

forward prototypes
public function boolean uf_getdata (string as_key, ref string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, string as_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, integer ai_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref long al_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, long al_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, real ar_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, datetime adt_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, time at_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref real ar_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref decimal ade_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref integer ai_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref time at_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, boolean ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, decimal adc_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref date ad_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref double adb_doublevalue, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, blob ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_Key, string as_Section, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (string as_key, ref blob ab_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, date ad_value, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_key, double ad_value, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_getdata (string as_key, ref string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			as_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a string value stored in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_getData(as_key,as_Value,au_errorcontext)


end function

public function boolean uf_setdata (string as_key, string as_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set String value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: String value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a String value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,as_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, integer ai_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set an integer value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Integer value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a integer value in the registry
			Only Positive integers can be stored in the registry. If you need to 
			store a negative number, you must convert them to a a Real, Double,String,or Decimal.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */

Return iu_registryservice.uf_SetData(as_key,ai_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref long al_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in the regaistr as long
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: Where the value will be placed Passed by ref
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Long value stored in an INI file
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_getData(as_key,al_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, long al_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set a Long value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Long value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Long value in the registry
			Only Positive longs can be stored in the registry. If you need to 
			store a negative number, you must convert them to a a Real, Double,String,or Decimal.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,al_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, real ar_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Real value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Real value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Real value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,ar_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, datetime adt_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set DateTime value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: DateTime value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a DateTime value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,adt_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, time at_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Time value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: time value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Real value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,at_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref real ar_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get a Real value stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ar_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a real value stored in the registry.
			Real Values will be converted to stirngs and then stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_getData(as_key,ar_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref decimal ade_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get decimal stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ade_value: Where the Decimal value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a decimal value stored in the registry file.
			If this function returns false, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_GetData(as_key,ade_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref integer ai_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get integer data stored the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ai_value: Where the Integer value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a integer value stored the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_getData(as_key,ai_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref time at_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get data stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			at_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a time value stored in an INI file
			If this function returns false, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_getData(as_key,at_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get datetime value stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			adt_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a DateTime value stored in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_GetData(as_key,adt_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, boolean ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Boolean value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Boolean value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Boolean value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,ab_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, decimal adc_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Decimal value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Decimal value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Decimal value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,adc_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref date ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get date stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the date value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a date value stored in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_GetData(as_key,ad_value,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref double adb_doublevalue, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Double stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ab_value: Variable to store the Double value by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Double value stored in the registry.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_getData(as_key,adb_doublevalue,au_errorcontext)
end function

public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Boolean stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Boolean value stored in the registry
			Returns False if fails, Error Context will have the reason</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_GetData(as_key,ab_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, blob ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Blob value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			Each key may have several named values. 
			To specify the unnamed value (the only option for Windows 3.1), 
			specify an empty string.
			ab_value: Blob value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Blob value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,ab_value,au_errorcontext)
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, string as_Key, string as_Section, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the Default Manager. Set the key the value and 
			sets the section </DESC>

<ARGS>	au_classfactory: Class Factory in case it needs other objects
			as_filename: File to access
			as_section: Section where values will be stored
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to initialize the Registry
			defaults manager.  Set the key and sub key. If the key is blank it will be
			defaulted to "HKEY_LOCAL_MACHINE\SOFTWARE\IBP\".IF the subkey is blank it 
			will be defaulted to the application.appName</USAGE>

-------------------------------------------------------- */

IF Len(Trim(as_Key)) > 0 Then
	is_key = as_Key
ELSE
	is_Key = "HKEY_LOCAL_MACHINE\SOFTWARE\IBP\"
END IF

IF Len(Trim(as_Section)) > 0 Then
	is_Key += as_Section
ELSE
	is_Key += GetApplication().appName
END IF

iu_classfactory = au_ClassFactory

IF Not iu_ClassFactory.uf_GetObject('u_RegistryService',iu_registryservice,au_errorcontext) Then
	IF Not au_errorcontext.uf_ISSuccessful() Then
		au_errorcontext.uf_AppendText("unable to get registry service")
		Return False
	ELSE
		IF Not iu_RegistryService.uf_Initialize(au_classfactory,is_key,au_errorcontext) Then
			au_errorcontext.uf_AppendText("unable to Initialize registry service")
			Return False
		END IF
	END IF
END IF

Return True
end function

public function boolean uf_getdata (string as_key, ref blob ab_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Get Blob stored in the registry
			</DESC>

<ARGS>	as_key: Key you are looking for
			ad_value: Where the blob value will be placed by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to get a Boolean value stored in the registry.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_GetData(as_key,ab_value,au_errorcontext)



end function

public function boolean uf_setdata (string as_key, date ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Date value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Date value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Date value in the registry
			Date values are converted to strings in the format "mm/dd/yyyy" and stored.
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,ad_value,au_errorcontext)
end function

public function boolean uf_setdata (string as_key, double ad_value, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set Double value stored in the registry
			</DESC>

<ARGS>	as_key: A string containing the name of a value in the registry. 
			ab_value: Double value you want to save
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set a Decimal value in the registry
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Return iu_registryservice.uf_SetData(as_key,ad_value,au_errorcontext)
end function

on u_registrydefaultmanager.create
TriggerEvent( this, "constructor" )
end on

on u_registrydefaultmanager.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_RegistryDefaultManager

<OBJECT>	Store and retrieve data in The registry file.</OBJECT>
			
<USAGE>	Initialize this object with a section name.  Call uf_Get() to retrieve
			and uf_Set() to write data in the Registry file.
			If an error occurs, the error context contains
			information about the error: The ReturnCode in the
			error context can be any of the following
			values:
			<LI> 0 - no error
			</USAGE>
			
<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
Return 0

end event

