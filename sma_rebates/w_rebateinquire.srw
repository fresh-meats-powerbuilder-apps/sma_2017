HA$PBExportHeader$w_rebateinquire.srw
forward
global type w_rebateinquire from w_abstractsheetext
end type
type dw_rebate_list from datawindow within w_rebateinquire
end type
type st_2 from statictext within w_rebateinquire
end type
type st_3 from statictext within w_rebateinquire
end type
end forward

global type w_rebateinquire from w_abstractsheetext
integer x = 110
integer y = 128
integer width = 2592
integer height = 1156
string title = "Charge Inquires"
dw_rebate_list dw_rebate_list
st_2 st_2
st_3 st_3
end type
global w_rebateinquire w_rebateinquire

type variables
w_rebateinquire                             iw_parent

string                                              is_title, &
                                                      is_inq_ind, &
                                                      is_inq_string

boolean                                         ib_reinquire

u_ErrorContext		      iu_ErrorContext
u_AbstractClassFactory	      iu_ClassFactory
u_NotificationController	      iu_Notification	
u_RebateDataAccess	      iu_RebateDataAccess
u_AbstractTutlTypeDataAccess    iu_TutlTypeDataAccess
w_smaframe		      iw_frame	
end variables

forward prototypes
public function boolean wf_fillcredmeth ()
public function boolean wf_fillrebatetype ()
public function boolean wf_fillcountrycode ()
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_fillcredmeth ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBCDMET'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_list.GetChild('credit_method', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

public function boolean wf_fillrebatetype ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBTYPE '
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_list.GetChild('rebate_type', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillcountrycode ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBCTYCD'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_list.GetChild('country_code', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True
end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillcredmeth() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If
If Not wf_fillrebatetype() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If
//dmk
If Not wf_fillcountrycode() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not iu_ClassFactory.uf_GetObject( "u_RebateDataAccess", iu_RebateDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

This.Event Post ue_inquire()

Return True
end function

on w_rebateinquire.create
int iCurrent
call super::create
this.dw_rebate_list=create dw_rebate_list
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_rebate_list
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_3
end on

on w_rebateinquire.destroy
call super::destroy
destroy(this.dw_rebate_list)
destroy(this.st_2)
destroy(this.st_3)
end on

event open;call super::open;iw_parent = This
is_Title = This.Title
is_inq_ind = ""
end event

event ue_inquire;String 					ls_inq_ind, &
							ls_AppName, &
							ls_WindowName, &
							ls_inq_string, &
							ls_RebateinquireString
							
integer					li_return_code

long						ll_rec_count

u_string_functions	lu_string



u_ParameterStack		lu_ParameterStack

iu_ErrorContext.uf_Initialize()

dw_rebate_list.reset()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
	
IF Not ib_ReInquire Then
	This.Event Trigger CloseQuery()
	
	If Message.ReturnValue <> 0 Then Return False
	
	ls_inq_ind = is_inq_ind
	ls_inq_string = is_inq_string
	
	lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_inq_ind)
	lu_ParameterStack.uf_Push('string', ls_inq_string)
	lu_ParameterStack.uf_Push('string', is_Title)
	lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
	
	iu_ErrorContext.uf_Initialize()
	
	iu_ClassFactory.uf_GetResponseWindow( "w_rebateInquire_open_b", lu_ParameterStack )
	
	lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
	lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Pop('string', ls_inq_string)
	lu_ParameterStack.uf_Pop('string', ls_inq_ind)
	lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
	
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
	If ls_inq_string = 'Cancel' Then 
		li_return_code = messagebox("Caution","Do you want to close this window?",StopSign!,YesNo!,2)
		if li_return_code = 1 then
			this.triggerevent(Close!)
			return false
		else
			Return False
		end if
	End If
	
	ls_inq_ind = left(ls_inq_ind,1)
	is_inq_ind = ls_inq_ind
	st_3.text = ls_inq_string
	
	//dmk
	choose case is_inq_ind
		Case "U"
			st_2.text = "Customer :"
		Case "E"
			st_2.text = "End Date :"
		Case "S"
			st_2.text = "Start Date :"
		Case "R"
			st_2.text = "Rebate Type :"
			Choose Case left(ls_inq_string,1)
				case 'A'
					st_3.text = "Accrual"
				case 'V'
					st_3.text = "VMR"	
				case 'O'
					st_3.text = "Order Conf Accrual"	
			end choose
		Case "I"
			st_2.text = "Initiated By :"
		Case "Q"
			st_2.text = "Requested By :"
		Case "C"
			st_2.text = "Credit Method :"
			Choose Case left(ls_inq_string,1)
				case 'O'
					st_3.text = "Off Invoice"
				case 'C'
					st_3.text = "Check"		
			end choose
		Case "L"
			st_2.text = "Ship To Country :"
			Choose Case  left(ls_inq_string,1)
				case 'A'
					st_3.text = "USA and Canada"
				case 'C'
					st_3.text = "Canada"	
				case 'U' 
					st_3.text = "USA"
			end choose
		Case "D"
			st_2.text = "Division :"
		Case "G"
			st_2.text = "Product Group :"
		Case "P"
			st_2.text = "Product :"
	End Choose
	
End If

is_inq_string = ls_inq_string

ls_AppName = GetApplication().AppName
ls_WindowName = 'rbtinquire'
ls_RebateinquireString = ls_inq_ind + "~t" + ls_inq_string +'~r~n'

iu_ErrorContext.uf_Initialize()

lu_ParameterStack.uf_initialize()

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_RebateinquireString)
//dmk
choose case ls_inq_ind
	case "S"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)
	case "E"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)
	case "R"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)
	case "I"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)		
	case "Q"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)
	case "C"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)
	case "L"
		iu_RebateDataAccess.uf_rebate_inquire(lu_ParameterStack)
	case "U"
		iu_RebateDataAccess.uf_rebate_inquire( lu_ParameterStack )
//		messagebox("Under construction" ,"Not available at this time.")
	case "D"
		iu_RebateDataAccess.uf_rebate_inquire( lu_ParameterStack )
//		messagebox("Under construction" ,"Not available at this time.")
	case "G"
//		iu_RebateDataAccess.uf_customer_inquire( lu_ParameterStack )
		messagebox("Under construction" ,"Not available at this time.")		
	case "P"
//		iu_RebateDataAccess.uf_customer_inquire( lu_ParameterStack )
		messagebox("Under construction" ,"Not available at this time.")
end choose
lu_ParameterStack.uf_Pop('string', ls_RebateInquireString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

//ls_RebateInquireString = string("1~tthis a test and only a test~t04/22/1999~t09/22/2999~tA~tC~tno one~tno body~r~n" & 
//								      + "2~tthis a test and only a test~t05/22/1999~t09/22/2999~tA~tC~tno one~tno body~r~n")

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If

//dw_rebate_list.ImportString(ls_RebateInquireString)

If Not lu_string.nf_IsEmpty(ls_RebateInquireString) Then
	ll_rec_count = dw_rebate_list.ImportString(ls_RebateInquireString)
	If ll_rec_count > 0 Then iw_frame.SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")	
Else
	iw_frame.SetMicroHelp("0 Rows Retrieved")
End if 			 		

dw_rebate_list.setsort("Rebate_id")
dw_rebate_list.sort()

This.SetRedraw(True)

ib_ReInquire = False

Return True

end event

event closequery;Return 0
end event

event close;close(this)
end event

event resize;call super::resize;IF newheight > dw_rebate_list.Y THEN
	dw_rebate_list.Height = newheight - dw_rebate_list.y - 20
END IF

IF newwidth > (dw_rebate_list.x * 2) THEN
	dw_rebate_list.WIdth = newwidth - (dw_rebate_list.x * 2)
END IF
end event

type dw_rebate_list from datawindow within w_rebateinquire
integer x = 37
integer y = 132
integer width = 2455
integer height = 416
integer taborder = 10
string title = "none"
string dataobject = "d_rebate_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_rebateinquire
integer x = 402
integer y = 32
integer width = 635
integer height = 72
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "none"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_rebateinquire
integer x = 1093
integer y = 24
integer width = 974
integer height = 72
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "none"
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

