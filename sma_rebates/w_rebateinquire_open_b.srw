HA$PBExportHeader$w_rebateinquire_open_b.srw
forward
global type w_rebateinquire_open_b from w_abstractresponseext
end type
type st_1 from statictext within w_rebateinquire_open_b
end type
type dw_rebate_inquire_options from datawindow within w_rebateinquire_open_b
end type
type em_customer_id from editmask within w_rebateinquire_open_b
end type
type em_initiated from editmask within w_rebateinquire_open_b
end type
type em_date from editmask within w_rebateinquire_open_b
end type
type em_requested from editmask within w_rebateinquire_open_b
end type
type dw_rebate_credit_and_type from datawindow within w_rebateinquire_open_b
end type
type uo_division from u_division within w_rebateinquire_open_b
end type
end forward

global type w_rebateinquire_open_b from w_abstractresponseext
string tag = "Inquire on Chargess by Customer or End Dates"
integer x = 46
integer y = 304
integer width = 2551
integer height = 532
st_1 st_1
dw_rebate_inquire_options dw_rebate_inquire_options
em_customer_id em_customer_id
em_initiated em_initiated
em_date em_date
em_requested em_requested
dw_rebate_credit_and_type dw_rebate_credit_and_type
uo_division uo_division
end type
global w_rebateinquire_open_b w_rebateinquire_open_b

type variables
String				is_inq_ind, &
                                                	is_inq_string, &
				is_title

u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractTutlTypeDataAccess	iu_TutlTypeDataAccess
u_AbstractClassFactory		iu_ClassFactory

u_NotificationController		iu_Notification	

w_smaframe			iw_frame	
end variables

forward prototypes
public function boolean wf_getshiptos (ref string as_customer_id)
public function boolean wf_getcorps (ref string as_customer_id)
public function boolean wf_getbilltos (ref string as_customer_id)
public function boolean wf_fillcredmeth ()
public function boolean wf_fillrebatetype ()
public function boolean wf_fillinqtype ()
public function boolean wf_checkcustid ()
public function boolean wf_fillcountrycode ()
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_getshiptos (ref string as_customer_id);u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_customer_id)

iu_CustomerDataAccess.uf_RetrieveShiptos(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_customer_id)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_getcorps (ref string as_customer_id);u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_customer_id)

iu_CustomerDataAccess.uf_RetrieveCorps(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_customer_id)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

return true
end function

public function boolean wf_getbilltos (ref string as_customer_id);u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_customer_id)

iu_CustomerDataAccess.uf_Retrievebilltos(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_customer_id)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

Return True

end function

public function boolean wf_fillcredmeth ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBCDMET'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_credit_and_type.GetChild('rebate_type', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillrebatetype ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBTYPE'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_credit_and_type.GetChild('rebate_type', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillinqtype ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBINQHD'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_inquire_options.GetChild('inquire_type', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_checkcustid ();string						ls_customer_id,&
								ls_errorstring, &
								ls_search

long							ll_rowcount, ll_findrow


datastore					lds_bill, lds_ship, lds_corp

lds_bill = Create DataStore
lds_bill.DataObject = 'd_billto_customers'
ls_customer_id = em_customer_id.text
wf_getbilltos(ls_customer_id)
ll_rowcount = lds_bill.importstring(ls_customer_id)
ls_search = string("bill_to_id = '" + em_customer_id.text +"'")
ll_findrow = lds_bill.find(ls_search, 1, ll_rowcount)
if ll_findrow = 0 then
	lds_ship = Create DataStore
	lds_ship.DataObject = 'd_shipto_customers'
	ls_customer_id = em_customer_id.text
	wf_getshiptos(ls_customer_id)
	ll_rowcount = lds_ship.importstring(ls_customer_id)
	ls_search = string("customer_id = '" + em_customer_id.text +"'")
	ll_findrow = lds_ship.find(ls_search, 1, ll_rowcount)
	if ll_findrow = 0 then
		lds_corp = Create DataStore
		lds_corp.DataObject = 'd_corp_customers'
		ls_customer_id = em_customer_id.text
		wf_getcorps(ls_customer_id)
		ll_rowcount = lds_corp.importstring(ls_customer_id)
		ls_search = string("corp_id = '" + em_customer_id.text +"'")
		ll_findrow = lds_corp.find(ls_search, 1, ll_rowcount)
		if ll_findrow = 0 then
			ls_errorstring = string(em_customer_id.text + " is an invalid Customer ID")
			messagebox("Invalid Infomation",ls_errorstring)	
			return false
		end if
	end if
end if
return true
end function

public function boolean wf_fillcountrycode ();String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_child
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'REBCTYCD'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_rebate_credit_and_type.GetChild('rebate_type', ldwc_child)
ldwc_child.Reset()
ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);DataWindowChild			ldwc_header, &
								ldwc_display

iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillcredmeth() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If
//dmk
If Not wf_fillcountrycode() Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not wf_fillrebatetype() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

iu_ErrorContext.uf_Initialize()

//If Not iu_ClassFactory.uf_GetObject( "u_RebateDataAccess", iu_RebateDataAccess, iu_ErrorContext ) Then
//	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
//		iu_Notification.uf_Display(iu_ErrorContext)
//		Return False
//	End If
//End If

Return True

end function

on w_rebateinquire_open_b.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_rebate_inquire_options=create dw_rebate_inquire_options
this.em_customer_id=create em_customer_id
this.em_initiated=create em_initiated
this.em_date=create em_date
this.em_requested=create em_requested
this.dw_rebate_credit_and_type=create dw_rebate_credit_and_type
this.uo_division=create uo_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_rebate_inquire_options
this.Control[iCurrent+3]=this.em_customer_id
this.Control[iCurrent+4]=this.em_initiated
this.Control[iCurrent+5]=this.em_date
this.Control[iCurrent+6]=this.em_requested
this.Control[iCurrent+7]=this.dw_rebate_credit_and_type
this.Control[iCurrent+8]=this.uo_division
end on

on w_rebateinquire_open_b.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_rebate_inquire_options)
destroy(this.em_customer_id)
destroy(this.em_initiated)
destroy(this.em_date)
destroy(this.em_requested)
destroy(this.dw_rebate_credit_and_type)
destroy(this.uo_division)
end on

event open;
iu_ParameterStack = Message.PowerObjectParm



If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_inq_string)
iu_ParameterStack.uf_Pop('string', is_inq_ind)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

This.Title = is_title + ' Inquire'

uo_division.uf_initialize(iu_classfactory,iu_errorcontext,iu_parameterstack)


If Not wf_fillinqtype() Then 
	iu_notification.uf_display(iu_ErrorContext)
	RETURN -1
End If

IF is_inq_ind = '' THEN
	dw_rebate_inquire_options.SETITEM(1, 'inquire_type', 'E')
	em_date.visible = true
else
	dw_rebate_inquire_options.SETITEM(1, 'inquire_type', is_inq_ind)
	this.PostEvent("ue_postopen")
end if

is_inq_string = "Cancel"

this.dw_rebate_inquire_options.setfocus()

this.setredraw(true)


end event

event close;iu_ErrorContext.uf_initialize()

is_inq_ind = dw_rebate_inquire_options.getitemstring(1,1)

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_inq_ind)
iu_ParameterStack.uf_Push('string', is_inq_string)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

event ue_postopen;this.setredraw(false)

if is_inq_string = "Cancel" then
	is_inq_string = ""
end if

//dmk	
choose case is_inq_ind
	case 'S'
		em_date.visible = true
		if is_inq_string = "" then
			em_date.text = string(today())
		else 
			em_date.text = is_inq_string
		end if
	case 'E'
		em_date.visible = true
		if is_inq_string = "" then
			em_date.text = string(today())
		else 
			em_date.text = is_inq_string
		end if
	case 'R'
		If Not wf_fillrebatetype() Then 	
			iu_notification.uf_display(iu_ErrorContext)
		else
			dw_rebate_credit_and_type.visible = true
			dw_rebate_credit_and_type.setitem(1, 'rebate_type', is_inq_string)
		End If
	case 'I'
		em_initiated.visible = true
		em_initiated.text = is_inq_string	
	case 'Q'
		em_requested.visible = true
		em_requested.text = is_inq_string
	case 'C'
		If Not wf_fillcredmeth() Then 	
			iu_notification.uf_display(iu_ErrorContext)
		else
			dw_rebate_credit_and_type.visible = true
			dw_rebate_credit_and_type.setitem(1, 'rebate_type', is_inq_string)
		End If
	case 'L'
		If Not wf_fillcountrycode() Then 	
			iu_notification.uf_display(iu_ErrorContext)
		else
			dw_rebate_credit_and_type.visible = true
			dw_rebate_credit_and_type.setitem(1, 'rebate_type', is_inq_string)
		End If
	case 'U'
		em_customer_id.visible = true
		em_customer_id.text = is_inq_string
	case 'D'
		uo_division.visible = true
		uo_division.uf_fillindivision()
		uo_division.dw_division.setitem(1, "div_code", is_inq_string)
		uo_division.dw_division.setitem(1, "div_description", is_inq_string)
End Choose

is_inq_string = "Cancel"

this.setredraw(true)
end event

type cb_help from w_abstractresponseext`cb_help within w_rebateinquire_open_b
integer x = 2245
integer y = 308
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_rebateinquire_open_b
integer x = 2245
integer y = 172
integer width = 247
boolean cancel = true
end type

event cb_cancel::clicked;em_customer_id.text = ""
is_inq_string = "Cancel"
Close(Parent)

end event

type cb_ok from w_abstractresponseext`cb_ok within w_rebateinquire_open_b
integer x = 2245
integer y = 36
boolean default = true
end type

event cb_ok::clicked;long							ll_count

string						ls_action_ind, &
								ls_customer_id, &
								ls_errorstring
			
u_String_Functions		lu_StringFunctions
//dmk
ls_action_ind = left(dw_rebate_inquire_options.getitemstring(1, 1),1)
choose case ls_action_ind
	case "U"
		if lu_StringFunctions.nf_IsEmpty(parent.em_customer_id.text) then
			messagebox("Required Field","Customer ID is a required field.",StopSign!,OK!)
			parent.em_customer_id.setfocus()
			return
		end if
		
		if not wf_checkcustid() then
			parent.em_customer_id.setfocus()
			parent.em_customer_id.SelectText ( 1, 10 )
			return
		end if
		
		is_inq_string = parent.em_customer_id.text
		
	case "S" 
		if parent.em_date.text = "00/00/0000" then
			messagebox("Required Field","Start Date is a required field.",StopSign!,OK!)
			parent.em_date.setfocus()
			is_inq_string = "Cancel"
			return
		end if
		
		if not isdate(string(parent.em_date.text)) or Mid(parent.em_date.text,7,4) < "1900" then
			messagebox("Invalid Data","Start Date is an invalid date.",StopSign!,OK!)
			parent.em_date.setfocus()
			is_inq_string = "Cancel"
			return
		end if
		
		is_inq_string = parent.em_date.text
		
	case "E" 
		if parent.em_date.text = "00/00/0000" then
			messagebox("Required Field","End Date is a required field.",StopSign!,OK!)
			parent.em_date.setfocus()
			is_inq_string = "Cancel"
			return
		end if
		
		if not isdate(string(parent.em_date.text)) or Mid(parent.em_date.text,7,4) < "1900" then
			messagebox("Invalid Data","End Date is an invalid date.",StopSign!,OK!)
			parent.em_date.setfocus()
			is_inq_string = "Cancel"
			return
		end if
		
		is_inq_string = parent.em_date.text
		
	case "R" 
		if lu_StringFunctions.nf_IsEmpty(parent.dw_rebate_credit_and_type.getitemstring(1,1)) then
			messagebox("Required Field","Rebate Type is a required field.",StopSign!,OK!)
			parent.dw_rebate_credit_and_type.setfocus()
			return
		end if
		is_inq_string = parent.dw_rebate_credit_and_type.getitemstring(1,1)
		
	case "C" 
		if lu_StringFunctions.nf_IsEmpty(parent.dw_rebate_credit_and_type.getitemstring(1,1)) then
			messagebox("Required Field","Credit Type is a required field.",StopSign!,OK!)
			parent.dw_rebate_credit_and_type.setfocus()
			return
		end if
		is_inq_string = parent.dw_rebate_credit_and_type.getitemstring(1,1)
	
	case "L" 
		if lu_StringFunctions.nf_IsEmpty(parent.dw_rebate_credit_and_type.getitemstring(1,1)) then
			messagebox("Required Field","Country Code is a required field.",StopSign!,OK!)
			parent.dw_rebate_credit_and_type.setfocus()
			return
		end if
		is_inq_string = parent.dw_rebate_credit_and_type.getitemstring(1,1)
		
	case "I" 
		if lu_StringFunctions.nf_IsEmpty(parent.em_initiated.text) then
			messagebox("Required Field","Initiated By is a required field.",StopSign!,OK!)
			parent.em_initiated.setfocus()
			parent.em_initiated.SelectText ( 1, 100 )
			return
		end if
		is_inq_string = parent.em_initiated.text

	case "Q" 
		if lu_StringFunctions.nf_IsEmpty(parent.em_requested.text) then
			messagebox("Required Field","Requested by is a required field.",StopSign!,OK!)
			parent.em_requested.setfocus()
			parent.em_requested.SelectText ( 1, 100 )
			return
		end if
		is_inq_string = parent.em_requested.text
		
	case "D" 
		parent.uo_division.dw_division.accepttext()
		if lu_StringFunctions.nf_IsEmpty(parent.uo_division.dw_division.getitemstring(1, 1)) then
			messagebox("Required Field","Division is a required field.",StopSign!,OK!)
			parent.uo_division.dw_division.setfocus()
			parent.uo_division.dw_division.SelectText ( 1, 100 )
			return
		end if

		is_inq_string = parent.uo_division.dw_division.getitemstring(1, 1)
		
	case else
		messagebox("Required Field","Inquire Options is a required field.",StopSign!,OK!)
		parent.dw_rebate_inquire_options.setfocus()
		return
		
end choose

Close(Parent)
end event

type st_1 from statictext within w_rebateinquire_open_b
integer x = 114
integer y = 24
integer width = 274
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Charges By:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_rebate_inquire_options from datawindow within w_rebateinquire_open_b
integer x = 59
integer y = 92
integer width = 855
integer height = 88
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_rebate_inquire_options"
boolean border = false
boolean livescroll = true
end type

event itemchanged;parent.dw_rebate_credit_and_type.visible = false
parent.em_customer_id.visible = false
parent.em_date.visible = false
parent.em_initiated.visible = false
parent.em_requested.visible = false
parent.uo_division.visible = false

choose case left(data,1)
	case 'S'
		parent.em_date.visible = true
		parent.em_date.text = string(today())
	case 'E'
		parent.em_date.visible = true
		parent.em_date.text = string(today())
	case 'R'
		If Not wf_fillrebatetype() Then 	
			iu_notification.uf_display(iu_ErrorContext)
		else
			parent.dw_rebate_credit_and_type.visible = true
			parent.dw_rebate_credit_and_type.setitem(1,1,'A')
		End If
		
	case 'I'
		parent.em_initiated.visible = true
		parent.em_initiated.text = ""
	case 'Q'
		parent.em_requested.visible = true
		parent.em_requested.text = ""
	case 'C'
		If Not wf_fillcredmeth() Then 	
			iu_notification.uf_display(iu_ErrorContext)
		else
			parent.dw_rebate_credit_and_type.visible = true
			parent.dw_rebate_credit_and_type.setitem(1,1,'C')
		End If
//dmk
	case 'L'
		If Not wf_fillcountrycode() Then 	
			iu_notification.uf_display(iu_ErrorContext)
		else
			parent.dw_rebate_credit_and_type.visible = true
			parent.dw_rebate_credit_and_type.setitem(1,1,'A')
		End If
	case 'U'
		parent.em_customer_id.visible = true
		parent.em_customer_id.text = ""
	case 'D'
		parent.uo_division.visible = true
		parent.uo_division.uf_fillindivision()
		parent.uo_division.dw_division.setitem(1,"div_code",'00')
		parent.uo_division.dw_division.setitem(1,"div_description",'00')
End Choose

//this.setredraw(true)
end event

event itemerror;return -1
end event

event constructor;this.insertrow(0)
parent.em_date.text = string(today())
end event

type em_customer_id from editmask within w_rebateinquire_open_b
boolean visible = false
integer x = 960
integer y = 92
integer width = 261
integer height = 88
integer taborder = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!!"
end type

type em_initiated from editmask within w_rebateinquire_open_b
boolean visible = false
integer x = 960
integer y = 92
integer width = 306
integer height = 88
integer taborder = 70
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!!"
end type

type em_date from editmask within w_rebateinquire_open_b
event doubleclicked pbm_lbuttondblclk
boolean visible = false
integer x = 974
integer y = 92
integer width = 311
integer height = 88
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "mm/dd/yyyy"
end type

event doubleclicked;u_AbstractParameterStack	lu_Stack
String							ls_Date

ls_date = String(Today())

iu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,iu_ErrorContext)

// Set up the parameters for the calendar window
lu_Stack.uf_Push("u_AbstractClassFactory",iu_ClassFactory)
lu_stack.uf_Push("String", ls_Date)
lu_Stack.uf_Push("String", "d_calendar")
lu_Stack.uf_Push("u_AbstractErrorContext",iu_ErrorContext)

// Open the calendar window
iu_ClassFactory.uf_GetResponseWindow("w_calendar",lu_Stack)
lu_Stack = Message.PowerObjectParm
If Not Isvalid( lu_stack ) Then Return
lu_Stack.uf_Pop("u_AbstractErrorContext",iu_ErrorContext)

// Was it successful?  If so, get the date passed back and set the column.
If iu_ErrorContext.uf_IsSuccessful() Then
	lu_Stack.uf_Pop("String",ls_Date)
	If Len(Trim(ls_Date)) = 0 Then Return
	This.Text = ls_date
// I put this in to fix a problem, however it caused it to trigger itemchanged
// twice which added two new lines.  When I took it out the original problem
// didn't show up.  I will leave it like this until the problems shows up.
//	If This.Event ItemChanged(row, dwo, ls_date) <> 0 Then Return 0
END IF
end event

type em_requested from editmask within w_rebateinquire_open_b
boolean visible = false
integer x = 960
integer y = 92
integer width = 1179
integer height = 88
integer taborder = 90
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
end type

type dw_rebate_credit_and_type from datawindow within w_rebateinquire_open_b
boolean visible = false
integer x = 960
integer y = 92
integer width = 722
integer height = 88
integer taborder = 60
string dataobject = "d_rebate_credit_and_type"
boolean border = false
boolean livescroll = true
end type

event constructor;this.insertrow(0)
end event

type uo_division from u_division within w_rebateinquire_open_b
boolean visible = false
integer x = 960
integer y = 92
integer height = 88
integer taborder = 30
end type

on uo_division.destroy
call u_division::destroy
end on

