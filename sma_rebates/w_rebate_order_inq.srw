HA$PBExportHeader$w_rebate_order_inq.srw
forward
global type w_rebate_order_inq from w_abstractresponse
end type
type dw_sales_order from datawindow within w_rebate_order_inq
end type
end forward

global type w_rebate_order_inq from w_abstractresponse
integer width = 1221
integer height = 400
string title = "Inquire Sales Order ID"
boolean contexthelp = true
dw_sales_order dw_sales_order
end type
global w_rebate_order_inq w_rebate_order_inq

type variables
String				is_inq_order, &
                  is_inq_string, &
						is_title, &
						is_Sales_Order

u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractTutlTypeDataAccess	iu_TutlTypeDataAccess
u_AbstractClassFactory		iu_ClassFactory

u_NotificationController		iu_Notification	

w_smaframe			iw_frame	
end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

iu_ErrorContext.uf_Initialize()

Return True
end function

on w_rebate_order_inq.create
int iCurrent
call super::create
this.dw_sales_order=create dw_sales_order
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_order
end on

on w_rebate_order_inq.destroy
call super::destroy
destroy(this.dw_sales_order)
end on

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_inq_string)
iu_ParameterStack.uf_Pop('string', is_inq_order)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

This.SetFocus()
this.setredraw(true)
end event

event ue_postopen;call super::ue_postopen;this.setredraw(false)

if is_inq_string = "Cancel" then
	is_inq_string = ""
end if

dw_sales_order.SetItem(1,"sales_order_id", is_inq_order)

this.setredraw(true)
end event

event close;call super::close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_inq_order)
iu_ParameterStack.uf_Push('string', is_inq_string)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

type cb_help from w_abstractresponse`cb_help within w_rebate_order_inq
integer x = 389
integer y = 364
integer taborder = 0
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_rebate_order_inq
integer x = 891
integer y = 180
integer taborder = 30
boolean cancel = true
end type

event cb_cancel::clicked;call super::clicked;is_inq_string = 'Cancel'

Close(Parent)
end event

type cb_ok from w_abstractresponse`cb_ok within w_rebate_order_inq
integer x = 910
integer y = 20
integer taborder = 20
boolean default = true
end type

event cb_ok::clicked;call super::clicked;String					ls_SalesOrder

u_string_functions	lu_string


If dw_sales_order.AcceptText() = -1 Then Return

//is_Sales_Order = dw_sales_order.GetItemString(1, 'sales_order_id')
//
//If lu_string.nf_IsEmpty(is_Sales_order) Then
//	iw_frame.SetMicroHelp('Sales Order ID is a required field')
//	dw_sales_order.SetFocus()
//	dw_sales_order.SetColumn('sales_order_id')
//	dw_sales_order.SelectText(1, 10000)
//	Return
//End If

ls_salesorder = dw_sales_order.GetItemString(1, "sales_order_id")

If (ls_salesorder = "00000") &
	or (lu_string.nf_isEmpty(ls_salesorder)) then
	messagebox("Required Field","Sales Order Id is a required field.",StopSign!,OK!)
	dw_sales_order.setfocus()
	dw_sales_order.SetColumn('sales_order_id')
	is_inq_string = "Cancel"
	return
End if

is_inq_order = ls_salesorder

Close(Parent)


end event

type dw_sales_order from datawindow within w_rebate_order_inq
integer x = 69
integer y = 92
integer width = 709
integer height = 96
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_sales_order"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;long							ll_count

string						ls_action_ind, &
								ls_sales_order_id, &
								ls_errorstring
			
u_String_Functions		lu_String

//ls_sales_order_id = dw_sales_order.GetItemString(1, "sales_order_id")

If (data = "00000") &
	or (lu_string.nf_isEmpty(data)) then
	messagebox("Required Field","Sales Order Id is a required field.",StopSign!,OK!)
	dw_sales_order.setfocus()
	dw_sales_order.SetColumn('sales_order_id')
	is_inq_string = "Cancel"
	return
End if

is_inq_order = data

//For some weird reason, the close below worked in PB8, but not in PB12, so commenting out
//The clicked event on the OK button will perform the Close 

//Close(Parent)
end event

event constructor;dw_sales_order.insertrow(0)
end event

event itemerror;return -1
end event

event itemfocuschanged;This.SelectText(1, 100)
end event

