HA$PBExportHeader$w_productpopup.srw
forward
global type w_productpopup from window
end type
type dw_productcodeshort from datawindow within w_productpopup
end type
end forward

global type w_productpopup from window
string tag = "Product"
integer x = 251
integer y = 208
integer width = 1056
integer height = 1188
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = popup!
long backcolor = 79741120
event ue_postopen ( )
event ue_losefocus pbm_dwnkillfocus
dw_productcodeshort dw_productcodeshort
end type
global w_productpopup w_productpopup

type variables
u_AbstractClassFactory			iu_ClassFactory 
u_abstractErrorContext                       		iu_ErrorContext


w_rebateselect         	iw_parent
u_olecom			iu_olepfm

u_user                                 iu_user
u_ParameterStack	           iu_ParameterStack
u_NotificationController       iu_NotificationController

String		is_userid, is_userpw, &
                	is_updateowner, &
                                is_group_id
end variables

forward prototypes
public subroutine wf_listproducts (string as_productgroupid)
public function string wf_getserver ()
public function boolean wf_initializesma ()
end prototypes

event ue_postopen;


wf_initializesma()

//iw_parent.trigger event ue_get_data("group_id")
//is_group_id = Message.StringParm

wf_listproducts(is_group_id)

end event

event ue_losefocus;close(this)
end event

public subroutine wf_listproducts (string as_productgroupid);Long 										ll_count, ll_index

String 									ls_label, ls_id, ls_descr, ls_product
 
integer									li_count, li_item

u_abstractErrorContext				lu_ErrorContext
u_productdataaccess    				lu_productdataaccess
u_AbstractNotificationController	lu_Notification
	
li_count = iu_olepfm.count()
li_item = 1
do until trim(as_productgroupid) = String(iu_olepfm.item(li_item).Groupid) or li_item > li_count
	li_item ++
loop  

ll_count = iu_olepfm.item(li_item).count
ls_label = ''

If Not iu_ClassFactory.uf_GetObject( "u_ProductDataAccess", lu_ProductDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		iu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		lu_productdataaccess.uf_Initialize( iu_ParameterStack )
		iu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		iu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			lu_Notification.uf_Display(iu_ErrorContext)
			Return  
		End If
	Else 
		lu_Notification.uf_Display(iu_ErrorContext)
		Return 
	End If
End If

FOR ll_index = 1 TO ll_count
	
	ls_product = iu_olepfm.item(li_item).item(ll_index).ProductCode
	
	iu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_errorcontext)
	iu_ParameterStack.uf_Push('string', ls_product)

	//call function
	lu_productdataaccess.uf_retrieveproducts(iu_ParameterStack)
	
	iu_ParameterStack.uf_Pop('string', ls_product)
	iu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_errorcontext)

	ls_label += ls_product + '~r~n'
	
NEXT

dw_productcodeshort.ImportString(ls_label)
end subroutine

public function string wf_getserver ();String	ls_return, ls_filename


//ls_filename = iw_grpmaint.wf_get_working_dir() + "ibp002.ini"
ls_filename = "ibp002.ini"

ls_return = ProfileString (ls_filename, "Netwise Server Info", "ServerPrefix", "dkmvs00.")
ls_return += "pfm001"
ls_return +=ProfileString (ls_filename, "Netwise Server Info", "ServerSuffix", "t")


Return ls_return
end function

public function boolean wf_initializesma ();String		ls_compare, ls_4compare, ls_app_name, ls_server
Integer		li_ret, li_count

SetPointer(HourGlass!)

iu_olepfm = Create u_olecom

li_ret = iu_olepfm.ConnectToNewObject("ProductGroupMaintenance.ProductGroupList.1")

If li_ret < 0 Then
	MessageBox("Couldn't Connect", "ConnectToNewObject returned: " + String(li_ret))
	SetPointer(Arrow!)
	Return False
End If

//ls_app_name = GetApplication().AppName

//iw_parent.trigger event ue_get_data("user_id")
//ls_user_id = Message.StringParm
//
//iw_parent.trigger event ue_get_data("password")
//ls_user_pw = Message.StringParm
ls_server = wf_getserver()


iu_olepfm.Initialize(is_userid, is_userpw, ls_server)


//iw_parent.trigger event ue_get_data("group_owner")
//is_updateowner = Message.StringParm
//Choose Case ls_app_name
//	Case "pas" 
//		is_updateowner = "SCHED"
//	Case "orp" 
//		is_updateowner = "SALES"
//	Case "sma"
//		is_updateowner = "SMACC"
//	Case Else
//		is_updateowner = "APPER"
//End Choose

SetPointer(Arrow!)
iu_olepfm.Retrieve()
//li_count = iu_olepfm.count()
iu_olepfm.filter(is_updateowner)

//li_count = iu_olepfm.count()
Return True
end function

on w_productpopup.create
this.dw_productcodeshort=create dw_productcodeshort
this.Control[]={this.dw_productcodeshort}
end on

on w_productpopup.destroy
destroy(this.dw_productcodeshort)
end on

event close;
iu_ErrorContext.uf_initialize()
iu_ParameterStack.uf_Initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('string', is_userid)
iu_ParameterStack.uf_Push('string', is_userpw)
iu_ParameterStack.uf_Push('string', is_group_id)
iu_ParameterStack.uf_Push('string', is_updateowner)
iu_ParameterStack.uf_Push('window', this)

CloseWithReturn(This, iu_ParameterStack)

end event

event open;long		ll_display

iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('window', iw_parent)
iu_ParameterStack.uf_Pop('string', is_updateowner)
iu_ParameterStack.uf_Pop('string', is_group_id)
iu_ParameterStack.uf_Pop('string', is_userpw)
iu_ParameterStack.uf_Pop('string', is_userid)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_errorcontext)

This.PostEvent('ue_postopen')

//If message.powerobjectparm.TypeOf() = window! Then
//	iw_parent = message.powerobjectparm
//End If
this.title = "Product Group Product List"
dw_productcodeshort.object.product_code.protect = 1
dw_productcodeshort.Modify ( "product_code.Background.Color = 12632256" )


end event

type dw_productcodeshort from datawindow within w_productpopup
integer x = 14
integer y = 16
integer width = 1010
integer height = 1064
integer taborder = 10
string dataobject = "d_productcodeshort"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event losefocus;close(parent)
end event

