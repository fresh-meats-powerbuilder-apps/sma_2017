HA$PBExportHeader$w_rebate_order_detail.srw
forward
global type w_rebate_order_detail from w_abstractsheetext
end type
type dw_rebate_order_list from datawindow within w_rebate_order_detail
end type
end forward

global type w_rebate_order_detail from w_abstractsheetext
boolean visible = true
integer width = 4507
string title = "Applied Rebates Sales Orders"
dw_rebate_order_list dw_rebate_order_list
end type
global w_rebate_order_detail w_rebate_order_detail

type variables
w_rebate_order_detail  iw_parent

string                 is_title, &
                       is_inq_order, &
							  is_date_type, &
							  is_customer_type, &
							  is_customer_id, &
							  is_sales_division, &
							  is_unapplied_rebate_ind, &
                       is_inq_string, &
							  is_dddw_uom_string, &
							  is_parameter

date						  idt_from_date, &
							  idt_to_date 
							  
boolean                ib_reinquire

u_ErrorContext		      iu_ErrorContext
u_AbstractClassFactory	      iu_ClassFactory
u_NotificationController	    iu_Notification	
u_RebateDataAccess	      iu_RebateDataAccess
u_AbstractTutlTypeDataAccess    iu_TutlTypeDataAccess
w_smaframe		      iw_frame	
end variables

forward prototypes
public function string wf_buildupdatestring ()
public function boolean wf_getdetailuom ()
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack)
end prototypes

public function string wf_buildupdatestring ();Long					ll_ModifiedCount, &
						ll_count
						
String				ls_UpdateString

dwItemStatus		lis_UpdateStatus


ll_ModifiedCount = dw_rebate_order_list.ModifiedCount()

If ll_ModifiedCount <= 0 Then
	SetMicroHelp('No Update Necessary')
	Return ''
End If

ls_UpdateString = ''

ll_Count = 0
IF ll_ModifiedCount > 0 Then
//	If Not wf_validate() Then Return ''
	ll_Count = dw_rebate_order_list.GetNextModified(ll_Count, Primary!)
	Do
		dw_rebate_order_list.SelectRow(0, False)
		dw_rebate_order_list.SelectRow(ll_count, True)
		lis_UpdateStatus = dw_rebate_order_list.GetItemStatus(ll_Count, 0, Primary!)
		Choose Case lis_UpdateStatus
			Case DataModified!
					ls_UpdateString += dw_rebate_order_list.GetItemString(ll_Count, 'order_id') + '~t' + &
							dw_rebate_order_list.GetItemString(ll_Count, 'line_number') + '~t' + &
							String(dw_rebate_order_list.GetItemNumber(ll_Count, 'accrual_amt')) + '~t' + &
							String(dw_rebate_order_list.GetItemNumber(ll_Count, 'buydown_amt')) + &							
							'~r~n'
			Case Else
				SetMicroHelp('Update Status is ' + String(lis_UpdateStatus) + &
				'.  Please Call Applications.')
				Return ''
		End Choose
		ll_Count = dw_rebate_order_list.GetNextModified(ll_Count, Primary!)
	Loop While ll_Count > 0
End If	

dw_rebate_order_list.SelectRow(0, False)

Return ls_UpdateString

end function

public function boolean wf_getdetailuom ();Long									ll_temp
String								ls_tutltype, ls_TransferTypeString
DataWindowChild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

ls_TutlType = 'PUOM'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)
iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

is_dddw_uom_string = ls_TransferTypeString
dw_rebate_order_list.GetChild('pricing_uom', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack);String 	ls_ClassName, &
			ls_Parameter, & 
			ls_WindowName, &
			ls_Type
			
iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not IsValid(au_parameterstack) Then 
	MessageBox("Open","This window can not be opened in this manner.")
	Close(This)
	Return FALSE
END IF


au_parameterstack.uf_Pop("string",ls_Type)
au_parameterstack.uf_Pop("string",ls_WindowName)
au_parameterstack.uf_Pop("string",is_Parameter)
au_parameterstack.uf_Pop("string",ls_ClassName)

IF NOT wf_initialize(au_ClassFactory,au_ErrorContext) THEN
	Close(this)
	Return FALSE
END IF

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_RebateDataAccess", iu_RebateDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

If Not wf_getdetailuom() Then 	
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

this.title = ls_WindowName

This.Event Post ue_inquire()

Return True
end function

on w_rebate_order_detail.create
int iCurrent
call super::create
this.dw_rebate_order_list=create dw_rebate_order_list
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_rebate_order_list
end on

on w_rebate_order_detail.destroy
call super::destroy
destroy(this.dw_rebate_order_list)
end on

event close;call super::close;close(this)
end event

event closequery;call super::closequery;Return 0
end event

event open;call super::open;
iw_parent = This
is_Title = This.Title
is_inq_order = ""


end event

event ue_inquire;call super::ue_inquire;string					ls_AppName, &
							ls_WindowName, &
							ls_RebateinquireString, &
							ls_RebateOutputString

integer					li_return_code

long						ll_rec_count
					

DataWindowChild		ldwc_child_uom

u_string_functions	lu_string

u_ParameterStack		lu_ParameterStack

iu_ErrorContext.uf_Initialize()

dw_rebate_order_list.reset()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
	
IF Not ib_ReInquire Then
	This.Event Trigger CloseQuery()
	
	If Message.ReturnValue <> 0 Then Return False
	
	If is_parameter = 'O' Then
			
		lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Push('string', is_inq_order)
		lu_ParameterStack.uf_Push('string', is_inq_string)
		lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
	
		iu_ErrorContext.uf_Initialize()
	
		iu_ClassFactory.uf_GetResponseWindow( "w_rebate_order_inq", lu_ParameterStack )
	
		lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
		lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Pop('string', is_inq_string)
		lu_ParameterStack.uf_Pop('string', is_inq_order)
		lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
	
		If Not iu_ErrorContext.uf_IsSuccessful( ) Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
		If is_inq_string = 'Cancel' Then 
			li_return_code = messagebox("Caution","Do you want to close this window?",StopSign!,YesNo!,2)
			if li_return_code = 1 then
				this.triggerevent(Close!)
				return false
			else
				Return False
			end if
		End If
	End If 	
	If is_parameter = 'D' Then
		
		lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
		lu_parameterstack.uf_Push('string', is_date_type)
		lu_parameterstack.uf_Push('date', idt_from_date)
		lu_parameterstack.uf_Push('date', idt_to_date)
		lu_parameterstack.uf_Push('string', is_customer_type)
		lu_parameterstack.uf_Push('string', is_customer_id)
		lu_parameterstack.uf_Push('string', is_sales_division)
		lu_parameterstack.uf_Push('string', is_unapplied_rebate_ind)
		lu_parameterstack.uf_Push('string', is_inq_string)
		lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
	
		iu_ErrorContext.uf_Initialize()
	
		iu_ClassFactory.uf_GetResponseWindow( "w_rebate_date_inq", lu_ParameterStack )
	
		lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
		lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
		lu_parameterstack.uf_Pop('string', is_inq_string)
		lu_parameterstack.uf_Pop('string', is_unapplied_rebate_ind)
		lu_parameterstack.uf_Pop('string', is_sales_division)
		lu_parameterstack.uf_Pop('string', is_customer_id)
		lu_parameterstack.uf_Pop('string', is_customer_type)
		lu_parameterstack.uf_Pop('date', idt_to_date)
		lu_parameterstack.uf_Pop('date', idt_from_date)
		lu_parameterstack.uf_Pop('string', is_date_type)
		lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
	
		If Not iu_ErrorContext.uf_IsSuccessful( ) Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
		If is_inq_string = 'Cancel' Then 
			li_return_code = messagebox("Caution","Do you want to close this window?",StopSign!,YesNo!,2)
			if li_return_code = 1 then
				this.triggerevent(Close!)
				return false
			else
				Return False
			end if
		End If
	End If
End If

ls_AppName = GetApplication().AppName
ls_WindowName = 'applrebate'
ls_RebateinquireString = is_parameter + "~t" + is_date_type + "~t" + & 
	string((idt_from_date), "YYYY-MM-DD") + '~t' + string((idt_to_date), "YYYY-MM-DD") + '~t' + &
	is_customer_type + "~t" + is_customer_id + "~t" + is_sales_division + "~t" + &
	is_unapplied_rebate_ind + "~t" + is_inq_order + "~t" +'~r~n'

iu_ErrorContext.uf_Initialize()

SetPointer(HourGlass!)

lu_ParameterStack.uf_initialize()

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_RebateinquireString)

iu_RebateDataAccess.uf_retrieveappliedrebates( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_RebateOutputString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If

ll_rec_count = dw_rebate_order_list.ImportString(ls_RebateOutputString)
//dmk
If IsNull(is_dddw_uom_string) Then
	If Not wf_getdetailuom() Then 	
		iu_notification.uf_display(iu_ErrorContext)
		Return False
	End If
Else
	dw_rebate_order_list.GetChild('pricing_uom', ldwc_child_uom)
	ldwc_child_uom.Reset()
	ldwc_child_uom.ImportString(is_dddw_uom_string)
End If

If ll_rec_count > 0 Then
	SetMicroHelp(String(ll_rec_count) + ' Rows Retrieved')
Else
	SetMicroHelp('No Rows Retrieved')
	dw_rebate_order_list.InsertRow(0)
	This.SetRedraw(True)
	Return True
End If


dw_rebate_order_list.ResetUpdate()

//dw_rebate_order_list.setsort("Order_id, line_number")
//dw_rebate_order_list.sort()
//
This.SetRedraw(True)

ib_ReInquire = False

Return True

end event

event ue_save;call super::ue_save;Long						ll_row
String					ls_detailstring

DataWindowChild						ldwc_TransferCustomer

u_ParameterStack						lu_ParameterStack

u_String_Functions					lu_strings


iu_ErrorContext.uf_Initialize()

IF dw_rebate_order_list.AcceptText() = -1 THEN Return False

This.SetRedraw(False)

ls_DetailString = wf_BuildUpdateString()

If lu_strings.nf_IsEmpty(ls_DetailString) Then 
	This.SetRedraw(True)
	Return False
End If

SetMicroHelp("Wait... Updating the Database")

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_RebateDataAccess.uf_updateappliedrebates( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

This.SetRedraw(True)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
Else
	ib_reinquire = True	
	This.TriggerEvent ("ue_inquire")
End If

dw_rebate_order_list.ResetUpdate()

SetMicroHelp("Update Successful")
Return True
end event

type dw_rebate_order_list from datawindow within w_rebate_order_detail
integer x = 59
integer y = 40
integer width = 4389
integer height = 1216
integer taborder = 10
string title = "none"
string dataobject = "d_rebate_order_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

