HA$PBExportHeader$u_rebate_header.sru
forward
global type u_rebate_header from datawindow
end type
end forward

global type u_rebate_header from datawindow
int Width=2039
int Height=112
int TabOrder=10
string DataObject="d_rebate_header_inquire"
boolean Border=false
boolean LiveScroll=true
event ue_accept ( )
end type
global u_rebate_header u_rebate_header

type variables
u_AbstractClassFactory		iu_Factory
u_AbstractErrorContext		iu_error
u_AbstractNotificationController	iu_notify
Boolean				ib_required
end variables

forward prototypes
public function string uf_getrebatecode ()
public function string uf_getrebatedescription ()
public subroutine uf_setrebatecode (string as_value)
public function boolean uf_fillrebatedropdown ()
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_notificationcontroller au_notificationcontroller, boolean ab_required, boolean ab_enabled)
public function boolean uf_validate (ref string as_returnstring)
end prototypes

event ue_accept;This.AcceptText()

end event

public function string uf_getrebatecode ();return Trim(This.GetItemString(1, "rebate_code"))
end function

public function string uf_getrebatedescription ();return Trim(This.GetItemString(1, "rebate_description"))
end function

public subroutine uf_setrebatecode (string as_value);DataWindowChild		ldwc_child

Long	ll_row, &
		ll_row_count

String	ls_text, &
			ls_description


This.SetItem(1, "rebate_code", as_value)

This.GetChild("rebate_code",ldwc_child)

ls_text = 'rebate_id = ' + as_value
ll_row_count = ldwc_child.RowCount()
ll_row = ldwc_child.Find(ls_text, 1, ll_row_count)
If ll_row <= 0 Then
	If ib_required OR as_value <> "  " Then
		iu_error.uf_SetText(as_value + " is an Invalid Rebate Description")
		This.SetFocus()
//		This.SelectText(1, Len(ls_text))
		This.SelectText(1, 100)
	End If
	ls_description = ""
Else
	This.SetFocus()
	This.SelectText(1, 100)
//	This.SelectText(1, Len(ls_text))
	ls_Description = ldwc_child.GetItemString(ll_row, "description")
End if

This.SetItem(1, "rebate_description", ls_Description)


end subroutine

public function boolean uf_fillrebatedropdown ();Boolean								lb_temp

Long									ll_temp

String								ls_appname, &
										ls_windowname, &
										ls_RebateListString
							
DataWindowChild					ldwc_RebateCode
							
u_AbstractParameterStack		lu_ParameterStack	

u_rebateDataAccess				lu_rebateDataAccess



iu_error.uf_initialize()

If Not iu_factory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_Error ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_factory.uf_GetObject( "u_RebateDataAccess", lu_rebateDataAccess, iu_Error ) Then
	If Not iu_Error.uf_IsSuccessful( ) Then
		iu_Notify.uf_Display(iu_Error)
		Return False
	End If
End If

ls_appname = GetApplication().appname
ls_windowname = 'w_rebatemain'
ls_RebateListString = ''

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_Factory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_Error)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_RebateListString)

lu_RebateDataAccess.uf_RetrieveList( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_RebateListString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_Error)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_Factory)

If Not iu_Error.uf_IsSuccessful( ) Then
	iu_Notify.uf_Display(iu_Error)
	Return False
End If

This.GetChild('rebate_code', ldwc_RebateCode)
ldwc_RebateCode.ImportString(ls_RebateListString)
ldwc_RebateCode.SetSort("#2 A")
ldwc_RebateCode.Sort()


Return True

end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_notificationcontroller au_notificationcontroller, boolean ab_required, boolean ab_enabled);iu_factory = au_ClassFactory
iu_error = au_ErrorContext
iu_notify = au_NotificationController

ib_required = ab_required

If not ab_enabled Then
	This.object.rebate_code.Background.color = 12632256
	This.object.rebate_code.Protect = 1
End If

If not This.uf_FillRebateDropDown() Then Return False
This.SetItem(1, 'rebate_code', '')

Return True
end function

public function boolean uf_validate (ref string as_returnstring);String					ls_RebateCode, &
							ls_text              // Added for invalid rebat code   *CJR///
							
long						ll_row					// Added for invalid rebat code   *CJR///

u_String_Functions		lu_strings
DataWindowChild		ldwc_child				// Added for invalid rebat code   *CJR///


If This.AcceptText() < 0 Then Return False

as_ReturnString = This.GetItemString(1, 'rebate_code')
IF lu_strings.nf_IsEmpty(as_ReturnString) Then
	iu_error.uf_SetText('Rebate Code is a required field')
	iu_error.uf_SetReturnCode(1)
	iu_notify.uf_display(iu_error)
	This.SetFocus()
	This.SetColumn('rebate_code')
	This.SelectText(1, 1000)
	Return False
End If

// added to check for invalid rebate code     *CJR////////////////////
This.GetChild('rebate_code', ldwc_child)
ls_text = "rebate_id = " + as_returnstring
ll_row = ldwc_child.Find(ls_text, 1, ldwc_child.RowCount())
If ll_row <= 0 Then
	iu_error.uf_SetText(as_returnstring + " is an Invalid Rebate Code")
	iu_error.uf_SetReturnCode(1)
	iu_notify.uf_display(iu_error)
	This.SetFocus()
	This.SelectText(1, Len(as_returnstring))
	return false
End if
//////////////////////////////////////////////////////////////////////

as_ReturnString += '~t' + This.GetItemString(1, 'rebate_description')
Return True
end function

event constructor;This.InsertRow(0)
end event

event itemchanged;DataWindowChild		ldwc_child

Long	ll_row

String	ls_text, &
			ls_description


iu_error.uf_initialize()

If Len(Trim(data)) = 0 Then 
	If ib_required Then
		This.SetItem(1, "rebate_description", "")	
		iu_error.uf_SetText("Rebate Description is a required field")
		iu_error.uf_SetReturnCode(1)
		iu_notify.uf_display(iu_error)
		return 1
	Else
		This.SetItem(1, "rebate_description", "")	
		iu_error.uf_SetText("Ready")
		iu_error.uf_SetReturnCode(1)
		iu_notify.uf_display(iu_error)
		return
	End If
End if
This.GetChild('rebate_code', ldwc_child)
ls_text = "rebate_id = " + data
ll_row = ldwc_child.Find(ls_text, 1, ldwc_child.RowCount())
If ll_row <= 0 Then
	iu_error.uf_SetText(data + " is an Invalid Rebate Code")
	iu_error.uf_SetReturnCode(1)
	iu_notify.uf_display(iu_error)
	This.SetFocus()
	This.SelectText(1, 100)
	return 1
Else
	This.SelectText(1, 100)
	ls_Description = ldwc_child.GetItemString(ll_row, "description")
End if

This.SetItem(1, "rebate_description", ls_Description)

iu_error.uf_SetText("Ready")
iu_error.uf_SetReturnCode(1)
iu_notify.uf_display(iu_error)

end event

event itemerror;return 1
end event

event losefocus;This.Event Post ue_Accept()

end event

