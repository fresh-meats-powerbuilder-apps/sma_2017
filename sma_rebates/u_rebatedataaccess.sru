HA$PBExportHeader$u_rebatedataaccess.sru
forward
global type u_rebatedataaccess from nonvisualobject
end type
end forward

global type u_rebatedataaccess from nonvisualobject
end type
global u_rebatedataaccess u_rebatedataaccess

forward prototypes
public function boolean uf_retrievelist (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_rebate_inquire (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_productgroupcheck (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieveappliedrebates (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_updateappliedrebates (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_customergroupcheck (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrievelist (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_rebate_inquire (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_productgroupcheck (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_retrieveappliedrebates (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_updateappliedrebates (ref u_abstractparameterstack au_parameterstack);
return True
end function

public function boolean uf_customergroupcheck (ref u_abstractparameterstack au_parameterstack);return true
end function

on u_rebatedataaccess.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_rebatedataaccess.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

