HA$PBExportHeader$w_rebate_date_inq.srw
forward
global type w_rebate_date_inq from w_abstractresponse
end type
type dw_applied_rebate_select from datawindow within w_rebate_date_inq
end type
end forward

global type w_rebate_date_inq from w_abstractresponse
integer width = 2258
integer height = 1380
string title = "Inquire Applied Charges"
dw_applied_rebate_select dw_applied_rebate_select
end type
global w_rebate_date_inq w_rebate_date_inq

type variables
string                 is_title, &
                       is_inq_order, &
							  is_date_type, &
							  is_customer_type, &
							  is_customer_id, &
							  is_sales_division, &
							  is_unapplied_rebate_ind, &
                       is_inq_string, &
							  is_parameter
							 

date						  idt_from_date, &
							  idt_to_date 
							  

u_abstracterrorcontext				iu_errorcontext
u_CustomerDataAccess					iu_CustomerDataAccess
u_abstractparameterstack			iu_parameterstack
u_AbstractTutlTypeDataAccess		iu_TutlTypeDataAccess
u_AbstractClassFactory				iu_ClassFactory
u_DivisionDataAccess					iu_DivisionDataAccess
u_NotificationController			iu_Notification	

w_smaframe			iw_frame	
end variables

forward prototypes
public function boolean wf_getshiptos (string as_shipto, ref string as_shipto_string)
public function boolean wf_getcorps (string as_corp, ref string as_corp_string)
public function boolean wf_getbilltos (string as_bill_to, ref string as_billto_string)
public function boolean wf_getdivisions (string as_division, ref string as_divisionstring)
end prototypes

public function boolean wf_getshiptos (string as_shipto, ref string as_shipto_string);Long									ll_temp

Datawindowchild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_shipto)

iu_CustomerDataAccess.uf_RetrieveShiptos(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_shipto_string)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_applied_rebate_select.GetChild('shipto_customer_id', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(as_shipto_string)

ldwc_child.setsort("Customer_id")
ldwc_child.sort()

Return True

end function

public function boolean wf_getcorps (string as_corp, ref string as_corp_string);Long									ll_temp

Datawindowchild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_corp)

iu_CustomerDataAccess.uf_RetrieveCorps(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_corp_string)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_applied_rebate_select.GetChild('corp_customer_id', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(as_corp_string)

ldwc_child.setsort("Corp_id")
ldwc_child.sort()

return true
end function

public function boolean wf_getbilltos (string as_bill_to, ref string as_billto_string);Long									ll_temp
DataWindowChild					ldwc_child

u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_CustomerDataAccess", iu_customerDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_customerDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_bill_to)

iu_CustomerDataAccess.uf_Retrievebilltos(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_billto_string)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_applied_rebate_select.GetChild('billto_customer_id', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(as_billto_string)
	
ldwc_child.setsort("bill_to_id")
ldwc_child.sort()

Return True
end function

public function boolean wf_getdivisions (string as_division, ref string as_divisionstring);Long									ll_temp

String								ls_temp

Datawindowchild					ldwc_child
u_AbstractParameterStack		lu_ParameterStack	

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_DivisionDataAccess", iu_DivisionDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_DivisionDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_division)

iu_DivisionDataAccess.uf_RetrieveDivisions(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_DivisionString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_applied_rebate_select.GetChild('division_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(as_divisionstring)

ldwc_child.InsertRow(1)
ldwc_child.SetItem(1, "type_code", 'ALL')
ldwc_child.SetItem(1, "type_desc", 'ALL DIVISIONS')

Return True

end function

on w_rebate_date_inq.create
int iCurrent
call super::create
this.dw_applied_rebate_select=create dw_applied_rebate_select
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_applied_rebate_select
end on

on w_rebate_date_inq.destroy
call super::destroy
destroy(this.dw_applied_rebate_select)
end on

event close;call super::close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_date_type)
iu_ParameterStack.uf_Push('date', idt_from_date)
iu_ParameterStack.uf_Push('date', idt_to_date)
iu_ParameterStack.uf_Push('string', is_customer_type)
iu_ParameterStack.uf_Push('string', is_customer_id)
iu_ParameterStack.uf_Push('string', is_sales_division)
iu_ParameterStack.uf_Push('string', is_unapplied_rebate_ind)
iu_ParameterStack.uf_Push('string', is_inq_string)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)

end event

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)	
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_parameterstack.uf_Pop('string', is_inq_string)
iu_parameterstack.uf_Pop('string', is_unapplied_rebate_ind)
iu_parameterstack.uf_Pop('string', is_sales_division)
iu_parameterstack.uf_Pop('string', is_customer_id)
iu_parameterstack.uf_Pop('string', is_customer_type)
iu_parameterstack.uf_Pop('date', idt_to_date)
iu_parameterstack.uf_Pop('date', idt_from_date)
iu_parameterstack.uf_Pop('string', is_date_type)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

this.setredraw(true)


end event

event ue_postopen;call super::ue_postopen;integer				li_tabnum

string				ls_string

this.setredraw(false)

if is_inq_string = "Cancel" then
	is_inq_string = ""
end if

if is_date_type > ' ' Then
	dw_applied_rebate_select.SetItem(1,"date_type", is_date_type)
	dw_applied_rebate_select.SetItem(1,"from_date", idt_from_date)
	dw_applied_rebate_select.SetItem(1,"to_date", idt_to_date)
end if

if is_customer_type > ' ' Then 
	dw_applied_rebate_select.SetItem(1,"customer_type", is_customer_type)

	li_TabNum = integer(dw_applied_rebate_select.object.customer_type.tabsequence)

	CHOOSE CASE is_customer_type
		CASE 'A'
			dw_applied_rebate_select.SetItem(1,"corp_customer_id", is_customer_id)
			dw_applied_rebate_select.Object.corp_customer_id.Visible = 0
			dw_applied_rebate_select.Object.billto_customer_id.Visible = 0
			dw_applied_rebate_select.Object.shipto_customer_id.Visible = 0
		CASE 'C'
			dw_applied_rebate_select.SetItem(1,"corp_customer_id", is_customer_id)
			dw_applied_rebate_select.object.corp_customer_id.tabsequence = (li_TabNum + 1)
		CASE 'B'
			dw_applied_rebate_select.SetItem(1,"billto_customer_id", is_customer_id)
			dw_applied_rebate_select.object.billto_customer_id.tabsequence = (li_TabNum + 1)
		CASE 'S'
			dw_applied_rebate_select.SetItem(1,"shipto_customer_id", is_customer_id)
			dw_applied_rebate_select.object.shipto_customer_id.tabsequence = (li_TabNum + 1)
	END CHOOSE
else
	dw_applied_rebate_select.Object.corp_customer_id.Visible = 0
	dw_applied_rebate_select.Object.billto_customer_id.Visible = 0
	dw_applied_rebate_select.Object.shipto_customer_id.Visible = 0
end if

dw_applied_rebate_select.SetItem(1,"division_code", is_sales_division)

If is_unapplied_rebate_ind > ' ' Then
	dw_applied_rebate_select.SetItem(1,"unappl_reb_ind", is_unapplied_rebate_ind)
End if

If Not wf_GetCorps('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If

If Not wf_Getbilltos('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If

If Not wf_GetShiptos('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If

If Not wf_GetDivisions('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If

This.SetRedraw(True)
							                       

end event

type cb_help from w_abstractresponse`cb_help within w_rebate_date_inq
boolean visible = false
integer taborder = 0
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_rebate_date_inq
integer x = 1687
integer y = 392
integer taborder = 30
boolean cancel = true
end type

event cb_cancel::clicked;call super::clicked;is_inq_string = 'Cancel'

Close(Parent)
end event

type cb_ok from w_abstractresponse`cb_ok within w_rebate_date_inq
integer x = 1687
integer y = 228
integer width = 279
integer taborder = 20
boolean default = true
end type

event cb_ok::clicked;call super::clicked;date							ldt_from_date, &
								ldt_to_date

If dw_applied_rebate_select.AcceptText() = -1 Then Return

idt_from_date = dw_applied_rebate_select.GetItemDate(1, "from_date")
idt_to_date = dw_applied_rebate_select.GetItemDate(1, "to_date")

If (idt_from_date > idt_to_date) Then
	messagebox("Date Range Error","From Date is after To Date.",StopSign!,OK!)
	dw_applied_rebate_select.setfocus()
  	dw_applied_rebate_select.SetColumn('from_date')
	is_inq_string = "Cancel"
	return
End if

is_date_type = dw_applied_rebate_select.GetItemString(1, "date_type")
is_customer_type = dw_applied_rebate_select.GetItemString(1, "customer_type")

CHOOSE CASE is_customer_type
	CASE 'A'
		is_customer_id = ' '
	CASE 'C'
		is_customer_id = dw_applied_rebate_select.GetItemString(1, "corp_customer_id")
	CASE 'B'
		is_customer_id = dw_applied_rebate_select.GetItemString(1, "billto_customer_id")
	CASE 'S'
		is_customer_id = dw_applied_rebate_select.GetItemString(1, "shipto_customer_id")
END CHOOSE

is_sales_division = dw_applied_rebate_select.GetItemString(1, "division_code")
is_unapplied_rebate_ind = dw_applied_rebate_select.GetItemString(1, "unappl_reb_ind")

Close(Parent)
end event

type dw_applied_rebate_select from datawindow within w_rebate_date_inq
integer x = 69
integer y = 48
integer width = 1618
integer height = 1084
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_applied_rebate_select"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;this.insertrow(0)
This.SetItem(1,"from_date", Today())
This.SetItem(1,"to_date", RelativeDate(Today(), 1))
end event

event itemchanged;long							ll_count

string						ls_action_ind, &
								ls_errorstring
								
integer						li_tabnum

			
u_String_Functions		lu_String

li_TabNum = integer(this.object.customer_type.tabsequence)

Choose Case dwo.name
Case 'customer_type'
	CHOOSE CASE data
		CASE 'A'
			This.Setitem(1, 'corp_customer_id', "")
			This.Object.corp_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 0
			This.Object.shipto_customer_id.Visible = 0
		CASE 'C'
			This.Setitem(1, 'corp_customer_id', "")
			This.Object.corp_customer_id.Visible = 1
			This.Object.billto_customer_id.Visible = 0
			This.Object.shipto_customer_id.Visible = 0
			this.object.corp_customer_id.tabsequence = (li_TabNum + 1)
		CASE 'B'
			This.Setitem(1, 'billto_customer_id', "")
			This.Object.corp_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 1
			This.Object.shipto_customer_id.Visible = 0
			this.object.billto_customer_id.tabsequence = (li_TabNum + 1)
		CASE 'S'
			This.Setitem(1, 'shipto_customer_id', "")
			This.Object.corp_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 0
			This.Object.shipto_customer_id.Visible = 1
			this.object.shipto_customer_id.tabsequence = (li_TabNum + 1)
		END CHOOSE
End Choose


//Close(Parent)
end event

event itemerror;return -1
end event

event itemfocuschanged;//This.SelectText(1, 100)
end event

