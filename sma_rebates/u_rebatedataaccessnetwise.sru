HA$PBExportHeader$u_rebatedataaccessnetwise.sru
forward
global type u_rebatedataaccessnetwise from u_rebatedataaccess
end type
end forward

global type u_rebatedataaccessnetwise from u_rebatedataaccess
end type
global u_rebatedataaccessnetwise u_rebatedataaccessnetwise

forward prototypes
public function boolean uf_retrievelist (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_rebate_inquire (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_productgroupcheck (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrievelist (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrievelist()

<DESC> Retrieve the Rebate List	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Rebate List
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_RebateListString
								
u_sma001							lu_sma001

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_RebateListString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateListString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateListString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateListString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateListString)
		Return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateListString)
		Return False
	End If
End if

//lu_sma001.uf_smas05ar_GetRebateList(ls_appname, &
//													ls_windowname, &
//													"ufrtrvlst", &
//													"", &
//													ls_userid, &
//													ls_RebateListstring, &
//													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_RebateListString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_RebateListString)

return True

end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_Update()

<DESC> Update the Rebate Main Window Data
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will Update the Rebate Main Windwo
</USAGE> Data from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_AppName, &
									ls_WindowName, &
									ls_UserID, &
									ls_Password, &
									ls_HeaderString, &
									ls_DetailString, &
									ls_GroupID
								
u_sma001							lu_sma001

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_DetailString)
au_ParameterStack.uf_Pop('string', ls_HeaderString)
au_ParameterStack.uf_Pop('string', ls_WindowName)
au_ParameterStack.uf_Pop('string', ls_AppName)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_AppName)
		au_ParameterStack.uf_Push('string', ls_WindowName)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_AppName)
		au_ParameterStack.uf_Push('string', ls_WindowName)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_AppName)
		au_ParameterStack.uf_Push('string', ls_WindowName)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_AppName)
		au_ParameterStack.uf_Push('string', ls_WindowName)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_AppName)
		au_ParameterStack.uf_Push('string', ls_WindowName)
		au_ParameterStack.uf_Push('string', ls_HeaderString)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

//lu_sma001.uf_smas03ar_updateRebateData("", &
//													ls_WindowName, &
//													"uf_update", &
//													ls_UserId, &
//													ls_HeaderString, &
//													ls_DetailString, &
//													lu_ErrorContext)

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_AppName)
au_ParameterStack.uf_Push('string', ls_WindowName)
au_ParameterStack.uf_Push('string', ls_HeaderString)
au_ParameterStack.uf_Push('string', ls_DetailString)

If Not lu_ErrorContext.uf_IsSuccessful() Then return False

return True

end function

public function boolean uf_rebate_inquire (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_rebate_inquire()

<DESC> Retrieve the Rebate Data for the Rebate inquire window	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Rebate inquire data
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_RebateinquireString 

				
u_sma001							lu_sma001

u_AbstractdataAccess		lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_RebateinquireString)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateinquireString)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateinquireString)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateinquireString)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateinquireString)
		Return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateinquireString)
		Return False
	End If
End if

//lu_sma001.uf_smas07ar_GetRebateData(ls_appname, &
//													ls_windowname, &
//													"ufrtrvdat", &
//													"", &
//													ls_userid, &
//													ls_RebateinquireString, &
//													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_RebateinquireString)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_RebateinquireString)

return true
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_RebateHeaderString, &
									ls_RebateDetailString, &
									ls_RebateCustomerString, &
									ls_RebateDivisionString, &
									ls_RebateProductString, &
									ls_RebateProductGroupString, &
									ls_RebateCode, &
									ls_RebateRateSeq
				
u_sma001							lu_sma001

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_RebateCode)
au_ParameterStack.uf_Pop('string', ls_RebateHeaderString)
au_ParameterStack.uf_Pop('string', ls_RebateDetailString)
au_ParameterStack.uf_Pop('string', ls_RebateCustomerString)
au_ParameterStack.uf_Pop('string', ls_RebateDivisionString)
au_ParameterStack.uf_Pop('string', ls_RebateProductGroupString)
au_ParameterStack.uf_Pop('string', ls_RebateProductString)
au_ParameterStack.uf_Pop('string', ls_RebateRateSeq)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
		au_ParameterStack.uf_Push('string', ls_RebateProductString)
		au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
		au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
		au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
		au_ParameterStack.uf_Push('string', ls_RebateDetailString)
		au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
		au_ParameterStack.uf_Push('string', ls_RebateCode)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
		au_ParameterStack.uf_Push('string', ls_RebateProductString)
		au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
		au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
		au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
		au_ParameterStack.uf_Push('string', ls_RebateDetailString)
		au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
		au_ParameterStack.uf_Push('string', ls_RebateCode)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
		au_ParameterStack.uf_Push('string', ls_RebateProductString)
		au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
		au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
		au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
		au_ParameterStack.uf_Push('string', ls_RebateDetailString)
		au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
		au_ParameterStack.uf_Push('string', ls_RebateCode)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
		au_ParameterStack.uf_Push('string', ls_RebateProductString)
		au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
		au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
		au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
		au_ParameterStack.uf_Push('string', ls_RebateDetailString)
		au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
		au_ParameterStack.uf_Push('string', ls_RebateCode)
		Return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
		au_ParameterStack.uf_Push('string', ls_RebateProductString)
		au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
		au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
		au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
		au_ParameterStack.uf_Push('string', ls_RebateDetailString)
		au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
		au_ParameterStack.uf_Push('string', ls_RebateCode)
		Return False
	End If
End if

ls_RebateHeaderString = ls_RebateCode

//lu_sma001.uf_smas04ar_GetRebateData(ls_appname, &
//													ls_windowname, &
//													"ufrtrvdat", &
//													"", &
//													ls_userid, &
//													ls_RebateHeaderString, &
//													ls_RebateDetailString, &
//													ls_RebateCustomerString, &
//													ls_RebateDivisionString, &
//													ls_RebateProductString, &
//													ls_RebateProductGroupString, &
//													ls_RebateRateSeq, &
//													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
	au_ParameterStack.uf_Push('string', ls_RebateProductString)
	au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
	au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
	au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
	au_ParameterStack.uf_Push('string', ls_RebateDetailString)
	au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
	au_ParameterStack.uf_Push('string', ls_RebateCode)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_RebateRateSeq)
au_ParameterStack.uf_Push('string', ls_RebateProductString)
au_ParameterStack.uf_Push('string', ls_RebateProductGroupString)
au_ParameterStack.uf_Push('string', ls_RebateDivisionString)
au_ParameterStack.uf_Push('string', ls_RebateCustomerString)
au_ParameterStack.uf_Push('string', ls_RebateDetailString)
au_ParameterStack.uf_Push('string', ls_RebateHeaderString)
au_ParameterStack.uf_Push('string', ls_RebateCode)

Return True
end function

public function boolean uf_productgroupcheck (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString 
				
u_sma001							lu_sma001

u_AbstractdataAccess			lu_User
u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquirestring)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_sma001", lu_sma001, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
	If Not lu_sma001.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
End if

//lu_sma001.uf_smas24ar_productgroupcheck (ls_appname, &
//													ls_windowname, &
//													"ufrtrvdat", &
//													"", &
//													ls_userid, &
//													ls_inquirestring, &
//													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_inquirestring)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_inquirestring)

return true
end function

on u_rebatedataaccessnetwise.create
call super::create
end on

on u_rebatedataaccessnetwise.destroy
call super::destroy
end on

