HA$PBExportHeader$u_tutltypeslocaldb.sru
forward
global type u_tutltypeslocaldb from u_abstracttutltypedataaccess
end type
end forward

global type u_tutltypeslocaldb from u_abstracttutltypedataaccess
end type
global u_tutltypeslocaldb u_tutltypeslocaldb

type variables
u_AbstractTransaction	iu_ODBCTransaction


end variables

forward prototypes
public function boolean uf_retrieve (ref u_AbstractParameterStack au_ParameterStack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieve (ref u_AbstractParameterStack au_ParameterStack);Long								ll_Rtn
DataStore						lds_TutlTypes

String							ls_RecordType, &
									ls_TutlTypesString

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_RecordType)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

lds_TutlTypes = Create DataStore
lds_TutlTypes.DataObject = 'd_TutlType'
lds_TutlTypes.SetTransObject(iu_ODBCTransaction)

lds_TutlTypes.Retrieve(ls_RecordType)
ls_TutlTypesString = lds_TutlTypes.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_TutlTypesString)

return true

end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);u_AbstractClassFactory					lu_ClassFactory

u_AbstractErrorContext				 	lu_ErrorContext

u_AbstractDefaultManager				lu_DefaultManager


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject( "u_ODBCTransaction", iu_ODBCTransaction, lu_ErrorContext ) Then
	If Not lu_ClassFactory.uf_GetObject( "u_iniDefaultManager", lu_DefaultManager, lu_ErrorContext ) Then
		If lu_ErrorContext.uf_IsSuccessful( ) Then
			lu_DefaultManager.uf_Initialize( lu_ClassFactory, 'ibp002.ini', 'SMA DATABASE', lu_ErrorContext )
		Else
			au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
			au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
			Return False
		End If
	End If

	If lu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ODBCTransaction.uf_Initialize( lu_ClassFactory, lu_DefaultManager, lu_ErrorContext )
	Else
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		Return False
	End If
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)

Return True


end function

on u_tutltypeslocaldb.create
call super::create
end on

on u_tutltypeslocaldb.destroy
call super::destroy
end on

