HA$PBExportHeader$u_fwhdataaccess.sru
forward
global type u_fwhdataaccess from nonvisualobject
end type
end forward

global type u_fwhdataaccess from nonvisualobject
end type
global u_fwhdataaccess u_fwhdataaccess

forward prototypes
public function boolean uf_fwh_str_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_str_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_str_divg_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_str_divg_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_div_adj_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_div_adj_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_sku_adj_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_sku_adj_upd (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_div_list (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_loc_list (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_assign_maint_inq (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_fwh_assign_maint_upd (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_fwh_str_inq (ref u_abstractparameterstack au_parameterstack);return true

end function

public function boolean uf_fwh_str_upd (ref u_abstractparameterstack au_parameterstack);return true

end function

public function boolean uf_fwh_str_divg_inq (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_str_divg_upd (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_div_adj_inq (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_div_adj_upd (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_sku_adj_inq (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_sku_adj_upd (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_div_list (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_loc_list (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_assign_maint_inq (ref u_abstractparameterstack au_parameterstack);return true
end function

public function boolean uf_fwh_assign_maint_upd (ref u_abstractparameterstack au_parameterstack);return true
end function

on u_fwhdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_fwhdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

