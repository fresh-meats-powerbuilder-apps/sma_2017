HA$PBExportHeader$w_fwh_sales_trans_freight_inq.srw
forward
global type w_fwh_sales_trans_freight_inq from w_abstractresponseext
end type
type dw_fwh from datawindow within w_fwh_sales_trans_freight_inq
end type
end forward

global type w_fwh_sales_trans_freight_inq from w_abstractresponseext
int Width=905
int Height=368
dw_fwh dw_fwh
end type
global w_fwh_sales_trans_freight_inq w_fwh_sales_trans_freight_inq

type variables
u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractClassFactory		iu_ClassFactory
u_NotificationController		iu_Notification	
w_smaframe			iw_frame	
String				is_title, &
				is_division_code
end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);DataWindowChild			ldwc_header, &
								ldwc_display

iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

iu_ErrorContext.uf_Initialize()

Return True

end function

on w_fwh_sales_trans_freight_inq.create
int iCurrent
call super::create
this.dw_fwh=create dw_fwh
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fwh
end on

on w_fwh_sales_trans_freight_inq.destroy
call super::destroy
destroy(this.dw_fwh)
end on

event open;call super::open;u_AbstractdataAccess			lu_User
string							ls_password, &
									ls_userid, &
									ls_div_code, &
									ls_div_desc, &
									ls_div_string = "", &
									ls_inputstring
DataWindowChild 				ldwc_child
transaction 					DBTrans
long								rtncode

iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_division_code)
iu_ParameterStack.uf_Pop('string', ls_inputstring)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

//If Not iu_ClassFactory.uf_GetObject("u_user", lu_user, iu_ErrorContext) Then
//	If Not iu_ErrorContext.uf_IsSuccessful() Then return 
//	If Not lu_User.uf_Initialize(iu_ClassFactory, iu_ErrorContext) Then
//		iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
//		iu_ParameterStack.uf_Push('string', is_division_code)
//		iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
//		iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
//		Return 
//	End If
//End if
//lu_User.uf_retrieve(iu_ParameterStack, iu_ErrorContext)	
//iu_ParameterStack.uf_Pop("string", ls_password)
//iu_ParameterStack.uf_Pop("string", ls_userid)

//DBTrans = CREATE transaction
//DBTrans.DBMS = 'ODBC'		
//DBTrans.dbParm =  "ConnectString='DSN=MVSDB200;UID=" + ls_userid +";PWD=" + ls_password + "'"
//
////"',ConnectOption='SQL_DRIVER_CONNECT,SQL_DRIVER_NOPROMPT;SQL_OPT_TRACE,SQL_OPT_TRACE_ON;SQL_OPT_TRACEFILE,C:\PB6\SQL.LOG;SQL_PRESERVE_CURSORS,SQL_PC_ON'"
//CONNECT USING DBTrans;
//
////declare div_csr cursor for 
//SELECT A.SALES_DIVISION                     
//      ,B.DESCRIPTION 
//	into :ls_div_code
//     ,:ls_div_desc
//  FROM dkmvsdb201.ibdk.TUTLDIVG A                           
//      ,dkmvsdb201.ibdk.TUTLSDIV B  
//
// WHERE A.DIVISION_GROUP LIKE 'FWH%'         
//   AND A.SALES_DIVISION = '11'; 
//
//
////	open div_csr;
//ls_div_string = ""	
//
////do while DBTrans.SQLCode = 0
////	fetch div_csr into
////		:ls_div_code
////     ,:ls_div_desc;
//	  
//	 ls_div_string += ls_div_code + "~t" + ls_div_desc + "~r~n"
//	  
////loop
//
////close div_csr;

//DECLARE div_proc PROCEDURE FOR SMA300SP using DBTrans;
//EXECUTE div_proc;
//
//do while DBTrans.SQLCode = 0
//	fetch div_proc into
//		:ls_div_code
//     ,:ls_div_desc;
//	  
//	 ls_div_string += ls_div_code + "~t" + ls_div_desc + "~r~n"
//	  
//loop


//IF DBTrans.SQLCode <> 0 THEN
//	MessageBox("Connect Failed", &
//	"Cannot connect to database." + DBTrans.SQLErrText)
//else
//	MessageBox("Connect Worked", &
//	"connected to database." + ls_div_string)
//END IF


rtncode = dw_fwh.GetChild('divisioncode', ldwc_child)
IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")
//ls_inputstring = "01" + "~t" + "CARCASS" + "~r~n" + &
//						"05" + "~t" + "VARIETY MEATS" + "~r~n" + &
//						"07" + "~t" + "TALLOW REFINERY" + "~r~n" + &
//						"11" + "~t" + "PROCESSING" + "~r~n" + &
//						"15" + "~t" + "COW PROCESSING / VM" + "~r~n" + &
//						"16" + "~t" + "VARIETY MEATS - COW" + "~r~n" + &
//						"18" + "~t" + "CARCASS - COW" + "~r~n" + &
//						"22" + "~t" + "DESIGN PRODUCTS" + "~r~n" + &
//						"23" + "~t" + "CONSUMER PRODUCTS" + "~r~n" + &
//						"31" + "~t" + "PORK PROCESSING" + "~r~n" + &
//						"32" + "~t" + "PORK VARIETY MEATS" + "~r~n" + &
//						"34" + "~t" + "MORELLI PORK PRODUCTS" + "~r~n" + &
//						"35" + "~t" + "SUPREME PROCESSED FOODS" + "~r~n" + &
//						"57" + "~t" + "CASE READY/PUMPED PRODUCTS" + "~r~n"
						

ldwc_child.importstring(ls_inputstring)
dw_fwh.setitem(1,1,is_division_code)

This.Title = is_title + ' Inquire'

this.setredraw(true)


end event

event close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_division_code)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

type cb_help from w_abstractresponseext`cb_help within w_fwh_sales_trans_freight_inq
int X=608
int Y=160
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_fwh_sales_trans_freight_inq
int X=306
int Y=160
boolean Cancel=true
end type

event cb_cancel::clicked;is_division_code = "Cancel"
Close(Parent) 
end event

type cb_ok from w_abstractresponseext`cb_ok within w_fwh_sales_trans_freight_inq
int X=37
int Y=160
boolean Default=true
end type

event cb_ok::clicked;u_String_Functions		lu_StringFunctions

parent.dw_fwh.accepttext()
if lu_StringFunctions.nf_IsEmpty(parent.dw_fwh.getitemstring(1, 1)) then
	messagebox("Required Field","Division is a required field.",StopSign!,OK!)
	parent.dw_fwh.setfocus()
	parent.dw_fwh.SelectText ( 1, 100 )
	return
end if

is_division_code = parent.dw_fwh.getitemstring(1, 1)

Close(Parent)
end event

type dw_fwh from datawindow within w_fwh_sales_trans_freight_inq
int X=59
int Y=36
int Width=539
int Height=76
int TabOrder=10
boolean BringToTop=true
string DataObject="d_fwh_sales_trans_freight_inq"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

