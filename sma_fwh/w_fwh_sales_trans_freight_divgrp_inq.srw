HA$PBExportHeader$w_fwh_sales_trans_freight_divgrp_inq.srw
forward
global type w_fwh_sales_trans_freight_divgrp_inq from w_abstractresponseext
end type
type dw_fwh from datawindow within w_fwh_sales_trans_freight_divgrp_inq
end type
end forward

global type w_fwh_sales_trans_freight_divgrp_inq from w_abstractresponseext
int Width=905
int Height=368
dw_fwh dw_fwh
end type
global w_fwh_sales_trans_freight_divgrp_inq w_fwh_sales_trans_freight_divgrp_inq

type variables
u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractClassFactory		iu_ClassFactory
u_NotificationController		iu_Notification	
w_smaframe			iw_frame	
String				is_title, &
				is_division_group
end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);DataWindowChild			ldwc_header, &
								ldwc_display

iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

iu_ErrorContext.uf_Initialize()

Return True

end function

on w_fwh_sales_trans_freight_divgrp_inq.create
int iCurrent
call super::create
this.dw_fwh=create dw_fwh
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fwh
end on

on w_fwh_sales_trans_freight_divgrp_inq.destroy
call super::destroy
destroy(this.dw_fwh)
end on

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm



If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_division_group)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

dw_fwh.setitem(1,1,is_division_group)

This.Title = is_title + ' Inquire'

this.setredraw(true)


end event

event close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_division_group)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

type cb_help from w_abstractresponseext`cb_help within w_fwh_sales_trans_freight_divgrp_inq
int X=608
int Y=160
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_fwh_sales_trans_freight_divgrp_inq
int X=306
int Y=160
boolean Cancel=true
end type

event cb_cancel::clicked;is_division_group = "Cancel"
Close(Parent) 
end event

type cb_ok from w_abstractresponseext`cb_ok within w_fwh_sales_trans_freight_divgrp_inq
int X=37
int Y=160
boolean Default=true
end type

event cb_ok::clicked;u_String_Functions		lu_StringFunctions

parent.dw_fwh.accepttext()
if lu_StringFunctions.nf_IsEmpty(parent.dw_fwh.getitemstring(1, 1)) then
	messagebox("Required Field","Division is a required field.",StopSign!,OK!)
	parent.dw_fwh.setfocus()
	parent.dw_fwh.SelectText ( 1, 100 )
	return
end if

is_division_group = parent.dw_fwh.getitemstring(1, 1)

Close(Parent)
end event

type dw_fwh from datawindow within w_fwh_sales_trans_freight_divgrp_inq
int X=59
int Y=36
int Width=786
int Height=76
int TabOrder=10
boolean BringToTop=true
string DataObject="d_fwh_sales_trans_freight_divgrp_inq"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;this.insertrow(0)
end event

