HA$PBExportHeader$w_fwh_assign_maint.srw
forward
global type w_fwh_assign_maint from w_abstractsheetext
end type
type dw_fwh_assign_maint from datawindow within w_fwh_assign_maint
end type
type dw_fwh_assign_maint_inq from datawindow within w_fwh_assign_maint
end type
end forward

global type w_fwh_assign_maint from w_abstractsheetext
integer x = 174
integer y = 80
integer width = 1211
string title = "Forward Warehouse Assignment Maintenance"
dw_fwh_assign_maint dw_fwh_assign_maint
dw_fwh_assign_maint_inq dw_fwh_assign_maint_inq
end type
global w_fwh_assign_maint w_fwh_assign_maint

type variables
u_AbstractClassFactory		iu_ClassFactory
u_ErrorContext			iu_ErrorContext
u_NotificationController		iu_Notification	
w_smaframe			iw_frame
w_fwh_assign_maint		iw_parent
u_fwhDataAccess	iu_fwhDataAccess
u_statesdataaccesslocaldb		iu_statesdataaccess
string				is_loc_list, &
				is_fwh,&
				is_zip, &
				is_state, &
				is_state_list, &
				is_title, &
				is_filter, &
				is_sorted_column, &
				is_sorted_way, &
				is_updatestring
boolean				ib_filtered = false

end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public subroutine wf_sort (string as_name)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not iu_ClassFactory.uf_GetObject( "u_FwhDataAccess", iu_fwhDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

This.Event Post ue_inquire()

Return True

end function

public subroutine wf_sort (string as_name);string		ls_sort

if as_name = is_sorted_column then
	choose case is_sorted_way
		case "A"
			is_sorted_way = "D"
		case "D"
			is_sorted_way = "A"
		case else
			is_sorted_way = "A"
	end choose
else
	is_sorted_way = "A"
end if

is_sorted_column = as_name

ls_sort = as_name + " " + is_sorted_way

dw_fwh_assign_maint.setsort(ls_sort)
dw_fwh_assign_maint.sort()
end subroutine

on w_fwh_assign_maint.create
int iCurrent
call super::create
this.dw_fwh_assign_maint=create dw_fwh_assign_maint
this.dw_fwh_assign_maint_inq=create dw_fwh_assign_maint_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fwh_assign_maint
this.Control[iCurrent+2]=this.dw_fwh_assign_maint_inq
end on

on w_fwh_assign_maint.destroy
call super::destroy
destroy(this.dw_fwh_assign_maint)
destroy(this.dw_fwh_assign_maint_inq)
end on

event ue_inquire;string					ls_appName, ls_windowName, ls_inquirestring, &
							ls_Statestring
							
long						ll_row, ll_deleted_rows, ll_rtn, rtncode, ll_count

DataWindowChild		ldwc_child

u_string_functions	lu_string

u_ParameterStack		lu_ParameterStack




ll_row = 0
ll_row = dw_fwh_assign_maint.GetNextModified(ll_row, Primary!)
ll_deleted_rows = dw_fwh_assign_maint.deletedcount()

This.Event Trigger CloseQuery()
If Message.ReturnValue <> 0 Then Return False

ls_AppName = GetApplication().AppName
ls_WindowName = 'FWHassignMaintupd'
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

if len(is_loc_list) = 0 then
	lu_ParameterStack.uf_initialize()
	iu_ErrorContext.uf_Initialize()	
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', ls_appname)
	lu_ParameterStack.uf_Push('string', ls_windowname)
	lu_ParameterStack.uf_Push('string', is_loc_list)
	iu_fwhDataAccess.uf_fwh_loc_list(lu_ParameterStack)
	lu_ParameterStack.uf_Pop('string', is_loc_list)
	lu_ParameterStack.uf_Pop('string', ls_windowname)
	lu_ParameterStack.uf_Pop('string', ls_appname)
	lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		This.SetRedraw(True)
		Return False
	End If

	rtncode = dw_fwh_assign_maint.GetChild('fwh', ldwc_child)
	IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")					
	ldwc_child.importstring(is_loc_list)

end if

//   new code for states

if len(is_state_list) = 0 then
	If Not iu_ClassFactory.uf_GetObject( "u_StatesDataAccesslocaldb", iu_StatesDataAccess, iu_ErrorContext ) Then
		If iu_ErrorContext.uf_IsSuccessful( ) Then
			lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
			lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
			iu_StatesDataAccess.uf_Initialize( lu_ParameterStack )
			lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
			lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
			If Not iu_ErrorContext.uf_IsSuccessful() Then
				iu_Notification.uf_Display(iu_ErrorContext)
				Return False
			End If
		Else 
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	End If
	
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', 'AA')
	
	iu_StatesDataAccess.uf_RetrieveStates(lu_ParameterStack)
	
	lu_ParameterStack.uf_Pop('string', is_state_list)
	lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
	
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
	
	rtncode = dw_fwh_assign_maint_inq.GetChild('state', ldwc_child)
	IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")					
	ldwc_child.importstring(is_state_list)
	
	rtncode = dw_fwh_assign_maint.GetChild('state', ldwc_child)
	IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")					
	ldwc_child.importstring(is_state_list)
end if	

//

iu_ErrorContext.uf_Initialize()
iw_frame.SetMicroHelp("Inquiring on the MainFrame. Please wait....")

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', is_loc_list)
lu_ParameterStack.uf_Push('string', is_state)
lu_ParameterStack.uf_Push('string', is_state_list)
lu_ParameterStack.uf_Push('string', is_zip)
lu_ParameterStack.uf_Push('string', is_fwh)
lu_ParameterStack.uf_Push('string', is_Title)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetResponseWindow( "w_fwh_assign_maint_inq", lu_ParameterStack )
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('string', is_fwh)
lu_ParameterStack.uf_Pop('string', is_zip)
lu_ParameterStack.uf_Pop('string', is_state_list)
lu_ParameterStack.uf_Pop('string', is_state)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

If is_fwh = 'Can' Then 
	ll_rtn = messagebox("Caution","Do you want to close this window?",StopSign!,YesNo!,2)
	if ll_rtn = 1 then
		this.triggerevent(Close!)
		return false
	else
		This.SetRedraw(True)
		is_fwh = '   '
		Return False
	end if
End If

dw_fwh_assign_maint.reset()

if lu_string.nf_isempty(is_fwh) then
	is_fwh = "   "
end if
if lu_string.nf_isempty(is_state) then
	is_state = "  "
end if
if lu_string.nf_isempty(is_zip) then
	is_zip = "   "
end if

dw_fwh_assign_maint_inq.setitem(1,"fwh",is_fwh)
dw_fwh_assign_maint_inq.setitem(1,"state",is_state)
dw_fwh_assign_maint_inq.setitem(1,"zip",is_zip)

ls_inquirestring = is_state + "~t" + is_zip + "~t" + is_fwh + "~r~n"

iu_ErrorContext.uf_Initialize()

lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_inquirestring)
iu_fwhDataAccess.uf_fwh_assign_maint_inq(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_InquireString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If
//// test
//ls_InquireString = 'DC' + '~t' + '123' + '~t' + '415' + '~t' + 'DC' + '~t' + '123' + '~t' + '415' +'~r~n' + &
//						 'DC' + '~t' + '124' + '~t' + '415' + '~t' + 'DC' + '~t' + '124' + '~t' + '415' +'~r~n' + &
//						 'DC' + '~t' + '125' + '~t' + '415' + '~t' + 'DC' + '~t' + '125' + '~t' + '415' +'~r~n' + &
//						 'DC' + '~t' + '126' + '~t' + '415' + '~t' + 'DC' + '~t' + '126' + '~t' + '415' +'~r~n'  

If Not lu_string.nf_IsEmpty(ls_InquireString) Then
	ll_row = dw_fwh_assign_maint.ImportString(ls_InquireString)
	If ll_row > 0 Then 
		iw_frame.SetMicroHelp(String(ll_row) + " Rows Retrieved")
		if ll_row > 2 then
			for ll_count = 1 to ll_row 
				dw_fwh_assign_maint.setitem(ll_count, "status_ind", "O")
			next
		end if
	end if
Else
	iw_frame.SetMicroHelp("0 Rows Retrieved")
End if

dw_fwh_assign_maint.SetColumn('fwh')
dw_fwh_assign_maint.InsertRow(0)
//dw_fwh_assign_maint.SetItem(dw_fwh_assign_maint.RowCount(),"state", '  ') 
This.SetRedraw(True)	
dw_fwh_assign_maint.ResetUpdate()
Return True
















end event

event open;call super::open;iw_parent = This
is_Title = This.Title
is_fwh = ""
is_state = ""
is_zip = ""
is_state_list = ""
end event

event close;Close(This)


end event

event closequery;long	ll_row, ll_rtn


dw_fwh_assign_maint.accepttext()
ll_row = 0
ll_row = dw_fwh_assign_maint.GetNextModified(ll_row, Primary!)

if ll_row > 0 then
	ll_rtn = messagebox("Warning","Do you want to save your changes?",StopSign!,YesNoCancel!)
	choose case ll_rtn
		case 1
			this.triggerevent("ue_save")
			return 0
		case 2
			return 0
		case else
			return 1
	end choose
end if



end event

event ue_save;String					ls_UpdateString, &
							ls_DetailString, &
							ls_AppName, &
							ls_WindowName, &
							ls_filter, ls_find
long						ll_row, ll_count
u_ParameterStack		lu_ParameterStack

u_String_Functions	lu_strings


iu_ErrorContext.uf_Initialize()
SetPointer(HourGlass!)
IF dw_fwh_assign_maint.AcceptText() = -1 THEN 
	dw_fwh_assign_maint.SetFocus()
	Return False
End IF
This.SetRedraw(False)

if ib_filtered then
	ls_filter = ""
	dw_fwh_assign_maint.setfilter(ls_filter)
	dw_fwh_assign_maint.filter()
end if

ls_UpdateString = ''


ll_row = 0
Do
	ll_row = dw_fwh_assign_maint.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		choose case dw_fwh_assign_maint.getitemstatus(ll_row, 0, Primary!)
			case DataModified!
				ls_UpdateString += dw_fwh_assign_maint.GetItemString(ll_row, 'state') + '~t' + &
					dw_fwh_assign_maint.GetItemString(ll_row, 'zip') + '~t' + &
					dw_fwh_assign_maint.GetItemstring(ll_row, 'fwh') + '~t' + &
					dw_fwh_assign_maint.GetItemString(ll_row, 'state_org') + '~t' + &
					dw_fwh_assign_maint.GetItemString(ll_row, 'zip_org') + '~t' + &
					dw_fwh_assign_maint.GetItemstring(ll_row, 'fwh_org') + '~t' + 'U' + '~r~n'
			case NewModified!
				if not(lu_strings.nf_IsEmpty(dw_fwh_assign_maint.GetItemString(ll_row, 'zip'))) then
					if not (lu_strings.nf_IsEmpty(dw_fwh_assign_maint.GetItemString(ll_row, 'fwh'))) then
						if not(lu_strings.nf_IsEmpty(dw_fwh_assign_maint.GetItemString(ll_row, 'state'))) then
								ls_UpdateString += dw_fwh_assign_maint.GetItemString(ll_row, 'state') + '~t' + &
									dw_fwh_assign_maint.GetItemString(ll_row, 'zip') + '~t' + &
									dw_fwh_assign_maint.GetItemstring(ll_row, 'fwh') + '~t' + &
									' ' + '~t' + &
									' ' + '~t' + &
									' ' + '~t' + 'A' + '~r~n'
						end if
					end if
				end if
		end choose
	End If
Loop While ll_row > 0

if isnull(is_UpdateString) then
	is_UpdateString = ls_UpdateString
else
	if isnull(ls_UpdateString) then
		is_UpdateString = is_UpdateString
	else
		is_UpdateString = is_UpdateString + ls_UpdateString
	end if
end if


If lu_strings.nf_IsEmpty(is_UpdateString) then
	SetMicroHelp('No update necessary')
	This.SetRedraw(True)
	Return False
end if

SetMicroHelp("Wait... Updating the Database")

SetPointer(HourGlass!)
	
ls_WindowName = 'FWH Div Mass Update'
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_AppName)
lu_ParameterStack.uf_Push('string', ls_WindowName)
lu_ParameterStack.uf_Push('string', is_UpdateString)

iu_fwhDataAccess.uf_fwh_assign_maint_upd( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('string', ls_AppName)
lu_ParameterStack.uf_Pop('string', ls_WindowName)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

is_UpdateString = ""

if ib_filtered then
	dw_fwh_assign_maint.setfilter(is_filter)
	dw_fwh_assign_maint.filter()
end if

This.SetRedraw(True)

dw_fwh_assign_maint.ResetUpdate()
SetMicroHelp("Update Successful")

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

return true
end event

event ue_delete;call super::ue_delete;GetFocus().TriggerEvent('ue_Delete')
end event

event ue_reset;call super::ue_reset;is_filter = ""

dw_fwh_assign_maint.setfilter(is_filter)
dw_fwh_assign_maint.filter()
ib_filtered = false
end event

event ue_filter;call super::ue_filter;ib_filtered = true
dw_fwh_assign_maint.setfilter(is_filter)
dw_fwh_assign_maint.filter()
end event

event resize;call super::resize;IF newheight > dw_fwh_assign_maint.Y THEN
	dw_fwh_assign_maint.Height = newheight - dw_fwh_assign_maint.y - 20
END IF

end event

type dw_fwh_assign_maint from datawindow within w_fwh_assign_maint
event ue_delete ( )
event ue_new ( )
integer x = 5
integer y = 180
integer width = 1147
integer height = 1280
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_fwh_assign_maint"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_delete;Long				ll_row


ll_row = This.GetRow()

If ll_row <= 0 Then Return

ll_row = 0

If MessageBox('Delete Row', 'Are you sure you want to delete these rows?', Exclamation!, YesNo!, 2) = 1 Then
	
	ll_row = dw_fwh_assign_maint.GetSelectedRow (ll_row)
	do while ll_row > 0
		if (dw_fwh_assign_maint.getitemstatus(ll_row, 0, Primary!) = NotModified!  or &
			dw_fwh_assign_maint.getitemstatus(ll_row, 0, Primary!) = DataModified!)  then
			is_UpdateString += dw_fwh_assign_maint.GetItemString(ll_row, 'state') + '~t' + &
				dw_fwh_assign_maint.GetItemString(ll_row, 'zip') + '~t' + &
				dw_fwh_assign_maint.GetItemstring(ll_row, 'fwh') +  '~t' + &
				dw_fwh_assign_maint.GetItemString(ll_row, 'state_org') + '~t' + &
				dw_fwh_assign_maint.GetItemString(ll_row, 'zip_org') + '~t' + &
				dw_fwh_assign_maint.GetItemstring(ll_row, 'fwh_org') + '~t' + 'D' + '~r~n'
		end if
		IF not(This.GetItemStatus(ll_row, 0, Primary!) = new!) Then
			This.DeleteRow(ll_row)
		else
			ll_row ++
		end if
		ll_row = dw_fwh_assign_maint.GetSelectedRow (ll_row - 1)
	loop 
	if this.rowcount() = 0 then
		this.insertrow(0)
	end if
	Return
End If

This.SelectRow(ll_row, False)
end event

event ue_new;This.InsertRow(0)
//dw_fwh_assign_maint.SetItem(dw_fwh_assign_maint.RowCount(),"state", '  ') 

end event

event itemfocuschanged;this.SelectText ( 1, 10 )
end event

event itemchanged;If row = This.RowCount() Then 
	This.Event ue_new()
End If
end event

event rbuttondown;m_fwh_popup		lm_popmenu
string			ls_column, ls_variable
date				ld_dates

ls_column = dwo.name
lm_popmenu = Create m_fwh_popup
lm_popmenu.m_fwh_options.m_massupdate.Enabled = false
lm_popmenu.m_fwh_options.m_massupdate.visible = false
lm_popmenu.m_fwh_options.m_filter.Enabled = false
lm_popmenu.m_fwh_options.m_filter.visible = false
lm_popmenu.m_fwh_options.m_reset.Enabled = false
lm_popmenu.m_fwh_options.m_reset.visible = false
choose case ls_column
	case "state", "zip", "fwh"
		ls_variable = dw_fwh_assign_maint.getitemstring(row, ls_column)
		if ib_filtered then
			is_filter += " AND " + ls_column + ' = "' + ls_variable + '"'
		else
			is_filter = ls_column + ' = "' + ls_variable + '"'
			ib_filtered = true
		end if
		
		if ib_filtered then
			lm_popmenu.m_fwh_options.m_reset.Enabled = True
			lm_popmenu.m_fwh_options.m_reset.visible = true
		else
			lm_popmenu.m_fwh_options.m_reset.Enabled = false
			lm_popmenu.m_fwh_options.m_reset.visible = false
		end if
		if this.rowcount() < 2 then
			lm_popmenu.m_fwh_options.m_filter.Enabled = false
			lm_popmenu.m_fwh_options.m_filter.visible = false
		else
			lm_popmenu.m_fwh_options.m_filter.Enabled = true
			lm_popmenu.m_fwh_options.m_filter.visible = true
		end if
	case else
		if ib_filtered then
			lm_popmenu.m_fwh_options.m_reset.Enabled = true
			lm_popmenu.m_fwh_options.m_reset.visible = true
		else
			lm_popmenu.m_fwh_options.m_reset.Enabled = false
			lm_popmenu.m_fwh_options.m_reset.visible = false
		end if
end choose
lm_popmenu.m_fwh_options.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
end event

event clicked;string ls_name

//if row = 0 then
//	ls_name = dwo.name
//	choose case dwo.name
//		case "state_t"
//			wf_sort ("state")
//		case "zip_t"
//			wf_sort ("zip")
//		case "fwh_t"
//			wf_sort ("fwh")
//		case else
//	end choose
//end if
if row > 0 then
	ls_name = dwo.name
	choose case dwo.name
		case "state"
			dw_fwh_assign_maint.selectrow(row, not(dw_fwh_assign_maint.IsSelected ( row )))
		case else
	end choose
end if
end event

type dw_fwh_assign_maint_inq from datawindow within w_fwh_assign_maint
integer x = 23
integer y = 16
integer width = 777
integer height = 164
integer taborder = 10
boolean bringtotop = true
boolean enabled = false
string dataobject = "d_fwh_assign_maint_inq"
boolean border = false
end type

event constructor;this.insertrow(0)
this.Object.state.Background.Color = 67108864
this.Object.zip.Background.Color = 67108864
this.Object.fwh.Background.Color = 67108864
end event

