HA$PBExportHeader$w_fwh_assign_maint_inq.srw
forward
global type w_fwh_assign_maint_inq from w_abstractresponseext
end type
type dw_fwh_assign_maint_inq from datawindow within w_fwh_assign_maint_inq
end type
end forward

global type w_fwh_assign_maint_inq from w_abstractresponseext
int Width=869
int Height=432
dw_fwh_assign_maint_inq dw_fwh_assign_maint_inq
end type
global w_fwh_assign_maint_inq w_fwh_assign_maint_inq

type variables
u_abstracterrorcontext		iu_errorcontext
u_CustomerDataAccess		iu_CustomerDataAccess
u_abstractparameterstack		iu_parameterstack
u_AbstractClassFactory		iu_ClassFactory
u_NotificationController		iu_Notification	
w_smaframe			iw_frame	
String				is_title, &
				is_state,&
				is_state_list, &
				is_fwh, &
				is_zip
end variables

on w_fwh_assign_maint_inq.create
int iCurrent
call super::create
this.dw_fwh_assign_maint_inq=create dw_fwh_assign_maint_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fwh_assign_maint_inq
end on

on w_fwh_assign_maint_inq.destroy
call super::destroy
destroy(this.dw_fwh_assign_maint_inq)
end on

event close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_state)
iu_ParameterStack.uf_Push('string', is_state_list)
iu_ParameterStack.uf_Push('string', is_zip)
iu_ParameterStack.uf_Push('string', is_fwh)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

event open;call super::open;string						ls_loc_string
DataWindowChild 			ldwc_child
long							rtncode

iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Pop('string', is_Title)
iu_ParameterStack.uf_Pop('string', is_fwh)
iu_ParameterStack.uf_Pop('string', is_zip)
iu_ParameterStack.uf_Pop('string', is_state_list)
iu_ParameterStack.uf_Pop('string', is_state)
iu_ParameterStack.uf_Pop('string', ls_loc_string)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

rtncode = dw_fwh_assign_maint_inq.GetChild('fwh', ldwc_child)
IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")					
ldwc_child.importstring(ls_loc_string)

rtncode = dw_fwh_assign_maint_inq.GetChild('state', ldwc_child)
IF rtncode = -1 THEN MessageBox( "Error", "Not a DataWindowChild")					
ldwc_child.importstring(is_state_list)

dw_fwh_assign_maint_inq.setitem(1,'fwh',is_fwh)

if len(is_state) > 0 then
	dw_fwh_assign_maint_inq.setitem(1,'state',is_state)
else
	dw_fwh_assign_maint_inq.setitem(1,'state','  ')
end if

dw_fwh_assign_maint_inq.setitem(1,'zip',is_zip)

This.Title = is_title + ' Inquire'

this.setredraw(true)
end event

type cb_help from w_abstractresponseext`cb_help within w_fwh_assign_maint_inq
int X=571
int Y=212
int TabOrder=40
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_fwh_assign_maint_inq
int X=302
int Y=208
int Width=251
int TabOrder=30
boolean Cancel=true
end type

event cb_cancel::clicked;call super::clicked;is_fwh = "Can"
Close(Parent) 
end event

type cb_ok from w_abstractresponseext`cb_ok within w_fwh_assign_maint_inq
int X=32
int Y=208
int TabOrder=20
boolean Default=true
end type

event cb_ok::clicked;call super::clicked;u_String_Functions		lu_StringFunctions

parent.dw_fwh_assign_maint_inq.accepttext()
//if lu_StringFunctions.nf_IsEmpty(parent.dw_fwh_assign_maint_inq.getitemstring(1, 'fwh')) and &
//	lu_StringFunctions.nf_IsEmpty(parent.dw_fwh_assign_maint_inq.getitemstring(1, 'state')) and &
//	lu_StringFunctions.nf_IsEmpty(parent.dw_fwh_assign_maint_inq.getitemstring(1, 'zip')) then
//	messagebox("Required Field","Forward Warehouse, Zip Code or State must be choosen.",StopSign!,OK!)
if lu_StringFunctions.nf_IsEmpty(parent.dw_fwh_assign_maint_inq.getitemstring(1, 'fwh')) and &
	lu_StringFunctions.nf_IsEmpty(parent.dw_fwh_assign_maint_inq.getitemstring(1, 'state'))then
	messagebox("Required Field","Forward Warehouse or State must be choosen.",StopSign!,OK!)
	parent.dw_fwh_assign_maint_inq.setfocus()
	parent.dw_fwh_assign_maint_inq.SelectText ( 1, 100 )
	return
end if

is_fwh = parent.dw_fwh_assign_maint_inq.getitemstring(1, 'fwh')
is_state = parent.dw_fwh_assign_maint_inq.getitemstring(1, 'state')
is_zip = parent.dw_fwh_assign_maint_inq.getitemstring(1, 'zip')

Close(Parent)
end event

type dw_fwh_assign_maint_inq from datawindow within w_fwh_assign_maint_inq
int X=41
int Y=28
int Width=763
int Height=152
int TabOrder=10
boolean BringToTop=true
string DataObject="d_fwh_assign_maint_inq"
boolean Border=false
end type

event constructor;this.insertrow(0)
end event

