HA$PBExportHeader$w_fwh_sku_mass_adjustment_upd.srw
forward
global type w_fwh_sku_mass_adjustment_upd from w_abstractsheetext
end type
type dw_fwh from datawindow within w_fwh_sku_mass_adjustment_upd
end type
end forward

global type w_fwh_sku_mass_adjustment_upd from w_abstractsheetext
integer x = 553
integer y = 304
integer width = 1225
integer height = 1276
string title = "SKU Mass Adjustment Update"
dw_fwh dw_fwh
end type
global w_fwh_sku_mass_adjustment_upd w_fwh_sku_mass_adjustment_upd

type variables
u_AbstractClassFactory		iu_ClassFactory
u_ErrorContext			iu_ErrorContext
u_NotificationController		iu_Notification	
w_smaframe			iw_frame
w_fwh_sku_mass_adjustment_upd	iw_parent
u_fwhDataAccess		iu_fwhDataAccess
string				is_ProductCode, &
				is_DivisionCode, &
				is_DestPlantCode, &
				is_title, &
				is_filter, &
				is_sorted_column, &
				is_sorted_way, &
				is_mass_ind = '0'
boolean				ib_filtered = false, &
				ib_mass_bol = false
long				il_mass_row
end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public subroutine wf_sort (string as_name)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not iu_ClassFactory.uf_GetObject( "u_FwhDataAccess", iu_fwhDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

This.Event Post ue_inquire()

Return True

end function

public subroutine wf_sort (string as_name);string		ls_sort

if as_name = is_sorted_column then
	choose case is_sorted_way
		case "A"
			is_sorted_way = "D"
		case "D"
			is_sorted_way = "A"
		case else
			is_sorted_way = "A"
	end choose
else
	is_sorted_way = "A"
end if

is_sorted_column = as_name

ls_sort = as_name + " " + is_sorted_way

dw_fwh.setsort(ls_sort)
dw_fwh.sort()

end subroutine

on w_fwh_sku_mass_adjustment_upd.create
int iCurrent
call super::create
this.dw_fwh=create dw_fwh
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fwh
end on

on w_fwh_sku_mass_adjustment_upd.destroy
call super::destroy
destroy(this.dw_fwh)
end on

event closequery;long	ll_row, ll_rtn


dw_fwh.accepttext()
ll_row = 0
ll_row = dw_fwh.GetNextModified(ll_row, Primary!)

if ll_row > 0 then
	ll_rtn = messagebox("Warning","Do you want to save your changes?",StopSign!,YesNoCancel!)
	choose case ll_rtn
		case 1
			this.triggerevent("ue_save")
			return 0
		case 2
			return 0
		case else
			return 1
	end choose
end if
end event

event open;call super::open;iw_parent = This
is_Title = This.Title
is_destplantcode = ""
is_divisioncode = ""
is_productcode = ""
end event

event ue_inquire;String 					ls_division_code, &
							ls_dest_plant_code, &
							ls_product_code, &
							ls_AppName, &
							ls_WindowName, &
							ls_inquireString
							
integer					li_return_code

long						ll_rec_count, ll_row, &
							ll_rtn

u_string_functions	lu_string

u_ParameterStack		lu_ParameterStack

ll_row = 0

ll_row = dw_fwh.GetNextModified(ll_row, Primary!)
if ll_row > 0 then
	ll_rtn = Messagebox("Unsaved Changes","Do you want to save changes?",StopSign!,YesNoCancel!, 3)
	choose case ll_rtn
		case 1
			this.triggerevent("ue_save")
		case 2
			//continue
		case 3
			return true
	end choose
end if


iu_ErrorContext.uf_Initialize()
iw_frame.SetMicroHelp("Inquiring on the MainFrame. Please wait....")
dw_fwh.reset()
iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
This.Event Trigger CloseQuery()
If Message.ReturnValue <> 0 Then Return False
//ls_division_code = is_divisioncode
//ls_dest_plant_code = is_destplantcode
//ls_product_code = is_productcode
//lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
//lu_ParameterStack.uf_Push('string', ls_product_code)
//lu_ParameterStack.uf_Push('string', ls_division_code)
//lu_ParameterStack.uf_Push('string', ls_dest_plant_code)
//lu_ParameterStack.uf_Push('string', is_Title)
//lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
//lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
//iu_ErrorContext.uf_Initialize()
//iu_ClassFactory.uf_GetResponseWindow( "w_fwh_sku_mass_adjustment_inq", lu_ParameterStack )
//lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
//lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
//lu_ParameterStack.uf_Pop('string', ls_dest_plant_code)
//lu_ParameterStack.uf_Pop('string', ls_division_code)
//lu_ParameterStack.uf_Pop('string', ls_product_code)
//lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
//If Not iu_ErrorContext.uf_IsSuccessful( ) Then
//	iu_Notification.uf_Display(iu_ErrorContext)
//	Return False
//End If

//If ls_division_code = 'Cancel' Then 
//	li_return_code = messagebox("Caution","Do you want to close this window?",StopSign!,YesNo!,2)
//	if li_return_code = 1 then
//		this.triggerevent(Close!)
//		return false
//	else
//		Return False
//	end if
//End If
//is_divisioncode = ls_division_code
//is_destplantcode = ls_dest_plant_code
//is_productcode = ls_product_code
ls_inquirestring = is_productcode + "~t" + is_divisioncode + "~t" + is_destplantcode +"~r~n"
ls_AppName = GetApplication().AppName
ls_WindowName = 'FWHskuMassAdjUpdate'
iu_ErrorContext.uf_Initialize()

lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_inquirestring)
iu_fwhDataAccess.uf_fwh_sku_adj_inq(lu_ParameterStack)
lu_ParameterStack.uf_Pop('string', ls_InquireString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)	

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If
//testing
//ls_InquireString = "product1" + "~t" + "01" + "~t" + "405" + "~t" + "0.05" + "~r~n" + &
//							"product2" + "~t" + "02" + "~t" + "406" + "~t" + "12345.67" + "~r~n" 

If Not lu_string.nf_IsEmpty(ls_InquireString) Then
	ll_rec_count = dw_fwh.ImportString(ls_InquireString)
	If ll_rec_count > 0 Then iw_frame.SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
	dw_fwh.ResetUpdate ( )
Else
	iw_frame.SetMicroHelp("0 Rows Retrieved")
End if
		
This.SetRedraw(True)	
Return True
end event

event ue_save;String					ls_DetailString, &
							ls_AppName, &
							ls_WindowName, &
							ls_UpdateString, &
							ls_filter, ls_find
long						ll_row,  ll_row_max = 10000
u_ParameterStack		lu_ParameterStack

u_String_Functions	lu_strings			


iu_ErrorContext.uf_Initialize()
SetPointer(HourGlass!)
IF dw_fwh.AcceptText() = -1 THEN 
	dw_fwh.SetFocus()
	Return False
End IF
This.SetRedraw(False)


if ib_filtered then
	ls_filter = ""
	dw_fwh.setfilter(ls_filter)
	dw_fwh.filter()
end if

// ** IBDKEEM ** 04/01/2003 ** Add rethrow logic
ll_row = 0
Do
	ls_UpdateString = ''

	Do
		ll_row = dw_fwh.GetNextModified(ll_row, Primary!)
		If ll_row > 0 Then
			ls_UpdateString += dw_fwh.GetItemString(ll_row, 'productcode') + '~t' + &
					dw_fwh.GetItemString(ll_row, 'divisioncode') + '~t' + &
					dw_fwh.GetItemString(ll_row, 'destplantcode') + '~t' + &
					string(dw_fwh.GetItemnumber(ll_row, 'adjustmentcwt')) + '~t' + &
					'0' + '~r~n'
		End If
	Loop While ll_row > 0 AND LEN(ls_UpdateString) < 19900
	
	
	If lu_strings.nf_IsEmpty(ls_UpdateString) then
		SetMicroHelp('No update necessary')
		This.SetRedraw(True)
		Return False
	end if
	
	////test
	//messagebox("test", ls_UpdateString)
	//messagebox("test", LEN(ls_UpdateString))
	
	SetMicroHelp("Wait... Updating the Database")
		
	ls_WindowName = 'FWH SKU Mass Update'
	iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
	
	lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('string', ls_AppName)
	lu_ParameterStack.uf_Push('string', ls_WindowName)
	lu_ParameterStack.uf_Push('string', ls_UpdateString)
	
	////test
	//messagebox("save",ls_UpdateString)
	
	iu_fwhDataAccess.uf_fwh_sku_adj_upd( lu_ParameterStack )
	
	lu_ParameterStack.uf_Pop('string', ls_DetailString)
	lu_ParameterStack.uf_Pop('string', ls_AppName)
	lu_ParameterStack.uf_Pop('string', ls_WindowName)
	lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

LOOP While ll_row > 0
// ** IBDKEEM ** END **

if ib_filtered then
	dw_fwh.setfilter(is_filter)
	dw_fwh.filter()
end if

This.SetRedraw(True)
dw_fwh.ResetUpdate()
If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

SetMicroHelp("Update Successful")
return true


end event

event ue_reset;is_filter = ""

dw_fwh.setfilter(is_filter)
dw_fwh.filter()
ib_filtered = false
end event

event ue_filter;ib_filtered = true
dw_fwh.setfilter(is_filter)
dw_fwh.filter()
end event

event ue_mass_update;string			ls_find
long				ll_row, ll_row_max = 10000

dw_fwh.accepttext()

choose case is_mass_ind
	case '1' 
		ls_find = "productcode = '" + dw_fwh.GetItemString(il_mass_row, 'productcode') + "'"	
	case '2'
		ls_find = "divisioncode = '" + dw_fwh.GetItemString(il_mass_row, 'divisioncode') + "'"
	case '3'
		ls_find = "destplantcode = '" + dw_fwh.GetItemString(il_mass_row, 'destplantcode') + "'"
end choose
ll_row = 1
ll_row = dw_fwh.find(ls_find,ll_row,ll_row_max)
do while ll_row > 0 and ll_row < dw_fwh.rowcount() + 1
	ll_row = dw_fwh.find(ls_find,ll_row ,dw_fwh.rowcount())
	if ll_row > 0 then
		dw_fwh.setitem(ll_row,'adjustmentcwt', dw_fwh.GetItemnumber(il_mass_row, 'adjustmentcwt'))
		ll_row ++
	end if
loop



end event

event close;close(this)
end event

event resize;call super::resize;IF newwidth  > 10 THEN
	dw_fwh.Width = newwidth - 10
END IF

IF newheight > 10 THEN
	dw_fwh.Height = newheight - 10
END IF
end event

event ue_postopen();call super::ue_postopen;IF this.width  > 50 THEN
	dw_fwh.Width = this.width - 50
END IF

IF this.height > 150 THEN
	dw_fwh.Height = this.height - 150
END IF
end event

type dw_fwh from datawindow within w_fwh_sku_mass_adjustment_upd
integer x = 9
integer y = 12
integer width = 1152
integer height = 1136
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_fwh_sku_mass_adjustment_upd"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event clicked;string ls_name

if row = 0 then
	ls_name = dwo.name
	choose case dwo.name
		case "divisioncode_t"
			wf_sort ("divisioncode")
		case "productcode_t"
			wf_sort ("productcode")
		case "destplantcode_t"
			wf_sort ("destplantcode")
		case "adjustmentcwt_t"
			wf_sort ("adjustmentcwt")
		case else
	end choose
end if
end event

event rbuttondown;m_fwh_popup		lm_popmenu
string			ls_column, ls_variable

ls_column = dwo.name
lm_popmenu = Create m_fwh_popup
choose case ls_column
	case "productcode", "divisioncode", "destplantcode"
		choose case ls_column
			case "productcode"
				is_mass_ind = '1'
			case "divisioncode"
				is_mass_ind = '2'
			case "destplantcode"
				is_mass_ind = '3'
			case else
				is_mass_ind = '0'
		end choose
		il_mass_row = row
		ls_variable = dw_fwh.getitemstring(row, ls_column)
		if ib_filtered then
			is_filter += " AND " + ls_column + ' = "' + ls_variable + '"'
		else
			is_filter = ls_column + ' = "' + ls_variable + '"'
		end if

		if ib_filtered then
			lm_popmenu.m_fwh_options.m_reset.Enabled = True
			lm_popmenu.m_fwh_options.m_reset.visible = true
		else
			lm_popmenu.m_fwh_options.m_reset.Enabled = false
			lm_popmenu.m_fwh_options.m_reset.visible = false
		end if
		if this.rowcount() < 2 then
			lm_popmenu.m_fwh_options.m_filter.Enabled = false
			lm_popmenu.m_fwh_options.m_filter.visible = false
		else
			lm_popmenu.m_fwh_options.m_filter.Enabled = true
			lm_popmenu.m_fwh_options.m_filter.visible = true
		end if
		lm_popmenu.m_fwh_options.m_massupdate.Enabled = true
		lm_popmenu.m_fwh_options.m_massupdate.visible = true
	case else
		lm_popmenu.m_fwh_options.m_massupdate.Enabled = false
		lm_popmenu.m_fwh_options.m_massupdate.visible = false
		lm_popmenu.m_fwh_options.m_filter.Enabled = false
		lm_popmenu.m_fwh_options.m_filter.visible = false
		if ib_filtered then
			lm_popmenu.m_fwh_options.m_reset.Enabled = true
			lm_popmenu.m_fwh_options.m_reset.visible = true
		else
			lm_popmenu.m_fwh_options.m_reset.Enabled = false
			lm_popmenu.m_fwh_options.m_reset.visible = false
		end if
end choose
lm_popmenu.m_fwh_options.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
end event

event itemfocuschanged;this.SelectText ( 1, 10 )
end event

