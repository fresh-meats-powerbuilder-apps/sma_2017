HA$PBExportHeader$smas01sr_programinterfacesmas01ci.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterfaceSmas01ci from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterfaceSmas01ci from nonvisualobject
end type

type variables
    smas01sr_ProgramInterfaceSmas01ciSma000sr_cics_container sma000sr_cics_container
    boolean structuredContainer
end variables

on smas01sr_ProgramInterfaceSmas01ci.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterfaceSmas01ci.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

