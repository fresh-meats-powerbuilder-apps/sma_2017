HA$PBExportHeader$smas01sr_programinterface1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterface1 from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterface1 from nonvisualobject
end type

type variables
    smas01sr_ProgramInterfaceSmas01ci1 smas01ci
    smas01sr_ProgramInterfaceSmas01pg1 smas01pg
    smas01sr_ProgramInterfaceSmas01in1 smas01in
    smas01sr_ProgramInterfaceSmas01ot1 smas01ot
    boolean channel
end variables

on smas01sr_ProgramInterface1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterface1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

