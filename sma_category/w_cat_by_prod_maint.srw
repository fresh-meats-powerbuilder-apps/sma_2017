HA$PBExportHeader$w_cat_by_prod_maint.srw
forward
global type w_cat_by_prod_maint from w_abstractsheetext
end type
type dw_assigned_cat from datawindow within w_cat_by_prod_maint
end type
type cb_left_all from commandbutton within w_cat_by_prod_maint
end type
type cb_right_all from commandbutton within w_cat_by_prod_maint
end type
type cb_left from commandbutton within w_cat_by_prod_maint
end type
type cb_right from commandbutton within w_cat_by_prod_maint
end type
type dw_unassigned_cat from datawindow within w_cat_by_prod_maint
end type
type dw_product_code from datawindow within w_cat_by_prod_maint
end type
end forward

global type w_cat_by_prod_maint from w_abstractsheetext
boolean visible = true
integer width = 3995
integer height = 1912
string title = "Category By Product Maintenance"
dw_assigned_cat dw_assigned_cat
cb_left_all cb_left_all
cb_right_all cb_right_all
cb_left cb_left
cb_right cb_right
dw_unassigned_cat dw_unassigned_cat
dw_product_code dw_product_code
end type
global w_cat_by_prod_maint w_cat_by_prod_maint

type variables
w_cat_by_prod_maint  iw_parent

string                 is_title, &
                       is_inq_product, &
							  is_inq_string, &
							  is_parameter

boolean                ib_reinquire
DataWindow				  idw_selected

u_AbstractErrorContext		    	iu_ErrorContext
u_AbstractClassFactory	    	  	iu_ClassFactory
u_NotificationController	 	   iu_Notification	
u_CategoryDataAccess			iu_CategoryDataAccess
u_ProductDataAccess			iu_ProductDataAccess
w_smaframe		      				iw_frame	
u_odbctransaction						iu_odbctransaction
u_dwselect								iu_DwSelect

u_AbstractParameterStack		iu_ParameterStack


end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack)
public function boolean wf_getproddescr (string as_product_code, ref string as_product_string)
public function boolean wf_move_to_unassign (string as_category_id, string as_category_desc)
public function string wf_buildupdatestring ()
public function boolean wf_move_to_assign (string as_category_id, string as_category_desc)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack);String 	ls_ClassName, &
			ls_Parameter, & 
			ls_WindowName, &
			ls_Type
			
iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

If Not IsValid(au_parameterstack) Then 
	MessageBox("Open","This window can not be opened in this manner.")
	Close(This)
	Return FALSE
END IF

If Not iu_ClassFactory.uf_GetObject( "u_CategoryDataAccess", iu_CategoryDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

au_parameterstack.uf_Pop("string",ls_Type)
au_parameterstack.uf_Pop("string",ls_WindowName)
au_parameterstack.uf_Pop("string",is_Parameter)
au_parameterstack.uf_Pop("string",ls_ClassName)

IF NOT wf_initialize(au_ClassFactory,au_ErrorContext) THEN
	Close(this)
	Return FALSE
END IF

this.title = ls_WindowName

This.Event Post ue_inquire()

Return True
end function

public function boolean wf_getproddescr (string as_product_code, ref string as_product_string);DataWindowChild					ldwc_ProductCode

u_AbstractParameterStack		lu_ParameterStack	

u_String_Functions					lu_String


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_ProductDataAccess", iu_ProductDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_ProductDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
//lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', as_product_code)

iu_ProductDataAccess.uf_RetrieveProducts(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_Product_String)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

If lu_String.nf_IsEmpty(as_product_String) Then
	Return False
End If

dw_product_code.GetChild('product_code', ldwc_ProductCode)
ldwc_ProductCode.ImportString(as_product_string)

Return True

end function

public function boolean wf_move_to_unassign (string as_category_id, string as_category_desc);Long						ll_FindRow, &
							ll_NewRow, &
							ll_SelectedCount

//check to see if the row was previously deleted, change the
//indicator so that it is not flagged for delete.
For ll_FindRow = 1 to dw_unassigned_cat.DeletedCount()
	If dw_unassigned_cat.GetItemString(ll_FindRow, 'category_id', Delete!, False) = as_category_id Then
		dw_unassigned_cat.RowsMove(ll_FindRow, ll_FindRow, Delete!, dw_assigned_cat, 100000, Primary!)
		ll_SelectedCount = dw_unassigned_cat.RowCount()
		Return True
	End If
Next

ll_newrow = dw_unassigned_cat.InsertRow(0)
dw_unassigned_cat.SetItem(ll_newrow, 'category_id', as_category_id)
dw_unassigned_cat.SetItem(ll_newrow, 'category_desc', as_category_desc)
dw_unassigned_cat.SetItem(ll_newrow, 'inc_req_ind', ' ')

Return True

end function

public function string wf_buildupdatestring ();Long					ll_ModifiedCount, &
						ll_count
						
String				ls_UpdateString, &
						ls_row_ind

dwItemStatus		lis_UpdateStatus

ll_ModifiedCount = dw_assigned_cat.ModifiedCount()

If ll_ModifiedCount <= 0 Then
	SetMicroHelp('No Update Necessary')
	Return ''
End If

ls_UpdateString = ''

ll_Count = 0
IF ll_ModifiedCount > 0 Then

	ll_Count = dw_assigned_cat.GetNextModified(ll_Count, Primary!)
	Do
		dw_assigned_cat.SelectRow(0, False)
		dw_assigned_cat.SelectRow(ll_count, True)
		ls_row_ind = dw_assigned_cat.GetItemString(ll_count, 'inc_req_ind')
		if ls_row_ind > ' ' then
			ls_UpdateString += dw_assigned_cat.GetItemString(ll_Count, 'inc_req_ind') + '~t' +  &
				is_inq_product + '~t' + &
				dw_assigned_cat.GetItemString(ll_Count, 'category_id') +  &
						'~r~n'
		End if
		ll_Count = dw_assigned_cat.GetNextModified(ll_Count, Primary!)
	Loop While ll_Count > 0
End If	

//		dw_product_code.GetItemString(1, 'product_code') + '~t' + &
dw_assigned_cat.SelectRow(0, False)

Return ls_UpdateString

end function

public function boolean wf_move_to_assign (string as_category_id, string as_category_desc);Long						ll_FindRow, &
							ll_NewRow, &
							ll_SelectedCount, &
							ll_DeletedCount

//check to see if the row was previously deleted, change the
//indicator so that it is not flagged for delete.

ll_DeletedCount = dw_assigned_cat.RowCount()
ll_FindRow = 1

If ll_DeletedCount > 0 then
  Do 
  		If	dw_assigned_cat.GetItemString(ll_FindRow, 'inc_req_ind') = 'D' and & 
		  dw_assigned_cat.GetItemString(ll_FindRow, 'category_id') = as_category_id Then
				dw_assigned_cat.SetItem(ll_FindRow, 'inc_req_ind', ' ')
				Return True
		End If
		ll_FindRow = ll_FindRow + 1
	Loop While ll_FindRow < ll_DeletedCount + 1
End If

ll_newrow = dw_assigned_cat.InsertRow(0)
dw_assigned_cat.SetItem(ll_newrow, 'category_id', as_category_id)
dw_assigned_cat.SetItem(ll_newrow, 'category_desc', as_category_desc)
dw_assigned_cat.SetItem(ll_newrow, 'inc_req_ind', 'A')

Return True

end function

on w_cat_by_prod_maint.create
int iCurrent
call super::create
this.dw_assigned_cat=create dw_assigned_cat
this.cb_left_all=create cb_left_all
this.cb_right_all=create cb_right_all
this.cb_left=create cb_left
this.cb_right=create cb_right
this.dw_unassigned_cat=create dw_unassigned_cat
this.dw_product_code=create dw_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_assigned_cat
this.Control[iCurrent+2]=this.cb_left_all
this.Control[iCurrent+3]=this.cb_right_all
this.Control[iCurrent+4]=this.cb_left
this.Control[iCurrent+5]=this.cb_right
this.Control[iCurrent+6]=this.dw_unassigned_cat
this.Control[iCurrent+7]=this.dw_product_code
end on

on w_cat_by_prod_maint.destroy
call super::destroy
destroy(this.dw_assigned_cat)
destroy(this.cb_left_all)
destroy(this.cb_right_all)
destroy(this.cb_left)
destroy(this.cb_right)
destroy(this.dw_unassigned_cat)
destroy(this.dw_product_code)
end on

event ue_save;call super::ue_save;Long						ll_row
String					ls_detailstring


u_ParameterStack						lu_ParameterStack

u_String_Functions					lu_strings


iu_ErrorContext.uf_Initialize()

IF dw_assigned_cat.AcceptText() = -1 THEN Return False

This.SetRedraw(False)

ls_DetailString = wf_BuildUpdateString()

If lu_strings.nf_IsEmpty(ls_DetailString) Then 
	This.SetRedraw(True)
	Return False
End If

SetMicroHelp("Wait... Updating the Database")

SetPointer(HourGlass!)

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_CategoryDataAccess.uf_upd_cat_by_prod_maint(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

This.SetRedraw(True)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_assigned_cat.ResetUpdate()

SetMicroHelp("Update Successful")
Return True
end event

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

//iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
//iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
//iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
//iu_ErrorContext.uf_Initialize()
//iu_ClassFactory.uf_GetObject('u_dwselect', iu_DwSelect)

iw_parent = This
is_Title = This.Title
is_inq_product = ""
end event

event close;call super::close;close(this)
end event

event closequery;call super::closequery;Return 0
end event

event ue_inquire;call super::ue_inquire;string					ls_AppName, &
							ls_WindowName, &
							ls_ProductinquireString, &
							ls_ProductAssignString, &
							ls_ProductUnAssignString, &
							ls_string

integer					li_return_code

long						ll_rec_count, &
							ll_unassign_row, &
							ll_assign_row, &
							ll_prod_count, &
							ll_cat_count, &
							ll_row

DataWindowChild				ldwc_child
Window							lw_parentwindow


u_string_functions	lu_string

u_ParameterStack		lu_ParameterStack



iu_ErrorContext.uf_Initialize()

//dw_product_code.reset()

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)
	
IF Not ib_ReInquire Then
	This.Event Trigger CloseQuery()
	
	If Message.ReturnValue <> 0 Then Return False
	
	lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
	lu_ParameterStack.uf_Push('string', is_inq_string)
	lu_ParameterStack.uf_Push('string', is_inq_product)
	lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
	
	iu_ErrorContext.uf_Initialize()
	
	iu_ClassFactory.uf_GetResponseWindow( "w_cat_by_prod_inq", lu_ParameterStack )
	
	lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
	lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
	lu_ParameterStack.uf_Pop('string', is_inq_product)
	lu_ParameterStack.uf_Pop('string', is_inq_string)
	lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)
	
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
	If is_inq_string = 'Cancel' Then 
		li_return_code = messagebox("Caution","Do you want to close this window?",StopSign!,YesNo!,2)
		if li_return_code = 1 then
			this.triggerevent(Close!)
			return false
		else
			Return False
		end if
	End If
End if

ls_AppName = GetApplication().AppName
ls_WindowName = 'catprod'
ls_ProductinquireString = is_inq_product + "~t" +'~r~n'

iu_ErrorContext.uf_Initialize()

lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_appname)
lu_ParameterStack.uf_Push('string', ls_windowname)
lu_ParameterStack.uf_Push('string', ls_ProductinquireString)

iu_CategoryDataAccess.uf_inq_cat_by_prod_maint(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_ProductUnAssignString)
lu_ParameterStack.uf_Pop('string', ls_ProductAssignString)
lu_ParameterStack.uf_Pop('string', ls_windowname)
lu_ParameterStack.uf_Pop('string', ls_appname)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	This.SetRedraw(True)
	Return False
End If

dw_product_code.reset()

If Not wf_getproddescr(is_inq_product,ls_string) Then
	iw_frame.SetMicroHelp('This is an invalid Product Code')
	dw_product_code.SetFocus()
	dw_product_code.SelectText(1, 1000)
	SetPointer(Arrow!)
	Return False
Else
	ll_prod_count = dw_product_code.ImportString(ls_String)
End if


dw_assigned_cat.reset()
dw_unassigned_cat.reset()


ll_unassign_row = dw_unassigned_cat.ImportString(ls_ProductUnAssignString)
ll_assign_row = dw_assigned_cat.ImportString(ls_ProductAssignString)


ll_rec_count = dw_assigned_cat.RowCount()

If ll_rec_count > 0 Then
	SetMicroHelp(String(ll_rec_count) + ' Rows Assigned')
	ll_row = 1
	Do
		dw_assigned_cat.SetItem(ll_row, 'inc_req_ind', ' ')
		ll_row = ll_row + 1
	Loop while ll_row < ll_rec_count + 1
Else
	SetMicroHelp('No Rows Assigned')
	This.SetRedraw(True)
	Return True
End If

dw_assigned_cat.ResetUpdate()

dw_assigned_cat.setsort("category_id")
dw_assigned_cat.Sort()

dw_unassigned_cat.setsort("category_id")
dw_unassigned_cat.Sort()

This.SetRedraw(True)

Return True

end event

event resize;call super::resize;IF newheight > dw_assigned_cat.Y THEN
	dw_assigned_cat.Height = newheight - dw_assigned_cat.y - 20
END IF

IF newheight > dw_unassigned_cat.Y THEN
	dw_unassigned_cat.Height = newheight - dw_unassigned_cat.y - 20
END IF
end event

type dw_assigned_cat from datawindow within w_cat_by_prod_maint
event ue_keydown pbm_dwnkey
integer x = 105
integer y = 220
integer width = 1646
integer height = 1468
integer taborder = 40
string title = "Assigned Categories"
string dataobject = "d_cat_prod_assign"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;DataWindow				ldw_datawindow

IF row = 0 Then Return

If This.IsSelected(row) Then
	This.SelectRow(row, False)
Else
	This.SelectRow(row, TRUE)
End If
dw_assigned_cat.AcceptText()


end event

type cb_left_all from commandbutton within w_cat_by_prod_maint
integer x = 1815
integer y = 948
integer width = 233
integer height = 112
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;String				ls_CategoryId, &
						ls_CategoryDesc

Long					ll_SelectedCount, &
						ll_row, &
						ll_newrow
						

ll_SelectedCount = dw_unassigned_cat.RowCount()

IF ll_SelectedCount = 0 Then Return

ll_row = 1

SetPointer(HourGlass!)


Parent.SetRedraw(False)

Do
	ls_CategoryId = dw_unassigned_cat.GetItemString(ll_row, 'category_id')
	ls_CategoryDesc = dw_unassigned_cat.GetItemString(ll_row, 'category_desc')
	Parent.wf_move_to_assign(ls_CategoryId, ls_CategoryDesc)
	dw_unassigned_cat.DeleteRow(ll_row)
	ll_SelectedCount = ll_SelectedCount - 1
	
Loop While ll_SelectedCount > 0

dw_assigned_cat.Sort()

Parent.SetRedraw(True)


end event

type cb_right_all from commandbutton within w_cat_by_prod_maint
integer x = 1815
integer y = 756
integer width = 233
integer height = 112
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;Boolean				lb_found
Long					ll_row, &
						ll_newrow, &
						ll_Count, &
						ll_findRow, &
						ll_SelectedCount, &
						ll_returncode
						
String				ls_CategoryId, &
						ls_CategoryDesc, &
						ls_inc_req_ind
			
						
ll_SelectedCount = dw_assigned_cat.RowCount()

IF ll_SelectedCount = 0 Then Return

ll_row = 1

Do
	ls_CategoryId = dw_assigned_cat.GetItemString(ll_row, 'category_id')
	ls_CategoryDesc = dw_assigned_cat.GetItemString(ll_row, 'category_desc')
	ls_inc_req_ind = dw_assigned_cat.GetItemString(ll_row, 'inc_req_ind')
	If ls_inc_req_ind = 'D' Then
		// do nothing- row has already been moved.
	Else
		Parent.wf_move_to_unassign(ls_CategoryId, ls_CategoryDesc)
	End If
	
	If ls_inc_req_ind = ' ' or ls_inc_req_ind = 'D'   Then
		dw_assigned_cat.SetItem(ll_row, 'inc_req_ind', 'D')
		ll_row = ll_row + 1	
	Else
		dw_assigned_cat.DeleteRow(ll_row)
		ll_SelectedCount = ll_SelectedCount - 1
	End If
	
Loop While (ll_row < ll_SelectedCount + 1) and (ll_SelectedCount > 0) 





end event

type cb_left from commandbutton within w_cat_by_prod_maint
integer x = 1815
integer y = 560
integer width = 233
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;String				ls_CategoryId, &
						ls_CategoryDesc

Long					ll_row, &
						ll_newrow
						
						
ll_row = dw_unassigned_cat.GetSelectedRow(0)

IF ll_row = 0 Then Return

SetPointer(HourGlass!)

Parent.SetRedraw(False)

Do
	ls_CategoryId = dw_unassigned_cat.GetItemString(ll_row, 'category_id')
	ls_CategoryDesc = dw_unassigned_cat.GetItemString(ll_row, 'category_desc')
	Parent.wf_move_to_assign(ls_CategoryId, ls_CategoryDesc)
	dw_unassigned_cat.DeleteRow(ll_row)
	
	ll_row = dw_unassigned_cat.GetSelectedRow(0)	
Loop While ll_row > 0

dw_assigned_cat.setsort("inc_req_ind, category_id")
dw_assigned_cat.Sort()

Parent.SetRedraw(True)


end event

type cb_right from commandbutton within w_cat_by_prod_maint
integer x = 1815
integer y = 372
integer width = 233
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;Boolean				lb_found
Long					ll_row, &
						ll_newrow, &
						ll_Count, &
						ll_findRow, &
						ll_SelectedCount, &
						ll_returncode
						
String				ls_CategoryId, &
						ls_CategoryDesc, &
						ls_inc_req_ind
			
						
ll_SelectedCount = dw_assigned_cat.RowCount()

ll_row = dw_assigned_cat.GetSelectedRow(0)

IF ll_row = 0 Then Return

Do
	ls_CategoryId = dw_assigned_cat.GetItemString(ll_row, 'category_id')
	ls_CategoryDesc = dw_assigned_cat.GetItemString(ll_row, 'category_desc')
	ls_inc_req_ind = dw_assigned_cat.GetItemString(ll_row, 'inc_req_ind')
	Parent.wf_move_to_unassign(ls_CategoryId, ls_CategoryDesc)
	If ls_inc_req_ind = 'A' Then
		dw_assigned_cat.DeleteRow(ll_row)
	Else
		dw_assigned_cat.SetItem(ll_row, 'inc_req_ind', 'D')
		dw_assigned_cat.SelectRow(ll_row, False)
	End If
	ll_row = dw_assigned_cat.GetSelectedRow(0)	
	
Loop While ll_row > 0

dw_assigned_cat.setsort("inc_req_ind, category_id")
dw_assigned_cat.Sort()




end event

type dw_unassigned_cat from datawindow within w_cat_by_prod_maint
event ue_keydown pbm_dwnkey
integer x = 2103
integer y = 212
integer width = 1646
integer height = 1468
integer taborder = 10
string title = "Assigned Categories"
string dataobject = "d_cat_prod_unassign"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;DataWindow				ldw_datawindow

IF row = 0 Then Return

If This.IsSelected(row) Then
	This.SelectRow(row, False)
Else
	This.SelectRow(row, TRUE)
End If
dw_unassigned_cat.AcceptText()
end event

type dw_product_code from datawindow within w_cat_by_prod_maint
integer x = 1193
integer y = 20
integer width = 1472
integer height = 100
string title = "none"
string dataobject = "d_product_code"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

