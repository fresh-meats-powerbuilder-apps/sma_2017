HA$PBExportHeader$u_categorydataaccesswebservice.sru
forward
global type u_categorydataaccesswebservice from u_categorydataaccess
end type
end forward

global type u_categorydataaccesswebservice from u_categorydataaccess
end type
global u_categorydataaccesswebservice u_categorydataaccesswebservice

forward prototypes
public function boolean uf_inq_prod_cat_maint (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_upd_prod_cat_maint (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_inq_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_upd_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_inq_prod_cat_maint (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString, &
									ls_outputstring
				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User
u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquirestring)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
End if

lu_smas01sr.uf_smas30er_inq_prod_cat_maint(ls_windowname, &
													"uf_inq_prod_cat_maint", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquirestring, &
													ls_outputstring, &
													lu_ErrorContext) 

												
If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_outputstring)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_outputstring)

return true
end function

public function boolean uf_upd_prod_cat_maint (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------
uf_Update()

<DESC> Update the Product Category Window Data
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will Update the product category info
</USAGE> Window Data from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_DetailString, &
									ls_GroupID, &
									ls_appname
								
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_DetailString)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

ls_AppName = GetApplication().AppName

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

lu_smas01sr.uf_smas27er_upd_prod_cat_maint(ls_appname, &
													"w_Prod_Cat_Maint", &
													"uf_upd_prod_cat_maint", &
													"", &
													ls_userid, &
													ls_password, &
													ls_detailstring, &
													lu_ErrorContext)

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_DetailString)

If Not lu_ErrorContext.uf_IsSuccessful() Then return False

return True

end function

public function boolean uf_inq_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_appname, &
									ls_windowname, &
									ls_GroupID, &
									ls_inquireString, &
									ls_assignstring, &
									ls_unassignstring
				
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User
u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_inquirestring)
au_ParameterStack.uf_Pop('string', ls_windowname)
au_ParameterStack.uf_Pop('string', ls_appname)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		au_ParameterStack.uf_Push('string', ls_assignstring)
		au_ParameterStack.uf_Push('string', ls_unassignstring)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		au_ParameterStack.uf_Push('string', ls_assignstring)
		au_ParameterStack.uf_Push('string', ls_unassignstring)
		Return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_assignstring)
		au_ParameterStack.uf_Push('string', ls_unassignstring)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('string', ls_appname)
		au_ParameterStack.uf_Push('string', ls_windowname)
		au_ParameterStack.uf_Push('string', ls_inquirestring)
		Return False
	End If
End if

lu_smas01sr.uf_smas28er_inq_cat_by_product (ls_windowname, &
													"uf_inq_cat_by_prod_maint", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inquirestring, &
													ls_assignstring, &
													ls_unassignstring, &
													lu_ErrorContext) 

												
If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('string', ls_appname)
	au_ParameterStack.uf_Push('string', ls_windowname)
	au_ParameterStack.uf_Push('string', ls_assignstring)
	au_ParameterStack.uf_Push('string', ls_unassignstring)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_appname)
au_ParameterStack.uf_Push('string', ls_windowname)
au_ParameterStack.uf_Push('string', ls_assignstring)
au_ParameterStack.uf_Push('string', ls_unassignstring)

return true
end function

public function boolean uf_upd_cat_by_prod_maint (ref u_abstractparameterstack au_parameterstack);String							ls_UserID, &
									ls_Password, &
									ls_DetailString, &
									ls_GroupID, &
									ls_appname
								
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack
u_AbstractClassFactory		lu_ClassFactory
u_AbstractErrorContext		lu_ErrorContext

au_ParameterStack.uf_Pop('string', ls_DetailString)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

ls_AppName = GetApplication().AppName

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If

	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_DetailString)
		return False
	End If
End if

lu_smas01sr.uf_smas29er_upd_cat_by_product(ls_appname, &
													"w_CatByProduct", &
													"uf_upd_cat_by_prod_maint", &
													"", &
													ls_userid, &
													ls_password, &
													ls_detailstring, &
													lu_ErrorContext)

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_DetailString)

If Not lu_ErrorContext.uf_IsSuccessful() Then return False

return True

end function

on u_categorydataaccesswebservice.create
call super::create
end on

on u_categorydataaccesswebservice.destroy
call super::destroy
end on

