HA$PBExportHeader$w_cat_by_prod_inq.srw
forward
global type w_cat_by_prod_inq from w_abstractresponseext
end type
type dw_product_code from datawindow within w_cat_by_prod_inq
end type
end forward

global type w_cat_by_prod_inq from w_abstractresponseext
integer width = 1769
integer height = 436
string title = "Product Code Inquire"
boolean contexthelp = true
dw_product_code dw_product_code
end type
global w_cat_by_prod_inq w_cat_by_prod_inq

type variables
String		is_selected, &
				is_type, &
            is_whatwastyped, &
            is_userid, &
            is_password, &
            is_owner, &
            is_inq_product, &
				is_inq_descr, &
				is_inq_string
				

datastore   ids_product

u_AbstractErrorContext				iu_ErrorContext
u_RebateDataAccess					iu_RebateDataAccess
u_ClassFactory							iu_ClassFactory
u_AbstractNotificationController	iu_Notification
u_AbstractParameterStack			iu_ParameterStack
u_ProductDataAccess					iu_ProductDataAccess
u_dwselect								iu_DwSelect
u_olecom									iu_olecom, &
                                 iu_olepfm


w_smaframe			iw_frame	
end variables

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean wf_getproducts (string as_product, ref string as_productstring)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then 
	iu_notification.uf_display(iu_ErrorContext)
	Return False
End If

iu_ErrorContext.uf_Initialize()

Return True
end function

public function boolean wf_getproducts (string as_product, ref string as_productstring);Long									ll_temp

u_AbstractParameterStack		lu_ParameterStack	
Datawindowchild					ldwc_child



iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject( "u_ProductDataAccess", iu_ProductDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_ProductDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

iu_ErrorContext.uf_Initialize()
lu_ParameterStack.uf_Initialize()

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', as_Product)

iu_ProductDataAccess.uf_RetrieveProducts(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', as_ProductString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_product_code.GetChild('product_code', ldwc_child)
ldwc_child.Reset()
ll_temp = ldwc_child.ImportString(as_productstring)

ldwc_child.setsort("sku_product_code")
ldwc_child.sort()

Return True

end function

on w_cat_by_prod_inq.create
int iCurrent
call super::create
this.dw_product_code=create dw_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_code
end on

on w_cat_by_prod_inq.destroy
call super::destroy
destroy(this.dw_product_code)
end on

event ue_postopen;call super::ue_postopen;integer				li_tabnum

string				ls_string

this.setredraw(false)

if is_inq_string = "Cancel" then
	is_inq_string = ""
end if

If Not wf_getproducts('', ls_String) Then 
	This.SetRedraw(True)
	Return
End If

dw_product_code.ImportString(ls_String)

dw_product_code.SetItem(1,"product_code", is_inq_product)

This.SetRedraw(True)
							                       

end event

event open;call super::open;iu_ParameterStack = Message.PowerObjectParm

If Not IsValid(iu_ParameterStack) Then Close(This)

iu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)	
iu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
iu_parameterstack.uf_Pop('string', is_inq_product)
iu_parameterstack.uf_Pop('string', is_inq_string)
iu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

this.setredraw(true)

end event

event close;call super::close;iu_ErrorContext.uf_initialize()

iu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
iu_ParameterStack.uf_Push('string', is_inq_string)
iu_ParameterStack.uf_Push('string', is_inq_product)
iu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
iu_ParameterStack.uf_Push('u_NotificationController', iu_notification)

CloseWithReturn(This, iu_ParameterStack)
end event

type cb_help from w_abstractresponseext`cb_help within w_cat_by_prod_inq
boolean visible = false
integer x = 590
integer y = 304
integer taborder = 0
boolean enabled = false
end type

type cb_cancel from w_abstractresponseext`cb_cancel within w_cat_by_prod_inq
integer x = 1381
integer y = 184
boolean cancel = true
end type

event cb_cancel::clicked;call super::clicked;is_inq_string = 'Cancel'

Close(Parent)
end event

type cb_ok from w_abstractresponseext`cb_ok within w_cat_by_prod_inq
integer x = 1408
integer y = 20
boolean default = true
end type

event cb_ok::clicked;call super::clicked;
If dw_product_code.AcceptText() = -1 Then Return

is_inq_product = dw_product_code.GetItemString(1, "product_code")

Close(Parent)
end event

type dw_product_code from datawindow within w_cat_by_prod_inq
integer x = 146
integer y = 92
integer width = 1166
integer height = 104
integer taborder = 5
boolean bringtotop = true
string title = "none"
string dataobject = "d_product_code_inq"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;dw_product_code.insertrow(0)
end event

event itemchanged;long							ll_count

string						ls_action_ind, &
								ls_product_code, &
								ls_errorstring
			
u_String_Functions		lu_String

If (data = "00000") &
	or (lu_string.nf_isEmpty(data)) then
	messagebox("Required Field","Product Code is a required field.",StopSign!,OK!)
	dw_product_code.setfocus()
	dw_product_code.SetColumn('product_code')
	is_inq_string = "Cancel"
	return
End if

is_inq_product = data

//Close(Parent)
end event

event itemerror;return -1
end event

event itemfocuschanged;This.SelectText(1, 100)
end event

