HA$PBExportHeader$u_utl001.sru
forward
global type u_utl001 from u_netwise
end type
end forward

global type u_utl001 from u_netwise
end type
global u_utl001 u_utl001

type prototypes
Private:// PowerBuilder Script File: j:\pb\test\src32\utl001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Sat Mar 22 12:20:31 1997
// Source Interface File: j:\pb\test\src32\utl001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: utlu00ar_get_security_code
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu00ar_get_security_code( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Name_System_input, &
    ref string group_id, &
    int CommHnd &
) library "utl001.dll" alias for "utlu00ar_get_security_code;Ansi"


//
// Declaration for procedure: utlu02ar_get_all_window_access
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu02ar_get_all_window_access( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double task_number, &
    ref double page_number, &
    ref double Max_page_number, &
    ref string window_access_string, &
    int CommHnd &
) library "utl001.dll" alias for "utlu02ar_get_all_window_access;Ansi"


//
// Declaration for procedure: utlu03ar_get_tutltype
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu03ar_get_tutltype( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref int tutltype_rec_occurs, &
    ref string tutltype_info, &
    char refetch_record_type[8], &
    char refetch_type_code[8], &
    int CommHnd &
) library "utl001.dll" alias for "utlu03ar_get_tutltype;Ansi"


//
// Declaration for procedure: utlu04ar_get_loc_codes
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu04ar_get_loc_codes( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref int tutlloc_rec_occurs, &
    ref string tutlloc_info, &
    int CommHnd &
) library "utl001.dll" alias for "utlu04ar_get_loc_codes;Ansi"


//
// Declaration for procedure: utlu05ar_Get_Sale_people
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu05ar_Get_Sale_people( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string sales_people_string, &
    ref int sales_people_count, &
    ref string smancode, &
    ref string smantype, &
    int CommHnd &
) library "utl001.dll" alias for "utlu05ar_Get_Sale_people;Ansi"


//
// Declaration for procedure: utlu06ar_get_windows_to_access
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu06ar_get_windows_to_access( &
    string se_app_name, &
    string se_window_name, &
    string se_function_name, &
    string se_event_name, &
    string se_procedure_name, &
    string se_use_id, &
    string se_return_code, &
    string se_message, &
    string groupid_name_string, &
    ref string window_info_String, &
    ref int num_recs_rtn, &
    int CommHnd &
) library "utl001.dll" alias for "utlu06ar_get_windows_to_access;Ansi"


//
// Declaration for procedure: utlu07ar_Get_Security
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu07ar_Get_Security( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string name_password, &
    ref string Action_Indicator, &
    int CommHnd &
) library "utl001.dll" alias for "utlu07ar_Get_Security;Ansi"


//
// Declaration for procedure: utlu09ar_Header
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu09ar_Header( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref long msg_number, &
    ref string header_String, &
    string exception_String, &
    string UserId_in, &
    int CommHnd &
) library "utl001.dll" alias for "utlu09ar_Header;Ansi"


//
// Declaration for procedure: utlu10ar_get_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu10ar_get_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref long msg_number, &
    ref long msg_detail_number, &
    ref string Detail_String, &
    string UserID_in, &
    int CommHnd &
) library "utl001.dll" alias for "utlu10ar_get_detail;Ansi"


//
// Declaration for procedure: utlu11ar_Delete_msg
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu11ar_Delete_msg( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string msg_number_string, &
    int CommHnd &
) library "utl001.dll" alias for "utlu11ar_Delete_msg;Ansi"


//
// Declaration for procedure: utlu12ar_check_message
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu12ar_check_message( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string ip_address, &
    ref char check_message, &
    int CommHnd &
) library "utl001.dll" alias for "utlu12ar_check_message;Ansi"


//
// Declaration for procedure: utlu14ar_GetMessageCounts
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu14ar_GetMessageCounts( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_reurn_code, &
    ref string se_message, &
    ref string message_count_and_type, &
    string UserID_in, &
    int CommHnd &
) library "utl001.dll" alias for "utlu14ar_GetMessageCounts;Ansi"


//
// Declaration for procedure: utlu15ar_GetUsers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu15ar_GetUsers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string List_Of_Users, &
    int CommHnd &
) library "utl001.dll" alias for "utlu15ar_GetUsers;Ansi"


//
// Declaration for procedure: utlu16ar_Alias_Extract
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu16ar_Alias_Extract( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Req_user_id, &
    ref string Alias_String, &
    int CommHnd &
) library "utl001.dll" alias for "utlu16ar_Alias_Extract;Ansi"


//
// Declaration for procedure: utlu17ar_Alias_Update
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu17ar_Alias_Update( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_String, &
    int CommHnd &
) library "utl001.dll" alias for "utlu17ar_Alias_Update;Ansi"


//
// Declaration for procedure: utlu18ar_get_shipto_customers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu18ar_get_shipto_customers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_number, &
    ref double Page_number, &
    ref double max_page_number, &
    ref string Shipto_Customers, &
    int CommHnd &
) library "utl001.dll" alias for "utlu18ar_get_shipto_customers;Ansi"


//
// Declaration for procedure: utlu19ar_get_billto_customers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu19ar_get_billto_customers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_Number, &
    ref double Page_number, &
    ref string Billto_customers, &
    int CommHnd &
) library "utl001.dll" alias for "utlu19ar_get_billto_customers;Ansi"


//
// Declaration for procedure: utlu20ar_Get_Corporate_customers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu20ar_Get_Corporate_customers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_Number, &
    ref double Page_number, &
    ref string Corporate_customers, &
    int CommHnd &
) library "utl001.dll" alias for "utlu20ar_Get_Corporate_customers;Ansi"


//
// Declaration for procedure: utlu21ar_get_serviceCenters
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu21ar_get_serviceCenters( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string sales_location_and_names, &
    int CommHnd &
) library "utl001.dll" alias for "utlu21ar_get_serviceCenters;Ansi"


//
// Declaration for procedure: utlu22ar_Get_Carrier_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu22ar_Get_Carrier_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string carrier_type, &
    string carrier_status, &
    ref string carrier_info_string, &
    ref string refetch_carrier_code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu22ar_Get_Carrier_info;Ansi"


//
// Declaration for procedure: utlu23ar_get_carrier_pref_ext
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu23ar_get_carrier_pref_ext( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string carrier_pref_string, &
    ref string refetch_customer_id, &
    ref string refetch_customer_type, &
    ref string refetch_carrier_code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu23ar_get_carrier_pref_ext;Ansi"


//
// Declaration for procedure: utlu24ar_get_customer_defaults
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu24ar_get_customer_defaults( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Location, &
    ref string customer_defaults_out, &
    ref double Task_number, &
    ref double Page_number, &
    ref double Max_Page_number, &
    int CommHnd &
) library "utl001.dll" alias for "utlu24ar_get_customer_defaults;Ansi"


//
// Declaration for procedure: utlu25ar
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu25ar( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Recipient_id, &
    string App_ID, &
    string Type_code, &
    ref string Telephone_Number, &
    ref string cover_form, &
    ref string Reciepient_name, &
    ref string Reciepient_attn, &
    int CommHnd &
) library "utl001.dll" alias for "utlu25ar;Ansi"


//
// Declaration for procedure: utlu26ar
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu26ar( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string product_codes_out, &
    ref double max_page_number, &
    ref double page_number, &
    ref double task_number, &
    int CommHnd &
) library "utl001.dll" alias for "utlu26ar;Ansi"

//
// Declaration for procedure: utlu27ar_Load_Address
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu27ar_Load_Address( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string Address_Data_String, &
    ref string Refetch_Address_Type, &
    ref string Refetch_Address_Code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu27ar_Load_Address;Ansi"

//
// Declaration for procedure: utlu28ar_Load_Tskuplt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu28ar_Load_Tskuplt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string Product_data_String, &
    ref string Refetch_product_code, &
    ref string Refetch_plant_code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu28ar_Load_Tskuplt;Ansi"


// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "utl001.dll"
function int WButl001CkCompleted( int CommHnd ) &
    library "utl001.dll"
//
// ***********************************************************


end prototypes

forward prototypes
public function integer uf_utlu07ar_getsecurity (ref string as_appname, ref string as_windowname, ref string as_functionname, ref string as_eventname, ref string as_procedurename, ref string as_userid, ref string as_returncode, ref string as_message, string as_password, ref character ac_actionindicator)
public function boolean uf_utlu02ar_getwindowaccess (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref string as_data, ref u_abstracterrorcontext as_errorcontext)
public function boolean uf_utlu00ar_getsecuritycode (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref string as_groupid, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu03ar_gettypedesc (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_tutltype, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu04ar_getlocations (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_locations, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu27ar_getaddresses (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_addresses, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu22ar_getcarriers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_carriers, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu23ar_getcarrierprefs (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_carrierprefs, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu21ar_getservicecenters (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_servicecenters, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu05ar_getsalespeople (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_salespeople, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu26ar_getproducts (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_products, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu18ar_getshiptocustomers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customers, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu19ar_getbilltocustomers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customers, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu20ar_getcorporatecustomers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customers, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu24ar_getcustomerdefaults (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customerdefaults, string as_location, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_utlu28ar_getskuplt (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_salespeople, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function integer uf_utlu07ar_getsecurity (ref string as_appname, ref string as_windowname, ref string as_functionname, ref string as_eventname, ref string as_procedurename, ref string as_userid, ref string as_returncode, ref string as_message, string as_password, ref character ac_actionindicator);
/* --------------------------------------------------------

<DESC>	Get security information from the mainframe.</DESC>

<ARGS>	as_AppName: String for Netwise
			as_WindowName: String for Netwise
			as_FunctionName: String for Netwise
			as_EventName: String for Netwise
			as_ProcedureName: String for Netwise
			as_UserID: UserID for mainframe	
			as_ReturnCode: String for Netwise
			as_Message: String for Netwise
			as_Password: UserID, tab, and Password for mainframe
			ac_actionindicator: Results of security check</ARGS>
			
<USAGE>	Call this function to verify the mainframe UserID
			passed.  Action indicator gives results of the
			security check:	<LI> G - good </LI>
									<LI> S - suspended </LI>
									<LI> A - security violation </LI>
									<LI> X - expired </LI>
									<LI> I - invalid </LI>
									</USAGE>
-------------------------------------------------------- */
Integer	li_ReturnCode

String ls_action

ls_action = 'V' + Char(0)

//li_ReturnCode = utlu07ar_get_security(as_AppName, &
//		as_WindowName, &
//		as_FunctionName, &
//		as_EventName, &
//		as_ProcedureName, &
//		as_UserID,&
//		as_ReturnCode,&
//		as_Message, &
//		as_Password, &
//		ls_action, &
//		ii_CommHnd)
				
ac_ActionIndicator = ls_action				
				
Return li_ReturnCode
end function

public function boolean uf_utlu02ar_getwindowaccess (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref string as_data, ref u_abstracterrorcontext as_errorcontext);
/* --------------------------------------------------------
uf_utlu02ar_GetWindowAccess

<DESC> This function retrieves all window permissions from DB/2
</DESC>

<ARGS>   as_objectname: The object that called this function
			as_functionname: Function of the object that called this
			as_eventname: Event on the object that called this
			as_userid: UserID of person logged in
			as_data: Data to be returned
			as_errorcontext: Error Context
</ARGS>

<USAGE> Call this function to retrieve the data for the users window
			permissions.  If an error occurs, as_ErrorContext will be populated.
</USAGE>
-------------------------------------------------------- */

Double	ld_Task, &
			ld_Page, &
			ld_MaxPage

Int	li_ReturnCode

String	ls_app, &
			ls_Procedure, &
			ls_ReturnCode, &
			ls_Message, &
			ls_Data


ls_Procedure = "uf_utlu02ar_GetWindowAccess"
ls_App = GetApplication().AppName
ls_Message = Space(70)

Do 
	ls_data = Space(19880)
//	li_ReturnCode =  utlu02ar_get_all_window_access( ls_App, &
//																	as_ObjectName, &
//																	as_Functionname, &
//																	as_eventName, &
//																	ls_Procedure, &
//																	as_userid, &
//																	ls_returnCode, &
//																	ls_Message, &
//																	ld_Task, &
//																	ld_Page, &
//																	ld_MaxPage, &
//																	ls_data, &
//																	ii_commhnd)
	If Right(as_data, 2) <> '~r~n' Then as_data += '~r~n'
	as_data += ls_data
	If Not This.uf_CheckRPCError(as_ErrorContext) Then Return False
Loop While ld_Page <> ld_MaxPage

//as_ErrorContext.uf_SetReturnCode(Long(ls_returnCode))
//as_ErrorContext.uf_AppendText(ls_Message)
return true
end function

public function boolean uf_utlu00ar_getsecuritycode (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref string as_groupid, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_utlu00ar_GetSecurityCode

<DESC> This function retrieves a user's Security Code from DB/2
</DESC>

<ARGS>   as_objectname: The object that called this function
			as_functionname: Function of the object that called this
			as_eventname: Event on the object that called this
			as_userid: UserID of person logged in
			as_groupid: Group ID of the current User
			as_errorcontext: Error Context
</ARGS>

<USAGE> Call this function to retrieve the user's permissions.  
			If an error occurs, as_ErrorContext will be populated.
</USAGE>
-------------------------------------------------------- */

Int	li_ReturnCode

String	ls_app, &
			ls_Procedure, &
			ls_ReturnCode, &
			ls_Message, &
			ls_Input


ls_Procedure = "uf_utlu00ar_GetSecurityCode"
ls_App = GetApplication().AppName
ls_Message = Space(70)
ls_Input = Upper(as_userid + '~t' + ls_App)

//li_ReturnCode =  utlu00ar_get_security_code( ls_App, &
//																as_ObjectName, &
//																as_Functionname, &
//																as_eventName, &
//																ls_Procedure, &
//																as_userid, &
//																ls_returnCode, &
//																ls_Message, &
//																ls_Input, &
//																as_groupid, &
//																ii_commhnd)
If Len(Trim(ls_Message)) > 0 Then 
	// This is usually an access-denied message, but uf_CheckRPCError() thinks everything's peachy
	au_ErrorContext.uf_AppendText(ls_Message)																
	au_ErrorContext.uf_SetReturnCode(-2)
End If
Return This.uf_CheckRPCError(au_ErrorContext)

end function

public function boolean uf_utlu03ar_gettypedesc (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_tutltype, ref u_abstracterrorcontext au_errorcontext);Char		lc_refetch_record_type[8], &
			lc_refetch_type_code[8]

Int 		li_rtn, &
			li_rec_count, &
			li_tutltype_rec_occurs

Long		ll_row_count

String	ls_tutltype_info, &
			ls_temp

String	ls_appname, &
			ls_procedurename = "utlu03ar_gettypedesc", &
			ls_returncode,&
			ls_message = Space(70)

// App names are only three characters passed to Netwise
ls_appname = Left(GetApplication().AppName,3)

// The following error is supposed to be discovered only in debug builds
If ads_tutltype.DataObject <> 'd_tutltype' Then
	au_ErrorContext.uf_AppendText("u_utl001.uf_utlu03ar: argument 'ads_tutltype' must have DataObject 'd_tutltype'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

// Warm and fuzzy for user
SetPointer(HourGlass!)

Do
	ls_TutlType_Info = Space(14601)

	// Find last key retrieved--that is input into RPC
	ll_row_count = ads_tutltype.RowCount()
	If ll_row_count > 0 Then
		ls_temp = ads_tutltype.GetItemString(ads_tutltype.RowCount(), "record_type")
		If Not IsNull(ls_temp) Then
			lc_refetch_record_type = ls_temp
		Else
			lc_refetch_record_type = Space(8)
		End if
	
		ls_temp = ads_tutltype.GetItemString(ads_tutltype.RowCount(), "type_code")
		If Not IsNull(ls_temp) Then 
			lc_refetch_type_code = ls_temp
		Else
			lc_refetch_type_code = Space(8)
		End if
	Else
		lc_refetch_record_type = Space(8)
		lc_refetch_type_code = Space(8)
	End if

	// Call a Netwise external function to get the required information
//	li_rtn = utlu03ar_get_tutltype(ls_appname, &
//											as_objectname, &
//											as_functionname, &
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											li_tutltype_rec_occurs, &
//											ls_tutltype_info, &
//											lc_refetch_record_type, &
//											lc_refetch_type_code, &
//											ii_commhnd)

	// Check RPC errors and display any messages
	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_Message)																
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return This.uf_CheckRPCError(au_ErrorContext)
	End If

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	// Add the retrieved data to the passed DataStore
	li_rec_count = ads_tutltype.ImportString(Trim(ls_TutlType_Info))
	If li_rec_count < 1 Then
		au_ErrorContext.uf_AppendText("Unable to import data for utility types.")
		au_ErrorContext.uf_Set("ImportString() returned", li_rec_count)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If

Loop While li_tutltype_rec_occurs = 200

Return This.uf_CheckRPCError(au_ErrorContext)

end function

public function boolean uf_utlu04ar_getlocations (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_locations, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info

SetPointer(HourGlass!)

If ads_locations.DataObject <> "d_locations" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu04ar: DataObject must be 'd_locations'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu04ar_get_loc_codes"
ls_message = Space(70)

Do
	ls_info = Space(9401)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu04ar_get_loc_codes( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											li_count, &
//											ls_info, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_locations.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu04ar: Cannot import locations")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While li_count = 200

Return True

end function

public function boolean uf_utlu27ar_getaddresses (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_addresses, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_addressdata, &
			ls_RefetchType, &
			ls_RefetchCode

SetPointer(HourGlass!)

If ads_addresses.DataObject <> "d_addresses" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu27ar: DataObject must be 'd_addresses'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu27ar_load_addresses"
ls_message = Space(70)

Do
	ls_addressdata = Space(60000)
	ls_RefetchType = Space(8)
	ls_RefetchCode = Space(10)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu27ar_Load_Address( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ls_addressdata, &
//											ls_RefetchType, &
//											ls_RefetchCode, &
//											ii_commhnd)


	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_addresses.ImportString(Trim(ls_AddressData))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu27ar: Cannot import addresses")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While Len(Trim(ls_RefetchType)) > 0 And Len(Trim(ls_RefetchCode)) > 0

Return True
end function

public function boolean uf_utlu22ar_getcarriers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_carriers, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_CarrierInfo, &
			ls_RefetchCode, &
			ls_CarrierStatus, &
			ls_CarrierType

SetPointer(HourGlass!)

If ads_carriers.DataObject <> "d_carriers" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu22ar: DataObject must be 'd_carriers'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu22ar_load_carriers"
ls_message = Space(70)

Do
	ls_CarrierStatus = Space(1)
	ls_CarrierType = Space(1)
	ls_CarrierInfo = Space(2101)
	ls_RefetchCode = Space(4)

	If ads_carriers.RowCount() > 0 Then
		ls_RefetchCode = ads_carriers.GetItemString(ads_carriers.RowCount(), "carrier_code")
	End If

	// Call a Netwise external function to get the required information
//	li_rtn = utlu22ar_Get_Carrier_Info( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ls_CarrierType, &
//											ls_CarrierStatus, &
//											ls_CarrierInfo, &
//											ls_RefetchCode, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_carriers.ImportString(Trim(ls_CarrierInfo))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu22ar: Cannot import carriers")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While li_rtn = 100
												 
Return True

end function

public function boolean uf_utlu23ar_getcarrierprefs (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_carrierprefs, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_CarrierPrefInfo, &
			ls_RefetchCustomerID, &
			ls_RefetchCustomerType, &
			ls_RefetchCarrierCode

SetPointer(HourGlass!)

If ads_carrierprefs.DataObject <> "d_carrierprefs" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu23ar: DataObject must be 'd_carrierprefs'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu23ar_load_carriers"
ls_message = Space(70)

Do
	ls_CarrierPrefInfo = Space(4501)
	ls_RefetchCustomerID = Space(7)
	ls_RefetchCustomerType = Space(1)
	ls_RefetchCarrierCode = Space(4)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu23ar_Get_Carrier_Pref_Ext( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ls_CarrierPrefInfo, &
//											ls_RefetchCustomerID, &
//											ls_RefetchCustomerType, &
//											ls_RefetchCarrierCode, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_carrierprefs.ImportString(Trim(ls_CarrierPrefInfo))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu23ar: Cannot import carrier prefs")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While li_rtn = 100
												 
Return True

end function

public function boolean uf_utlu21ar_getservicecenters (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_servicecenters, ref u_abstracterrorcontext au_errorcontext);int 		li_rtn

String	ls_appname, &
			ls_procedurename, &
			ls_returncode,&
			ls_message, &
			ls_output

ls_output = Fill(Char(0),3101)
ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu21ar_get_servicecenters"

If ads_servicecenters.DataObject <> "d_servicecenters" Then
	au_ErrorContext.uf_AppendText("u_utl001.uf_utlu21ar: DataObject must be 'd_servicecenters'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

SetPointer(HourGlass!)

// Call a Netwise external function to get the customer information
//li_rtn = utlu21ar_get_serviceCenters( &
//			ls_appname, &
//			as_objectname,&
//			as_functionname,&
//			as_eventname, &
//			ls_procedurename, &
//			as_userid, &
//			ls_returncode, &
//			ls_message, &
//			ls_output, &
//			ii_commhnd)

If Not This.uf_CheckRPCError(au_errorcontext) Then Return False

li_rtn = ads_servicecenters.ImportString(ls_output)

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText("u_utl001.uf_utlu21ar: Problem importing into DataStore")
	au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
	au_ErrorContext.uf_SetReturnCode(-2)
	Return False
End If

Return True


end function

public function boolean uf_utlu05ar_getsalespeople (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_salespeople, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info, &
			ls_smancode, &
			ls_smantype

SetPointer(HourGlass!)

If ads_salespeople.DataObject <> "d_salespeople" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu05ar: DataObject must be 'd_salespeople'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu05ar_get_sale_people"
ls_message = Space(70)

Do
	ls_info = Space(23201)
	ls_smancode = Space(1)
	ls_smantype = Space(1)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu05ar_get_sale_people( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ls_info, &
//											li_count, &
//											ls_smancode, &
//											ls_smantype, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_salespeople.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu05ar: Cannot import locations")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While Len(Trim(ls_smancode)) > 0 And Len(Trim(ls_smantype)) > 0

Return True

end function

public function boolean uf_utlu26ar_getproducts (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_products, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info

Double	ld_PageNumber, &
			ld_TaskNumber, &
			ld_MaxPageNumber

SetPointer(HourGlass!)

If ads_products.DataObject <> "d_products" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu26ar: DataObject must be 'd_products'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu26ar_load_productcodes"
ls_message = Space(70)

Do
	ls_info = Space(61200)
	ls_info = Fill(Char(0),61200)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu26ar( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ls_info, &
//											ld_TaskNumber, &
//											ld_PageNumber, &
//											ld_MaxPageNumber, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_products.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu26ar: Cannot import products")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While ld_PageNumber <> 0 And ld_TaskNumber <> 0 And ld_MaxPageNumber <> 0

Return True

end function

public function boolean uf_utlu18ar_getshiptocustomers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customers, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info

Double	ld_PageNumber, &
			ld_TaskNumber, &
			ld_MaxPageNumber

SetPointer(HourGlass!)

If ads_customers.DataObject <> "d_shiptocustomers" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu18ar: DataObject must be 'd_shiptocustomers'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu18ar_get_shipto_customers"
ls_message = Space(70)

Do
	ls_info = Space(19880)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu18ar_get_shipto_customers( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ld_TaskNumber, &
//											ld_PageNumber, &
//											ld_MaxPageNumber, &
//											ls_info, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_customers.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu18ar: Cannot import shipto customers")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While ld_PageNumber <> ld_MaxPageNumber

Return True

end function

public function boolean uf_utlu19ar_getbilltocustomers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customers, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info

Double	ld_PageNumber, &
			ld_TaskNumber

SetPointer(HourGlass!)

If ads_customers.DataObject <> "d_billtocustomers" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu19ar: DataObject must be 'd_billtocustomers'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu19ar_get_billto"
ls_message = Space(70)

Do
	ls_info = Space(20010)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu19ar_get_billto_customers( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ld_TaskNumber, &
//											ld_PageNumber, &
//											ls_info, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_customers.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu19ar: Cannot import billto customers")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While ld_PageNumber <> 0 And ld_TaskNumber <> 0

Return True

end function

public function boolean uf_utlu20ar_getcorporatecustomers (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customers, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info

Double	ld_PageNumber, &
			ld_TaskNumber

SetPointer(HourGlass!)

If ads_customers.DataObject <> "d_corporatecustomers" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu20ar: DataObject must be 'd_corporatecustomers'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu20ar_get_corp_customers"
ls_message = Space(70)

Do
	ls_info = Space(20010)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu20ar_get_corporate_customers( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ld_TaskNumber, &
//											ld_PageNumber, &
//											ls_info, &
//											ii_commhnd)
//
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_customers.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu20ar: Cannot import corporate customers")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While ld_PageNumber <> 0 And ld_TaskNumber <> 0

Return True

end function

public function boolean uf_utlu24ar_getcustomerdefaults (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_customerdefaults, string as_location, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info

Double	ld_PageNumber, &
			ld_TaskNumber, &
			ld_MaxPageNumber

SetPointer(HourGlass!)

If ads_customerdefaults.DataObject <> "d_customerdefaults" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu24ar: DataObject must be 'd_customerdefaults'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu24ar_get_customer_defaults"
ls_message = Space(70)

Do
	ls_info = Space(61200)
	ls_info = Fill(Char(0),61200)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu24ar_get_customer_defaults( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											as_location, &
//											ls_info, &
//											ld_TaskNumber, &
//											ld_PageNumber, &
//											ld_MaxPageNumber, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_customerdefaults.ImportString(Trim(ls_info))
	If li_rtn < 0 Then
		au_errorcontext.uf_AppendText("utl001.uf_utlu24ar: Cannot import customer defaults")
		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
		au_ErrorContext.uf_SetReturnCode(-2)
		Return False
	End If
	
Loop While ld_PageNumber <> 0 And ld_TaskNumber <> 0 And ld_MaxPageNumber <> 0

Return True

end function

public function boolean uf_utlu28ar_getskuplt (string as_objectname, string as_functionname, string as_eventname, string as_userid, ref datastore ads_salespeople, ref u_abstracterrorcontext au_errorcontext);Int 		li_rtn, &
			li_count

String	ls_appname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_info, &
			ls_RefetchProductCode, &
			ls_RefetchPlantCode

SetPointer(HourGlass!)

If ads_salespeople.DataObject <> "d_skuplt" Then
	au_ErrorContext.uf_AppendText("utl001.uf_utlu28ar: DataObject must be 'd_skuplt'")
	au_ErrorContext.uf_SetReturnCode(-1)
	Return False
End If

ls_appname = Left(GetApplication().AppName,3)
ls_procedurename = "utlu28ar_load_tskuplt"
ls_message = Space(70)

ls_RefetchProductCode = Space(10)
ls_RefetchPlantCode = Space(3)

Do
	ls_info = Space(60000)

	// Call a Netwise external function to get the required information
//	li_rtn = utlu28ar_load_tskuplt( ls_appname, &
//											as_objectname,&
//											as_functionname,&
//											as_eventname, &
//											ls_procedurename, &
//											as_userid, &
//											ls_returncode, &
//											ls_message, &
//											ls_info, &
//											ls_RefetchProductCode, &
//											ls_RefetchPlantCode, &
//											ii_commhnd)

	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	li_rtn = ads_salespeople.ImportString(Trim(ls_info))
	// For some reason, ImportString returns -9, but it's still okay anyway, so comment out the check (while we fix that)
//	If li_rtn < 0 Then
//		au_errorcontext.uf_AppendText("utl001.uf_utlu28ar: Cannot import plant product codes")
//		au_ErrorContext.uf_Set("ImportString() returned", li_rtn)
//		au_ErrorContext.uf_SetReturnCode(-2)
//		Return False
//	End If
	
Loop While Len(Trim(ls_RefetchProductCode)) > 0 And Len(Trim(ls_RefetchPlantCode)) > 0

Return True

end function

on u_utl001.create
call super::create
end on

on u_utl001.destroy
call super::destroy
end on

event constructor;call super::constructor;
/* --------------------------------------------------------
u_utl001

<OBJECT>	This object is a Netwise object for the utility
			functions. Currently it implements only
			utlu07ar (login).</OBJECT>
			
<USAGE>	Initialize this object and call functions on it
			to connect to the mainframe via Netwise.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */

end event

