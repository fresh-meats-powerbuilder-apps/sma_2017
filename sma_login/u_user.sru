HA$PBExportHeader$u_user.sru
forward
global type u_user from u_abstractdataaccess
end type
end forward

global type u_user from u_abstractdataaccess
end type
global u_user u_user

type variables
private:
u_AbstractClassFactory	iu_ClassFactory
u_AbstractDefaultManager	iu_DefaultManager

String	is_UserID, &
	is_Password, &
	is_GroupID
end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_retrieve (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_update (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_validate (ref string as_userid, ref string as_password, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_changepassword (string as_userid, string as_password, string as_newpassword, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_retrieve (ref string as_userid, ref string as_password)
public function boolean uf_update (string as_userid, string as_password, u_abstracterrorcontext au_errorcontext)
public subroutine uf_setgroupid (string as_groupid)
public function string uf_getgroupid (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getsaleslocation (ref string as_location, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory);
/* --------------------------------------------------------

<DESC>	This method is not implemented.</DESC>

<ARGS>	au_classfactory: ClassFactory</ARGS>
			
<USAGE>	Call the other uf_initialize() method
			with the error context object argument.</USAGE>
-------------------------------------------------------- */
return false
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the User information. Obtain the
			default User ID from persistent storage
			(u_inidefaultmanager).</DESC>

<ARGS>	au_classfactory: ClassFactory
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function after obtaining the object
			from the ClassFactory.  This function returns
			true on success, false otherwise.</USAGE>
-------------------------------------------------------- */
Boolean	lb_initialized

iu_classfactory = au_classfactory

lb_initialized = iu_classfactory.uf_GetObject("u_inidefaultmanager", iu_defaultmanager, au_errorcontext)
If Not au_ErrorContext.uf_IsSuccessful() Then
	Return False
End If

// We initialize it no matter what
If Not iu_defaultmanager.uf_Initialize(iu_classfactory, "ibpuser.ini", "User Info", au_errorcontext) Then
	// Error initializing default manager, but that's okay.
	// Reset error context and return True
	au_errorcontext.uf_Initialize()
	Return True
End If

If Not iu_defaultmanager.uf_GetData("UserID", is_UserID, au_errorcontext) Then
	// Error getting User ID, but that's okay.
	// Reset error context and return True
	au_errorcontext.uf_Initialize()
	Return True
End If

If Not iu_defaultmanager.uf_GetData("UserPassword", is_Password, au_errorcontext) Then
	// Error getting password, but that's okay.
	// Reset error context and return True
	au_errorcontext.uf_Initialize()
	Return True
End If

au_errorcontext.uf_Initialize()
return True
end function

public function boolean uf_retrieve (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Retrieve the user id and password for
			user interface purposes.</DESC>

<ARGS>	au_stack: ParameterStack to hold the user ID and password.
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	This function pushes the User ID and
			password onto the parameter stack in
			that order. It always returns true.</USAGE>
-------------------------------------------------------- */
au_stack.uf_Push("string", is_userid)
au_stack.uf_Push("string", is_password)

return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_stack, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Update the user id and password for
			user interface purposes.</DESC>

<ARGS>	au_stack: ParameterStack to hold the user ID and password.
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	This function pops the User ID and
			password off of the parameter stack in
			that order. It returns false if the stack
			is not in the correct state or if another
			error occurs.</USAGE>
-------------------------------------------------------- */
Boolean	lb_success

lb_success = au_stack.uf_Pop("string",is_Password)
If Not lb_success Then Return False

lb_success = au_stack.uf_Pop("string",is_UserID)
If Not lb_success Then Return False

lb_success = iu_defaultmanager.uf_SetData("UserID",is_UserID,au_errorcontext)
If Not lb_success Then Return False

return True
end function

public function boolean uf_validate (ref string as_userid, ref string as_password, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Validate the User ID and password.</DESC>

<ARGS>	as_userid: Mainframe UserID for WBSetSecurity call
			as_password: Password for WBSetSecurity call
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to validate the User ID
			and password. This function returns true on
			a successful validation, false otherwise. If
			an error occurs, the error context message text
			contains the reason for the error.</USAGE>
-------------------------------------------------------- */
u_smas01sr lu_smas01sr
Boolean lb_successful
Integer li_returncode
String	ls_action

String 	ls_app, &
			ls_windowname, &
			ls_functionname, &
			ls_eventname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_namepassword

ls_app = "SMA" 
ls_windowname = "u_user" 
ls_functionname = "uf_validate" 
ls_eventname = ""  
ls_procedurename = ""  
ls_returncode = "" 
ls_message = Space(70)
ls_namepassword = as_userid + "~t" + as_password + "~r~n"

ls_action = 'V'

lb_successful = True
If Not iu_classfactory.uf_GetObject("u_smas01sr",lu_smas01sr,au_errorcontext) Then
	lb_successful = lu_smas01sr.uf_initialize(iu_classfactory,"", "",au_errorcontext)
End If

If Not lb_successful Then
	// Can't initialize smas01sr object
	// Populate error context and return false
	au_errorcontext.uf_AppendText("Can't initialize smas01sr object")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
Else
	li_returncode = lu_smas01sr.uf_utlu07er_getsecurity(ls_app, ls_windowname, ls_functionname, ls_eventname, &
		ls_procedurename, as_userid, as_password, ls_returncode, ls_message, ls_namepassword, ls_action, au_errorcontext)
	
//	If Not lu_smas01sr.uf_checkrpcerror(au_errorcontext) Then	
//		// Some kind of error
//		// Populate error context and return False
//		au_errorcontext.uf_AppendText("An error occurred in communication with the mainframe")
//		au_errorcontext.uf_SetReturnCode(-1)	
//		Return False
//	End If
End If

// Success! (on the call)

Choose Case ls_action
	Case "G"
		is_UserID = Upper(as_userid)
		is_Password = Upper(as_password)
		iu_defaultmanager.uf_SetData("UserID",is_UserID,au_errorcontext)
		Return True
	Case Else
		au_errorcontext.uf_Set("action",ls_action)		
		Return False
End Choose

end function

public function boolean uf_changepassword (string as_userid, string as_password, string as_newpassword, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Change the user's password.</DESC>

<ARGS>	as_userid: Mainframe UserID
			as_password: Password
			as_newpassword: New password
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to change the user's
			password. This function returns true on
			success, false otherwise. If
			an error occurs, the error context message text
			contains the reason for the error.</USAGE>
-------------------------------------------------------- */
u_smas01sr lu_smas01sr
Boolean lb_successful
Integer li_returncode
String	ls_action

String 	ls_app, &
			ls_windowname, &
			ls_functionname, &
			ls_eventname, &
			ls_procedurename, &
			ls_returncode, &
			ls_message, &
			ls_namepassword

ls_app = "SMA" 
ls_windowname = "u_user" 
ls_functionname = "uf_ChangePassword" 
ls_eventname = ""  
ls_procedurename = ""  
ls_returncode = "" 
ls_message = Space(70)
ls_namepassword = as_userid + "~t" + as_password + "~t" + as_newpassword + "~r~n"

ls_action = 'M'

lb_successful = True
If Not iu_classfactory.uf_GetObject("u_smas01sr",lu_smas01sr,au_errorcontext) Then
	lb_successful = lu_smas01sr.uf_initialize(iu_classfactory,"", "",au_errorcontext)
End If

If Not lb_successful Then
	// Can't initialize smas01sr object
	// Populate error context and return false
	au_errorcontext.uf_AppendText("Can't initialize smas01sr object")
	au_errorcontext.uf_SetReturnCode(-1)
	Return False
Else
	li_returncode = lu_smas01sr.uf_utlu07er_getsecurity(ls_app, ls_windowname, ls_functionname, ls_eventname, &
		ls_procedurename, as_userid, as_password, ls_returncode, ls_message, ls_namepassword, ls_action, au_errorcontext)
	
//	If Not lu_smas01sr.uf_checkrpcerror(au_errorcontext) Then	
//		// Some kind of error
//		// Populate error context and return False
//		au_errorcontext.uf_AppendText("Some RPC error occurred")
//		au_errorcontext.uf_SetReturnCode(-1)	
//		Return False
//	End If
End If

// Success! (on the call)

Choose Case ls_action
	Case "G"
		is_UserID = as_userid
		is_Password = as_newpassword
		iu_defaultmanager.uf_SetData("UserID",is_UserID,au_errorcontext)
		Return True
	Case Else
		au_errorcontext.uf_Set("action",ls_action)		
		Return False
End Choose

end function

public function boolean uf_retrieve (ref string as_userid, ref string as_password);
/* --------------------------------------------------------

<DESC>	Retrieve the user id and password for
			user interface purposes.</DESC>

<ARGS>	as_userid: User ID
			as_password: Password</ARGS>
			
<USAGE>	This function gets the User ID and
			password. It always returns true.</USAGE>
-------------------------------------------------------- */
as_userid = is_userid
as_password = is_password

return True
end function

public function boolean uf_update (string as_userid, string as_password, u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Update the user id and password for
			user interface purposes.</DESC>

<ARGS>	as_userid:  UserID
			as_password: Password
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	This function sets the default UserID from
			the arguments passed in. It returns false if an
			error occurs.</USAGE>
-------------------------------------------------------- */
Boolean	lb_success

is_Password = as_Password
is_UserID = as_UserID

lb_success = iu_defaultmanager.uf_SetData("UserID", is_UserID, au_errorcontext)
If Not lb_success Then Return False

return True
end function

public subroutine uf_setgroupid (string as_groupid);
/* --------------------------------------------------------
uf_SetGroupID

<DESC> Set the user's Group ID.  This is the ID stored in 
		TUTLPROF for this application.
</DESC>

<ARGS> as_groupid: Group ID of the user to store
</ARGS>

<USAGE> This function will set the current user's Group ID
			This function is currently called by u_NetwiseSecurityManager.
			If you do not use that object to set your menu permissions
			you must set the group ID yourself.
</USAGE>
-------------------------------------------------------- */
is_groupid = as_GroupID
end subroutine

public function string uf_getgroupid (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_GetGroupID

<DESC> Return the user's Group ID.  This is the ID stored in 
		TUTLPROF for this application.
</DESC>

<USAGE> This function will return the current user's Group ID
			This value is populated from a Netwise call to
			utlu00ar the first time this function is called.
			If an error occurs, the function returns an empty
			string.
</USAGE>
-------------------------------------------------------- */
u_utl001	lu_utl001

If Len(Trim(is_groupid)) > 0 Then Return is_groupid

If Not iu_ClassFactory.uf_GetObject("u_utl001", lu_Utl001, au_ErrorContext) Then
	If Not au_ErrorContext.uf_IsSuccessful() Then return ""
	If Not lu_utl001.uf_Initialize(iu_ClassFactory, is_userid, is_password, au_ErrorContext) Then Return ""
End if
//
//lu_utl001.uf_utlu00ar_GetSecurityCode("u_User", &
//													"uf_GetGroupID", &
//													"", &
//													is_userid, &
//													is_GroupID, &
//													au_ErrorContext)
//													
If Not au_ErrorContext.uf_IsSuccessful() Then return ""

return is_groupid
end function

public function boolean uf_getsaleslocation (ref string as_location, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Retrieve the user's sales location.</DESC>

<ARGS>	as_location: sales location by reference
			au_errorcontext: Error context</ARGS>
			
<USAGE>	This function gets the user's sales location
			currently from the u_pblocaldb object.
			On error, it returns false.  The error
			context object is populated with information
			about any errors that occurred.</USAGE>
-------------------------------------------------------- */
u_ODBCTransaction	lu_pblocaldb
Boolean	lb_Success
String	ls_smanloc
String	ls_UserID

au_ErrorContext.uf_Initialize()

If Not iu_ClassFactory.uf_GetObject("u_pblocaldb", lu_pblocaldb, au_ErrorContext) Then
	If au_ErrorContext.uf_IsSuccessful() Then
		lb_Success = iu_defaultmanager.uf_Initialize(iu_ClassFactory,"ibpuser.ini","SMA DATABASE",au_ErrorContext)
		If lb_Success Then
			lb_Success = lu_pblocaldb.uf_Initialize(iu_ClassFactory, iu_defaultmanager, au_ErrorContext)
		End If
	End If
End If

ls_UserID = Trim(is_userid) + Space(8 - Len(Trim(is_userid)))

SELECT smanloc INTO :as_location
FROM salesman
WHERE userid = :ls_userid
USING lu_pblocaldb;

If lu_pblocaldb.SQLCode <> 0 Then
	as_location = ""
	au_ErrorContext.uf_AppendText(lu_pblocaldb.SQLErrText)
	Return False
End If

Return True
end function

on u_user.create
call super::create
end on

on u_user.destroy
call super::destroy
end on

event constructor;
/* --------------------------------------------------------
u_user

<OBJECT>	This object encapsulates functionality related
			to the mainframe representation of the user.</OBJECT>
			
<USAGE>	Obtain this object from the ClassFactory and
			use it to keep the UserID and password around.
			Call uf_validate() to validate the login.
			Call uf_ChangePassword() to change the user's
			password.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */
end event

