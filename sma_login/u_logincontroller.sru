HA$PBExportHeader$u_logincontroller.sru
forward
global type u_logincontroller from u_abstractcontroller
end type
end forward

global type u_logincontroller from u_abstractcontroller
end type
global u_logincontroller u_logincontroller

type variables
Private:

u_ClassFactory		iu_ClassFactory
u_NotificationController	iu_NotificationController
u_user			iu_User
end variables

forward prototypes
public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_getdata (ref string as_userid, ref string as_password, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setdata (string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_validate (string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext, ref string as_action)
public function boolean uf_changepassword (string as_userid, string as_password, string as_newpassword, ref u_abstracterrorcontext au_errorcontext, ref string as_action)
end prototypes

public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize and start up the login controller.</DESC>

<ARGS>	au_classfactory: ClassFactory
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to start the login processing.
			A window (w_wls_login) will be opened to provide
			the user interface for the login. This function
			returns true on success, false on failure.  If
			this function returns false, the error context
			message text contains information related to the
			error.</USAGE>
-------------------------------------------------------- */

u_ParameterStack lu_ParameterStack
Boolean	lb_Cancelled 
	
w_abstractframe lw_frame

iu_ClassFactory = au_ClassFactory

If Not iu_ClassFactory.uf_GetObject( 'u_ParameterStack', lu_ParameterStack, au_ErrorContext ) Then
	If Not au_ErrorContext.uf_IsSuccessful( ) Then
		au_ErrorContext.uf_AppendText( "Error Creating Local Parameter Stack" )
		au_ErrorContext.uf_SetReturnCode( -1 )
		Return False
	Else
		lu_ParameterStack.uf_Initialize( )
	End If
End If

If Not iu_ClassFactory.uf_GetObject( 'u_user', iu_user, au_ErrorContext ) Then
	If Not au_ErrorContext.uf_IsSuccessful( ) Then
		au_ErrorContext.uf_AppendText( "Error creating user info object" )
		au_ErrorContext.uf_SetReturnCode( -1 )
		Return False
	Else
		If Not iu_user.uf_Initialize(iu_classfactory, au_errorcontext) Then
			au_ErrorContext.uf_AppendText("Couldn't initialize user info object")
		End If
	End If
End If

lu_ParameterStack.uf_Push( 'u_ErrorContext', au_ErrorContext )
lu_ParameterStack.uf_Push( 'u_logincontroller', This )

iu_ClassFactory.uf_GetResponseWindow( 'w_Login', lu_ParameterStack )

lu_ParameterStack.uf_Pop( 'u_ErrorContext', au_ErrorContext )

If au_ErrorContext.uf_GetReturnCode() < 0 Then

	// We only do this if the frame window has been initialized already
	If iu_ClassFactory.uf_GetObject("window", lw_frame, au_ErrorContext) Then
		If au_ErrorContext.uf_IsSuccessful() Then
			lw_frame.ib_NoPromptOnExit = True
		End If
	End If

	au_ErrorContext.uf_AppendText( "Error Returned from w_login" )
	au_ErrorContext.uf_SetReturnCode( -1 )
	Return False
End If

If au_ErrorContext.uf_GetReturnCode() > 0 Then
	// This is just a warning-type message.  Close the frame.
	au_ErrorContext.uf_Initialize()
	
	// We only do this if the frame window has been initialized already
	If iu_ClassFactory.uf_GetObject("window", lw_frame, au_ErrorContext) Then
		If au_ErrorContext.uf_IsSuccessful() Then
			lw_frame.ib_NoPromptOnExit = True
		End If
	End If
	
	au_ErrorContext.uf_Initialize()
	Return False
End If

Return True

end function

public function boolean uf_getdata (ref string as_userid, ref string as_password, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Obtain the default User ID and password for
			the login user interface.</DESC>

<ARGS>	as_userid: User ID by reference
			as_password: Password by reference
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	The user interface calls this function to obtain
			the User ID and password.  The login
			controller passes the request on to the 
			aggregated u_user object. This function returns
			true on success, false on failure.</USAGE>
-------------------------------------------------------- */

Boolean	lb_success
u_abstractparameterstack	lu_stack

If Not iu_classfactory.uf_GetObject('u_parameterstack',lu_stack, au_errorcontext) Then
	lb_success = lu_stack.uf_Initialize()
	If Not lb_success Then Return False
End If

lb_success = iu_user.uf_retrieve(lu_stack, au_errorcontext)
If Not lb_success Then Return False

lb_success = lu_stack.uf_Pop("string",as_Password)
If Not lb_success Then Return False
lb_success = lu_stack.uf_pop("string",as_UserID)
If Not lb_success Then Return False

Return True
end function

public function boolean uf_setdata (string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set the default User ID and password for
			the login user interface.</DESC>

<ARGS>	as_userid: User ID
			as_password: Password
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	The user interface calls this function to set
			the default User ID and password.  The login
			controller passes the request on to the 
			aggregated u_user object. This function returns
			true on success, false on failure.</USAGE>
-------------------------------------------------------- */
Boolean	lb_success
u_abstractparameterstack	lu_stack

lb_success = iu_classfactory.uf_GetObject('u_parameterstack',lu_stack)
If Not lb_success Then Return False
lb_success = lu_stack.uf_Initialize()
If Not lb_success Then Return False

lb_success = lu_stack.uf_Push("string",as_UserID)
If Not lb_success Then Return False
lb_success = lu_stack.uf_Push("string",as_Password)
If Not lb_success Then Return False

lb_success = iu_user.uf_Update(lu_stack, au_errorcontext)
If Not lb_success Then Return False

Return True
end function

public function boolean uf_validate (string as_userid, string as_password, ref u_abstracterrorcontext au_errorcontext, ref string as_action);
/* --------------------------------------------------------

<DESC>	Validate the UserID/password.</DESC>

<ARGS>	as_userid: User ID
			as_password: Password
			au_errorcontext: ErrorContext
			as_action: Action code by reference</ARGS>
			
<USAGE>	This function is called by the login
			window.  It passes off validation to the
			u_user object aggregated by the login
			controller, and interprets the action codes
			returned from that object's validation.
			This function returns true on a successful
			validation and false otherwise.  If this
			function returns true, the as_action action
			code contains "X" if the password is expired.
			Otherwise, some kind of error occurred and
			the information is available in the error
			context object.</USAGE>
-------------------------------------------------------- */
Boolean	lb_success
String	ls_newpassword

lb_success = iu_user.uf_validate(as_userid, as_password, au_ErrorContext)
If lb_success Then Return True

au_errorcontext.uf_Get("action",as_action)

Choose Case as_action
	Case "G"
		// This should have been handled above by lb_success
		Return True
	Case "A"
		// Security violation
		au_errorcontext.uf_AppendText("You don't have access to this CICS region.")
		au_errorcontext.uf_SetReturnCode(-1)
		Return False
	Case "X"
		// Password expired
		as_action = "X"
		Return False
	Case "I"
		// Invalid password
		au_errorcontext.uf_AppendText("Invalid Password/User ID.")
		as_action = "I"
		Return False
	Case "S"
		// Password suspended
		au_errorcontext.uf_AppendText("Your password has been suspended.")
		Return False
	Case Else
		// Some other error
		au_errorcontext.uf_AppendText("An error occurred attempting to validate your password/User ID.")
		au_errorcontext.uf_SetReturnCode(-1)
		Return False
End Choose

Return False
end function

public function boolean uf_changepassword (string as_userid, string as_password, string as_newpassword, ref u_abstracterrorcontext au_errorcontext, ref string as_action);
/* --------------------------------------------------------

<DESC>	Change the user's password.</DESC>

<ARGS>	as_userid: User ID
			as_password: Password
			as_newpassword: New Password
			au_errorcontext: ErrorContext
			as_action: Action code by reference</ARGS>
			
<USAGE>	This function is called by the login
			window, and is very similar to the
			uf_validate() function.  
			This function returns true on success.
			Otherwise, some kind of error occurred and
			the information about the error is 
			available in the error
			context object.</USAGE>
-------------------------------------------------------- */
Boolean	lb_success
String	ls_newpassword

lb_success = iu_user.uf_ChangePassword(as_userid, as_password, as_newpassword, au_ErrorContext)
If lb_success Then Return True

au_errorcontext.uf_Get("action",as_action)

Choose Case as_action
	Case "G"
		// This should have been handled above by lb_success
		Return True
	Case "A"
		// Security violation
		au_errorcontext.uf_AppendText("You don't have access to this CICS region.")
		au_errorcontext.uf_SetReturnCode(-1)
		Return False
	Case "X"
		// Password expired
		as_action = "X"
		Return True
	Case "S"
		// Password suspended
		au_errorcontext.uf_AppendText("Your password has been suspended.")
		au_errorcontext.uf_SetReturnCode(-1)
		Return False
	Case Else
		// Some other error
		au_errorcontext.uf_AppendText("An error occurred in communication with the mainframe.")
		au_errorcontext.uf_AppendText("Action is: " + as_action)
		au_errorcontext.uf_SetReturnCode(-1)
		Return False
End Choose

Return False
end function

on u_logincontroller.create
TriggerEvent( this, "constructor" )
end on

on u_logincontroller.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;/* --------------------------------------------------------
u_logincontroller

<OBJECT>	This object handles the process of logging in
			to the mainframe.  It knows how to validate users
			and change their passwords. It enlists the service
			of a u_user object to contain user information such
			as UserID and password as well as the functionality
			to validate the UserID.</OBJECT>
			
<USAGE>	Call uf_initialize().  Sit back and watch it work.
			</USAGE>

<AUTH>	Conrad Engel	</AUTH>

<ALSO>	u_user </ALSO>
--------------------------------------------------------- */
end event

