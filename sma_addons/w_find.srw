HA$PBExportHeader$w_find.srw
forward
global type w_find from w_abstractpopup
end type
type cb_ok from commandbutton within w_find
end type
type cb_cancel from commandbutton within w_find
end type
type cb_help from commandbutton within w_find
end type
type dw_find from datawindow within w_find
end type
end forward

global type w_find from w_abstractpopup
int Width=2011
int Height=508
boolean TitleBar=true
string Title="Find"
cb_ok cb_ok
cb_cancel cb_cancel
cb_help cb_help
dw_find dw_find
end type
global w_find w_find

type variables
Private:
DataWindow		idw_Data
u_DWEdit		iu_Edit
end variables

forward prototypes
public function boolean wf_initialize (u_abstractclassfactory au_classfactory, u_dwedit au_edit, datawindow adw_data, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean wf_initialize (u_abstractclassfactory au_classfactory, u_dwedit au_edit, datawindow adw_data, ref u_abstracterrorcontext au_errorcontext);DataWindowChild	ldwc_Columns

Long		ll_ColumnCount, &
			ll_Counter
			
String	ls_Import, &
			ls_ColumnName, &
			ls_Temp

u_string_functions	lu_string


idw_data = adw_Data
iu_edit = au_Edit


ll_ColumnCount = Long(idw_Data.Object.DataWindow.Column.Count)
For ll_Counter = 1 to ll_ColumnCount
	ls_Import += String(ll_Counter) + '~t'
	ls_ColumnName = idw_Data.Describe('#' + String(ll_Counter) + '.Name')
	ls_Temp = idw_Data.Describe(ls_ColumnName + "_t" + ".Text")
	If ls_Temp <> "!" Then
		// Remove all ~r~n and ~t
		lu_String.nf_GlobalReplace(ls_Temp, '~r~n', ' ')
		lu_String.nf_GlobalReplace(ls_Temp, '~t', ' ')
		ls_Import += ls_Temp 
	Else
		ls_Import += ls_ColumnName
	End if
	ls_Import += '~t' + idw_Data.Describe('#' + String(ll_Counter) + '.ColType')
	ls_Import += '~r~n'
Next

dw_find.GetChild("columnnames", ldwc_Columns)
ldwc_Columns.ImportString(ls_Import)



Return True
end function

on w_find.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.cb_cancel=create cb_cancel
this.cb_help=create cb_help
this.dw_find=create dw_find
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_help
this.Control[iCurrent+4]=this.dw_find
end on

on w_find.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.cb_cancel)
destroy(this.cb_help)
destroy(this.dw_find)
end on

type cb_ok from commandbutton within w_find
int X=1614
int Y=24
int Width=352
int Height=108
int TabOrder=10
boolean BringToTop=true
string Text="&Find Next"
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Boolean	lb_Wrap, &
			lb_StartAtBegining
DataWindowChild	ldwc_Columns
Long		ll_Ret
String	ls_Find, &
			ls_ColType, &
			ls_ColName, &
			ls_Method, &
			ls_Wrap, &
			ls_StartAtBegining



dw_find.GetChild("columnNames", ldwc_Columns)
ls_ColName = ldwc_Columns.GetItemString(ldwc_Columns.GetRow(), "name")
ls_ColType = Lower(Left(ldwc_Columns.GetItemString(ldwc_Columns.GetRow(), "ColType"), 4))

ls_Method = dw_find.GetItemString(1, 'FindMethod')
ls_Wrap = dw_find.GetItemString(1, 'wrap')
If ls_Wrap = 'T' Then
	lb_Wrap = True
Else 
	lb_Wrap = False
End if

ls_StartAtBegining = dw_find.GetItemString(1, 'startatbegining')
If ls_StartAtBegining = 'T' Then
	lb_StartAtBegining = True
Else
	lb_StartAtBegining = False
End if


ls_Find = ls_ColName + " = " 
If ls_ColType = 'char' Then
	ls_Find += "'"
End if

ls_Find += dw_find.GetItemString(1, "FindText") 
If ls_ColType = 'char' Then
	ls_Find += "'"
End if

ll_Ret = iu_edit.uf_Find(ls_Find, Integer(ls_Method), lb_wrap, lb_StartAtbegining)

If ll_Ret < 1 Then
	MessageBox("Find", "No Rows Found")
	return
End if

idw_data.SetColumn(ls_ColName)
idw_Data.SetRow(ll_Ret)
idw_Data.SelectText(1, 10000)
idw_Data.SetFocus()


end event

type cb_cancel from commandbutton within w_find
int X=1614
int Y=144
int Width=352
int Height=108
int TabOrder=20
boolean BringToTop=true
string Text="&Close"
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Close(Parent)
end event

type cb_help from commandbutton within w_find
int X=1614
int Y=264
int Width=352
int Height=108
int TabOrder=30
boolean BringToTop=true
string Text="&Help"
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_find from datawindow within w_find
int X=27
int Y=32
int Width=1445
int Height=376
int TabOrder=20
boolean BringToTop=true
string DataObject="d_findreplace"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;This.InsertRow(0)
end event

