HA$PBExportHeader$u_odbctransaction.sru
forward
global type u_odbctransaction from u_abstracttransaction
end type
end forward

global type u_odbctransaction from u_abstracttransaction
end type
global u_odbctransaction u_odbctransaction

type variables
u_AbstractErrorContext	iu_ErrorContext
end variables

forward prototypes
public function boolean uf_disconnect (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_connect (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstractdefaultmanager au_defaultmanager, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_disconnect (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	DisConnect an ODBC Datasource 
			</DESC>

<ARGS>	au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	This function Should be called before the Destructor.
			If this function returns False, the error 
			context text contains the reason for the failure.
			</USAGE>
-------------------------------------------------------- */
DisConnect Using This;
IF this.SQLCode <> 0 Then
	au_errorcontext.uf_AppendText(String(This.SQLDBCode)+" - "+This.SQLErrText)
	Return False
END IF
Return True
end function

public function boolean uf_connect (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Connect to ODBC Datasource using parms read from
			u_iniDefaultManger in the initialize
			</DESC>

<ARGS>	au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	This function is called from the initialize.
			If this function returns False, the error 
			context text contains the reason for the failure.
			</USAGE>
-------------------------------------------------------- */
Connect Using This;
IF this.SQLCode <> 0 Then
	au_errorcontext.uf_AppendText(String(This.SQLDBCode)+" - "+This.SQLErrText)
	Return False
END IF

// ** IBDKEEM ** 03/11/2002 ** If Connection is successful the Clear Errors
au_errorcontext.uf_initialize( )
// ** END **

Return True
end function

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstractdefaultmanager au_defaultmanager, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the ODBC DataSource
			</DESC>

<ARGS>	au_classfactory: ClassFactory
			au_DefaultManager: DefaultManager
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function after getting the descendant
			object from the ClassFactory.  If this function
			then returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
Boolean	lb_AutoCommit,&
			lb_FileAccess
			
String	ls_DataBase,&
			ls_DBMS,&
			ls_DBParm,&
			ls_DBPass,&
			ls_Lock,&
			ls_LogID,&
			ls_LogPass,&
			ls_ServerName,&
			ls_UserID
			
// ** IBDKEEM ** 03/11/2002 ** Made SQL Server Compatable
IF Not au_Defaultmanager.uf_getdata ( "AutoCommit", lb_AutoCommit, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
ELSE
	This.AutoCommit = lb_AutoCommit
END IF

IF Not au_Defaultmanager.uf_getdata ( "DBPass", ls_DBPass, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
ELSE
	This.DBPass = ls_DbPass	
END IF

IF Not au_Defaultmanager.uf_getdata ( "Lock", ls_Lock, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
ELSE
	This.Lock = ls_Lock	
END IF

IF Not au_Defaultmanager.uf_getdata ( "UserID", ls_UserID, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
ELSE
	This.USerID = ls_UserID	
END IF

IF Not au_Defaultmanager.uf_getdata ( "DataBase", ls_DataBase, au_errorcontext)  then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
END IF

IF Not au_Defaultmanager.uf_getdata ( "DBMS", ls_DBMS, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
END IF

IF Not au_Defaultmanager.uf_getdata ( "DBParm", ls_DBParm, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
END IF

IF Not au_Defaultmanager.uf_getdata ( "LogID", ls_LogID, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
END IF

IF Not au_Defaultmanager.uf_getdata ( "LogPass", ls_LogPass, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
END IF

IF Not au_Defaultmanager.uf_getdata ( "ServerName", ls_ServerName, au_errorcontext) then 	
	au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
	IF lb_FileAccess Then return False
END IF

This.DBMS = ls_DBMS
This.DataBase = ls_DataBase
This.DBParm = ls_DbParm
	
//if Upper(ls_DBMS) = Upper("SNC SQL Native Client") Then	 // Connecting to a SQL Server 
	This.ServerName = ls_ServerName	// SQL Database Server Name
	This.LogID = ls_LogID			 	// SQL User name
	This.LogPass = ls_LogPass		 	// SQL Password
//End If

IF Not This.uf_Connect(au_errorcontext) then return False

Return True

/////////////////////////////////////////////////////////////////////////////////////////
//IF Not au_Defaultmanager.uf_getdata ( "AutoCommit", lb_AutoCommit, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.AutoCommit = lb_AutoCommit
//
//IF Not au_Defaultmanager.uf_getdata ( "DataBase", ls_DataBase, au_errorcontext)  then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.DataBase = ls_DataBase
//
//IF Not au_Defaultmanager.uf_getdata ( "DBMS", ls_DBMS, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.DBMS = ls_DBMS
//
//IF Not au_Defaultmanager.uf_getdata ( "DBParm", ls_DBParm, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.DBParm = ls_DbParm
//
//IF Not au_Defaultmanager.uf_getdata ( "DBPass", ls_DBPass, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.DBPass = ls_DbPass
//
//IF Not au_Defaultmanager.uf_getdata ( "Lock", ls_Lock, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.Lock = ls_Lock
//
//IF Not au_Defaultmanager.uf_getdata ( "LogID", ls_LogID, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.LogID = ls_LogID
//
//IF Not au_Defaultmanager.uf_getdata ( "LogPass", ls_LogPass, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.LogPass = ls_LogPass
//
//IF Not au_Defaultmanager.uf_getdata ( "ServerName", ls_ServerName, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.ServerName = ls_ServerName
//
//IF Not au_Defaultmanager.uf_getdata ( "UserID", ls_UserID, au_errorcontext) then 	
//		au_errorcontext.uf_Get("INI File Access",lb_FileAccess)
//		IF lb_FileAccess Then return False
//END IF
//This.USerID = ls_UserID
//
//IF Not This.uf_Connect(au_errorcontext) then return False
//
//Return True
// ** END ** IBDKEEM ** 03/11/2002 ** 
/////////////////////////////////////////////////////////////////////////////////////////
end function

on u_odbctransaction.create
call super::create
end on

on u_odbctransaction.destroy
call super::destroy
end on

event constructor;/* --------------------------------------------------------
u_ODBCTransAction

<OBJECT>	This object is a utility object for connecting
			to an ODBC DataSource. It reads from a u_inidefaultsmanager
			to figure out the ODBC PARAMETERS. IF you do not use 
			u_INIDefaultManager you must represent severe error by
			calling au_ErrorContext.uf_Set("INI File Access",TRUE) in 
			your default manager on the GET functions.
			</OBJECT>
			
<USAGE>	In an application, get your inherited
			object from the ClassFactory and call
			uf_Initialize(). The Connect and Disconnect  are done
			automatically

<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
Return 0

end event

