HA$PBExportHeader$u_dweditcontroller.sru
forward
global type u_dweditcontroller from u_abstractcontroller
end type
end forward

global type u_dweditcontroller from u_abstractcontroller
end type
global u_dweditcontroller u_dweditcontroller

type variables
Private:
u_AbstractClassFactory	iu_ClassFactory
u_dwEdit			iu_DWEdit
end variables

forward prototypes
public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_initialize (u_abstractclassfactory au_classfactory, datawindow adw_data, string as_type, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize

<DESC> This is the wrong initialize to use for this obejct. 
			
</DESC>

<ARGS>    au_classfactory: ClassFactory
			 au_errorcontext: ErrorContext
</ARGS>

<USAGE> Don't use this one, use the other uf_Initialize
</USAGE>
-------------------------------------------------------- */
au_errorcontext.uf_AppendText("This is the wrong function, please call the other initialize")
au_errorcontext.uf_SetReturnCode(-1)
Return False


end function

public function boolean uf_initialize (u_abstractclassfactory au_classfactory, datawindow adw_data, string as_type, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize

<DESC> Initialize the Edit Controller and open the appropriate
			visual and non-visual controls
</DESC>

<ARGS>	au_classfactory: ClassFactory
			 adw_data: DataWindow that all actions will be performed on
			 as_type: Type of service to initialize Currently supported values are 'find' or 'replace'
			 au_errorcontext: ErrorContext
</ARGS>

<USAGE> Call this function to create the appropriate dialogue 
			boxes.  
</USAGE>
-------------------------------------------------------- */
u_DWEdit lu_Edit
w_Find	lw_Find



au_ClassFactory.uf_GetObject("u_dwedit", lu_Edit, au_ErrorContext)
If Not au_ErrorContext.uf_IsSuccessful() Then Return False
lu_edit.uf_Initialize(adw_Data)

Choose Case Lower(as_Type)
	Case "find"
		//This must be declared in the object list or else it will try to open a sheet
		au_ClassFactory.uf_GetObject("w_find", lw_Find, au_ErrorContext)
		If Not au_ErrorContext.uf_IsSuccessful() Then Return False
		lw_Find.wf_Initialize(au_classfactory, lu_Edit, adw_data, au_errorcontext)
	Case "replace"
		//This must be declared in the object list or else it will try to open a sheet
		au_ClassFactory.uf_GetObject("w_replace", lw_Find, au_ErrorContext)
		If Not au_ErrorContext.uf_IsSuccessful() Then Return False
		lw_Find.wf_Initialize(au_classfactory, lu_Edit, adw_data, au_errorcontext)
End Choose

Return True
end function

on u_dweditcontroller.create
TriggerEvent( this, "constructor" )
end on

on u_dweditcontroller.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_dweditcontroller

<OBJECT> This is the controller for tying all of the visual
			and non-visual objects together on a standard 
			Windows Edit Menu.  This includes Find, Replace currently.
</OBJECT>

<USAGE> Instantiate this object. Then call uf_initialize with the 
			appropriate parameters.  This object will then create the
			correct non visual objects and open the correct visual objects.
			It will also initialize all objects it creates.
</USAGE>

<AUTH> Tim Bornholtz </AUTH>
--------------------------------------------------------- */


end event

