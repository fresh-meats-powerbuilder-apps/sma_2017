HA$PBExportHeader$u_appsetup.sru
forward
global type u_appsetup from nonvisualobject
end type
end forward

global type u_appsetup from nonvisualobject
end type
global u_appsetup u_appsetup

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);String	ls_JustPath, &
			ls_JustFileName, &
			ls_App_Path, &
			ls_filepath, &
			ls_registrystring, &
			ls_installed, &
			ls_subkeylist[], &
			ls_dec_reg_val
			
Integer	li_LoopCount, &
			li_pos, &
			li_rtn, &
			li_counter, &
			li_numberofusers
			
Environment	lu_env
u_win32api lu_sdkcalls
lu_sdkcalls = Create u_win32api

// Get the full path and name of the module that started us
// This is either the EXE or the PB development environment
ls_FilePath = lu_sdkcalls.uf_getmodulefilename()
li_LoopCount = 0
// Find The Last "\"
Do
	li_LoopCount = Pos(ls_FilePath, "\", li_LoopCount + 1)
	If li_LoopCount > 0 Then li_pos = li_LoopCount
Loop While li_LoopCount > 0

//Get just The Path
ls_JustPath = Left(ls_FilePath, li_pos - 1)
//Get Just The EXE name
ls_JustFileName = Mid(ls_FilePath, li_pos + 1)

// Not running through PowerBuilder
IF Pos(Lower(ls_JustFileName), "pb") > 0 Then
	ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\" + ls_JustFileName
	li_rtn = RegistryGet(ls_RegistryString, "Path", ls_App_Path)
	IF li_rtn < 0 OR  Len(Trim(ls_App_Path)) < 1  Then
		//Application has never been run before
		li_rtn = RegistrySet(ls_RegistryString, "", ls_FilePath)
		ls_App_Path="c:\Sqlany50\win32;" + ls_JustPath
		li_rtn = RegistrySet(ls_RegistryString, "Path", ls_App_Path)		
	END IF
ELSE
	Return True
END IF

ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers"
li_rtn = RegistryGet(ls_RegistryString, "Sybase SQL Anywhere 5.0", ls_Installed)
If li_rtn < 0 or ls_Installed <> "Installed" Then
	RegistrySet(ls_RegistryString, "Sybase SQL Anywhere 5.0", "Installed")
End If

ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\Sybase SQL Anywhere 5.0"
li_rtn = RegistryGet(ls_RegistryString, "DRIVER", ls_Installed)
If li_rtn < 0 or Len(TRim(ls_Installed)) < 1 Then
	RegistrySet(ls_RegistryString, "DRIVER", "c:\SQLANY50\win32\WOD50T.DLL")
   RegistrySet(ls_RegistryString, "SETUP" , "c:\SQLANY50\win32\WOD50T.DLL")
End If

GetEnvironment(lu_Env)
// Do this only if this is Windows95/98--NT has a system datasource
IF lu_Env.OSType  =  Windows! Then
	//Check to see if the data source has been installed for each user
	li_rtn = RegistryKeys("HKEY_USERS", ls_subkeylist)
	li_numberofusers = UpperBound( ls_subkeylist)							
	For li_counter = 1 to li_numberofusers
		ls_RegistryString = "HKEY_USERS\" + ls_subkeylist[li_counter] + &
										"\Software\ODBC\ODBC.INI\ODBC Data Sources"							
		li_rtn = RegistryGet(ls_RegistryString, "pblocaldb", ls_Installed)
		IF li_rtn < 0 OR Len(Trim(ls_Installed)) < 1 Then
			RegistrySet(ls_RegistryString, "pblocaldb","Sybase SQL Anywhere 5.0")
			ls_RegistryString = "HKEY_USERS\" + ls_subkeylist[li_counter] + &
										"\Software\ODBC\ODBC.INI\pblocaldb"
			RegistrySet(ls_RegistryString, "Start", "c:\SQLANY50\win32\Rtdsk50.exe")
			RegistrySet(ls_RegistryString, "Driver", "c:\sqlany50\win32\WOD50T.DLL")
			RegistrySet(ls_RegistryString, "DatabaseFile", ls_JustPath + "\pblocaldb.db")
			RegistrySet(ls_RegistryString, "UID", "dba")
			RegistrySet(ls_RegistryString, "PWD", "sql")
			RegistrySet(ls_RegistryString, "AutoStop", "yes")
			ls_RegistryString = "HKEY_USERS\" + ls_subkeylist[li_counter] + "\Software\ODBC\ODBC.INI\Sybase SQL Anywhere 5.0"
			RegistrySet(ls_RegistryString, "Setup", "c:\sqlany50\win32\WOD50T.DLL")
			RegistrySet(ls_RegistryString, "Driver", "c:\sqlany50\win32\WOD50T.DLL")
			ls_RegistryString = "HKEY_USERS\" + ls_subkeylist[li_counter] + "\Software\ODBC\ODBC.INI\ODBC Data Sources"
		END IF
	Next
ELSE // This is windows NT
	ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources"
	li_rtn = RegistryGet(ls_RegistryString, "pblocaldb", ls_Installed)
	IF li_rtn < 0 OR Len(Trim(ls_Installed)) < 1 Then
		RegistrySet(ls_RegistryString, "pblocaldb", "Sybase SQL Anywhere 5.0")
		ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\Pblocaldb"
		RegistrySet(ls_RegistryString, "Start", "c:\SQLANY50\win32\Rtdsk50.exe")
		RegistrySet(ls_RegistryString, "Driver", "c:\sqlany50\win32\WOD50T.DLL")
		RegistrySet(ls_RegistryString, "DatabaseFile", ls_JustPath + "\pblocaldb.db")
		RegistrySet(ls_RegistryString, "UID", "dba")
		RegistrySet(ls_RegistryString, "PWD", "sql")
		RegistrySet(ls_RegistryString, "AutoStop", "yes")
		ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\ODBCAD32.exe"
		RegistrySet(ls_RegistryString, "", '"C:\WINNT\System32\ODBCAD32.exe"')
		ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\RTDSK50.EXE"
		Registryset(ls_RegistryString, "", '"c:\sqlany50\win32\RTDSK50.EXE"')

		// Increment the shared dll count by one
		ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\SharedDLLs"
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ctl3d32.dll",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ctl3d32.dll",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\DECODBDR.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\DECODBDR.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\DECODBSP.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\DECODBSP.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\DS32GT.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\DS32GT.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\MSVCRT10.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\MSVCRT10.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\MSVCRT20.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\MSVCRT20.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBC16GT.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBC16GT.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBC32.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBC32.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBC32GT.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBC32GT.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCAD32.exe",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBCAD32.exe",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCCP32.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBCCP32.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCCR32.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBCCR32.DLL",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCINST.hlp",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBCINST.hlp",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCINST.cnt",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBCINST.cnt",String(Long(ls_Dec_Reg_Val)+1))
		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCINT.DLL",ls_Dec_Reg_Val)
   	RegistrySet(ls_RegistryString, "C:\WINNT\System32\ODBCINT.DLL",String(Long(ls_Dec_Reg_Val)+1))
		//Messagebox("Registry changes", "Windows NT has detected registry changes. You must reboot for these changes to take effect. The application will now close.")
		//HALT CLOSE
	End If
End If

Return True
end function

on u_appsetup.create
TriggerEvent( this, "constructor" )
end on

on u_appsetup.destroy
TriggerEvent( this, "destructor" )
end on

