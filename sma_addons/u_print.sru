HA$PBExportHeader$u_print.sru
forward
global type u_print from u_abstractprintfunctions
end type
end forward

global type u_print from u_abstractprintfunctions
end type
global u_print u_print

type prototypes
Function int showPrintDlg(ULONG hwndOwner, ref string as_printer, &
		ref boolean ab_all_pages,  &
		ref boolean ab_collate, &
		ref boolean ab_disable_printtofile,  &
		ref boolean ab_hide_printtofile, &
		ref boolean ab_nopagenums,  &
		ref boolean ab_noselection, &
		ref boolean ab_nowarning,  &
		ref boolean ab_pagenums,  &
		ref boolean ab_printsetup,  &
		ref boolean ab_printtofile, &
		ref boolean ab_returndefault,  &
		ref boolean ab_selection, &
		ref boolean ab_showhelp,  &
		ref int ai_frompage,  &
		ref int ai_topage, &
		ref int ai_minpage,  &
		ref int ai_maxpage,  &
		ref int ai_copies)  &
library "printdlg.dll" alias for "showPrintDlg;Ansi"

Function int showPageSetupDlg(long hwndOwner,  &
			ref string as_printer, &
			ref boolean ab_default_min_margins, &
			ref boolean ab_disable_margins, &
			ref boolean ab_disable_orientation, &
			ref boolean ab_disable_paper, &
			ref boolean ab_disable_printer, &
			ref boolean ab_margins, &
			ref boolean ab_min_margins, &
			ref boolean ab_nowarning, &
			ref boolean ab_return_default, &
			ref boolean ab_show_help, &
			ref long al_size_x, &
			ref long al_size_y, &
			ref long al_min_left, &
			ref long al_min_right, &
			ref long al_min_top, &
			ref long al_min_bottom, &
			ref long al_left, &
			ref long al_right, &
			ref long al_top, &
			ref long al_bottom) &
library "printdlg.dll" alias for "showPageSetupDlg;Ansi"
end prototypes

type variables
u_RegistryService	iu_RegistryService
end variables

forward prototypes
public function boolean uf_printdialog (ref string as_printer_name, ref boolean ab_all_pages, ref boolean ab_collate, ref boolean ab_disable_printtofile, ref boolean ab_hide_printtofile, ref boolean ab_nopagenums, ref boolean ab_noselection, ref boolean ab_nowarning, ref boolean ab_pagenums, ref boolean ab_printsetup, ref boolean ab_printtofile, ref boolean ab_returndefault, ref boolean ab_selection, ref boolean ab_showhelp, ref integer ai_frompage, ref integer ai_topage, ref integer ai_minpage, ref integer ai_maxpage, ref integer ai_copies)
public function boolean uf_printdialog (ref boolean ab_collate, ref integer ai_from_page, ref integer ai_to_page, ref integer ai_min_page, ref integer ai_max_page, ref integer ai_copies)
public function boolean uf_pagesetupdialog (ref string as_printer_name, ref boolean ab_default_min_margins, ref boolean ab_disable_margins, ref boolean ab_disable_orientation, ref boolean ab_disable_paper, ref boolean ab_disable_printer, ref boolean ab_margins, ref boolean ab_min_margins, ref boolean ab_nowarning, ref boolean ab_return_default, ref boolean ab_show_help, ref long al_size_x, ref long al_size_y, ref long al_min_left, ref long al_min_right, ref long al_min_top, ref long al_min_bottom, ref long al_left, ref long al_right, ref long al_top, ref long al_bottom)
public function boolean uf_resetprinter ()
public function string uf_getdefaultprinter ()
public function boolean uf_intialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_getcurrentprinter ()
public function boolean uf_setprinter (string as_printer)
public function boolean uf_getsavedprinter (ref string as_printername)
public function boolean uf_saveprinter (string as_printername)
end prototypes

public function boolean uf_printdialog (ref string as_printer_name, ref boolean ab_all_pages, ref boolean ab_collate, ref boolean ab_disable_printtofile, ref boolean ab_hide_printtofile, ref boolean ab_nopagenums, ref boolean ab_noselection, ref boolean ab_nowarning, ref boolean ab_pagenums, ref boolean ab_printsetup, ref boolean ab_printtofile, ref boolean ab_returndefault, ref boolean ab_selection, ref boolean ab_showhelp, ref integer ai_frompage, ref integer ai_topage, ref integer ai_minpage, ref integer ai_maxpage, ref integer ai_copies);
/* --------------------------------------------------------

<DESC>	This function supplies most default arguments to PrintDialog().
			use this function for unlimited options </DESC>

<ARGS>	as_printer_name: Device Name that to default when opened, Device Name the user 
			chooses on return
			ab_all_pages: The default flag that indicates that the All radio button is 
			initially selected. This flag is used as a placeholder to indicate that the 
			ab_nopagenums and ab_Selection flags are not specified. 
			ab_collate: Places a checkmark in the Collate check box when set on input. 
			When the PrintDlg function returns, this flag indicates that the user selected the 
			Collate option and the printer driver does not support collation. In this case, the
			application must provide collation.
			ab_disable_printtofile: Disables the Print to File check box
			ab_hide_printtofile: Hides the Print to File check box.
			ab_nopagenums: Disables the Pages radio button and the associated edit controls.
			ab_noselection: Disables the Selection radio button.
			ab_nowarning: Prevents the warning message from being displayed when there is no default printer
			ab_pagenums: Causes the Pages radio button to be in the selected state when the 
			dialog box is created. When PrintDlg returns, this flag is set if the Pages radio 
			button is in the selected state. The ai_frompage and ai_topage members indicate the
			starting and ending pages specified by the user.
			ab_printsetup: Causes the system to display the Print Setup dialog box rather than the Print dialog box.
			ab_printtofile: Causes the Print to File check box to be checked when the dialog box
			is created. When PrintDlg returns, this flag is set if the check box is checked. You must
			prompt the user for a file name
			ab_returndefault: The PrintDlg function does not display the dialog box. Instead, it sets
			the hDevNames and hDevMode members to handles to DEVMODE and DEVNAMES structures that are
			initialized for the system default printer. Both hDevNames or hDevMode must be NULL, or 
			PrintDlg returns an error. If the system default printer is supported by an old printer 
			driver (earlier than Windows version 3.0), only hDevNames is returned; hDevMode is NULL.
			ab_selection: Causes the Selection radio button to be in the selected state when the
			dialog box is created. When PrintDlg returns, this flag is specified if the Selection 
			radio button is selected. If neither ab_PAGENUMS nor ab_SELECTION is set, the All radio 
			button is selected
			ab_showhelp: Causes the dialog box to display the Help button. The hwndOwner member must 
			specify the window to receive the HELPMSGSTRING registered messages that the dialog
			box sends when the user clicks the Help button.
			ai_frompage: Specifies the initial value for the starting page edit control. 
			When PrintDlg returns, ai_frompage is the starting page specified by the user. 
			If the Pages radio button is selected when the user clicks the Okay button, PrintDlg
			sets the ab_PAGENUMS flag and does not return until the user enters a starting page 
			value that is within the minimum to maximum page range.
			Windows 95 -If the input value for either ai_frompage or ai_TOpage is outside the
			range specified by ai_MinPage and ai_MaxPage, PrintDlg returns an error.
			Windows NT -If the input value for either ai_FromPage or aiToPage is outside the 
			minimum/maximum range, PrintDlg returns an error only if the ab_PAGENUMS flag is 
			specified; otherwise, it displays the dialog box but changes the out-of-range value
			to the minimum or maximum value.
			ai_topage: Specifies the initial value for the ending page edit control. 
			When PrintDlg returns, ai_ToPage is the ending page specified by the user. If the 
			Pages radio button is selected when the use clicks the Okay button, PrintDlg sets 
			the ab_PAGENUMS flag and does not return until the user enters an ending page value
			that is within the minimum to maximum page range.
			ai_minpage: Specifies the minimum value for the range of pages specified in the From 
			and To page edit controls. If ai_MinPage equals ai_MaxPage, the Pages radio button and
			the starting and ending page edit controls are disabled
			ai_maxpage: Specifies the maximum value for the range of pages specified in the From and To page edit controls. 
			ai_copies: Contains the initial number of copies for the Copies edit control. When 
			PrintDlg returns, this member contains the actual number of copies to print. If the 
			printer driver does not support multiple copies, this value may be greater than one 
			and the application must print all requested copies. </ARGS>
			
<USAGE>	On entry, these arguments populate the PrintDialog dialog box initially.
			On exit, these arguments contain the final values the user selected in the 
			dialog box</USAGE>
			
-------------------------------------------------------- */





Boolean	lb_return 
Int li_temp
Long ll_handle
String	ls_LastPrinter


u_string_functions	lu_string

as_printer_name = Space(63)


//Save The current Printer Name (Default Printer)
lb_return  = This.uf_getcurrentPrinter()
IF Not  LB_Return Then Return FALSE
is_OrigPrinter = is_Default


// Get the one we last printed to
IF  uf_getSavedPrinter(ls_LastPrinter) Then
	lb_Return = This.uf_SetPrinter(ls_LastPrinter)
	IF Not  lb_Return Then Return FALSE
END IF

li_temp = ShowPrintDlg(ll_handle, as_printer_name, &
		ref ab_all_pages,   &
		ref ab_collate,     &
		ref ab_disable_printtofile, &
		ref ab_hide_printtofile,    &
		ref ab_nopagenums,  &
		ref ab_noselection, &
		ref ab_nowarning,   &
		ref ab_pagenums,    &  
		ref ab_printsetup,  &
		ref ab_printtofile, & 
		ref ab_returndefault, & 
		ref ab_selection,     & 
		ref ab_showhelp,      &  
		ref ai_frompage,      & 
		ref ai_topage,        & 
		ref ai_minpage,       & 
		ref ai_maxpage,       & 
		ref ai_copies)        & 

If li_temp <> 0 Then
	// Error Occurred
	return False
End If

If lu_string.nf_isempty(as_printer_name) Then
	// Chose Cancel
	return False
End If

lb_Return = This.uf_SetPrinter(as_Printer_name)
IF Not  lb_Return Then Return FALSE

IF uf_SavePrinter(as_Printer_Name)  Then
	Return True
ELSE
	Return False
ENd IF
end function

public function boolean uf_printdialog (ref boolean ab_collate, ref integer ai_from_page, ref integer ai_to_page, ref integer ai_min_page, ref integer ai_max_page, ref integer ai_copies);
/* --------------------------------------------------------

<DESC>	This function supplies most default arguments to PrintDialog().
			use this function for limited options </DESC>

<ARGS>	ab_collate: Whether collating is set on or not
			ai_from_page: Start page
			ai_to_page: End Page
			ai_min_page: Minimum page number the user can choose
			ai_max_page: Maximum page number the user can choose
			ai_copies: Number of copies the user wants</ARGS>
			
<USAGE>	On entry, these arguments populate the PrintDialog dialog box initially.
			On exit, these arguments contain the final values the user selected in the 
			dialog box</USAGE>
			
-------------------------------------------------------- */



String ls_printer_name
Boolean	lb_true = True, &
			lb_false = False

return uf_printdialog(ls_printer_name, &
					lb_True, &
					ab_collate, &
					lb_False, &
					lb_True, &
					lb_False, &
					lb_True, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					lb_False, &
					ai_from_page, &
					ai_to_page, &
					ai_min_page, &
					ai_max_page, &
					ai_copies)
				
end function

public function boolean uf_pagesetupdialog (ref string as_printer_name, ref boolean ab_default_min_margins, ref boolean ab_disable_margins, ref boolean ab_disable_orientation, ref boolean ab_disable_paper, ref boolean ab_disable_printer, ref boolean ab_margins, ref boolean ab_min_margins, ref boolean ab_nowarning, ref boolean ab_return_default, ref boolean ab_show_help, ref long al_size_x, ref long al_size_y, ref long al_min_left, ref long al_min_right, ref long al_min_top, ref long al_min_bottom, ref long al_left, ref long al_right, ref long al_top, ref long al_bottom);/* --------------------------------------------------------

<DESC>	Function used to display the standard Windows Pagesetup
			Dialog.</DESC>

<ARGS>	as_printer_name: Name of printer to display as defaullt
			ab_default_min_margins: Sets the minimum values that the user can specify 
			for the page margins to be the minimum margins allowed by the printer. 
			This is the default. This flag is ignored if the ab_margins and ab_minmargins
			flags are also specified.
			ab_disable_margins: Disables the margin controls, preventing the user from 
			setting the margins.
			ab_disable_orientation: Disables the orientation controls, preventing the user from
			setting the page orientation.
			ab_disable_paper: Disables the paper controls, preventing the user from setting page
			parameters such as the paper size and source.
			ab_disable_printer:  Disables the Printer button, preventing the user from invoking a 
			dialog box that contains additional printer setup information. 
			ab_margins: Causes the system to use the values specified in the rtMargin member as
			the initial widths for the left, top, right, and bottom margins. If ab_margins is 
			not set, the system sets the initial widths to one inch for all margins
			ab_min_margins: Causes the system to use the values specified in the al_min_right
			member as the minimum allowable widths for the left, top, right, and bottom margins.
			The system prevents the user from entering a width that is less than the specified 
			minimum. If ab_min_Margins is not specified, the system sets the minimum allowable widths 
			to those allowed by the printer. 
			ab_nowarning: Prevents the system from displaying a warning message when there is no 
			default printer.
			ab_return_default: PageSetupDlg does not display the dialog box. Instead, it sets the 
			hDevNames and hDevMode members to handles to DEVMODE and DEVNAMES structures that are 
			initialized for the system default printer. PageSetupDlg returns an error if either 
			hDevNames or hDevMode is not NULL.
			ab_show_help: Causes the dialog box to display the Help button. The hwndOwner member 
			must specify the window to receive the HELPMSGSTRING registered messages that the 
			dialog box sends when the user clicks the Help button
			al_size_x: Specifies the x dimensions of the paper selected by the user. 
			al_size_y: Specifies the y dimensions of the paper selected by the user. 
			al_min_left: Specifies the minimum allowable widths for the left margins. The system
			ignores this member if the ab_disable_margins flag is not set. These values must be 
			less than or equal to the values specified in the al_left.
			al_min_right: Specifies the minimum allowable widths for the right margins. The system
			ignores this member if the ab_disable_margins flag is not set. These values must be 
			less than or equal to the values specified in the al_right.
			al_min_top: Specifies the minimum allowable widths for the top margins. The system
			ignores this member if the ab_disable_margins flag is not set. These values must be 
			less than or equal to the values specified in the al_top.
			al_min_bottom: Specifies the minimum allowable widths for the bottom margins. The system
			ignores this member if the ab_disable_margins flag is not set. These values must be 
			less than or equal to the values specified in the al_bottom.
			al_left: Specifies the widths of the left, if you set the ab_MARGINS flag, al_Left
			specifies the initial margin values. When PageSetupDlg returns, al_Left contains the margin 
			widths selected by the user. 
			al_right: Specifies the widths of the right, if you set the ab_MARGINS flag, al_right
			specifies the initial margin values. When PageSetupDlg returns, al_right contains the margin 
			widths selected by the user. 
			al_top: Specifies the widths of the top, if you set the ab_MARGINS flag, al_top
			specifies the initial margin values. When PageSetupDlg returns, al_top contains the margin 
			widths selected by the user. 
			al_bottom: Specifies the widths of the Bottom, if you set the ab_MARGINS flag, 
			al_bottom specifies the initial margin values. When PageSetupDlg returns, al_bottom
			contains the margin widths selected by the user. 

			
<USAGE>	Call this function to display windows common pagesetup dialog setting flags as needed</USAGE>
			
-------------------------------------------------------- */

Int li_temp
Long ll_handle
u_string_functions	lu_string

as_printer_name = Space(63)


li_temp = ShowPageSetupDlg(ll_handle, as_printer_name, &
		ref ab_default_min_margins, &
		ref ab_disable_margins, &
		ref ab_disable_orientation, &
		ref ab_disable_paper, &
		ref ab_disable_printer, &
		ref ab_margins, &
		ref ab_min_margins, &
		ref ab_nowarning, &
		ref ab_return_default, &
		ref ab_show_help, &
		ref al_size_x, &
		ref al_size_y, &
		ref al_min_left, &
		ref al_min_right, &
		ref al_min_top, &
		ref al_min_bottom, &
		ref al_left, &
		ref al_right, &
		ref al_top, &
		ref al_bottom)

If li_temp <> 0 Then
	// Error Occurred
	return False
End If

If lu_string.nf_isempty(as_printer_name) Then
	// Chose Cancel
	return False
End If

return True
end function

public function boolean uf_resetprinter ();/* --------------------------------------------------------

<DESC>	Function used to Reset default printer back to what is was</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Do not attempt to use			</USAGE>
			
-------------------------------------------------------- */

Return This.uf_SetPrinter(is_origprinter)
end function

public function string uf_getdefaultprinter ();
/* --------------------------------------------------------

<DESC>	Function used to get Windows default printer</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Call this function to obtain the name of the default
			printer</USAGE>
			
-------------------------------------------------------- */


String	ls_printer_name
Boolean	lb_ret, &
			lb_true = True, &
			lb_false = False

Integer	li_from = 1, &
			li_to = 1, &
			li_min = 1, &
			li_max = 1, &
			li_copies = 1

u_string_functions	lu_string


lb_ret = uf_printdialog(ls_printer_name, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_false, &
								lb_true, /* return values for default printer */ &    
								lb_false, &
								lb_false, &
								li_from, &
								li_to, &
								li_min, &
								li_max, &
								li_copies)
								
If Not lb_ret or lu_string.nf_isempty(ls_printer_name) Then
	return ""
Else
	return ls_printer_name								
End If
					
end function

public function boolean uf_intialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);/* --------------------------------------------------------

<DESC>	Function used to set instance variables to a referance of
			the expected type</DESC>

<ARGS>	au_classfactory: Class Factory 
			au_errorcontext: Error Context</ARGS>

			
<USAGE>	Call this function to set the instance variables refering to the 
			class factory and an error context
			printer</USAGE>
			
-------------------------------------------------------- */
iu_classfactory = au_classfactory
iu_errorcontext = au_errorcontext 

Return True
end function

private function boolean uf_getcurrentprinter ();
/* --------------------------------------------------------

<DESC>	Private function used to "Remember" windows default printer</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Do not attempt to use			</USAGE>
			
-------------------------------------------------------- */

//Variable declarations:
int li_rtn
Environment env

String	ls_Key
string 	ls_default, ls_driver, ls_port, ls_printer

iu_ClassFactory.uf_GetObject("u_registryService",&
											iu_registryservice,iu_errorcontext) 
IF Not iu_errorcontext.uf_IsSuccessful() Then
	Return False
END IF



li_rtn = GetEnvironment(env)
IF li_rtn <> 1 THEN RETURN FALSE

CHOOSE CASE env.OSType
	CASE Windows!
		//Get the current default printer name.
		iu_registryservice.uf_Initialize(iu_classfactory,"HKEY_LOCAL_MACHINE\Config\0001\System\CurrentControlSet\Control\Print\Printers",iu_errorcontext)
		IF iu_ErrorContext.uf_IsSuccessful() Then
			IF iu_registryservice.uf_GetData("default", ls_default,iu_errorcontext) Then
				is_Default = ls_Default
			Else
				iu_errorcontext.uf_AppendText("Unable to get default printer")
				Return False
			END IF
		ELSE 
			iu_errorcontext.uf_AppendText("Unable to initialize the registry Manager")
			Return False
		END IF	
		//Get the default printers driver and port.
		ls_key = "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Print\Printers\"+ ls_Default
		iu_registryservice.uf_Initialize(iu_classfactory,ls_Key,iu_errorcontext)
		IF iu_ErrorContext.uf_IsSuccessful() Then
			IF iu_registryservice.uf_GetData("Printer Driver", ls_driver, iu_errorcontext) Then
				is_Driver = ls_Driver
			ELSE
				Return False
			END IF
			if iu_registryservice.uf_GetData("Port", ls_port,iu_errorcontext) Then
				is_Port = ls_Port
			ELSE
				Return False
			END IF
		END IF
		iu_registryservice.uf_GetData("Port", ls_port,iu_errorcontext)
		is_Port = ls_Port
	Case WindowsNT!
		//Get the current default printer name.
		ls_Key = "HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Windows"
		iu_registryservice.uf_Initialize(iu_classfactory,ls_Key,iu_errorcontext)
		IF iu_ErrorContext.uf_IsSuccessful() Then
			IF iu_registryservice.uf_GetData("Device", ls_default, iu_errorcontext) Then
				is_Default = ls_Default
			else
				Return False
			End IF
		ELSE
			Return False
		END IF
	CASE ELSE
		RETURN False
END CHOOSE
Return True
















end function

public function boolean uf_setprinter (string as_printer);//Variable declarations:
int li_rtn
Environment	env

li_rtn = GetEnvironment(env)

IF li_rtn <> 1 THEN RETURN FALSE
String	ls_Printer


iu_ClassFactory.uf_GetObject("u_registrydefaultmanager",&
											iu_registryservice,iu_errorcontext) 
IF Not iu_errorcontext.uf_IsSuccessful() Then
	Return False
END IF
ls_Printer=as_printer

CHOOSE CASE env.OSType
	CASE Windows!
		if Right(ls_Printer,2) <> ",," Then ls_Printer += ",,"
		iu_registryservice.uf_Initialize(iu_classfactory,"HKEY_LOCAL_MACHINE\Config\0001\System\CurrentControlSet\Control\Print\Printers",iu_errorcontext)
		IF iu_ErrorContext.uf_IsSuccessful() Then
			IF Not iu_registryservice.uf_SetData("default", ls_printer,iu_errorcontext) Then
				iu_errorcontext.uf_AppendText("Unable to Set default printer")
				Return False
			END IF
		ELSE 
			iu_errorcontext.uf_AppendText("Unable to initialize the registry Manager")
			Return False
		END IF	
	Case WindowsNT!
		//Set a new default printer name.
		if Right(ls_Printer,15) <> ",winspool,Ne01:" Then ls_Printer += ",winspool,Ne01:"
		iu_registryservice.uf_Initialize(iu_classfactory,"HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Windows",iu_errorcontext)
		IF iu_ErrorContext.uf_IsSuccessful() Then
			IF Not iu_registryservice.uf_SetData("Device", ls_printer,iu_errorcontext) Then
				iu_errorcontext.uf_AppendText("Unable to Set default printer")
				Return False
			END IF
		ELSE 
			iu_errorcontext.uf_AppendText("Unable to initialize the registry Manager")
			Return False
		END IF	
	CASE ELSE
		RETURN FALSE
END CHOOSE
Return TRUE










end function

public function boolean uf_getsavedprinter (ref string as_printername);
/* --------------------------------------------------------

<DESC>	Function to obtain the default printer for the application</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Call this function to obtain the name of the printer
			the application last used</USAGE>
			
-------------------------------------------------------- */

Application	la_Application

Integer	li_LoopCount,&
			li_Pos
String	ls_lastprinter,&
			ls_AppName,&
			ls_Key,&
			ls_Printer
			
la_Application = GetApplication()

ls_AppName = la_Application.AppNAme
ls_Key = "HKEY_LOCAL_MACHINE\SOFTWARE\IBP\"+ls_AppName
			
iu_ClassFactory.uf_GetObject("u_registryservice",&
											iu_RegistryService,iu_errorcontext) 
IF Not iu_errorcontext.uf_IsSuccessful() Then
	Return False
END IF
ls_Printer=as_printerName

iu_RegistryService.uf_Initialize(iu_classfactory,ls_Key,iu_errorcontext)
IF iu_ErrorContext.uf_IsSuccessful() Then
	IF Not iu_RegistryService.uf_GetData("default printer", ls_printer,iu_errorcontext) Then
		iu_errorcontext.uf_AppendText("Unable to Set default printer")
		Return False
	END IF
ELSE 
	iu_errorcontext.uf_AppendText("Unable to initialize the registry Manager")
	Return False
END IF	
Return True



end function

public function boolean uf_saveprinter (string as_printername);
/* --------------------------------------------------------

<DESC>	Function used to Save the printer the application last used</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Call this function to obtain the name of the default of the app
			Call uf_GetSaved printer to retrieve</USAGE>
			
-------------------------------------------------------- */
Application	la_Application

Integer	li_LoopCount,&
			li_Pos
String	ls_lastprinter,&
			ls_AppName,&
			ls_Key,&
			ls_Printer
			
la_Application = GetApplication()

ls_AppName = la_Application.AppNAme
ls_Key = "HKEY_LOCAL_MACHINE\SOFTWARE\IBP\"+ls_AppName
			
iu_ClassFactory.uf_GetObject("u_registryService",&
											iu_registryservice,iu_errorcontext) 
IF Not iu_errorcontext.uf_IsSuccessful() Then
	Return False
END IF
ls_Printer=as_printerName

iu_registryservice.uf_Initialize(iu_classfactory,ls_Key,iu_errorcontext)
IF iu_ErrorContext.uf_IsSuccessful() Then
	IF Not iu_registryservice.uf_SetData("default printer", ls_printer,iu_errorcontext) Then
		iu_errorcontext.uf_AppendText("Unable to Set default printer")
		Return False
	END IF
ELSE 
	iu_errorcontext.uf_AppendText("Unable to initialize the registry Manager")
	Return False
END IF	
Return True



end function

on u_print.create
TriggerEvent( this, "constructor" )
end on

on u_print.destroy
TriggerEvent( this, "destructor" )
end on

