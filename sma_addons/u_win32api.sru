HA$PBExportHeader$u_win32api.sru
forward
global type u_win32api from nonvisualobject
end type
end forward

type os_filedatetime from structure
	unsignedlong		ul_lowdatetime
	unsignedlong		ul_highdatetime
end type

type os_systemtime from structure
	unsignedinteger		ui_wyear
	unsignedinteger		ui_wmonth
	unsignedinteger		ui_wdayofweek
	unsignedinteger		ui_wday
	unsignedinteger		ui_whour
	unsignedinteger		ui_wminute
	unsignedinteger		ui_wsecond
	unsignedinteger		ui_wmilliseconds
end type

type os_finddata from structure
	unsignedlong		ul_fileattributes
	os_filedatetime		str_creationtime
	os_filedatetime		str_lastaccesstime
	os_filedatetime		str_lastwritetime
	unsignedlong		ul_filesizehigh
	unsignedlong		ul_filesizelow
	unsignedlong		ul_reserved0
	unsignedlong		ul_reserved1
	character		ch_filename[260]
	character		ch_alternatefilename[14]
end type

type os_choosecolor from structure
	unsignedlong		lstructsize
	unsignedinteger		hwndowner
	unsignedinteger		hinstance
	long		rgbresult
	string		lpcustcolors
	unsignedlong		flags
	long		lcustdata
	unsignedinteger		lpfnhook
	string		lptemplatename
end type

type os_memorystatus from structure
	long		dwlength
	long		dwmemoryload
	long		dwtotalphys
	long		dwavailphys
	long		dwtotalpagefile
	long		dwavailpagefile
	long		dwtotalvirtual
	long		dwavailvirtual
end type

global type u_win32api from nonvisualobject
end type
global u_win32api u_win32api

type prototypes
Private:
function int ChooseColorA(ref os_choosecolor lstr_color) library "comdlg32.dll" alias for "ChooseColorA;Ansi"
Function Long GetSysColor(Int index) Library "user32.dll"

function int GetModuleFileNameA(int hinst, ref string filename, int namesize) library "kernel32.dll" alias for "GetModuleFileNameA;Ansi"

function uint GetWindowsDirectoryA(ref string WindowsDir, int StringSize) library "kernel32.dll" alias for "GetWindowsDirectoryA;Ansi"

subroutine GlobalMemoryStatus( ref os_MemoryStatus as_MemoryStatus) library "kernel32.dll" alias for "GlobalMemoryStatus;Ansi"

// Win32 calls for file date and time
Function boolean GetFileTime(long hFile, ref os_filedatetime  lpCreationTime, ref os_filedatetime  lpLastAccessTime, ref os_filedatetime  lpLastWriteTime  )  library "KERNEL32.DLL" alias for "GetFileTime;Ansi"
Function boolean FileTimeToSystemTime(ref os_filedatetime lpFileTime, ref os_systemtime lpSystemTime) library "KERNEL32.DLL" alias for "FileTimeToSystemTime;Ansi"
Function boolean FileTimeToLocalFileTime(ref os_filedatetime lpFileTime, ref os_filedatetime lpLocalFileTime) library "KERNEL32.DLL" alias for "FileTimeToLocalFileTime;Ansi"
Function boolean SetFileTime(ulong hFile, os_filedatetime  lpCreationTime, os_filedatetime  lpLastAccessTime, os_filedatetime  lpLastWriteTime  )  library "KERNEL32.DLL" alias for "SetFileTime;Ansi"
Function boolean SystemTimeToFileTime(os_systemtime lpSystemTime, ref os_filedatetime lpFileTime) library "KERNEL32.DLL" alias for "SystemTimeToFileTime;Ansi"
Function boolean LocalFileTimeToFileTime(ref os_filedatetime lpLocalFileTime, ref os_filedatetime lpFileTime) library "KERNEL32.DLL" alias for "LocalFileTimeToFileTime;Ansi"
Function long FindFirstFileA (ref string filename, ref os_finddata findfiledata) library "KERNEL32.DLL" alias for "FindFirstFileA;Ansi"
Function boolean FindNextFileA (long handle, ref os_finddata findfiledata) library "KERNEL32.DLL" alias for "FindNextFileA;Ansi"
Function boolean FindClose (long handle) library "KERNEL32.DLL"

Function boolean ExitWindowsEx(Uint Uflags, Double dwreserved  )  library "User32.dll"

function long GetComputerNameA( ref String lpBuffer, ref long nSize) library "kernel32.dll" alias for "GetComputerNameA;Ansi"
function long GetUserNameA(ref String lpBuffer, ref long nSize) library "advapi32.dll" alias for "GetUserNameA;Ansi"

end prototypes

type variables

end variables

forward prototypes
public function integer uf_choosecolor (ref long arg_color)
public function long uf_getsyscolor (integer ai_option)
public function string uf_getwindowsdirectory ()
public function string uf_getmodulefilename ()
public subroutine uf_exitwindows ()
private function integer uf_convertfiledatetimetopb (os_filedatetime astr_filetime, ref string as_filedate, ref string as_filetime)
public function integer uf_getcreationdatetime (string as_filename, ref string as_date, ref string as_time)
public function string uf_getcomputername ()
public function string uf_getusername ()
private function boolean uf_globalmemorystatus (ref long al_totalvirtual, ref long al_freevirtual, ref long al_totalphysical, ref long al_freephysical, ref long al_totalpagefile, ref long al_freepagefile)
end prototypes

public function integer uf_choosecolor (ref long arg_color);
/* --------------------------------------------------------
uf_ChooseColor()

<DESC>	This function opens the common dialog for choosing
			a color.
</DESC>

<ARGS>	arg_color: Variable to receive the color by reference
</ARGS>

<USAGE>	Call this function to open the common dialog
			for choosing a color.  The window will have no
			parent window.  The color chosen is placed in the
			argument passed by reference.
</USAGE>
-------------------------------------------------------- */
os_ChooseColor	lstr_color
Int				li_null
String			ls_cust_colors

SetNull(li_null)

ls_cust_colors = Space(64)
lstr_color.hwndOwner 	= li_null
lstr_color.hInstance 	= 0
lstr_color.rgbResult 	= 0
lstr_color.lpCustColors = ls_cust_colors
lstr_color.Flags 			= 0
lstr_color.lCustData 	= 0
lstr_color.lpfnHook		= 0
lstr_color.lpTemplateName = ls_cust_colors
lstr_color.lStructSize 	= 36
ChooseColorA(lstr_color)

arg_color = lstr_color.rgbResult

return 0



end function

public function long uf_getsyscolor (integer ai_option);
/* --------------------------------------------------------
uf_GetSysColor()

<DESC>	This function gets the system color currently
			defined for the given item.
</DESC>

<ARGS>	ai_option: Integer specifying the user interface
			item for which you want its color
</ARGS>

<USAGE>	Call this function to obtain the current RGB
			value for a system user interface item.  The
			valid options are defined in the winuser.h 
			header file from the Platform SDK.
</USAGE>
-------------------------------------------------------- */

/* This is from winuser.h

#define COLOR_SCROLLBAR         0
#define COLOR_BACKGROUND        1
#define COLOR_ACTIVECAPTION     2
#define COLOR_INACTIVECAPTION   3
#define COLOR_MENU              4
#define COLOR_WINDOW            5
#define COLOR_WINDOWFRAME       6
#define COLOR_MENUTEXT          7
#define COLOR_WINDOWTEXT        8
#define COLOR_CAPTIONTEXT       9
#define COLOR_ACTIVEBORDER      10
#define COLOR_INACTIVEBORDER    11
#define COLOR_APPWORKSPACE      12
#define COLOR_HIGHLIGHT         13
#define COLOR_HIGHLIGHTTEXT     14
#define COLOR_BTNFACE           15
#define COLOR_BTNSHADOW         16
#define COLOR_GRAYTEXT          17
#define COLOR_BTNTEXT           18
#define COLOR_INACTIVECAPTIONTEXT 19
#define COLOR_BTNHIGHLIGHT      20

#if(WINVER >= 0x0400)
#define COLOR_3DDKSHADOW        21
#define COLOR_3DLIGHT           22
#define COLOR_INFOTEXT          23
#define COLOR_INFOBK            24
#endif /* WINVER >= 0x0400 */

#if(WINVER >= 0x0500)
#define COLOR_HOTLIGHT                  26
#define COLOR_GRADIENTACTIVECAPTION     27
#define COLOR_GRADIENTINACTIVECAPTION   28
#endif /* WINVER >= 0x0500 */

#if(WINVER >= 0x0400)
#define COLOR_DESKTOP           COLOR_BACKGROUND
#define COLOR_3DFACE            COLOR_BTNFACE
#define COLOR_3DSHADOW          COLOR_BTNSHADOW
#define COLOR_3DHIGHLIGHT       COLOR_BTNHIGHLIGHT
#define COLOR_3DHILIGHT         COLOR_BTNHIGHLIGHT
#define COLOR_BTNHILIGHT        COLOR_BTNHIGHLIGHT
#endif /* WINVER >= 0x0400 */
*/

Return GetSysColor(ai_option)

end function

public function string uf_getwindowsdirectory ();
/* --------------------------------------------------------
uf_GetWindowsDirectory()

<DESC>	This function retrieves the path to the Windows
			directory.  Under Windows 95, this defaults to
			C:\WINDOWS.  Under Windows NT, this defaults to
			C:\WINNT.
</DESC>

<ARGS></ARGS>

<USAGE>	Call this function to obtain the full path to
			the Windows directory.
</USAGE>
-------------------------------------------------------- */
String	ls_windows_dir

ls_windows_dir = Space(255)

GetWindowsDirectoryA(ls_windows_dir, 255)

RETURN ls_windows_dir


end function

public function string uf_getmodulefilename ();
/* --------------------------------------------------------
uf_GetModuleFileName()

<DESC>	This function gets the full path and executable name
			for the current module.
</DESC>

<ARGS>	</ARGS>

<USAGE>	Call this function to obtain the path and
			filename for the executable that started this
			application.  For applications running through
			the PowerBuilder development environment, the
			executable is that of PowerBuilder itself.
			</USAGE>
-------------------------------------------------------- */

String	ls_FilePath

ls_filepath = Space(64)
GetModuleFileNameA(Handle(GetApplication()), ls_filepath, 64)

return ls_FilePath
end function

public subroutine uf_exitwindows ();
/* --------------------------------------------------------
uf_ExitWindows()

<DESC>	This functions restarts Windows.
</DESC>

<ARGS>	</ARGS>

<USAGE>	Call this function to restart Windows.  The
			current user is logged off.  Under Windows NT,
			the process may not have sufficient privileges
			to perform the restart procedure.
</USAGE>
-------------------------------------------------------- */
ExitWindowsEx(2,0)

end subroutine

private function integer uf_convertfiledatetimetopb (os_filedatetime astr_filetime, ref string as_filedate, ref string as_filetime);
/* --------------------------------------------------------
uf_ConvertFileDateTimeToPB()

<DESC>	This function converts a system date/time value to
			PowerBuilder's date and time values.
</DESC>

<ARGS>	astr_filetime: The os_filedatetime structure containing
			the system date/time for a file.
			as_filedate: The file date in PowerBuilder date format
			passed by reference.
			as_filetime: The file time in PowerBuilder time format
			passed by reference.
</ARGS>

<USAGE>	Call this function to convert the system date/time
			structure into a PowerBuilder date and time value.
			This function returns 1 if it succeeds, -1 otherwise.
</USAGE>
-------------------------------------------------------- */

os_filedatetime		lstr_LocalTime
os_systemtime		lstr_SystemTime

If Not FileTimeToLocalFileTime(astr_FileTime, lstr_LocalTime) Then Return -1
If Not FileTimeToSystemTime(lstr_LocalTime, lstr_SystemTime) Then Return -1

as_filedate = String(lstr_SystemTime.ui_WMonth) + "/" + &
				String(lstr_SystemTime.ui_WDay) + "/" + &
				String(lstr_SystemTime.ui_wyear)

as_filetime = String(lstr_SystemTime.ui_wHour) + ":" + &
				String(lstr_SystemTime.ui_wMinute) + ":" + &
				String(lstr_SystemTime.ui_wSecond) + ":" + &
				String(lstr_SystemTime.ui_wMilliseconds)
Return 1

end function

public function integer uf_getcreationdatetime (string as_filename, ref string as_date, ref string as_time);
/* --------------------------------------------------------
uf_GetCreationDateTime()

<DESC>	This function gets the date and time a file was created.</DESC>

<ARGS>	as_filename: The name of the file for which you want its
			date and time; an absolute path may be specified or it
			will be relative to the current working directory.
			as_date: The date the file was created, passed by reference.
			as_time: The time the file was created, passed by reference.
</ARGS>

<USAGE>	Call this function to get the date and time a
			file was created.
</USAGE>
-------------------------------------------------------- */
Long						ll_Handle
os_finddata	lstr_FindData

// Get the file information
ll_Handle = FindFirstFileA(as_FileName, lstr_FindData)
If ll_Handle <= 0 Then Return -1
FindClose(ll_Handle)

// Convert the date and time
Return This.uf_ConvertFileDatetimeToPB(lstr_FindData.str_CreationTime, as_Date, as_Time)

end function

public function string uf_getcomputername ();
/* --------------------------------------------------------
uf_GetComputerName()

<DESC>	This function retrieves the name of the local
			system.
</DESC>

<ARGS></ARGS>

<USAGE>	Call this function to obtain the name of
			the local system.
</USAGE>
-------------------------------------------------------- */
String	ls_computer
Long		ll_size

ls_computer = Space(64)
ll_size = 64

GetComputerNameA(ls_computer, ll_size)

Return ls_computer


end function

public function string uf_getusername ();
/* --------------------------------------------------------
uf_GetUserName()

<DESC>	This function retrieves the name of the currently
			logged-on user.
</DESC>

<ARGS></ARGS>

<USAGE>	Call this function to obtain the name of
			the currently logged-on user.
</USAGE>
-------------------------------------------------------- */
String	ls_user
Long		ll_size

ls_user = Space(64)
ll_size = 64

GetUserNameA(ls_user, ll_size)

Return ls_user


end function

private function boolean uf_globalmemorystatus (ref long al_totalvirtual, ref long al_freevirtual, ref long al_totalphysical, ref long al_freephysical, ref long al_totalpagefile, ref long al_freepagefile);
/* --------------------------------------------------------
uf_GlobalMemoryStatus()

<DESC>	This function retrieves volatile system information
			about current memory usage.
</DESC>

<ARGS>	al_totalvirtual: Total bytes of user address space
			al_freevirtual: Available bytes of user address space
			al_totalphysical: Total bytes of physical memory
			al_freephysical: Available bytes of physical memory
			al_totalpagefile: Total bytes of pagefile space
			al_freepagefile: Available bytes of pagefile space
</ARGS>

<USAGE>	Call this function to obtain memory usage information
			from the system.  This function returns true.
</USAGE>
-------------------------------------------------------- */

os_memorystatus lstr_memorystatus

GlobalMemoryStatus(lstr_memorystatus)
al_totalvirtual = lstr_memorystatus.dwtotalvirtual
al_freevirtual = lstr_memorystatus.dwavailvirtual
al_totalphysical = lstr_memorystatus.dwtotalphys
al_freephysical = lstr_memorystatus.dwavailphys
al_totalpagefile = lstr_memorystatus.dwtotalpagefile
al_freepagefile = lstr_memorystatus.dwavailpagefile

Return True
end function

on u_win32api.create
TriggerEvent( this, "constructor" )
end on

on u_win32api.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_win32api

<OBJECT>	This object encapsulates calls to the Win32 API.
</OBJECT>

<USAGE>	This object needs no initialization.  Call
			any API functions necessary through the
			provided interface.
</USAGE>

<AUTH>	Conrad Engel</AUTH>
--------------------------------------------------------- */


end event

