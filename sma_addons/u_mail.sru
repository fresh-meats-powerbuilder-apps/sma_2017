HA$PBExportHeader$u_mail.sru
forward
global type u_mail from nonvisualobject
end type
end forward

global type u_mail from nonvisualobject
end type
global u_mail u_mail

type variables
Private:
u_AbstractClassFactory	iu_ClassFactory
Boolean	ib_InterActive


MailFileDescription	imfa_Attachments[]

String		is_Subject,&
		is_TextBody

mailRecipient	imr_Recipient[ ]

end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, boolean ab_interactive, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_registerocx (ref u_abstracterrorcontext au_errorcontext)
public subroutine uf_setsubject (string as_subject)
public function boolean uf_address (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_address (mailrecipient amr_recipient[], ref u_abstracterrorcontext au_errorcontext)
public subroutine uf_setbody (string as_body)
public function boolean uf_send (ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_sendmapi (ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_sendocx (ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_attach (string filename, mailfiletype amft_filetype, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory, boolean ab_interactive, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Initialize the u_Mail.</DESC>

<ARGS>	au_ClassFactory: Class Factory - Needs to be able read the Registry 
			ab_interactive: If sending mail is interactive with the user
			au_errorcontext: Error context</ARGS>
			
<USAGE>	Pass true in ab_interactive if you want to send mail interactive. You may use 
			this function if you need to reset such things as the subject,TextBody,recipients and attachments
			This function returns true on success, false otherwise, Error Context will have contain error 
			information.YOU MUST HAVE MSMAPI.OCX for this to work properly</USAGE>
			
-------------------------------------------------------- */
MailFileDescription	lmfa_Attachments[]
mailRecipient	      lmr_Recipient[ ]
is_Subject = ''
is_TextBody = ''

imfa_attachments = lmfa_attachments
imr_recipient = lmr_recipient

IF ab_interactive Then
	// Check to make sure that the OCX is Registered
	iu_classfactory = au_ClassFactory
	ib_interactive = ab_interactive
	Return uf_RegisterOCX(au_errorcontext) 
	//Return True
END IF
ib_interactive = ab_interactive	
return True

end function

private function boolean uf_registerocx (ref u_abstracterrorcontext au_errorcontext);/* -------------------------------------------------------

<DESC>	Private function. Don't even attempt to use this</DESC>

<ARGS>	au_errorcontext: Error context</ARGS>
			
<USAGE>	Registers the OCX when needed. </USAGE>
			
-------------------------------------------------------- */



//Variable declarations:
int li_rtn
String	ls_Key,&
		 	ls_default,&
			ls_driver,&
			ls_port,&
			ls_printer

u_RegistryService	lu_registryservice

iu_ClassFactory.uf_GetObject("u_registryService",&
											lu_registryservice,au_errorcontext) 

IF Not au_errorcontext.uf_IsSuccessful() Then
	Return False
END IF

lu_registryservice.uf_Initialize(iu_classfactory,"HKey_Classes_root\MSMAPI.MAPIMessages",au_errorcontext)
IF au_errorcontext.uf_IsSuccessful() Then
	IF Not lu_registryservice.uf_GetData("", ls_default,au_errorcontext) Then
		if FileExists("MSMAPI32.OCX") Then
			IF Run("REGSVR32.EXE MSMAPI32.OCX", Minimized!) < 0 Then 
				au_errorcontext.uf_AppendText("Unable to Register MAPI OCX")
				Return False
			END IF	
		ELSE
		 au_errorcontext.uf_AppendText("Unable to Register MAPI OCX ... Cant not Start Mail")
		 Return False	
		END IF
	END IF	
ELSE	
	au_errorcontext.uf_AppendText("Unable to Initialize Registry Service")
			Return False
END IF
Return True
end function

public subroutine uf_setsubject (string as_subject);
/* --------------------------------------------------------

<DESC>	Set the subject of the Message</DESC>

<ARGS>	as_subject: The subject
			
<USAGE>	Call this function to put a subject on a message. </USAGE>
			
-------------------------------------------------------- */


is_subject = as_Subject
end subroutine

public function boolean uf_address (ref u_abstracterrorcontext au_errorcontext);


/* --------------------------------------------------------

<DESC>	Set list of people to address the message to
			</DESC>

<ARGS>	au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to prompt the user to select users to address the message to
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */

mailSession mSes

mailReturnCode mRet
mailMessage lmMsg
mailFileDescription mAttach

// Create a mail session
mSes = CREATE mailSession

// Log on to the session
mRet = mSes.mailLogon()
Choose Case  mRet 
	Case mailReturnSuccess!
	Case mailReturnLoginFailure!
		au_ErrorContext.uf_AppendText("Mail Login Failure")
		au_ErrorContext.uf_SetReturnCode(-1)
	Case mailReturnInsufficientMemory!
		au_ErrorContext.uf_AppendText("Unable to set Recipients - Insufficient Memory")
		au_ErrorContext.uf_SetReturnCode(-2)
	Case mailReturnTooManySessions!
		au_ErrorContext.uf_AppendText("Unable to Login - Too Many session")
		au_ErrorContext.uf_SetReturnCode(-3)
	Case mailReturnUserAbort!
		au_ErrorContext.uf_AppendText("Unable to Login - User Abort")
		au_ErrorContext.uf_SetReturnCode(-4)
END Choose





mRet = mSes.mailAddress(lmMsg)
Choose Case mRet
	Case mailReturnSuccess!
	Case mailReturnFailure!
		au_ErrorContext.uf_AppendText("Unable to set Recipients")
		au_ErrorContext.uf_SetReturnCode(-5)
		RETURN False
	Case mailReturnInsufficientMemory!
		au_ErrorContext.uf_AppendText("Unable to set Recipients - Insufficient Memory")
		au_ErrorContext.uf_SetReturnCode(-6)
		RETURN False
	Case mailReturnUserAbort!
		au_ErrorContext.uf_AppendText("Unable to set Recipients - UserAborted")
		au_ErrorContext.uf_SetReturnCode(-7)
		RETURN False
END Choose

imr_recipient[] = lmMsg.Recipient


Return True




end function

public function boolean uf_address (mailrecipient amr_recipient[], ref u_abstracterrorcontext au_errorcontext);

/* --------------------------------------------------------

<DESC>	Set list of people to address the message to
			</DESC>

<ARGS>	amr_recipient[]: mailrecipient structure
			au_errorcontext: ErrorContext</ARGS>
			
<USAGE>	Call this function to set list of recipients for the mail message.
			IF you pass a NULL recipient or there are no recipients the user will be prompted 
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */
IF IsNull(amr_Recipient) Then Return uf_Address(au_ErrorContext)	
IF UpperBound(amr_Recipient) < 0  then
	Return uf_Address(au_ErrorContext)	
ELSE
	imr_recipient = amr_Recipient
	Return True
	
END IF
end function

public subroutine uf_setbody (string as_body);
/* --------------------------------------------------------

<DESC>	Call this function to set the body of message</DESC>

<ARGS>	au_errorcontext: Error Context
			
<USAGE>	Call this function to set the Note Text of a message. IF the string is longer than 2048
			Characters, Save it as a file and send it as an attachment.
			</USAGE>
			
-------------------------------------------------------- */

is_textbody = as_Body
end subroutine

public function boolean uf_send (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Send a message.</DESC>

<ARGS>	au_errorcontext: Error Context
			
<USAGE>	Call this function to send a message. If you are sending NONINTERACTIVE, make sure 
			all the necessary information is filled in on NON interactive or it will fail. The 
			inteactive implementation needs no information.
			This function returns true on success, false otherwise, Error Context will have contain error 
			information.</USAGE>
			
-------------------------------------------------------- */

IF ib_interactive Then 
	Return uf_SendOCX(au_errorcontext)
ELSE
	Return uf_SendMAPI(au_errorcontext)
END IF
end function

private function boolean uf_sendmapi (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Private Function. DO NOT attempt to use</DESC>

<ARGS>	au_errorcontext: Error Context
			
<USAGE>	Call this function to send a message. If you are sending NONINTERACTIVE, make sure 
			all the necessary information is filled in on NON interactive or it will fail.
			This function returns true on success, false otherwise, Error Context will have contain error 
			information.</USAGE>
			
-------------------------------------------------------- */


mailSession mSes

mailReturnCode mRet
mailMessage mMsg

// Create a mail session
mSes = create mailSession

// Log on to the session
mRet = mSes.mailLogon()
IF mRet <> mailReturnSuccess! THEN
	MessageBox("Mail", 'Logon failed.')
	RETURN False
END IF

// Populate the mailMessage structure
mMsg.Subject = is_subject
mMsg.NoteText = is_textbody

IF UpperBound(imr_recipient) > 0 then
	mMsg.Recipient = imr_recipient
ELSE
	mRet = mSes.mailAddress(mMsg)
	IF mRet <> mailReturnSuccess! THEN
		MessageBox("Mail", 'Addressing failed.')
		RETURN FALSE
	END IF
END IF

mMsg.AttachmentFile = imfa_attachments

// Send the mail
mRet = mSes.mailSend(mMsg)

IF mRet <> mailReturnSuccess! THEN
	Choose Case mret
		Case mailReturnSuccess!
		Case mailReturnFailure!
			MessageBox("Mail Send", 'Mail not sent Return Failure')
		Case mailReturnInsufficientMemory!
			MessageBox("Mail Send", 'Mail not sent mailReturnInsufficientMemory!')
//		Case mailReturnLogFailure!
//			MessageBox("Mail Send", 'Mail not sent mailReturnLogFailure!')
		Case mailReturnUserAbort!
			MessageBox("Mail Send", 'Mail not sent mailReturnUserAbort!')
		Case mailReturnDiskFull!
			MessageBox("Mail Send", 'Mail not sent mailReturnDiskFull!')
		Case mailReturnTooManySessions!
			MessageBox("Mail Send", 'Mail not sent mailReturnTooManySessions!')
		Case mailReturnTooManyFiles!
			MessageBox("Mail Send", 'Mail not sent mailReturnTooManyFiles!')
		Case mailReturnTooManyRecipients!
			MessageBox("Mail Send", 'Mail not sent mailReturnTooManyRecipients!')
		Case mailReturnUnknownRecipient!
			MessageBox("Mail Send", 'Mail not sent mailReturnUnknownRecipient!')
		Case mailReturnAttachmentNotFound!
			MessageBox("Mail Send", 'Mail not sent mailReturnAttachmentNotFound!')
	End Choose
	
	RETURN FALSE
END IF

mSes.mailLogoff()
DESTROY mSes
Return True
end function

private function boolean uf_sendocx (ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Private Function. DO NOT attempt to use</DESC>

<ARGS>	au_errorcontext: Error Context
			
<USAGE>	Call this function to send a message. If you are sending NONINTERACTIVE, make sure 
			all the necessary information is filled in on NON interactive or it will fail.
			This function returns true on success, false otherwise, Error Context will have contain error 
			information.</USAGE>
			
-------------------------------------------------------- */


integer	li_Header,&
			li_result
			
Long		ll_AttachMentCount,&
			ll_LoopCount,&
			ll_SendToCount,&
			ll_MessageLength
			
String ls_Error
oleObject MapiSession,&
	  MapiMessage

mapimessage = CREATE u_mail_exceptionhandler
li_result = mapimessage.ConnectToNewObject&
	("MSMapi.MapiMessages")

Choose Case li_result
	Case -1  
		ls_Error = 'Invalid Call: the argument is the Object property of a control'
		li_Header = -1
	Case -2
		ls_Error = 'Class name not found'
		li_Header = -2
	Case -3  
		ls_Error = 'Object could not be created'
		li_Header = -3
	Case -4  
		ls_Error = 'Could not connect to object'
		li_Header = -4
	Case -9  
		ls_Error = 'Other error'
		li_Header = -9
End choose
if li_result < 0 Then 
	au_errorcontext.uf_AppendText("Creation of message object Failed:"+ ls_Error)
	au_errorcontext.uf_SetReturnCode(li_Header)
	Return False
END IF
MapiSession = CREATE u_mail_exceptionhandler
li_result = mapiSession.ConnectToNewObject&
	("MSMapi.MapiSession")

Choose Case li_result
	Case -1  
		ls_Error = 'Invalid Call: the argument is the Object property of a control'
		li_Header = -5
	Case -2
		ls_Error = 'Class name not found'
		li_Header = -6
	Case -3  
		ls_Error = 'Object could not be created'
		li_Header = -7
	Case -4  
		ls_Error = 'Could not connect to object'
		li_Header = -8
	Case -9  
		ls_Error = 'Other error'
		li_Header =  -9
End choose
if li_result < 0 Then 
	au_errorcontext.uf_AppendText("Creation of Session object Failed:"+ ls_Error)
	au_errorcontext.uf_SetReturnCode(li_Header)
	Return False
END IF


mapisession.Action = 1
MapiMessage.SessionID = MapiSession.SessionID

mapimessage.Action = 6
is_TextBody = Trim(is_TextBody)
MapiMessage.MsgNoteText = is_textbody
is_subject = Trim(is_subject)
MapiMessage.MsgSubject = is_subject
ll_MessageLength = Len(is_TextBody)
//Set MailFileDescription into Mapi Session
ll_AttachMentCount = UpperbOund(imfa_attachments)
For ll_LoopCount = 1 to ll_AttachMentCount
	IF MapiMessage.MsgIndex = -1 Then
		MapiMessage.AttachmentIndex = MapiMessage.AttachMentCount
		MapiMessage.AttachmentPosition  = ll_MessageLength
		ll_MessageLength++
		MapiMessage.AttachMentPathName = imfa_attachments[ll_LoopCount].PathName
		MapiMessage.AttachmentType = imfa_attachments[ll_LoopCount].FileType
   	MapiMessage.AttachmentName = imfa_attachments[ll_LoopCount].fileName
		END IF
NExt

// set mailRecipient	 to Mapi Session
ll_SendToCount = UpperBound(imr_recipient)
For ll_LoopCount = 1 to ll_SendToCount
	IF MapiMessage.MsgIndex = -1 Then
		MapiMessage.RecipIndex = MapiMessage.RecipCount
		Choose Case	imr_Recipient[ll_loopCount].RecipientType
			Case	mailBCC! 
				MapiMessage.Reciptype = 3
			Case mailCC!
				MapiMessage.Reciptype = 2
			Case mailOriginator!
				// You cant do this
				Continue
			Case 	mailTo!
				MapiMessage.Reciptype = 1
		End Choose
		MapiMessage.RecipAddress = imr_Recipient[ll_loopCount].Address	
		MapiMessage.RecipDisplayName = imr_Recipient[ll_loopCount].name
	END IF
NExt


IF IsValid(MapiMessage) Then
	mapimessage.Action = 2 
end if
//LogOff
MapiSession.Action = 2
mapimessage.DisconnectObject ( )
MapiSession.DisconnectObject ( )
Return True
end function

public function boolean uf_attach (string filename, mailfiletype amft_filetype, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------

<DESC>	Set list of attachments to send
			</DESC>

<ARGS>	filename: Name of file you want to send with fully qualified path
			amft_filetype: The type of attachment you want, if it is NULL mailAttach! 
			will be defaulted. For further descriptions of filetypes please read the 
			PowerBuilder documentation
			ErrorContext</ARGS>
			
<USAGE>	Call this function to set File attachments. 
			If this function returns False, the error context text contains
			the reason for the failure.</USAGE>
-------------------------------------------------------- */


Long	ll_pos,&
		ll_NumberOFAttachMents,&
		ll_PLaceHolder

String	ls_FileName,&
			ls_PAth


Do
	ll_pos = Pos(filename,"\",ll_pos + 1)
	IF ll_pos = 0 Then exit
	ll_PlaceHolder = ll_pos
Loop While True

IF ll_PLaceHolder < 1 Then
	au_errorcontext.uf_AppendText("No Path Found")
	au_ErrorContext.uf_SetReturnCode(-1)
END IF


ls_Path  = filename
ls_FileName = Mid(filename,ll_PLaceHolder + 1)


ll_NumberOFAttachMents = UpperBound(imfa_attachments)+1
imfa_attachments[ll_NumberOFAttachMents].filename = ls_FileNAme
imfa_attachments[ll_NumberOFAttachMents].PathNAme = ls_Path
IF IsNull(amft_filetype) Then amft_filetype = mailAttach!
imfa_attachments[ll_NumberOFAttachMents].filetype = amft_filetype	 



Return True
end function

event constructor;
/* --------------------------------------------------------
u_Mail

<OBJECT>	This a MAPI interface.
			uses MSMAPI32.OCX</OBJECT>
			
<USAGE>	Allows interactive and NON interactive modes.
			On initialize of the object pass TRUE for interactive use.
			
			Example:
			
			Sends a mail interactive with the user  Assuming au_ClassFactory and au_ErrorContext
			are passed as Parameters
			<PRE>
			u_mail	lu_mail
			Boolean  lb_Initialized

			lb_Initialized = au_ClassFactory.GetObject("u_mail",lu_Mail,au_ErrorContext)
			IF Not au_ErrorContext.uf_IsSuccesful()
				// Do some error Handling
			ELSE
				IF Not lb_Initialized Then &
					lu_Mail.uf_Initialize(lu_ClassFactory,TRUE,lu_ErrorContext)
				// You may want to put more error checking here	
				lu_mail.uf_Address(lu_ErrorContext)
				lu_mail.uf_Attach("C:\Netwise.ini",lu_ErrorContext)
				lu_mail.uf_SetBody("Hello World!!")
				lu_mail.uf_SetSubject("Testing MAPI")
				lu_Mail.uf_Send(lu_ErrorContext)
			END IF
			</PRE>
			</USAGE>


<AUTH>	Jim Weier</AUTH>

--------------------------------------------------------- */

end event

on u_mail.create
TriggerEvent( this, "constructor" )
end on

on u_mail.destroy
TriggerEvent( this, "destructor" )
end on

