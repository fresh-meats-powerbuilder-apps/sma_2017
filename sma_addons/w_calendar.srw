HA$PBExportHeader$w_calendar.srw
forward
global type w_calendar from Window
end type
type dw_calendar from datawindow within w_calendar
end type
type cb_cancel from commandbutton within w_calendar
end type
type cb_previous from commandbutton within w_calendar
end type
type cb_next from commandbutton within w_calendar
end type
end forward

global type w_calendar from Window
int X=823
int Y=356
int Width=800
int Height=856
boolean TitleBar=true
boolean ControlMenu=true
WindowType WindowType=response!
dw_calendar dw_calendar
cb_cancel cb_cancel
cb_previous cb_previous
cb_next cb_next
end type
global w_calendar w_calendar

type variables
Private:
u_AbstractCalendar	iu_Calendar
u_AbstractParameterStack	iu_ParameterStack
end variables

on w_calendar.create
this.dw_calendar=create dw_calendar
this.cb_cancel=create cb_cancel
this.cb_previous=create cb_previous
this.cb_next=create cb_next
this.Control[]={this.dw_calendar,&
this.cb_cancel,&
this.cb_previous,&
this.cb_next}
end on

on w_calendar.destroy
destroy(this.dw_calendar)
destroy(this.cb_cancel)
destroy(this.cb_previous)
destroy(this.cb_next)
end on

event open;
/* --------------------------------------------------------
w_calendar

<OBJECT>	This object is a utility to display a calendar.
			The calendar-related processing is encapsulated in an object
			inherited from u_AbstractCalendar (currently u_calendar).
			</OBJECT>
			
<USAGE>	Open this response window with a parameter stack consisting of 
			the following objects pushed onto the stack in the given order:
			<UL>
			<LI>u_AbstractClassFactory:  Class factory</LI>
			<LI>String:  String representation of the initial date. 
			If it is not a valid date, Today() will be used.</LI>
			<LI>String:  DataWindow object to use</LI>
			<LI>u_AbstractErrorContext: Error context</LI>
			</UL>
			The window returns a parameter stack consisting of the
			following objects to be popped off the stack in the given
			order:
			<UL>
			<LI>u_AbstractErrorContext: Error context</LI>
			<LI>String: String representation of the chosen date.</LI>
			</UL>
			If the error context has a nonzero return code, the
			string representation of the chosen date is not on the
			parameter stack.
			</USAGE>

<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
u_AbstractErrorContext		lu_ErrorContext
u_AbstractClassFactory		lu_ClassFactory

String	ls_ObjectName,&
			ls_InitDate

iu_ParameterStack = Message.PowerObjectParm

IF Not iu_ParameterStack.uf_Pop("u_AbstractErrorContext", lu_ErrorContext) Then 
	lu_ErrorContext = Create Using "u_ErrorContext"
	lu_ErrorContext.uf_Initialize()
	lu_ErrorContext.uf_SetReturnCode(-1)
	lu_ErrorContext.uf_AppendText("Missing Parameter: Error Context")
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not iu_ParameterStack.uf_Pop("String", ls_ObjectName) Then
	lu_ErrorContext.uf_AppendText("Missing Parameter: DataObject")
	lu_ErrorContext.uf_SetReturnCode(-2)
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not iu_ParameterStack.uf_Pop("String", ls_InitDate) Then
	lu_ErrorContext.uf_AppendText("Missing Parameter: Initial Date")
	lu_ErrorContext.uf_SetReturnCode(-3)
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not iu_ParameterStack.uf_Pop("u_AbstractClassFactory", lu_ClassFactory) Then
	lu_ErrorContext.uf_AppendText("Missing Parameter: Class Factory")
	lu_ErrorContext.uf_SetReturnCode(-4)
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not IsDate(ls_InitDate) Then ls_InitDate = String(Today(),"mm/dd/yyyy")

lu_ClassFactory.uf_GetObject("u_calendar", iu_calendar, lu_ErrorContext) 
If Not IsValid(iu_calendar) Then
	lu_ErrorContext.uf_SetReturnCode(-5)
	lu_ErrorContext.uf_AppendText("Can't get calendar object")
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
ELSE
	IF Not iu_Calendar.uf_Initialize(lu_ClassFactory, dw_calendar, ls_ObjectName, &
										ls_InitDate, lu_ErrorContext)	Then
		lu_ErrorContext.uf_AppendText("Can't initialize calendar object")
		lu_ErrorContext.uf_SetReturnCode(-6)
		iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
		CloseWithReturn(This, iu_ParameterStack)
		Return
	ELSE
		This.Title = iu_Calendar.uf_Display()
	END IF
END IF

Return

end event

event key;
/* --------------------------------------------------------

<DESC>	Calls a function on the calendar user object 
			depending on the key the user presses. The keys 
			handled are:
			<UL>
			<LI>KeyPageUp!</LI>
			<LI>KeyPageDown!</LI>
			<LI>KeyLeftArrow!</LI>
			<LI>KeyUpArrow!</LI>
			<LI>KeyRightArrow!</LI>
			<LI>KeyDownArrow!</LI>
			<LI>KeyEnter!</LI>
			</UL>
			If the user presses KeyEnter!, the date chosen is
			pushed onto the parameter stack as a string (after
			a successful error context) and the window closes.
</DESC>

-------------------------------------------------------- */
String	ls_Date
u_AbstractErrorContext	lu_ErrorContext

Choose Case key
	Case	KeyPageUp!
		This.Title = iu_Calendar.uf_KeyPageUp()
	Case	KeyPageDown!
		This.Title = iu_Calendar.uf_KeyPageDown()		
	Case	KeyLeftArrow!
		This.Title = iu_Calendar.uf_KeyLeftArrow()
	Case	KeyUpArrow!
		This.Title = iu_Calendar.uf_KeyUpArrow()	
	Case	KeyRightArrow!
		This.Title = iu_Calendar.uf_KeyRightArrow()
	Case	KeyDownArrow!
		This.Title = iu_Calendar.uf_KeyDownArrow()
	Case KeyEnter!
		ls_Date = iu_calendar.uf_KeyEnter()
		lu_ErrorContext = CREATE USING "u_ErrorContext"
		lu_ErrorContext.uf_Initialize()
		lu_ErrorContext.uf_SetReturnCode(0)
		iu_parameterstack.uf_Push("String",ls_Date)
		iu_parameterstack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)
		CloseWithReturn(This, iu_parameterstack)
	CASE ELSE
End Choose
		
end event

type dw_calendar from datawindow within w_calendar
int X=37
int Y=136
int Width=695
int Height=484
int TabOrder=30
BorderStyle BorderStyle=StyleLowered!
boolean LiveScroll=true
end type

event constructor;This.InsertRow(0)
end event

event clicked;
/* --------------------------------------------------------

<DESC>	Happens when user clicks on the DataWindow control.

Usage:	If a valid date is returned by the calendar object the string
			representation of the date is pushed onto the stack and the
			window is closed.
			
</DESC>
-------------------------------------------------------- */

String	ls_Date
u_AbstractErrorContext lu_ErrorContext

if Row < 1 then Return

ls_Date = iu_calendar.uf_Clicked(row, String(dwo.Name))
IF NOT IsDate(ls_Date) Then return

lu_ErrorContext = CREATE USING "u_ErrorContext"
lu_ErrorContext.uf_Initialize()
lu_ErrorContext.uf_SetReturnCode(0)
iu_ParameterStack.uf_Push("String", ls_Date)
iu_parameterstack.uf_Push("u_AbstractErrorContext", lu_ErrorContext)
CloseWithReturn(Parent,iu_ParameterStack)

end event

type cb_cancel from commandbutton within w_calendar
int X=37
int Y=624
int Width=690
int Height=96
int TabOrder=30
string Text="Cancel"
boolean Cancel=true
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;

/* --------------------------------------------------------

<DESC>	Calls uf_Cancel() on the calendar UserObject.
			Pushes the string that represents cancel to the calling 
			window onto the stack and closes the window.
</DESC>

-------------------------------------------------------- */
String	ls_CancelReturn
u_AbstractErrorContext lu_ErrorContext

ls_CancelReturn  = iu_calendar.uf_Cancel()

lu_ErrorContext = CREATE USING "u_ErrorContext"
lu_ErrorContext.uf_Initialize()
lu_ErrorContext.uf_SetReturnCode(0)
iu_ParameterStack.uf_Push("String", ls_CancelReturn)
iu_ParameterStack.uf_Push("u_AbstractErrorContext", lu_ErrorContext)
CloseWithReturn(Parent, iu_parameterstack)
end event

type cb_previous from commandbutton within w_calendar
int X=37
int Y=36
int Width=343
int Height=96
int TabOrder=20
string Text="<<&Previous"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Calls uf_Previous() on the calendar UserObject.
			Sets the title appropriately.
			</DESC>
-------------------------------------------------------- */
Parent.Title = iu_calendar.uf_Previous()
end event

type cb_next from commandbutton within w_calendar
int X=375
int Y=36
int Width=343
int Height=96
int TabOrder=10
string Text="&Next>>"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Calls uf_next() on the calendar UserObject.
			Sets the title appropriately.
			
</DESC>
-------------------------------------------------------- */
Parent.Title = iu_calendar.uf_Next()
end event

