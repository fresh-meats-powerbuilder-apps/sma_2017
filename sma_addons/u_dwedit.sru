HA$PBExportHeader$u_dwedit.sru
forward
global type u_dwedit from nonvisualobject
end type
end forward

global type u_dwedit from nonvisualobject
end type
global u_dwedit u_dwedit

type variables
Private:
DataWindow 	idw_Data

Long		ii_Find_LastRowFound

end variables

forward prototypes
public function boolean uf_initialize (ref datawindow adw_data)
public subroutine uf_filter (ref datawindow adw_data)
public subroutine uf_filter ()
public subroutine uf_sort (ref datawindow adw_data)
public subroutine uf_sort ()
public function long uf_find (string as_expression, integer ai_method, boolean ab_wrap, boolean ab_startatbegining, datawindow adw_data)
public function long uf_find (string as_expression, integer ai_method, boolean ab_wrap, boolean ab_StartAtBegining)
public function long uf_find (string as_expression, integer ai_method, boolean ab_wrap)
public function long uf_find (string as_expression, integer ai_method)
public function long uf_find (string as_expression)
end prototypes

public function boolean uf_initialize (ref datawindow adw_data);
/* --------------------------------------------------------
uf_initialize()

<DESC>	Initialize the Edit Object
			</DESC>

<ARGS>	adw_data: DataWindow on which all processing will occur
</ARGS>
			
<USAGE>	Call this function when all edit functionality will occur
			on the same datawindow.
</USAGE>
-------------------------------------------------------- */

idw_data = adw_Data
return True
end function

public subroutine uf_filter (ref datawindow adw_data);
/* --------------------------------------------------------
uf_filter()

<DESC>	Filter the DataWindow passed as an argument
</DESC>

<ARGS>	adw_Data: DataWindow to filter </ARGS>
<USAGE>	Call this function to open the standard PowerBuilder
			Filter window and filter the datawindow passed as 
			an argument.</USAGE>
-------------------------------------------------------- */

String	ls_Null

SetNull(ls_Null)
adw_Data.SetFilter(ls_Null)

adw_Data.Filter()

return
end subroutine

public subroutine uf_filter ();
/* --------------------------------------------------------
uf_filter()

<DESC>	Filter the DataWindow passed as an argument to 
			uf_initialize
			</DESC>

<ARGS></ARGS>
<USAGE>	Call this function to open the standard PowerBuilder
			Filter window and filter the datawindow passed as 
			an argument to uf_initialize.</USAGE>
-------------------------------------------------------- */
This.uf_Filter(idw_data)
end subroutine

public subroutine uf_sort (ref datawindow adw_data);
/* --------------------------------------------------------
uf_sort()

<DESC>	Sort the DataWindow passed as an argument
</DESC>

<ARGS>	adw_data: Datawindow to sort</ARGS>
<USAGE>	Call this function to open the standard PowerBuilder
			Sort window and Sort the datawindow passed as 
			an argument.</USAGE>
-------------------------------------------------------- */
String			ls_Null


// Pop up the default PB Filter Dialog
SetNull(ls_Null)
adw_Data.SetSort(ls_Null)
adw_Data.Sort()

return
end subroutine

public subroutine uf_sort ();
/* --------------------------------------------------------
uf_Sort()

<DESC>	Sort the DataWindow passed as an argument to 
			uf_initialize
			</DESC>

<ARGS></ARGS>
<USAGE>	Call this function to open the standard PowerBuilder
			Sort window and Sort the datawindow passed as 
			an argument to uf_initialize.</USAGE>
-------------------------------------------------------- */
This.uf_Sort(idw_data)
return
end subroutine

public function long uf_find (string as_expression, integer ai_method, boolean ab_wrap, boolean ab_startatbegining, datawindow adw_data);
/* --------------------------------------------------------
uf_find()
<DESC> Return the number of the next row that satisfies the search criteria
</DESC>

<ARGS>   as_expression: Expression to find (See Find() Powerscript function for details
			ai_method: Should this search up or down (1 = Up, 2 = Down)
			ab_wrap: Should this wrap at the beginning or end (True = Wrap, False = No Wrap)
			ab_StartAtBegining: Start searching from row 1 (True = Start at row 1, False = Start at current row or last found row)
			adw_Data: DataWindow to search
</ARGS>

<USAGE> Call this function to find text within a DataWindow,
			This will return 0 when no more rows are found.
			It may be called multiple times to find multiple rows
</USAGE>
-------------------------------------------------------- */
Long	ll_Row, &
		ll_StartRow, &
		ll_EndRow


If ab_startatbegining Or ii_Find_LastRowFound = 0 Then
	If ai_Method = 2 Then
		ll_StartRow = 1
	Else
		ll_StartRow = adw_Data.RowCount()
	End if
Elseif ai_method = 1 Then
	ll_StartRow = ii_find_lastrowfound - 1
Else
	ll_StartRow = ii_find_lastrowfound + 1
End if

If ai_method = 1 Then
	ll_EndRow = 1
	//If we are already at the first row, kludge it to make it work
	If ll_StartRow < ll_EndRow Then ll_EndRow = ll_StartRow
Else
	ll_EndRow = adw_data.RowCount()
	If ll_StartRow > ll_EndRow Then ll_EndRow = ll_StartRow
End if
	
ll_Row = adw_data.Find(as_expression, ll_StartRow, ll_EndRow)

If ll_Row > 0 Then 
	ii_find_LastRowFound = ll_Row
	return ll_Row
End if

If ab_wrap Then
	If ai_method = 1 Then
		// start at bottom and go until Last Row Found
		ll_StartRow = adw_Data.RowCount()
		ll_EndRow = ii_Find_LastRowFound
	Else
		// Start at top and go until Last Row Found
		ll_StartRow = 1
		ll_EndRow = ii_Find_LastRowFound
	End if
	ll_Row = adw_data.Find(as_expression, ll_StartRow, ll_EndRow)
End if

ii_Find_LastRowFound = ll_Row
return ll_Row
end function

public function long uf_find (string as_expression, integer ai_method, boolean ab_wrap, boolean ab_StartAtBegining);
/* --------------------------------------------------------
uf_find()
<DESC> Return the number of the next row that satisfies the search criteria
</DESC>

<ARGS>   as_expression: Expression to find (See Find() Powerscript function for details
			ai_method: Should this search up or down (1 = Up, 2 = Down)
			ab_wrap: Should this wrap at the beginning or end (True = Wrap, False = No Wrap)
			ab_StartAtBegining: Start searching from row 1 (True = Start at row 1, False = Start at current row or last found row)
</ARGS>

<USAGE> Call this function to find text within a DataWindow,
			This will return 0 when no more rows are found.
			It may be called multiple times to find multiple rows.
			The datawindow will be the object that was passed to uf_initialize()
</USAGE>
-------------------------------------------------------- */

return This.uf_Find(as_expression, ai_method, ab_wrap, ab_StartAtBegining, idw_data)

end function

public function long uf_find (string as_expression, integer ai_method, boolean ab_wrap);
/* --------------------------------------------------------
uf_find()
<DESC> Return the number of the next row that satisfies the search criteria
</DESC>

<ARGS>   as_expression: Expression to find (See Find() Powerscript function for details
			ai_method: Should this search up or down (1 = Up, 2 = Down)
			ab_wrap: Should this wrap at the beginning or end (True = Wrap, False = No Wrap)
</ARGS>

<USAGE> Call this function to find text within a DataWindow,
			This will return 0 when no more rows are found.
			It may be called multiple times to find multiple rows.
			The datawindow will be the object that was passed to uf_initialize()
			Searching will start at the current row or the last found row.
</USAGE>
-------------------------------------------------------- */

return This.uf_Find(as_expression, ai_method, ab_wrap, False, idw_data)

end function

public function long uf_find (string as_expression, integer ai_method);
/* --------------------------------------------------------
uf_find()
<DESC> Return the number of the next row that satisfies the search criteria
</DESC>

<ARGS>   as_expression: Expression to find (See Find() Powerscript function for details
			ai_method: Should this search up or down (1 = Up, 2 = Down)
</ARGS>

<USAGE> Call this function to find text within a DataWindow,
			This will return 0 when no more rows are found.
			It may be called multiple times to find multiple rows.
			The datawindow will be the object that was passed to uf_initialize()
			Searching will start at the current row or the last 
			found row and will not wrap.
</USAGE>
-------------------------------------------------------- */

return This.uf_Find(as_expression, ai_method, False, False, idw_data)

end function

public function long uf_find (string as_expression);
/* --------------------------------------------------------
uf_find()
<DESC> Return the number of the next row that satisfies the search criteria
</DESC>

<ARGS>   as_expression: Expression to find (See Find() Powerscript function for details
</ARGS>

<USAGE> Call this function to find text within a DataWindow,
			This will return 0 when no more rows are found.
			It may be called multiple times to find multiple rows.
			The datawindow will be the object that was passed to uf_initialize()
			Searching will start at the current row or the last 
			found row and will not wrap.
			This will search down only.
</USAGE>
-------------------------------------------------------- */

return This.uf_Find(as_expression, 2, False, False, idw_data)

end function

on u_dwedit.create
TriggerEvent( this, "constructor" )
end on

on u_dwedit.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_dwEdit

<OBJECT>	This is an object that handles the work from the
			Edit Menu. 			
</OBJECT>
			
<USAGE>	Declare an instance of this object in your datawindow.
			When you recieve a message from the menu that some edit
			functionality was triggered, call the appropriate
			function on this object.
</USAGE>

<AUTH>	Tim Bornholtz	</AUTH>

--------------------------------------------------------- */
end event

