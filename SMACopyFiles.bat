ECHO OFF

set FromDir=C:\IBP\PB8\SMA
set ToDir=C:\PBTEST32\SMA


XCopy %FromDir%\SMA.EXE				%ToDir% /Y
XCopy %FromDir%\sma.pbd				%ToDir% /Y
XCopy %FromDir%\sma_abstract\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_abstractinterface\*.PBD	%ToDir% /Y
XCopy %FromDir%\sma_addons\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_classfactory\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_dddw\*.PBD			%ToDir% /Y
XCopy %FromDir%\sma_defaultmanager\*.PBD	%ToDir% /Y
XCopy %FromDir%\sma_ext\*.PBD			%ToDir% /Y
XCopy %FromDir%\sma_fwh\*.PBD			%ToDir% /Y
XCopy %FromDir%\sma_groups\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_login\*.PBD			%ToDir% /Y
XCopy %FromDir%\sma_mpr\*.PBD			%ToDir% /Y
XCopy %FromDir%\sma_netwise\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_notification\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_productgroup\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_rebates\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_transfer\*.PBD		%ToDir% /Y
XCopy %FromDir%\sma_category\*.PBD		%ToDir% /Y