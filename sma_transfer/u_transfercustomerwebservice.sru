HA$PBExportHeader$u_transfercustomerwebservice.sru
forward
global type u_transfercustomerwebservice from u_transfercustomerdataaccess
end type
end forward

global type u_transfercustomerwebservice from u_transfercustomerdataaccess
end type
global u_transfercustomerwebservice u_transfercustomerwebservice

forward prototypes
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the Transfer Customer
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Transfer Customers
</USAGE> from a rpc on the main frame using SMA001.dll
-------------------------------------------------------- */

String							ls_UserID, &
									ls_Password, &
									ls_InputString, &
									ls_OutputString, &
									ls_GroupID
								
u_smas01sr						lu_smas01sr

u_AbstractdataAccess			lu_User

u_AbstractParameterStack	lu_stack

u_AbstractClassFactory		lu_ClassFactory

u_AbstractErrorContext		lu_ErrorContext


au_ParameterStack.uf_Pop('string', ls_inputstring)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If Not lu_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If

End if

If Not lu_ClassFactory.uf_GetObject("u_user", lu_user, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(lu_ClassFactory, lu_ErrorContext) Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if
	
lu_User.uf_retrieve(lu_Stack, lu_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", ls_userid)

If Not lu_ClassFactory.uf_GetObject("u_smas01sr", lu_smas01sr, lu_ErrorContext) Then
	If Not lu_ErrorContext.uf_IsSuccessful() Then
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
	If Not lu_smas01sr.uf_Initialize(lu_ClassFactory, ls_UserID, ls_Password, lu_ErrorContext) Then 
		au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
		au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
		au_ParameterStack.uf_Push('string', ls_outputstring)
		Return False
	End If
End if

lu_smas01sr.uf_smas02er_get_transfer_customers("w_TransferCustomerInquire", &
													"uf_retrieve", &
													"", &
													ls_userid, &
													ls_password, &
													ls_inputstring, &
													ls_outputstring, &
													lu_ErrorContext)

If Not lu_ErrorContext.uf_IsSuccessful() Then 
	au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
	au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
	au_ParameterStack.uf_Push('string', ls_outputstring)
	return False
End If

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
au_ParameterStack.uf_Push('string', ls_outputstring)

return True
end function

on u_transfercustomerwebservice.create
call super::create
end on

on u_transfercustomerwebservice.destroy
call super::destroy
end on

