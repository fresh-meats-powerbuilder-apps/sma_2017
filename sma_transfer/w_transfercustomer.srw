HA$PBExportHeader$w_transfercustomer.srw
forward
global type w_transfercustomer from w_abstractsheetext
end type
type dw_header from u_abstractdwext within w_transfercustomer
end type
type dw_detail from u_abstractdwext within w_transfercustomer
end type
end forward

global type w_transfercustomer from w_abstractsheetext
integer x = 146
integer y = 300
integer width = 2075
integer height = 756
string title = "Transfer Customer"
boolean toolbarvisible = false
dw_header dw_header
dw_detail dw_detail
end type
global w_transfercustomer w_transfercustomer

type variables
String					is_Title

u_AbstractClassFactory			iu_ClassFactory

u_AbstractPlantDataAccess			iu_PlantDataAccess

u_AbstractTutlTypeDataAccess		iu_TutlTypeDataAccess

u_TransferCustomerDataAccess	iu_TransferCustomerDataAccess

u_TransCustWindowDataAccess	iu_TransCustWindowDataAccess

u_AbstractDataAccess			iu_TransferTypeDataAccess

u_ErrorContext				iu_ErrorContext

u_NotificationController			iu_Notification	

w_smaframe				iw_frame	
end variables

forward prototypes
public function boolean wf_fillplantdropdown ()
public function boolean wf_fillplanttypedropdown ()
public function boolean wf_fillcustomerdropdown (string as_transfercustomer)
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean wf_validate ()
public function boolean wf_fillprodrecasdropdown ()
public function string wf_buildupdatestring ()
end prototypes

public function boolean wf_fillplantdropdown ();Boolean								lb_temp

Long									ll_temp

String								ls_PlantCode, &
										ls_plantString
							
DataWindowChild					ldwc_destinationPlant, &
										ldwc_FreightPlant
										
u_AbstractParameterStack		lu_ParameterStack	



If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_PlantDataAccess", iu_PlantDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_PlantDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_PlantCode = ''
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_PlantCode)

iu_PlantDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_PlantString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_detail.GetChild('destination_plant', ldwc_DestinationPlant)
ll_temp = ldwc_DestinationPlant.ImportString(ls_PlantString)

Return True



end function

public function boolean wf_fillplanttypedropdown ();Long									ll_temp

String								ls_tutltype, &
										ls_TransferTypeString
							
DataWindowChild					ldwc_TransferType
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'TFERTYPE'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_TransferTypeString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_detail.GetChild('Transfer_type', ldwc_TransferType)
ll_temp = ldwc_TransferType.ImportString(ls_TransferTypeString)

Return True

end function

public function boolean wf_fillcustomerdropdown (string as_transfercustomer);Boolean								lb_temp

Long									ll_temp

String								ls_TransferCustomer, &
										ls_TransferCustomerString
							
DataWindowChild					ldwc_TransferCustomer
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TransferCustomersDataAccess", iu_TransferCustomerDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

as_TransferCustomer = as_transfercustomer + '~tS'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', as_TransferCustomer)

iu_TransferCustomerDataAccess.uf_Retrieve( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_TransferCustomerString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_header.GetChild('customer_number', ldwc_TransferCustomer)
ldwc_TransferCustomer.ImportString(ls_TransferCustomerString)

Return True

end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);iu_ClassFactory = au_ClassFactory

iu_ErrorContext = au_ErrorContext

If Not iu_ClassFactory.uf_GetObject( "u_NotificationController", iu_Notification, iu_ErrorContext ) Then
	iu_Notification.uf_Initialize(iu_ClassFactory)
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_Frame, iu_ErrorContext ) Then Return False

If Not wf_FillCustomerDropDown(' ') Then Return False
If Not wf_FillPlantDropDown() Then Return False
If Not wf_FillPlantTypeDropDown() Then Return False
If Not wf_fillprodrecasdropdown() Then Return False


If Not iu_ClassFactory.uf_GetObject( "u_TransCustWindowDataAccess", iu_TransCustWindowDataAccess, iu_ErrorContext ) Then
	If Not iu_ErrorContext.uf_IsSuccessful( ) Then
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

dw_detail.ResetUpdate()

This.Event Post ue_inquire()

Return True

end function

public function boolean wf_validate ();String									ls_temp

u_String_Functions					lu_string


ls_temp = dw_detail.GetItemString(1, 'destination_plant')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Destination Plant is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('destination_plant')
	Return False
End If

ls_temp = dw_detail.GetItemString(1, 'freight_associated_plant')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Freight Associated Plant is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('destination_plant')
	Return False
End If

ls_temp = dw_detail.GetItemString(1, 'transfer_type')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Transfer Type is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('destination_plant')
	Return False
End If

ls_temp = dw_detail.GetItemString(1, 'prod_rec_as')

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Product received as is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('destination_plant')
	Return False
End If

Return True
end function

public function boolean wf_fillprodrecasdropdown ();Long									ll_temp

String								ls_tutltype, &
										ls_ProdRecAsString
							
DataWindowChild					ldwc_ProdRecAs
							
u_AbstractParameterStack		lu_ParameterStack	


If Not iu_ClassFactory.uf_GetObject( "u_ParameterStack", lu_ParameterStack, iu_ErrorContext ) Then
	lu_ParameterStack.uf_Initialize()
End If

If Not iu_ClassFactory.uf_GetObject( "u_TutlTypeDataAccess", iu_TutlTypeDataAccess, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
		lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
		iu_TutlTypeDataAccess.uf_Initialize( lu_ParameterStack )
		lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)
		lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
		If Not iu_ErrorContext.uf_IsSuccessful() Then
			iu_Notification.uf_Display(iu_ErrorContext)
			Return False
		End If
	Else 
		iu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

ls_TutlType = 'PRODRECD'
//ls_TutlType = 'YLDSPECS'

lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TutlType)

iu_TutlTypeDataAccess.uf_Retrieve(lu_ParameterStack)

lu_ParameterStack.uf_Pop('string', ls_ProdRecAsString)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_detail.GetChild('prod_rec_as', ldwc_ProdRecAs)
ll_temp = ldwc_ProdRecAs.ImportString(ls_ProdRecAsString)

Return True

end function

public function string wf_buildupdatestring ();Long					ll_DeletedCount, &
						ll_ModifiedCount, &
						ll_count
						
String				ls_UpdateString

dwItemStatus		lis_UpdateStatus


ll_DeletedCount = dw_detail.DeletedCount()
ll_ModifiedCount = dw_detail.ModifiedCount()

If ll_DeletedCount + ll_ModifiedCount <= 0 Then
	SetMicroHelp('No Update Necessary')
	Return ''
End If

ls_UpdateString = ''

IF ll_DeletedCount > 0 Then
	dw_detail.RowsMove( 1, 10000, Primary!, dw_detail, 10000, Filter!)
	dw_detail.RowsMove( 1, 10000, Delete!, dw_detail, 10000, Primary!)
	For ll_count = 1 to dw_detail.RowCount()
		ls_UpdateString += dw_detail.GetItemString(ll_Count, 'destination_plant') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'freight_associated_plant') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'transfer_type') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'prod_rec_as') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'include_bod_ind') + '~t' + &
								'D~r~n'
	Next
	dw_detail.RowsMove( 1, 10000, Primary!, dw_detail, 100000, Delete!)
	dw_detail.RowsMove( 1, 10000, Filter!, dw_detail, 100000, Primary!)
End If	

ll_Count = 0
IF ll_ModifiedCount > 0 Then
	If Not wf_validate() Then Return ''
	ll_Count = dw_detail.GetNextModified(ll_Count, Primary!)
	Do
		dw_detail.SelectRow(0, False)
		dw_Detail.SelectRow(ll_count, True)
		lis_UpdateStatus = dw_detail.GetItemStatus(ll_Count, 0, Primary!)
		Choose Case lis_UpdateStatus
			Case NewModified!
		ls_UpdateString += dw_detail.GetItemString(ll_Count, 'destination_plant') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'freight_associated_plant') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'transfer_type') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'prod_rec_as') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'include_bod_ind') + '~t' + &
								'I~r~n'
			Case DataModified!
		ls_UpdateString += dw_detail.GetItemString(ll_Count, 'destination_plant') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'freight_associated_plant') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'transfer_type') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'prod_rec_as') + '~t' + &
								dw_detail.GetItemString(ll_Count, 'include_bod_ind') + '~t' + &
								'U~r~n'
			Case Else
				SetMicroHelp('Update Status is ' + String(lis_UpdateStatus) + &
				'.  Please Call Applications.')
				Return ''
		End Choose
		ll_Count = dw_detail.GetNextModified(ll_Count, Primary!)
	Loop While ll_Count > 0
End If	

dw_detail.SelectRow(0, False)

Return ls_UpdateString

end function

on w_transfercustomer.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_detail
end on

on w_transfercustomer.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_detail)
end on

event ue_inquire;String					ls_TransferCustomer,&
							ls_TransCustWindowString
							
Long						ll_row

DataWindowChild		ldwc_TransferCustomer

u_ParameterStack		lu_ParameterStack


iu_ErrorContext.uf_Initialize()

This.Event Trigger CloseQuery()

If Message.ReturnValue <> 0 Then Return False

ls_TransferCustomer = dw_header.GetItemString(1, 'customer_number')
dw_header.GetChild('customer_number', ldwc_TransferCustomer)

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

lu_ParameterStack.uf_Push('u_ErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('string', ls_TransferCustomer)
lu_ParameterStack.uf_Push('string', is_Title)
lu_ParameterStack.uf_Push('datawindowchild', ldwc_TransferCustomer)
lu_ParameterStack.uf_Push('window', iw_frame)
lu_ParameterStack.uf_Push('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Push('u_TransferCustomerDataAccess', iu_TransferCustomerDataAccess)

iu_ClassFactory.uf_GetResponseWindow( "w_TransferCustomerInquire", lu_ParameterStack )
lu_ParameterStack.uf_Pop('u_TransferCustomerDataAccess', iu_TransferCustomerDataAccess)
lu_ParameterStack.uf_Pop('u_NotificationController', iu_notification)
lu_ParameterStack.uf_Pop('u_ClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('window', iw_frame)
lu_ParameterStack.uf_Pop('datawindowchild', ldwc_TransferCustomer)
lu_ParameterStack.uf_Pop('string', is_Title)
lu_ParameterStack.uf_Pop('string', ls_TransferCustomer)
lu_ParameterStack.uf_Pop('u_ErrorContext', iu_ErrorContext)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

If ls_TransferCustomer = 'Cancel' Then Return False

dw_header.SetItem(1, 'Customer_number', ls_TransferCustomer)

dw_detail.Reset()

lu_ParameterStack.uf_initialize()

SetPointer(HourGlass!)

ls_TransferCustomer += '~tS'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_TransferCustomer)
iu_TransCustWindowDataAccess.uf_Retrieve( lu_ParameterStack )
lu_ParameterStack.uf_Pop('string', ls_TransCustWindowString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

ll_row = dw_detail.ImportString(ls_TransCustWindowString)

If ll_row > 0 Then
	SetMicroHelp(String(ll_row) + ' Rows Retrieved')
Else
	SetMicroHelp('No Rows Retrieved')
	dw_detail.InsertRow(0)
End If

dw_detail.ResetUpdate()

Return False

end event

event closequery;If dw_detail.ModifiedCount() + dw_detail.DeletedCount() > 0 Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
//			Message.ReturnValue = 1	 // Update failed - do not close window
			Return 1
		Else
//			Message.ReturnValue = 0
		End If

	CASE 2	// Do not save changes
//		Message.ReturnValue = 0

	CASE 3	// Cancel the closing of window
//		Message.ReturnValue = 1
		Return 1
	END CHOOSE
End If
Return 0
end event

event open;is_Title = This.Title

end event

event ue_save;String					ls_HeaderString,&
							ls_DetailString
							
Long						ll_row

DataWindowChild						ldwc_TransferCustomer

u_ParameterStack						lu_ParameterStack

u_String_Functions					lu_strings


iu_ErrorContext.uf_Initialize()

IF dw_detail.AcceptText() = -1 THEN Return False

ls_HeaderString = dw_header.GetItemString(1, 'customer_number')

If lu_strings.nf_IsEmpty(ls_HeaderString) Then
	SetMicroHelp("Please inquire on a customer before updating")
	Return False
End If

This.SetRedraw(False)

ls_DetailString = wf_BuildUpdateString()

If lu_strings.nf_IsEmpty(ls_DetailString) Then 
	This.SetRedraw(True)
	Return False
End If

SetMicroHelp("Wait... Updating the Database")

iu_ClassFactory.uf_GetObject('u_parameterstack', lu_parameterstack)

SetPointer(HourGlass!)

ls_HeaderString += '~tS~t'
lu_ParameterStack.uf_Push('u_AbstractErrorContext', iu_ErrorContext)
lu_ParameterStack.uf_Push('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Push('string', ls_HeaderString)
lu_ParameterStack.uf_Push('string', ls_DetailString)

iu_TransCustWindowDataAccess.uf_Update( lu_ParameterStack )

lu_ParameterStack.uf_Pop('string', ls_DetailString)
lu_ParameterStack.uf_Pop('string', ls_HeaderString)
lu_ParameterStack.uf_Pop('u_AbstractClassFactory', iu_ClassFactory)
lu_ParameterStack.uf_Pop('u_AbstractErrorContext', iu_ErrorContext)

SetPointer(Arrow!)

This.SetRedraw(True)

If Not iu_ErrorContext.uf_IsSuccessful( ) Then
	iu_Notification.uf_Display(iu_ErrorContext)
	Return False
End If

dw_detail.ResetUpdate()

SetMicroHelp("Update Successful")

Return True

end event

event ue_delete;dw_detail.DeleteRow(1)
dw_detail.InsertRow(0)
end event

type dw_header from u_abstractdwext within w_transfercustomer
integer y = 8
integer width = 1806
integer height = 80
boolean bringtotop = true
string dataobject = "d_customer"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event constructor;DataWindowChild					ldwc_code, &
										ldwc_descr
										

This.InsertRow(0)

This.GetChild('customer_number', ldwc_code)
This.GetChild('customer_number_descr', ldwc_descr)
ldwc_code.ShareData(ldwc_descr)


end event

type dw_detail from u_abstractdwext within w_transfercustomer
integer x = 46
integer y = 140
integer width = 1874
integer height = 428
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_transfercustomerdetail"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event itemchanged;DataWindowChild						ldwc_PlantList

u_String_Functions					lu_string


This.GetChild('destination_plant', ldwc_PlantList)

Choose Case dwo.name
	Case 'destination_plant'
		If ldwc_PlantList.Find('plant_code = "' + data + '"', &
				1, ldwc_PlantList.RowCount() + 1) <= 0 Then
			SetMicroHelp('This is an invalid Plant')
			This.SetFocus()
			This.SelectText(1, 1000)
			Return 1
		End If
		If lu_string.nf_IsEmpty(This.GetItemString(1, 'freight_associated_plant')) Then
			This.SetItem(1, 'freight_associated_plant', data)
		End If
	Case 'freight_associated_plant'
		If ldwc_PlantList.Find('plant_code = "' + data + '"', &
				1, ldwc_PlantList.RowCount() + 1) <= 0 Then
			SetMicroHelp('This is an invalid Plant')
			This.SetFocus()
			This.SelectText(1, 1000)
			Return 1
		End If
End Choose
end event

event constructor;DataWindowChild					ldwc_code, &
										ldwc_frieght_code, &
										ldwc_descr
										

This.InsertRow(0)

dw_detail.GetChild('destination_plant', ldwc_code)

dw_detail.GetChild('destination_plant_descr', ldwc_descr)
ldwc_code.ShareData(ldwc_descr)
dw_detail.GetChild('freight_associated_plant', ldwc_frieght_code)
ldwc_code.ShareData(ldwc_frieght_code)
dw_detail.GetChild('freight_associated_plant_descr', ldwc_descr)
ldwc_code.ShareData(ldwc_descr)


end event

event itemerror;Return 1
end event

