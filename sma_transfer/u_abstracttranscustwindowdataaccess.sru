$PBExportHeader$u_abstracttranscustwindowdataaccess.sru
forward
global type u_abstracttranscustwindowdataaccess from nonvisualobject
end type
end forward

global type u_abstracttranscustwindowdataaccess from nonvisualobject
end type
global u_abstracttranscustwindowdataaccess u_abstracttranscustwindowdataaccess

forward prototypes
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_abstracttranscustwindowdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_abstracttranscustwindowdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

