$PBExportHeader$u_abstracttranfercustomersdataaccess.sru
forward
global type u_abstracttranfercustomersdataaccess from nonvisualobject
end type
end forward

global type u_abstracttranfercustomersdataaccess from nonvisualobject
end type
global u_abstracttranfercustomersdataaccess u_abstracttranfercustomersdataaccess

forward prototypes
public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_update (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieve (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_update (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_abstracttranfercustomersdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_abstracttranfercustomersdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

