HA$PBExportHeader$w_abstractsheetext.srw
forward
global type w_abstractsheetext from w_abstractsheet
end type
end forward

global type w_abstractsheetext from w_abstractsheet
event ue_reset ( )
event ue_mass_update ( )
end type
global w_abstractsheetext w_abstractsheetext

forward prototypes
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);return true
end function

public function boolean wf_initialize (ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext, ref u_abstractparameterstack au_parameterstack);return wf_initialize(au_ClassFactory,au_ErrorContext)
end function

on w_abstractsheetext.create
call super::create
end on

on w_abstractsheetext.destroy
call super::destroy
end on

event open;This.PostEvent('ue_postopen')
end event

