HA$PBExportHeader$u_productdataaccesslocaldb.sru
forward
global type u_productdataaccesslocaldb from u_productdataaccess
end type
end forward

global type u_productdataaccesslocaldb from u_productdataaccess
end type
global u_productdataaccesslocaldb u_productdataaccesslocaldb

type variables
u_odbctransaction			iu_odbctransaction
end variables

forward prototypes
public function boolean uf_retrieveproducts (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrieveproductdivision (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieveproducts (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the products
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the products from the 
</USAGE> pblocaldb database
-------------------------------------------------------- */
Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_products

String							ls_product, &
									ls_productString, &
									ls_ErrorString, &
									ls_PlantType, &
									ls_Beginproduct, &
									ls_Endproduct, &
									lsa_PlantTypes[]

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('string', ls_product)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If lu_string.nf_IsEmpty(ls_product) Then
	ls_Beginproduct = '000'
	ls_Endproduct = 'ZZZ'
Else
	ls_Beginproduct = ls_product
	ls_Endproduct = ls_product
End If

lds_products = Create DataStore
lds_products.DataObject = 'd_products'
lds_products.SetTransObject(iu_ODBCTransaction)

lds_products.Retrieve(ls_Beginproduct, ls_Endproduct)
ls_productString = lds_products.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_productString)

return true

end function

public function boolean uf_retrieveproductdivision (ref u_abstractparameterstack au_parameterstack);/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the products
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the products from the 
</USAGE> pblocaldb database
-------------------------------------------------------- */
Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_products

String							ls_product, &
									ls_productString, &
									ls_ErrorString, &
									ls_PlantType, &
									ls_Beginproduct, &
									ls_Endproduct, &
									lsa_PlantTypes[]

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('string', ls_product)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If lu_string.nf_IsEmpty(ls_product) Then
	ls_Beginproduct = '000'
	ls_Endproduct = 'ZZZ'
Else
	ls_Beginproduct = ls_product
	ls_Endproduct = ls_product
End If

lds_products = Create DataStore
lds_products.DataObject = 'd_product_division'
lds_products.SetTransObject(iu_ODBCTransaction)

lds_products.Retrieve(ls_Beginproduct, ls_Endproduct)
ls_productString = lds_products.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_productString)

return true

end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);u_AbstractClassFactory					lu_ClassFactory

u_AbstractErrorContext				 	lu_ErrorContext

u_AbstractDefaultManager				lu_DefaultManager


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject( "u_ODBCTransaction", iu_ODBCTransaction, lu_ErrorContext ) Then
	If Not lu_ClassFactory.uf_GetObject( "u_iniDefaultManager", lu_DefaultManager, lu_ErrorContext ) Then
		If lu_ErrorContext.uf_IsSuccessful( ) Then
			lu_DefaultManager.uf_Initialize( lu_ClassFactory, 'ibp002.ini', 'SMA DATABASE', lu_ErrorContext )
			If Not lu_ErrorContext.uf_IsSuccessful() Then
				au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
				Return False
			End If
		End If
	End If

	If lu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ODBCTransaction.uf_Initialize( lu_ClassFactory, lu_DefaultManager, lu_ErrorContext )
		If Not lu_ErrorContext.uf_IsSuccessful() Then
			au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
			au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
			Return False
		End If
	End If
End If

Return True
end function

on u_productdataaccesslocaldb.create
call super::create
end on

on u_productdataaccesslocaldb.destroy
call super::destroy
end on

