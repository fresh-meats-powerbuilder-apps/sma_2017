HA$PBExportHeader$u_projectfunctions.sru
forward
global type u_projectfunctions from nonvisualobject
end type
end forward

global type u_projectfunctions from nonvisualobject autoinstantiate
end type

forward prototypes
public function boolean uf_getuserid (ref string as_userid, ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
public subroutine uf_changerowstatus (ref datawindow adw_datawindow, long al_row, dwitemstatus ae_desiredstatus)
end prototypes

public function boolean uf_getuserid (ref string as_userid, ref u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);String							ls_password

u_user							lu_user
u_abstractparameterstack	lu_stack


If Not au_ClassFactory.uf_GetObject("u_user", lu_user, au_ErrorContext) Then
	If Not au_ErrorContext.uf_IsSuccessful() Then Return False
	// If I have to initialize it, I'm in trouble.  I don't know how, but I'll guess
	If Not lu_User.uf_Initialize(au_ClassFactory, au_ErrorContext) Then
		Return False
	End If
End if
	
au_ErrorContext.uf_initialize()

If Not au_ClassFactory.uf_GetObject("u_ParameterStack", lu_stack, au_ErrorContext) Then
	If Not au_ErrorContext.uf_IsSuccessful() Then
		Return False
	End If
	If Not lu_Stack.uf_Initialize() Then
		Return False
	End If
End if

lu_User.uf_retrieve(lu_Stack, au_ErrorContext)
lu_Stack.uf_Pop("string", ls_password)
lu_Stack.uf_Pop("string", as_userid)

Return True

end function

public subroutine uf_changerowstatus (ref datawindow adw_datawindow, long al_row, dwitemstatus ae_desiredstatus);//////////////////////////////////////////////////////////////////////////
//
//	Function:	uf_ChangeRowStatus(al_row, le_DesiredStatus)
//
//	Purpose:		Changes the Item Status of a row or all the rows
//
//	Arguments:	al_row				A long identifying the row(s) to change
//											the itemstatus.  If al_row is 0 then all
//											 rows in the datawindow will be changed.
//					ae_DesiredStatus	The dwItemStatus that you want the row(s)
//											to change to.
//
//	Author:		T.J. Cox
//////////////////////////////////////////////////////////////////////////

long				ll_RowCount, &
					ll_RowIndex, &
					ll_StartRow
integer			li_ChangeSteps, &
					li_ChangeIndex
dwitemstatus	le_CurrentStatus, &
					le_ChangeStatus[]


// Find out if the status should be changed for a single row or
// for every row in adw_datawindow datawindow
If al_row = 0 Then
	ll_StartRow = 1
	ll_RowCount = adw_datawindow.RowCount()	
Else
	ll_StartRow = al_row
	ll_RowCount = al_row
End If

// Loop through the row(s) and change the status to desired itemstatus
For ll_RowIndex = ll_StartRow to ll_RowCount

	// Get the status of the current row
	le_CurrentStatus = adw_datawindow.GetItemStatus(ll_RowIndex, 0, Primary!)

	Choose Case	le_CurrentStatus
		Case	ae_DesiredStatus
			// Do not do anything - it is already at the specified status

		Case	New!
			Choose Case ae_DesiredStatus
				Case NewModified!
					le_ChangeStatus[1] = NewModified!
				Case DataModified!
					le_ChangeStatus[1] = DataModified!
				Case NotModified!
					le_ChangeStatus[1] = DataModified!
					le_ChangeStatus[2] = NotModified!
			End Choose

		Case	NewModified!
			Choose Case ae_DesiredStatus
				Case	New!
					le_ChangeStatus[1] = NotModified!
				Case	DataModified!
					le_ChangeStatus[1] = DataModified!
				Case 	NotModified!
					le_ChangeStatus[1] = DataModified!
					le_ChangeStatus[2] = NotModified!
			End Choose

		Case DataModified!
			Choose Case ae_DesiredStatus
				Case	New!
					le_ChangeStatus[1] = NotModified!
					le_ChangeStatus[2] = New!					
				Case	NewModified!
					le_ChangeStatus[1] = NewModified!
				Case NotModified!
					le_ChangeStatus[1] = NotModified!
			End Choose

		Case NotModified!
			Choose Case ae_DesiredStatus
				Case New!
					le_ChangeStatus[1] = New!					
				Case NewModified!
					le_ChangeStatus[1] = NewModified!
				Case DataModified!
					le_ChangeStatus[1] = DataModified!
			End Choose
	End Choose	

	// Find out the number of steps needed to change the status of the row
	li_ChangeSteps = UpperBound(le_ChangeStatus[])

	// Change the status of the row
	For li_ChangeIndex = 1 to li_ChangeSteps
		adw_datawindow.SetItemStatus(ll_RowIndex, 0, Primary!, le_ChangeStatus[li_ChangeIndex])	
	Next

Next
end subroutine

on u_projectfunctions.create
TriggerEvent( this, "constructor" )
end on

on u_projectfunctions.destroy
TriggerEvent( this, "destructor" )
end on

