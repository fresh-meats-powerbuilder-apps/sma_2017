HA$PBExportHeader$u_customerdataaccess.sru
forward
global type u_customerdataaccess from nonvisualobject
end type
end forward

global type u_customerdataaccess from nonvisualobject
end type
global u_customerdataaccess u_customerdataaccess

forward prototypes
public function boolean uf_retrieveshiptos (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievecorps (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievebilltos (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieveshiptos (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrievecorps (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrievebilltos (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_customerdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_customerdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

