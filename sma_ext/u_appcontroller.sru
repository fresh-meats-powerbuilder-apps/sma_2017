HA$PBExportHeader$u_appcontroller.sru
forward
global type u_appcontroller from u_abstractcontroller
end type
end forward

global type u_appcontroller from u_abstractcontroller
end type
global u_appcontroller u_appcontroller

type variables
Private:

w_smaframe					 iw_smaframe
w_AbstractSheet			 iw_Window
u_AbstractClassFactory	 iu_ClassFactory
u_AbstractErrorContext 	 iu_ErrorContext
u_Abstractparameterstack iu_ParameterStack

end variables

forward prototypes
public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory)
end prototypes

public function boolean uf_initialize (ref u_abstractclassfactory au_classfactory);Boolean										lb_Success
Boolean										lb_Resize

u_resize										lu_Resize

u_AbstractController	  					lu_AppController

u_AbstractController	 				 	lu_LoginController

u_AbstractNotificationController		lu_Notification

m_AbstractMenu								lm_menu

iu_ClassFactory = au_ClassFactory
iu_ErrorContext = CREATE u_errorcontext

If Not IsValid(iu_ErrorContext) Then
	Messagebox("Can't start application","Can't create the error context object for this application.") 
	Halt Close
End If 

If Not iu_ErrorContext.uf_Initialize() Then
	Messagebox("Can't start application","Can't initialize the error context object for this application.") 
	Halt Close
End If

If Not iu_ClassFactory.uf_GetObject("u_notificationcontroller",lu_Notification) Then
	If iu_ErrorContext.uf_IsSuccessful() Then
		lb_Success = lu_Notification.uf_Initialize(iu_ClassFactory)
	Else
		Messagebox("Can't start application","Can't get notification controller object for this application.") 
		Halt Close
	End If
End If

If Not iu_ClassFactory.uf_GetObject( "window", iw_smaframe, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lu_AppController = This
		iw_smaframe.wf_Initialize( iu_ClassFactory,lu_AppController, iu_ErrorContext )
	Else
		lu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

// Set up the resize service for the frame window
//iu_ErrorContext.uf_Initialize()
//iu_ClassFactory.uf_GetObject("u_inidefaultmanager", lu_defaults, iu_ErrorContext)
//If iu_ErrorContext.uf_IsSuccessful() Then
//	lu_defaults.uf_Initialize(iu_ClassFactory, "IBPUSER.INI", GetApplication().AppName + " System Settings", &
//		iu_ErrorContext)
//	If iu_ErrorContext.uf_IsSuccessful() Then
//		lu_defaults.uf_GetData("arrangeopen", ls_arrangeopen, iu_ErrorContext)
//		If iu_ErrorContext.uf_IsSuccessful() Then
//			lb_Resize = (ls_arrangeopen = "last saved")
//		End If
//	End If
//End If

//For the frame, there's no need to check arrangeopen, we always open it last saved:
lb_Resize = True

iu_ErrorContext.uf_Initialize()
iu_ClassFactory.uf_GetObject("u_Resize", lu_Resize, iu_ErrorContext)
If iu_ErrorContext.uf_IsSuccessful() Then
	lu_Resize.uf_Initialize(iu_Classfactory, iw_smaframe, lb_Resize, iu_ErrorContext)
	lu_Resize.uf_Launch(iu_ErrorContext)
End If

iu_ErrorContext.uf_Initialize()
If Not iu_ClassFactory.uf_GetObject( "u_LoginController", lu_LoginController, iu_ErrorContext ) Then
	If iu_ErrorContext.uf_IsSuccessful( ) Then
		lb_Success = lu_LoginController.uf_Initialize( iu_ClassFactory, iu_ErrorContext )
	Else
		lu_Notification.uf_Display(iu_ErrorContext)
		Return False
	End If
End If

If Not iu_ErrorContext.uf_IsSuccessful() Then
	lu_Notification.uf_Display(iu_ErrorContext)
	// Close the frame
	Close(iw_smaframe)
	Return False
End If

If Not lb_Success Then
	Close(iw_smaframe)
	Return False
End If

lm_menu = iw_smaframe.MenuID
lm_menu.m_file.m_open.Event Clicked()

Return True
end function

on u_appcontroller.create
call super::create
end on

on u_appcontroller.destroy
call super::destroy
end on

event ue_open(string as_parameterstring);String							ls_windowname, &
									ls_parameters, &
									ls_WindowTitle, &
									ls_WindowGroup

u_string_functions			lu_string

iu_ClassFactory.uf_GetObject('u_ParameterStack', iu_ParameterStack, iu_ErrorContext)
If Not iu_ErrorContext.uf_IsSuccessful() Then
	iu_ErrorContext.uf_AppendText( "Error creating ParameterStack" )
	iu_ErrorContext.uf_SetReturnCode( -1 )
	Return
End If

iu_ParameterStack.uf_initialize( )

// Get Window Name, Parameters, Window Title and Window Group
ls_windowname = lu_string.nf_GetToken(as_parameterstring, '~t') 
iu_ParameterStack.uf_push( "string", ls_windowname)

ls_parameters = lu_string.nf_GetToken(as_parameterstring, '~t') 
iu_ParameterStack.uf_push( "string", ls_parameters)

ls_WindowTitle = lu_string.nf_GetToken(as_parameterstring, '~t') 
iu_ParameterStack.uf_push( "string", ls_WindowTitle)

ls_WindowGroup = lu_string.nf_GetToken(as_parameterstring, '~t') 
iu_ParameterStack.uf_push( "string", ls_WindowGroup)


IF Not lu_string.nf_IsEmpty(ls_WindowName) Then
	If Not iu_ClassFactory.uf_GetObject( ls_WindowName, iw_Window, iu_ErrorContext ) Then
		If Not iu_ErrorContext.uf_IsSuccessful( ) Then
			iu_ErrorContext.uf_AppendText( "Error creating Window" )
			iu_ErrorContext.uf_SetReturnCode( -1 )
			Return
		Else
// ** IBDKEEM ** Changed wf_Initialize in ext layer to have a 3rd param
//			iw_Window.Post Dynamic wf_Initialize(iu_ClassFactory, iu_ErrorContext)
//			iw_smaframe.wf_setMostRecent(iw_Window.Title) 
// ** END **

			iw_Window.Post Dynamic wf_Initialize(iu_ClassFactory, iu_ErrorContext, iu_ParameterStack)
			iw_smaframe.wf_setMostRecent(ls_WindowTitle)
		End If
	ELSE
		MessageBox("Debug","WindowName is Empty")
	END IF
END IF

end event

