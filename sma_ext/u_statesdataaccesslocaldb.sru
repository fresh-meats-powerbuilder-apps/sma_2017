HA$PBExportHeader$u_statesdataaccesslocaldb.sru
forward
global type u_statesdataaccesslocaldb from u_statesdataaccess
end type
end forward

global type u_statesdataaccesslocaldb from u_statesdataaccess
end type
global u_statesdataaccesslocaldb u_statesdataaccesslocaldb

type variables
u_odbctransaction		iu_odbctransaction
end variables

forward prototypes
public function boolean uf_retrievestates (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrievestates (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the us state codes	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the states from the
</USAGE>  pblocaldb database
-------------------------------------------------------- */
Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_states

String							ls_statestring, &
									ls_ErrorString
									
u_AbstractErrorContext		lu_ErrorContext

u_string_functions			lu_String


au_ParameterStack.uf_Pop('string', ls_statestring)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

//If lu_string.nf_IsEmpty(ls_division) Then
//	ls_Begindivision = '00'
//	ls_Enddivision = 'ZZ'
//Else
//	ls_Begindivision = ls_division
//	ls_Enddivision = ls_division
//End If

lds_states = Create DataStore
lds_states.DataObject = 'd_tutltype_states'
lds_states.SetTransObject(iu_ODBCTransaction)

lds_states.Retrieve()
ls_statestring = lds_states.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_statestring)

return true

end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);u_AbstractClassFactory					lu_ClassFactory

u_AbstractErrorContext				 	lu_ErrorContext

u_AbstractDefaultManager				lu_DefaultManager


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject( "u_ODBCTransaction", iu_ODBCTransaction, lu_ErrorContext ) Then
	If Not lu_ClassFactory.uf_GetObject( "u_iniDefaultManager", lu_DefaultManager, lu_ErrorContext ) Then
		If lu_ErrorContext.uf_IsSuccessful( ) Then
			lu_DefaultManager.uf_Initialize( lu_ClassFactory, 'ibp002.ini', 'SMA DATABASE', lu_ErrorContext )
			If Not lu_ErrorContext.uf_IsSuccessful() Then
				au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
				Return False
			End If
		End If
	End If

	If lu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ODBCTransaction.uf_Initialize( lu_ClassFactory, lu_DefaultManager, lu_ErrorContext )
		If Not lu_ErrorContext.uf_IsSuccessful() Then
			au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
			au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
			Return False
		End If
	End If
End If

Return True
end function

on u_statesdataaccesslocaldb.create
call super::create
end on

on u_statesdataaccesslocaldb.destroy
call super::destroy
end on

