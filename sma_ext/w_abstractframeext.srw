HA$PBExportHeader$w_abstractframeext.srw
forward
global type w_abstractframeext from w_abstractframe
end type
end forward

global type w_abstractframeext from w_abstractframe
boolean TitleBar=true
string Title="SMA Application"
end type
global w_abstractframeext w_abstractframeext

on w_abstractframeext.create
call super::create
end on

on w_abstractframeext.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

