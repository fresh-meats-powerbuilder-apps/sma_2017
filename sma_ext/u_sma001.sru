HA$PBExportHeader$u_sma001.sru
forward
global type u_sma001 from u_netwise
end type
end forward

global type u_sma001 from u_netwise
integer ii_commhnd = 0
end type
global u_sma001 u_sma001

type prototypes
// PowerBuilder Script File: c:\ibp\sma001\sma001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Fri Jul 24 15:39:49 2009
// Source Interface File: c:\ibp\sma001\sma001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: smas00ar_inquiretranscust
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas00ar_inquiretranscust( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "sma001.dll" alias for "smas00ar_inquiretranscust;Ansi"


//
// Declaration for procedure: smas01ar_updtranscust
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas01ar_updtranscust( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_input, &
    string se_detail_input, &
    int CommHnd &
) library "sma001.dll" alias for "smas01ar_updtranscust;Ansi"


//
// Declaration for procedure: smas02ar_inquirecustmast
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas02ar_inquirecustmast( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas02ar_inquirecustmast;Ansi"


//
// Declaration for procedure: smas03ar_updrebateheader
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas03ar_updrebateheader( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string se_input_header, &
    string se_input_detail, &
    int CommHnd &
) library "sma001.dll" alias for "smas03ar_updrebateheader;Ansi"


//
// Declaration for procedure: smas04ar_inqrebateheader
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas04ar_inqrebateheader( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas04ar_inqrebateheader;Ansi"


//
// Declaration for procedure: smas05ar_inqrebatelist
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas05ar_inqrebatelist( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas05ar_inqrebatelist;Ansi"


//
// Declaration for procedure: smas07ar_inqrebates
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas07ar_inqrebates( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas07ar_inqrebates;Ansi"


//
// Declaration for procedure: smas08ar_inqFWHSalesByDivGrp
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas08ar_inqFWHSalesByDivGrp( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas08ar_inqFWHSalesByDivGrp;Ansi"


//
// Declaration for procedure: smas09ar_updFWHSalesByDivGrp
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas09ar_updFWHSalesByDivGrp( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas09ar_updFWHSalesByDivGrp;Ansi"


//
// Declaration for procedure: smas10ar_inqFWHSalesByDiv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas10ar_inqFWHSalesByDiv( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas10ar_inqFWHSalesByDiv;Ansi"


//
// Declaration for procedure: smas11ar_updFWHSalesByDiv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas11ar_updFWHSalesByDiv( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas11ar_updFWHSalesByDiv;Ansi"


//
// Declaration for procedure: smas12ar_updFWHSKUAdj
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas12ar_updFWHSKUAdj( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas12ar_updFWHSKUAdj;Ansi"


//
// Declaration for procedure: smas13ar_inqFWHSKUAdj
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas13ar_inqFWHSKUAdj( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas13ar_inqFWHSKUAdj;Ansi"


//
// Declaration for procedure: smas14ar_inqFWHDivAdj
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas14ar_inqFWHDivAdj( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas14ar_inqFWHDivAdj;Ansi"


//
// Declaration for procedure: smas15ar_updFWHDivAdj
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas15ar_updFWHDivAdj( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas15ar_updFWHDivAdj;Ansi"


//
// Declaration for procedure: smas16ar_inqFWHdivlist
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas16ar_inqFWHdivlist( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas16ar_inqFWHdivlist;Ansi"


//
// Declaration for procedure: smas17ar_inqFWHLocationList
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas17ar_inqFWHLocationList( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas17ar_inqFWHLocationList;Ansi"


//
// Declaration for procedure: smas18ar_inqFWHAssignMaint
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas18ar_inqFWHAssignMaint( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas18ar_inqFWHAssignMaint;Ansi"


//
// Declaration for procedure: smas19ar_updFWHAssignMaint
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas19ar_updFWHAssignMaint( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas19ar_updFWHAssignMaint;Ansi"


//
// Declaration for procedure: smas24ar_productgroupcheck
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas24ar_productgroupcheck( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas24ar_productgroupcheck;Ansi"


//
// Declaration for procedure: smas25ar_inq_appliedrebates
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas25ar_inq_appliedrebates( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas25ar_inq_appliedrebates;Ansi"


//
// Declaration for procedure: smas26ar_upd_appliedrebates
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas26ar_upd_appliedrebates( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string detail_string, &
    int CommHnd &
) library "sma001.dll" alias for "smas26ar_upd_appliedrebates;Ansi"


//
// Declaration for procedure: smas27ar_upd_prod_cat_maint
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas27ar_upd_prod_cat_maint( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "sma001.dll" alias for "smas27ar_upd_prod_cat_maint;Ansi"


//
// Declaration for procedure: smas28ar_inq_cat_by_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas28ar_inq_cat_by_product( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas28ar_inq_cat_by_product;Ansi"


//
// Declaration for procedure: smas29ar_upd_cat_by_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas29ar_upd_cat_by_product( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "sma001.dll" alias for "smas29ar_upd_cat_by_product;Ansi"


//
// Declaration for procedure: smas30ar_inq_tutltype
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int smas30ar_inq_tutltype( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "sma001.dll" alias for "smas30ar_inq_tutltype;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "sma001.dll"
function int WBsma001CkCompleted( int CommHnd ) &
    library "sma001.dll"
//
// ***********************************************************


end prototypes

type variables
Integer		ii_sma001_commhandle
end variables

forward prototypes
public function boolean uf_smas00ar_gettranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas01ar_updatetranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas05ar_getrebatelist (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas03ar_updaterebatedata (string as_appname, string as_windowname, string as_functionname, string as_userid, ref string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas06ar_getorderdata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas07ar_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas11ar_fwh_sales_trans_feight_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas10ar_fwh_sales_trans_freight_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas08ar_fwh_sales_trans_divg_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas09ar_fwh_sales_trans_divg_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas14ar_fwh_div_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas13ar_fwh_sku_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas15ar_fwh_div_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas16ar_fwh_div_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas17ar_fwh_location_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas18ar_fwh_assign_maint_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas19ar_fwh_assign_maint_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas12ar_fwh_sku_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas04ar_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_headerstring, ref string as_detailstring, ref string as_customerstring, ref string as_divisionstring, ref string as_productstring, ref string as_productgroupstring, ref string as_rateseqstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas24ar_productgroupcheck (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas02ar_gettransfercustomers (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas25ar_inq_appliedrebates (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas26ar_upd_appliedrebates (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas29ar_upd_cat_by_product (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas27ar_upd_prod_cat_maint (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas28ar_inq_cat_by_product (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_assignstring, ref string as_unassignstring, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_smas30ar_inq_tutltype (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_smas00ar_gettranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_appname, &
								ls_returncode, &
								ls_message

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
as_outputstring = Space(100)
as_outputstring = Fill(char(0),100)

ls_procedurename = "smas00ar_inquiretranscust"

ls_message = Space(71)
ls_message = Fill(char(0),71)

//li_rtn = smas00ar_inquiretranscust(ls_appname, &
//				as_windowname,&
//				as_functionname,&
//				as_eventname, &
//				ls_procedurename, &
//				as_userid, &
//				ls_returncode, &
//				ls_message, &
//				as_inputstring, &
//				as_outputstring, &
//				ii_commhnd)
  
If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
End If
If li_rtn > 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
End If

Return True


end function

public function boolean uf_smas01ar_updatetranscustwindowdata (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_appname, &
								ls_returncode, &
								ls_message

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information

ls_procedurename = "smas01ar_updtranscustwin"

ls_message = Space(71)
ls_message = Fill(char(0),71)

//li_rtn = smas01ar_updtranscust(ls_appname, &
//				as_windowname,&
//				as_functionname,&
//				as_eventname, &
//				ls_procedurename, &
//				as_userid, &
//				ls_returncode, &
//				ls_message, &
//				as_headerstring, &
//				as_detailstring, &
//				ii_commhnd)

If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
End If
If li_rtn > 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
End If

Return True

end function

public function boolean uf_smas05ar_getrebatelist (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message,ls_output_string

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//as_outputstring = Space(20000)
//as_outputstring = Fill(char(0),20000)
ls_output_string = Space(20000)
ls_output_string = Fill(char(0),20000)

ls_procedurename = "smas05ar_getrebatelist"

ls_message = Space(71)
ls_message = Fill(char(0),71)

as_outputstring = ""

Do
//	li_rtn = smas05ar_inqrebatelist(as_appname, &
//						as_windowname,&
//						as_functionname,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						ls_output_string, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	  
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	as_outputstring += ls_output_string

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If

Loop while ld_last_record_number <> ld_max_record_number 
  
Return True
  

end function

public function boolean uf_smas03ar_updaterebatedata (string as_appname, string as_windowname, string as_functionname, string as_userid, ref string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_EventName, &
								ls_message, &
								ls_string

Int							li_rtn

Time							lt_starttime, &
								lt_endtime

u_String_Functions		lu_String
								

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information

ls_procedurename = "smas03ar_updrebatedata"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_EventName = ''

Do
	ls_string = Left(as_DetailString, lu_string.nf_npos(as_DetailString, '~n', 20000, -2))
	as_DetailString = Mid(as_DetailString, lu_string.nf_npos(as_DetailString, '~n', 20000, -2) + 1) 
	
//	li_rtn = smas03ar_updrebateheader(as_appname, &
//					as_WindowName,&
//					as_FunctionName,&
//					ls_EventName, &
//					ls_ProcedureName, &
//					as_UserId, &
//					ls_ReturnCode, &
//					ls_Message, &
//					as_headerstring, &
//					ls_string, &
//					ii_commhnd)
Loop While Not lu_string.nf_IsEmpty(as_DetailString) And li_rtn >= 0

If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
End If
If li_rtn > 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
End If

Return True

end function

public function boolean uf_smas06ar_getorderdata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

as_outputstring = Space(20000)
as_outputstring = Fill(char(0),20000)

ls_procedurename = "smas06ar_getorderdata"

ls_message = Space(71)
ls_message = Fill(char(0),71)

Do
//	li_rtn = smas06ar_inqorders(as_appname, &
//						as_windowname,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_outputstring, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	  
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If

Loop while ld_last_record_number <> ld_max_record_number 
  
Return True
  

end function

public function boolean uf_smas07ar_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas07ar_getrebatedata"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(20000)
ls_OutputString = Fill(char(0),20000)

Do
//	li_rtn = smas07ar_inqrebates(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	as_detailstring = ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
  
Return True

end function

public function boolean uf_smas11ar_fwh_sales_trans_feight_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas11ar_FWHSalesTranFrieghtUpd"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

Do
//	li_rtn = smas11ar_updFWHSalesByDiv(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas10ar_fwh_sales_trans_freight_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas10ar_FWHSalesTranFrieghtInq"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(20000)
ls_OutputString = Fill(char(0),20000)
ls_output = ""
Do
//	li_rtn = smas10ar_inqFWHSalesByDiv(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas08ar_fwh_sales_trans_divg_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas08ar_FWHSalesTranByDivGInq"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(20000)
ls_OutputString = Fill(char(0),20000)

ls_output = ""

Do
//	li_rtn = smas08ar_inqFWHSalesByDivGrp(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas09ar_fwh_sales_trans_divg_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_headerstring, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas08ar_FWHSalesTranByDivGupd"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

Do
//	li_rtn = smas09ar_updFWHSalesByDivGrp(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_headerstring, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output = ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas14ar_fwh_div_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas14ar_FWH_div_adj_Inq"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(20000)
ls_OutputString = Fill(char(0),20000)
ls_output = ""

Do
//	li_rtn = smas14ar_inqFWHDivAdj(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas13ar_fwh_sku_adj_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas13ar_FWH_sku_adj_Inq"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(20000)
ls_OutputString = Fill(char(0),20000)
ls_output = ""
Do
//	li_rtn = smas13ar_inqFWHSKUAdj(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas15ar_fwh_div_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas15ar_FWH_div_adj_upd"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

Do
//	li_rtn = smas15ar_updFWHDivAdj(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number
as_detailstring = ls_output
return true
end function

public function boolean uf_smas16ar_fwh_div_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas16ar_FWHDivList"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(5500)
ls_OutputString = Fill(char(0),5500)

ls_output = ""

Do
//	li_rtn = smas16ar_inqFWHdivlist(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
//test
as_detailstring = "01" + "~t" + "CARCASS" + "~r~n" + &
						"05" + "~t" + "VARIETY MEATS" + "~r~n" + &
						"07" + "~t" + "TALLOW REFINERY" + "~r~n" + &
						"11" + "~t" + "PROCESSING" + "~r~n" + &
						"15" + "~t" + "COW PROCESSING / VM" + "~r~n" + &
						"16" + "~t" + "VARIETY MEATS - COW" + "~r~n" + &
						"18" + "~t" + "CARCASS - COW" + "~r~n" + &
						"22" + "~t" + "DESIGN PRODUCTS" + "~r~n" + &
						"23" + "~t" + "CONSUMER PRODUCTS" + "~r~n" + &
						"31" + "~t" + "PORK PROCESSING" + "~r~n" + &
						"32" + "~t" + "PORK VARIETY MEATS" + "~r~n" + &
						"34" + "~t" + "MORELLI PORK PRODUCTS" + "~r~n" + &
						"35" + "~t" + "SUPREME PROCESSED FOODS" + "~r~n" + &
						"57" + "~t" + "CASE READY/PUMPED PRODUCTS" + "~r~n"
return true
end function

public function boolean uf_smas17ar_fwh_location_list (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas17ar_FWHLocationList"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(10000)
ls_OutputString = Fill(char(0),10000)

ls_output = ""

Do
//	li_rtn = smas17ar_inqFWHLocationList(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
////test
//as_detailstring = "206" + "~t" + "NORFOLK FWRD WHSE FREEZER" + "~r~n" + &
//						"410" + "~t" + "FORWARD WAREHOUSE-LASALLE" + "~r~n" + &
//						"405" + "~t" + "IBP FWH-MONTGOMERY       " + "~r~n" + &
//						"415" + "~t" + "NORFOLK FORWARD WAREHOUSE" + "~r~n" + &
//						"416" + "~t" + "NORFOLK FORWARD WHSE FREZ" + "~r~n" + &
//						"406" + "~t" + "MONTGOMERY CTY FWH FREZER" + "~r~n" 
return true
end function

public function boolean uf_smas18ar_fwh_assign_maint_inq (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas18ar_inqFWHAssignMaint"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(10000)
ls_OutputString = Fill(char(0),10000)
ls_output = ""
Do
//	li_rtn = smas18ar_inqFWHAssignMaint(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas19ar_fwh_assign_maint_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas19ar_updFWHAssignMaint"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

Do
//	li_rtn = smas19ar_updFWHAssignMaint(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas12ar_fwh_sku_adj_upd (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas12ar_FWH_SKU_adj_upd"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

//** IBDKEEM ** 04/01/2003 ** Remove the DO Loop
//** Why is there refetch logic in an update RPC?
//** This caused the window to call the RPC all night and all day. 
//** Lets try to be a little less sloppy next time, Thanks.
//Do
//	li_rtn = smas12ar_updFWHskuAdj(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
	
//Loop while ld_last_record_number <> ld_max_record_number
as_detailstring = ls_output

return true
end function

public function boolean uf_smas04ar_getrebatedata (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_headerstring, ref string as_detailstring, ref string as_customerstring, ref string as_divisionstring, ref string as_productstring, ref string as_productgroupstring, ref string as_rateseqstring, ref u_abstracterrorcontext au_errorcontext);//The reason this has such a wierd name, because I created one with a proper name and it
//kept getting gpfs.  After naming it to this it started to work.  I am behind schedule, so
//am going with this.  If you want to play, go right ahead.

Boolean						lb_FirstTime

String						ls_ProcedureName, &
								ls_ReturnCode, &
								ls_Message, &
								ls_StringInd, &
								ls_output, &
								ls_OutputString

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_TaskNumber, &
								ld_MaxRecordNumber, &
								ld_LastRecordNumber
								
u_String_Functions		lu_string

SetPointer(HourGlass!)

lb_FirstTime = True

//Call a Netwise external function to get the required information
ld_TaskNumber = 0
ld_LastRecordNumber = 0
ld_MaxRecordNumber = 0

ls_ProcedureName = "smas04ar_inqrebateheader"

ls_Message = Space(71)
ls_Message = Fill(char(0),71)

Do
	ls_OutputString = Space(20000)
	ls_OutputString = Fill(char(0),20000)

//	li_rtn = smas04ar_inqrebateheader(as_AppName, &
//					as_WindowName,&
//					as_FunctionName,&
//					as_EventName, &
//					ls_ProcedureName, &
//					as_UserId, &
//					ls_ReturnCode, &
//					ls_Message, &
//					as_HeaderString, &
//					ls_OutputString, &
//					ld_TaskNumber, &
//					ld_LastRecordNumber, &
//					ld_MaxRecordNumber, &
//					ii_commhnd)
	  
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If

	If lb_FirstTime Then
		as_HeaderString = ''
		lb_FirstTime = False
	End If
	ls_StringInd = Mid(ls_OutputString, 2, 1)
	ls_OutputString = Mid(ls_OutputString, 3)
	Do While Len(Trim(ls_OutputString)) > 0
		ls_output = lu_string.nf_GetToken(ls_OutputString, '~f')
		
		Choose Case ls_StringInd
			Case 'H'
				as_HeaderString += ls_output
			Case 'D'
				as_DetailString += ls_output
			Case 'C'
				as_CustomerString += ls_output
			Case 'V'
				as_DivisionString += ls_output
			Case 'G'
				as_ProductGroupString += ls_output
			Case 'P'
				as_ProductString += ls_output
//  RevGLL  //  *(Begin)
			Case 'L'
				as_RateSeqstring += ls_output
//  RevGLL  //  *(End)
		End Choose
		ls_StringInd = Left(ls_OutputString, 1)
		ls_OutputString = Mid(ls_OutputString, 2)
	Loop 
Loop while ld_LastRecordNumber <> ld_MaxRecordNumber 
  
Return True

end function

public function boolean uf_smas24ar_productgroupcheck (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas24ar_productgroupcheck"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(20000)
ls_OutputString = Fill(char(0),20000)
ls_output = ""

Do
//	li_rtn = smas24ar_productgroupcheck(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_OutputString, &
//						ld_task_number, &
//						ld_last_record_number, &
//						ld_max_record_number, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
Loop while ld_last_record_number <> ld_max_record_number 
as_detailstring = ls_output
return true
end function

public function boolean uf_smas02ar_gettransfercustomers (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_appname, &
								ls_returncode, &
								ls_message, &
								ls_outputstring

Int							li_rtn

Time							lt_starttime, &
								lt_endtime

Double						ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number
								

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ls_outputstring = Space(20000)
ls_outputstring = Fill(char(0),20000)
as_outputstring = ''

ls_procedurename = "smas02ar_inquirecustmast"

ls_message = Space(71)
ls_message = Fill(char(0),71)

Do

//	li_rtn = smas02ar_inquirecustmast(ls_appname, &
//					as_windowname,&
//					as_functionname,&
//					as_eventname, &
//					ls_procedurename, &
//					as_userid, &
//					ls_returncode, &
//					ls_message, &
//					as_inputstring, &
//					ls_outputstring, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhnd)
					
		as_outputstring += ls_outputstring
		
	Loop while ld_last_record_number <> ld_max_record_number
  
If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
End If
If li_rtn > 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
End If

Return True

end function

public function boolean uf_smas25ar_inq_appliedrebates (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_appname, &
								ls_returncode, &
								ls_message, &
								ls_outputstring

Int							li_rtn

Time							lt_starttime, &
								lt_endtime

Double						ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number
								

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ls_outputstring = Space(20000)
ls_outputstring = Fill(char(0),20000)
as_outputstring = ''

ls_procedurename = "smas25ar_inq_appliedrebates"

ls_message = Space(71)
ls_message = Fill(char(0),71)

Do

//	li_rtn = smas25ar_inq_appliedrebates(ls_appname, &
//					as_windowname,&
//					as_functionname,&
//					as_eventname, &
//					ls_procedurename, &
//					as_userid, &
//					ls_returncode, &
//					ls_message, &
//					as_inputstring, &
//					ls_outputstring, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhnd)
					
		as_outputstring += ls_outputstring
		
Loop while ld_last_record_number <> ld_max_record_number
  
If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
End If
If li_rtn > 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
End If

Return True
end function

public function boolean uf_smas26ar_upd_appliedrebates (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas26ar_upd_appliedrebates"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

//li_rtn = smas26ar_upd_appliedrebates(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
	
as_detailstring = ls_output

return true	
end function

public function boolean uf_smas29ar_upd_cat_by_product (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas29ar_upd_cat_by_product"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

//li_rtn = smas29ar_upd_cat_by_product(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_outputstring, &
//						ii_commhnd)
	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
	
as_detailstring = ls_output

return true	
end function

public function boolean uf_smas27ar_upd_prod_cat_maint (string as_appname, string as_windowname, string as_functionname, string as_eventname, string as_userid, ref string as_detailstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_returncode, &
								ls_message, &
								ls_OutputString, &
								ls_output

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_task_number, &
								ld_max_record_number, &
								ld_last_record_number

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0 

ls_procedurename = "smas27ar_upd_prod_cat_maint"

ls_message = Space(71)
ls_message = Fill(char(0),71)
ls_OutputString = space(100)
ls_OutputString = Fill(char(0),100)
ls_output = ""

//li_rtn = smas27ar_upd_prod_cat_maint(as_appname, &
//						as_windowname,&
//						as_FunctionName,&
//						as_eventname, &
//						ls_procedurename, &
//						as_userid, &
//						ls_returncode, &
//						ls_message, &
//						as_detailstring, &
//						ls_outputstring, &
//						ii_commhnd)
//	    
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If
	ls_output += ls_outputstring
	
as_detailstring = ls_output

return true	
end function

public function boolean uf_smas28ar_inq_cat_by_product (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_assignstring, ref string as_unassignstring, ref u_abstracterrorcontext au_errorcontext);Boolean						lb_FirstTime

String						ls_ProcedureName, &
								ls_ReturnCode, &
								ls_Message, &
								ls_StringInd, &
								ls_output, &
								ls_OutputString, &
								ls_appname

Int							li_rtn

Time							lt_starttime, &
								lt_endtime
								
Double						ld_TaskNumber, &
								ld_MaxRecordNumber, &
								ld_LastRecordNumber
								
u_String_Functions		lu_string

SetPointer(HourGlass!)

lb_FirstTime = True

//Call a Netwise external function to get the required information
ld_TaskNumber = 0
ld_LastRecordNumber = 0
ld_MaxRecordNumber = 0

ls_ProcedureName = "smas28ar_inq_cat_by_product"

ls_Message = Space(71)
ls_Message = Fill(char(0),71)

Do
	ls_OutputString = Space(20000)
	ls_OutputString = Fill(char(0),20000)

//	li_rtn = smas28ar_inq_cat_by_product(ls_appname, &
//					as_WindowName,&
//					as_FunctionName,&
//					as_EventName, &
//					ls_ProcedureName, &
//					as_UserId, &
//					ls_ReturnCode, &
//					ls_Message, &
//					as_inputString, &
//					ls_OutputString, &
//					ld_TaskNumber, &
//					ld_LastRecordNumber, &
//					ld_MaxRecordNumber, &
//					ii_commhnd)
  
	If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False
	
	If li_rtn < 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
		Return False
	End If
	If li_rtn > 0 Then
		au_ErrorContext.uf_AppendText(ls_message)
		au_ErrorContext.uf_SetReturnCode(li_rtn)
	End If

	If lb_FirstTime Then
		as_inputString = ''
		lb_FirstTime = False
	End If
	ls_StringInd = Mid(ls_OutputString, 2, 1)
	ls_OutputString = Mid(ls_OutputString, 3)
	Do While Len(Trim(ls_OutputString)) > 0
		ls_output = lu_string.nf_GetToken(ls_OutputString, '~f')
		
		Choose Case ls_StringInd
			Case 'A'
				as_assignstring += ls_output
			Case 'U'
				as_unassignstring += ls_output
		End Choose
		ls_StringInd = Left(ls_OutputString, 1)
		ls_OutputString = Mid(ls_OutputString, 2)
	Loop 
Loop while ld_LastRecordNumber <> ld_MaxRecordNumber 
  
Return True

end function

public function boolean uf_smas30ar_inq_tutltype (string as_windowname, string as_functionname, string as_eventname, string as_userid, string as_inputstring, ref string as_outputstring, ref u_abstracterrorcontext au_errorcontext);String						ls_procedurename, &
								ls_appname, &
								ls_returncode, &
								ls_message, &
								ls_outputstring

Int							li_rtn

Time							lt_starttime, &
								lt_endtime

Double						ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number
								

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information
ls_outputstring = Space(20000)
ls_outputstring = Fill(char(0),20000)
as_outputstring = ''

ls_procedurename = "smas30ar_inq_tutltype"

ls_message = Space(71)
ls_message = Fill(char(0),71)

Do

//	li_rtn = smas30ar_inq_tutltype(ls_appname, &
//					as_windowname,&
//					as_functionname,&
//					as_eventname, &
//					ls_procedurename, &
//					as_userid, &
//					ls_returncode, &
//					ls_message, &
//					as_inputstring, &
//					ls_outputstring, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhnd)
					
		as_outputstring += ls_outputstring
		
Loop while ld_last_record_number <> ld_max_record_number
  
If Not This.uf_CheckRPCError(au_ErrorContext) Then Return False

If li_rtn < 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
	Return False
End If
If li_rtn > 0 Then
	au_ErrorContext.uf_AppendText(ls_message)
	au_ErrorContext.uf_SetReturnCode(li_rtn)
End If

Return True
end function

on u_sma001.create
call super::create
end on

on u_sma001.destroy
call super::destroy
end on

