HA$PBExportHeader$u_divisiondataaccess.sru
forward
global type u_divisiondataaccess from nonvisualobject
end type
end forward

global type u_divisiondataaccess from nonvisualobject
end type
global u_divisiondataaccess u_divisiondataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievedivisions (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrievedivisions (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_divisiondataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_divisiondataaccess.destroy
TriggerEvent( this, "destructor" )
end on

