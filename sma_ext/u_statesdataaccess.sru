HA$PBExportHeader$u_statesdataaccess.sru
forward
global type u_statesdataaccess from nonvisualobject
end type
end forward

global type u_statesdataaccess from nonvisualobject
end type
global u_statesdataaccess u_statesdataaccess

forward prototypes
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievestates (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);Return True
end function

public function boolean uf_retrievestates (ref u_abstractparameterstack au_parameterstack);Return True
end function

on u_statesdataaccess.create
TriggerEvent( this, "constructor" )
end on

on u_statesdataaccess.destroy
TriggerEvent( this, "destructor" )
end on

