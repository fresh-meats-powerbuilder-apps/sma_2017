HA$PBExportHeader$u_customerdataaccesslocaldb.sru
forward
global type u_customerdataaccesslocaldb from u_customerdataaccess
end type
end forward

global type u_customerdataaccesslocaldb from u_customerdataaccess
end type
global u_customerdataaccesslocaldb u_customerdataaccesslocaldb

type variables
u_odbctransaction		iu_odbctransaction
end variables

forward prototypes
public function boolean uf_retrieveshiptos (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievecorps (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_retrievebilltos (ref u_abstractparameterstack au_parameterstack)
public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack)
end prototypes

public function boolean uf_retrieveshiptos (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the Shipto Customers	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the shipto Customers
</USAGE> from the pblocaldb database
-------------------------------------------------------- */
Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_Shiptos

String							ls_shipto, &
									ls_shiptoString, &
									ls_ErrorString, &
									ls_PlantType, &
									ls_BeginCustomer, &
									ls_EndCustomer, &
									lsa_PlantTypes[]

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('string', ls_shipto)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If lu_string.nf_IsEmpty(ls_shipto) Then
	ls_BeginCustomer = '000'
	ls_EndCustomer = 'ZZZ'
Else
	ls_BeginCustomer = ls_shipto
	ls_EndCustomer = ls_shipto
End If

lds_Shiptos = Create DataStore
lds_Shiptos.DataObject = 'd_shipto_customers'
lds_Shiptos.SetTransObject(iu_ODBCTransaction)

lds_Shiptos.Retrieve(ls_BeginCustomer, ls_EndCustomer)
ls_shiptoString = lds_Shiptos.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_shiptoString)

return true

end function

public function boolean uf_retrievecorps (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the Corporate Customers	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Corporate Customers
</USAGE> from the pblocaldb database
-------------------------------------------------------- */
Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_Corps

String							ls_Corp, &
									ls_CorpString, &
									ls_ErrorString, &
									ls_PlantType, &
									ls_BeginCustomer, &
									ls_EndCustomer, &
									lsa_PlantTypes[]

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('string', ls_Corp)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If lu_string.nf_IsEmpty(ls_Corp) Then
	ls_BeginCustomer = '000'
	ls_EndCustomer = 'ZZZ'
Else
	ls_BeginCustomer = ls_Corp
	ls_EndCustomer = ls_Corp
End If

lds_Corps = Create DataStore
lds_Corps.DataObject = 'd_corp_customers'
lds_Corps.SetTransObject(iu_ODBCTransaction)

lds_Corps.Retrieve(ls_BeginCustomer, ls_EndCustomer)
ls_CorpString = lds_Corps.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_CorpString)

return true

end function

public function boolean uf_retrievebilltos (ref u_abstractparameterstack au_parameterstack);
/* --------------------------------------------------------
uf_retrieve()

<DESC> Retrieve the Billto Customers	
</DESC>

<ARGS>	au_ParameterStack: ParameterStack
</ARGS>

<USAGE> This function will retrieve the Billto Customers
</USAGE> from the pblocaldb database
-------------------------------------------------------- */
Integer							li_Count

Long								ll_Rtn, &
									ll_findrow

DataStore						lds_Billtos

String							ls_Billto, &
									ls_BilltoString, &
									ls_ErrorString, &
									ls_PlantType, &
									ls_BeginCustomer, &
									ls_EndCustomer, &
									lsa_PlantTypes[]

u_AbstractErrorContext		lu_ErrorContext

u_string_functions				lu_String


au_ParameterStack.uf_Pop('string', ls_Billto)
au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)

If lu_string.nf_IsEmpty(ls_Billto) Then
	ls_BeginCustomer = '000'
	ls_EndCustomer = 'ZZZ'
Else
	ls_BeginCustomer = ls_Billto
	ls_EndCustomer = ls_Billto
End If

lds_Billtos = Create DataStore
lds_Billtos.DataObject = 'd_Billto_customers'
lds_Billtos.SetTransObject(iu_ODBCTransaction)

lds_Billtos.Retrieve(ls_BeginCustomer, ls_EndCustomer)
ls_BilltoString = lds_Billtos.object.datawindow.data

au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Push('string', ls_BilltoString)

return true

end function

public function boolean uf_initialize (ref u_abstractparameterstack au_parameterstack);u_AbstractClassFactory					lu_ClassFactory

u_AbstractErrorContext				 	lu_ErrorContext

u_AbstractDefaultManager				lu_DefaultManager


au_ParameterStack.uf_Pop('u_AbstractErrorContext', lu_ErrorContext)
au_ParameterStack.uf_Pop('u_AbstractClassFactory', lu_ClassFactory)

If Not lu_ClassFactory.uf_GetObject( "u_ODBCTransaction", iu_ODBCTransaction, lu_ErrorContext ) Then
	If Not lu_ClassFactory.uf_GetObject( "u_iniDefaultManager", lu_DefaultManager, lu_ErrorContext ) Then
		If lu_ErrorContext.uf_IsSuccessful( ) Then
			lu_DefaultManager.uf_Initialize( lu_ClassFactory, 'ibp002.ini', 'SMA DATABASE', lu_ErrorContext )
			If Not lu_ErrorContext.uf_IsSuccessful() Then
				au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
				au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
				Return False
			End If
		End If
	End If

	If lu_ErrorContext.uf_IsSuccessful( ) Then
		iu_ODBCTransaction.uf_Initialize( lu_ClassFactory, lu_DefaultManager, lu_ErrorContext )
		If Not lu_ErrorContext.uf_IsSuccessful() Then
			au_ParameterStack.uf_Push('u_AbstractClassFactory', lu_ClassFactory)
			au_ParameterStack.uf_Push('u_AbstractErrorContext', lu_ErrorContext)
			Return False
		End If
	End If
End If

Return True
end function

on u_customerdataaccesslocaldb.create
call super::create
end on

on u_customerdataaccesslocaldb.destroy
call super::destroy
end on

