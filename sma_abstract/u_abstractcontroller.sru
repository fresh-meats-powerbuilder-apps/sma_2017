HA$PBExportHeader$u_abstractcontroller.sru
forward
global type u_abstractcontroller from nonvisualobject
end type
end forward

global type u_abstractcontroller from nonvisualobject
event ue_open ( string as_parameterstring )
end type
global u_abstractcontroller u_abstractcontroller

forward prototypes
public function boolean uf_initialize (u_AbstractClassFactory au_ClassFactory, ref u_AbstractErrorContext au_ErrorContext)
end prototypes

public function boolean uf_initialize (u_AbstractClassFactory au_ClassFactory, ref u_AbstractErrorContext au_ErrorContext);return false
end function

on u_abstractcontroller.create
TriggerEvent( this, "constructor" )
end on

on u_abstractcontroller.destroy
TriggerEvent( this, "destructor" )
end on

