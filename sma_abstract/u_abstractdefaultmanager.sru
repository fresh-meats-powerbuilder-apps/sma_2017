HA$PBExportHeader$u_abstractdefaultmanager.sru
forward
global type u_abstractdefaultmanager from nonvisualobject
end type
end forward

global type u_abstractdefaultmanager from nonvisualobject
end type
global u_abstractdefaultmanager u_abstractdefaultmanager

forward prototypes
public function boolean uf_getdata (string as_key, ref any aa_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref string as_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref long al_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref decimal adc_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref real ar_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref int ai_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_initialize (ref u_abstractclassfactory acf_classfactory, string as_datasource, string as_tableorsection, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, any aa_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, string as_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, long al_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, datetime adt_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, decimal adc_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, real ar_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, int ai_value, ref u_AbstractErrorContext au_ErrorContext)
public function boolean uf_setdata (string as_key, boolean ab_value, ref u_AbstractErrorContext au_ErrorContext)
end prototypes

public function boolean uf_getdata (string as_key, ref any aa_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref string as_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref long al_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref datetime adt_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref decimal adc_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref real ar_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref int ai_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_getdata (string as_key, ref boolean ab_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_initialize (ref u_abstractclassfactory acf_classfactory, string as_datasource, string as_tableorsection, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, any aa_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, string as_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, long al_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, datetime adt_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, decimal adc_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, real ar_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, int ai_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

public function boolean uf_setdata (string as_key, boolean ab_value, ref u_AbstractErrorContext au_ErrorContext);Return False
end function

on u_abstractdefaultmanager.create
TriggerEvent( this, "constructor" )
end on

on u_abstractdefaultmanager.destroy
TriggerEvent( this, "destructor" )
end on

