HA$PBExportHeader$u_abstractsecuritymanager.sru
forward
global type u_abstractsecuritymanager from nonvisualobject
end type
end forward

global type u_abstractsecuritymanager from nonvisualobject
end type
global u_abstractsecuritymanager u_abstractsecuritymanager

forward prototypes
public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext)
private function boolean uf_getpermissions (string as_objectname, ref boolean ab_inquire, ref boolean ab_modify, ref boolean ab_add, ref boolean ab_delete, ref u_abstracterrorcontext au_errorcontext)
public function boolean uf_setmenupermissions (ref menu am_menu, ref u_abstracterrorcontext au_errorcontext)
end prototypes

public function boolean uf_initialize (u_abstractclassfactory au_classfactory, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_initialize

<DESC> Initialize the Security manager and obtain all 
		connections to the source of the data
</DESC>

<ARGS>    au_classfactory: ClassFactory
          au_errorcontext: ErrorContext
</ARGS>

<USAGE> Call this function after getting the descendant
        object from the ClassFactory. If this function
        returns False, the error context text contains
        the reason for the failure.
</USAGE>
-------------------------------------------------------- */
return false
end function

private function boolean uf_getpermissions (string as_objectname, ref boolean ab_inquire, ref boolean ab_modify, ref boolean ab_add, ref boolean ab_delete, ref u_abstracterrorcontext au_errorcontext);
/* --------------------------------------------------------
uf_GetPermissions

<DESC> Get the permissions for a particular object
</DESC>

<ARGS> 	as_objectname: The object to retrieve the permissions for
			ab_inquire: True if the object can be inquired on
			ab_save: True if the object can be saved
			ab_new: True if the object can do "new"
			ab_delete: True if object has permission to delete
			au_errorcontext: Handles all errors for the function
</ARGS>

<USAGE> Pass any object to this function and the booleans
			will be filled in with the appropriate value.
			If this function returns false, then the errorcontext
			will be populated with the error.
</USAGE>
-------------------------------------------------------- */
return false
end function

public function boolean uf_setmenupermissions (ref menu am_menu, ref u_abstracterrorcontext au_errorcontext);return False
end function

on u_abstractsecuritymanager.create
TriggerEvent( this, "constructor" )
end on

on u_abstractsecuritymanager.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_AbstractSecurityManager

<OBJECT> The decendants of this abstract object will control
			Window, menu and object level security.
</OBJECT>

<USAGE> Inherit from this object, and implement all functions.
</USAGE>

<AUTH> Tim Bornholtz </AUTH>
--------------------------------------------------------- */


end event

